import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCoaComponent } from './main-coa.component';

describe('MainCoaComponent', () => {
  let component: MainCoaComponent;
  let fixture: ComponentFixture<MainCoaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainCoaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainCoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
