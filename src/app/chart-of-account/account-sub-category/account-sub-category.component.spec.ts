import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSubCategoryComponent } from './account-sub-category.component';

describe('AccountSubCategoryComponent', () => {
  let component: AccountSubCategoryComponent;
  let fixture: ComponentFixture<AccountSubCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountSubCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
