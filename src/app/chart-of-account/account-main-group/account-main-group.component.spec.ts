import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountMainGroupComponent } from './account-main-group.component';

describe('AccountMainGroupComponent', () => {
  let component: AccountMainGroupComponent;
  let fixture: ComponentFixture<AccountMainGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountMainGroupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountMainGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
