import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartOfAccountRoutingModule } from './chart-of-account-routing.module';
import { AccountCategoryComponent } from './account-category/account-category.component';
import { AccountSubCategoryComponent } from './account-sub-category/account-sub-category.component';
import { AccountMainGroupComponent } from './account-main-group/account-main-group.component';
import { AccountSubGroupComponent } from './account-sub-group/account-sub-group.component';
import { ChartOfAccountComponent } from './chart-of-account/chart-of-account.component';
import { MainCoaComponent } from './main-coa/main-coa.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '../shared/dynamic-form/dynamic-form.module';
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../shared/components/components.module';

@NgModule({
  declarations: [
    
    AccountCategoryComponent,
    AccountSubCategoryComponent,
    AccountMainGroupComponent,
    AccountSubGroupComponent,
    ChartOfAccountComponent,
    MainCoaComponent,
  ],
  imports: [
    CommonModule,
    ChartOfAccountRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule,
    ComponentsModule
  ]
})
export class ChartOfAccountModule { }
