import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSubGroupComponent } from './account-sub-group.component';

describe('AccountSubGroupComponent', () => {
  let component: AccountSubGroupComponent;
  let fixture: ComponentFixture<AccountSubGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountSubGroupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountSubGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
