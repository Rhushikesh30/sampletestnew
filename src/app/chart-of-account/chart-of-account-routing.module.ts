import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



import { AccountCategoryComponent } from './account-category/account-category.component';
import { AccountSubCategoryComponent } from './account-sub-category/account-sub-category.component';
import { AccountMainGroupComponent } from './account-main-group/account-main-group.component';
import { AccountSubGroupComponent } from './account-sub-group/account-sub-group.component';
import { ChartOfAccountComponent } from './chart-of-account/chart-of-account.component';

import { MainCoaComponent } from './main-coa/main-coa.component';


const routes: Routes = [

    {
      path: '',
      pathMatch: 'full',
      component: MainCoaComponent
    },
    {
      path: 'chartofaccount',
      component: ChartOfAccountComponent
    }, 
    {
      path: 'accountcategory',
      component: AccountCategoryComponent
    }, 
    {
      path: 'accountsubcategory',
      component: AccountSubCategoryComponent
    },
    {
      path: 'accountmaingroup',
      component: AccountMainGroupComponent
    },
    {
      path: 'accountsubgroup',
      component: AccountSubGroupComponent
    },
   
   
  
  ];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartOfAccountRoutingModule { }
