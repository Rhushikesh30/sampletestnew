import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from './material.module';
import { FeatherIconsModule } from './components/feather-icons/feather-icons.module';
import { DynamicFormModule } from './dynamic-form/dynamic-form.module';
import { SafeHtmlPipe } from './common-methods/safe-html.pipe';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [SafeHtmlPipe],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    FeatherIconsModule,
    DynamicFormModule,
    ToastrModule.forRoot(),
    // DynamicTableDataModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    FeatherIconsModule,
    SafeHtmlPipe,
  ],
})
export class SharedModule {}
