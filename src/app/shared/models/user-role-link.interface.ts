export interface UserRoleLinkInterface { 
    id:Number,
    emp_name:string,
    email:string,
    phone:number,
    designation:string
}