import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root'
})
export class StockAdjestmentService extends UnsubscribeOnDestroyAdapter {

  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  constructor(private http: HttpClient) { super(); }

  get data(): any[] {
    return this.dataChange.value;
  }

  getAllAdvanceTables() {
    return this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/stock-adjestment/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        }
      }
      );
  }

  getMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getCountryData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getFiscalYearAndPeriod(){
    return this.http.get(`${environment.apiUrl}/fiscal-year-period/`);
  }

  getStockDataByItem(item_type_ref_id:any){
    return this.http.get<any[]>(`${environment.apiUrl}/inventory-stock/`, {
      params: { item_type_ref_id },
    });
  }

  getDynamicData(get_dynamic_data: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }

  createStockAdjestment(formValue: any, id: any) {
    if (id) {
      return this.http.put<any[]>(`${environment.apiUrl}/stock-adjestment/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
    else {

      return this.http.post<any[]>(`${environment.apiUrl}/stock-adjestment/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
    }
    return throwError(() => new Error(errorMessage));;
  }
}
