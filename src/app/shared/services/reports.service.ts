import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class ReportsService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/payable_reports/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getDynamicData(get_dynamic_data: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }

  getInventoryData(
    item_ref_id: any,
    item_attribute1: any,
    item_attribute2: any,
    page_number: number,
    page_limit: number
  ) {
    return this.http.get(`${environment.apiUrl}/inventory_reports/`, {
      params: {
        item_ref_id,
        item_attribute1,
        item_attribute2,
        page_number,
        page_limit,
      },
    });
  }

  GetPayableBySupplier(GetPayableBySupplier: any, supplier_ref_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/payable_reports/`, {
      params: { GetPayableBySupplier, supplier_ref_id },
    });
  }

  DownloadPyableLedgerTable(type: any, formValue: any) {
    const ReportType = 'Payable';

    return this.http.post<any[]>(
      `${environment.apiUrl}/download_payable_reports/`,
      {
        params: { ReportType, formValue, type },
      }
    );
  }

  GetPayableAllDaata() {
    return this.http.get<any[]>(`${environment.apiUrl}/payable_reports/`);
  }

  GetPayableAllDaataWithPagination(
    page_number: number,
    page_limit: number,
    payable_or_receivable: string,
    phone_number: string
    // supplier_name: string
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/payable_reports/`, {
      params: {
        page_number,
        page_limit,
        payable_or_receivable,
        phone_number,
        // supplier_name,
      },
    });
  }

  Getgstr1AllDaataWithPagination(
    parameter: any,
    date: any,
    condition: any,
    page_number: number,
    page_limit: number
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/gstr1_reports/`, {
      params: { page_number, page_limit, date, condition, parameter },
    });
  }

  getTrialBalanceReportData(
    date: any,
    page_number: number,
    page_limit: number
  ) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/trial_balance_reports/`,
      {
        params: {
          date,
          // ,page_number, page_limit
        },
      }
    );
  }

  GetBalanceSheet(date: any, page_number: number, page_limit: number) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/balance_sheet_reports/`,
      {
        params: {
          date,
          page_number,
          page_limit,
        },
      }
    );
  }

  getProfitLossReportData(
    date: any,
    division_ref_id: any,
    department_ref_id: any,
    item_ref_id: any,
    location_ref_id: any,
    sale_type_ref_id: any,
    page_number: number,
    page_limit: number
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/profit_loss_report/`, {
      params: {
        date,
        division_ref_id,
        department_ref_id,
        item_ref_id,
        location_ref_id,
        sale_type_ref_id,
        page_number,
        page_limit,
      },
    });
  }

  // getLedgerEnquiryReportData(date:any,page_number: number, page_limit: number) {
  //   return this.http.get<any[]>(`${environment.apiUrl}/ledger-enquiry-report/`, {
  //     params: {
  //       date,page_number, page_limit
  //     }});
  // }
  getLedgerEnquiryReportData(
    account_ref_id: any,
    from_date: any,
    to_date: any,
    page_number: number,
    page_limit: number
  ) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/ledger-enquiry-report/`,
      {
        params: {
          account_ref_id,
          from_date,
          to_date,
          page_number,
          page_limit,
        },
      }
    );
  }

  download(formValue: any, report_name: any) {
    return this.http.post<any[]>(`${environment.apiUrl}/download-excel/`, {
      params: { formValue, report_name },
    });
  }

  GetGSTR1(date: any, condition: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/gstr1_reports/`, {
      params: { date, condition },
    });
  }

  GetGSTR3B(date: any, condition: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/gstr1_reports/`, {
      params: { date, condition },
    });
  }

  DownloadGSTR1Table(type: any, formValue: any, ReportType: any) {
    return this.http.post<any[]>(
      `${environment.apiUrl}/download_payable_reports/`,
      {
        params: { formValue, type, ReportType },
      }
    );
  }

  getDynamicData1(screen_name: any, field_name: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
