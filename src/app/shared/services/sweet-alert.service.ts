import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class SweetAlertService {
  constructor(private toastr: ToastrService) {}

  showSwalMessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any,
    backgroundColor: string
  ): void {
    const options: ToastOptions = {
      timeOut: 200000,
      closeButton: !confirmButton,
      // progressBar: true,
      positionClass: 'toast-bottom-center',
    };
    console.log(confirmButton);

    if (backgroundColor) {
      options['onShown'] = (toast: any) => {
        toast.el.style.backgroundColor = backgroundColor;
        setTimeout(() => {
          toast.el.style.top = '20px'; // Adjust as needed
        });
      };
    }

    this.toastr.success(message, text, options);
  }
}

interface ToastOptions {
  timeOut: number;
  closeButton: boolean;
  //progressBar: boolean;
  positionClass: string;
  [key: string]: any; // Index signature for potential additional properties
}
