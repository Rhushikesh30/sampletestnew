import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorReportService {
  next(error: any) {
    throw new Error('Method not implemented.');
  }
  errorReport = new Subject<any>();

  constructor() { }
}
