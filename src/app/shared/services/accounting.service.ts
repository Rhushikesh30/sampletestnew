import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class AccountingService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/accounting-definition/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getTenantData() {
    return this.http.get(`${environment.apiUrl}/tenantdata/`);
  }

  getDynamicScreenData(screen_name: string) {
    return this.http.get<any>(
      `${environment.apiUrl}`.concat(
        '/dynamic-screen-wrapper/' + screen_name + '/'
      )
    );
  }

  getTenantById(id: any) {
    return this.http
      .get(`${environment.apiUrl}/accounting-definition/`, { params: { id } })
      .pipe(catchError(this.errorHandler));
  }

  createAccounting(formValue: any, id: any) {
    if (id) {
      // for(var i=0 ; i < formValue.defination_details.length; i++){

      //   if (formValue.defination_details[i].id == '') {
      //     delete formValue.defination_details[i].id;
      //   }
      // }

      return this.http
        .put<any[]>(
          `${environment.apiUrl}/accounting-definition/${id}/`,
          formValue
        )
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/accounting-definition/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getAccountingParameters(parent_id: any) {
    var GetAccountdefinitionRefName = 'GetAccountdefinitionRefName';

    return this.http.get<any[]>(`${environment.apiUrl}/accounting_parameter/`, {
      params: { parent_id, GetAccountdefinitionRefName },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
