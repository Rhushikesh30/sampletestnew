import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class DynamicFormService extends UnsubscribeOnDestroyAdapter {
  constructor(private http: HttpClient) {
    super();
  }

  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables(screen_name: any): void {
    this.subs.sink = this.http
      .get<any[]>(
        `${environment.apiUrl}/dynamic-screen-wrapper/` + screen_name + '/'
      )
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data.screenmatlistingdata_set);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      });
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/')
    );
  }

  getStepperDyamicDetails(stepper_module_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat(
        '/dynamic-screen-hierarchy/?stepper_module_name=' + stepper_module_name
      )
    );
  }

  saveStepperPoolAttribute(formValue: any): Observable<any[]> {
    const id = formValue[0].id;
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/dynamic-screen-module/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          }),
          catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/dynamic-screen-module/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          }),
          catchError(this.errorHandler)
        );
    }
  }

  bulkUploadFile(file: any) {
    var fd = new FormData();
    fd.append('file', file);
    return this.http
      .post(`${environment.apiUrl}/bulk-upload/`, fd)
      .pipe(map((response: any) => response))
      .pipe(
        map((data) => {
          data['status'] = 2;
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  createScreenData(formValue: any) {
    if (formValue.id) {
      const id = formValue.id;
      return this.http
        .put(`${environment.apiUrl}/dynamic-screen-wrapper/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
        );
    } else {
      return this.http
        .post(`${environment.apiUrl}/dynamic-screen-wrapper/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
        );
    }
  }

  deleteScreenData(value: any) {
    return this.http
      .put(`${environment.apiUrl}/dynamic-screen-wrapper/${value.id}/`, value)
      .pipe(
        map((data: any) => {
          data['status'] = 1;
          return data;
        })
      );
  }

  getDynamicScreenData(screen_name: string) {
    return this.http.get<any>(
      `${environment.apiUrl}`.concat(
        '/dynamic-screen-wrapper/' + screen_name + '/'
      )
    );
  }

  getScreennDataById(id: number, screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}/dynamic-screen-wrapper/` +
        screen_name +
        '/' +
        id +
        '/'
    );
  }

  getFpcNameInRateCommision(pricing_type: any) {
    return this.http.get(`${environment.apiUrl}/tenant-by-role/`, {
      params: { pricing_type },
    });
  }

  getOnChangeEventtVal(dict: any) {
    return this.http.post(
      `${environment.apiUrl}/dynamic-screen-field-data/`,
      dict
    );
  }

  getOnChangeEventDetailsVal(dict: any) {
    return this.http.post(
      `${environment.apiUrl}/dynamic-screen-details-field-data/`,
      dict
    );
  }

  saveFile(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/fileuploadurl/`, formValue)
      .pipe(
        map((data) => {
          return data;
        })
        //  catchError(this.errorHandler)
      );
  }

  getDynamicData(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  validateStepDetailsData(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/upload-item-details/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
    //  return this.http.post<any[]>(`${environment.apiUrl}/upload-item-details/`,formValue)
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
