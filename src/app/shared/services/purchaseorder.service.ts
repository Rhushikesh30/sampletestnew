import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class PurchaseorderService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/purchase_order/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  createPurchaseOrder(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/purchase_order/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/purchase_order/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
        );
    }
  }

  getTenatNameData(tenant_name_get: any) {
    return this.http.get(`${environment.apiUrl}/tenant/`, {
      params: { tenant_name_get },
    });
  }

  getDynamicData(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  ItemTaxCal(item_id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/purchase_item_tax_cal/`,
      {
        params: { item_id },
      }
    );
  }

  SalesOrderNumberGet() {
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {});
  }

  getTaxRate(GetTaxRate: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase_order/`, {
      params: { GetTaxRate },
    });
  }

  GetBillingShippingAdrress(screen_name: any, id: any, parent_id: any) {
    let get_address = 'get_address';
    return this.http.get(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      { params: { get_address, screen_name, id, parent_id } }
    );
  }

  getPODataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/purchase_order/` + id + '/'
    );
  }

  getAllempusrData(employee_user: string) {
    return this.http.get<any[]>(`${environment.apiUrl}/employeeuser/`, {
      params: { employee_user },
    });
  }

  getFilterIData(GatePassFilter: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { GatePassFilter, screen_name, field_name },
    });
  }

  getPOPrintDataById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/print-po/` + id + '/');
  }

  getCustomerByType(screen_name: any, field_name: any, type: any) {
    let get_data_by_type = 'get_data_by_type';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_data_by_type, screen_name, field_name, type },
      }
    );
  }

  errorHandler(error: any) {
    console.log(error);

    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message);
      errorMessage = error.error.message;
    } else {
      console.log(JSON.stringify(error), JSON.stringify(error), error.message);
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
