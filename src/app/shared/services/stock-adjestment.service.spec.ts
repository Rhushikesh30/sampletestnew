import { TestBed } from '@angular/core/testing';

import { StockAdjestmentService } from './stock-adjestment.service';

describe('StockAdjestmentService', () => {
  let service: StockAdjestmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockAdjestmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
