import { TestBed } from '@angular/core/testing';

import { PurchaseBookingService } from './purchase-booking.service';

describe('PurchaseBookingService', () => {
  let service: PurchaseBookingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchaseBookingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
