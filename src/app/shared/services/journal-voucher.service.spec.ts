import { TestBed } from '@angular/core/testing';

import { JournalVoucherService } from './journal-voucher.service';

describe('JournalVoucherService', () => {
  let service: JournalVoucherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JournalVoucherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
