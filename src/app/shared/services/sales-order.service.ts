import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class SalesOrderService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getCountryData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getStatesData(country_id: any) {
    return this.http.get(`${environment.apiUrl}/state/`, {
      params: { country_id },
    });
  }

  getDistrictData(state_id: any) {
    return this.http.get(`${environment.apiUrl}/district/`, {
      params: { state_id },
    });
  }

  getTalukaData(district_id: any) {
    return this.http.get(`${environment.apiUrl}/taluka/`, {
      params: { district_id },
    });
  }

  getDynamicData(get_dynamic_data: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }

  getDynamicPaymentData(
    get_dynamic_data: any,
    screen_name: any,
    field_name: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }

  getDynamicDataCombine(
    get_combine_columns: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_combine_columns, screen_name, field_name1, field_name2 },
    });
  }

  getAllMasterCustomerTypeDataCombine(field_name1: any) {
    return this.http.get(`${environment.apiUrl}/master/`, {
      params: { field_name1 },
    });
  }

  GetBillingShippingAdrress(screen_name: any, id: any, parent_id: any) {
    const get_address = 'get_address';
    return this.http.get(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      { params: { get_address, screen_name, id, parent_id } }
    );
  }

  getPurchaseOrder(purchase_order: any) {
    return this.http.get(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      { params: { purchase_order } }
    );
  }

  getFpcPodetails(purchase_order: any) {
    return this.http.post(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      purchase_order
    );
  }

  getTenatNameData(tenant_name_get: any) {
    return this.http.get(`${environment.apiUrl}/tenant/`, {
      params: { tenant_name_get },
    });
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/sales-order/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getItemQualityParameter(
    item_ref_id: any,
    master_value: any,
    transaction_date: any
  ) {
    return this.http
      .get(`${environment.apiUrl}/so_quality_data/`, {
        params: { item_ref_id, master_value, transaction_date },
      })
      .pipe(catchError(this.errorHandler));
  }

  getEmployeeById(id: any) {
    return this.http
      .get(`${environment.apiUrl}/employee/`, { params: { id } })
      .pipe(catchError(this.errorHandler));
  }

  createSalesOrderRecord(formValue: any, id: any) {
    if (id) {
      // formValue.order_details.forEach((ele:any, ind:number) => {
      //   if(ele.id==''){
      //     delete formValue.order_details.at(ind).id;
      //   }
      // });
      return this.http
        .put<any[]>(`${environment.apiUrl}/sales-order/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          // catchError(this.errorHandler)
        );
    } else {
      // delete formValue['id']
      return this.http
        .post<any[]>(`${environment.apiUrl}/sales-order/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
  getDynamicDataa(screen_name: any, field_name: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }
}
