import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class ManageSecurityService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAccessLeftPanel(username: any, form_name: any) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/left_pannel_access/'),
      {
        params: {
          username,
          form_name,
        },
      }
    );
  }

  getAllPagesRole() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/pages-role-link/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getEmployeeById(id: any) {
    return this.http
      .get(`${environment.apiUrl}/employee/`, { params: { id } })
      .pipe(catchError(this.errorHandler));
  }

  createScreenToRole(formValue: any) {
    formValue.forEach((ele: any, ind: number) => {
      if (ele.id == '') {
        delete formValue.at(ind).id;
      }
    });
    return this.http
      .post<any[]>(`${environment.apiUrl}/pages-role-link/`, formValue)
      .pipe(
        map((data: any) => {
          return data;
        })
        //     catchError(this.errorHandler)
      );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
