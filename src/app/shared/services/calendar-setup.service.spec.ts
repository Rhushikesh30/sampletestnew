import { TestBed } from '@angular/core/testing';

import { CalendarSetupService } from './calendar-setup.service';

describe('CalendarSetupService', () => {
  let service: CalendarSetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalendarSetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
