import { TestBed } from '@angular/core/testing';

import { JournalVoucherBookingService } from './journal-voucher-booking.service';

describe('JournalVoucherBookingService', () => {
  let service: JournalVoucherBookingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JournalVoucherBookingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
