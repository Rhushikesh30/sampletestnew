import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class PaymentsService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/payments/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  createPayments(formValue: any) {
    const id = formValue['id'];
    if (id) {
      formValue.payment_details.forEach((ele: any, ind: number) => {
        if (ele.id == '') {
          delete formValue.payment_details.at(ind).id;
        }
      });
      return this.http
        .put<any[]>(`${environment.apiUrl}/payments/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          // catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/payments/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          // catchError(this.errorHandler)
        );
    }
  }

  getPaymentDataById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/payments/`, {
      params: { id },
    });
  }

  getPOSupplierData() {
    let getPOSupplierData = 'getPOSupplierData';
    return this.http.get<any[]>(`${environment.apiUrl}/payments/`, {
      params: { getPOSupplierData },
    });
  }

  getRegularSupplierData() {
    let getRegularSupplierData = 'getRegularSupplierData';
    return this.http.get<any[]>(`${environment.apiUrl}/payments/`, {
      params: { getRegularSupplierData },
    });
  }

  GetPurchaseBokkingBySupplier(get_by_supplier: any, supplier_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase-booking/`, {
      params: { get_by_supplier, supplier_id },
    });
  }

  GetExpenseBookingBySupplier(get_expense_by_supplier: any, supplier_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase-booking/`, {
      params: { get_expense_by_supplier, supplier_id },
    });
  }
  getPoAmountAdvancePayment(getPoAmountAdvancePayment: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/payments/`, {
      params: { getPoAmountAdvancePayment },
    });
  }

  GetPOBySupplier(po_by_supplier_id: any, supplier_ref_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase_order/`, {
      params: { po_by_supplier_id, supplier_ref_id },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
