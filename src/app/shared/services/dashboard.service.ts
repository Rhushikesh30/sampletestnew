import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getTilesDetails() {
    return this.http.get(`${environment.apiUrl}/dashboard-tiles/`).pipe(catchError(this.errorHandler));
  }

  getSalesTableDetails(item_ref_id:any,customer_ref_id:any,supplier_ref_id:any) {
    let type:any = 'Sales'
    return this.http.get(`${environment.apiUrl}/dashboard-tables/`,{ params: { type,item_ref_id,customer_ref_id,supplier_ref_id } }).pipe(catchError(this.errorHandler));
  }
  getPurchaseTableDetails(item_ref_id:any,customer_ref_id:any,supplier_ref_id:any) {
    let type:any = 'Purchase'
    return this.http.get(`${environment.apiUrl}/dashboard-tables/`,{ params: { type,item_ref_id,customer_ref_id,supplier_ref_id } }).pipe(catchError(this.errorHandler));
  }
  getGrossMarginData() {
    return this.http.get(`${environment.apiUrl}/gross-margin/`).pipe(catchError(this.errorHandler));
  }
  // getTablesDetails(type:any,formdata:any) {
  //   return this.http.get(`${environment.apiUrl}/dashboard-tables/`,{ params: {type ,formdata} }).pipe(catchError(this.errorHandler));
  // }
  getDynamicData(screen_name: any,field_name:any) {
    let get_dynamic_data='get_dynamic_data'
    return this.http.get<any[]>(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data,screen_name,field_name },
    });
  }
  getDynamicfieldData(get_dynamic_data: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }
  getCustomerItemData(customer_ref_id: any) {
    return this.http.get(`${environment.apiUrl}/so-customer/`, {
      params: {customer_ref_id},
    });
  }
  
  getSupplierItemData(supplier_ref_id: any) {
    return this.http.get(`${environment.apiUrl}/item-by-supplier/`, {
      params: {supplier_ref_id},
    });
  }
  


errorHandler(error: any) {
  let errorMessage = '';
  if (error.error instanceof ErrorEvent) {
    errorMessage = error.error.message;
  } else {
    errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
  }
  return throwError(() => new Error(errorMessage));
}

}