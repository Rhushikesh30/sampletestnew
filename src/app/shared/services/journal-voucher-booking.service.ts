import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class JournalVoucherBookingService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/journal_voucher_report/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getJVDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/journal_voucher_report/`,
      {
        params: { id },
      }
    );
  }

  createJournalVoucher(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(
          `${environment.apiUrl}/journal_voucher_report/${id}/`,
          formValue
        )
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/journal_voucher_report/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getSessionData() {
    let Session_data = 'Session_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/journal_voucher_report/`,
      {
        params: { Session_data },
      }
    );
  }

  getDynamicData(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
