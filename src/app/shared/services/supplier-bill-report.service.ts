import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root'
})
export class SupplierBillReportService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }
  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/show-farmer-bill/`)
      .subscribe({
        next: (data: any) => {
          console.log(data)
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        }
      });
  }

  // getFarmerBillDataById(id: any) {
  //   return this.http.get(`${environment.apiUrl}/show-farmer-bill/`, { params: { id } });
  // }

  getFarmerBillData(id: any) {
    return this.http.get(`${environment.apiUrl}/print-farmerbill/` + id + '/');
  }

  getSearchData(search: any, data:any){
    return this.http.get(`${environment.apiUrl}/show-farmer-bill/`, { params: { search, data } })
    .subscribe({
      next: (data: any) => {
        console.log(data)
        this.isTblLoading = false;
        this.dataChange.next(data);
      },
      error: (e) => {
        this.isTblLoading = false;
      }
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
    }
    return throwError(() => new Error(errorMessage));;
  }

  getAllFarmerPrintData(){
    return this.http.get<any[]>(`${environment.apiUrl}/show-farmer-bill/`, {
       
    });
  }

  getFarmerPrintDataById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/show-farmer-bill/` + id +'/');
  }

  getTenantById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/tenant/` ,{params:{id}});
  }

  getFarmerBillDataById(id: any) {
        return this.http.get(`${environment.apiUrl}/show-farmer-bill/`, { params: { id } });
  }

  //Purchasebooking print//
  getPurchaseBookingPrintDataById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/show-purchase_booking/` + id +'/');
  }


  getPurchaseBookingDataById(id: any) {
        return this.http.get(`${environment.apiUrl}/show-purchase_booking/`, { params: { id } });
  }



}
