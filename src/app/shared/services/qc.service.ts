import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class QcService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  errorReport = new Subject<any>();

  constructor(private http: HttpClient) {
    super();
  }
  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/quality-control/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      });
  }

  getAllQcDataByid(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { id },
    });
  }

  createQC(formValue: any, id: any) {
    if (id) {
      // formValue.qc_details.forEach((ele:any, ind:number) => {
      //   if(ele.id==''){
      //     delete formValue.qc_details.at(ind).id;

      //   }
      // });
      return this.http
        .put<any[]>(`${environment.apiUrl}/quality-control/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //     catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/quality-control/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getAllPurchaseData(purchase_order: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase_order/`, {
      params: { purchase_order },
    });
  }

  getFarmerName(Farmer_Name: any, val: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { Farmer_Name, val },
    });
  }

  getRateFromSalesOrder(SalesOrderid: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { SalesOrderid },
    });
  }

  GetPassNumber(
    GetPassNumber: any,
    getPassId: any,
    item_type_id: any,
    grn_id: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { GetPassNumber, getPassId, item_type_id, grn_id },
    });
  }

  getGrnDetails(
    getGrnDetails: any,
    supplier_id: any,
    company_id: any,
    item_type: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { getGrnDetails, supplier_id, company_id, item_type },
    });
  }

  getQualityParameterDetail(getQualityParameterDetail: any, master_value: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { getQualityParameterDetail, master_value },
    });
  }
  getQualityParameterDetailForQC(
    getQualityParameterDetailForQc: any,
    master_value: any,
    transaction_date: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: {
        getQualityParameterDetailForQc,
        master_value,
        transaction_date,
      },
    });
  }

  saveFile(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/fileuploadurl/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  CalculateDeductionRate(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/get-rate_deduction/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/')
    );
  }

  getGrnData(getGrnData: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { getGrnData },
    });
  }

  getDynamicData(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }

  getFilterIData(GatePassFilter: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { GatePassFilter, screen_name, field_name },
    });
  }

  getGRNAndGatePassBySupplier(
    GRNAndGatePassBySupplier: any,
    supplier_ref_id: any,
    item_type_id: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { GRNAndGatePassBySupplier, supplier_ref_id, item_type_id },
    });
  }

  getGRNById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/grn/`, {
      params: { id },
    });
  }

  getGrnAndGatePass(
    GrnAndGatePass: any,
    supplier_ref_id: any,
    item_type_id: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { GrnAndGatePass, supplier_ref_id, item_type_id },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    error = JSON.parse(error);
    console.log(error);
    console.log(error.error);
    console.log(error.status);
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
