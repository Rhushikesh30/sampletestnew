import { TestBed } from '@angular/core/testing';

import { FpcSetupService } from './fpc-setup.service';

describe('FpcSetupService', () => {
  let service: FpcSetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FpcSetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
