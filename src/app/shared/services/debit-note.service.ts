import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class DebitNoteService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDebitNoteData() {
    return this.http.get<any[]>(`${environment.apiUrl}/debit_note/`);
  }
  getDynamicData(screen_name: any, field_name: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }
  postDebitNoteData(debit_note: any) {
    return this.http.post(`${environment.apiUrl}/debit_note/`, debit_note);
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/debit_note/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: () => {
          this.isTblLoading = false;
        },
      });
  }
}
