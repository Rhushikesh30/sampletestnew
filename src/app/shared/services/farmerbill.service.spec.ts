import { TestBed } from '@angular/core/testing';

import { FarmerbillService } from './farmerbill.service';

describe('FarmerbillService', () => {
  let service: FarmerbillService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FarmerbillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
