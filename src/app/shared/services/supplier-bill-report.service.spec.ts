import { TestBed } from '@angular/core/testing';

import { SupplierBillReportService } from './supplier-bill-report.service';

describe('SupplierBillReportService', () => {
  let service: SupplierBillReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SupplierBillReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
