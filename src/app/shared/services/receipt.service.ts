import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class ReceiptService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/receipt/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  createReceipt(formValue: any) {
    const id = formValue['id'];
    if (id) {
      formValue.receipt_details.forEach((ele: any, ind: number) => {
        if (ele.id == '') {
          delete formValue.receipt_details.at(ind).id;
        }
      });
      return this.http
        .put<any[]>(`${environment.apiUrl}/receipt/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/receipt/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    }
  }

  getReceiptDataById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/receipt/`, {
      params: { id },
    });
  }

  GetInvoiceBySupplier(get_invoice_by_customer: any, customer_ref_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/dispatch-note/`, {
      params: { get_invoice_by_customer, customer_ref_id },
    });
  }

  GetSOBySupplier(so_by_supplier_id: any, customer_ref_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {
      params: { so_by_supplier_id, customer_ref_id },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
