import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class GatePassService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }

  getAllGatePassDataByid(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/gate-pass/`, {
      params: { id },
    });
  }

  createGatePass(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/gate-pass/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/gate-pass/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          // catchError(this.errorHandler)
        );
    }
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/gate-pass/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: () => {
          this.isTblLoading = false;
        },
      });
  }

  getAllGatePassPrintData() {
    return this.http.get<any[]>(`${environment.apiUrl}/print-gatepass/`, {});
  }

  getGatePassPrintDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/print-gatepass/` + id + '/'
    );
  }

  getTenantById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/tenant/`, {
      params: { id },
    });
  }

  getAllGrnData(grn: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/grn/`, {});
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
