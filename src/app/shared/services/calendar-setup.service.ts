import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root',
})
export class CalendarSetupService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/calender_view/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getCalenderById(year: any) {
    return this.http
      .get(`${environment.apiUrl}/calender_details_view/`, { params: { year } })
      .pipe(catchError(this.errorHandler));
  }

  createCalendar(formValue: any, id: any, fiscal_year: any) {
    if (id) {
      // const id = formValue.id
      return this.http
        .put<any[]>(
          `${environment.apiUrl}/calendar-setup/${id}/?fiscal_year=${fiscal_year}`,
          formValue
        )
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      // delete formValue['id']
      return this.http
        .post<any[]>(`${environment.apiUrl}/calendar-setup/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
