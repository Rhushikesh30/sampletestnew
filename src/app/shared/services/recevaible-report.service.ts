import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root'
})
export class RecevaibleReportService extends UnsubscribeOnDestroyAdapter{
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }
  
  getAllAdvanceTables() {
    return this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/receivable_reports/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        }
      }
      );
  }


  DownloadReceivableLedgerTable(formValue:any) {
    let ReportType='Receivable'
    return this.http.post<any[]>(`${environment.apiUrl}/download_payable_reports/`, {
      params: {ReportType,formValue },
    });
  }
  
// GetPayableAllDaata() {
//   return this.http.get<any[]>(`${environment.apiUrl}/payable_reports/`);
// }

errorHandler(error: any) {
  let errorMessage = '';
  if (error.error instanceof ErrorEvent) {
    errorMessage = error.error.message;
  } else {
    errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
  }
  return throwError(() => new Error(errorMessage));;
}
}
