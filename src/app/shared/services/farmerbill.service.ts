import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
import { grn } from 'src/app/core/models/grn.model';
import { Dynamicmodel } from 'src/app/core/models/dynamictable.model';


@Injectable({
  providedIn: 'root'
})
export class FarmerbillService extends UnsubscribeOnDestroyAdapter  {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  // Temporarily stores data from dialogs
  dialogData!: any;

  constructor(private http: HttpClient) { 
        // console.log("in service")
        super();
        // this.getAllAdvanceTables();
  }

  get data(): any[] {
    console.log("in service")
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/grn/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        }
      }
      );
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getAllPurchaseData(purchase_order: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase_order/`, {
      params: { purchase_order },
    });
  }

  getAllGrnData(grn:any){
    return this.http.get<any[]>(`${environment.apiUrl}/grn/`, {
       
    });
  }

  getAllQcData(quality_control:any){
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { quality_control },
    });
  }

  getQcDataById(id:any) {
  return this.http.get<any[]>(`${environment.apiUrl}/quality-control/` + id +'/');
  }

  getGrnDataById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/grn/` + id +'/');
    }


  getQcByIdData(supplier_ref_id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/quality-control/`, {
      params: { supplier_ref_id },
    })
  }
  createFarmer(formValue: any) {
    console.log("check for form value ",formValue);
    const id = formValue['id']
    if (id) {
      formValue.fpc_farmer_bill.forEach((ele:any, ind:number) => {
        if(ele.id==''){
          delete formValue.fpc_farmer_bill.at(ind).id;
        }
      });
      return this.http.put<any[]>(`${environment.apiUrl}/fpc-farmer-bill/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
    else {
      delete formValue['id']
      return this.http.post<any[]>(`${environment.apiUrl}/fpc-farmer-bill/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(`${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/'));
  }

  

  getDynamicData(screen_name: any,field_name:any) {
    let get_dynamic_data='get_dynamic_data'
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: {get_dynamic_data,screen_name,field_name },
    });
  }

  getSupplierBillData() {
    return this.http.get(`${environment.apiUrl}/show-farmer-bill/`);
  }

  getFarmerBillData(id:any){
    return this.http.get(`${environment.apiUrl}/print-farmerbill/`+ id +'/');
  }




  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
    }
    return throwError(() => new Error(errorMessage));;
  }


  getAllFarmerPrintData(){
    return this.http.get<any[]>(`${environment.apiUrl}/print_farmerbill/`, {
       
    });
  }

  getFarmerPrintDataById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/print_farmerbill/` + id +'/');
  }

  getTenantById(id:any) {
    return this.http.get<any[]>(`${environment.apiUrl}/tenant/` ,{params:{id}});
  }







}
