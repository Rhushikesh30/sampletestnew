import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
@Injectable({
  providedIn: 'root',
})
export class LotCreationService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(
    private http: HttpClient,
    private encdecservice: EncrDecrService
  ) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllDispatchNote() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/lot-creation/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getSalesOrder(customer_ref_id: any, screen_name: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {
      params: { customer_ref_id, screen_name },
    });
  }

  createLot(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/lot-creation/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            console.log(data['message']);
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/lot-creation/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getSalesOrderData() {
    let getAllSalesOrder = 'getAllSalesOrder';
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {
      params: { getAllSalesOrder },
    });
  }

  getFarmerBySO(so_id: any) {
    let getFarmerBySalesOrder = 'getFarmerBySalesOrder';
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {
      params: { getFarmerBySalesOrder, so_id },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    error = JSON.parse(error);
    console.log(error['error']);
    console.log(this.encdecservice.decryptedData(error['error']['message']));
    if (error.error instanceof ErrorEvent) {
      console.log('if');
      errorMessage = this.encdecservice.decryptedData(
        error['error']['message']
      );
    } else {
      console.log('else');
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
