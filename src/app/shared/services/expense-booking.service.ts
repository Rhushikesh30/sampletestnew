import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class ExpenseBookingService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    // this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }
  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getDynamicDataCombine(
    get_combine_columns: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_combine_columns, screen_name, field_name1, field_name2 },
    });
  }
  getSessionData() {
    const Session_data = 'Session_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/journal_voucher_report/`,
      {
        params: { Session_data },
      }
    );
  }
  getAllAdvanceTables(type: any) {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/expense-booking/`, {
        params: { type },
      })
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getExpenseById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/expense-booking/`, {
      params: { id },
    });
  }

  createExpenseBooking(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/expense-booking/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/expense-booking/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getDynamicData(screen_name: any, field_name: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }
  getAccountingParameters() {
    return this.http.get<any[]>(`${environment.apiUrl}/accounting_parameter/`);
  }

  getAllDynamicData(screenName: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/` + screenName + '/'
    );
  }
  getHsnSacCode(gethsncode: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/expense-booking/`, {
      params: { gethsncode },
    });
  }
  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getTaxRate(hsn_sac_no: any) {
    return this.http.get<any>(`${environment.apiUrl}/get-tax-rate/`, {
      params: { hsn_sac_no },
    });
  }

  UnpostexpenseBooking(formValue: any, id: any) {
    return this.http
      .put<any[]>(`${environment.apiUrl}/expense-booking/${id}/`, formValue)
      .pipe(
        map((data: any) => {
          data['status'] = 1;
          return data;
        })
        //catchError(this.errorHandler)
      );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
  postExpenseBookingData(debit_note: any) {
    return this.http.post(`${environment.apiUrl}/expense-booking/`, debit_note);
  }
}
