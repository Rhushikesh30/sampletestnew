import { TestBed } from '@angular/core/testing';

import { ExpenseBookingService } from './expense-booking.service';

describe('ExpenseBookingService', () => {
  let service: ExpenseBookingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExpenseBookingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
