import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class PurchaseBookingService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/purchase-booking/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getSupplierGRNData(Supplier_GRN: any, grn_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_GRN, grn_id },
    });
  }

  getSupplierIdGRNData(Supplier_id_GRN: any, supplier_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_id_GRN, supplier_id },
    });
  }

  getSupplierGatepassData(Supplier_GatePass: any, grn_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_GatePass, grn_id },
    });
  }

  getSupplierBillNoData(Supplier_Bill_No: any, supplier_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_Bill_No, supplier_id },
    });
  }

  getSupplierPOData(Supplier_PO: any, supplier_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_PO, supplier_id },
    });
  }

  getSupplierQCData(Supplier_QC: any, supplier_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_QC, supplier_id },
    });
  }

  getSupplierAllQCData() {
    return this.http.get(`${environment.apiUrl}/quality-control/`);
  }

  getSupplierSOData(Supplier_SO: any, supplier_id: any) {
    return this.http.get(`${environment.apiUrl}/grn/`, {
      params: { Supplier_SO, supplier_id },
    });
  }

  createPurchaseBooking(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/purchase-booking/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/purchase-booking/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  getSupplierBillById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/fpc-farmer-bill/` + id + '/'
    );
  }

  getPurchaseBookDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/purchase-booking/` + id + '/'
    );
  }

  getQCSupplierData(getQCSupplierData: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase-booking/`, {
      params: { getQCSupplierData },
    });
  }

  UnpostPurchaseBooking(formValue: any, id: any) {
    return this.http
      .put<any[]>(`${environment.apiUrl}/purchase-booking/${id}/`, formValue)
      .pipe(
        map((data: any) => {
          data['status'] = 1;
          return data;
        })
        //catchError(this.errorHandler)
      );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
