import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
@Injectable({
  providedIn: 'root',
})
export class SalesService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(
    private http: HttpClient,
    private encdecservice: EncrDecrService
  ) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllDispatchNote() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/dispatch-note/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllInvoice() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/invoice/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getSalesOrder(customer_ref_id: any, screen_name: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/sales-order/`, {
      params: { customer_ref_id, screen_name },
    });
  }

  getPaymentTermsById(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  getDynamicData(screenName: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/` + screenName + '/'
    );
  }

  getBillingShippingAdrress(screen_name: any, parent_id: any) {
    let get_address_data = 'get_address_data';
    return this.http.get(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      { params: { get_address_data, screen_name, parent_id } }
    );
  }

  getDynamicDataById(screen_name: any, field_name: any) {
    let get_dynamic_data = 'get_dynamic_data';
    return this.http.get<any[]>(
      `${environment.apiUrl}/dynamic-screen-wrapper/`,
      {
        params: { get_dynamic_data, screen_name, field_name },
      }
    );
  }

  getAvailableQty(item_ref_id: any, so_type: any, so_ref_id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/inventory-available-stock/`,
      { params: { item_ref_id, so_type, so_ref_id } }
    );
  }

  getTaxRate(hsn_sac_no: any) {
    return this.http.get<any>(`${environment.apiUrl}/get-tax-rate/`, {
      params: { hsn_sac_no },
    });
  }

  getSalesOrderById(id: number) {
    return this.http.get<any>(`${environment.apiUrl}/sales-order/` + id + '/');
  }

  getAllocationData(dispatch_note_id: any) {
    return this.http.get<any>(`${environment.apiUrl}/allocation/`, {
      params: { dispatch_note_id },
    });
  }

  getDisaptchFromSO(so_id: any) {
    return this.http.get<any>(`${environment.apiUrl}/dispatch-note/`, {
      params: { so_id },
    });
  }

  getAllDispatchData() {
    return this.http.get<any>(`${environment.apiUrl}/dispatch-note/`);
  }

  getFpcCommission(item_ref_id: any) {
    return this.http.get<any>(`${environment.apiUrl}/get-fpc-commission/`, {
      params: { item_ref_id },
    });
  }

  getCustomerFarmerFederation(
    get_combine_columns_customer_farmer_federation: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: {
        get_combine_columns_customer_farmer_federation,
        screen_name,
        field_name1,
        field_name2,
      },
    });
  }

  getStatus(payload: any) {
    return this.http.post<any>(
      `${environment.apiUrl}/dispatch_quality_check/`,
      payload
    );
  }

  getQualityParameterInSO(item_ref_id: any, so_ref_id: any) {
    return this.http.get<any>(`${environment.apiUrl}/so_quality_data/`, {
      params: { item_ref_id, so_ref_id },
    });
  }

  getCustomerFarmer(
    get_combine_columns_customer_farmer: any,
    screen_name: any,
    table_name: any,
    field_name1: any,
    field_name2: any,
    val: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: {
        get_combine_columns_customer_farmer,
        screen_name,
        table_name,
        field_name1,
        field_name2,
        val,
      },
    });
  }

  getFarmerBillCount(farmerBillExists: any, item_ref_id: any) {
    return this.http.get(`${environment.apiUrl}/fpc-farmer-bill/`, {
      params: { farmerBillExists, item_ref_id },
    });
  }

  createDispatchNote(formValue: any, id: any) {
    if (id) {
      // formValue.dispatch_note_details.forEach((ele: any, ind: number) => {
      //   if (ele.id == '') {
      //     delete formValue.dispatch_note_details.at(ind).id;
      //   }
      // });
      return this.http
        .put<any[]>(`${environment.apiUrl}/dispatch-note/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            console.log(data['message']);
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      // delete formValue['id']
      // formValue.dispatch_note_details.forEach((ele: any, ind: number) => {
      //   if (ele.id == '') {
      //     delete formValue.dispatch_note_details.at(ind).id;
      //   }
      // });
      return this.http
        .post<any[]>(`${environment.apiUrl}/dispatch-note/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  createInvoice(formValue: any, id: any) {
    if (id) {
      // formValue.invoice_details.forEach((ele: any, ind: number) => {
      //   if (ele.id == '') {
      //     delete formValue.invoice_details.at(ind).id;
      //   }
      // });
      return this.http
        .put<any[]>(`${environment.apiUrl}/invoice/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      // delete formValue['id']
      // formValue.invoice_details.forEach((ele: any, ind: number) => {
      //   if (ele.id == '') {
      //     delete formValue.invoice_details.at(ind).id;
      //   }
      // });
      return this.http
        .post<any[]>(`${environment.apiUrl}/invoice/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    }
  }

  saveFile(formValue: any) {
    console.log('formValue: ', formValue);
    return this.http
      .post(`${environment.apiUrl}/fileuploadurl/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  getsalesPrintDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/print-sales-order/` + id + '/'
    );
  }

  getFPCTaxRate(
    customer_ref_id: any,
    fpc_commision_amt: any,
    customer_bill_from: any,
    customer_ship_to: any
  ) {
    return this.http.get<any[]>(`${environment.apiUrl}/fpc-tax-rate/`, {
      params: {
        customer_ref_id,
        fpc_commision_amt,
        customer_bill_from,
        customer_ship_to,
      },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    error = JSON.parse(error);
    console.log(error['error']);
    console.log(this.encdecservice.decryptedData(error['error']['message']));
    if (error.error instanceof ErrorEvent) {
      console.log('if');
      errorMessage = this.encdecservice.decryptedData(
        error['error']['message']
      );
    } else {
      console.log('else');
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
