import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Dynamicmodel } from 'src/app/core/models/dynamictable.model';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
@Injectable({
  providedIn: 'root',
})
export class FinanceService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  get data(): any[] {
    return this.dataChange.value;
  }

  constructor(private http: HttpClient) {
    super();
  }

  getDynamicScreenData(screen_name: string) {
    return this.http.get<any>(
      `${environment.apiUrl}`.concat(
        '/dynamic-screen-wrapper/' + screen_name + '/'
      )
    );
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<Dynamicmodel[]>(`${environment.apiUrl}/tenant/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },

        error: (e) => {
          this.isTblLoading = false;
        },
      });
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/')
    );
  }

  createScreenData(formValue: any) {
    if (formValue.id) {
      const id = formValue.id;
      return this.http
        .put<any[]>(
          `${environment.apiUrl}/dynamic-screen-wrapper/${id}/`,
          formValue
        )
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //    catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/dynamic-screen-wrapper/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
