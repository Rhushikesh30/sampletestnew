import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class GrnService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/grn/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: () => {
          this.isTblLoading = false;
        },
      });
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getAllPurchaseData(purchase_order: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/purchase_order/`, {
      params: { purchase_order },
    });
  }

  saveFile(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/fileuploadurl/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  createGrn(formValue: any, id: any) {
    if (id) {
      // formValue.grn_details.forEach((ele:any, ind:number) => {
      //   if(ele.id==''){
      //     delete formValue.grn_details.at(ind).id;
      //   }
      // });
      return this.http
        .put<any[]>(`${environment.apiUrl}/grn/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    } else {
      // delete formValue['id']
      return this.http
        .post<any[]>(`${environment.apiUrl}/grn/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //catchError(this.errorHandler)
        );
    }
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/')
    );
  }

  getDynamicData(screen_name: any, field_name: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name },
    });
  }
  getItemDynamicData(screen_name: any, field_name: any, field_name1: any) {
    const get_dynamic_data = 'get_dynamic_data';
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_dynamic_data, screen_name, field_name, field_name1 },
    });
  }
  getAgentDynamicData(Get_agnet_only: any, screen_name: any, field_name: any) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { Get_agnet_only, screen_name, field_name },
    });
  }
  getDynamicDataCombine(
    get_combine_columns: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_combine_columns, screen_name, field_name1, field_name2 },
    });
  }

  getSupplierFarmerList(
    get_combine_columns_supplier_farmer: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: {
        get_combine_columns_supplier_farmer,
        screen_name,
        field_name1,
        field_name2,
      },
    });
  }

  getFpcPodetails(purchase_order: any) {
    return this.http.post(
      `${environment.apiUrl}/get_billing_shipping_adrress/`,
      purchase_order
    );
  }

  getAllGrnData(grn: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/grn/`, {});
  }
  getItemData() {
    return this.http.get<any[]>(`${environment.apiUrl}/get-nonfarm/`, {});
  }

  getAllGatepassData(gatepass: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/gate-pass/`, {});
  }
  getGetPassNumber(GetPassNumberForNonFarm: any, grnGetPassId: any) {
    return this.http.get<any>(`${environment.apiUrl}/grn/`, {
      params: { GetPassNumberForNonFarm, grnGetPassId },
    });
  }

  getgatepassDataById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/gate-pass/` + id + '/');
  }
  getGatePassDetails(getGatePassDetails: any) {
    return this.http.get<any>(`${environment.apiUrl}/grn/`, {
      params: { getGatePassDetails },
    });
  }

  getItemTypeAndItem(GetItemMaster: any) {
    return this.http.get<any>(`${environment.apiUrl}/gate-pass/`, {
      params: { GetItemMaster },
    });
  }

  errorHandler(error: any) {
    let errorMessage = '';
    error = JSON.parse(error);
    console.log(error);
    console.log(error['error']);
    console.log(error.error.message);
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }

  //Print API'S//
  getTenantById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/tenant/`, {
      params: { id },
    });
  }

  getInvoicePrintDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/print-invoice/` + id + '/'
    );
  }

  getInvoicePrintDataByIdParams(id: any) {
    return this.http.get(`${environment.apiUrl}/print-invoice/`, {
      params: { id },
    });
  }

  getAllGRNPrintData() {
    return this.http.get<any[]>(`${environment.apiUrl}/print-grn/`, {});
  }

  getGRNPrintDataById(id: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/print-grn/` + id + '/');
  }

  getSupplierData(
    get_Supplier: any,
    screen_name: any,
    field_name1: any,
    field_name2: any
  ) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { get_Supplier, screen_name, field_name1, field_name2 },
    });
  }
}
