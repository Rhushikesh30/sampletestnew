import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { environment } from "src/environments/environment";
import { Federation } from 'src/app/core/models/federation.model';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root'
})
export class FederationService extends UnsubscribeOnDestroyAdapter{
  isTblLoading = true;
  dataChange: BehaviorSubject<Federation[]> = new BehaviorSubject<
  Federation[]
  >([]);
  dialogData!: Federation;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): Federation[] {
    return this.dataChange.value;
  }

  getProfileData(profile:string){
    return this.http.get(`${environment.apiUrl}/tenant/`,{params:{profile}});
  }

  getAllMasterData(master_type: string) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getDynamicScreenData(screen_name:string) {
    return this.http.get<any>(`${environment.apiUrl}`.concat('/dynamic-screen-wrapper/'+ screen_name + '/')); 
   }

  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getStatesData(country_id: any) {
    return this.http.get(`${environment.apiUrl}/state/`,{params:{country_id},});
  }
  
  getDistrictData(state_id: any) {
    return this.http.get(`${environment.apiUrl}/district/`,{params:{state_id},});
  }

  getTalukaData(district_id: any) {
    return this.http.get(`${environment.apiUrl}/taluka/`,{params:{district_id},});
  }

  getAllempusrData(employee_user: string) {
    return this.http.get<any[]>(`${environment.apiUrl}/employeeuser/`, {
      params: { employee_user },
    });
  }

  getTenantById(id: any) {
    return this.http.get(`${environment.apiUrl}/tenant/`, { params: { id } }).pipe(catchError(this.errorHandler));
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<Federation[]>(`${environment.apiUrl}/tenant/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        
        error: (e) => {
          this.isTblLoading = false;
        }
      }
      );
  }
 
  saveFile(formValue: any) {
    return this.http.post(`${environment.apiUrl}/fileuploadurl/`, formValue).pipe(map((data) => {
      return data;
    }),
      catchError(this.errorHandler));
  }

  createFederation(formValue: any, id:any) {
    if (id) {
      // formValue.contact_details.forEach((ele:any)=>{
      //   if(ele.id==''){
      //     delete ele.id;
      //   }
      //   if(ele.full_name==''){
      //     formValue.contact_details = [];
      //   }
      // })
      return this.http.put<any[]>(`${environment.apiUrl}/tenant/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
    else {
      // delete formValue['id']
      // formValue.contact_details.forEach((ele:any)=>{
      //   if(ele.full_name==''){
      //     formValue.contact_details = [];
      //   }
      // })
      return this.http.post<any[]>(`${environment.apiUrl}/tenant/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          }),
          catchError(this.errorHandler)
        )
    }
  }
  
  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(error.message)}`;
    }
    return throwError(() => new Error(errorMessage));;
  }

}
