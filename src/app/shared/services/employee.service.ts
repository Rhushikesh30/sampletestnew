import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';
import { Employee } from 'src/app/core/models/employee.model';
import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  // Temporarily stores data from dialogs
  dialogData!: any;
  constructor(private http: HttpClient) {
    console.log('in service');
    super();
    // this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables() {
    return (this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/employee/`)
      .subscribe({
        next: (data: any) => {
          console.log(data);
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e: any) => {
          this.isTblLoading = false;
        },
      }));
  }

  getEmployeeById(id: any) {
    return this.http
      .get(`${environment.apiUrl}/employee/`, { params: { id } })
      .pipe(catchError(this.errorHandler));
  }

  getDynamicData(formname: string) {
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { formname },
    });
  }

  createEmployee(formValue: any, id: any) {
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/employee/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/employee/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          // catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
