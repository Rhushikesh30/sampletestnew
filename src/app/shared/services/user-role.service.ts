import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';
import { environment } from 'src/environments/environment.development';
@Injectable({
  providedIn: 'root',
})
export class UserRoleService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
  }
  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllUserRoleLinkData() {
    return (this.subs.sink = this.http
      .get(`${environment.apiUrl}/user-role-link/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getAllRoleData(all: any) {
    return (this.subs.sink = this.http
      .get(`${environment.apiUrl}/role/`, { params: { all } })
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      }));
  }

  getUserFormData(user_id: any) {
    return this.http.get(`${environment.apiUrl}/user-role-link/`, {
      params: { user_id },
    });
  }
  getUserRoleData(user_id: any) {
    return this.http.get(`${environment.apiUrl}/role/`, {
      params: { user_id },
    });
  }

  getUserData(except: any) {
    return this.http.get(`${environment.apiUrl}/employee/`, {
      params: { except },
    });
  }

  getRoleData(all: any) {
    return this.http.get(`${environment.apiUrl}/role/`, { params: { all } });
  }

  getUserAllData() {
    return this.http.get(`${environment.apiUrl}/employee/`);
  }

  getTenantByRole(role_id: any) {
    return this.http.get(`${environment.apiUrl}/tenant-by-role/`, {
      params: { role_id },
    });
  }

  createUserRoleLink(formValue: any) {
    formValue.forEach((ele: any, ind: number) => {
      if (ele.id == '') {
        delete formValue.at(ind).id;
      }
    });
    return this.http
      .post(`${environment.apiUrl}/user-role-link/`, formValue)
      .pipe(
        map((data: any) => {
          return data;
        })
        // catchError(this.errorHandler)
      );
  }

  createRole(formValue: any) {
    if (formValue.id == '') {
      delete formValue.id;
      return this.http.post(`${environment.apiUrl}/role/`, formValue).pipe(
        map((data: any) => {
          data['status'] = 2;
          return data;
        })
        // catchError(this.errorHandler)
      );
    } else {
      return this.http
        .put(`${environment.apiUrl}/role/${formValue.id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //  catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(error);
  }
}
