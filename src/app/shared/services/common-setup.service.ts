import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Dynamicmodel } from 'src/app/core/models/dynamictable.model';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class CommonSetupService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;

  get data(): any[] {
    return this.dataChange.value;
  }

  constructor(private http: HttpClient) {
    super();
  }

  getDynamicScreenData(screen_name: string) {
    return this.http.get<any>(
      `${environment.apiUrl}`.concat(
        '/dynamic-screen-wrapper/' + screen_name + '/'
      )
    );
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<Dynamicmodel[]>(`${environment.apiUrl}/tenant/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },

        error: (e) => {
          this.isTblLoading = false;
        },
      });
  }

  getDynamicScreenFormData(screen_name: string) {
    return this.http.get(
      `${environment.apiUrl}`.concat('/dynamic-screen/' + screen_name + '/')
    );
  }

  createScreenData(formValue: any) {
    if (formValue.create_version) {
      return this.http
        .post<any[]>(`${environment.apiUrl}/dynamic-screen-wrapper/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 3;
            return data;
          })
        );
    } else if (formValue.id) {
      const id = formValue.id;
      return this.http
        .put<any[]>(
          `${environment.apiUrl}/dynamic-screen-wrapper/${id}/`,
          formValue
        )
        .pipe(
          map((data: any) => {
            console.log('adta', data);

            data['status'] = 1;
            return data;
          })
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/dynamic-screen-wrapper/`, formValue)
        .pipe(
          map((data: any) => {
            console.log('adta in post', data);

            data['status'] = 2;
            return data;
          })
          //     catchError(this.errorHandler)
        );
    }
  }

  CheckVersionExist(screen_name: any, id: any) {
    let CheckVersionExist = 'CheckVersionExist';
    return this.http.get(`${environment.apiUrl}/dynamic-screen-wrapper/`, {
      params: { CheckVersionExist, screen_name, id },
    });
  }

  bulkUploadFile(file: any) {
    var fd = new FormData();
    fd.append('file', file);
    return this.http
      .post(`${environment.apiUrl}/bulk-upload/`, fd)
      .pipe(map((response: any) => response))
      .pipe(
        map((data) => {
          data['status'] = 2;
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
