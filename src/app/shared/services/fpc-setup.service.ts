import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class FpcSetupService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllAdvanceTables(): void {
    this.subs.sink = this.http
      .get<any[]>(`${environment.apiUrl}/tenant/`)
      .subscribe({
        next: (data: any) => {
          this.isTblLoading = false;
          this.dataChange.next(data);
        },
        error: (e) => {
          this.isTblLoading = false;
        },
      });
  }

  getTenantById(id: any) {
    return this.http
      .get(`${environment.apiUrl}/tenant/`, { params: { id } })
      .pipe(catchError(this.errorHandler));
  }

  getAllMasterData(master_type: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/master/`, {
      params: { master_type },
    });
  }

  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  getProcurementCenterData() {
    return this.http.get(`${environment.apiUrl}/procurementcenter/`);
  }

  getStatesData(country_id: any) {
    return this.http.get(`${environment.apiUrl}/state/`, {
      params: { country_id },
    });
  }

  getDistrictData(state_id: any) {
    return this.http.get(`${environment.apiUrl}/district/`, {
      params: { state_id },
    });
  }

  getTalukaData(district_id: any) {
    return this.http.get(`${environment.apiUrl}/taluka/`, {
      params: { district_id },
    });
  }

  saveFile(formValue: any) {
    return this.http
      .post(`${environment.apiUrl}/fileuploadurl/`, formValue)
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.errorHandler)
      );
  }

  createTenantfpc(formValue: any, id: any) {
    if (id) {
      // for (var i = 0; i < formValue.kyc_details.length; i++) {
      //   if (formValue.kyc_details[i].id == '') {
      //     delete formValue.kyc_details[i].id;
      //   }
      //   if (formValue.kyc_details[i].attachment_path == '' && formValue.kyc_details[i].kyc_doc_id == '') {
      //     delete formValue.kyc_details[i];
      //   }
      // }
      // for (var i = 0; i < formValue.contact_details.length; i++) {
      //   if (formValue.contact_details[i].id == '') {
      //     delete formValue.contact_details[i].id;
      //   }
      //   if (formValue.contact_details[i].full_name == '' || formValue.contact_details[i].full_name == null) {
      //     formValue.contact_details = []
      //   }
      // }
      // for (var i = 0; i < formValue.contract_details.length; i++) {
      //   if (formValue.contract_details[i].id == '') {
      //     delete formValue.contract_details[i].id;
      //   }
      //   if (formValue.contract_details[i].from_date == '' || formValue.contract_details[i].from_date == null) {
      //     formValue.contract_details = []
      //   }
      // }
      return this.http
        .put<any[]>(`${environment.apiUrl}/tenant/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //catchError(this.errorHandler)
        );
    } else {
      // for (var i = 0; i < formValue.kyc_details.length; i++) {
      //   if (formValue.kyc_details[i].id == '') {
      //     delete formValue.kyc_details[i].id;
      //   }
      //   if (formValue.kyc_details[i].attachment_path == '' && formValue.kyc_details[i].kyc_doc_id == '') {
      //     delete formValue.kyc_details[i];
      //   }
      // }
      // for (var i = 0; i < formValue.contact_details.length; i++) {
      //   if (formValue.contact_details[i].id == '') {
      //     delete formValue.contact_details[i].id;
      //   }
      //   if (formValue.contact_details[i].full_name == '' || formValue.contact_details[i].full_name == null) {
      //     formValue.contact_details = []
      //   }
      // }
      // for (var i = 0; i < formValue.contract_details.length; i++) {
      //   if (formValue.contract_details[i].id == '') {
      //     delete formValue.contract_details[i].id;
      //   }
      //   if (formValue.contract_details[i].from_date == '' || formValue.contract_details[i].from_date == null) {
      //     formValue.contract_details = []
      //   }
      // }
      // delete formValue['id']
      return this.http
        .post<any[]>(`${environment.apiUrl}/tenant/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //    catchError(this.errorHandler)
        );
    }
  }

  createTenant(formValue: any) {
    const id = formValue['id'];
    if (id) {
      return this.http
        .put<any[]>(`${environment.apiUrl}/tenant/${id}/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    } else {
      delete formValue['id'];
      return this.http
        .post<any[]>(`${environment.apiUrl}/tenant/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //   catchError(this.errorHandler)
        );
    }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
