import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class JournalVoucherService extends UnsubscribeOnDestroyAdapter {
  isTblLoading = true;
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  dialogData!: any;
  constructor(private http: HttpClient) {
    super();
    this.getAllAdvanceTables();
  }

  get data(): any[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  // getAllAdvanceTables() {
  //   return (this.subs.sink = this.http
  //     .get<any[]>(`${environment.apiUrl}/journal_voucher_report/`)
  //     .subscribe({
  //       next: (data: any) => {
  //         this.isTblLoading = false;
  //         this.dataChange.next(data);
  //         return data; // Return the data
  //       },
  //       error: (e) => {
  //         this.isTblLoading = false;

  //       },
  //     }));
  // }
  getAllAdvanceTables(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.subs.sink = this.http
        .get<any[]>(`${environment.apiUrl}/journal_voucher_report/`)
        .subscribe({
          next: (data: any) => {
            this.isTblLoading = false;
            this.dataChange.next(data);
            resolve(data);
          },
          error: (e) => {
            this.isTblLoading = false;
            console.error('Error fetching data:', e);
            reject(e);
          },
        });
    });
  }

  GetJVAllDaataWithPagination(page_number: number, page_limit: number) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/journal_voucher_report/`,
      {
        params: { page_number, page_limit },
      }
    );
  }

  getJVDataById(id: any) {
    return this.http.get<any[]>(
      `${environment.apiUrl}/journal_voucher_report/`,
      {
        params: { id },
      }
    );
  }

  getCurrencyData() {
    return this.http.get(`${environment.apiUrl}/countrycurrency/`);
  }

  DownloadJournalVoucherTable(formValue: any) {
    return this.http.post<any[]>(
      `${environment.apiUrl}/download_journal_voucher_reports/`,
      {
        params: { formValue },
      }
    );
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }
}
