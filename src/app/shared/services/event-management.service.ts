import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, throwError } from 'rxjs';

import { environment } from 'src/environments/environment.development';
import { UnsubscribeOnDestroyAdapter } from '../UnsubscribeOnDestroyAdapter';

@Injectable({
  providedIn: 'root',
})
export class EventManagementService {
  constructor(private http: HttpClient) {}
  createEvent(formValue: any) {
    if (formValue.id) {
      return this.http
        .put<any[]>(
          `${environment.apiUrl}/event-management/${formValue.id}/`,
          formValue
        )
        .pipe(
          map((data: any) => {
            data['status'] = 1;
            return data;
          })
          // catchError(this.errorHandler)
        );
    } else {
      return this.http
        .post<any[]>(`${environment.apiUrl}/event-management/`, formValue)
        .pipe(
          map((data: any) => {
            data['status'] = 2;
            return data;
          })
          //    catchError(this.errorHandler)
        );
    }
  }

  getAllEventData() {
    return this.http.get<any[]>(`${environment.apiUrl}/event-management/`, {});
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${JSON.stringify(
        error.message
      )}`;
    }
    return throwError(() => new Error(errorMessage));
  }

  deleteEvent(ID: any) {
    return this.http.delete(
      `${environment.apiUrl}`.concat('/event-management/' + ID + '/')
    );
  }
}
