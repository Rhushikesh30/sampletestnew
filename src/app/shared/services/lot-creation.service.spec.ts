import { TestBed } from '@angular/core/testing';

import { LotCreationService } from './lot-creation.service';

describe('LotCreationService', () => {
  let service: LotCreationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LotCreationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
