import { TestBed } from '@angular/core/testing';

import { GatePassPrintService } from './gate-pass-print.service';

describe('GatePassPrintService', () => {
  let service: GatePassPrintService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GatePassPrintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
