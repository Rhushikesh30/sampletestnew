import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-screen-header',
  templateUrl: './screen-header.component.html',
  styleUrls: ['./screen-header.component.scss']
})
export class ScreenHeaderComponent implements OnInit {
  @Input() screenName!: string;
  @Input() showList!: boolean;
  @Input() sidebarData: any;
  @Output() showFormList = new EventEmitter<boolean>();

  hideadd=true
  
  constructor() { }

  ngOnInit(): void {
   
    if(this.showList === true)
      this.showListTable();
    else
      this.showFrom();

    let role: any = localStorage.getItem('roles');
    let roles = JSON.parse(role);
    let rolename = roles.map((a:any) => a.role_name)
    rolename = rolename[0]
  }
  
  showFrom()
  {
    this.showList = false;
    this.showFormList.emit(this.showList);
  }

	showListTable()
  {
    this.showList = true;
    this.showFormList.emit(this.showList);
  }

}
