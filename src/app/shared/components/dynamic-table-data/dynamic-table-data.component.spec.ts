import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicTableDataComponent } from './dynamic-table-data.component';

describe('DynamicTableDataComponent', () => {
  let component: DynamicTableDataComponent;
  let fixture: ComponentFixture<DynamicTableDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicTableDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DynamicTableDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
