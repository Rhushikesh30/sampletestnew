import { Component, Input } from '@angular/core';
import { STATUS } from 'src/app/constants/constants';

@Component({
  selector: 'app-dynamic-table-data',
  templateUrl: './dynamic-table-data.component.html',
  styleUrls: ['./dynamic-table-data.component.scss'],
})
export class DynamicTableDataComponent {
  @Input() data: any;

  getStatusColor(status: string): string {
    console.log('status: ', status);
    switch (status.toLowerCase()) {
      case STATUS.APPROVED:
        return '#5CBE63';
      case STATUS.ACTIVE:
        return '#5CBE63';
      case STATUS.REJECTED:
        return '#E35140';
      case STATUS.INACTIVE:
        return '#E35140';
      case STATUS.PENDING:
        return '#FFE900';
      default:
        return 'gray';
    }
  }
}
