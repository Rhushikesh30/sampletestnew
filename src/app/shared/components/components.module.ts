import { NgModule } from '@angular/core';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { SharedModule } from '../shared.module';
import { ScreenHeaderComponent } from './screen-header/screen-header.component';
import { DynamicTableDataComponent } from './dynamic-table-data/dynamic-table-data.component';

@NgModule({
  declarations: [
    FileUploadComponent,
    ScreenHeaderComponent,
    DynamicTableDataComponent,
  ],
  imports: [SharedModule],
  exports: [
    FileUploadComponent,
    ScreenHeaderComponent,
    DynamicTableDataComponent,
  ],
})
export class ComponentsModule {}
