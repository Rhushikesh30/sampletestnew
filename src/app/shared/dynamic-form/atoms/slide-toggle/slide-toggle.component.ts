import { Component,OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-slide-toggle',
  templateUrl: './slide-toggle.component.html',
  styleUrls: ['./slide-toggle.component.scss']
})
export class SlideToggleComponent implements OnInit {

  constructor() { }

  @Input() field:any = {};
  @Input() form!:FormGroup;
  @Output() change = new EventEmitter<any>();
  @Input() isOnlyMaster:any
  ngOnInit(): void {
  }

  clickEvent(event:any, val:any){
    if(val=='is_rate_percentage' && event.checked){
      this.change.emit('Percentage');
    }
    else if(val=='is_rate_percentage' && !event.checked){
      this.change.emit('Rate');
    }
    if(val=='is_tds_applicable' && event.checked){
      this.change.emit('Yes');
    }
    else if(val=='is_tds_applicable' && !event.checked){
      this.change.emit('No');
    }
  }
 
}
