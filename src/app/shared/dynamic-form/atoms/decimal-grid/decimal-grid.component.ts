import { Component,OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-decimal-grid',
  templateUrl: './decimal-grid.component.html',
  styleUrls: ['./decimal-grid.component.scss']
})
export class DecimalGridComponent implements OnInit {
  constructor() { }

  @Input() field:any = {};
  @Input() form!:FormGroup;

  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }
  ngOnInit(): void {
  }
  
  

  cultivationchangeHector(event:any) {
    const cultivation_land_in_hector_r =   event.target.value
    const cultivation_land_in_acreage = parseFloat(cultivation_land_in_hector_r) * 2.47105
    this.form.get('cultivation_land_in_acreage')?.setValue(cultivation_land_in_acreage.toFixed(5))
  }

  totalcultivationchangeHector(event:any){

    const cultivation_land_in_hector =this.form.get('total_land_in_hectare_are')?.value
    const cultivation_land_in_hector_r =   event.target.value
    const cultivation_land_in_acreage = parseFloat(cultivation_land_in_hector_r) * 2.47105
    this.form.get('total_cultivation_land_in_acreage')?.setValue(cultivation_land_in_acreage.toFixed(5))

  }
}

