import { Component,OnInit, Input } from '@angular/core';
import { FormGroup ,FormControl } from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {MatDatepicker} from '@angular/material/datepicker';

import * as _moment from 'moment';
import { Moment} from 'moment';

const moment =  _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-grid-datepicker',
  templateUrl: './grid-datepicker.component.html',
  styleUrls: ['./grid-datepicker.component.scss']
})
export class GridDatepickerComponent implements OnInit {

  constructor() { }

  @Input() field:any = {};
  @Input() form!:FormGroup;
  todayDateMin = new Date().toJSON().split('T')[0];
  todayDate = new Date();
  newDate=new Date()
  today: any;

  ngOnInit(): void {
  

  }

 
 

  countage(value:any) {

    let birthdate = this.form.value.dob
    this.today = new Date();
    let timediff = Math.abs(this.today - birthdate);
    let Age = Math.floor((timediff / (1000 * 3600 * 24)) / 365);
    this.form.get("age")?.setValue(Age)
  }
}
