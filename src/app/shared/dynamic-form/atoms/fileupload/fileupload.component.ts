import { Component, ElementRef, Input, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { environment } from 'src/environments/environment.development';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.scss'],
  providers: [ErrorCodes],
})
export class FileuploadComponent implements OnInit {
  constructor(
    private errorCodes: ErrorCodes,
    private dynamicservice: DynamicFormService
  ) {}

  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;

  @Input() field: any = {};
  @Input() form!: FormGroup;
  @Input()
  submitBtn!: boolean;
  @Input()
  previewImage1: any;
  @Input() previewImageType1: any;
  fileName = '';
  uploadStatus = '';
  showFileLoader = false;
  Fileplaceholder = 'Choose';
  lrAttachmentUrl!: string;

  ngOnInit(): void {}

  setFileName(event: any) {
    var file = event.target.files[0];
  }

  removeImage() {
    this.fileName = '';
    // Optionally, you can also clear the input field
    const fileInput = this.fileInput.nativeElement;
    if (fileInput) {
      fileInput.value = '';
    }
    this.previewImage1 = '';
    this.previewImageType1 = false;
    let gridRow = (<FormArray>this.form.get('master_data')).at(0);

    gridRow.get(this.field['field_key'])?.setValue('');
    this.Fileplaceholder = 'Choose';
  }
  onClickFileUpload() {}

  setAttachment(event: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';

    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage1 = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType1 = true;
      }
      this.fileName = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        let formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');

        this.showFileLoader = true;
        this.uploadStatus = '';
        this.dynamicservice.saveFile(formData1).subscribe({
          next: (data: any) => {
            console.log(data);

            this.showFileLoader = false;
            let files3path = data['s3_file_path'];
            let gridRow = (<FormArray>this.form.get('master_data')).at(0);
            this.previewImage1 =
              environment.endpoint_url_cdn + '/' + data['s3_file_path'];
            this.lrAttachmentUrl =
              environment.endpoint_url_cdn + '/' + data['s3_file_path'];
            gridRow.get(this.field['field_key'])?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            this.Fileplaceholder = 'Change';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e: any) => {
            this.uploadStatus = '';
            this.fileName = '';
            this.showFileLoader = false;
            this.Fileplaceholder = 'Choose';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileName = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      this.showSwalmessage('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }
  openImage(path: any) {
    window.open(this.previewImage1, '_blank');
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 3000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 3000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
