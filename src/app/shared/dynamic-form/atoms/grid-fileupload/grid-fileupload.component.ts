import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { environment } from 'src/environments/environment.development';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-grid-fileupload',
  templateUrl: './grid-fileupload.component.html',
  styleUrls: ['./grid-fileupload.component.scss'],
  providers: [ErrorCodes],
})
export class GridFileuploadComponent implements OnInit {
  constructor(
    private errorCodes: ErrorCodes,
    private dynamicservice: DynamicFormService
  ) {}

  @Input() field: any = {};
  @Input() form!: FormGroup;
  @Input()
  submitBtn!: boolean;
  @Input()
  previewImage!: any;
  @Input() i: any;
  @Input() key!: any;
  previewImageType3 = false;
  previewImage1: any;
  fileName = '';
  uploadStatus3 = '';
  showFileLoader3 = false;
  fileNameKYC3: any = [];
  Attachmentfilespath: any = [];
  fileattachment!: FormData;
  fileNameAttachment: any = [];
  lrAttachmentUrl!: string;
  Fileplaceholder = 'Choose';
  ngOnInit(): void {}

  setFileName(event: any) {
    var file = event.target.files[0];
  }

  onClickFileUpload() {}

  removeinspectionImage(i: any, ind: any, name: any) {
    this.previewImage.splice(ind, 1);
    this.Fileplaceholder = 'Choose';
    this.Attachmentfilespath.forEach((ele: any) => {
      if (ele.id == i) {
        ele.path_arr.forEach((e: any, j: any) => {
          if (e == name) {
            ele.path_arr.splice(j, 1);
          }
        });
      }
    });
  }

  setAttachmentFile(event: any, i: any) {
    this.previewImageType3 = false;
    this.previewImage = [];
    let exists = this.Attachmentfilespath.filter((x: any) => x.id == i);
    if (exists.length == 0) {
      this.Attachmentfilespath.push({ id: i, path_arr: [] });
    }

    for (let fu = 0; fu < event.target.files.length; fu++) {
      let fileToUpload = event.target.files[fu];

      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage.push({ id: i, image: reader.result });
      };
      if (fileToUpload.size <= 5 * 1024 * 1024) {
        let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
        if (
          fileFormat == 'png' ||
          fileFormat == 'jpg' ||
          fileFormat == 'jpeg'
        ) {
          this.previewImageType3 = true;
        }
        this.fileName = fileToUpload['name'];
        this.fileNameAttachment.push({ id: i, image: fileToUpload['name'] });
        console.log(this.previewImage, this.key, this.i);

        let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];
        if (compare_file_type.includes(fileFormat)) {
          let formData3 = new FormData();
          formData3.append('uploadedFile', fileToUpload);
          formData3.append('folder_name', 'fpcuploads');
          this.fileattachment = formData3;
          this.showFileLoader3 = true;
          this.uploadStatus3 = '';
          this.Fileplaceholder = 'Change';

          this.dynamicservice.saveFile(this.fileattachment).subscribe({
            next: (data: any) => {
              this.showFileLoader3 = false;
              this.Attachmentfilespath.forEach((ele: any) => {
                if (ele.id == i) {
                  this.previewImage =
                    environment.endpoint_url_cdn + '/' + data['s3_file_path'];
                  this.lrAttachmentUrl =
                    environment.endpoint_url_cdn + '/' + data['s3_file_path'];

                  ele.path_arr.push(data['s3_file_path']);
                  this.form
                    .get(this.field['field_key'])
                    ?.setValue(data['s3_file_path']);
                }
              });

              if (fu == event.target.files.length - 1) {
                this.uploadStatus3 = 'Uploaded';
                Swal.fire('File Uploaded Successfully!');
              }
            },
            error: (e) => {
              this.uploadStatus3 = '';
              this.fileNameAttachment = [];
              this.showFileLoader3 = false;
              this.Fileplaceholder = 'Choose';
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
        } else {
          this.uploadStatus3 = '';
          this.fileNameAttachment = [];
          Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
        }
      } else {
        this.uploadStatus3 = '';
        this.fileNameAttachment = [];
        this.showSwalmessage(
          'File Size Not More Than 5 MB',
          '',
          'error',
          false
        );
      }
    }
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }
  openImage(path: any, i: any) {
    window.open(this.previewImage[this.key][i]['image'], '_blank');
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
