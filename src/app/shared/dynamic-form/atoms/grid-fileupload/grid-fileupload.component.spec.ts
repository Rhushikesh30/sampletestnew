import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridFileuploadComponent } from './grid-fileupload.component';

describe('GridFileuploadComponent', () => {
  let component: GridFileuploadComponent;
  let fixture: ComponentFixture<GridFileuploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridFileuploadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridFileuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
