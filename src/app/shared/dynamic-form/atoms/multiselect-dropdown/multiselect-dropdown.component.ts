import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-multiselect-dropdown',
  templateUrl: './multiselect-dropdown.component.html',
  styleUrls: ['./multiselect-dropdown.component.scss']
})
export class MultiselectDropdownComponent {


  @Input() field_id:any;
  @Input() field:any = {};
  @Input() form!:FormGroup;
  @Output() onChange = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit(): void { 
    
  }
  onSelectionChange(event:any){
    this.onChange.emit(event);
  }
}
