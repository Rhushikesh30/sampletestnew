import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map, Observable, startWith } from 'rxjs';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss'],
})
export class TextboxComponent implements OnInit {
  @Input() screenName: any;
  @Input() field: any = {};
  @Input() form!: FormGroup;
  @Input() submitBtn: any;
  //@Output() input: EventEmitter<any> = new EventEmitter();
  @Output() change = new EventEmitter<any>();

  numbersArray: number[] = Array.from({ length: 10 }, (_, index) => index);
  get isValid() {
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }

  options = [];
  filteredOptions!: Observable<any>;
  constructor(private dynamicFormService: DynamicFormService) {}

  ngOnInit(): void {}
  clickEvent(event: any, val: any) {
    this.change.emit('event');
  }
}
