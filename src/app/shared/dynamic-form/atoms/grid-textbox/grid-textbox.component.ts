import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-grid-textbox',
  templateUrl: './grid-textbox.component.html',
  styleUrls: ['./grid-textbox.component.scss'],
})
export class GridTextboxComponent implements OnInit {
  constructor() {}

  @Input() field: any = {};
  @Input() i: any;
  @Input() form!: FormGroup;
  @Input() form1: any;
  @Input() item: any;
  get isValid() {
    return this.form.controls[this.field.name].valid;
  }
  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }
  ngOnInit(): void {
    this.form.get('doc_sequence_no')?.setValue(this.i);
  }
  clickEvent(e: any, f: any) {
    if (f == 'farm_name') {
      for (let i = 0; i < this.item.length; i++) {
        const keys = Object.keys(this.item[i]);
        if (keys.includes('Farmer Crop')) {
          for (let j = 0; j < this.item[i]['Farmer Crop'].length; j++) {
            if (this.item[i]['Farmer Crop'][j]['field_key'] == 'farm_ref_id') {
              const gridRow1 = <FormArray>this.form1.get('Farmer Land');
              let prev_drp: any = [];

              for (let j = 0; j < gridRow1.length; j++) {
                if (
                  gridRow1.at(j).value.farm_name == '' ||
                  gridRow1.at(j).value.farm_name == null
                ) {
                  prev_drp.splice(j, 1);
                } else {
                  prev_drp.push({
                    id: gridRow1.at(j).value.farm_name,
                    key: gridRow1.at(j).value.farm_name,
                  });
                }
              }
              this.item[i]['Farmer Crop'][j]['dropdown_raw_query'] = prev_drp;
              this.item[i]['Farmer Crop'][j]['filter_dropdown_raw_query'] =
                prev_drp.slice();
            }
          }
        }
      }
    }
  }
}
