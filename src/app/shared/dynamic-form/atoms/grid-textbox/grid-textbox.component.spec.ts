import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridTextboxComponent } from './grid-textbox.component';

describe('GridTextboxComponent', () => {
  let component: GridTextboxComponent;
  let fixture: ComponentFixture<GridTextboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridTextboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridTextboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
