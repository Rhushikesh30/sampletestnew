import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-grid-toggle',
  templateUrl: './grid-toggle.component.html',
  styleUrls: ['./grid-toggle.component.scss'],
})
export class GridToggleComponent implements OnInit {
  constructor() {}

  @Input() field: any = {};
  @Input() form!: FormGroup;
  @Output() change = new EventEmitter<any>();

  ngOnInit(): void {}

  clickEvent(e: any) {
    this.change.emit(e);

    if (this.field.field_key == 'is_member_stying_with_farmer') {
      if (e.target.checked == true) {
        this.form.controls['pincode_details'].disable();
        this.form.controls['address'].disable();
      } else if (e.target.checked == false) {
        this.form.controls['pincode_details'].enable();
        this.form.controls['address'].enable();
      }
    }
  }
}
