import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import Swal from 'sweetalert2';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class DatepickerComponent implements OnInit {
  constructor() {}

  @Input() field: any = {};
  @Input() form!: FormGroup;
  @Input() BTN_VAL: any = {};
  todayDate = new Date().toJSON().split('T')[0];
  todayDateMin = new Date().toJSON().split('T')[0];
  today: any;

  ngOnInit(): void {
    // if ( this.BTN_VAL == 'Create Version') {
    //   if(this.field.field_key=='from_date'){
    //     const today = new Date();
    //     // Add one day to the current date
    //     const tomorrow = new Date(today);
    //     tomorrow.setDate(today.getDate() + 1);
    //     // Format the date as a string in the desired format (e.g., YYYY-MM-DD)
    //     const tomorrowDate = tomorrow.toJSON().split('T')[0];
    //   console.log(tomorrowDate);
    //     this.todayDate=  tomorrowDate
    //     this.todayDateMin = tomorrowDate
    //   }
    // }
    // else{
    //   console.log("yes");
    //   this.todayDate = new Date().toJSON().split('T')[0];
    //   this.todayDateMin = new Date().toJSON().split('T')[0];
    // }
  }

  countage(value: any) {
    let birthdate = this.form.value.dob;
    this.today = new Date();
    let timediff = Math.abs(this.today - birthdate);
    let Age = Math.floor(timediff / (1000 * 3600 * 24) / 365);
    this.form.get('age')?.setValue(Age);
  }

  FromDateChange(value: any) {
    // console.log(value,this.BTN_VAL);
    // if ( this.BTN_VAL == 'Create Version') {
    //     let currtDate = new Date().toJSON().split("T")[0];
    //     console.log(currtDate);
    //     if (value== currtDate) {
    //       Swal.fire({ title: 'Not allowed', text: 'Cannot create version on same day!!', icon: 'warning' });
    //     }
    //   }
  }
}
