import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridRadioComponent } from './grid-radio.component';

describe('GridRadioComponent', () => {
  let component: GridRadioComponent;
  let fixture: ComponentFixture<GridRadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridRadioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
