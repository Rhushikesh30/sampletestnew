import { Component,OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-grid-radio',
  templateUrl: './grid-radio.component.html',
  styleUrls: ['./grid-radio.component.scss']
})
export class GridRadioComponent implements OnInit {

  constructor() { }

  @Input() field:any = {};
  @Input() form!:FormGroup;
  
  ngOnInit(): void {
  }

}
