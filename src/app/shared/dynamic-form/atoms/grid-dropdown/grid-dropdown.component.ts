import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-grid-dropdown',
  templateUrl: './grid-dropdown.component.html',
  styleUrls: ['./grid-dropdown.component.scss']
})
export class GridDropdownComponent implements OnInit {


  @Input() field_id: any;
  @Input() field:any = {};
  @Input() form!:FormGroup;
  @Output() onChange = new EventEmitter<any>();
  @Input() disabledvalue!:boolean;
  @Input() i: any;
  @Input() formDatDetailsaExist:any;
  constructor() { }

  ngOnInit(): void {
   // if(this.field.is_onchange_event == false) this.field.dropdown_raw_query=[]
  }
  
  onSelectionChange(event:any){
    this.onChange.emit(event);
  }

}

