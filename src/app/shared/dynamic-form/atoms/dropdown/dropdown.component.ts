import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input() field_id:any;
  @Input() field:any = {};
  @Input() form!:FormGroup;
  @Output() onChange = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit(): void { 
    
  }
  onSelectionChange(event:any){
    this.onChange.emit(event);
  }
}
