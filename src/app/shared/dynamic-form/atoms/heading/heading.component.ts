
import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map, Observable, startWith } from 'rxjs';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss']
  
})
export class HeadingComponent implements OnInit {
  @Input() screenName: any
  @Input() field: any = {};
  @Input() form!: FormGroup;

  constructor(private dynamicFormService: DynamicFormService) { }

  ngOnInit(): void {
    
}

}


