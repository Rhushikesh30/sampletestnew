import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { DynamicFormComponent } from './dynamic-form.component';
import { CheckboxComponent } from './atoms/checkbox/checkbox.component';
import { DropdownComponent } from './atoms/dropdown/dropdown.component';
import { RadioComponent } from './atoms/radio/radio.component';
import { TextboxComponent } from './atoms/textbox/textbox.component';
import { DatepickerComponent } from './atoms/datepicker/datepicker.component';
import { SlideToggleComponent } from './atoms/slide-toggle/slide-toggle.component';
import { FileuploadComponent } from './atoms/fileupload/fileupload.component';
import { GridTextboxComponent } from './atoms/grid-textbox/grid-textbox.component';
import { GridDropdownComponent } from './atoms/grid-dropdown/grid-dropdown.component';
import { GridCheckboxComponent } from './atoms/grid-checkbox/grid-checkbox.component';
import { GridRadioComponent } from './atoms/grid-radio/grid-radio.component';
import { GridDatepickerComponent } from './atoms/grid-datepicker/grid-datepicker.component';
import { GridToggleComponent } from './atoms/grid-toggle/grid-toggle.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { DecimalGridComponent } from './atoms/decimal-grid/decimal-grid.component';
import { MatButtonModule } from '@angular/material/button';
import { HeadingComponent } from './atoms/heading/heading.component';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import { GridFileuploadComponent } from './atoms/grid-fileupload/grid-fileupload.component';
import { MultiselectDropdownComponent } from './atoms/multiselect-dropdown/multiselect-dropdown.component';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { MatSelectFilterModule } from 'mat-select-filter';

@NgModule({
  declarations: [
    DynamicFormComponent,
    CheckboxComponent,
    DropdownComponent,
    RadioComponent,
    TextboxComponent,
    DatepickerComponent,
    SlideToggleComponent,
    FileuploadComponent,
    GridTextboxComponent,
    GridDropdownComponent,
    GridCheckboxComponent,
    GridRadioComponent,
    GridDatepickerComponent,
    GridToggleComponent,
    DecimalGridComponent,
    HeadingComponent,
    GridFileuploadComponent,
    MultiselectDropdownComponent,
    
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonToggleModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    FormsModule,
    MatButtonModule,
    CdkAccordionModule,
    MomentDateModule,
    MatSelectFilterModule
  ],
  
  exports: [DynamicFormComponent,CheckboxComponent, DropdownComponent,RadioComponent,TextboxComponent,],
})
export class DynamicFormModule { }
