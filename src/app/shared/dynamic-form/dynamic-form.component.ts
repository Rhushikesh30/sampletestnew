import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
} from '@angular/forms';
import { DatePipe, formatDate } from '@angular/common';
import { Router } from '@angular/router';

import { ErrorCodes } from '../codes/error-codes';
import { DynamicFormService } from '../services/dynamic-form.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class DynamicFormComponent implements OnInit {
  @Input() fields: any;
  @Input() field: any;
  @Input()
  screenName!: string;
  @Input()
  BTN_VAL!: string;
  @Input()
  submitBtn!: boolean;
  @Input()
  rowData: any = [];
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input()
  editFlag!: string;
  @Input()
  editOrView!: string;
  @Input()
  showLoaderSubmit!: boolean;
  @Input()
  isOnlyMaster!: boolean;
  @Input()
  tableData: any[] = [];

  form!: FormGroup;
  initialItemRow!: [];
  initialItemRow1!: [];
  formdata: any;
  formarraydata: any;
  newArray: any[] = [];
  formarraysubdata: any;
  fieldsCtrls: any = {};
  fieldsCtrls1: any = {};
  fieldsCtrls2: any = {};
  fieldsCtrls3: any = {};
  next_screen_name!: string;
  prev_screen_name!: string;
  formDataExist = false;
  formDatDetailsaExist!: boolean;
  formDatSubDetailsaExist!: boolean;
  formHierarchyDetailsaExist!: boolean;
  todayDate = new Date().toJSON().split('T')[0];
  COMPANY_ID: any;
  details_length: any;
  showgridadd = false;
  disabledvalue = false;
  heading_arr: any = [];
  Newformdata: any;
  sorted: any = [];
  objectKeys: any;
  items = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5'];
  expandedIndex = 0;
  detail_array: any = [];
  form_detail_array: any = [];
  previewImage1: any;
  formarrayhierDetaildata: any;
  previewImage: any = [];
  previewImageType1: any = false;
  item_name: any;
  item_code: any;
  roleName: any;
  count_add = 0;
  rate_dropdown = [];
  hideButton = true;
  hideUpload = true;
  showLoaderupload = false;
  columnNames: any = [];
  selectedEmployee: any = null;

  constructor(
    private formBuilder: FormBuilder,
    private commonsetupservice: DynamicFormService,
    public datepipe: DatePipe,
    private router: Router,
    private errorCodes: ErrorCodes,
    private encDecService: EncrDecrService
  ) {}

  ngOnInit(): void {
    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.roleName = rolename[0];

    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');

    let data1: any;
    // data1 = this.fields['screendefinitiondetails_set'].filter((ele: any) => {
    //   return ele.field_key != 'to_date'
    // })
    // if (this.rowData.length == 0) {
    //   this.fields['screendefinitiondetails_set'] = data1
    // }
    this.formdata = this.fields['screendefinition_set'];
    this.Newformdata = this.formdata;
    this.sorted = this.processFormData(this.Newformdata);

    this.columnNames.push('Item Name', 'Item Rate', 'UOM');

    // if (this.screenName == 'Item Rate Commision') {
    //   if (this.roleName == 'Federation Admin') {
    //     let filter_vale = this.formdata[0]['dropdown_raw_query'].filter((d: any) => (d.key == 'FPC' || d.key == 'Common'))
    //     this.formdata[0]['dropdown_raw_query'] = filter_vale
    //   }
    //   else {
    //     let filter_value = this.formdata[0]['dropdown_raw_query'].filter((d: any) => (d.key != 'FPC'))
    //     this.formdata[0]['dropdown_raw_query'] = filter_value
    //   }
    // }

    for (let i = 0; i < this.formdata.length; i++) {
      if (this.formdata[i]['field_type_id'] == 'heading') {
        const j = i + 1;
        for (let k = j; k < this.formdata.length; k++) {
          this.heading_arr.push(this.formdata[k]);
        }
      } else if (this.formdata[i]['field_type_id'] == 'dropdown') {
        this.formdata[i]['filter_dropdown_raw_query'] =
          this.formdata[i]['dropdown_raw_query'].slice();
      }
    }

    this.Newformdata = this.Newformdata.filter(
      (item: any) => !this.heading_arr.includes(item)
    );

    this.detail_array = this.fields['screendefinitiondetails_set'];

    this.formarraydata = this.fields['screendefinitiondetails_set'];
    this.formarraysubdata = this.fields['screendefinitionsubdetails_set'];
    this.newArray.push(this.formarraydata);

    this.details_length = 0;

    if (Object.keys(this.formdata).length != 0) {
      this.formDataExist = true;
      this.fieldsCtrls['master_data'] = this.formBuilder.array([
        this.masterdata(),
      ]);
    }
    this.formarrayhierDetaildata = this.fields['screenhierarchydetails_set'];

    if (Object.keys(this.formarraydata).length != 0) {
      if (this.fields['screenhierarchydetails_set'].length == 0) {
        this.formDatDetailsaExist = true;
        this.formHierarchyDetailsaExist = false;
      } else if (this.fields['screenhierarchydetails_set'].length > 0) {
        this.formDatDetailsaExist = false;
        this.formHierarchyDetailsaExist = true;
      }
    }

    if (this.formDatDetailsaExist) {
      for (const j in this.newArray[0]) {
        this.newArray[0][j]['dropdown_raw_query_0'] =
          this.newArray[0][j]['dropdown_raw_query'];
        if (this.newArray[0][j]['field_type_id'] == 'dropdown') {
          this.newArray[0][j]['filter_dropdown_raw_query_0'] =
            this.newArray[0][j]['dropdown_raw_query_0'].slice();
        }
      }
    }

    if (this.formHierarchyDetailsaExist) {
      this.formarraydata.forEach((entry: any) => {
        const key = Object.keys(entry)[0];
        const value = entry[key];
        for (let i = 0; i < value.length; i++) {
          if (value[i]['field_key'] == 'farm_ref_id') {
            value[i]['filter_dropdown_raw_query'] = [];
          } else {
            if (value[i]['field_type_id'] == 'dropdown') {
              value[i]['filter_dropdown_raw_query'] =
                value[i]['dropdown_raw_query'].slice();
            }
          }
        }

        this.fieldsCtrls[key] = this.formBuilder.array([
          this.initialitemRow(value),
        ]);
      });
    } else {
      this.fieldsCtrls['details_data'] = this.formBuilder.array([
        this.initialitemRow11(),
      ]);
    }

    if (Object.keys(this.formarraysubdata).length != 0) {
      this.formDatSubDetailsaExist = true;
    }

    this.fieldsCtrls['sub_details_data'] = this.formBuilder.array([
      this.initialitemRow1(),
    ]);
    this.fieldsCtrls['screen_name'] = new FormControl(this.screenName);
    this.fieldsCtrls['id'] = new FormControl('');
    this.form = new FormGroup(this.fieldsCtrls);

    if (this.screenName == 'Item Rate Commision') {
      if (this.roleName == 'FPC Admin') {
        const gridrow = (<FormArray>this.form.get('master_data')).at(0);
        gridrow.get('pricing_type_ref_id')?.disable();
      }
    }

    if (this.screenName == 'Supplier' || this.screenName == 'Customer') {
      const gridrow = (<FormArray>this.form.get('master_data')).at(0);
      gridrow.get('tds_percentage')?.disable();
    }

    if (!Array.isArray(this.rowData)) {
      this.viewRecord(this.rowData);
      if (this.submitBtn === true && this.editOrView == 'createversion')
        this.BTN_VAL = 'Create Version';
      if (this.submitBtn === true && this.editOrView == 'updateversion')
        this.BTN_VAL = 'Update Version';
    }
    const gridrow = (<FormArray>this.form.get('master_data')).at(0);
    if (this.screenName == 'Farmer' && this.BTN_VAL == 'Submit') {
      gridrow.get('password')?.addValidators(Validators.required);

      gridrow.get('password')?.updateValueAndValidity();
    }
    if (this.screenName == 'Farmer' && this.BTN_VAL == 'Update') {
    }
    if (
      this.submitBtn == true &&
      this.BTN_VAL == 'Update' &&
      this.rowData['revision_status'] == 'Effective'
    ) {
    } else {
      if (gridrow.get('from_date') != null) {
        gridrow.get('to_date')?.setValue('31-12-2099');

        gridrow.get('from_date')?.valueChanges.subscribe((val) => {
          const curr_date = new Date().toJSON().split('T')[0];
          val = this.datepipe.transform(val, 'yyyy-MM-dd');

          if (val != null) {
            if (val === curr_date) {
              gridrow.get('revision_status')?.setValue('Effective');
            } else {
              gridrow.get('revision_status')?.setValue('Future');
            }
          }
          // }
        });
      }
    }
    this.next_screen_name = this.fields['next_attribute_name'];
    this.prev_screen_name = this.fields['prev_attribute_name'];
  }

  processFormData(formdata: any[]): any[] {
    const sorted: any[] = [];
    let dict1: any = {};

    for (let i = 0; i < formdata.length; i++) {
      if (formdata[i]['field_type_id'] === 'heading') {
        if (Object.keys(dict1).length > 0) {
          sorted.push(dict1);
          dict1 = {}; // Reset the dictionary for the next heading
        }

        dict1[formdata[i].field_label] = [];

        const j = i + 1;
        for (let k = j; k < formdata.length; k++) {
          if (formdata[k]['field_type_id'] === 'heading') {
            break;
          }
          dict1[formdata[i].field_label].push(formdata[k]);
        }
      }
    }

    if (Object.keys(dict1).length > 0) {
      sorted.push(dict1);
    }

    return sorted;
  }
  nextRoutingForm() {
    this.next_screen_name = this.next_screen_name.toLowerCase();
    const replaced = this.next_screen_name.replace(/ /g, '-');
    const routing_url = '/common-setup/' + replaced;
    this.router.navigate([routing_url]).then(() => {
      setTimeout(function () {
        window.location.reload();
      }, 2000);
    });
  }

  prevRoutingForm() {
    this.prev_screen_name = this.prev_screen_name.toLowerCase();

    const replaced = this.prev_screen_name.replace(/ /g, '-');

    const routing_url = '/common-setup/' + replaced;
    this.router.navigate([routing_url]).then(() => {
      setTimeout(function () {
        window.location.reload();
      }, 2000);
    });
  }

  setExistingArray(initialArray = [], val: any): FormArray {
    const formArray: any = new FormArray([]);
    this.previewImage[val] = [];
    let index = 0;

    initialArray.forEach((element: any, ind) => {
      this.count_add = this.count_add + 1;
      if (element.href_receipt_attachment_path) {
        if (element.href_receipt_attachment_path != '') {
          this.previewImage[val].push({
            id: ind,
            image: this.encDecService.decryptedData(
              element.href_receipt_attachment_path
            ),
          });
        }
      }

      for (let i = 0; i < this.formarraydata.length; i++) {
        if (this.formarraydata[i]['is_onchange_event'] == true) {
          const ele = this.formarraydata[i]['field_key'];

          if (element[ele] != null) {
            this.onChange(
              element[ele],
              'details_data',
              this.formarraydata[i]['id'],
              index,
              false,
              '',
              '',
              [],
              ele,
              this.formarraydata[i]['is_onchange_event']
            );
          }
        }
      }
      index = index + 1;
      this.newArray.push(this.formarraydata);

      if (this.formDatDetailsaExist) {
        this.newArray.push(this.formarraydata);
        for (let i = 1; i < this.newArray.length; i++) {
          for (const j in this.newArray[i]) {
            this.newArray[i][j]['dropdown_raw_query_' + String(i)] =
              this.newArray[i][j]['dropdown_raw_query'];
            if (this.newArray[i][j]['field_type_id'] == 'dropdown') {
              this.newArray[i][j]['filter_dropdown_raw_query_' + String(i)] =
                this.newArray[i][j]['dropdown_raw_query'].slice();
            }
          }
        }
      }
      formArray.push(this.formBuilder.group(element));
    });
    return formArray;
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  viewRecord(payload: any) {
    console.log(payload);

    this.hideUpload = false;
    if (this.BTN_VAL != 'Update') {
      this.disabledvalue = true;
    }
    if (!this.submitBtn) {
      this.form.disable();
      this.hideButton = false;
    }
    this.commonsetupservice
      .getScreennDataById(payload.id, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.form.get('id')?.setValue(data['id']);
          for (const val in data.master_data) {
            if(val=='to_date'){
              let convert_date = new Date(data.master_data[val])
              const new_d = this.datepipe.transform(
                convert_date,
                'dd-MM-yyyy'
              );
              data.master_data[val]=  new_d
            }
            if (this.fields['screenhierarchydetails_set'].length > 0) {
              for (
                let k = 0;
                k < this.fields['screenhierarchydetails_set'].length;
                k++
              ) {
                const val11 =
                  this.fields['screenhierarchydetails_set'][k][
                    'attribute_name'
                  ];
                if (val == val11) {
                  if (!this.submitBtn) {
                    this.form.get(val)?.disable();
                  }
                  const details_data = data.master_data[val].filter(function (
                    data: any
                  ) {
                    return data.is_active == true;
                  });
                  this.details_length = details_data.length - 1;

                  if (details_data.length > 0) {
                    if (val == 'Farmer Crop') {
                      for (let i = 0; i < this.detail_array.length; i++) {
                        const keys = Object.keys(this.detail_array[i]);
                        if (keys.includes('Farmer Crop')) {
                          for (
                            let j = 0;
                            j < this.detail_array[i]['Farmer Crop'].length;
                            j++
                          ) {
                            if (
                              this.detail_array[i]['Farmer Crop'][j][
                                'field_key'
                              ] == 'farm_ref_id'
                            ) {
                              const gridRow1 = <FormArray>(
                                this.form.get('Farmer Land')
                              );

                              let prev_drp: any = [];

                              for (let j = 0; j < gridRow1.length; j++) {
                                prev_drp.push({
                                  id: gridRow1.at(j).value.farm_name,
                                  key: gridRow1.at(j).value.farm_name,
                                });
                              }
                              this.detail_array[i]['Farmer Crop'][j][
                                'dropdown_raw_query'
                              ] = prev_drp;
                              this.detail_array[i]['Farmer Crop'][j][
                                'filter_dropdown_raw_query'
                              ] = prev_drp;
                            }
                          }
                        }
                      }
                      this.form.setControl(
                        val,
                        this.setExistingArray(details_data, val)
                      );
                    } else {
                      this.form.setControl(
                        val,
                        this.setExistingArray(details_data, val)
                      );
                    }
                  }

                  const fc: any = this.form.controls;

                  if (val == 'Farmer Family') {
                    for (let i = 0; i < fc[val].controls.length; i++) {
                      const md = fc[val].controls[i].controls;

                      if (md['is_member_stying_with_farmer'].value == true) {
                        md['address'].disable();
                        md['pincode_details'].disable();
                      }
                    }
                  }
                }
              }
            }
            if (val == 'details_data') {
              const details_data = data.master_data.details_data.filter(
                function (data: any) {
                  return data.is_active == true;
                }
              );
              this.details_length = details_data.length - 1;

              if (data.master_data.details_data.length > 0) {

                this.form.setControl(
                  'details_data',
                  this.setExistingArray(details_data, val)
                );
              }

              if (!this.submitBtn) {
                this.form.get(val)?.disable();
              }
            } else {
              if (
                (<FormArray>this.form.get('master_data')).at(0).get(val) != null
              ) {
                const gridRow = (<FormArray>this.form.get('master_data')).at(0);
                if (this.screenName == 'Farmer') {
                  gridRow.get('phone')?.disable();
                }

                if (data['master_data']['href_receipt_attachment_path'] != '') {
                  this.previewImage1 = this.encDecService.decryptedData(
                    data['master_data']['href_receipt_attachment_path']
                  );

                  this.previewImageType1 = true;
                }
                for (let i = 0; i < this.formdata.length; i++) {
                  if (this.formdata[i]['is_onchange_event'] == true) {
                    const ele = this.formdata[i]['field_key'];
                    if (this.formdata[i]['field_key'] == 'is_tds_applicable') {
                      if (gridRow.get(ele)?.value == true) {
                        this.onChangeToggle(true, ele);
                      }
                    }

                    if (data['master_data'][val] != null) {
                      if (val == ele) {
                        this.onChange(
                          data['master_data'][val],
                          'master_data',
                          this.formdata[i]['id'],
                          i,
                          false,
                          '',
                          '',
                          [],
                          ele,
                          this.formdata[i]['is_onchange_event']
                        );
                      }
                    }
                  }
                }
                gridRow.get(val)?.setValue(data['master_data'][val]);
                if (
                  this.submitBtn &&
                  this.BTN_VAL == 'Update' &&
                  gridRow.get('revision_status')?.value == 'Effective'
                ) {
                  gridRow.disable();
                } else if (
                  this.submitBtn &&
                  this.BTN_VAL == 'Update' &&
                  gridRow.get('revision_status')?.value == 'Future'
                ) {
                  gridRow.enable();
                  // if(this.screenName=='Item Quality Parameter'){
                  gridRow.disable();
                  gridRow.get('from_date')?.enable();
                  //}
                }
                if (
                  this.BTN_VAL == 'Create Version' ||
                  this.BTN_VAL == 'Update Version'
                ) {
                  var gridrow = (<FormArray>this.form.get('master_data')).at(0);
                  let dt = new Date().setDate(new Date().getDate() + 1);
                  let val1 = this.datepipe.transform(dt, 'yyyy-MM-dd');
                  gridrow.get('from_date')?.setValue(val1);
                }
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  fpcnameinratecommision(event: any) {
    const key = this.formdata[0]['dropdown_raw_query'].filter(
      (d: any) => d.id == event
    );
    if (key[0]['key'] != 'Common') {
      this.commonsetupservice
        .getDynamicData('item_supplier_data', 'company_name')
        .subscribe({
          next: (data: any) => {
            this.formdata[1]['dropdown_raw_query'] = data;
            this.formdata[1]['filter_dropdown_raw_query'] =
              this.formdata[1]['dropdown_raw_query'].slice();

            this.formdata[1]['field_label'] = 'Supplier Name';
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    } else {
      this.commonsetupservice
        .getDynamicData('Item_fpc_name', 'company_name')
        .subscribe({
          next: (data: any) => {
            this.formdata[1]['dropdown_raw_query'] = data;
            this.formdata[1]['filter_dropdown_raw_query'] =
              this.formdata[1]['dropdown_raw_query'].slice();
            this.formdata[1]['field_label'] = 'FPC Name';
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    }
  }

  quality_type_change(event: any, index: any, field_key: any) {
    if (index > 0) {
      const gridRow = (<FormArray>this.form.get('details_data')).at(index - 1);
      const gridRow1 = (<FormArray>this.form.get('details_data')).at(index);

      if (gridRow && gridRow1) {
        const qt = gridRow.get('quality_type')?.value;
        const mat_qt_value = gridRow.get('max_quality_value')?.value;
        if (event == qt) {
          const min_value = (parseFloat(mat_qt_value) + 0.001).toFixed(3);
          gridRow1.get('min_quality_value')?.setValue(min_value);
        } 
        else {
          let min_value1
          const total_grid = (<FormArray>this.form.get('details_data'))
          for (let ind = (total_grid.length-3); ind > 0 ; ind--) {
            let exist_qt = total_grid.at(ind).get('quality_type')?.value
            if(event == exist_qt){
              const prev_qt_value = total_grid.at(ind).get('max_quality_value')?.value
              min_value1 = (parseFloat(prev_qt_value) + 0.001).toFixed(3);
              break;
          }
          }
          if(min_value1){
            gridRow1.get('min_quality_value')?.setValue(min_value1);
          }
          else{
            gridRow1.get('min_quality_value')?.setValue(0);

          }
        }
      }
    }
  }

  GetConversionFactor(e: any) {
    this.commonsetupservice
      .getDynamicData('item_uom_conversion_data', 'uom')
      .subscribe({
        next: (data: any) => {
          const gridrow = (<FormArray>this.form.get('master_data')).at(0);
          const base_uom = gridrow.get('base_uom')?.value;
          if (base_uom == '' || base_uom == undefined) {
          } else {
            if (data.length > 0) {
              for (let i = 0; i < data.length; i++) {
                if (
                  data[i]['target_uom_code'] == base_uom &&
                  data[i]['source_uom_code'] == e
                ) {
                  gridrow
                    .get('base_uom_conversion')
                    ?.setValue(data[i]['conversion_factor']);
                }
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  onChange(
    event: any,
    val: any,
    field_id: any,
    index: any,
    is_concatanate: any,
    concatanate_field: any,
    cancatanate_value: any,
    dropdown_raw_query: any,
    field_key: any,
    is_onchange_event: any
  ) {
    if (val == 'master_data') {
      if (is_concatanate == true) {
        if (this.screenName == 'Item Master') {
          const concatanate_field_data = concatanate_field.split(',');
          let cancatanate_value_data;

          for (const i in concatanate_field_data) {
            cancatanate_value_data = concatanate_field_data[i].split('-');
            for (let j = 0; j < dropdown_raw_query.length; j++) {
              if (dropdown_raw_query[j]['id'] == event) {
                const val = cancatanate_value_data[1];
                const val2 = cancatanate_value_data[0];

                const gridrow = (<FormArray>this.form.get('master_data')).at(0);
                if (field_key == 'item_attribute1') {
                  gridrow
                    .get('base_uom')
                    ?.setValue(dropdown_raw_query[j]['base_uom']);
                }
                if (gridrow.get(val2)?.value == '') {
                  gridrow.get(val2)?.setValue(dropdown_raw_query[j][val]);
                } else if (cancatanate_value == 1) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];

                  const parts = firstPart.split('-');
                  parts[0] = replacement;
                  const modifiedString = parts.join('-');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 2) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('-');
                  parts[1] = replacement;
                  const modifiedString = parts.join('-');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 3) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('-');
                  parts[2] = replacement;
                  const modifiedString = parts.join('-');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 4) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('-');
                  parts[3] = replacement;
                  const modifiedString = parts.join('-');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 5) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('-');
                  parts[4] = replacement;
                  const modifiedString = parts.join('-');
                  gridrow.get(val2)?.setValue(modifiedString);
                }
              }
            }
          }
        } else if (this.screenName == 'Chart Of Account') {
          if (is_onchange_event == true) {
            const screen_defination_field_ref_id = field_id;
            const dict = {
              screen_hierarchy_id: this.fields['id'],
              screen_field_value: event,
              screen_defination_field_ref_id: screen_defination_field_ref_id,
            };
            this.commonsetupservice.getOnChangeEventtVal(dict).subscribe({
              next: (data: any) => {
                for (let i = 0; i < Object.keys(this.formdata).length; i++) {
                  if (
                    this.formdata[i]['field_key'] == data['parameter_name'] &&
                    field_key == 'is_rate_percentage' &&
                    event == true
                  ) {
                    this.rate_dropdown = data['parameter_data'];
                    this.formdata[i]['dropdown_raw_query'] = [
                      {
                        id: 0,
                        key: 'Transaction Value',
                      },
                    ];
                    this.formdata[i]['filter_dropdown_raw_query'] =
                      this.formdata[i]['dropdown_raw_query'].slice();
                  }
                  if (
                    this.formdata[i]['field_key'] == data['parameter_name'] &&
                    this.formdata[i]['field_key'] != 'basis_type_id'
                  ) {
                    this.formdata[i]['dropdown_raw_query'] =
                      data['parameter_data'];
                    this.formdata[i]['filter_dropdown_raw_query'] =
                      this.formdata[i]['dropdown_raw_query'].slice();
                  }
                }
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
          }
          const concatanate_field_data = concatanate_field.split(',');
          let cancatanate_value_data;

          for (const i in concatanate_field_data) {
            cancatanate_value_data = concatanate_field_data[i].split('-');
            for (let j = 0; j < dropdown_raw_query.length; j++) {
              if (dropdown_raw_query[j]['id'] == event) {
                const val = cancatanate_value_data[1];
                const val2 = cancatanate_value_data[0];

                const gridrow = (<FormArray>this.form.get('master_data')).at(0);
                // if (field_key == 'item_attribute1') {
                //   gridrow.get('base_uom')?.setValue(dropdown_raw_query[j]['base_uom']);
                // }
                if (gridrow.get(val2)?.value == '') {
                  gridrow.get(val2)?.setValue(dropdown_raw_query[j][val] + '.');
                } else if (cancatanate_value == 1) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];

                  const parts = firstPart.split('.');
                  parts[0] = replacement;
                  const modifiedString = parts.join('.');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 2) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];

                  const parts = firstPart.split('.');

                  parts[1] = replacement;
                  const modifiedString = parts.join('.');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 3) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('.');
                  parts[2] = replacement;
                  const modifiedString = parts.join('.');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 4) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('.');
                  parts[3] = replacement;
                  const modifiedString = parts.join('.');
                  gridrow.get(val2)?.setValue(modifiedString);
                } else if (cancatanate_value == 5) {
                  const firstPart = gridrow.get(val2)?.value;
                  const replacement = dropdown_raw_query[j][val];
                  const parts = firstPart.split('.');
                  parts[4] = replacement;
                  const modifiedString = parts.join('.');
                  gridrow.get(val2)?.setValue(modifiedString);
                }
              }
            }
          }
        }
      } else if (field_key == 'pricing_type_ref_id') {
        if (this.roleName == 'Federation Admin') {
          this.fpcnameinratecommision(event);
        }
      } else if (this.screenName == 'Farmer' && field_key == 'bank_name') {
      } else if (
        this.screenName == 'Farmer' &&
        field_key == 'bank_account_holder_name'
      ) {
      } else if (
        this.screenName == 'Farmer' &&
        field_key == 'bank_account_no'
      ) {
      } else if (this.screenName == 'Farmer' && field_key == 'bank_ifsc_code') {
      } else if (this.screenName == 'Farmer' && field_key == 'bank_doc_id') {
        // this.UpdateValidityFarmerBankDetails();
      } else if (this.screenName == 'Farmer' && field_key == 'account_type') {
        //this.UpdateValidityFarmerBankDetails();
      } else if (field_key == 'is_tds_applicable') {
      } else if (field_key == 'tds_percentage') {
      } else if (field_key == 'commission') {
      } else if (this.screenName == 'Item Master' && field_key == 'uom') {
        this.GetConversionFactor(event);
      } else {
        const screen_defination_field_ref_id = field_id;
        const dict = {
          screen_hierarchy_id: this.fields['id'],
          screen_field_value: event,
          screen_defination_field_ref_id: screen_defination_field_ref_id,
        };
        this.commonsetupservice.getOnChangeEventtVal(dict).subscribe({
          next: (data: any) => {
            for (let i = 0; i < Object.keys(this.formdata).length; i++) {
              if (
                this.formdata[i]['field_key'] == data['parameter_name'] &&
                field_key == 'is_rate_percentage' &&
                event == true
              ) {
                this.rate_dropdown = data['parameter_data'];
                this.formdata[i]['dropdown_raw_query'] = [
                  {
                    id: 0,
                    key: 'Transaction Value',
                  },
                ];
                this.formdata[i]['filter_dropdown_raw_query'] =
                  this.formdata[i]['dropdown_raw_query'].slice();
              }
              if (
                this.formdata[i]['field_key'] == data['parameter_name'] &&
                this.formdata[i]['field_key'] != 'basis_type_id'
              ) {
                this.formdata[i]['dropdown_raw_query'] = data['parameter_data'];
                this.formdata[i]['filter_dropdown_raw_query'] =
                  this.formdata[i]['dropdown_raw_query'].slice();
              }
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      }
    } else if (val == 'details_data') {
      if (this.screenName == 'Item Quality Parameter') {
        this.quality_type_change(event, index, field_key);
      } else if (field_key == 'vehicle_ref_id') {
        this.commonsetupservice
          .getDynamicData('vehicle_master', 'vehicle_type_id')
          .subscribe({
            next: (data: any) => {
              if (data.length > 0) {
                const fc1: any = this.form.controls;
                const key = 'Farmer Vehicle';

                for (let i = 0; i < data.length; i++) {
                  if (event == data[0]['id']) {
                    for (let i = 0; i < fc1[key].controls.length; i++) {
                      const md1 = fc1[key].controls[i].controls;
                      md1.vehicle_type_id.disable();

                      md1.vehicle_type_id.setValue(data[0]['key']);
                    }
                  } else {
                    for (let i = 0; i < fc1[key].controls.length; i++) {
                      const md1 = fc1[key].controls[i].controls;
                      md1.vehicle_type_id.disable();
                      md1.vehicle_type_id.setValue(0);
                    }
                  }
                }
              }
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
      } else {
        const screen_defination_detail_field_ref_id = field_id;

        this.quality_type_change(event, index, field_key);

        if (this.screenName == 'Supplier' || this.screenName == 'Customer') {
          const gridRow = <FormArray>this.form.get('details_data');

          if (field_key == 'country_ref_id_id') {
            for (let rec = 0; rec < dropdown_raw_query.length; rec++) {
              if (dropdown_raw_query[rec]['id'] == event) {
                if (dropdown_raw_query[rec]['key'] == 'India') {
                  gridRow
                    .at(index)
                    .get('state_ref_id_id')
                    ?.addValidators(Validators.required);
                  gridRow
                    .at(index)
                    .get('district_ref_id_id')
                    ?.addValidators(Validators.required);
                  gridRow
                    .at(index)
                    .get('sub_district_ref_id_id')
                    ?.addValidators(Validators.required);
                  gridRow
                    .at(index)
                    .get('pin_code')
                    ?.addValidators(Validators.required);

                  const fc1: any = this.form.controls;
                  const md1 = fc1['details_data'].controls[index].controls;
                  md1['district_ref_id_id'].updateValueAndValidity();
                  md1['state_ref_id_id'].updateValueAndValidity();
                  md1['sub_district_ref_id_id'].updateValueAndValidity();
                  md1['pin_code'].updateValueAndValidity();
                } else {
                  gridRow.at(index).get('state_ref_id_id')?.clearValidators();
                  gridRow
                    .at(index)
                    .get('district_ref_id_id')
                    ?.clearValidators();
                  gridRow
                    .at(index)
                    .get('sub_district_ref_id_id')
                    ?.clearValidators();
                  gridRow.at(index).get('pin_code')?.clearValidators();

                  const fc1: any = this.form.controls;
                  const md1 = fc1['details_data'].controls[index].controls;
                  md1['district_ref_id_id'].updateValueAndValidity();
                  md1['state_ref_id_id'].updateValueAndValidity();
                  md1['sub_district_ref_id_id'].updateValueAndValidity();
                  md1['pin_code'].updateValueAndValidity();
                }
              }
            }
          }
        }
        if (this.screenName == 'Farmer' && field_key == 'farm_name') {
        } else {
          const dict1 = {
            screen_hierarchy_id: this.fields['id'],
            screen_field_value: event,
            screen_defination_detail_field_ref_id:
              screen_defination_detail_field_ref_id,
          };
          this.commonsetupservice.getOnChangeEventDetailsVal(dict1).subscribe({
            next: (data: any) => {
              for (const j in this.newArray[index]) {
                if (
                  this.newArray[index][j]['field_key'] == data['parameter_name']
                ) {
                  this.newArray[index][j][
                    'dropdown_raw_query_' + String(index)
                  ] = data['parameter_data'];
                  this.newArray[index][j][
                    'filter_dropdown_raw_query_' + String(index)
                  ] =
                    this.newArray[index][j][
                      'dropdown_raw_query_' + String(index)
                    ];
                }
              }
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
        }
      }
    }
  }

  onChangeToggle(event: any, field_key: any) {
    const gridrow = (<FormArray>this.form.get('master_data')).at(0);

    for (let i = 0; i < Object.keys(this.formdata).length; i++) {
      if (
        this.formdata[i]['field_key'] == 'basis_type_id' &&
        field_key == 'is_rate_percentage'
      ) {
        const val11 = gridrow.get('is_rate_percentage')?.value;

        if (val11 == true) {
          this.rate_dropdown = this.formdata[i]['dropdown_raw_query'];
          this.formdata[i]['dropdown_raw_query'] = [
            {
              id: 0,
              key: 'Transaction Value',
            },
          ];
          this.formdata[i]['filter_dropdown_raw_query'] =
            this.formdata[i]['dropdown_raw_query'].slice();
          const gridrow = (<FormArray>this.form.get('master_data')).at(0);
          gridrow.get('commission')?.setValue('');
        }
        if (val11 == false) {
          this.commonsetupservice.getDynamicData('uom', 'uom_code').subscribe({
            next: (data: any) => {
              this.formdata[i]['dropdown_raw_query'] = data;
              this.formdata[i]['filter_dropdown_raw_query'] =
                this.formdata[i]['dropdown_raw_query'].slice();
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
        }
      }

      if (
        this.formdata[i]['field_key'] == 'tds_percentage' &&
        field_key == 'is_tds_applicable'
      ) {
        const gridrow = (<FormArray>this.form.get('master_data')).at(0);
        const val11 = gridrow.get('is_tds_applicable')?.value;

        if (val11 == true) {
          gridrow.get('tds_percentage')?.enable();
        } else {
          gridrow.get('tds_percentage')?.disable();
          gridrow.get('tds_percentage')?.setValue('');
        }
      }
    }
  }

  onGridToggleChange(event: any, ind: any, field_key: any) {
    if (this.screenName == 'Item Quality Parameter') {
      const gridRow = <FormArray>this.form.get('details_data');
      let val;
      if (event) {
        val = event.checked;
      } else {
        val = gridRow.at(0).get('rate_percentage')?.value;
        field_key = 'rate_percentage';
      }
      for (let index = 0; index < gridRow.length; index++) {
        gridRow.at(index).get(field_key)?.setValue(val);
      }
    }
  }

  onChangeText(event: any, val: any) {
    if (val == 'tds_percentage') {
      if (
        !(
          event.target.value == '' ||
          event.target.value == 'None' ||
          event.target.value == undefined
        )
      ) {
        if (parseFloat(event.target.value) > 100) {
          Swal.fire({
            title: 'Not allowed',
            text: 'Please Enter Percentage less or equal 100!',
            icon: 'warning',
          });
          const gridrow = (<FormArray>this.form.get('master_data')).at(0);
          gridrow.get('tds_percentage')?.setValue('');
        }
      }
    } else if (this.screenName == 'Agent' && val == 'commission') {
      const gridrow = (<FormArray>this.form.get('master_data')).at(0);
      const get_val = gridrow.get('is_rate_percentage')?.value;
      if (get_val == true) {
        if (parseFloat(gridrow.get('commission')?.value) > 100) {
          Swal.fire({
            title: 'Not allowed',
            text: 'Please Enter Percentage less or equal 100!',
            icon: 'warning',
          });
          gridrow.get('commission')?.setValue('');
        }
      }
    } else if (this.screenName == 'Farmer' && val == 'bank_name') {
      this.UpdateValidityFarmerBankDetails();
    } else if (
      this.screenName == 'Farmer' &&
      val == 'bank_account_holder_name'
    ) {
      this.UpdateValidityFarmerBankDetails();
    } else if (this.screenName == 'Farmer' && val == 'bank_account_no') {
      this.UpdateValidityFarmerBankDetails();
    } else if (this.screenName == 'Farmer' && val == 'bank_attachment_path') {
      this.UpdateValidityFarmerBankDetails();
    } else if (this.screenName == 'Farmer' && val == 'bank_ifsc_code') {
      this.UpdateValidityFarmerBankDetails();
    }
  }

  UpdateValidityFarmerBankDetails() {
    const gridrow = (<FormArray>this.form.get('master_data')).at(0);
    gridrow.get('bank_ifsc_code')?.addValidators(Validators.required);
    gridrow.get('account_type')?.addValidators(Validators.required);
    gridrow.get('bank_account_holder_name')?.addValidators(Validators.required);
    gridrow.get('bank_account_no')?.addValidators(Validators.required);
    gridrow.get('bank_doc_id')?.addValidators(Validators.required);
    gridrow.get('bank_attachment_path')?.addValidators(Validators.required);
    gridrow.get('bank_name')?.addValidators(Validators.required);

    gridrow.get('bank_ifsc_code')?.updateValueAndValidity();
    gridrow.get('account_type')?.updateValueAndValidity();
    gridrow.get('bank_account_holder_name')?.updateValueAndValidity();
    gridrow.get('bank_account_no')?.updateValueAndValidity();
    gridrow.get('bank_doc_id')?.updateValueAndValidity();
    gridrow.get('bank_attachment_path')?.updateValueAndValidity();
    gridrow.get('bank_name')?.updateValueAndValidity();
  }

  ClearValidatorsFarmerBankDetails() {
    const gridrow = (<FormArray>this.form.get('master_data')).at(0);
    gridrow.get('bank_ifsc_code')?.clearValidators();
    gridrow.get('account_type')?.clearValidators();
    gridrow.get('bank_account_holder_name')?.clearValidators();
    gridrow.get('bank_account_no')?.clearValidators();
    gridrow.get('bank_doc_id')?.clearValidators();
    gridrow.get('bank_attachment_path')?.clearValidators();
    gridrow.get('bank_name')?.clearValidators();

    gridrow.get('bank_ifsc_code')?.updateValueAndValidity();
    gridrow.get('account_type')?.updateValueAndValidity();
    gridrow.get('bank_account_holder_name')?.updateValueAndValidity();
    gridrow.get('bank_account_no')?.updateValueAndValidity();
    gridrow.get('bank_doc_id')?.updateValueAndValidity();
    gridrow.get('bank_attachment_path')?.updateValueAndValidity();
    gridrow.get('bank_name')?.updateValueAndValidity();
  }
  onChangefloat(
    event: any,
    val: any,
    field_id: any,
    index: any,
    field_key: any
  ) {
    const event_value = event.target.value;
    if (
      this.screenName == 'Item Quality Parameter' &&
      field_key == 'max_quality_value'
    ) {
      const gridRow1 = (<FormArray>this.form.get('details_data')).at(index);
      const min_value = parseFloat(gridRow1.get('min_quality_value')?.value);
      if (min_value > event_value) {
        gridRow1.get('max_quality_value')?.setValue('');
        this.showSwalMassage(
          'Max Quality Value Should be greater than min val',
          'warning'
        );
      }
      const all_grid = <FormArray>this.form.get('details_data');
      if (all_grid.length > index + 1) {
        for (let ind = all_grid.length; ind > index; ind--) {
          const qc_type = all_grid.value;
          const nqc = qc_type[ind - 1]['quality_type'];
          const cqc_type = gridRow1.get('quality_type')?.value;
          if (nqc == cqc_type) {
            this.deleteRow11(ind - 1);
          }
        }
      }
    }
  }

  onResetForm() {
    const gridRow = (<FormArray>this.form.get('master_data')).at(0);
    const masterData = (<FormArray>this.form.get('id')).reset();
    gridRow.reset();
    this.BTN_VAL = 'Submit';
  }

  onCancelForm() {
    this.onResetForm();
    this.handleCancel.emit(false);
  }

  create_date_formate(date: any) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1),
      day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  create_from_date(today_date: any) {
    const new_date_time = today_date.setDate(today_date.getDate() + 1);
    const new_date = this.create_date_formate(new_date_time);
    return new_date;
  }

  checkForBillingAddress(formarraydata: any, form: any) {
    let isBillingAddressSelected = false;
    let isshippingAddressSelected = false;
    let status = true;
    for (let j = 0; j < formarraydata.length; j++) {
      if (formarraydata[j]['key'] === 'Billing') {
        const billingId = formarraydata[j]['id'];

        for (let l = 0; l < form.value.details_data.length; l++) {
          if (form.value.details_data[l]['address_type'] === billingId) {
            isBillingAddressSelected = true;
            break;
          }
        }
      } else if (formarraydata[j]['key'] === 'Shipping') {
        const billingId = formarraydata[j]['id'];

        for (let l = 0; l < form.value.details_data.length; l++) {
          if (form.value.details_data[l]['address_type'] === billingId) {
            isshippingAddressSelected = true;
            break;
          }
        }
      }
    }

    if (!isBillingAddressSelected) {
      status = false;
      Swal.fire('Select One Billing Address');
    }
    if (!isshippingAddressSelected) {
      status = false;
      Swal.fire('Select One Shipping Address');
    }
    return status;
  }

  onSubmit() {
    console.log(this.form);

    if (this.form.invalid) {
      if (this.formHierarchyDetailsaExist) {
        this.formarraydata.forEach((entry: any) => {
          const key = Object.keys(entry)[0];
          const value = entry[key];

          for (const k in entry[key]) {
            const fd1 = entry[key][k];
            const fc1: any = this.form.controls;
            for (let i = 0; i < fc1[key].controls.length; i++) {
              const md1 = fc1[key].controls[i].controls;
              if (md1[fd1.field_key].invalid) {
                if (fd1.field_key == 'attachment_path') {
                  Swal.fire('KYC Attachment Is Missing');
                }
                md1[fd1.field_key].markAsTouched();
              }
            }
          }
        });
      } else {
        for (const j in this.formarraydata) {
          const fd = this.formarraydata[j];
          const fc: any = this.form.controls;

          for (let i = 0; i < fc.details_data.controls.length; i++) {
            const md = fc.details_data.controls[i].controls;
            if (md[fd.field_key].invalid) {
              md[fd.field_key].markAsTouched();
            }
          }
        }
      }

      for (const j in this.formdata) {
        const fd = this.formdata[j];
        const fc: any = this.form.controls;
        const md = fc.master_data.controls[0].controls;
        if (md[fd.field_key].invalid) {
          if (
            fd.field_key == 'attachment' ||
            fd.field_key == 'bank_attachment_path'
          ) {
            Swal.fire('Attachment Is Missing');
          }
          md[fd.field_key].markAsTouched();
        }
      }
    } else {
      const payload = this.form.getRawValue();
      if (this.screenName == 'Farmer') {
        const gridRow = (<FormArray>this.form.get('master_data')).at(0);

        if (gridRow.value.dob != '' || gridRow.value.dob != null) {
          gridRow.value.dob = this.datepipe.transform(
            gridRow.value.dob,
            'yyyy-MM-dd'
          );
        }
        const gridRow1 = <FormArray>this.form.get('Farmer Family');
        for (let j = 0; j < gridRow1.length; j++) {
          if (
            gridRow1.at(j).value.dob != '' ||
            gridRow1.at(j).value.dob != null
          ) {
            gridRow1.at(j).value.dob = this.datepipe.transform(
              gridRow1.at(j).value.dob,
              'yyyy-MM-dd'
            );
          }
        }
        const fc1: any = this.form.controls;
        const key = 'Farmer Family';

        for (let i = 0; i < fc1[key].controls.length; i++) {
          const md1 = fc1[key].controls[i].controls;
          md1.address.enable();
          md1.pincode_details.enable();
        }
      }
      if (this.screenName == 'Item Pricing') {
        const gridRow = (<FormArray>this.form.get('master_data')).at(0);
        gridRow.value.from_date = this.datepipe.transform(
          gridRow.value.from_date,
          'yyyy-MM-dd'
        );
      }
      if (
        this.screenName == 'Item Quality Parameter' ||
        this.screenName == 'Item Rate Commision' ||
        this.screenName == 'Tax Rate' ||
        this.screenName == 'With Holding'
      ) {
        const val = this.datepipe.transform(
          payload.master_data[0]['from_date'],
          'yyyy-MM-dd'
        );
        payload.master_data[0]['from_date'] = val;
        if(this.screenName != 'Item Rate Commision'){
        let d1 = (payload.master_data[0]['to_date']).toString()
        const parts = d1.split('-');
        const formattedDate = new Date(`${parts[2]}-${parts[1]}-${parts[0]}`);
        const formattedDateStr = this.datepipe.transform(formattedDate, 'yyyy-MM-dd');
        payload.master_data[0]['to_date'] = formattedDateStr
        }

      }

      if (this.screenName == 'Item Rate Commision') {
        if (this.roleName == 'FPC Admin') {
          const fc1: any = this.form.controls;
          const key = 'master_data';

          for (let i = 0; i < fc1[key].controls.length; i++) {
            const md1 = fc1[key].controls[i].controls;
            md1.pricing_type_ref_id.enable();
            //  md1.pincode_details.enable()
          }
          // let gridrow = (<FormArray>(this.form.get('master_data'))).at(0)
          // gridrow.get('pricing_type_ref_id')?.enable()
        }
      }
      let result = true;
      if (this.formDatDetailsaExist) {
        for (const j in this.formarraydata) {
          if (this.formarraydata[j]['field_key'] == 'address_type') {
            result = this.checkForBillingAddress(
              this.formarraydata[j]['dropdown_raw_query'],
              this.form
            );
          }
        }
      }

      if (this.formDatDetailsaExist) {
        if (result == true) {
          if (
            (this.screenName == 'Tax Rate' ||
              this.screenName == 'Item Quality Parameter' ||
              this.screenName == 'With Holding') &&
            this.BTN_VAL == 'Create Version'
          ) {
            const currtDate = new Date().toJSON().split('T')[0];
            const gridRow = (<FormArray>this.form.get('master_data')).at(0);

            if (gridRow.get('from_date')?.value == currtDate) {
              Swal.fire({
                title: 'Not allowed',
                text: 'Cannot create version on same day!!',
                icon: 'warning',
              });
            } else {
              this.handleSave.emit(payload);
            }
          } else {
            this.handleSave.emit(payload);
          }
        }
      } else {
        this.handleSave.emit(payload);
      }
    }
  }

  masterdata() {
    this.fieldsCtrls3 = this.getFormControls(this.formdata);
    return new FormGroup(this.fieldsCtrls3);
  }
  initialitemRow(value: any) {
    this.fieldsCtrls1 = this.getFormControls(value);
    return new FormGroup(this.fieldsCtrls1);
  }

  initialitemRow11() {
    this.fieldsCtrls1 = this.getFormControls(this.formarraydata);
    return new FormGroup(this.fieldsCtrls1);
  }

  detail_array_form() {
    for (const data in this.detail_array) {
      for (const data1 in this.detail_array[data]) {
        this.form_detail_array.push({
          data1: this.formBuilder.array([this.initialitemRow('')]),
        });
      }
    }
    return new FormGroup(this.form_detail_array);
  }

  initialitemRow1() {
    this.fieldsCtrls2 = this.getFormControls(this.formarraysubdata);
    return new FormGroup(this.fieldsCtrls2);
  }

  get formArr() {
    return this.form.get('details_data') as FormArray;
  }

  formArr11(key: any) {
    return this.form.get(key) as FormArray;
  }

  get formArr1() {
    return this.form.get('sub_details_data') as FormArray;
  }

  addNewRow(key: any, json_form: any, j: number) {
    this.formarraydata.filter((entry: any) => {
      const key1 = Object.keys(entry)[0];
      if (key1 == key) {
        const value = entry[key];
        if (!this.fieldsCtrls[key]) {
          this.fieldsCtrls[key] = this.formBuilder.array([
            this.initialitemRow(value),
          ]);
        } else {
          this.fieldsCtrls[key].push(this.initialitemRow(value));
        }
      }
    });
    this.showgridadd = true;
    this.newArray.push(this.formarraydata);
  }
  addNewRow1() {
    this.formArr1.push(this.initialitemRow1());
  }

  addNewRow11() {
    this.details_length = (<FormArray>this.form.get('details_data')).length;
    this.count_add = this.count_add + 1;

    this.showgridadd = true;
    this.formArr.push(this.initialitemRow11());

    this.newArray.push(this.formarraydata);
    for (const j in this.newArray[this.count_add]) {
      this.newArray[this.count_add][j][
        'dropdown_raw_query_' + String(this.count_add)
      ] = this.newArray[this.count_add][j]['dropdown_raw_query'];
      if (this.newArray[this.count_add][j]['field_type_id'] == 'dropdown') {
        this.newArray[this.count_add][j][
          'filter_dropdown_raw_query_' + String(this.count_add)
        ] = this.newArray[this.count_add][j]['dropdown_raw_query'].slice();
      }
    }
    if (this.screenName == 'Item Quality Parameter') {
      this.onGridToggleChange(null, null, null);
    }
  }

  deleteRow11(index: any) {
    if (this.formArr.length == 1) {
      return false;
    } else if (
      this.screenName == 'Process Cost Head Link' ||
      this.screenName == 'Link Sub Department Segment'
    ) {
      this.formArr.removeAt(index);
      this.processGridRow(index);
      this.details_length =
        (<FormArray>this.form.get('details_data')).length - 1;
      return true;
    } else {
      this.count_add = this.count_add - 1;
      this.formArr.removeAt(index);
      this.details_length =
        (<FormArray>this.form.get('details_data')).length - 1;
      return true;
    }
  }

  processGridRow(index: any) {
    const length = (<FormArray>this.form.get('details_data')).length;
    for (index; index < length; index++) {
      const gridRow = (<FormArray>this.form.get('details_data')).at(index);
      gridRow.get('sequence_number')?.setValue(index + 1);
    }
  }

  deleteRow(index: any, key: any, json_form: any, j: number) {
    this.fieldsCtrls[key].removeAt(index);
    return true;
  }

  deleteRow1(index: any) {
    if (this.formArr1.length == 1) {
      return false;
    } else {
      this.formArr1.removeAt(index);
      return true;
    }
  }

  getFormControls(formdata1: any) {
    const fieldsCtrls2: any = {};
    for (const f of formdata1) {
      if (
        f.field_type_id != 'checkbox' &&
        f.field_type_id != 'boolean' &&
        f.field_type_id != 'float'
      ) {
        if (f.is_required == true) {
          if (f.field_key == 'tenant_id') {
            fieldsCtrls2[f.field_key] = new FormControl(
              this.COMPANY_ID,
              Validators.required
            );
          } else if (f.field_key == 'company_id') {
            fieldsCtrls2[f.field_key] = new FormControl(
              this.COMPANY_ID,
              Validators.required
            );
          } else if (f.field_key == 'unique_id') {
            fieldsCtrls2[f.field_key] = new FormControl(0, Validators.required);
          }

          // else if(f.field_key == 'phone' || f.field_key == 'mobile_no' || f.field_key == 'phone_farmer' ){
          //   fieldsCtrls2[f.field_key] = new FormControl(f.value || '',[Validators.required,Validators.maxLength(10),Validators.pattern("^[0-9_]*$")])//,Validators.maxLength(10),Validators.pattern("^[0-9_]*$")])
          // }
          else if (
            f.field_key == 'pincode' ||
            f.field_key == 'pin_code' ||
            f.field_key == 'pincode_details'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.maxLength(6),
              Validators.pattern('^[0-9_]*$'),
            ]);
          } else if (f.field_key == 'email_id' || f.field_key == 'email') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.email,
            ]);
          } else if (f.field_key == 'gstin' || f.field_key == 'gstin_details') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.pattern(
                '[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}'
              ),
            ]);
          } else if (
            f.field_key == 'bank_ifsc_code' ||
            f.field_key == 'ifsc_code'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.pattern('[A-Z]{4}[0]{1}[0-9A-Z]{6}'),
            ]);
          }

          // else if (f.field_key == 'bank_account_no' || f.field_key == 'account_number') {
          //   fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [Validators.required, Validators.pattern("[^-,@,!,#,$,%,^,&,*,(,)]+")])
          // }
          else if (f.field_key == 'pan_no') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}'),
            ]);
          } else if (
            f.field_key == 'bank_branch_code' ||
            f.field_key == 'branch_code'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.pattern('[^-,@,!,#,$,%,^,&,*,(,)]+'),
            ]);
          } else if (
            f.field_key == 'account_holder_name' ||
            f.field_key == 'bank_account_holder_name'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.required,
              Validators.pattern('[^-,@,!,#,$,%,^,&,*,(,)]+'),
            ]);
          } else if (f.field_key == 'supplier_status') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Active') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id'],
                  Validators.required
                );
              }
            }
          } else if (f.field_key == 'customer_status') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Active') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id'],
                  Validators.required
                );
              }
            }
          } else if (f.field_key == 'po_type_ref_id') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Domestic') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id'],
                  Validators.required
                );
              }
            }
          }
          // if(this.roleName=='FPC Admin'){
          if (f.field_key == 'pricing_type_ref_id') {
            {
              for (let i = 0; i < f.dropdown_raw_query.length; i++) {
                if (f.dropdown_raw_query[i]['key'] == 'Common') {
                  fieldsCtrls2[f.field_key] = new FormControl(
                    f.dropdown_raw_query[i]['id'],
                    Validators.required
                  );
                }
              }
            }
          } else {
            fieldsCtrls2[f.field_key] = new FormControl(
              f.value || '',
              Validators.required
            );
          }
        } else {
          if (f.field_key == 'tenant_id') {
            fieldsCtrls2[f.field_key] = new FormControl(this.COMPANY_ID);
          } else if (f.field_key == 'company_id') {
            fieldsCtrls2[f.field_key] = new FormControl(this.COMPANY_ID);
          } else if (f.field_key == 'mobile_no' || f.field_key == 'phone') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.maxLength(10),
              Validators.pattern('^[0-9_]*$'),
            ]);
          } else if (
            f.field_key == 'pin_code' ||
            f.field_key == 'pincode' ||
            f.field_key == 'pincode_details'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.maxLength(6),
              Validators.pattern('^[0-9_]*$'),
            ]);
          } else if (f.field_key == 'email_id' || f.field_key == 'email') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.email,
            ]);
          } else if (f.field_key == 'gstin' || f.field_key == 'gstin_details') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.pattern(
                '[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}'
              ),
            ]);
          } else if (
            f.field_key == 'ifsc_code' ||
            f.field_key == 'bank_ifsc_code'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.pattern('[A-Z]{4}[0]{1}[0-9A-Z]{6}'),
            ]);
          } else if (f.field_key == 'pan_no') {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}'),
            ]);
          } else if (
            f.field_key == 'bank_branch_code' ||
            f.field_key == 'branch_code'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.pattern('[^-,@,!,#,$,%,^,&,*,(,)]+'),
            ]); //,Validators.pattern("[^-,@,!,#,$,%,^,&,*,(,)]+")])
          } else if (
            f.field_key == 'bank_account_holder_name' ||
            f.field_key == 'account_holder_name'
          ) {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '', [
              Validators.pattern('[^-,@,!,#,$,%,^,&,*,(,)]+'),
            ]);
          } else if (f.field_key == 'supplier_status') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Active') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id']
                );
              }
            }
          } else if (f.field_key == 'customer_status') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Active') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id']
                );
              }
            }
          } else if (f.field_key == 'po_type_ref_id') {
            for (let i = 0; i < f.dropdown_raw_query.length; i++) {
              if (f.dropdown_raw_query[i]['key'] == 'Domestic') {
                fieldsCtrls2[f.field_key] = new FormControl(
                  f.dropdown_raw_query[i]['id']
                );
              }
            }
          } else if (f.field_key == 'pricing_type_ref_id') {
            {
              for (let i = 0; i < f.dropdown_raw_query.length; i++) {
                if (f.dropdown_raw_query[i]['key'] == 'Common') {
                  fieldsCtrls2[f.field_key] = new FormControl(
                    f.dropdown_raw_query[i]['id']
                  );
                }
              }
            }
          } else {
            fieldsCtrls2[f.field_key] = new FormControl(f.value || '');
          }
        }
      } else if (f.field_type_id == 'boolean') {
        if (f.field_key == 'is_active') {
          fieldsCtrls2[f.field_key] = new FormControl(true);
        } else if (f.field_key == 'is_gst_set_off') {
          fieldsCtrls2[f.field_key] = new FormControl(true);
        } else {
          fieldsCtrls2[f.field_key] = new FormControl(false);
        }
      } else if (f.field_type_id == 'float') {
        if (
          f.field_key == 'max_quality_value' ||
          f.field_key == 'base_uom_conversion' ||
          f.field_key == 'item_rate' ||
          f.field_key == 'multiplier'
        ) {
          fieldsCtrls2[f.field_key] = new FormControl('', Validators.required);
        } else {
          fieldsCtrls2[f.field_key] = new FormControl(0, Validators.required);
        }
      } else {
        const opts: any = {};
        for (const opt of f.options) {
          opts[opt.key] = new FormControl(opt.key);
        }
        fieldsCtrls2[f.key] = new FormGroup(opts);
      }
    }
    fieldsCtrls2['id'] = new FormControl('');
    fieldsCtrls2['created_by'] = new FormControl('');
    fieldsCtrls2['updated_by'] = new FormControl('');
    fieldsCtrls2['is_deleted'] = new FormControl('');
    fieldsCtrls2['created_date_time'] = new FormControl('');
    fieldsCtrls2['updated_date_time'] = new FormControl('');

    return fieldsCtrls2;
  }

  downloadTemplate() {
    const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    const mainSheet = XLSX.utils.aoa_to_sheet([this.columnNames]);
    XLSX.utils.book_append_sheet(workBook, mainSheet, 'Main');
    const metaSheet = XLSX.utils.aoa_to_sheet([
      ['Do not touch this sheet'],
      ['Screen Name', this.screenName],
    ]);
    XLSX.utils.book_append_sheet(workBook, metaSheet, 'META');
    XLSX.writeFile(workBook, `${this.screenName} Template.xlsx`);
  }

  uploadTemplate(e: any) {
    const target: DataTransfer = <DataTransfer>e.target;
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(target.files[0]);
    reader.onload = (e: any) => {
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      const data1 = XLSX.utils.sheet_to_json(ws);
      const formdata = { mst_data: this.form.value, xlsx_data: data1 };

      this.commonsetupservice.validateStepDetailsData(formdata).subscribe(
        (data: any) => {
          if (data['msg']) {
            Swal.fire(data['msg']);
          } else {
            this.showLoaderupload = false;
            const dict1: any = [];
            for (let idx = 0; idx < data.length; idx++) {
              dict1.push({
                item_ref_id: data[idx]['item_ref_id'],
                item_rate: data[idx]['Item Rate'],
                uom: data[idx]['uom_ref_id'],
              });
            }
            this.form.setControl(
              'details_data',
              this.setExistingArray(dict1, 'details_data')
            );
          }
        },
        (error) => {}
      );
    };
  }

  seeDetails(employee: any) {
    this.selectedEmployee = { ...employee };
    this.BTN_VAL = 'Update';
    const gridRow = (<FormArray>this.form.get('master_data')).at(0);
    this.commonsetupservice
      .getScreennDataById(this.selectedEmployee['id'], this.screenName)
      .subscribe({
        next: (data: any) => {
          this.form.get('id')?.setValue(data['id']);
          for (const val in data.master_data) {
            gridRow.get(val)?.setValue(data['master_data'][val]);
            if (
              this.BTN_VAL == 'Update' &&
              gridRow.get('revision_status')?.value == 'Effective'
            ) {
              gridRow.disable();
            } else if (
              this.BTN_VAL == 'Update' &&
              gridRow.get('revision_status')?.value == 'Future'
            ) {
              gridRow.enable();
            }
          }
        },
      });
  }
}
