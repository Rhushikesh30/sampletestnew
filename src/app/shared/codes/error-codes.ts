export class ErrorCodes {
    errorMsg: any
    getErrorMessage(code: any) {
        console.log(code);

        if (code == 400) {
            this.errorMsg = 'Bad request'
        }
        if (code == 401) {
            this.errorMsg = "Authentication credentials were not provided";
        }
        if (code == 403) {
            this.errorMsg = "You do not have permission";
        }
        if (code == 404) {
            this.errorMsg = "Records not found";
        }
        if (code == 405) {
            this.errorMsg = "Requested method not allowed";
        }
        if (code == 406) {
            this.errorMsg = "Not acceptable";
        }
        if (code == 409) {
            this.errorMsg = "Already Exists";
        }
        if (code == 500) {
            this.errorMsg = "Internal Server error";
        }
        if (code == 0) {
            this.errorMsg = "Server Down!!! Please try again after some time";
        }
        if (code == 208) {
            this.errorMsg = "Already Reported";
        }
        if(code == 421){
            this.errorMsg = "Invalid Session Key"
        }
        return this.errorMsg;
    }
}