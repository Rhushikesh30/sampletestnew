import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockAdjestmentRoutingModule } from './stock-adjestment-routing.module';
import { StockAdjestmentComponent } from './stock-adjestment/stock-adjestment.component';
import { StockAdjestmentDetailsComponent } from './stock-adjestment/stock-adjestment-details/stock-adjestment-details.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';


@NgModule({
  declarations: [
    StockAdjestmentComponent,
    StockAdjestmentDetailsComponent
  ],
  imports: [
    CommonModule,
    StockAdjestmentRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatIconModule,
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSelectFilterModule,
    MatTableModule
  ]
})
export class StockAdjestmentModule { }
