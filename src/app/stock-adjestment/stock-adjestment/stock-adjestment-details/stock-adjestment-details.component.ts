import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormArray,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { StockAdjestmentService } from 'src/app/shared/services/stock-adjestment.service';
import Swal from 'sweetalert2';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-stock-adjestment-details',
  templateUrl: './stock-adjestment-details.component.html',
  styleUrls: ['./stock-adjestment-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class StockAdjestmentDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() screenName!: string;

  stockAdjForm!: UntypedFormGroup;
  btnVal = 'Submit';
  showReset = true;

  todayDate = new Date().toJSON().split('T')[0];
  itenTypeData: any = [];
  countryData: any = [];
  txn_currency: any;
  itemData: any = [];
  uomData: any = [];
  is_rate_qty: any = [];
  viewBtn = true;
  myTemplate = '';
  constructor(
    private formBuilder: UntypedFormBuilder,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private stockAdjestmentService: StockAdjestmentService
  ) {}

  ngOnInit(): void {
    console.log(this.todayDate);
    this.initializeForm();
    this.getAlldata();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    }
  }

  initializeForm() {
    this.stockAdjForm = this.formBuilder.group({
      id: [''],
      transaction_date: [new Date(), [Validators.required]],
      fiscal_year: ['', [Validators.required]],
      period: ['', [Validators.required]],
      item_type_ref_id: ['', Validators.required],
      txn_currency: ['', [Validators.required]],
      base_currency: ['', [Validators.required]],
      conversion_rate: [1],
      txn_currency_amount: [0, [Validators.required]],
      base_currency_amount: [0, [Validators.required]],
      stock_adjestment_details: this.formBuilder.array([
        this.stock_adjestment_details(),
      ]),
    });
  }

  stock_adjestment_details() {
    return this.formBuilder.group({
      id: [''],
      is_qty_rate_adjustment: [true],
      item_ref_id: [''],
      uom: [''],
      quantity: [''],
      physical_uom: [''],
      physical_quantity: [''],
      surplus_shortage: [''],
      damaged_uom: [''],
      damaged_quantity: [''],
      request_for: [''],
      txn_currency: ['', [Validators.required]],
      base_currency: ['', [Validators.required]],
      conversion_rate: ['', [Validators.required]],
      system_rate: ['', [Validators.required]],
      txn_currency_system_amount: ['', [Validators.required]],
      base_currency_system_amount: ['', [Validators.required]],
      changed_rate: ['', [Validators.required]],
      txn_currency_changed_amount: ['', [Validators.required]],
      base_currency_changed_amount: ['', [Validators.required]],
      difference_in_rate: ['', [Validators.required]],
      txn_currency_difference_in_amount: ['', [Validators.required]],
      base_currency_difference_in_amount: ['', [Validators.required]],
      transaction_type: ['', [Validators.required]],
    });
  }

  getAlldata() {
    this.stockAdjestmentService.getMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.itenTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.stockAdjestmentService.getCountryData().subscribe({
      next: (data: any) => {
        this.countryData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.stockAdjForm
          .get('txn_currency')
          ?.setValue(this.txn_currency[0]['id']);
        this.stockAdjForm
          .get('base_currency')
          ?.setValue(this.txn_currency[0]['id']);
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.stockAdjestmentService.getFiscalYearAndPeriod().subscribe({
      next: (data: any) => {
        this.stockAdjForm.get('fiscal_year')?.setValue(data['fiscal_year']);
        this.stockAdjForm.get('period')?.setValue(data['period']);
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.stockAdjestmentService
      .getDynamicData('get_dynamic_data', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.itemData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.stockAdjestmentService
      .getDynamicData('get_dynamic_data', 'uom', 'uom_code')
      .subscribe({
        next: (data: any) => {
          this.uomData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  viewRecord(row: any) {
    console.log(row);
    this.stockAdjForm.patchValue({
      id: row.id,
      transaction_date: row.transaction_date,
      item_type_ref_id: row.item_type_ref_id,
      txn_currency: row.txn_currency,
      base_currency: row.base_currency,
      conversion_rate: row.conversion_rate,
      txn_currency_amount: parseFloat(row.txn_currency_amount).toFixed(2),
      base_currency_amount: parseFloat(row.base_currency_amount).toFixed(2),
      created_by: row.created_by,
      tenant_id: row.tenant_id,
    });

    const contractItemRow = row.stock_adjestment_details.filter(function (
      data: any
    ) {
      return data;
    });
    if (contractItemRow.length >= 1) {
      this.stockAdjForm.setControl(
        'stock_adjestment_details',
        this.setExistingArray(contractItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';

      this.stockAdjForm.get('item_type_ref_id')?.disable();
      this.stockAdjForm.get('txn_currency')?.disable();
      this.stockAdjForm.get('transaction_date')?.disable();

      this.stockAdjForm.value.stock_adjestment_details.forEach(
        (ele: any, ind: number) => {
          const gridRow = (<FormArray>(
            this.stockAdjForm.get('stock_adjestment_details')
          )).at(ind);
          gridRow.get('item_ref_id')?.disable();
          gridRow.get('uom')?.disable();
          gridRow.get('physical_uom')?.disable();
        }
      );
    } else {
      this.stockAdjForm.disable();
    }
  }

  setExistingArray(initialArray: any): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, ind: number) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          is_qty_rate_adjustment: element.is_qty_rate_adjustment,
          item_ref_id: element.item_ref_id,
          uom: element.uom,
          quantity: element.quantity,
          physical_uom: element.physical_uom,
          physical_quantity: element.physical_quantity,
          surplus_shortage: element.surplus_shortage,
          conversion_rate: element.conversion_rate,
          damaged_uom: element.damaged_uom,
          damaged_quantity: element.damaged_quantity,
          request_for: element.request_for,
          txn_currency: element.txn_currency,
          base_currency: element.base_currency,
          system_rate: parseFloat(element.system_rate).toFixed(2),
          txn_currency_system_amount: parseFloat(
            element.txn_currency_system_amount
          ).toFixed(2),
          changed_rate: parseFloat(element.changed_rate).toFixed(2),
          txn_currency_changed_amount: parseFloat(
            element.txn_currency_changed_amount
          ).toFixed(2),
          difference_in_rate: element.difference_in_rate,
          txn_currency_difference_in_amount:
            element.txn_currency_difference_in_amount,
          transaction_type: element.created_bytransaction_type,
          created_by: element.created_by,
          tenant_id: element.tenant_id,
          base_currency_changed_amount: element.base_currency_changed_amount,
          base_currency_system_amount: element.base_currency_system_amount,
          base_currency_difference_in_amount:
            element.base_currency_difference_in_amount,
        })
      );

      this.is_rate_qty.push(element.is_qty_rate_adjustment);
    });
    return formArray;
  }

  setArray(initialArray: any): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, ind: number) => {
      const qty =
        parseFloat(element.quantity) -
        parseFloat(element.physical_qty_shop_floor);
      const amt = parseFloat(element.quantity) * parseFloat(element.rate);
      let request_for, transaction_type;
      if (qty < 0) {
        request_for = 'Stock Down';
      } else {
        request_for = 'Stock Up';
      }
      if (
        parseFloat(element.physical_qty_shop_floor) >
        parseFloat(element.quantity)
      ) {
        transaction_type = 'Purchase';
      } else {
        transaction_type = 'Issue';
      }
      formArray.push(
        this.formBuilder.group({
          id: '',
          is_qty_rate_adjustment: true,
          item_ref_id: element.item_ref_id,
          uom: element.uom,
          quantity: element.quantity,
          physical_uom: element.uom,
          physical_quantity: element.physical_qty_shop_floor,
          surplus_shortage:
            parseFloat(element.quantity) -
            parseFloat(element.physical_qty_shop_floor),
          conversion_rate: 1,
          damaged_uom: 0,
          damaged_quantity: 0,
          request_for: request_for,
          txn_currency: this.txn_currency[0]['id'],
          base_currency: this.txn_currency[0]['id'],
          system_rate: parseFloat(element.rate).toFixed(2),
          txn_currency_system_amount: (
            parseFloat(element.quantity) * parseFloat(element.rate)
          ).toFixed(2),
          changed_rate: parseFloat(element.rate).toFixed(2),
          txn_currency_changed_amount: (
            parseFloat(element.physical_qty_shop_floor) *
            parseFloat(element.rate)
          ).toFixed(2),
          difference_in_rate: (
            parseFloat(element.rate) - parseFloat(element.rate)
          ).toFixed(2),
          txn_currency_difference_in_amount: (amt - amt).toFixed(2),
          transaction_type: transaction_type,
          created_by: element.created_by,
          tenant_id: element.tenant_id,
          base_currency_changed_amount: 0,
          base_currency_system_amount: 0,
          base_currency_difference_in_amount: 0,
        })
      );
      this.is_rate_qty.push(true);
    });
    return formArray;
  }

  onCancelForm() {
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  get formArr() {
    return this.stockAdjForm.get(
      'stock_adjestment_details'
    ) as UntypedFormArray;
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArr.removeAt(index);
      return true;
    }
  }

  addNewRow() {
    this.formArr.push(this.stock_adjestment_details());
  }

  itemTypeChange(event: any) {
    console.log(event);
    this.stockAdjestmentService.getStockDataByItem(event).subscribe({
      next: (data: any) => {
        console.log(data);
        this.stockAdjForm.setControl(
          'stock_adjestment_details',
          this.setArray(data)
        );

        this.stockAdjForm.value.stock_adjestment_details.forEach(
          (ele: any, ind: number) => {
            const gridRow = (<FormArray>(
              this.stockAdjForm.get('stock_adjestment_details')
            )).at(ind);
            console.log(gridRow);
            const diff_amt = gridRow.get(
              'txn_currency_difference_in_amount'
            )?.value;
            const sys_amt = gridRow.get('txn_currency_system_amount')?.value;
            const change_amt = gridRow.get(
              'txn_currency_changed_amount'
            )?.value;
            const conversion_rate = gridRow.get('conversion_rate')?.value;
            gridRow
              .get('base_currency_difference_in_amount')
              ?.setValue(parseFloat(diff_amt) * conversion_rate);
            gridRow
              .get('base_currency_system_amount')
              ?.setValue(parseFloat(sys_amt) * conversion_rate);
            gridRow
              .get('base_currency_changed_amount')
              ?.setValue(parseFloat(change_amt) * conversion_rate);

            const amt =
              parseFloat(this.stockAdjForm.get('txn_currency_amount')?.value) +
              parseFloat(diff_amt);
            this.stockAdjForm
              .get('txn_currency_amount')
              ?.setValue(amt.toFixed(2));
            this.stockAdjForm
              .get('base_currency_amount')
              ?.setValue((amt * ele.conversion_rate).toFixed(2));

            gridRow.get('item_ref_id')?.disable();
            gridRow.get('uom')?.disable();
            gridRow.get('physical_uom')?.disable();
          }
        );
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  qtyRateChange(event: any, j: number) {
    this.is_rate_qty[j] = event.target.checked;
    const gridRow = (<FormArray>(
      this.stockAdjForm.get('stock_adjestment_details')
    )).at(j);
    if (event.target.checked) {
      if (
        parseFloat(gridRow.get('physical_quantity')?.value) >
        parseFloat(gridRow.get('quantity')?.value)
      ) {
        gridRow.get('transaction_type')?.setValue('Purchase');
      } else {
        gridRow.get('transaction_type')?.setValue('Issue');
      }
    } else {
      if (
        parseFloat(gridRow.get('changed_rate')?.value) >
        parseFloat(gridRow.get('system_rate')?.value)
      ) {
        gridRow.get('transaction_type')?.setValue('Sales');
      } else {
        gridRow.get('transaction_type')?.setValue('Issue');
      }
    }
  }

  changeQty() {
    this.stockAdjForm.get('txn_currency_amount')?.setValue(0);
    this.stockAdjForm.value.stock_adjestment_details.forEach(
      (ele: any, ind: number) => {
        const gridRow = (<FormArray>(
          this.stockAdjForm.get('stock_adjestment_details')
        )).at(ind);
        ele.surplus_shortage =
          parseFloat(ele.quantity) - parseFloat(ele.physical_quantity);
        ele.txn_currency_changed_amount =
          parseFloat(ele.physical_quantity) * parseFloat(ele.changed_rate);
        gridRow
          .get('surplus_shortage')
          ?.setValue(ele.surplus_shortage.toFixed(2));
        gridRow
          .get('txn_currency_changed_amount')
          ?.setValue(ele.txn_currency_changed_amount.toFixed(2));
        if (ele.is_qty_rate_adjustment) {
          if (parseFloat(ele.physical_quantity) > parseFloat(ele.quantity)) {
            gridRow.get('transaction_type')?.setValue('Purchase');
          } else {
            gridRow.get('transaction_type')?.setValue('Issue');
          }
        } else {
          if (parseFloat(ele.changed_rate) > parseFloat(ele.system_rate)) {
            gridRow.get('transaction_type')?.setValue('Sales');
          } else {
            gridRow.get('transaction_type')?.setValue('Issue');
          }
        }

        if (parseFloat(ele.surplus_shortage) < 0) {
          gridRow.get('request_for')?.setValue('Stock Down');
        } else {
          gridRow.get('request_for')?.setValue('Stock Up');
        }

        ele.txn_currency_difference_in_amount =
          parseFloat(ele.txn_currency_system_amount) -
          parseFloat(ele.txn_currency_changed_amount);
        gridRow
          .get('txn_currency_difference_in_amount')
          ?.setValue(ele.txn_currency_difference_in_amount.toFixed(2));
        ele.base_currency_difference_in_amount =
          parseFloat(ele.txn_currency_difference_in_amount) *
          ele.conversion_rate;
        gridRow
          .get('base_currency_difference_in_amount')
          ?.setValue(ele.base_currency_difference_in_amount.toFixed(4));
        ele.base_currency_system_amount =
          parseFloat(ele.txn_currency_system_amount) * ele.conversion_rate;
        gridRow
          .get('base_currency_system_amount')
          ?.setValue(ele.base_currency_system_amount.toFixed(4));
        ele.base_currency_changed_amount =
          parseFloat(ele.txn_currency_changed_amount) * ele.conversion_rate;
        gridRow
          .get('base_currency_changed_amount')
          ?.setValue(ele.base_currency_changed_amount.toFixed(4));

        const amt =
          parseFloat(this.stockAdjForm.get('txn_currency_amount')?.value) +
          parseFloat(ele.txn_currency_difference_in_amount);
        this.stockAdjForm.get('txn_currency_amount')?.setValue(amt.toFixed(2));
        this.stockAdjForm
          .get('base_currency_amount')
          ?.setValue((amt * ele.conversion_rate).toFixed(2));
      }
    );
  }

  onSubmit() {
    console.log(this.stockAdjForm);
    if (this.stockAdjForm.invalid) {
      return;
    } else {
      this.showLoader = true;
      const payload = this.stockAdjForm.getRawValue();

      payload.transaction_date = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );

      this.handleSave.emit(payload);
    }
  }
}
