import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAdjestmentDetailsComponent } from './stock-adjestment-details.component';

describe('StockAdjestmentDetailsComponent', () => {
  let component: StockAdjestmentDetailsComponent;
  let fixture: ComponentFixture<StockAdjestmentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockAdjestmentDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StockAdjestmentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
