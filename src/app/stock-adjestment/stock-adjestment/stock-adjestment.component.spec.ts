import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAdjestmentComponent } from './stock-adjestment.component';

describe('StockAdjestmentComponent', () => {
  let component: StockAdjestmentComponent;
  let fixture: ComponentFixture<StockAdjestmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockAdjestmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StockAdjestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
