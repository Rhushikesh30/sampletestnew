import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockAdjestmentComponent } from './stock-adjestment/stock-adjestment.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: StockAdjestmentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockAdjestmentRoutingModule { }
