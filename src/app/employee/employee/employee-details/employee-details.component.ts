import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss'],
  providers: [ErrorCodes],
})
export class EmployeeDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  cancelFlag = true;
  userForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  designationData: any = [];
  makerCheckerData: any = [];
  departmentData: any = [];
  subDepartmentData: any = [];
  divMessages = false;
  screenName = 'Employee';

  constructor(
    private formBuilder: FormBuilder,
    private fpcService: FpcSetupService,
    private errorCodes: ErrorCodes,
    private employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewEditRecord(this.rowData);
    }

    this.employeeService.getDynamicData('designation').subscribe({
      next: (data: any) => {
        this.designationData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.employeeService.getDynamicData('department').subscribe({
      next: (data: any) => {
        this.departmentData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.employeeService.getDynamicData('sub_department').subscribe({
      next: (data: any) => {
        this.subDepartmentData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Maker/Checker').subscribe({
      next: (data: any) => {
        this.makerCheckerData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.userForm = this.formBuilder.group({
      id: [''],
      maker_checker: ['', [Validators.required]],
      designation: [0],
      department: [0],
      sub_department: [0],
      emp_name: [
        '',
        [
          Validators.required,
          Validators.maxLength(255),
          Validators.pattern('^[a-zA-Z_ ]*$'),
        ],
      ],
      phone: [
        '',
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.pattern('[1-9]{1}[0-9]{9}'),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.maxLength(355),
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$'),
        ],
      ],
      is_active: [true],
      is_deleted: [false],
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.userForm.reset();
      this.handleCancel.emit(false);
    }
  }

  viewEditRecord(row: any) {
    this.userForm.patchValue({
      id: row.id,
      maker_checker: row.maker_checker ? row.maker_checker : '',
      emp_name: row.emp_name,
      email: row.email,
      phone: row.phone,
      designation: row.designation,
      department: row.department,
      sub_department: row.sub_department,
      is_active: row.is_active,
      is_deleted: row.is_deleted,
    });
    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.userForm.disable();
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.userForm.reset();
    this.handleCancel.emit(false);
  }

  onSubmit(value: any) {
    if (this.userForm.invalid) {
      return;
    } else {
      this.handleSave.emit(this.userForm.value);
    }
  }

  onResetForm() {
    this.initializeForm();
  }

  changeStatus(event: any) {
    if (event.checked) {
      this.userForm.get('is_deleted')?.setValue(false);
    } else {
      this.userForm.get('is_deleted')?.setValue(true);
    }
  }
}
