import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { ThemePalette } from '@angular/material/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { EmployeeService } from 'src/app/shared/services/employee.service';

import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { STATUS, getStatusColor } from 'src/app/constants/constants';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  providers: [ErrorCodes],
})
export class EmployeeComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  displayedColumns = [
    'View',
    'edit',
    'emp_name',
    'designation',
    'department',
    'phone',
    'role_name',
    'active',
  ];
  renderedData: any = [];
  screenName = 'Employee';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;

  exampleDatabase?: EmployeeService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  constructor(
    private roleSecurityService: RoleSecurityService,
    private employeeService: EmployeeService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e: any) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }
  getColorStatus(status: string): string {
    return getStatusColor(status);
  }
  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    if (this.dataSource) {
      for (const res in this.dataSource.filteredData) {
        if (
          formValue.id &&
          this.dataSource.filteredData[res].id != formValue.id &&
          this.dataSource.filteredData[res].phone == formValue.phone.toString()
        ) {
          this.showSwalmessage(
            'Record Already Exist!',
            'Phone Number should be unique',
            'warning',
            false
          );
          return;
        }
        if (
          !formValue.id &&
          this.dataSource.filteredData[res].phone == formValue.phone
        ) {
          this.showSwalmessage(
            'Record Already Exist!',
            'Phone Number should be unique',
            'warning',
            false
          );
          return;
        }
        if (
          formValue.id &&
          this.dataSource.filteredData[res].id != formValue.id &&
          this.dataSource.filteredData[res].email == formValue.email.toString()
        ) {
          this.showSwalmessage(
            'Email Already Exist!',
            'Email should be unique',
            'warning',
            false
          );
          return;
        }
        if (
          !formValue.id &&
          this.dataSource.filteredData[res].email == formValue.email
        ) {
          this.showSwalmessage(
            'Email Already Exist!',
            'Email should be unique',
            'warning',
            false
          );
          return;
        }
      }
    }
    this.showLoader = true;

    const json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.employeeService.createEmployee(json_data, formValue.id).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 2) {
          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e: any) => {
        this.showLoader = false;
        console.log(e);
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new EmployeeService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        // 'Employee Id': x.unique_id,
        'Employee Name': x.emp_name,
        Designation: x.designation_name,
        Department: x.department_name,
        'Mobile Number': x.phone,
        'Maker/Checker': x.role_name,
        Status: x.active,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  divMessages: any;
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: EmployeeService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['active'] = 'Inactive';
          }
        }
        // if (this.exampleDatabase.data[i]['mat_header_cell'] == 'Is Active') {
        //   this.exampleDatabase.data[i]['mat_header_cell'] = 'Status';
        // }

        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.unique_id +
              advanceTable.emp_name +
              advanceTable.designation_name +
              advanceTable.department_name +
              advanceTable.phone +
              advanceTable.role_name +
              advanceTable.is_active
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'unique_id':
          [propertyA, propertyB] = [a.unique_id, b.unique_id];
          break;
        case 'emp_name':
          [propertyA, propertyB] = [a.emp_name, b.emp_name];
          break;
        case 'designation_name':
          [propertyA, propertyB] = [a.designation_name, b.designation_name];
          break;
        case 'department_name':
          [propertyA, propertyB] = [a.department_name, b.department_name];
          break;
        case 'phone':
          [propertyA, propertyB] = [a.phone, b.phone];
          break;
        case 'role_name':
          [propertyA, propertyB] = [a.role_name, b.role_name];
          break;
        case 'active':
          [propertyA, propertyB] = [a.active, b.active];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
