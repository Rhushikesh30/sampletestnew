/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable prefer-const */
import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ReportsService } from 'src/app/shared/services/reports.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

@Component({
  selector: 'app-balance-sheet-report',
  templateUrl: './balance-sheet-report.component.html',
  styleUrls: ['./balance-sheet-report.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class BalanceSheetReportComponent {
  screenName = 'Balance Sheet';
  BalanceSheetReportForm: FormGroup;
  formattedDate: string | null = '';
  reportData: any = [];
  trialReportData: any;
  isTotal = false;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;

  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial selected page size
  showLoader = false;
  isLoading = false;

  constructor(
    private fb: FormBuilder,
    private ReportsService: ReportsService,
    private datepipe: DatePipe,
    private errorCodes: ErrorCodes,
    private encDecryService: EncrDecrService
  ) {
    this.BalanceSheetReportForm = this.fb.group({
      date: [null, Validators.required],
    });
  }

  isExpandup = false;
  isExpanddown = false;
  liability_details: any = [];
  NewreportData: any;

  currentDate = new Date().toJSON().split('T')[0];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ngOnInit(): void {
    this.initializeForm();
    this.onSubmit();
  } //end of ngoninit

  initializeForm() {
    this.BalanceSheetReportForm = this.fb.group({
      id: [''],
      date: [this.currentDate],
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  prevPageClick() {
    console.log('Clicked previous');
    let fromdate1: any = this.datepipe.transform(
      this.BalanceSheetReportForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.ReportsService.GetBalanceSheet(
      fromdate1,
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data.data;
        this.NewreportData = data.data;

        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  nextPageClick() {
    let fromdate1: any = this.datepipe.transform(
      this.BalanceSheetReportForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    this.ReportsService.GetBalanceSheet(
      fromdate1,
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data.data;
        this.NewreportData = data.data;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onPageSizeSelect() {
    // Handle the page size selection here
    let fromdate1: any = this.datepipe.transform(
      this.BalanceSheetReportForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.GetBalanceSheet(
      fromdate1,
      this.pageNumber,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data.data;
        this.NewreportData = data.data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    // You can perform any additional actions here based on the selected page size
  }

  onSubmit() {
    this.showLoader = true;
    this.isLoading = true;
    const date = this.BalanceSheetReportForm.value.date;
    console.log(date);
    const d = new Date(date);

    let fromdate1 = this.datepipe.transform(
      this.BalanceSheetReportForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    if (date) {
      this.ReportsService.GetBalanceSheet(
        fromdate1,
        this.pageNumber,
        this.pageLimit
      ).subscribe({
        next: (data: any) => {
          this.showLoader = false;
          this.isLoading = false;
          this.reportData = data.data;
          this.NewreportData = data.data;
          this.liability_details = this.reportData[0]['Liability_Details'];
          console.log(data);
          console.log('reportData', this.reportData);
          console.log('liability_details', this.liability_details);
        },
        error: (e) => {
          this.showLoader = false;
        },
      });
    }
  }
  // download-excel

  exportExcel() {
    const json_data = {
      data: this.encDecryService.encryptedData(this.reportData),
    };
    this.ReportsService.download(json_data, 'Balance Sheet').subscribe({
      next: (data: any) => {
        const url = data['data'];
        window.open(url);
        // this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        // this.showLoader = false;
      },
    });
  }

  searchField(event: any) {
    const inputValue = (event.target as HTMLInputElement).value;

    if (this.reportData && this.reportData) {
      this.reportData = this.NewreportData.slice().filter(
        (advanceTable: any) => {
          const searchStr = (
            advanceTable.account_category + advanceTable.category_level_total
          ).toLowerCase();

          return searchStr.indexOf(inputValue.toLowerCase()) !== -1;
        }
      );
    } else {
      console.error('this.reportData or this.reportData.data is undefined');
    }
  }
}
