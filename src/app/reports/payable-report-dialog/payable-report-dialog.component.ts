import { Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup} from '@angular/forms';

import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-payable-report-dialog',
  templateUrl: './payable-report-dialog.component.html',
  styleUrls: ['./payable-report-dialog.component.scss'],
  providers: [ErrorCodes]
})
export class PayableReportDialogComponent {
  filterCriteria: any = {}; // Initialize with your filter criteria
  payableForm!: FormGroup;
  filterSupplierData:any=[]
  SupplierData:any=[]

  constructor(private formBuilder: FormBuilder,private DynamicFormService:DynamicFormService ,
    private errorCodes: ErrorCodes,
    public dialogRef: MatDialogRef<PayableReportDialogComponent>
  ) {}

  ngOnInit(): void {
    this.payableForm = this.formBuilder.group({
      id: [''],
      supplier_ref_id:[''],
    })

    this.DynamicFormService.getDynamicData("supplier",'company_name').subscribe({
      next: (data: any) => {
        if(data.length>0){
          for(let i=0;i<data.length;i++){
            data[i]['key1']=data[i]['key'] 

            data[i]['key']=data[i]['key'] + '/' + data[i]['ref_type'] 
          }
          
        }
        console.log(data);
        
          this.SupplierData = data;
          this.filterSupplierData = this.SupplierData.slice();
      }, 
      error: (e) => {
        this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
      }    
    });

  }
  showSwalMassage(massage:any, icon:any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false
    });
  }


  onSubmit(){
    if(this.payableForm.invalid){
      return
    }
    else{
    this.dialogRef.close(this.payableForm.get('supplier_ref_id')?.value);
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  ResetDialog(){
    this.dialogRef.close('Reset');
  }
}
