import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayableReportDialogComponent } from './payable-report-dialog.component';

describe('PayableReportDialogComponent', () => {
  let component: PayableReportDialogComponent;
  let fixture: ComponentFixture<PayableReportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayableReportDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PayableReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
