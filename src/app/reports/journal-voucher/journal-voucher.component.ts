import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { ThemePalette } from '@angular/material/core';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { TableElement } from 'src/app/shared/TableElement';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { JournalVoucherService } from 'src/app/shared/services/journal-voucher.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-journal-voucher',
  templateUrl: './journal-voucher.component.html',
  styleUrls: ['./journal-voucher.component.scss'],
  providers: [ErrorCodes],
})
export class JournalVoucherComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  displayedColumns = [
    'actions',
    'transaction_ref_no',
    'transaction_date',
    'ledger_group',
    'fiscal_year',
    'period',
    'txn_currency',
    'conversion_rate',
    'txn_currency_debit_amount',
    'txn_currency_credit_amount',
    'base_currency_debit_amount',
    'base_currency_credit_amount',
    'posting_status',

    //  'workflow_status'
  ];

  renderedData: any = [];
  screenName = 'Journal Voucher';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;
  reportData: any = [];

  exampleDatabase?: JournalVoucherService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';
  employee_authority: any;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;

  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial selected page size
  isLoading = false;
  NewreportData: any;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private JournalVoucherService: JournalVoucherService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');
    this.employee_authority = localStorage.getItem('employee_authority');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Finance Settings')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
          console.log(this.sidebarData);
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    this.showLoader = true;
    this.JournalVoucherService.GetJVAllDaataWithPagination(1, 10).subscribe({
      next: (data: any) => {
        console.log(data);

        this.reportData = data;
        this.NewreportData = data;
        this.showLoader = false;
      },
      error: (e) => {
        this.showLoader = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    // this.refresh();
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  editViewRecord(row: any, flag: boolean) {
    console.log('yess', this.rowData, row);

    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.isLoading = true;

    const exampleDatabase = new JournalVoucherService(this.httpClient);

    exampleDatabase.getAllAdvanceTables().then(
      (data: any) => {
        this.dataSource = new ExampleDataSource(
          exampleDatabase,
          this.paginator,
          this.sort
        );
        this.isLoading = false;
      },
      (error) => {
        console.error('Error refreshing data:', error);
        this.isLoading = false;
      }
    );
  }

  prevPageClick() {
    this.isLoading = true;
    console.log('Clicked previous');
    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.JournalVoucherService.GetJVAllDaataWithPagination(
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data;
        this.NewreportData = data;
        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
  nextPageClick() {
    this.isLoading = true;
    this.JournalVoucherService.GetJVAllDaataWithPagination(
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data;
        this.NewreportData = data;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onPageSizeSelect() {
    // Handle the page size selection here
    console.log(`Selected page size: ${this.pageLimit}`);
    this.JournalVoucherService.GetJVAllDaataWithPagination(
      this.pageNumber,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data;
        this.NewreportData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    // You can perform any additional actions here based on the selected page size
  }

  exportExcel() {
    // const exportData: Partial<TableElement>[] =
    //   this.dataSource.filteredData.map((x) => ({
    //      'Journal Voucher Booking Ref. Number': x.transaction_ref_no,
    //     'Transaction Date':x.transaction_date,
    //      'Ledger Group':x.ledger_group,
    //      'Fiscal Year': x.fiscal_year,
    //     'Period': x.period,
    //     'Transaction Currency': x.currency_name,
    //     'Conversion Rate': x.conversion_rate,
    //     'Total Debit Transaction Amount': x.txn_currency_debit_amount,
    //     'Total Credit Transaction Amount': x.txn_currency_credit_amount,
    //     'Total Debit Amount': x.base_currency_debit_amount,
    //     'Total Credit Amount': x.base_currency_credit_amount,
    //     'Posting Status': x.posting_status,
    //   }));

    // TableExportUtil.exportToExcel(exportData, 'Journal Voucher Details');
    const json_data = {
      data: this.encDecryService.encryptedData(this.reportData),
    };

    if (this.reportData.length == 0) {
      Swal.fire('No Data Found');
    } else {
      this.JournalVoucherService.DownloadJournalVoucherTable(
        json_data
      ).subscribe({
        next: (data: any) => {
          const url = data['data'];
          window.open(url);
          this.showLoader = false;
        },
        // error: (e) => {
        //   this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
        //   this.showLoader=false;
        // }
      });
    }
  }

  searchField(event: any) {
    const inputValue = (event.target as HTMLInputElement).value;

    if (this.reportData && this.reportData) {
      this.reportData = this.NewreportData.slice().filter(
        (advanceTable: any) => {
          const searchStr = (
            advanceTable.transaction_ref_no +
            advanceTable.transaction_date +
            advanceTable.ledger_group_name +
            advanceTable.fiscal_year +
            advanceTable.period +
            advanceTable.currency_name +
            advanceTable.conversion_rate +
            advanceTable.txn_currency_debit_amount +
            advanceTable.txn_currency_credit_amount +
            advanceTable.base_currency_debit_amount +
            advanceTable.base_currency_credit_amount
          ).toLowerCase();

          return searchStr.indexOf(inputValue.toLowerCase()) !== -1;
        }
      );
    } else {
      console.error('this.reportData or this.reportData.data is undefined');
    }
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: JournalVoucherService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            console.log(advanceTable);

            const searchStr = (advanceTable.transaction_ref_no,
            advanceTable.transaction_date,
            advanceTable.ledger_group_name,
            advanceTable.fiscal_year,
            advanceTable.period,
            advanceTable.currency_name,
            advanceTable.conversion_rate,
            advanceTable.txn_currency_debit_amount,
            advanceTable.txn_currency_credit_amount,
            advanceTable.base_currency_debit_amount,
            advanceTable.base_currency_credit_amount)
              // advanceTable.posting_status

              .toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        console.log(this.renderedData);
        return this.renderedData;
      })
    );
  }
  disconnect() {
    //disconnect
  }

  //         /** Returns a sorted copy of the database data. */
  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'district_name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'taluka_name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
