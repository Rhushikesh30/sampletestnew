import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { JournalVoucherService } from 'src/app/shared/services/journal-voucher.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { ExpenseBookingService } from 'src/app/shared/services/expense-booking.service';

@Component({
  selector: 'app-journal-voucher-details',
  templateUrl: './journal-voucher-details.component.html',
  styleUrls: ['./journal-voucher-details.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class JournalVoucherDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() screenName!: string;

  cancelFlag = true;
  JVForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  CurrencyData: any = [];
  todayDate = new Date().toJSON().split('T')[0];
  ItemRow: any = [];
  columnsData: any[] = [];
  columnsValues: any[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private errorCodes: ErrorCodes,
    public datepipe: DatePipe,
    private JournalVoucherService: JournalVoucherService,
    private dynamic: DynamicFormService,
    private expenseBookingService: ExpenseBookingService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    console.log(this.rowData);

    this.getAllData().then(() => {
      if (!Array.isArray(this.rowData)) {
        this.showReset = false;
        this.viewEditRecord(this.rowData);
      }
    });
  }

  getAllData() {
    return new Promise((resolve, reject) => {
      this.JournalVoucherService.getCurrencyData().subscribe({
        next: (data: any) => {
          this.CurrencyData = data;
        },
      });
      this.expenseBookingService.getAccountingParameters().subscribe({
        next: (data: any) => {
          console.log('accounting_parameter: ', data);
          this.columnsData = data.columns;
          this.columnsValues = data.data;
          resolve(data);
        },
      });
    });
  }

  getColumValues(paramter: any, id: any) {
    const arrayOfElement = this.columnsValues[paramter];
    console.log('arrayOfElement: ', arrayOfElement);

    const x = arrayOfElement.find((element: any) => element.id == id);
    console.log('x: ', x);
    return x?.['name'] || 'NA';
  }

  initializeForm() {
    this.JVForm = this.formBuilder.group({
      id: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      transaction_date: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      txn_currency: [],
      base_currency: [''],
      conversion_rate: [1],
      ref_transaction_id: [''],
      ref_transaction_type_id: [''],
      txn_currency_debit_amount: [],
      txn_currency_credit_amount: [],
      base_currency_debit_amount: [],
      base_currency_credit_amount: [0],
      posting_status: [0],
      posting_ref_no: [0],
      journal_voucher_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      account_ref_id: [''],
      txn_currency: [''],
      txn_currency_debit_amount: [''],
      txn_currency_credit_amount: [''],
      base_currency_debit_amount: [''],
      base_currency_credit_amount: [''],
      ref_id_4: [''],
      ref_id_5: [''],
      ref_id_6: [''],
      ref_id_7: [''],
      ref_id_8: [''],
      ref_id_9: [''],
      ref_id_10: [''],
    });
  }

  get formArray() {
    return this.JVForm.get('journal_voucher_details') as FormArray;
  }

  roundValue(value: any, decimalPlaces: number): any {
    const val = parseFloat(value).toFixed(decimalPlaces);
    return val;
  }

  viewEditRecord(row1: any) {
    this.JournalVoucherService.getJVDataById(row1.id).subscribe({
      next: (row: any) => {
        console.log(row);
        row = row[0];
        this.JVForm.patchValue({
          id: row.id,
          transaction_id: row.transaction_id,
          transaction_ref_no: row.transaction_ref_no,
          transaction_type_id: row.transaction_type_id,
          fiscal_year: row.fiscal_year,
          period: row.period,
          conversion_rate: row.conversion_rate,
          txn_currency: row.txn_currency,
          base_currency: row.base_currency,
          txn_currency_debit_amount: row.txn_currency_debit_amount,
          txn_currency_credit_amount: row.txn_currency_credit_amount,
          base_currency_debit_amount: row.base_currency_debit_amount,
          base_currency_credit_amount: row.base_currency_credit_amount,
          posting_status: row.posting_status,
          posting_ref_no: row.posting_ref_no,
        });
        console.log(row.journal_voucher_details);

        this.ItemRow = row.journal_voucher_details.filter(function (data: any) {
          return data.is_deleted == false && data.is_active == true;
        });

        // this.JVForm.setControl(
        //   'journal_voucher_details',
        //   this.setExistingArray(this.ItemRow)
        // );
        this.JVForm.disable();
      },
    });
  }

  // getColumnValue(id: number) {

  // }

  setExistingArray(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);

    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          account_ref_id: element.account_ref_id,
          txn_currency: element.txn_currency,
          txn_currency_debit_amount: element.txn_currency_debit_amount,
          txn_currency_credit_amount: element.txn_currency_credit_amount,
          base_currency_debit_amount: element.base_currency_debit_amount,
          base_currency_credit_amount: element.base_currency_credit_amount,
          ref_id_4: element.ref_id_4,
          ref_id_5: element.ref_id_5,
          ref_id_6: element.ref_id_6,
          ref_id_7: element.ref_id_7,
          ref_id_8: element.ref_id_8,
          ref_id_9: element.ref_id_9,
          ref_id_10: element.ref_id_10,
        })
      );
    });
    return formArray;
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.JVForm.reset();
    this.handleCancel.emit(false);
  }
}
