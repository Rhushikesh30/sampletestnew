import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTR1Component } from './gstr1.component';

describe('GSTR1Component', () => {
  let component: GSTR1Component;
  let fixture: ComponentFixture<GSTR1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSTR1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GSTR1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
