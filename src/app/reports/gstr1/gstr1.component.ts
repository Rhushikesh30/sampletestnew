import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  OnInit,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  FormArray,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  ThemePalette,
} from '@angular/material/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { TableElement } from 'src/app/shared/TableElement';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { DatePipe, formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { SupplierBillReportService } from 'src/app/shared/services/supplier-bill-report.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import { Router } from '@angular/router';
import { ReportsService } from 'src/app/shared/services/reports.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { MatDatepicker } from '@angular/material/datepicker';
import { Moment } from 'moment';
import * as moment from 'moment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-gstr1',
  templateUrl: './gstr1.component.html',
  styleUrls: ['./gstr1.component.scss'],
  // standalone: true,
  // imports: [MatPaginatorModule],

  providers: [
    ErrorCodes,
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class GSTR1Component
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  displayedColumns: string[] = [
    'Sr_No',
    'no_of_invoice',
    'total_invoice_value',
    'total_taxable_value',
  ];
  showHeader = false;
  showFooter = false;
  expandedElements: any[] = [];

  screenName = 'GSTR1';
  renderedData: any = [];
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  inputId!: number;
  listDiv = false;
  showList = true;
  sidebarData: any;
  data: User[] = USERS;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  data1: User1[] = USERS1;
  exampleDatabase?: SupplierBillReportService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';
  todayDate = '';

  allBillData!: ExampleDataSource;

  gstr1Form!: FormGroup;
  supplier_name: any;
  transaction_date: any;
  to_date: any;
  so_type_id: any;
  payment_status: any;
  paymentData: any[] = [];
  supplierData: any[] = [];
  date: any;
  bill_no: any;
  net_quantity: any;
  amount: any;
  reportData: any = [];
  reportData1: any = [];
  reportData2: any = [];
  sum_all: any;
  isReportDataEmpty = false;
  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial
  pageNumber = 1;
  isLoading = false;

  pageSizes1 = [5, 10, 20, 50]; // Define your page size options
  pageLimit1 = 10; // Initial
  pageNumber1 = 1;
  isLoading1 = false;

  pageSizes2 = [5, 10, 20, 50]; // Define your page size options
  pageLimit2 = 10; // Initial
  pageNumber2 = 1;
  isLoading2 = false;

  jsonData: any = null;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private formBuilder: FormBuilder,
    private supplierbillreportService: SupplierBillReportService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private ReportsService: ReportsService,
    public datepipe: DatePipe,
    public router: Router,
    private encDecryService: EncrDecrService
  ) {
    super();
  }
  capitalize(word: string) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }
  checkLastWord() {
    const urlParts = this.router.url.split('/');
    const isPayableOrReceivable = urlParts[urlParts.length - 1];
    return isPayableOrReceivable;
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');

    console.log('this.checkLastWord(): ', this.checkLastWord());

    this.roleSecurityService
      .getAccessLeftPanel(userId, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.initializeForm();
  }

  initializeForm() {
    this.gstr1Form = this.formBuilder.group({
      date: [null, Validators.required],
    });
  }

  seerachData() {
    if (this.gstr1Form.invalid) {
      // Form is invalid, handle as needed
      return;
    }
    this.jsonData = {
      date: '',
    };
    console.log('Search button clicked!', this.gstr1Form.value);
    this.jsonData['date'] = this.getDate(this.gstr1Form.value.date);
    // if (this.gstr1Form.value.date != '') {
    //   this.jsonData['date'] = this.getDate(this.gstr1Form.value.date);
    //   this.DateChange(this.jsonData['date']);
    // }
    this.pageLimit = 10; // Initial
    this.pageNumber = 1;
    this.pageLimit1 = 10; // Initial
    this.pageNumber1 = 1;
    this.listDiv = true;
    this.showLoader = true;
    setTimeout(() => {
      this.showLoader = false;
    }, 2000);
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'all',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData1 = data['hsn_data'];

        console.log(data);
        this.reportData = data['b2b_data'];
        this.reportData2 = data['export_data'];

        this.showLoader = false;

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;

        this.errorCodes.getErrorMessage(JSON.parse(e).status),
          (this.showLoader = false);
      },
    });
  }

  getDate(new_date: any) {
    const str = new_date;
    const date = new Date(str);
    const day = date.getDate(); //Date of the month: 2 in our example
    const month = date.getMonth() + 1; //Month of the Year: 0-based index, so 1 in our example
    const year = date.getFullYear(); //Year: 2013
    const new_to_date = year + '-' + month + '-' + day;
    return new_to_date;
  }
  setMonthAndYear(
    normalizedMonthAndYear: Moment,
    datepicker: MatDatepicker<Moment>
  ) {
    const ctrlValue = this.date.value ?? moment();
    ctrlValue.month(normalizedMonthAndYear.month());
    ctrlValue.year(normalizedMonthAndYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
  onResetForm() {
    this.gstr1Form.reset();
    this.listDiv = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }
  isExpanded(row: User): string {
    return this.expandedElements.includes(row) ? 'expanded' : 'collapsed';
  }

  get_data(result: any) {
    if (result == '' || result == null || result == undefined) {
      // pass
    } else if (result == 'Reset') {
      this.refresh();
    } else {
      this.applyFilter(String(result));
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  prevPageClick() {
    this.isLoading = true;

    if (this.pageNumber - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr1Form.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'b2b',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data['b2b_data'];

        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }
  nextPageClick() {
    this.isLoading = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'b2b',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data['b2b_data'];

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  onPageSizeSelect() {
    // this.showLoader = true;

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'b2b',
      this.jsonData['date'],
      'gstr1',
      1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data['b2b_data'];

        this.pageNumber = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  prevPageClick1() {
    this.isLoading1 = true;

    if (this.pageNumber1 - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr1Form.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'hsn',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber1 - 1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.reportData1 = data['hsn_data'];

        this.pageNumber1--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading1 = false;
      },
      error: (e) => {
        this.isLoading1 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        //  this.showLoader = false;
      },
    });
  }
  nextPageClick1() {
    this.isLoading1 = true;
    // this.showLoader = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'hsn',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber1 + 1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.pageNumber1++;

        this.reportData1 = data['hsn_data'];

        //        this.showLoader = false;

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading1 = false;
      },
      error: (e) => {
        this.isLoading1 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        //  this.showLoader = false;
      },
    });
  }

  onPageSizeSelect1() {
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'hsn',
      this.jsonData['date'],
      'gstr1',
      1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.reportData1 = data['hsn_data'];

        //   this.showLoader = false;
        this.pageNumber1 = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        // this.showLoader = false;
      },
    });
  }
  // for exportdata
  prevPageClick2() {
    this.isLoading2 = true;

    if (this.pageNumber2 - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr1Form.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'export',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber2 - 1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.reportData2 = data['export_data'];

        this.pageNumber2--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading2 = false;
      },
      error: (e) => {
        this.isLoading2 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        //  this.showLoader = false;
      },
    });
  }
  nextPageClick2() {
    this.isLoading2 = true;
    // this.showLoader = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'export',
      this.jsonData['date'],
      'gstr1',
      this.pageNumber2 + 1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.pageNumber2++;
        this.reportData2 = data['export_data'];

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading2 = false;
      },
      error: (e) => {
        this.isLoading2 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        //  this.showLoader = false;
      },
    });
  }

  onPageSizeSelect2() {
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'export',
      this.jsonData['date'],
      'gstr1',
      1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.reportData2 = data['export_data'];

        //   this.showLoader = false;
        this.pageNumber2 = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
        // this.showLoader = false;
      },
    });
  }
  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new SupplierBillReportService(this.httpClient);
    this.dataSource = new ExampleDataSource();

    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  exportExcel(type: any, data: any) {
    //  this.showLoader = true;
    const json_data = {
      data: this.encDecryService.encryptedData(data),
    };

    this.ReportsService.DownloadGSTR1Table('gstr1', json_data, type).subscribe({
      next: (data: any) => {
        const url = data['data'];
        window.open(url);
        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }

  DateChange(e: any) {
    this.ReportsService.GetGSTR1(e, 'gstr1').subscribe({
      next: (data: any) => {
        this.reportData1 = data['hsn_data'];

        console.log(data);
        this.reportData = data['b2b_data'];
        this.reportData2 = data['export_data'];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }
  applyFilter(filterValue: string) {
    const filter = filterValue.toLowerCase().trim();

    if (this.dataSource) {
      this.dataSource.filter = filter;
    }
  }
  clearFilter() {
    this.refresh();
  }
}

export class ExampleDataSource {
  filterChange = new BehaviorSubject('');
  usersData: User[] = [];
  dialog: any;

  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];
}
export interface User {
  Sr_No: string;
  no_of_invoice: string;
  total_invoice_value: string;
  total_taxable_value: string;
  // payable_amount: string;
  addresses?: Address[] | MatTableDataSource<Address>;
}
export interface Address {
  Sr_No: string;
  customer_name: string;
  invoice_number: string;
  invoice_date: string;
  invoice_value: string;
  taxable_value: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}

const USERS: User[] = [];
export interface User1 {
  Sr_No: string;
  hsn_sac_no: string;
  igst_amount: string;
  sgst_amount: string;
  cgst_amount: string;
  total_amount: string;
  transaction_amount: string;

  addresses?: Address[] | MatTableDataSource<Address>;
}
export interface Address {
  Sr_No: string;
  cgst_amount: string;
  hsn_sac_no: string;
  igst_amount: string;
  sgst_amount: string;
  total_amount: string;
  total_quantity: string;
  transaction_amount: string;
  uom: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}
const USERS1: User1[] = [];
