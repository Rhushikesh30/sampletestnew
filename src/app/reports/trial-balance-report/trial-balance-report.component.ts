import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as XLSX from 'xlsx';
import { ReportsService } from 'src/app/shared/services/reports.service';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

export interface Data {
  id: number;
  account_name: string;
  account_description: number;
  quantity: number;
  closing_qty: number;
}

@Component({
  selector: 'app-trial-balance-report',
  templateUrl: './trial-balance-report.component.html',
  styleUrls: ['./trial-balance-report.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class TrialBalanceReportComponent implements OnInit {
  @ViewChild('dataSourcePaginator', { read: MatPaginator })
  dataSourcePaginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  // @ViewChild('filter', { static: true }) filter!: ElementRef;
  dataSource: MatTableDataSource<Data> = new MatTableDataSource<Data>();

  screenName = 'Trial Balance Report';
  displayedColumns = [
    'Sr. No.',
    'Account Number',
    'Account Description',
    'Opening Balance (Dr.)',
    'Opening Balance (Cr.)',
    'Transactions',
    'Closing Balance',
  ];
  showLoader = false;
  myForm!: FormGroup;
  formattedDate: string | null = '';
  trialReportData: any;
  isTotal = false;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;
  currentDate = new Date().toJSON().split('T')[0];

  pageSizes = [2, 5, 10, 20, 50]; // Define your page size options
  pageLimit = -1; // Initial selected page size
  isLoading = false;
  NewtrialReportData: any;

  constructor(
    private formBuilder: FormBuilder,
    private ReportsService: ReportsService,
    private datepipe: DatePipe,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit(): void {
    this.isLoading = true; // Show loading indicator when the component is first loaded
    this.initializeForm();
    this.onSubmit();
  } //end of ngoninit

  initializeForm() {
    this.myForm = this.formBuilder.group({
      id: [''],
      date: [this.currentDate],
    });
  }

  prevPageClick() {
    console.log('Clicked previous');
    this.isLoading = true;
    const fromdate1: any = this.datepipe.transform(
      this.myForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.ReportsService.getTrialBalanceReportData(
      fromdate1,
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.trialReportData = data.data;
        this.NewtrialReportData = data.data;

        this.pageNumber--;
        if (this.trialReportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false; // Set isLoading to false after data is loaded
      },
      error: (e) => {
        this.isLoading = false; // Set isLoading to false in case of error

        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  nextPageClick() {
    this.isLoading = true;
    const fromdate1: any = this.datepipe.transform(
      this.myForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    this.ReportsService.getTrialBalanceReportData(
      fromdate1,
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.trialReportData = data.data;
        this.NewtrialReportData = data.data;
        if (this.trialReportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false; // Set isLoading to false after data is loaded
      },
      error: (e) => {
        this.isLoading = false; // Set isLoading to false after data is loaded

        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onPageSizeSelect() {
    // Handle the page size selection here
    const fromdate1: any = this.datepipe.transform(
      this.myForm.get('date')?.value,
      'yyyy-MM-dd'
    );

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.getTrialBalanceReportData(
      fromdate1,
      this.pageNumber,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.trialReportData = data.data;
        this.NewtrialReportData = data.data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    // You can perform any additional actions here based on the selected page size
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  exportExcel() {
    console.log(this.trialReportData);

    const data = this.trialReportData;
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'trial-balance-report.xlsx');
  }

  onSubmit() {
    this.showLoader = true;
    this.isLoading = true;
    const fromdate1 = this.datepipe.transform(
      this.myForm.get('date')?.value,
      'yyyy-MM-dd'
    );
    const selectedDate = this.myForm.value.date;
    const d = new Date(selectedDate);

    if (selectedDate) {
      this.formattedDate = d.toISOString().slice(0, 10);
      console.log(this.formattedDate);
      this.ReportsService.getTrialBalanceReportData(
        fromdate1,
        this.pageNumber,
        this.pageLimit
      ).subscribe({
        next: (data: any) => {
          this.showLoader = false;
          this.trialReportData = data.data;
          this.NewtrialReportData = data.data;
          const total = this.trialReportData.filter(
            (d: any) => d.account_name == 'Total'
          );
          if (total.closing_balance_credit != 0) {
            this.isTotal = true;
            this.isLoading = false;
          }

          const nottotal = this.trialReportData.filter(
            (d: any) => d.account_name != 'Total'
          );
          console.log('nottotal', nottotal);

          console.log('total', total);
        },
        error: (e) => {
          this.showLoader = false;
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }

  searchField(event: any) {
    const inputValue = (event.target as HTMLInputElement).value;

    if (this.trialReportData && this.trialReportData) {
      this.trialReportData = this.NewtrialReportData.slice().filter(
        (advanceTable: any) => {
          const searchStr = (
            advanceTable.name +
            advanceTable.account_description +
            advanceTable.closing_balance_credit +
            advanceTable.closing_balance_debit +
            advanceTable.opening_balance_credit +
            advanceTable.opening_balance_debit +
            advanceTable.transactions_credit +
            advanceTable.transactions_debit
          ).toLowerCase();

          return searchStr.indexOf(inputValue.toLowerCase()) !== -1;
        }
      );
    } else {
      console.error('this.reportData or this.reportData.data is undefined');
    }
  }
}
