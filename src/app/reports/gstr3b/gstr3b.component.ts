import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  OnInit,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  FormArray,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  ThemePalette,
} from '@angular/material/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { TableElement } from 'src/app/shared/TableElement';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { DatePipe, formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { SupplierBillReportService } from 'src/app/shared/services/supplier-bill-report.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import { Router } from '@angular/router';
import { ReportsService } from 'src/app/shared/services/reports.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-gstr3b',
  templateUrl: './gstr3b.component.html',
  styleUrls: ['./gstr3b.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class Gstr3bComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  total_transaction_amount = 0;
  showHeader = false;
  showFooter = false;
  expandedElements: any[] = [];
  gstr3bForm!: FormGroup;
  screenName = 'GSTR 3B';
  renderedData: any = [];
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  inputId!: number;
  listDiv = false;
  showList = true;
  sidebarData: any;
  reportData: any = [];
  reportData1: any = [];
  reportData2: any = [];
  reportData3: any = [];
  data: User[] = USERS;
  data1: User1[] = USERS1;
  exampleDatabase?: SupplierBillReportService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';
  jsonData: any = null;
  itemData: any = [];
  isReportDataEmpty = false;
  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial
  pageNumber = 1;
  isLoading = false;

  pageSizes1 = [5, 10, 20, 50]; // Define your page size options
  pageLimit1 = 10; // Initial
  pageNumber1 = 1;
  isLoading1 = false;

  pageSizes2 = [5, 10, 20, 50]; // Define your page size options
  pageLimit2 = 10; // Initial
  pageNumber2 = 1;
  isLoading2 = false;
  Array: any;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private formBuilder: FormBuilder,
    private supplierbillreportService: SupplierBillReportService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private ReportsService: ReportsService,
    public datepipe: DatePipe,
    public router: Router,
    private encDecryService: EncrDecrService
  ) {
    super();
  }
  capitalize(word: string) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }
  checkLastWord() {
    const urlParts = this.router.url.split('/');
    const isPayableOrReceivable = urlParts[urlParts.length - 1];
    return isPayableOrReceivable;
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    // this.ReportsService.GetGSTR1(e , 'gstr1').subscribe({
    //   next: (data: any) => {
    //     this.reportData1 = data['hsn_data'];

    //     console.log(data);
    //     this.reportData = data['b2b_data'];
    //     this.reportData2 = data['export_data'];
    //   },
    //   error: (e) => {
    //     this.showSwalmessage(
    //       this.errorCodes.getErrorMessage(JSON.parse(e).status),
    //       '',
    //       'error',
    //       false
    //     );
    //   },
    // });

    this.initializeForm();
  }
  initializeForm() {
    this.gstr3bForm = this.formBuilder.group({
      date: [''],
    });
  }

  seerachData() {
    if (this.gstr3bForm.invalid) {
      // Form is invalid, handle as needed
      return;
    }
    this.jsonData = {
      date: '',
    };
    console.log('Search button clicked!', this.gstr3bForm.value);

    if (this.gstr3bForm.value.date != '') {
      this.jsonData['date'] = this.getDate(this.gstr3bForm.value.date);
      this.DateChange(this.jsonData['date']);
    }
    this.listDiv = true;
    this.showLoader = true;
    setTimeout(() => {
      this.showLoader = false;
    }, 2000);
  }

  getDate(new_date: any) {
    const str = new_date;
    const date = new Date(str);
    const day = date.getDate(); //Date of the month: 2 in our example
    const month = date.getMonth() + 1; //Month of the Year: 0-based index, so 1 in our example
    const year = date.getFullYear(); //Year: 2013
    const new_to_date = year + '-' + month + '-' + day;
    return new_to_date;
  }
  onResetForm() {
    this.gstr3bForm.reset();
    this.listDiv = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  get_data(result: any) {
    if (result == '' || result == null || result == undefined) {
      // pass
    } else if (result == 'Reset') {
      this.refresh();
    } else {
      this.applyFilter(String(result));
    }
  }
  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new SupplierBillReportService(this.httpClient);
    this.dataSource = new ExampleDataSource();

    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  DateChange(e: any) {
    this.pageLimit = 10; // Initial
    this.pageNumber = 1;
    this.pageLimit1 = 10; // Initial
    this.pageNumber1 = 1;
    this.pageLimit2 = 10; // Initial
    this.pageNumber2 = 1;
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'all',
      this.jsonData['date'],
      'gstrb3',
      1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data['sales_all_data'];
        this.reportData = data.sales_all_data.sales_data;
        this.reportData1 = data['purchase_all_data'];
        this.reportData1 = data.purchase_all_data.purchase_data;
        this.reportData2 = data.expense_all_data.expense_data;
        this.reportData3 = data['total_payable_amt'];

        // this.reportData3 = data.total_payable_amt;
        console.log(this.reportData3);

        // this.reportData2 = data.expense_all_data.expense_data;

        console.log(this.reportData, 'SALESSSSSSSSSSSSSSSSSS DATA');
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }
  applyFilter(filterValue: string) {
    const filter = filterValue.toLowerCase().trim();

    if (this.dataSource) {
      this.dataSource.filter = filter;
    }
  }
  clearFilter() {
    this.refresh();
  }

  prevPageClick() {
    this.isLoading = true;

    if (this.pageNumber - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr3bForm.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'sales',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data['sales_all_data'];
        this.reportData = data.sales_all_data.sales_data;
        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }
  nextPageClick() {
    this.isLoading = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'sales',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data['sales_all_data'];
        this.reportData = data.sales_all_data.sales_data;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  onPageSizeSelect() {
    // this.showLoader = true;

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'sales',
      this.jsonData['date'],
      'gstrb3',
      1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data['sales_all_data'];
        this.reportData = data.sales_all_data.sales_data;
        this.pageNumber = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  prevPageClick1() {
    this.isLoading1 = true;

    if (this.pageNumber1 - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr3bForm.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'purchase',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber1 - 1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.reportData1 = data['purchase_all_data'];
        this.reportData1 = data.purchase_all_data.purchase_data;

        this.pageNumber1--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading1 = false;
      },
      error: (e) => {
        this.isLoading1 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }
  nextPageClick1() {
    this.isLoading1 = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'purchase',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber1 + 1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.pageNumber1++;
        this.reportData1 = data['purchase_all_data'];
        this.reportData1 = data.purchase_all_data.purchase_data;

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading1 = false;
      },
      error: (e) => {
        this.isLoading1 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  onPageSizeSelect1() {
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'purchase',
      this.jsonData['date'],
      'gstrb3',
      1,
      this.pageLimit1
    ).subscribe({
      next: (data: any) => {
        this.reportData1 = data['purchase_all_data'];
        this.reportData1 = data.purchase_all_data.purchase_data;

        this.pageNumber1 = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  prevPageClick2() {
    this.isLoading2 = true;

    if (this.pageNumber2 - 1 < 1) {
      return;
    }
    this.jsonData['date'] = this.getDate(this.gstr3bForm.value.date);

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'expense',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber2 - 1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.reportData2 = data['expense_all_data'];
        this.reportData2 = data.expense_all_data.expense_data;

        this.pageNumber2--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading2 = false;
      },
      error: (e) => {
        this.isLoading2 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }
  nextPageClick2() {
    this.isLoading2 = true;

    this.ReportsService.Getgstr1AllDaataWithPagination(
      'expense',
      this.jsonData['date'],
      'gstrb3',
      this.pageNumber2 + 1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.pageNumber2++;
        this.reportData2 = data['expense_all_data'];
        this.reportData2 = data.expense_all_data.expense_data;

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
        } else {
          this.isReportDataEmpty = false;
        }
        this.isLoading2 = false;
      },
      error: (e) => {
        this.isLoading1 = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  onPageSizeSelect2() {
    this.ReportsService.Getgstr1AllDaataWithPagination(
      'expense',
      this.jsonData['date'],
      'gstrb3',
      1,
      this.pageLimit2
    ).subscribe({
      next: (data: any) => {
        this.reportData2 = data['expense_all_data'];
        this.reportData2 = data.expense_all_data.expense_data;
        this.pageNumber2 = 1;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error',
          '',
          ''
        );
      },
    });
  }

  exportExcel(type: any, data: any) {
    // this.showLoader = true;
    const json_data = {
      data: this.encDecryService.encryptedData(data),
    };

    this.ReportsService.DownloadGSTR1Table('gstrb3', json_data, type).subscribe(
      {
        next: (data: any) => {
          const url = data['data'];
          window.open(url);
          this.showLoader = false;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
          this.showLoader = false;
        },
      }
    );
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }
}

export class ExampleDataSource {
  filterChange = new BehaviorSubject('');
  usersData: User[] = [];
  dialog: any;

  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];
}
export interface User {
  Sr_No: string;
  no_of_invoice: string;
  total_invoice_value: string;
  total_taxable_value: string;
  // payable_amount: string;
  addresses?: Address[] | MatTableDataSource<Address>;
}
export interface Address {
  Sr_No: string;
  customer_name: string;
  invoice_number: string;
  invoice_date: string;
  invoice_value: string;
  taxable_value: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}

const USERS: User[] = [];
export interface User1 {
  Sr_No: string;
  hsn_sac_no: string;
  igst_amount: string;
  sgst_amount: string;
  cgst_amount: string;
  total_amount: string;
  transaction_amount: string;

  addresses?: Address[] | MatTableDataSource<Address>;
}
export interface Address {
  Sr_No: string;
  cgst_amount: string;
  hsn_sac_no: string;
  igst_amount: string;
  sgst_amount: string;
  total_amount: string;
  total_quantity: string;
  transaction_amount: string;
  uom: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}
const USERS1: User1[] = [];
