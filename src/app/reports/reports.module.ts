import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { PayableComponent } from './payable/payable.component';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ComponentsModule } from '../shared/components/components.module';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { DummyComponent } from './dummy/dummy.component';
import { PayableReportDialogComponent } from './payable-report-dialog/payable-report-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReceivableReportsDialogComponent } from './receivable-reports-dialog/receivable-reports-dialog.component';
import { InventoryReportComponent } from './inventory-report/inventory-report.component';
import { TrialBalanceReportComponent } from './trial-balance-report/trial-balance-report.component';
import { BalanceSheetReportComponent } from './balance-sheet-report/balance-sheet-report.component';
import { ProfitLossReportComponent } from './profit-loss-report/profit-loss-report.component';
import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';
import { JournalVoucherDetailsComponent } from './journal-voucher/journal-voucher-details/journal-voucher-details.component';
import { LedgerEnquiryReportComponent } from './ledger-enquiry-report/ledger-enquiry-report.component';
import { GSTR1Component } from './gstr1/gstr1.component';
import { Gstr3bComponent } from './gstr3b/gstr3b.component';

@NgModule({
  declarations: [
    PayableComponent,
    DummyComponent,
    PayableReportDialogComponent,
    ReceivableReportsDialogComponent,
    InventoryReportComponent,
    TrialBalanceReportComponent,
    BalanceSheetReportComponent,
    ProfitLossReportComponent,
    JournalVoucherComponent,
    JournalVoucherDetailsComponent,
    LedgerEnquiryReportComponent,
    GSTR1Component,
    Gstr3bComponent,
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatCheckboxModule,
    ComponentsModule,
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,
    MatDialogModule,
  ],
})
export class ReportsModule {}
