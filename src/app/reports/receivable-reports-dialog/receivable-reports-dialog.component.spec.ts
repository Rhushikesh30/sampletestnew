import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivableReportsDialogComponent } from './receivable-reports-dialog.component';

describe('ReceivableReportsDialogComponent', () => {
  let component: ReceivableReportsDialogComponent;
  let fixture: ComponentFixture<ReceivableReportsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceivableReportsDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReceivableReportsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
