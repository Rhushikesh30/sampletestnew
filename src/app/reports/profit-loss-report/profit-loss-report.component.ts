/* eslint-disable prefer-const */
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import * as XLSX from 'xlsx';
import { ReportsService } from 'src/app/shared/services/reports.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profit-loss-report',
  templateUrl: './profit-loss-report.component.html',
  styleUrls: ['./profit-loss-report.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class ProfitLossReportComponent {
  screenName = 'Profit Loss';
  myForm: FormGroup;
  formattedDate: string | null = '';
  isTotal = false;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;
  ProfitLossSum = 0;

  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial selected page size

  reportData: any = [];
  showLoader = false;
  isLoading = false;
  NewreportData: any;
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  ProductData: any = [];

  constructor(
    private fb: FormBuilder,
    private ReportsService: ReportsService,
    private datepipe: DatePipe,
    private errorCodes: ErrorCodes,
    private encDecryService: EncrDecrService
  ) {
    this.myForm = this.fb.group({
      selectedDate: [null, Validators.required],
    });
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  currentDate = new Date().toJSON().split('T')[0];

  ngOnInit(): void {
    this.showLoader = true;
    this.initializeForm();
    this.onSubmit();

    this.ReportsService.getDynamicData1('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
        // let division_id = data.filter((d:any)=>d.key=='Business')
        // if(division_id){
        //   this.purchaseBookingForm.get('division_ref_id')?.setValue(division_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.ReportsService.getDynamicData1('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
        // let dept_id = data.filter((d:any)=>d.key=='Trading')
        // if(dept_id){
        //   this.purchaseBookingForm.get('department_ref_id')?.setValue(dept_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.ReportsService.getDynamicData1('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
        // let dept_id = data.filter((d:any)=>d.key=='B2B')
        // if(dept_id){
        //   this.purchaseBookingForm.get('sale_type_ref_id')?.setValue(dept_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.ReportsService.getDynamicData1('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
        // if(data){
        //   this.purchaseBookingForm.get('location_ref_id')?.setValue(data[0]['id'])

        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.ReportsService.getDynamicData(
      'get_dynamic_data',
      'item_master',
      'code'
    ).subscribe({
      next: (data: any) => {
        this.ProductData = data;
        this.showLoader = false;
      },
    });
  } //end of ngoninit

  initializeForm() {
    this.myForm = this.fb.group({
      id: [''],
      selectedDate: [this.currentDate],
      division_ref_id: ['all'],
      department_ref_id: ['all'],
      sale_type_ref_id: ['all'],
      location_ref_id: ['all'],
      item_ref_id: ['all'],
    });
  }

  exportExcel() {
    const json_data = {
      data: this.encDecryService.encryptedData(this.reportData),
    };
    this.ReportsService.download(json_data, 'Profit-Loss').subscribe({
      next: (data: any) => {
        const url = data['data'];
        window.open(url);
        // this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        // this.showLoader = false;
      },
    });
  }
  onResetForm() {
    this.myForm.reset();
    // this.refresh();
  }

  onSubmit() {
    const selectedDate = this.myForm.value.selectedDate;
    console.log(selectedDate);
    const d = new Date(selectedDate);

    if (selectedDate) {
      this.formattedDate = d.toISOString().slice(0, 10);
      console.log(this.formattedDate);
    }

    let fromdate1 = this.datepipe.transform(
      this.myForm.get('selectedDate')?.value,
      'yyyy-MM-dd'
    );

    if (selectedDate) {
      this.showLoader = true;
      this.isLoading = true;
      this.ReportsService.getProfitLossReportData(
        fromdate1,
        this.myForm.value.division_ref_id,
        this.myForm.value.department_ref_id,
        this.myForm.value.item_ref_id,
        this.myForm.value.location_ref_id,
        this.myForm.value.sale_type_ref_id,
        this.pageNumber,
        this.pageLimit
      ).subscribe({
        next: (data: any) => {
          if (data.data.length > 0) {
            this.reportData = data.data;
            this.NewreportData = data.data;
          } else {
            this.reportData = [];
            this.showSwalMassage(
              'No Data Available for this Search ',
              'warning'
            );
          }
          this.showLoader = false;
          this.isLoading = false;
        },
        error: (e) => {
          this.showLoader = false;
        },
      });
    }
  }
  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  prevPageClick() {
    console.log('Clicked previous');
    let fromdate1: any = this.datepipe.transform(
      this.myForm.get('selectedDate')?.value,
      'yyyy-MM-dd'
    );

    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.ReportsService.getProfitLossReportData(
      fromdate1,
      this.myForm.value.division_ref_id,
      this.myForm.value.department_ref_id,
      this.myForm.value.item_ref_id,
      this.myForm.value.location_ref_id,
      this.myForm.value.sale_type_ref_id,
      this.pageNumber - 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data.data;
        this.NewreportData = data.data;
        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  nextPageClick() {
    let fromdate1: any = this.datepipe.transform(
      this.myForm.get('selectedDate')?.value,
      'yyyy-MM-dd'
    );

    this.ReportsService.getProfitLossReportData(
      fromdate1,
      this.myForm.value.division_ref_id,
      this.myForm.value.department_ref_id,
      this.myForm.value.item_ref_id,
      this.myForm.value.location_ref_id,
      this.myForm.value.sale_type_ref_id,
      this.pageNumber + 1,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data.data;
        this.NewreportData = data.data;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onPageSizeSelect() {
    // Handle the page size selection here
    let fromdate1: any = this.datepipe.transform(
      this.myForm.get('selectedDate')?.value,
      'yyyy-MM-dd'
    );

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.getProfitLossReportData(
      fromdate1,
      this.myForm.value.division_ref_id,
      this.myForm.value.department_ref_id,
      this.myForm.value.item_ref_id,
      this.myForm.value.location_ref_id,
      this.myForm.value.sale_type_ref_id,
      this.pageNumber,
      this.pageLimit
    ).subscribe({
      next: (data: any) => {
        this.reportData = data.data;
        this.NewreportData = data.data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    // You can perform any additional actions here based on the selected page size
  }

  searchField(event: any) {
    const inputValue = (event.target as HTMLInputElement).value;

    if (this.reportData && this.reportData) {
      this.reportData = this.NewreportData.slice().filter(
        (advanceTable: any) => {
          const searchStr = (
            advanceTable.account_category + advanceTable.account_category
          ).toLowerCase();

          return searchStr.indexOf(inputValue.toLowerCase()) !== -1;
        }
      );
    } else {
      console.error('this.reportData or this.reportData.data is undefined');
    }
  }
}
