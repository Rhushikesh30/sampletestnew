import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PayableComponent } from './payable/payable.component';
import { DummyComponent } from './dummy/dummy.component';
import { InventoryReportComponent } from './inventory-report/inventory-report.component';
import { TrialBalanceReportComponent } from './trial-balance-report/trial-balance-report.component';
import { BalanceSheetReportComponent } from './balance-sheet-report/balance-sheet-report.component';
import { ProfitLossReportComponent } from './profit-loss-report/profit-loss-report.component';
import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';
import { LedgerEnquiryReportComponent } from './ledger-enquiry-report/ledger-enquiry-report.component';
import { GSTR1Component } from './gstr1/gstr1.component';
import { Gstr3bComponent } from './gstr3b/gstr3b.component';
const routes: Routes = [
  {
    path: 'payable',
    component: PayableComponent,
  },
  {
    path: 'receivable',
    component: PayableComponent,
  },
  {
    path: 'dummy',
    component: DummyComponent,
  },
  {
    path: 'inventory-summary',
    component: InventoryReportComponent,
  },
  {
    path: 'trial-balance',
    component: TrialBalanceReportComponent,
  },
  {
    path: 'balance-sheet',
    component: BalanceSheetReportComponent,
  },
  {
    path: 'profit-loss',
    component: ProfitLossReportComponent,
  },

  {
    path: 'journal-voucher',
    component: JournalVoucherComponent,
  },
  {
    path: 'ledger-enquiry',
    component: LedgerEnquiryReportComponent,
  },
  {
    path: 'gstr1',
    component: GSTR1Component,
  },
  {
    path: 'gst3b',
    component: Gstr3bComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {}
