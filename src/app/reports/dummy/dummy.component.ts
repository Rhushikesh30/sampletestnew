
import { Component, OnInit, ViewChild,ViewChildren,QueryList,ChangeDetectorRef } from '@angular/core';
import { animate,state,style,transition,trigger} from '@angular/animations';

import { MatTableDataSource,MatTable } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { ReportsService } from 'src/app/shared/services/reports.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.scss'],
  providers: [ErrorCodes],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class DummyComponent implements OnInit {

  @ViewChild('outerSort', { static: true }) sort!: MatSort;
  @ViewChildren('innerSort') innerSort!: QueryList<MatSort>;
  @ViewChildren('innerTables') innerTables!: QueryList<MatTable<Address>>;

  data: User[] = USERS;

  dataSource!: MatTableDataSource<User>;
  usersData: User[] = [];
  columnsToDisplay = ['supplier_name', 'currency_code', 'txn_amount'];
  innerDisplayedColumns =['supplier_name', 'currency_code', 'txn_amount'];
 // innerInnerDisplayedColumns = ['comment', 'commentStatus'];
  expandedElement!: User | null;
  expandedElements: any[] = [];
  tableData:any;


  constructor(private cd: ChangeDetectorRef,private ReportsService:ReportsService, private errorCodes: ErrorCodes) { }

  ngOnInit(): void { 

    this.ReportsService.GetPayableAllDaata().subscribe({
      next: (data: any) => {
        console.log(data);
        this.tableData = data;

        // Now that you have the data, you can initialize USERS
      //  const USERS: User[] = this.tableData;
       
      },
      error: (e) => {
        this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
      }
    });

    this.initializeUserData(USERS);   
  
    
  }

  initializeUserData(USERS: User[]) {
    console.log(USERS);
    
    USERS.forEach(user => {
      if (
        user.addresses &&
        Array.isArray(user.addresses) &&
        user.addresses.length
      ) {
        console.log("yesss");
        
        this.usersData = [
          ...this.usersData,
          { ...user, addresses: new MatTableDataSource(user.addresses) }
        ];
      } else {
        console.log("noooo");
        
        this.usersData = [...this.usersData, user];
      }
    });
    this.dataSource = new MatTableDataSource(this.usersData);
    console.log(this.dataSource);
    
    this.dataSource.sort = this.sort;
  }

  showSwalMassage(massage:any, icon:any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false
    });
  }


  applyFilter(filterValue: string) {
    console.log("in filter");
    
    this.innerTables.forEach(
      (table, index) =>
        ((table.dataSource as MatTableDataSource<
          Address
        >).filter = filterValue.trim().toLowerCase())
    );
  }

  toggleRow(element: User) {
    console.log("in togglerow");
    element.addresses &&
    (element.addresses as MatTableDataSource<Address>).data.length
      ? this.toggleElement(element)
      : null;
    this.cd.detectChanges();
    this.innerTables.forEach(
      (table, index) =>
        ((table.dataSource as MatTableDataSource<
          Address
        >).sort = this.innerSort.toArray()[index])
    );
  }

  isExpanded(row: User): string {
    console.log("in expand");
    const index = this.expandedElements.findIndex(x => x.name == row.supplier_name);
    if (index !== -1) {
      return 'expanded';
    }
    return 'collapsed';
  }

  toggleElement(row: User) {
    console.log("in toggle ele");
    const index = this.expandedElements.findIndex(x => x.name == row.supplier_name);
    if (index === -1) {
      this.expandedElements.push(row);
    } else {
      this.expandedElements.splice(index, 1);
    }

    //console.log(this.expandedElements);
  }

}

export interface User {
  supplier_name: string;
  currency_code: string;
  txn_amount: string;
  addresses?: Address[] | MatTableDataSource<Address>;
}

// export interface Comment{
//   commenID: number;
//   comment: string;
//   commentStatus: string;
// }

export interface Address {
  supplier_name: string;
  currency_code: string;
  txn_amount: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}

const USERS: User[] = [
  {
  supplier_name: 'Mason',
  currency_code: 'mason@test.com',
  txn_amount: '9864785214',
    addresses: [
      {
        supplier_name: 'Street 1',
        currency_code: '78542',
        txn_amount: 'Kansas',
       
      },
      {
        supplier_name: 'Street 1',
        currency_code: '78542',
        txn_amount: 'Kansas',
       
      }
    ]
  },
  {
    supplier_name: 'Mason',
    currency_code: 'mason@test.com',
    txn_amount: '9864785214',
      addresses: [
        {
          supplier_name: 'Street 1',
          currency_code: '78542',
          txn_amount: 'Kansas',
         
        },
        {
          supplier_name: 'Street 1',
          currency_code: '78542',
          txn_amount: 'Kansas',
         
        }
      ]
    },
 
  
   ]
  

