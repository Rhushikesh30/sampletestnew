// import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  UntypedFormArray,
  UntypedFormGroup,
  FormArray,
  Validators,
} from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { ReportsService } from 'src/app/shared/services/reports.service';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import Swal from 'sweetalert2';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import * as XLSX from 'xlsx';
import { Router } from '@angular/router';
import { TableElement } from 'src/app/shared/TableElement';

export interface Data {
  item_name: string;
  uom_name: number;
  quantity: number;
  closing_qty: number;
}

@Component({
  selector: 'app-inventory-report',
  templateUrl: './inventory-report.component.html',
  styleUrls: ['./inventory-report.component.scss'],
  providers: [DatePipe, ErrorCodes],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class InventoryReportComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild('dataSourcePaginator', { read: MatPaginator })
  dataSourcePaginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  dataSource: MatTableDataSource<Data> = new MatTableDataSource<Data>();
  // dataSource2!: ExampleDataSource;

  displayedColumns: any = ['item_name', 'uom_name', 'quantity', 'closing_qty'];
  screenName = 'Inventory Report';
  inventoryreportForm!: FormGroup;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  exampleDatabase?: ReportsService;
  // dataSource =new MatTableDataSource;
  dataSourceWithPageSize = new MatTableDataSource();
  obs!: Observable<any>;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;
  inventoryReportData: any = [];
  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial selected page size
  tenantData: any = [];
  productData: any = [];
  header: any = [];
  item1Data: any = [];
  item2Data: any = [];
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];
  showNoData = false;
  sum_amt: any;

  showLoader = false;
  isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    public datepipe: DatePipe,
    private reportService: ReportsService,
    private errorCodes: ErrorCodes,
    public router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.showLoader = true;

    this.initializeForm();
    this.showtable(
      this.inventoryreportForm.get('item_ref_id')?.value,
      this.inventoryreportForm.get('item_attribute1')?.value,
      this.inventoryreportForm.get('item_attribute2')?.value,
      this.pageNumber,
      this.pageLimit
    );
    let e: any;

    this.reportService
      .getDynamicData('get_dynamic_data', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.productData = data;
          this.showLoader = false;
        },
      });

    this.reportService
      .getDynamicData('get_dynamic_data', 'item_attribute1', 'name')
      .subscribe({
        next: (data: any) => {
          this.item1Data = data;
          this.showLoader = false;
        },
      });

    this.reportService
      .getDynamicData('get_dynamic_data', 'item_attribute2', 'name')
      .subscribe({
        next: (data: any) => {
          this.item2Data = data;
          this.showLoader = false;
        },
      });

    this.inventoryreportForm.get('item_attribute1')?.valueChanges.subscribe({
      next: (val: any) => {
        this.showLoader = false;

        if (val) {
          this.showtable(
            this.inventoryreportForm.get('item_ref_id')?.value,
            val,
            this.inventoryreportForm.get('item_attribute2')?.value,
            this.pageNumber,
            this.pageLimit
          );
        } else {
          // this.stateData = [];
        }
      },
      error: (e) => {
        console.log('e', e);

        // this.stateData = [];
        this.showLoader = false;
      },
    });

    this.inventoryreportForm.get('item_attribute2')?.valueChanges.subscribe({
      next: (val: any) => {
        this.showLoader = false;

        if (val) {
          this.showtable(
            this.inventoryreportForm.get('item_ref_id')?.value,
            this.inventoryreportForm.get('item_attribute1')?.value,
            val,
            this.pageNumber,
            this.pageLimit
          );
        } else {
          // this.stateData = [];
        }
      },
      error: (e) => {
        console.log('e', e);

        // this.stateData = [];
        this.showLoader = false;
      },
    });

    this.inventoryreportForm.get('item_ref_id')?.valueChanges.subscribe({
      next: (val: any) => {
        this.showLoader = false;

        if (val) {
          this.showtable(
            val,
            this.inventoryreportForm.get('item_attribute1')?.value,
            this.inventoryreportForm.get('item_attribute2')?.value,
            this.pageNumber,
            this.pageLimit
          );
        } else {
          // this.stateData = [];
        }
      },
      error: (e) => {
        console.log('e', e);

        // this.stateData = [];
        this.showLoader = false;
      },
    });
  }

  initializeForm() {
    this.inventoryreportForm = this.formBuilder.group({
      id: [''],
      item_attribute1: [''],
      item_attribute2: [''],
      item_ref_id: ['all'],
    });
  }

  search(value: string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  showtable(
    item: any,
    item_attr1: any,
    item_attr2: any,
    pageNumber: any,
    pageLimit: any
  ) {
    this.showLoader = true;
    this.isLoading = true;

    console.log('item--', item);
    console.log('item_attr1---', item_attr1);
    console.log('item_attr2---', item_attr2);

    if (item == '') {
      item = null;
    }
    if (item_attr1 == '') {
      item_attr1 = null;
    }
    if (item_attr2 == '') {
      item_attr2 = null;
    }

    const tranc_date = this.datepipe.transform(
      this.inventoryreportForm.value.transaction_date,
      'yyyy-MM-dd'
    );

    this.reportService
      .getInventoryData(item, item_attr1, item_attr2, pageNumber, pageLimit)
      .subscribe({
        next: (data: any) => {
          console.log('data', data);
          this.inventoryReportData = data;
          this.inventoryReportData = data.map((item: any) => {
            console.log(item, 'item of closing Quantity...');

            const numericClosingQuantity = parseFloat(item.closing_quantity);

            // Check if numericClosingQuantity is zero
            if (numericClosingQuantity === 0.0) {
              console.log(
                numericClosingQuantity,
                'Numeric Closing Quantity is zero'
              );

              // Set weighted_rate to zero
              return { ...item, weighted_rate: 0.0 };
            }
            return item;
          });

          this.sum_amt = data[0].sum_amt;

          this.showLoader = false;

          this.dataSource = data;
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.paginator = this.dataSourcePaginator;
          this.isLoading = false;

          console.log('reportData------------', this.dataSource);
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
          this.showLoader = false;
        },
      });

    // this.dataSource = this.reportData;
    // this.dataSourceWithPageSize = this.reportData;
  } //end of showtable

  prevPageClick() {
    this.showLoader = true;

    if (this.pageNumber - 1 < 1) {
      return;
    }

    let item = this.inventoryreportForm.get('item_ref_id')?.value;
    let item_attr1 = this.inventoryreportForm.get('item_attribute1')?.value;
    let item_attr2 = this.inventoryreportForm.get('item_attribute2')?.value;
    if (item == '') {
      item = null;
    }
    if (item_attr1 == '') {
      item_attr1 = null;
    }
    if (item_attr2 == '') {
      item_attr2 = null;
    }

    this.reportService
      .getInventoryData(
        item,
        item_attr1,
        item_attr2,
        this.pageNumber - 1,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          this.inventoryReportData = data;
          this.showLoader = false;

          this.pageNumber--;
          if (this.inventoryReportData.length === 0) {
            this.isReportDataEmpty = true;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = true;
          } else {
            this.isReportDataEmpty = false;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = false;
          }
        },
        error: (e: any) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
          this.showLoader = false;
        },
      });
  }

  nextPageClick() {
    this.showLoader = true;

    let item = this.inventoryreportForm.get('item_ref_id')?.value;
    let item_attr1 = this.inventoryreportForm.get('item_attribute1')?.value;
    let item_attr2 = this.inventoryreportForm.get('item_attribute2')?.value;
    if (item == '') {
      item = null;
    }
    if (item_attr1 == '') {
      item_attr1 = null;
    }
    if (item_attr2 == '') {
      item_attr2 = null;
    }
    this.reportService
      .getInventoryData(
        item,
        item_attr1,
        item_attr2,
        this.pageNumber + 1,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          if (data) {
            console.log();
          }
          this.pageNumber++;

          this.inventoryReportData = data;
          this.showLoader = false;

          if (this.inventoryReportData.length === 0) {
            this.isReportDataEmpty = true;
            this.isNextClickDisable = true;
            this.isPreviousClickDisable = false;
          } else {
            this.isReportDataEmpty = false;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = false;
          }
        },
        error: (e: any) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
          this.showLoader = false;
        },
      });
  }

  checkLastWord() {
    const urlParts = this.router.url.split('/');
    const isPayableOrReceivable = urlParts[urlParts.length - 1];
    return isPayableOrReceivable;
  }
  searchIconClick() {
    // this.showLoader = true;
    // console.log("hello i wascllicked",this.filter.nativeElement.value);
    // this.reportService.GetPayableAllDaataWithPagination(
    //   this.pageNumber,
    //   this.pageLimit,
    //   this.checkLastWord(),
    //   this.filter.nativeElement.value
    // ).subscribe({
    //   next: (data: any) => {z
    //     this.inventoryReportData = data;
    //     this.showLoader = false;
    //     if (this.inventoryReportData.length === 0) {
    //       this.isReportDataEmpty = true;
    //       this.isNextClickDisable = false;
    //       this.isPreviousClickDisable = false;
    //     } else {
    //       this.isReportDataEmpty = false;
    //       this.isNextClickDisable = false;
    //       this.isPreviousClickDisable = false;
    //     }
    //   },
    //   error: (e) => {
    //     this.showSwalMassage(
    //       this.errorCodes.getErrorMessage(JSON.parse(e).status),
    //       'error'
    //     );
    //     this.showLoader = false;
    //   },
    // });
  }

  onPageSizeSelect() {
    this.showLoader = true;

    let item = this.inventoryreportForm.get('item_ref_id')?.value;
    let item_attr1 = this.inventoryreportForm.get('item_attribute1')?.value;
    let item_attr2 = this.inventoryreportForm.get('item_attribute2')?.value;
    if (item == '') {
      item = null;
    }
    if (item_attr1 == '') {
      item_attr1 = null;
    }
    if (item_attr2 == '') {
      item_attr2 = null;
    }
    console.log(`Selected page size: ${this.pageLimit}`);
    this.reportService
      .getInventoryData(
        item,
        item_attr1,
        item_attr2,
        this.pageNumber,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          this.inventoryReportData = data;
          this.showLoader = false;
        },
        error: (e: any) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
          this.showLoader = false;
        },
      });
  }

  // exportExcel() {
  //   const data = this.inventoryReportData;
  //   const invdata = [];
  //   for (let index = 0; index < this.inventoryReportData.length; index++) {
  //     invdata.push({
  //       Product: this.inventoryReportData[index]['item_name'],
  //       Quantity: this.inventoryReportData[index]['closing_quantity'],
  //       Rate: this.inventoryReportData[index]['weighted_rate'],
  //       'Alternate Quantity':
  //         this.inventoryReportData[index]['alternate_closing_qty'],
  //       'Alternate Rate':
  //         this.inventoryReportData[index]['alternate_weighted_rate'],
  //       Amount: this.inventoryReportData[index]['alternate_amount'],
  //     });
  //   }
  //   const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(invdata);
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   XLSX.writeFile(wb, 'Inventoy-report.xlsx');
  // }

  exportExcel() {
    const data = this.inventoryReportData;
    const invdata = [];
    for (let index = 0; index < this.inventoryReportData.length; index++) {
      invdata.push({
        Product: this.inventoryReportData[index]['item_name'],
        Quantity: this.inventoryReportData[index]['closing_quantity'],
        Rate: this.inventoryReportData[index]['weighted_rate'],
        'Alternate Quantity':
          this.inventoryReportData[index]['alternate_closing_qty'],
        'Alternate Rate':
          this.inventoryReportData[index]['alternate_weighted_rate'],
        Amount: this.inventoryReportData[index]['alternate_amount'],
        // Total_Amount: this.inventoryReportData[index]['sum_amt'],
      });
    }
    console.log(
      'this.inventoryReportData[this.inventoryReportData.length-1]: ',
      this.inventoryReportData[0]
    );

    invdata.push({
      // Product: -"",
      // Quantity: -1,
      // Rate: -1,
      // 'Alternate Quantity': -1,
      'Alternate Rate': 'Total Amount',
      // Amount: -1,
      Amount: this.inventoryReportData[0]['sum_amt'],
    });
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(invdata);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'Inventoy-report.xlsx');
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }
}
