import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { ReportsService } from 'src/app/shared/services/reports.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-ledger-enquiry-report',
  templateUrl: './ledger-enquiry-report.component.html',
  styleUrls: ['./ledger-enquiry-report.component.scss'],
  providers: [DatePipe, ErrorCodes],
})
export class LedgerEnquiryReportComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  ledgerenquiryForm!: FormGroup;
  currentDate = new Date().toJSON().split('T')[0];
  today = new Date().toJSON().split('T')[0];
  accountData: any = [];
  ledgerEnquiryData: any = [];
  screenName = 'Ledger Enquiry';
  showLoader = false;
  pageNumber = 1;
  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10;
  minDate: any;
  maxDate: any;
  isTotal = false;
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    public datepipe: DatePipe,
    private reportService: ReportsService,
    private errorCodes: ErrorCodes,
    public router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.initializeForm();
    this.reportService
      .getDynamicData('get_dynamic_data', 'chart_of_account', 'name')
      .subscribe({
        next: (data: any) => {
          this.accountData = data;
          this.showLoader = false;
        },
      });
    const today = new Date();
    this.minDate = new Date(today.getFullYear(), 2, 31);
    this.maxDate = new Date(today.getFullYear() + 1, 2, 31);
  }

  initializeForm() {
    this.ledgerenquiryForm = this.formBuilder.group({
      id: [''],
      from_date: [''],
      to_date: [''],
      account_ref_id: ['', [Validators.required]],
    });
  }

  onSubmit() {
    this.isLoading = true;
    const fromdate1 = this.datepipe.transform(
      this.ledgerenquiryForm.get('from_date')?.value,
      'yyyy-MM-dd'
    );
    const todate1 = this.datepipe.transform(
      this.ledgerenquiryForm.get('to_date')?.value,
      'yyyy-MM-dd'
    );

    const selectedDate = this.ledgerenquiryForm.value.date;

    if (this.ledgerenquiryForm.invalid) {
      this.isLoading = false;
      // this.showSwalMassage('Please fill required fields','error')
    } else {
      this.showLoader = true;
      this.reportService
        .getLedgerEnquiryReportData(
          this.ledgerenquiryForm.get('account_ref_id')?.value,
          fromdate1,
          todate1,
          this.pageNumber,
          this.pageLimit
        )
        .subscribe({
          next: (data: any) => {
            this.showLoader = false;
            this.ledgerEnquiryData = data;
            this.isLoading = false;
          },

          error: (e) => {
            this.showLoader = false;
            this.isLoading = false;
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    }
  }

  prevPageClick() {
    this.isLoading = true;
    console.log('Clicked previous');
    const fromdate1: any = this.datepipe.transform(
      this.ledgerenquiryForm.get('from_date')?.value,
      'yyyy-MM-dd'
    );
    const todate1 = this.datepipe.transform(
      this.ledgerenquiryForm.get('to_date')?.value,
      'yyyy-MM-dd'
    );

    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.reportService
      .getLedgerEnquiryReportData(
        this.ledgerenquiryForm.get('account_ref_id')?.value,
        fromdate1,
        todate1,
        this.pageNumber - 1,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          this.ledgerEnquiryData = data;
          this.pageNumber--;
          if (this.ledgerEnquiryData.length === 0) {
            this.isReportDataEmpty = true;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = true;
          } else {
            this.isReportDataEmpty = false;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = false;
          }
          this.isLoading = false;
        },
        error: (e) => {
          this.isLoading = false;
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  nextPageClick() {
    this.isLoading = true;
    const fromdate1: any = this.datepipe.transform(
      this.ledgerenquiryForm.get('from_date')?.value,
      'yyyy-MM-dd'
    );
    const todate1 = this.datepipe.transform(
      this.ledgerenquiryForm.get('to_date')?.value,
      'yyyy-MM-dd'
    );

    this.reportService
      .getLedgerEnquiryReportData(
        this.ledgerenquiryForm.get('account_ref_id')?.value,
        fromdate1,
        todate1,
        this.pageNumber + 1,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          this.pageNumber++;

          this.ledgerEnquiryData = data;
          if (this.ledgerEnquiryData.length === 0) {
            this.isReportDataEmpty = true;
            this.isNextClickDisable = true;
            this.isPreviousClickDisable = false;
          } else {
            this.isReportDataEmpty = false;
            this.isNextClickDisable = false;
            this.isPreviousClickDisable = false;
          }
          this.isLoading = false;
        },
        error: (e) => {
          this.isLoading = false;
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  onPageSizeSelect() {
    // Handle the page size selection here
    const fromdate1: any = this.datepipe.transform(
      this.ledgerenquiryForm.get('from_date')?.value,
      'yyyy-MM-dd'
    );

    console.log(`Selected page size: ${this.pageLimit}`);
    this.reportService
      .getLedgerEnquiryReportData(
        this.ledgerenquiryForm.get('account_ref_id')?.value,
        fromdate1,
        this.ledgerenquiryForm.get('to_date')?.value,
        this.pageNumber,
        this.pageLimit
      )
      .subscribe({
        next: (data: any) => {
          this.ledgerEnquiryData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    // You can perform any additional actions here based on the selected page size
  }

  search(val: any) {
    console.log('vallllll', val);

    const filteredData = this.ledgerEnquiryData.filter(
      (item: any) => item.transaction_date === '2023-10-25'
    );
  }

  exportExcel() {
    const data = this.ledgerEnquiryData;
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'ledger-enquiry-report.xlsx');
  }

  showSwalMassage(message: any, icon: any): void {
    Swal.fire({
      title: message,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }
}
