import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LedgerEnquiryReportComponent } from './ledger-enquiry-report.component';

describe('LedgerEnquiryReportComponent', () => {
  let component: LedgerEnquiryReportComponent;
  let fixture: ComponentFixture<LedgerEnquiryReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LedgerEnquiryReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LedgerEnquiryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
