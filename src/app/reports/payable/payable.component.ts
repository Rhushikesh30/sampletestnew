import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewChildren,
  QueryList,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { ThemePalette } from '@angular/material/core';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ReportsService } from 'src/app/shared/services/reports.service';
import { PayableReportDialogComponent } from '../payable-report-dialog/payable-report-dialog.component';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payable',
  templateUrl: './payable.component.html',
  styleUrls: ['./payable.component.scss'],
  providers: [ErrorCodes],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class PayableComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  @ViewChildren('innerTables') innerTables!: QueryList<MatTable<Address>>;

  resultData!: [];

  displayedColumns = [
    'Sr_No',
    'supplier_name',
    'currency_code',
    'txn_amount',
    'payable_amount',
  ];
  @ViewChild('epltable', { static: true }) epltable: ElementRef | any;

  renderedData: any = [];
  NewtrialReportData: any;

  showLoader = false;

  exampleDatabase?: ReportsService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  data: User[] = USERS;
  expandedElement!: User | null;
  expandedElements: any[] = [];
  tableCols: any[] = [];
  innerColumnExpansion: { [key: string]: boolean } = {};
  reportData: any = [];
  isReportDataEmpty = false;
  isNextClickDisable = false;
  isPreviousClickDisable = false;
  pageNumber = 1;
  sum_all: any;

  pageSizes = [5, 10, 20, 50]; // Define your page size options
  pageLimit = 10; // Initial selected page size
  isLoading = false;

  constructor(
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private ReportsService: ReportsService,
    public dialog: MatDialog,
    private encDecryService: EncrDecrService,
    public router: Router
  ) {
    super();
  }
  capitalize(word: string) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }
  checkLastWord() {
    const urlParts = this.router.url.split('/');
    const isPayableOrReceivable = urlParts[urlParts.length - 1];
    return isPayableOrReceivable;
  }

  ngOnInit(): void {
    this.showLoader = true;

    console.log('this.checkLastWord(): ', this.checkLastWord());
    this.ReportsService.GetPayableAllDaataWithPagination(
      1,
      10,
      this.checkLastWord(),

      this.filter.nativeElement.value
    ).subscribe({
      next: (data: any) => {
        this.reportData = data;
        this.sum_all = data[0].sum_all;

        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }

  searchIconClick() {
    this.isLoading = true;
    this.showLoader = true;

    console.log('hello i wascllicked', this.filter.nativeElement.value);
    this.ReportsService.GetPayableAllDaataWithPagination(
      this.pageNumber,
      this.pageLimit,
      this.checkLastWord(),

      this.filter.nativeElement.value
    ).subscribe({
      next: (data: any) => {
        console.log(data);

        this.reportData = data;
        console.log('REOPORT DATA', this.reportData);

        this.showLoader = false;

        if (this.reportData.length === '') {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }
  prevPageClick() {
    this.isLoading = true;
    this.showLoader = true;

    console.log('Clicked previous');
    if (this.pageNumber - 1 < 1) {
      return;
    }

    this.ReportsService.GetPayableAllDaataWithPagination(
      this.pageNumber - 1,
      this.pageLimit,
      this.checkLastWord(),

      this.filter.nativeElement.value
    ).subscribe({
      next: (data: any) => {
        this.reportData = data;
        this.NewtrialReportData = data.data;

        this.showLoader = false;

        this.pageNumber--;
        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = true;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }
  nextPageClick() {
    this.isLoading = true;
    this.showLoader = true;

    this.ReportsService.GetPayableAllDaataWithPagination(
      this.pageNumber + 1,

      this.pageLimit,
      this.checkLastWord(),
      this.filter.nativeElement.value
    ).subscribe({
      next: (data: any) => {
        this.pageNumber++;

        this.reportData = data;
        this.NewtrialReportData = data.data;

        this.showLoader = false;

        if (this.reportData.length === 0) {
          this.isReportDataEmpty = true;
          this.isNextClickDisable = true;
          this.isPreviousClickDisable = false;
        } else {
          this.isReportDataEmpty = false;
          this.isNextClickDisable = false;
          this.isPreviousClickDisable = false;
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.isLoading = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }

  onPageSizeSelect() {
    this.showLoader = true;

    console.log(`Selected page size: ${this.pageLimit}`);
    this.ReportsService.GetPayableAllDaataWithPagination(
      1,
      this.pageLimit,
      this.checkLastWord(),

      this.filter.nativeElement.value
    ).subscribe({
      next: (data: any) => {
        this.reportData = data;
        this.NewtrialReportData = data.data;
        this.showLoader = false;
        this.pageNumber = 1;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    // window.location.reload();

    this.isLoading = true;
    this.exampleDatabase = new ReportsService(this.httpClient);
    const subscription = fromEvent(
      this.filter.nativeElement,
      'keyup'
    ).subscribe(() => {
      if (!this.dataSource) {
        console.log('this.dataSource: ', this.dataSource);
        return;
      }
      this.dataSource.filter = this.filter.nativeElement.value;
    });

    this.subs.sink = subscription;

    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
  }

  isExpanded(row: User): string {
    return this.expandedElements.includes(row) ? 'expanded' : 'collapsed';
  }

  openFilterDialog() {
    const dialogRef = this.dialog.open(PayableReportDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      console.log('Filter Criteria:', result);
      this.get_data(result);
    });
  }

  get_data(result: any) {
    if (result == '' || result == null || result == undefined) {
      // pass
    } else if (result == 'Reset') {
      this.refresh();
    } else {
      this.applyFilter(String(result));
    }
  }

  onPageChange(event: PageEvent) {
    console.log(event.pageIndex);
    console.log(event.pageSize);
    if (event.pageIndex > this.pageNumber) {
      // Clicked on next button
      this.nextPageClick();
    } else {
      // Clicked on previous button
      this.prevPageClick();
    }
  }
  exportExcel() {
    this.showLoader = true;
    console.log(this.dataSource);
    const json_data = {
      data: this.encDecryService.encryptedData(this.reportData),
    };

    this.ReportsService.DownloadPyableLedgerTable(
      'payable_ledger',
      json_data
    ).subscribe({
      next: (data: any) => {
        const url = data['data'];
        window.open(url);
        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
        this.showLoader = false;
      },
    });
  }

  applyFilter(filterValue: string) {
    const filter = filterValue.toLowerCase().trim();

    if (this.dataSource) {
      this.dataSource.filter = filter;
    }
  }
  clearFilter() {
    this.refresh();
  }
}

export class ExampleDataSource {
  filterChange = new BehaviorSubject('');
  usersData: User[] = [];
  dialog: any;

  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];
}

export interface User {
  Sr_No: string;
  supplier_name: string;
  currency_code: string;
  txn_amount: string;
  payable_amount: string;
  addresses?: Address[] | MatTableDataSource<Address>;
}

export interface Address {
  Sr_No: string;
  transaction_ref_no: string;
  currency_code: string;
  txn_amount: string;
  //  payable_amount: string;
  comments?: Comment[] | MatTableDataSource<Comment>;
}

const USERS: User[] = [];
