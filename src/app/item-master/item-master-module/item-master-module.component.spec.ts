import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemMasterModuleComponent } from './item-master-module.component';

describe('ItemMasterModuleComponent', () => {
  let component: ItemMasterModuleComponent;
  let fixture: ComponentFixture<ItemMasterModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemMasterModuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemMasterModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
