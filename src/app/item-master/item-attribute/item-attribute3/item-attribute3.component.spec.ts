import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAttribute3Component } from './item-attribute3.component';

describe('ItemAttribute3Component', () => {
  let component: ItemAttribute3Component;
  let fixture: ComponentFixture<ItemAttribute3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemAttribute3Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemAttribute3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
