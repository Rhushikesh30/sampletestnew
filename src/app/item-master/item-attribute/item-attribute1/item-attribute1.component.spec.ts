import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAttribute1Component } from './item-attribute1.component';

describe('ItemAttribute1Component', () => {
  let component: ItemAttribute1Component;
  let fixture: ComponentFixture<ItemAttribute1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemAttribute1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemAttribute1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
