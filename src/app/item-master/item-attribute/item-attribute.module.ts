import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemAttributeRoutingModule } from './item-attribute-routing.module';
import { ItemAttributeComponent } from './item-attribute/item-attribute.component';
import { ItemAttribute1Component } from './item-attribute1/item-attribute1.component';
import { ItemAttribute2Component } from './item-attribute2/item-attribute2.component';
import { ItemAttribute3Component } from './item-attribute3/item-attribute3.component';
import { ItemAttribute4Component } from './item-attribute4/item-attribute4.component';
import { ItemAttribute5Component } from './item-attribute5/item-attribute5.component';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ComponentsModule } from '../../shared/components/components.module';
import { DynamicFormModule } from "../../shared/dynamic-form/dynamic-form.module";
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ItemAttributeComponent,
    ItemAttribute1Component,
    ItemAttribute2Component,
    ItemAttribute3Component,
    ItemAttribute4Component,
    ItemAttribute5Component
  ],
  imports: [
    CommonModule,
    ItemAttributeRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    DynamicFormModule,
    SharedModule
  ]
})
export class ItemAttributeModule { }
