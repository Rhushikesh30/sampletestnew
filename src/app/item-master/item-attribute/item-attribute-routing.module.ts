import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { ItemAttributeComponent } from './item-attribute/item-attribute.component';
import { ItemAttribute1Component } from './item-attribute1/item-attribute1.component';
import { ItemAttribute2Component } from './item-attribute2/item-attribute2.component';
import { ItemAttribute3Component } from './item-attribute3/item-attribute3.component';
import { ItemAttribute4Component } from './item-attribute4/item-attribute4.component';
import { ItemAttribute5Component } from './item-attribute5/item-attribute5.component';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'item-attribute',
    pathMatch: 'full'
  },
  {
    path: 'item-attribute',
    component: ItemAttributeComponent
  },
  


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemAttributeRoutingModule { }
