import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAttribute5Component } from './item-attribute5.component';

describe('ItemAttribute5Component', () => {
  let component: ItemAttribute5Component;
  let fixture: ComponentFixture<ItemAttribute5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemAttribute5Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemAttribute5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
