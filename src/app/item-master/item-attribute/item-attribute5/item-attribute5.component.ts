import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';

import Swal from 'sweetalert2';
import { CommonSetupService } from 'src/app/shared/services/common-setup.service';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { MatMenuTrigger } from '@angular/material/menu';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-item-attribute5',
  templateUrl: './item-attribute5.component.html',
  styleUrls: ['./item-attribute5.component.scss'],
  providers: [ErrorCodes],
})
export class ItemAttribute5Component
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  renderedData: any = [];
  screenName = 'Item Attribute5';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: DynamicFormService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  tableCols: any = [];
  tableData: any = [];
  ScreenJsonData: any = [];
  displayedColumns = [];
  BTN_VAL = 'Submit';
  showLoaderSubmit = false;
  form!: FormGroup;
  isOnlyMaster: any = true;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private dynamicService: DynamicFormService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private commonsetupservice: CommonSetupService
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Commodity Master')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
          console.log(this.sidebarData);
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.commonsetupservice
      .getDynamicScreenFormData(this.screenName)
      .subscribe({
        next: (data: any) => {
          this.ScreenJsonData = data;
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.ScreenJsonData)),
    });

    this.refresh();
    this.getAllData();
  }
  getAllData() {
    this.commonsetupservice
      .getDynamicScreenData(this.screenName)
      .subscribe((data: any) => {
        this.tableCols[0] = { mat_cell_name: 'View', mat_header_cell: 'View' };
        this.tableCols[1] = { mat_cell_name: 'Edit', mat_header_cell: 'Edit' };
        for (let i = 0; i < data['screenmatlistingcolumms_set'].length; i++) {
          this.tableCols[i + 2] = data['screenmatlistingcolumms_set'][i];

          console.log(
            'this.tableCols',
            this.tableCols[i + 1]['mat_header_cell']
          );
          if (this.tableCols[i + 2]['mat_header_cell'] == 'Is Active') {
            this.tableCols[i + 2]['mat_header_cell'] = 'Status';
          }
        }
        this.displayedColumns = this.tableCols.map((c: any) => c.mat_cell_name);
        this.tableData = data.screenmatlistingdata_set;
        for (let i = 0; i < this.tableData.length; i++) {
          this.tableData[i]['key'] = this.tableData[i]['name'];
          if (this.tableData[i]['is_active'] == true) {
            this.tableData[i]['is_active'] = 'Active';
          } else if (this.tableData[i]['is_active'] == false) {
            this.tableData[i]['is_active'] = 'Inactive';
          }
        }
      });
  }
  editViewRecord(row: any, flag: boolean) {
    if (flag == false) {
      this.rowData = row;
      this.showList = false;
      this.submitBtn = false;
      // this.listDiv = true;
    } else {
      this.rowData = row;
      this.showList = false;
      //this.listDiv = true;
      this.submitBtn = true;
      this.BTN_VAL = 'Update';
    }
  }

  showFormList(item: boolean) {
    // window.scrollTo(0, 0);
    this.rowData = [];
    this.BTN_VAL = 'Submit';
    if (item === false) {
      //this.listDiv = true;
      this.showList = false;
      this.submitBtn = true;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
  handleSave(formValue: any) {
    this.showLoaderSubmit = true;

    this.commonsetupservice.createScreenData(formValue).subscribe({
      next: (data: any) => {
        console.log('dataaaaa', data);

        if (data['status'] == 1) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.getAllData();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 2) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.getAllData();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
        }
      },
      error: (e) => {
        this.showLoaderSubmit = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DynamicFormService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    console.log(this.dataSource);
    console.log(typeof this.dataSource);
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    // key name with space add in brackets
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        Name: x.name,
        Description: x.description,
        Status: x.is_active,
      }));
    console.log(this.dataSource.filteredData);

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DynamicFormService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.exampleDatabase.dataChange,

      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables('Item Attribute5');
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['is_active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['is_active'] = 'Inactive';
          }
        }
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.name +
              advanceTable.description +
              advanceTable.is_active
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.filteredData.slice();
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'Uom Code':
          [propertyA, propertyB] = [a.uom_code, b.uom_code];
          break;
        case 'Description':
          [propertyA, propertyB] = [a.uom_description, b.uom_description];
          break;
        case 'Status':
          [propertyA, propertyB] = [a.is_active, b.is_active];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
