import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAttribute4Component } from './item-attribute4.component';

describe('ItemAttribute4Component', () => {
  let component: ItemAttribute4Component;
  let fixture: ComponentFixture<ItemAttribute4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemAttribute4Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemAttribute4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
