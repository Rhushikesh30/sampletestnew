import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAttribute2Component } from './item-attribute2.component';

describe('ItemAttribute2Component', () => {
  let component: ItemAttribute2Component;
  let fixture: ComponentFixture<ItemAttribute2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemAttribute2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemAttribute2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
