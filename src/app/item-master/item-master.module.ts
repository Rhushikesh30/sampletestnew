import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemMasterRoutingModule } from './item-master-routing.module';
import { ItemMasterComponent } from './item-master/item-master.component';

import { DynamicFormModule } from "../shared/dynamic-form/dynamic-form.module";
import { ComponentsModule } from '../shared/components/components.module';
import { SharedModule } from '../shared/shared.module';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { ItemMasterModuleComponent } from './item-master-module/item-master-module.component';
import { ItemQualityParameterComponent } from './item-quality-parameter/item-quality-parameter.component';
import { ItemPricingComponent } from './item-pricing/item-pricing.component';
//import { ItemAttributeComponent } from './item-attribute/item-attribute.component';


@NgModule({
  declarations: [
    ItemMasterComponent,
    ItemMasterModuleComponent,
    ItemQualityParameterComponent,
    ItemPricingComponent,
   // ItemAttributeComponent,
    
  ],
  imports: [
    CommonModule,
    ItemMasterRoutingModule,
    DynamicFormModule,
    ComponentsModule,
    SharedModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule
  ]
})
export class ItemMasterModule { }
