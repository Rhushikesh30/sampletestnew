import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, fromEvent, Observable, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';

import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { CommonSetupService } from 'src/app/shared/services/common-setup.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-item-quality-parameter',
  templateUrl: './item-quality-parameter.component.html',
  styleUrls: ['./item-quality-parameter.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class ItemQualityParameterComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  renderedData: any = [];
  screenName = 'Item Quality Parameter';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: DynamicFormService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  tableCols: any = [];
  tableData: any = [];
  ScreenJsonData: any = [];
  displayedColumns = [];
  showLoaderSubmit: any = false;
  BTN_VAL = 'Submit';
  form!: FormGroup;

  todayDate = new Date().toJSON().split('T')[0];

  constructor(
    private roleSecurityService: RoleSecurityService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private commonsetupservice: CommonSetupService,
    public datepipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.commonsetupservice
      .getDynamicScreenFormData(this.screenName)
      .subscribe((data) => {
        this.ScreenJsonData = data;
      });

    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.ScreenJsonData)),
    });

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Commodity Settings')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
    this.getAllData();
  }
  getAllData() {
    this.commonsetupservice
      .getDynamicScreenData(this.screenName)
      .subscribe((data: any) => {
        this.tableCols[0] = { mat_cell_name: 'View', mat_header_cell: 'View' };
        this.tableCols[1] = { mat_cell_name: 'Edit', mat_header_cell: 'Edit' };
        this.tableCols[2] = {
          mat_cell_name: 'Version',
          mat_header_cell: 'Version',
        };
        for (let i = 0; i < data['screenmatlistingcolumms_set'].length; i++) {
          this.tableCols[i + 3] = data['screenmatlistingcolumms_set'][i];

          if (this.tableCols[i + 3]['mat_header_cell'] == 'Is Active') {
            this.tableCols[i + 3]['mat_header_cell'] = 'Status';
          }
        }
        this.displayedColumns = this.tableCols.map((c: any) => c.mat_cell_name);
        this.tableData = data.screenmatlistingdata_set;
      });
  }

  createVersion(row: any, flag: boolean) {
    if (
      row.from_date == this.datepipe.transform(this.todayDate, 'yyyy-MM-dd')
    ) {
      Swal.fire({
        title: 'Not allowed',
        text: 'Cannot create version on same day!!',
        icon: 'warning',
      });
    } else {
      this.commonsetupservice
        .CheckVersionExist('Item Quality Parameter', row.id)
        .subscribe({
          next: (data: any) => {
            if (data['msg'] == 'Version already exists.') {
              Swal.fire({
                title: 'Not allowed',
                text: data['msg'] + '!!',
                icon: 'warning',
              });
            } else {
              this.BTN_VAL = 'Create Version';
              this.rowData = row;
              this.showList = false;
              this.listDiv = true;
              this.submitBtn = true;
            }
          },
          error: (e: any) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
    }
  }

  editViewRecord(row: any, flag: boolean) {
    if (flag == false) {
      this.rowData = row;
      this.showList = false;
      this.submitBtn = false;
      //  this.listDiv = true;
    } else {
      this.rowData = row;
      this.showList = false;
      //this.listDiv = true;
      this.submitBtn = true;
      this.BTN_VAL = 'Update';
    }
  }
  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  showFormList(item: boolean) {
    window.scrollTo(0, 0);
    this.rowData = [];
    this.BTN_VAL = 'Submit';
    if (item === false) {
      // this.listDiv = true;
      this.showList = false;
      this.submitBtn = true;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    if (this.BTN_VAL == 'Create Version') {
      formValue['create_version'] = true;
    }

    if (this.BTN_VAL == 'Update') {
      console.log(formValue);
      if (formValue.master_data[0].revision_status == 'Future') {
        formValue['update_version'] = true;
      }
    }

    this.showLoaderSubmit = true;
    this.commonsetupservice.createScreenData(formValue).subscribe(
      (data: any) => {
        if (data['status'] == 1) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 2) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 3) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your Version Created  successfully!',
              '',
              'success',
              false
            );
          }
        }
      },
      (error) => {
        this.showLoaderSubmit = false;
      }
    );
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DynamicFormService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Item Name': x.item_ref_id,
        'Quality Type': x.quality_type,
        'From Date': x.from_date,
        'To Date': x.to_date,
        'Revision Status': x.revision_status,
        Status: x.is_active,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DynamicFormService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables('Item Quality Parameter');

    return merge(...displayDataChanges).pipe(
      map(() => {
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['is_active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['is_active'] = 'Inactive';
          }
        }

        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.item_ref_id +
              advanceTable.quality_type +
              advanceTable.revision_status +
              advanceTable.from_date +
              advanceTable.to_date +
              advanceTable.is_active
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }
  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'Item':
          [propertyA, propertyB] = [a.item_ref_id, b.item_ref_id];
          break;
        case 'From Date':
          [propertyA, propertyB] = [a.from_date, b.from_date];
          break;
        case 'To Date':
          [propertyA, propertyB] = [a.to_date, b.to_date];
          break;
        case 'Revision Status':
          [propertyA, propertyB] = [a.revision_status, b.revision_status];
          break;
        case 'Quality Type':
          [propertyA, propertyB] = [a.quality_type, b.quality_type];
          break;
        case 'Status':
          [propertyA, propertyB] = [a.is_active, b.is_active];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
