import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemQualityParameterComponent } from './item-quality-parameter.component';

describe('ItemQualityParameterComponent', () => {
  let component: ItemQualityParameterComponent;
  let fixture: ComponentFixture<ItemQualityParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemQualityParameterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemQualityParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
