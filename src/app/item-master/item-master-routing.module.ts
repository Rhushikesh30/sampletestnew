import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemMasterComponent } from './item-master/item-master.component';
import { ItemMasterModuleComponent } from './item-master-module/item-master-module.component';
import { ItemAttributeModule } from './item-attribute/item-attribute.module'; // Import ItemAttributeModule
import { ItemQualityParameterComponent } from './item-quality-parameter/item-quality-parameter.component';
import { ItemPricingComponent } from './item-pricing/item-pricing.component';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'item',
  //   pathMatch: 'full'
  // },
  {
    path: 'item-master',
    component: ItemMasterComponent
  },
  {
    path: 'item-attribute',
    loadChildren: () =>
      import('./item-attribute/item-attribute.module').then((m) => m.ItemAttributeModule)
  },
  {

    path: 'item-quality-parameter',
    component: ItemQualityParameterComponent
  },
  {
    path: 'item-pricing',
    component: ItemPricingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemMasterRoutingModule { }
