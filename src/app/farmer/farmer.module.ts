import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ComponentsModule } from '../shared/components/components.module';
import { DynamicFormModule } from "../shared/dynamic-form/dynamic-form.module";

import { FarmerRoutingModule } from './farmer-routing.module';
import { FarmerComponent } from './farmer/farmer.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    FarmerComponent
  ],
  imports: [
    CommonModule,
    FarmerRoutingModule,
    ComponentsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule
  ]
})
export class FarmerModule { }
