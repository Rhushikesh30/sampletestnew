import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { SalesService } from 'src/app/shared/services/sales.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { GrnService } from 'src/app/shared/services/grn.service';

import { ToWords } from 'to-words';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sales-order-print',
  templateUrl: './sales-order-print.component.html',
  styleUrls: ['./sales-order-print.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class SalesOrderPrintComponent {
  showHeader = false;
  showFooter = false;
  tenant_name = localStorage.getItem('COMPANY_NAME');

  constructor(
    private grnService: GrnService,
    private route: ActivatedRoute,
    private encDecService: EncrDecrService,
    private errorCodes: ErrorCodes,
    private router: Router,
    private datePipe: DatePipe,
    private SalesOrderService: SalesService
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.queryParams['id']) {
      this.getAllData(this.route.snapshot.queryParams['id']);
    }
  }

  screenName = 'Sales Order';

  agent_name: any;
  customer_address_bill: any;
  customer_address_ship: any;
  customer_ref_id: any;
  customer_ref_type: any;
  gst_no: any;
  dispatch_no_of_bags: any;
  hsn_sac_no: any;
  invoice_date: any;
  mode_of_delivary_terms: any;
  mode_of_delivary: any;
  state_name: any;
  po_no: any;
  item_name: any;
  rate: any;
  quantity: any;
  payment_terms: any;
  cgst_rate: any;
  cgst_amount: any;
  sgst_rate: any;
  sgst_amount: any;
  taxabale_value: any;
  txn_currency_amount: any;
  sales_order_no: any;
  sales_order_date: any;
  transaction_date: any;
  transaction_ref_no: any;
  cancelFlag = true;
  customer_name: any;
  contact_person_name: any;
  so_ref_type: any;
  txn_currency_grand_total_amount: any;
  transportation_charges: any;
  tax_rate: any;
  total_tax_amount: any;

  totalAmount = 0;
  amount_in_word = '';

  itemData: any;
  allData: any[] = [];
  invoice_detials: any[] = [];
  itemDataList: any[] = [];

  header_image_type: any;
  header_image_logo: any;
  header_full_image = false;

  footer_image_type: any;
  footer_full_image = false;
  footer_image_logo = false;

  previewImage1: any;
  previewImage2 = '';

  NotFullImage = false;
  NotFullImageFooter = false;
  headerapplicable = false;
  footerapplicable = false;
  header_image_applicable = false;

  header_text_top_left = false;
  header_text_top_right = false;
  header_text_top_center = false;
  header_text_center_left = false;
  header_text_center_right = false;
  header_text_center_center = false;
  header_text_bottom_left = false;
  header_text_bottom_right = false;
  header_text_bottom_center = false;

  footer_image_top_left = false;
  footer_image_top_right = false;
  footer_image_top_center = false;
  footer_image_center_left = false;
  footer_image_center_right = false;
  footer_image_center_center = false;
  footer_image_bottom_left = false;
  footer_image_bottom_right = false;
  footer_image_bottom_center = false;

  footer_image_details: any;
  footer_image_position: any;
  footer_text_details: any;
  footer_text_position: any;
  header_image_details: any;
  header_image_position: any;
  header_text_details: any;
  header_text_position: any;

  headerText = '';
  headeratextpplicable = false;
  top_left = false;
  top_right = false;
  top_center = false;
  center_left = false;
  center_right = false;
  center_center = false;
  bottom_left = false;
  bottom_right = false;
  bottom_center = false;

  footertextpplicable = false;
  footerText = '';
  footer_text_top_left = false;
  footer_text_top_right = false;
  footer_text_top_center = false;
  footer_text_center_left = false;
  footer_text_center_right = false;
  footer_text_center_center = false;
  footer_text_bottom_left = false;
  footer_text_bottom_right = false;
  footer_text_bottom_center = false;

  customer_gst: any;

  getAllCompanyData(company_id: any) {
    this.grnService.getTenantById(company_id).subscribe({
      next: (edata: any) => {
        console.log(edata[0]['header_image_type']);

        this.header_image_type = edata[0]['header_image_type'];
        this.footer_image_type = edata[0]['footer_image_type'];

        if (edata.length > 0) {
          if (edata.header_href_attachment_path != '') {
            this.headerapplicable = true;

            if (edata[0]['header_href_attachment_path'] != '') {
              this.previewImage1 = this.encDecService.decryptedData(
                edata[0]['header_href_attachment_path']
              );
            }
            this.NotFullImage = false;
          }

          if (this.header_image_type == 'full_image') {
            this.headerapplicable = true;
            this.header_full_image = true;
            this.header_image_logo = false;
            this.NotFullImage = false;
          } else if (this.header_image_type == 'logo') {
            this.NotFullImage = true;
            this.headerapplicable = true;
            this.header_full_image = false;
            this.header_image_logo = true;
            if (edata[0]['header_image_position'] != '') {
              this.header_image_applicable = true;

              if (edata[0]['header_image_position'] == 'top-right') {
                this.header_text_top_right = true;
              }
              if (edata[0]['header_image_position'] == 'top-center') {
                this.header_text_top_center = true;
              }
              if (edata[0]['header_image_position'] == 'center-left') {
                this.header_text_center_left = true;
              }
              if (edata[0]['header_image_position'] == 'center-right') {
                this.header_text_center_right = true;
              }
              if (edata[0]['header_image_position'] == 'center-center') {
                this.header_text_center_center = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-left') {
                this.header_text_bottom_left = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-right') {
                this.header_text_bottom_right = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-center') {
                this.header_text_bottom_center = true;
              }
              if (edata[0]['header_image_position'] == 'top-left') {
                this.header_text_top_left = true;
              }
            }
          }

          if (edata[0]['header_text_details'] != '') {
            this.headerapplicable = true;
            this.NotFullImage = true;
            this.headerText = edata[0]['header_text_details'];
            this.headeratextpplicable = true;
            console.log(edata[0]['header_text_position'], 'Dataaaaaaaaaaaaa');

            if (edata[0]['header_text_position'] == 'top-left') {
              this.top_left = true;
            }
            if (edata[0]['header_text_position'] == 'top-right') {
              this.top_right = true;
            }
            if (edata[0]['header_text_position'] == 'top-center') {
              this.top_center = true;
            }
            if (edata[0]['header_text_position'] == 'center-left') {
              this.center_left = true;
            }
            if (edata[0]['header_text_position'] == 'center-right') {
              this.center_right = true;
            }
            if (edata[0]['header_text_position'] == 'center-center') {
              this.center_center = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-left') {
              this.bottom_left = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-right') {
              this.bottom_right = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-center') {
              this.bottom_center = true;
            }
          }

          if (edata[0]['footer_href_attachment_path'] != '') {
            if (edata[0]['footer_href_attachment_path'] != '') {
              this.previewImage2 = this.encDecService.decryptedData(
                edata[0]['footer_href_attachment_path']
              );
            }
            this.footerapplicable = true;
            this.NotFullImageFooter = false;
          }
          if (this.footer_image_type == 'full_image') {
            this.footerapplicable = true;
            this.footer_full_image = true;
            this.footer_image_logo = false;
            this.NotFullImageFooter = false;
          } else if (this.footer_image_type == 'logo') {
            this.footerapplicable = true;
            this.footer_full_image = false;
            this.footer_image_logo = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_image_position'] != '') {
              if (edata[0]['footer_image_position'] == 'top-left') {
                this.footer_image_top_left = true;
              }
              if (edata[0]['footer_image_position'] == 'top-right') {
                this.footer_image_top_right = true;
              }
              if (edata[0]['footer_image_position'] == 'top-center') {
                this.footer_image_top_center = true;
              }
              if (edata[0]['footer_image_position'] == 'center-left') {
                this.footer_image_center_left = true;
              }
              if (edata[0]['footer_image_position'] == 'center-right') {
                this.footer_image_center_right = true;
              }
              if (edata[0]['footer_image_position'] == 'center-center') {
                this.footer_image_center_center = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-left') {
                this.footer_image_bottom_left = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-right') {
                this.footer_image_bottom_right = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-center') {
                this.footer_image_bottom_center = true;
              }
            }
          }

          if (edata[0]['footer_text_details'] != '') {
            this.footerText = edata[0]['footer_text_details'];
            this.footertextpplicable = true;
            this.footerapplicable = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_text_position'] == 'top-left') {
              this.footer_text_top_left = true;
            }
            if (edata[0]['footer_text_position'] == 'top-right') {
              this.footer_text_top_right = true;
            }
            if (edata[0]['footer_text_position'] == 'top-center') {
              this.footer_text_top_center = true;
            }
            if (edata[0]['footer_text_position'] == 'center-left') {
              this.footer_text_center_left = true;
            }
            if (edata[0]['footer_text_position'] == 'center-right') {
              this.footer_text_center_right = true;
            }
            if (edata[0]['footer_text_position'] == 'center-center') {
              this.footer_text_center_center = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-left') {
              this.footer_text_bottom_left = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-right') {
              this.footer_text_bottom_right = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-center') {
              this.footer_text_bottom_center = true;
            }
          }
        }
      },
    });
  }
  getAllData(id: any) {
    this.SalesOrderService.getsalesPrintDataById(id).subscribe({
      next: (data: any) => {
        console.log('API DATAAAAAAAAAAAAAAAAAAAAAAAAA: ', data);
        this.allData = data;
        this.customer_address_bill = data.customer_address_bill;
        this.customer_address_ship = data.customer_address_ship;
        this.gst_no = data.gst_no;
        this.customer_name = data.customer_name;
        this.transaction_date = data.transaction_date;
        this.agent_name = data.agent_name;
        this.state_name = data.state_name;
        this.po_no = data.customer_po_number;
        this.invoice_date = data.customer_po_date;
        this.sales_order_no = data.sales_no;
        this.sales_order_date = data.so_date;
        this.mode_of_delivary_terms = data.mode_of_delivary_terms;
        this.mode_of_delivary = data.mode_of_delivary;
        this.payment_terms = data.payment_terms;
        this.contact_person_name = data.customer_contact_person_name;
        this.so_ref_type = data.so_ref_type;
        this.tenant_name = data.tenant_name;
        this.customer_gst = data.customer_gst;
        this.txn_currency_grand_total_amount =
          data.txn_currency_grand_total_amount;
        this.transportation_charges = data.transportation_charges;
        this.tax_rate = data.tax_rate;
        this.total_tax_amount = data.total_tax_amount;
        // this.cgst_rate=data.cgst_rate;
        // this.cgst_amount =data.cgst_amount;
        // this.sgst_rate=data.sgst_rate;
        // this.sgst_amount=data.sgst_amount;
        // this.total_tax_amount=data.total_tax_amount;
        // this.taxabale_value=data.taxabale_value;

        this.itemDataList = data.order_detials;
        for (let i = 0; i < this.itemDataList.length; i++) {
          this.itemDataList[i]['rate'] = parseFloat(
            this.itemDataList[i]['rate']
          ).toFixed(2);
        }
        // this.totalAmount =data.amount
        //= this.itemDataList.reduce((acc, itemData) => acc + itemData.txn_currency_amount, 0);

        const totalAmount = this.itemDataList.reduce(
          (total, itemData) => total + parseFloat(itemData.txn_currency_amount),
          0
        );
        this.totalAmount = totalAmount;

        this.getAllCompanyData(data['tenant_id']);
      },
    });
  }

  // convertToWords(amount: number) {
  //   const words = new ToWords().convert(amount, { currency: true });
  //   return `Rupees ${words.replace('Rupees', '')}`;
  // }

  convertToWords(amount: number) {
    if (!isNaN(amount)) {
      const words = new ToWords().convert(amount, { currency: true });
      return `Rupees ${words.replace('Rupees', '')}`;
    } else {
      return 'Invalid Amount';
    }
  }

  printComponent() {
    window.print();
  }

  Back() {
    window.close();
  }
}
