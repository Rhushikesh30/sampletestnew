import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesOrderPrintComponent } from './sales-order-print/sales-order-print.component';

const routes: Routes = [
  {
    path: 'sales-order',
    component: SalesOrderPrintComponent
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesOrderPrintRoutingModule { }
