import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesOrderPrintRoutingModule } from './sales-order-print-routing.module';
import { SalesOrderPrintComponent } from './sales-order-print/sales-order-print.component';


@NgModule({
  declarations: [
    SalesOrderPrintComponent
  ],
  imports: [
    CommonModule,
    SalesOrderPrintRoutingModule
  ]
})
export class SalesOrderPrintModule { }
