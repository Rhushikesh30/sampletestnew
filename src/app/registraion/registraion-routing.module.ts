import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SupplierComponent } from './supplier/supplier.component';
import { RegistrationComponent } from './registration/registration.component';
import { AgentComponent } from './agent/agent.component';
import { CustomerComponent } from './customer/customer.component';

const routes: Routes = [
  {
    path: 'agent-registration',
    component: AgentComponent,
  },
  {
    path: 'supplier-registration',
    component: SupplierComponent,
  },
  {
    path: 'customer-registration',
    component: CustomerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistraionRoutingModule {}
