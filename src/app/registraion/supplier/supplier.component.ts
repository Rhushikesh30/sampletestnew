import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { CommonSetupService } from 'src/app/shared/services/common-setup.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss'],
  providers: [ErrorCodes],
})
export class SupplierComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  renderedData: any = [];
  screenName = 'Supplier';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;

  exampleDatabase?: DynamicFormService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  tableCols: any = [];
  tableData: any = [];
  ScreenJsonData: any = [];
  displayedColumns = [];
  showLoaderSubmit: any = false;
  showLoaderSubmit1: any = false;
  showLoader1: any = false;
  BTN_VAL = 'Submit';
  form!: FormGroup;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private dynamicService: DynamicFormService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private commonsetupservice: CommonSetupService
  ) {
    super();
  }

  ngOnInit(): void {
    this.showLoaderSubmit1 = false;

    const userId = localStorage.getItem('user_id');

    this.commonsetupservice
      .getDynamicScreenFormData(this.screenName)
      .subscribe((data) => {
        this.ScreenJsonData = data;

        if (this.ScreenJsonData == '') {
          this.showLoaderSubmit1 = true;
        } else {
          this.showLoaderSubmit1 = false;
        }
      });

    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.ScreenJsonData)),
    });

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Registration')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
    this.getAllData();
  }

  getAllData() {
    this.commonsetupservice
      .getDynamicScreenData(this.screenName)
      .subscribe((data: any) => {
        this.tableCols[0] = { mat_cell_name: 'View', mat_header_cell: 'View' };
        this.tableCols[1] = { mat_cell_name: 'Edit', mat_header_cell: 'Edit' };
        for (let i = 0; i < data['screenmatlistingcolumms_set'].length; i++) {
          this.tableCols[i + 2] = data['screenmatlistingcolumms_set'][i];

          if (this.tableCols[i + 2]['mat_header_cell'] == 'Is Active') {
            this.tableCols[i + 2]['mat_header_cell'] = 'Status';
          }
        }
        this.displayedColumns = this.tableCols.map((c: any) => c.mat_cell_name);
      });
  }

  editViewRecord(row: any, flag: boolean) {
    if (flag == false) {
      this.rowData = row;
      this.showList = false;
      this.submitBtn = false;
      // this.listDiv = true;
    } else {
      this.rowData = row;
      this.showList = false;
      // this.listDiv = true;
      this.submitBtn = true;
      this.BTN_VAL = 'Update';
    }
  }
  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  showFormList(item: boolean) {
    window.scrollTo(0, 0);
    this.rowData = [];
    this.BTN_VAL = 'Submit';
    this.showLoaderSubmit1 = false;

    if (this.ScreenJsonData == '') {
      this.showLoaderSubmit1 = true;
    } else if (item === false || this.ScreenJsonData != '') {
      // this.listDiv = true;
      this.showList = false;
      this.submitBtn = true;
      this.showLoaderSubmit1 = false;
    } else {
      this.listDiv = false;
      this.showList = true;
      this.showLoaderSubmit1 = false;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
  handleSave(formValue: any) {
    this.showLoaderSubmit = true;
    this.commonsetupservice.createScreenData(formValue).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 2) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
        }
      },
      error: (e) => {
        this.showLoaderSubmit = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DynamicFormService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Supplier Code': x.vendor_code,
        'Company Name': x.company_name,
        'Email Id': x.email_id,
        'Phone Number': x.mobile_no,
        'Supplier Type': x.ref_type,
        Status: x.is_active,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }

  set filter(filter: string) {
    this.filterChange.next(filter);
  }

  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DynamicFormService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];

    this.exampleDatabase.getAllAdvanceTables('Supplier');
    return merge(...displayDataChanges).pipe(
      map(() => {
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['is_active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['is_active'] = 'Inactive';
          }
        }
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.company_name +
              advanceTable.email_id +
              advanceTable.mobile_no +
              advanceTable.status +
              advanceTable.ref_type +
              advanceTable.vendor_code
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'company_name':
          [propertyA, propertyB] = [a.company_name, b.company_name];
          break;
        case 'email_id':
          [propertyA, propertyB] = [a.email_id, b.email_id];
          break;
        case 'mobile_no':
          [propertyA, propertyB] = [a.mobile_no, b.mobile_no];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
