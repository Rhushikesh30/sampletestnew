import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistraionRoutingModule } from './registraion-routing.module';
import { SupplierComponent } from './supplier/supplier.component';
import { RegistrationComponent } from './registration/registration.component';


import { DynamicFormModule } from "../shared/dynamic-form/dynamic-form.module";
import { ComponentsModule } from '../shared/components/components.module';
import { SharedModule } from '../shared/shared.module';


import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { AgentComponent } from './agent/agent.component';
import { CustomerComponent } from './customer/customer.component';

@NgModule({
  declarations: [
    SupplierComponent,
    RegistrationComponent,
    AgentComponent,
    CustomerComponent
  ],
  imports: [
    CommonModule,
    RegistraionRoutingModule,
    DynamicFormModule,
    ComponentsModule,
    SharedModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule
  ]
})
export class RegistraionModule { }
