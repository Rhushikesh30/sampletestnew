import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockAdjustmentComponent } from './stock-adjustment/stock-adjustment.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: StockAdjustmentComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
