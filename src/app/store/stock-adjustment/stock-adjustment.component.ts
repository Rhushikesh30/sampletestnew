// import { Component } from '@angular/core';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { ThemePalette } from '@angular/material/core';
import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { StockAdjustmentService } from 'src/app/shared/services/stock-adjustment.service';

@Component({
  selector: 'app-stock-adjustment',
  templateUrl: './stock-adjustment.component.html',
  styleUrls: ['./stock-adjustment.component.scss'],
  providers: [ErrorCodes],
})
export class StockAdjustmentComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  displayedColumns: any = ['actions', 'transaction_date'];
  renderedData: any = [];
  screenName = 'Stock Adjustmet';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;
  rolename: any;

  exampleDatabase?: StockAdjustmentService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  constructor(
    private roleSecurityService: RoleSecurityService,
    private StockAdjustmentService: StockAdjustmentService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.rolename = rolename[0];

    const userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Purchase Order')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          console.log();

          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;
    const json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.StockAdjustmentService.create(json_data, formValue['id']).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 2) {
          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e) => {
        console.log(e);

        this.showLoader = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }
  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new StockAdjustmentService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'JV Number': x.transaction_ref_no,
        'Currency Name': x.currency_name,
        'Total Transaction Debit Amount': x.txn_currency_debit_amount,
        'Total Transaction Credit Amount': x.txn_currency_credit_amount,
        'Total Base Debit Amount': x.base_currency_debit_amount,
        'Total Base Credit Amount': x.base_currency_credit_amount,
        'Posting Status': x.posting_status,
      }));

    TableExportUtil.exportToExcel(exportData, 'Journal Voucher Details');
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: StockAdjustmentService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.currency_name +
              advanceTable.posting_status +
              advanceTable.txn_currency_debit_amount +
              advanceTable.txn_currency_credit_amount +
              advanceTable.base_currency_debit_amount +
              advanceTable.base_currency_credit_amount
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'district_name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'taluka_name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
