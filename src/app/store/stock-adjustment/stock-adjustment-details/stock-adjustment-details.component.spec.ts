import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAdjustmentDetailsComponent } from './stock-adjustment-details.component';

describe('StockAdjustmentDetailsComponent', () => {
  let component: StockAdjustmentDetailsComponent;
  let fixture: ComponentFixture<StockAdjustmentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockAdjustmentDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StockAdjustmentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
