import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MAT_DATE_FORMATS } from '@angular/material/core';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { JournalVoucherBookingService } from 'src/app/shared/services/journal-voucher-booking.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { StockAdjustmentService } from 'src/app/shared/services/stock-adjustment.service';
export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-stock-adjustment-details',
  templateUrl: './stock-adjustment-details.component.html',
  styleUrls: ['./stock-adjustment-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class StockAdjustmentDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  cancelFlag = true;

  StockAdjForm!: FormGroup;

  todayDate = new Date().toJSON().split('T')[0];
  ItemTypeData: any = [];
  CurrencyData: any = [];
  ItemData: any = [];
  filterItemData: any;
  uomData: any = [];
  requestData: any = [];
  btnVal = 'Submit';

  constructor(
    private formBuilder: FormBuilder,
    private errorCodes: ErrorCodes,
    public datepipe: DatePipe,
    private StockAdjustmentService: StockAdjustmentService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.viewEditRecord(this.rowData);
    }

    // this.StockAdjustmentService.getDynamicData('item_master', 'name').subscribe({
    //   next: (data: any) => {
    //     this.ItemData = data;
    //     this.filterItemData = this.ItemData.slice();

    //   },
    //   error: (e) => {
    //     this.showSwalMassage(
    //       this.errorCodes.getErrorMessage(JSON.parse(e).status),
    //       'error'
    //     );
    //   },
    // });

    this.StockAdjustmentService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.uomData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.StockAdjustmentService.getAllMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.ItemTypeData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  ItemTypeChange(item_id: any) {
    this.StockAdjForm.get('stock_adjustment_details')?.reset();
    let gridRow = <FormArray>this.StockAdjForm.get('stock_adjustment_details');
    if (gridRow.length > 1) {
      for (let index = gridRow.length - 1; index > 0; index--) {
        this.deleteRow(index);
      }
    }

    // this.ItemData = this.itemRefId.filter((d:any)=> d.item_type== item_id)
    this.filterItemData;
  }

  initializeForm() {
    this.StockAdjForm = this.formBuilder.group({
      id: [''],
      transaction_date: [''],
      fiscal_year: [''],
      period: [''],
      item_type_ref_id: [''],
      txn_currency: [''],
      total_txn_currency: [''],
      stock_adjustment_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      is_adjustment_with_qty_amt: [''],
      item_ref_id: [''],
      uom: [''],
      system_stock_quantity: [''],
      rate: [''],
      alternate_uom: [''],
      system_stock_alternate_quantity: [''],
      alternate_rate: [''],
      physical_stock_uom: [''],
      physical_stock_qty: [''],
      physical_stock_alternate_uom: [''],
      physical_stock_alternate_qty: [''],
      suplus_shortage: [''],
      request: [''],
      damaged_stock_uom: [''],
      damaged_stock_qty: [''],
      damaged_stock_alternate_uom: [''],
      damaged_stock_alternate_qty: [''],
    });
  }

  get formArray() {
    return this.StockAdjForm.get('stock_adjustment_details') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  viewEditRecord(row1: any) {
    // this.StockAdjustmentService.getJVDataById(row1.id).subscribe({
    //   next: (row: any) => {
    //     console.log(row);
    //     row=row[0]
    //     this.JVForm.patchValue({
    //       id: row.id,
    //       transaction_id:row.transaction_id,
    //       transaction_ref_no:row.transaction_ref_no,
    //       transaction_type_id:row.transaction_type_id,
    //       period:row.period,
    //       ledger_group:row.ledger_group,
    //       fiscal_year:row.fiscal_year,
    //       created_by:row.created_by,
    //       base_currency:row.base_currency,
    //       transaction_date: row.transaction_date,
    //       txn_currency:row.txn_currency,
    //       tenant_id:row.tenant_id,
    //       is_active: row.is_active,
    //       is_deleted: row.is_deleted,
    //       ref_transaction_id:row.ref_transaction_id,
    //       ref_transaction_type_id:row.ref_transaction_type_id,
    //       conversion_rate:row.conversion_rate,
    //       txn_currency_debit_amount:row.txn_currency_debit_amount,
    //       txn_currency_credit_amount:row.txn_currency_credit_amount,
    //       base_currency_debit_amount:row.base_currency_debit_amount,
    //       base_currency_credit_amount:row.base_currency_credit_amount,
    //       posting_status:row.posting_status,
    //       posting_ref_no:row.posting_ref_no,
    //       is_temporary_flag:row.is_temporary_flag
    //     });
    //    console.log(this.txn_currency);
    //     let ItemRow = row.journal_voucher_details.filter(function (data: any) {
    //       return data.is_deleted == false && data.is_active == true;
    //     });
    //    this.JVForm.setControl('journal_voucher_details', this.setExistingArray(ItemRow));
    //   }})
    //   if (this.submitBtn) {
    //  //   this.btnVal = 'Update';
    //   if (this.JVForm.get('is_temporary_flag')?.value== true) {
    //     this.btnVal = 'Temporary Save';
    //   }
    //   else {
    //     this.btnVal = 'Permanent Save';
    //   }
    //   }
    //   else {
    //     this.JVForm.disable();
    //   }
  }

  onSubmit() {
    if (this.StockAdjForm.invalid) {
      this.showSwalMassage('Please Fill All Required Fields', 'error');
    } else {
      // for (var i = 0; i < (<FormArray>(this.StockAdjForm.get('contract_details'))).length; i++) {
      //   let gridrow1 = (<FormArray>(this.StockAdjForm.get('contract_details'))).at(i)
      //   let fromdate = gridrow1.get('from_date')?.value
      //   let todate = gridrow1.get('to_date')?.value
      //   let fromdate1 = this.datepipe.transform(fromdate, 'yyyy-MM-dd')
      //   let todate1 = this.datepipe.transform(todate, 'yyyy-MM-dd')
      //   gridrow1.get('from_date')?.setValue(fromdate1)
      //   gridrow1.get('to_date')?.setValue(todate1)

      // }

      this.handleSave.emit(this.StockAdjForm.value);
    }
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  onResetForm() {
    this.initializeForm();
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.StockAdjForm.reset();
    this.handleCancel.emit(false);
  }
}
