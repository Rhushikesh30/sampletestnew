import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { AuthService } from 'src/app/core/service/auth.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [ErrorCodes]
})
export class ForgotPasswordComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit {
  loginForm!: FormGroup;
  submitted = false;
  error = '';

  showLoader = false;
  isForgotScreen:boolean = true;
  hideOtp = true;
  hidePassword = true;
  hidecPassword = true;

  constructor(private errorCodes: ErrorCodes,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private encrDecrService: EncrDecrService
  ) {
    super();
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      otp: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confirmpassword: [null, [Validators.required]],
      profile: [null, [Validators.required]]
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.showLoader = true;
    this.error = '';
    this.loginForm.get('otp')?.clearValidators(); 
    this.loginForm.get('otp')?.updateValueAndValidity();

    this.loginForm.get('password')?.clearValidators(); 
    this.loginForm.get('password')?.updateValueAndValidity();

    this.loginForm.get('confirmpassword')?.clearValidators(); 
    this.loginForm.get('confirmpassword')?.updateValueAndValidity();

    this.loginForm.get('profile')?.clearValidators(); 
    this.loginForm.get('profile')?.updateValueAndValidity();

    if (this.loginForm.invalid) {
      this.error = 'Incorrect Username';
    } else {
      
      this.authService.ResetPassword(this.encrDecrService.encryptedData(this.loginForm.get('username')?.value)).subscribe({
        next: (res:any) => {
          if (this.encrDecrService.decryptedData(res['status'])=='Success'){
            this.loginForm.get('otp')?.clearValidators(); 
            this.loginForm.get('otp')?.updateValueAndValidity();
            this.loginForm.get('password')?.clearValidators(); 
            this.loginForm.get('password')?.updateValueAndValidity();
            this.loginForm.get('confirmpassword')?.clearValidators(); 
            this.loginForm.get('confirmpassword')?.updateValueAndValidity();
            this.loginForm.get('profile')?.clearValidators(); 
            this.loginForm.get('profile')?.updateValueAndValidity();
            this.isForgotScreen = false; 
            this.loginForm.get('profile')?.setValue(res['profile'])
            this.loginForm.get('otp')?.setValue('')    
            this.loginForm.get('password')?.setValue('')    
            this.loginForm.get('confirmpassword')?.setValue('')    
            this.showLoader = false;
          } else {
            
            this.showLoader = false;
            this.error = 'Something went Wrong... Please try again!';
          } 
        },
        error:(error) => {
          this.showLoader = false;
          this.error = 'Incorrect Username';
          // this.error = error.error;
        }
      })
    }
  }

  updatePassword(){
    this.showLoader = true;
    this.loginForm.get('otp')?.updateValueAndValidity();
    this.loginForm.get('password')?.updateValueAndValidity();
    this.loginForm.get('confirmpassword')?.updateValueAndValidity();
    this.loginForm.get('profile')?.updateValueAndValidity();

    let fotp = this.loginForm.get('otp')?.value

    // let otp = this.authService.encryptedData(this.loginForm.get('otp')?.value)
    let otp = this.loginForm.get('otp')?.value
    let password = this.loginForm.get('password')?.value
    let profile = this.loginForm.get('profile')?.value
    let username = this.loginForm.get('username')?.value   
    // let username = this.authService.encryptedData(this.loginForm.get('username')?.value)    
    let confirmpassword = this.loginForm.get('confirmpassword')?.value

    if (otp==''&& password==''&&confirmpassword==''){
      this.showLoader = false;
      this.error = 'Please Provide details!';
    }
    if(password === confirmpassword){
      let userData = {
        'password': this.encrDecrService.encryptedData(password),
        'otp': this.encrDecrService.encryptedData(otp),
        'profile': profile,
        'username': this.encrDecrService.encryptedData(username)
      }
      this.authService.UpdatePassword(userData).subscribe({
        next: (res:any) => {
          if (this.encrDecrService.decryptedData(res['status'])=="Success") {
            this.showLoader = false;
            Swal.fire({
              title: 'Password Updated Successfully',
              icon: 'success',
              timer: 2000,
              showConfirmButton: false
            });
            this.router.navigate(['']).then(() => {
              setTimeout(function() {window.location.reload();} , 2000);
            });  
          }else if(this.encrDecrService.decryptedData(res['status'])=="Expired"){
              this.showLoader = false;
              this.error = 'Time Out... Please try again!';
              this.router.navigate(['']).then(() => {
                setTimeout(function() {window.location.reload();} , 2000);
              });  
            }else{
             this.error = 'Something went Wrong... Please try again!';
          }
        },
        error:(error) => {
          this.showLoader = false;
          this.error = error.error;
        }
      })
    }else{
      this.showLoader = false;
      this.error = 'Password Not Match!';     
    }
  }

}
