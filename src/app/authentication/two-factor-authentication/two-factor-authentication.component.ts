import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService, DataService } from 'src/app/core/service/auth.service';
import { Router } from '@angular/router';

import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-two-factor-authentication',
  templateUrl: './two-factor-authentication.component.html',
  styleUrls: ['./two-factor-authentication.component.scss'],
  providers: [ErrorCodes],
})
export class TwoFactorAuthenticationComponent implements OnInit {
  loginForm!: FormGroup;
  submitted = false;
  returnUrl!: string;
  hide = true;
  error = '';
  errorAttempts = '';
  code_token: any;
  userData: any;
  errorAttemptsCount = 4;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private dataService: DataService,
    private errorCodes: ErrorCodes,
    private encrDecrService: EncrDecrService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      OTP: ['', [Validators.required]],
    });

    this.dataService.data.subscribe({
      next: (response: any) => {
        this.userData = response;
        if (response == 'Initial Value') {
          this.router.navigate(['']);
        }
      },
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  resendCode() {
    this.authService.resendCode().subscribe({
      next: (data: any) => {
        this.authService.setCodeToken(data['token']);
      },
    });
    this.code_token = this.authService.getCodeToken();
  }

  login(otp: any) {
    this.error = '';
    this.errorAttempts = '';
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.error = 'Unable to login! Please Enter OTP';
      this.submitted = false;
    } else {
      this.userData['otp'] = this.encrDecrService.encryptedData(otp);

      this.authService.login(this.userData).subscribe({
        next: (success: any) => {
          this.submitted = false;
          if (success['status'] === 'Success') {
            this.router.navigate(['/home']).then(() => {
              window.location.reload();
            });
          } else {
            this.error = success['details'];
            if (success['error_code'] == 449) {
              this.errorAttemptsCount = this.errorAttemptsCount - 1;
              this.errorAttempts =
                'Your Remaining Attempts are ' + this.errorAttemptsCount;
            } else {
              this.dataService.sendData(null);
              this.errorAttempts = 'Please try after sometime';
              // this.router.navigate([''])
            }
          }
        },
        error: (error) => {
          this.submitted = false;
          this.showSwalMessage(
            this.errorCodes.getErrorMessage(JSON.parse(error).status)
          );
        },
      });
    }
  }

  showSwalMessage(title: any) {
    Swal.fire({
      title: title,
      icon: 'error',
      timer: 2000,
      showConfirmButton: false,
    });
  }
}
