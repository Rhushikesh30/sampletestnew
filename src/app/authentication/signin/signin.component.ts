import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AuthService, DataService } from 'src/app/core/service/auth.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers: [ErrorCodes],
})
export class SigninComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  loginForm!: UntypedFormGroup;
  submitted = false;
  error = '';
  hide = true;
  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private authService: AuthService,
    private errorCodes: ErrorCodes,
    private dataService: DataService,
    private encrDecrService: EncrDecrService
  ) {
    super();
  }
  ngOnInit() {
    this.authService.getLicenseKey().subscribe((licensekey: any) => {
      if (licensekey.status === 'not found' || licensekey.status === '')
        this.licenselogout();
    });

    this.loginForm = this.formBuilder.group({
      username: [
        '',
        [Validators.required, Validators.email, Validators.minLength(5)],
      ],
      password: ['', Validators.required],
      profile: [],
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    this.error = '';

    if (this.loginForm.invalid) {
      this.error = 'Username and Password not valid !';
      this.submitted = false;
    } else {
      this.subs.sink = this.authService
        .code(
          this.encrDecrService.encryptedData(this.form['username'].value),
          this.encrDecrService.encryptedData(this.form['password'].value)
        )
        .subscribe({
          next: (res: any) => {
            this.submitted = false;
            if (res['status'] == 'Success') {
              this.loginForm.value.profile = this.encrDecrService.encryptedData(
                res['profile']
              );
              this.loginForm.value.username =
                this.encrDecrService.encryptedData(this.form['username'].value);
              this.loginForm.value.password =
                this.encrDecrService.encryptedData(this.form['password'].value);
              this.dataService.sendData(this.loginForm.value);
              this.router.navigate(['authentication/2fa']);
            } else {
              this.error = 'Invalid Username or Password';
            }
          },
          error: (e) => {
            this.error = this.errorCodes.getErrorMessage(JSON.parse(e).status);
            this.submitted = false;
          },
        });
    }
  }

  licenselogout() {
    this.subs.sink = this.authService.logout().subscribe((res) => {
      if (!res.success) {
        this.router.navigate(['/authentication/locked']);
      }
    });
  }

  showSwalMessage(title: any) {
    Swal.fire({
      title: title,
      icon: 'error',
      timer: 2000,
      showConfirmButton: false,
    });
  }
}
