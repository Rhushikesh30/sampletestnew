import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from 'src/app/core/service/auth.service';

@Component({
  selector: 'app-locked',
  templateUrl: './locked.component.html',
  styleUrls: ['./locked.component.scss'],
})
export class LockedComponent implements OnInit {
  licenseForm!: UntypedFormGroup;
  submitted = false;
  hide = true;
  error = ''
  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}
  ngOnInit() {
    this.licenseForm = this.formBuilder.group({
      licensekey: ['', Validators.required],
    });
  }
  get form(): { [key: string]: AbstractControl } {
    return this.licenseForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    this.error = '';
    // stop here if form is invalid
    if (this.licenseForm.invalid) {
      this.error = 'Enter valid License Key!!!';
      return;
    } else {
      this.authService
        .saveLicenseKey(this.licenseForm.value)
        .subscribe(
          (res:any) => {
            if (res.status == 'ok') {
              this.router.navigate(['']);
            } else {
              this.error = res.status;
            }
          },
          (error) => {
            this.error = error;
            this.submitted = false;
          }
        );
    }
  }
}
