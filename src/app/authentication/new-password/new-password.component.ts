import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/service/auth.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import Swal from "sweetalert2";
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss'],
  providers: [ErrorCodes]
})


export class NewPasswordComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit {
  newPasswordForm!: FormGroup;
  loginForm!: FormGroup;
  submitted: boolean = false;
  PasswordHide: boolean = true;
  confirmPasswordHide: boolean = true;
  oldPasswordHide: boolean = true;
  error = '';
  code_token!: string;
  RouterForgottenPasswordToken!: string;
  RouterForgottenPasswordUsername!: string;

  showLoader = false;
  hideExistingPassword:boolean  = true;
  hideNewPassword:boolean  = true;
  hideNewCPassword:boolean  = true;

  constructor(private errorCodes: ErrorCodes,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    super();
  }

  ngOnInit(){
    this.loginForm = this.formBuilder.group({
      oldpassword:['',[Validators.required]],
      password: ['',[Validators.required]],
      confirmpassword: ['',[Validators.required]],
    });
  }

  showSwalmessage(message:any, text:any, icon:any, confirmButton:any): void {
    if (! confirmButton) {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
    } else {
        Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
    }
  }

  get f() {
    return this.loginForm.controls;
  }

  homePage(){
    let isPasswordChanged = localStorage.getItem('is_password_changed')
    if(isPasswordChanged=='True'){
      this.router.navigate(['/dashboard/main']).then(() => {window.location.reload();});
    } 
  }

  onSubmit() {
    this.error = ''
    this.showLoader = true;
    if (this.loginForm.invalid){
      this.showLoader = false;
      this.error = 'Unable to process! Please Enter input'
    } else {
      let oldpassword = this.loginForm.get('oldpassword')?.value
      let password = this.loginForm.get('password')?.value
      let confirmpassword = this.loginForm.get('confirmpassword')?.value
      if (password!=confirmpassword){
        this.error = 'New Password and Confirm Password Not Match'
        this.showLoader = false;
        return
      }
      if (oldpassword==password){
        this.error = 'Old Password and New Password Same'
        this.showLoader = false;
        return
      }
      let payload ={
        'oldpassword': oldpassword,
        'password': password,
        'confirmpassword': confirmpassword
      }
      this.authService.UpdatePassword(payload).subscribe({
        next: (success:any) => {
          if (success['status']==='Success'){
            localStorage.setItem('is_password_changed', 'False')
            this.showSwalmessage('Your Password has been updated successfully!','','success',false)
            this.router.navigate(['/dashboard/main']).then(() => {window.location.reload();});
          }else{
            this.error = success['details'];
          }
          this.showLoader = false;
        },
        error:(error) => {
          this.showLoader = false;
          this.error = error.error;
        }
      })
    }
  }

}
