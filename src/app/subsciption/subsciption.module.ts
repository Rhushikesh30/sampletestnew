import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubsciptionComponent } from './subsciption.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'subsciption',
    pathMatch: 'full',
  },
  {
    path: '',
    component: SubsciptionComponent,
  },
];

@NgModule({
  declarations: [SubsciptionComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class SubsciptionModule {}
