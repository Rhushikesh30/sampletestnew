import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsciptionComponent } from './subsciption.component';

describe('SubsciptionComponent', () => {
  let component: SubsciptionComponent;
  let fixture: ComponentFixture<SubsciptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsciptionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SubsciptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
