import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { ToWords } from 'to-words';
interface Feature {
  key: any;
  label: any;
}
interface Subscription {
  is_most_popular: any;
  price: number;
  total_user: any;
  farmer_member_base: any;
  standard: any;
  [key: string]: string | number | boolean;
}
@Component({
  selector: 'app-subsciption',
  templateUrl: './subsciption.component.html',
  styleUrls: ['./subsciption.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class SubsciptionComponent {
  constructor(
    private route: ActivatedRoute,
    private encDecService: EncrDecrService,
    private errorCodes: ErrorCodes,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    console.log();
  }

  is_most_popular!: false;

  total_user: any;
  farmer_member_base: any;
  sales: any;
  purchase: any;
  reports: any;
  others: any;
  management: any;
  amount: any;

  featuresData: Feature[] = [
    { key: 'total_user', label: 'Total Users' },
    { key: 'farmer_member_base', label: 'Farmer Member Base' },
    { key: 'sales', label: 'Sales' },
    { key: 'purchase', label: 'Purchase' },
    { key: 'reports', label: 'Reports' },
    { key: 'others', label: 'Others (Electricity, Rent data, etc)' },
    { key: 'management', label: 'Management (HR, Finance, Compliance, etc)' },
  ];

  subcription_Data: Subscription[] = [
    {
      is_most_popular: false,
      price: 6990,
      total_user: '100 user',
      farmer_member_base: 200,
      standard: 'Standard',
      sales: 'check',
      purchase: 'check',
      reports: 'check',
      others: 'dash',
      management: 'dash',
    },
    {
      is_most_popular: true,
      price: 8990,
      total_user: '20 user',
      farmer_member_base: 500,
      standard: 'Professional',
      sales: 'check',
      purchase: 'check',
      reports: 'check',
      others: 'check',
      management: 'dash',
    },
    {
      is_most_popular: false,
      price: 12490,
      total_user: '50 user',
      farmer_member_base: 1000,
      standard: 'Premium',
      sales: 'check',
      purchase: 'check',
      reports: 'check',
      others: 'check',
      management: 'check',
    },
  ];

  // featuresData: { key: string; label: string }[] = [
  //   { key: 'total_users', label: 'Total Users' },
  //   { key: 'farmer_member_base', label: 'Farmer Member Base' },
  //   { key: 'sales', label: 'Sales' },
  //   { key: 'purchase', label: 'Purchase' },
  //   { key: 'reports', label: 'Reports' },
  //   { key: 'others', label: 'Others (Electricity, Rent data, etc)' },
  //   { key: 'management', label: 'Management (HR, Finance, Compliance, etc)' },
  // ];

  // subcription_Data: any = [
  //   {
  //     is_most_popular: false,
  //     price: 6990,
  //     total_user: '100 user',
  //     farmer_member_base: 200,
  //     standard: 'Standard',
  //     sales: 'check',
  //     purchase: 'check',
  //     reports: 'check',
  //     others: 'dash',
  //     management: 'dash',
  //   },
  //   {
  //     is_most_popular: true,
  //     price: 8990,
  //     total_user: '20 user',
  //     farmer_member_base: 500,
  //     standard: 'Professional',
  //     sales: 'check',
  //     purchase: 'check',
  //     reports: 'check',
  //     others: 'check',
  //     management: 'dash',
  //   },
  //   {
  //     is_most_popular: false,
  //     price: 12490,
  //     total_user: '50 user',
  //     farmer_member_base: 1000,
  //     standard: 'Premium',
  //     sales: 'check',
  //     purchase: 'check',
  //     reports: 'check',
  //     others: 'check',
  //     management: 'check',

  //     management1: 'hELLO',
  //   },
  // ];
}
