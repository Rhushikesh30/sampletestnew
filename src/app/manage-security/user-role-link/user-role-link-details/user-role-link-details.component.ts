import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormArray,
  UntypedFormGroup,
} from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { UserRoleService } from 'src/app/shared/services/user-role.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-role-link-details',
  templateUrl: './user-role-link-details.component.html',
  styleUrls: ['./user-role-link-details.component.scss'],
  providers: [ErrorCodes],
})
export class UserRoleLinkDetailsComponent implements OnInit {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() screenName: any;
  userroleForm!: UntypedFormGroup;

  UserRole: any = [];
  UserName: any = [];
  tenantNames: any = [];
  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  btnVal = 'Submit';

  constructor(
    private formBuilder: FormBuilder,
    private userroleservice: UserRoleService,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit(): void {
    this.initializeForm();

    this.userroleservice.getUserData('except').subscribe({
      next: (data: any) => {
        this.UserName = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.userroleservice.getRoleData('all').subscribe({
      next: (data: any) => {
        this.UserRole = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    if (this.rowData.length >= 1) {
      this.viewRecord();
    }
  }

  initializeForm() {
    this.userroleForm = this.formBuilder.group({
      id: [''],
      user: ['', [Validators.required]],
      initialitemRow: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.userroleForm.reset();
      this.handleCancel.emit(false);
    }
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      user: ['', Validators.required],
      role: ['', Validators.required],
      tenant: [''],
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  viewRecord() {
    let edata: any = this.rowData;

    this.userroleservice.getUserAllData().subscribe({
      next: (data: any) => {
        this.UserName = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.userroleForm.patchValue({
      id: edata[0].user_id,
      user: edata[0].emp_id,
    });

    if (edata.length >= 1) {
      this.userroleForm.setControl(
        'initialitemRow',
        this.setExistingArray(edata)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
      this.showReset = false;
      this.userroleForm.controls['user'].disable();
    } else {
      this.userroleForm.disable();
    }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          user: element.tenant_id,
          role: element.role_id,
          tenant: element.tenant_id,
        })
      );
    });
    return formArray;
  }

  get formArray() {
    return this.userroleForm.get('initialitemRow') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  onSubmit() {
    this.userroleForm.enable();

    // this.userroleForm.get('initialitemRow')?.value.forEach((ele:any)=>{

    //   ele.user = this.userroleForm.value.user;
    //   ele.tenant = localStorage.getItem('COMPANY_ID');
    // })
    var gridRow = <FormArray>this.userroleForm.get('initialitemRow');
    for (let i = 0; i < gridRow.length; i++) {
      gridRow.at(i).get('user')?.setValue(this.userroleForm.value.user);
      gridRow.at(i).get('tenant')?.setValue(localStorage.getItem('COMPANY_ID'));
    }
    console.log();
    if (this.userroleForm.invalid) {
      return;
    } else {
      this.handleSave.emit(this.userroleForm.value.initialitemRow);
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.userroleForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }
}
