import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRoleLinkDetailsComponent } from './user-role-link-details.component';

describe('UserRoleLinkDetailsComponent', () => {
  let component: UserRoleLinkDetailsComponent;
  let fixture: ComponentFixture<UserRoleLinkDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserRoleLinkDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserRoleLinkDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
