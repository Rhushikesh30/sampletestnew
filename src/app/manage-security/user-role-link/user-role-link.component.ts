import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';

import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { MatMenuTrigger } from '@angular/material/menu';
import { ThemePalette } from '@angular/material/core';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';

import { UserRoleLinkInterface } from 'src/app/shared/models/user-role-link.interface';
import { UserRoleService } from 'src/app/shared/services/user-role.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-user-role-link',
  templateUrl: './user-role-link.component.html',
  styleUrls: ['./user-role-link.component.scss'],
  providers: [ErrorCodes],
})
export class UserRoleLinkComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild('dataSourcePaginator', { read: MatPaginator })
  dataSourcePaginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  //dataSource!: MatTableDataSource<UserRoleLinkInterface>;
  resultData!: UserRoleLinkInterface[];
  displayedColumns = ['view', 'edit', 'user_name', 'role_name'];
  // renderedData!: UserRoleLinkInterface[];
  renderedData: any = [];
  screenName = 'User Role Link';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  disableView: any = [];
  disableEdit: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: UserRoleService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  constructor(
    private roleSecurityService: RoleSecurityService,
    private userroleservice: UserRoleService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Manage Security')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.userroleservice.getUserFormData(row.user_id).subscribe({
      next: (data: any) => {
        this.rowData = data;
        this.showList = false;
        this.submitBtn = flag;
        // this.listDiv = true;
        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  showFormList(item: boolean) {
    if (item === false) {
      //this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: boolean
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;

    this.userroleservice.createUserRoleLink(formValue).subscribe({
      next: (data: any) => {
        if (data['status'] == 'Record Updated Successfully') {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 'Record Created Successfully') {
          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e) => {
        this.showLoader = false;
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new UserRoleService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'User Name': x.user_name,
        'Role Name': x.role_name,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: UserRoleService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllUserRoleLinkData();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.user_name + advanceTable.role_name
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'user_name':
          [propertyA, propertyB] = [a.user_name, b.user_name];
          break;
        case 'role_name':
          [propertyA, propertyB] = [a.role_name, b.role_name];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
