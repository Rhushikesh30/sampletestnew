import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRoleLinkComponent } from './user-role-link.component';

describe('UserRoleLinkComponent', () => {
  let component: UserRoleLinkComponent;
  let fixture: ComponentFixture<UserRoleLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserRoleLinkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserRoleLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
