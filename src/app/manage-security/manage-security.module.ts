import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";

import { ComponentsModule } from "../shared/components/components.module";

import { UserRoleLinkComponent } from './user-role-link/user-role-link.component';
import { UserRoleLinkDetailsComponent } from './user-role-link/user-role-link-details/user-role-link-details.component';
import { ManageSecurityRoutingModule } from './manage-security-routing.module';
import { ScreenRoleLinkComponent } from './screen-role-link/screen-role-link.component';
import { ScreenRoleLinkDetailsComponent } from './screen-role-link/screen-role-link-details/screen-role-link-details.component';
import { ManageSecurityComponent } from './manage-security/manage-security.component';
import { SharedModule } from '../shared/shared.module';
import { RoleComponent } from './role/role.component';
import { RoleDetailsComponent } from './role/role-details/role-details.component';

@NgModule({
  declarations: [
    UserRoleLinkComponent,
    UserRoleLinkDetailsComponent,
    ScreenRoleLinkComponent,
    ScreenRoleLinkDetailsComponent,
    ManageSecurityComponent,
    RoleComponent,
    RoleDetailsComponent
],
  imports: [
    CommonModule,
    ComponentsModule,
    ManageSecurityRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    SharedModule
  ]
})
export class ManageSecurityModule { }
