import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormBuilder, UntypedFormGroup } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { UserRoleService } from 'src/app/shared/services/user-role.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-role-details',
  templateUrl: './role-details.component.html',
  styleUrls: ['./role-details.component.scss'],
  providers: [ErrorCodes],
})
export class RoleDetailsComponent implements OnInit {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName: any;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  roleForm!: UntypedFormGroup;
  UserName: any = [];
  tenantNames: any = [];

  TenantName: any = []; //tenant
  UserRole: any = []; //role

  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  btnVal = 'Submit';

  constructor(
    private formBuilder: FormBuilder,
    private userroleservice: UserRoleService,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit(): void {
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.viewRecord(this.rowData);
    }
  }

  initializeForm() {
    this.roleForm = this.formBuilder.group({
      id: [''],
      tenant: [localStorage.getItem('COMPANY_ID')],
      role_name: [''],
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.roleForm.reset();
      this.handleCancel.emit(false);
    }
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  viewRecord(edata: any) {
    this.roleForm.patchValue({
      id: edata.id,
      tenant: edata.tenant,
      role_name: edata.role_name,
    });

    if (this.submitBtn) {
      this.btnVal = 'Update';
    }
  }

  onSubmit() {
    if (this.roleForm.invalid) {
      return;
    } else {
      this.handleSave.emit(this.roleForm.value);
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.roleForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }
}
