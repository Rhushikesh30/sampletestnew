import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRoleLinkComponent } from './user-role-link/user-role-link.component';
import { ScreenRoleLinkComponent } from './screen-role-link/screen-role-link.component';
import { ManageSecurityComponent } from './manage-security/manage-security.component';
import { RoleComponent } from './role/role.component';

const routes: Routes = [

  {
    path: '',
    pathMatch: 'full',
    component: ManageSecurityComponent
  },
  {
    path: 'user-role-link',
    component: UserRoleLinkComponent,
  },
  {
    path: 'screen-role-link',
    component: ScreenRoleLinkComponent,
  },
  {
    path: 'role',
    component: RoleComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageSecurityRoutingModule { }
