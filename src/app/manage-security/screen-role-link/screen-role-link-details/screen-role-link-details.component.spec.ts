import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenRoleLinkDetailsComponent } from './screen-role-link-details.component';

describe('ScreenRoleLinkDetailsComponent', () => {
  let component: ScreenRoleLinkDetailsComponent;
  let fixture: ComponentFixture<ScreenRoleLinkDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenRoleLinkDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScreenRoleLinkDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
