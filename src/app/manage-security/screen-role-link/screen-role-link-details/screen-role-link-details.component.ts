import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormControl, UntypedFormGroup } from '@angular/forms';

import { ThemePalette } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-screen-role-link-details',
  templateUrl: './screen-role-link-details.component.html',
  styleUrls: ['./screen-role-link-details.component.scss'],
  providers: [ErrorCodes],
})
export class ScreenRoleLinkDetailsComponent {
  dataSource: any;
  @ViewChild('AssignScreenMasterPaginator', { read: MatPaginator })
  AssignScreenMasterPaginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  AssignScreenMasterData!: any;
  displayColumns = ['actions', 'company_name', 'role_name'];
  renderedData: any = [];

  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() rowData: any = [];
  @Input()
  submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName: any;

  cancelFlag = true;
  showReset = true;

  ScreenToRole_Form!: UntypedFormGroup;
  submitted = false;
  BTN_VAL = 'Submit';
  ScreenToRole_Data: any;
  screenToRole: any;
  companydata: any = [];
  roledata: any = [];
  designationdata: any = [];
  leftpaneldata = [];
  assign_screen_to_role: any = [];
  taken: any;
  color: ThemePalette = 'primary';
  newscreen_to_role: any;
  newScreenToRole_Data: any;
  newrolepayload = [];
  sidebardata: any;
  FilterMasterData: any;
  filterdCompanyName!: string;
  public getCompanytoSearch: FormControl = new FormControl();

  columns = [
    { name: 'form_name' },
    { name: 'read_access' },
    { name: 'write_access' },
    { name: 'delete_access' },
  ];

  displayColumn: string[] = [
    'check',
    'form_name',
    'read_access',
    'write_access',
    'delete_access',
  ];

  tbl_FilteredData = [];
  role: any;
  company: any;
  role_by_id: any = [];
  company_by_id: any = [];
  shouldDisable: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private manageSecurityService: RoleSecurityService,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit(): void {
    this.ScreenToRole_Form = this.formBuilder.group({
      role: [''],
      employee: [''],
      assign_screen_to_role: [this.assign_screen_to_role],
      id: [''],
    });

    if (!Array.isArray(this.rowData)) {
      this.viewRecord(this.rowData);
    }

    this.manageSecurityService.getRoleData('except').subscribe({
      next: (data: any) => {
        this.roledata = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.manageSecurityService.getLeftPanel('all').subscribe({
      next: (data: any) => {
        this.leftpaneldata = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  employeeChanged(event: any) {}

  onCheckboxChange(leftpanel: any, event: any) {
    let values = [];
    let fid: any = this.leftpaneldata.filter(
      (x: any) => x.parent_code == leftpanel.parent_code
    );
    const role = this.ScreenToRole_Form.value.role;
    const tenant = localStorage.getItem('COMPANY_ID');
    const employee = this.ScreenToRole_Form.value.employee;
    if (event.target.checked == true && role == '') {
      Swal.fire('Please select role first');
      event.target.checked = false;
    }
    if (event.target.checked == true && role != '') {
      if (leftpanel.is_parent == 'Y') {
        for (let i = 0; i < fid.length; i++) {
          if (
            leftpanel.parent_code == fid[i].parent_code &&
            leftpanel.is_parent == 'Y'
          ) {
            values.push(fid[i].id);
            this.assign_screen_to_role.push({
              id: '',
              role: role,
              form: fid[i].id,
              tenant: tenant,
              employee: employee,
              parent_code: fid[i].parent_code,
              child_code: fid[i].child_code,
              sub_child_code: fid[i].sub_child_code,
              read_access: 'Y',
              write_access: 'Y',
              delete_access: 'Y',
            });
          } else {
            this.leftpaneldata.forEach((ele: any) => {
              if (ele.id == fid[i].id) {
                ele.check = true;
                ele.readAccess = true;
                ele.writeAccess = true;
                ele.deleteAccess = true;
              }
            });
          }
        }
      } else {
        this.assign_screen_to_role.push({
          id: '',
          role: role,
          form: leftpanel.id,
          tenant: tenant,
          employee: employee,
          parent_code: leftpanel.parent_code,
          child_code: leftpanel.child_code,
          sub_child_code: leftpanel.sub_child_code,
          read_access: 'Y',
          write_access: 'Y',
          delete_access: 'Y',
        });
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == leftpanel.id) {
            ele.check = true;
            ele.readAccess = true;
            ele.writeAccess = true;
            ele.deleteAccess = true;
          }
        });
      }
    }
    if (event.target.checked == false && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        // $('#list [value="' + values.join('"],[value="') + '"]').prop('checked', false);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            let index = this.assign_screen_to_role.indexOf(
              this.assign_screen_to_role[j]
            );
            if (index > -1) {
              this.assign_screen_to_role.splice(index, 1);
            }
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.check = false;
            ele.readAccess = false;
            ele.writeAccess = false;
            ele.deleteAccess = false;
          }
        });
      }
    }
    if (event.target.checked == false && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role.splice(j, 1);
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.check = false;
          ele.readAccess = false;
          ele.writeAccess = false;
          ele.deleteAccess = false;
        }
      });
    }
  }

  read(leftpanel: any, event: any) {
    let values = [];
    let fid: any = this.leftpaneldata.filter(
      (x: any) => x.parent_code == leftpanel.parent_code
    );
    if (event.checked == true && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'Y';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = true;
          }
        });
      }
    }
    if (event.checked == true && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'Y';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = true;
        }
      });
    }
    if (event.checked == false && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'N';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = false;
          }
        });
      }
    }
    if (event.checked == false && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'N';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = true;
        }
      });
    }
  }

  write(leftpanel: any, event: any) {
    let values = [];
    let fid: any = this.leftpaneldata.filter(
      (x: any) => x.parent_code == leftpanel.parent_code
    );
    if (event.checked == true && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'Y';
            this.assign_screen_to_role[j].write_access = 'Y';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = true;
            ele.writeAccess = true;
          }
        });
      }
    }
    if (event.checked == true && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'Y';
          this.assign_screen_to_role[j].write_access = 'Y';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = true;
          ele.writeAccess = true;
        }
      });
    }
    if (event.checked == false && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'N';
            this.assign_screen_to_role[j].write_access = 'N';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = false;
            ele.writeAccess = false;
          }
        });
      }
    }
    if (event.checked == false && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'N';
          this.assign_screen_to_role[j].write_access = 'N';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = false;
          ele.writeAccess = false;
        }
      });
    }
  }

  delete(leftpanel: any, event: any) {
    let values = [];
    let fid: any = this.leftpaneldata.filter(
      (x: any) => x.parent_code == leftpanel.parent_code
    );
    if (event.checked == true && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'Y';
            this.assign_screen_to_role[j].write_access = 'Y';
            this.assign_screen_to_role[j].delete_access = 'Y';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = true;
            ele.writeAccess = true;
            ele.deleteAccess = true;
          }
        });
      }
    }
    if (event.checked == true && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'Y';
          this.assign_screen_to_role[j].write_access = 'Y';
          this.assign_screen_to_role[j].delete_access = 'Y';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = true;
          ele.writeAccess = true;
          ele.deleteAccess = true;
        }
      });
    }
    if (event.checked == false && leftpanel.is_parent == 'Y') {
      for (let i = 0; i < fid.length; i++) {
        values.push(fid[i].id);
        for (let j = 0; j < this.assign_screen_to_role.length; j++) {
          if (this.assign_screen_to_role[j].form == fid[i].id) {
            this.assign_screen_to_role[j].read_access = 'N';
            this.assign_screen_to_role[j].write_access = 'N';
            this.assign_screen_to_role[j].delete_access = 'N';
          }
        }
        this.leftpaneldata.forEach((ele: any) => {
          if (ele.id == fid[i].id) {
            ele.readAccess = false;
            ele.writeAccess = false;
            ele.deleteAccess = false;
          }
        });
      }
    }
    if (event.checked == false && leftpanel.is_parent == 'N') {
      for (let j = 0; j < this.assign_screen_to_role.length; j++) {
        if (this.assign_screen_to_role[j].form == leftpanel.id) {
          this.assign_screen_to_role[j].read_access = 'N';
          this.assign_screen_to_role[j].write_access = 'N';
          this.assign_screen_to_role[j].delete_access = 'N';
        }
      }
      this.leftpaneldata.forEach((ele: any) => {
        if (ele.id == leftpanel.id) {
          ele.readAccess = false;
          ele.writeAccess = false;
          ele.deleteAccess = false;
        }
      });
    }
  }

  onSubmit() {
    if (this.ScreenToRole_Form.invalid) {
      return;
    } else {
      this.handleSave.emit(this.ScreenToRole_Form.value.assign_screen_to_role);
    }
  }

  viewRecord(assignscreen: any) {
    this.shouldDisable = true;

    this.ScreenToRole_Form.patchValue({
      id: assignscreen.id,
      tenant: assignscreen.tenant_id,
      role: assignscreen.role_id,
    });

    this.manageSecurityService.getRoleData('all').subscribe({
      next: (rdata: any) => {
        this.roledata = rdata;

        this.manageSecurityService.getByRoleId(assignscreen.role_id).subscribe({
          next: (data: any) => {
            this.screenToRole = data;
            let values = [],
              read = [],
              write = [],
              deletes = [];
            for (let i = 0; i < this.screenToRole.length; i++) {
              values.push(this.screenToRole[i].form);
              this.assign_screen_to_role.push({
                id: this.screenToRole[i].id,
                role: assignscreen.role_id,
                tenant: assignscreen.tenant_id,
                employee: assignscreen.employee_id,
                form: this.screenToRole[i].form_id,
                read_access: this.screenToRole[i].read_access,
                write_access: this.screenToRole[i].write_access,
                delete_access: this.screenToRole[i].delete_access,
                parent_code: this.screenToRole[i].parent_code,
                child_code: this.screenToRole[i].child_code,
                sub_child_code: this.screenToRole[i].sub_child_code,
              });

              if (this.screenToRole[i].read_access == 'Y') {
                read.push(this.screenToRole[i].form_id);
                this.leftpaneldata.forEach((ele: any) => {
                  if (ele.id == this.screenToRole[i].form_id) {
                    ele.readAccess = true;
                  }
                });
              }
              if (this.screenToRole[i].write_access == 'Y') {
                write.push(this.screenToRole[i].form_id);
                this.leftpaneldata.forEach((ele: any) => {
                  if (ele.id == this.screenToRole[i].form_id) {
                    ele.writeAccess = true;
                  }
                });
              }
              if (this.screenToRole[i].delete_access == 'Y') {
                deletes.push(this.screenToRole[i].form_id);
                this.leftpaneldata.forEach((ele: any) => {
                  if (ele.id == this.screenToRole[i].form_id) {
                    ele.deleteAccess = true;
                  }
                });
              }
              this.leftpaneldata.forEach((ele: any) => {
                if (ele.id == this.screenToRole[i].form_id) {
                  ele.check = true;
                  ele.readAccess = true;
                  ele.writeAccess = true;
                  ele.deleteAccess = true;
                }
              });
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
        if (this.submitBtn === true) this.BTN_VAL = 'Update';
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.ScreenToRole_Form.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.ScreenToRole_Form.reset();
  }
}
