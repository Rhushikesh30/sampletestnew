import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenRoleLinkComponent } from './screen-role-link.component';

describe('ScreenRoleLinkComponent', () => {
  let component: ScreenRoleLinkComponent;
  let fixture: ComponentFixture<ScreenRoleLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenRoleLinkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScreenRoleLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
