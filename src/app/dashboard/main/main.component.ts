import {
  Component,
  OnInit,
  ViewChild,
  Renderer2,
  ElementRef,
  HostListener,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
// import { MatDialog,MatDialogRef} from "@angular/material/dialog";
import { EChartsOption } from 'echarts';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [ErrorCodes],
})
export class MainComponent implements OnInit {
  dataSource = new MatTableDataSource<any>(ELEMENT_DATA);
  dataSource1 = new MatTableDataSource<any>(ELEMENT_DATA);
  dataSource2 = new MatTableDataSource<any>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: any;
  form!: FormGroup;
  public pieChartsOption: ChartConfiguration['options'];
  public pieChartData!: ChartData<'pie', number[], string | string[]>;
  public pieChartType: ChartType = 'pie';

  dashboardAllData: any = [];
  dashboardTableSalesData: any = [];
  displayedColumns: string[] = ['type', 'count', 'amount'];
  displayedColumns1: string[] = [
    'sr.no',
    'name',
    'sales',
    'purchase_amount',
    'closing_amount',
    'gross_marigin',
  ];

  dashboardData: any = [
    {
      order_table_data: [
        { type: 'Order', count: 2, amount: 10360.0 },
        { type: 'Sales', count: 2, amount: 10360.0 },
        { type: 'Collection', count: 2, amount: 10360.0 },
      ],
    },
  ];
  total_orders: any = [];
  total_stock: any = [];
  total_sales: any = [];
  total_purchase: any = [];
  farmer_count: any = [];
  itemData: any = [];
  customerData: any = [];
  filterCustomerData: any = [];
  supplierData: any = [];
  filterSupplierData: any = [];
  cardLabel = '';
  today_total_delivery_rejection_per = '90';
  grossMarginTableData: any;
  dashboardTablePurchaseData: any;
  salesTotalCount: any;
  salesTotalAmount: any;
  purchaseTotalCount: any;
  purchaseTotalAmount: any;
  public ispopupOpened = false;
  public ispopupOpened1 = false;
  public ispopupOpened2 = false;
  public ispopupOpened3 = false;
  popupPosition = { x: 0, y: 0 };
  popupPosition1 = { x: 0, y: 0 };
  salesLegendData: any[] = []; // Replace with your actual data
  purchaseLegendData: any[] = []; // Replace with your actual data

  togglePopup() {
    this.ispopupOpened = !this.ispopupOpened;
    window.scrollTo(0, 0);

    const section =
      this.el.nativeElement.ownerDocument.querySelector('.content');
    if (section) {
      if (this.ispopupOpened) {
        this.renderer.addClass(section, 'popup-section');
      } else {
        this.renderer.removeClass(section, 'popup-section');
      }
    }
  }

  togglePopup1() {
    this.ispopupOpened1 = !this.ispopupOpened1;
    window.scrollTo(0, 0);

    const section =
      this.el.nativeElement.ownerDocument.querySelector('.content');
    if (section) {
      if (this.ispopupOpened1) {
        this.renderer.addClass(section, 'popup-section');
      } else {
        this.renderer.removeClass(section, 'popup-section');
      }
    }
  }

  togglePopup2(event: MouseEvent) {
    this.ispopupOpened2 = !this.ispopupOpened2;
    this.ispopupOpened3 = false;
    this.setPopupPosition(event);
    window.scrollTo(0, 0);
    this.dashboardfunc(
      'all',
      'all',
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );
    this.form.get('customer_ref_id')?.setValue('');
    this.form.get('s_item_ref_id')?.setValue('all');
  }

  togglePopup3(event: MouseEvent) {
    this.ispopupOpened3 = !this.ispopupOpened3;
    this.ispopupOpened2 = false;
    this.setPopupPosition1(event);
    window.scrollTo(0, 0);
    this.dashboardfunc(
      'all',
      'all',
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );
    this.form.get('supplier_ref_id')?.setValue('');
    this.form.get('p_item_ref_id')?.setValue('all');
  }

  constructor(
    private formbuilder: FormBuilder,
    private dashboardService: DashboardService,
    private errorCodes: ErrorCodes,
    private dialog: MatDialog,
    private renderer: Renderer2,
    private el: ElementRef
  ) {}
  public bar_chart: Partial<EChartsOption> | any;
  public bar_chart1: Partial<EChartsOption> | any;

  private chart1(chartdata: any, Amountdata: any) {
    console.log(chartdata);

    this.bar_chart = {
      grid: {
        top: '6',
        right: '0',
        bottom: '17',
        left: '25',
      },
      xAxis: {
        data: ['Order', 'Sales', 'Collection'],

        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac',
        },
      },

      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis',
      },
      yAxis: {
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac',
        },
      },
      series: [
        {
          name: 'count',
          type: 'bar',
          data: chartdata,
        },
        {
          name: 'amount',
          type: 'bar',
          data: Amountdata,
        },
      ],
      color: ['#66B7E1', '#73C87A'],
    };
  }

  private chart2(chartdata: any, Amountdata: any) {
    this.bar_chart1 = {
      grid: {
        top: '6',
        right: '0',
        bottom: '17',
        left: '25',
      },
      xAxis: {
        data: ['Inventory', 'Purchase Booking', 'Payment'],

        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac',
        },
      },

      tooltip: {
        show: true,
        showContent: true,
        alwaysShowContent: false,
        triggerOn: 'mousemove',
        trigger: 'axis',
      },
      yAxis: {
        axisLabel: {
          fontSize: 10,
          color: '#9aa0ac',
        },
      },
      series: [
        {
          name: 'count',
          type: 'bar',
          data: chartdata,
        },
        {
          name: 'amount',
          type: 'bar',
          data: Amountdata,
        },
      ],
      color: ['#66B7E1', '#73C87A'],
    };
  }
  ngAfterViewInit(): void {
    this.dataSource2.paginator = this.paginator;
  }

  ngOnInit() {
    this.form = this.formbuilder.group({
      id: [''],
      customer_ref_id: [''],
      s_item_ref_id: ['all'],
      p_item_ref_id: ['all'],
      supplier_ref_id: [''],
    });
    this.dashboardfunc(
      this.form.get('s_item_ref_id')?.value,
      this.form.get('p_item_ref_id')?.value,
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );
    console.log(this.dashboardTableSalesData);

    // this.form.get('s_item_ref_id')?.valueChanges.subscribe({
    //   next: (val: any) => {
    //     if (val) {
    //       this.dashboardfunc(
    //         val,
    //         this.form.get('p_item_ref_id')?.value,
    //         this.form.get('customer_ref_id')?.value,
    //         ''
    //       );
    //     } else {
    //       this.dashboardTableSalesData = [];
    //     }
    //   },
    //   error: (e) => {
    //     this.dashboardTableSalesData = [];
    //   },
    // });
  }

  getConvertedTo2Decimal(value: any) {
    return Number(value).toFixed(2);
  }

  dashboardfunc(s_item: any, p_item: any, customer: any, supplier: any) {
    this.dashboardService.getTilesDetails().subscribe({
      next: (data: any) => {
        this.dashboardAllData = data;
        this.total_orders = this.dashboardAllData.order_data.total_amount_lakhs;
        this.total_stock = this.dashboardAllData.stock_data.total_amount_lakhs;
        this.total_sales = this.dashboardAllData.sales_data.total_amount_lakhs;
        this.total_purchase =
          this.dashboardAllData.purchase_data.total_amount_lakhs;
        this.farmer_count = this.dashboardAllData.farmer_count.farmer_count;
      },
      error: (e: any) => {
        console.log('eeeeeee', e);

        // this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
      },
    });

    this.dashboardService
      .getSalesTableDetails(s_item, customer, supplier)
      .subscribe({
        next: (data: any) => {
          this.dataSource.data = data.all_data_sales;

          this.dashboardTableSalesData = data.all_data_sales;
          this.filterCustomerData = data.total_customer_data;
          this.filterSupplierData = data.total_supplier_data;

          const countarr: any = [];
          const amtarr: any = [];
          let totalcount: any = 0;
          let totalamt: any = 0;

          for (let i = 0; i < this.dashboardTableSalesData.length; i++) {
            countarr.push(this.dashboardTableSalesData[i]['count']);
            amtarr.push(this.dashboardTableSalesData[i]['amount']);
            totalcount =
              totalcount + parseFloat(this.dashboardTableSalesData[i]['count']);
            totalamt =
              totalamt + parseFloat(this.dashboardTableSalesData[i]['amount']);
          }
          this.salesTotalCount = Math.round(totalcount);
          this.salesTotalAmount = totalamt.toFixed(2);

          this.chart1(countarr, amtarr);
        },
      });
    console.log(this.filterSupplierData);

    this.dashboardService
      .getPurchaseTableDetails(p_item, customer, supplier)
      .subscribe({
        next: (data: any) => {
          this.dataSource1.data = data.all_data_purchase;

          this.dashboardTablePurchaseData = data.all_data_purchase;
          const countarr: any = [];
          const amtarr: any = [];
          let totalcount: any = 0;
          let totalamt: any = 0;

          for (let i = 0; i < this.dashboardTablePurchaseData.length; i++) {
            countarr.push(this.dashboardTablePurchaseData[i]['count']);
            amtarr.push(this.dashboardTablePurchaseData[i]['amount']);
            totalcount =
              totalcount +
              parseFloat(this.dashboardTablePurchaseData[i]['count']);
            totalamt =
              totalamt +
              parseFloat(this.dashboardTablePurchaseData[i]['amount']);
          }
          this.purchaseTotalCount = Math.round(totalcount);
          this.purchaseTotalAmount = totalamt.toFixed(2);
          // purchaseTotalCount:any;
          // purchaseTotalAmount:any;
          this.chart2(countarr, amtarr);
        },
      });
    this.dashboardService.getGrossMarginData().subscribe({
      next: (data: any) => {
        this.grossMarginTableData = data.filter((item: any) => {
          if (item.purchase_amount !== 0) {
            return item;
          }
        });
        this.dataSource2.data = this.grossMarginTableData;
      },
    });

    // this.dashboardService.getDynamicData('customer', 'company_name').subscribe({
    //   next: (data: any) => {
    //     this.customerData = data;
    //     this.filterCustomerData = this.customerData.slice();
    //   },
    //   error: (e: any) => {
    //     this.showSwalMassage(
    //       this.errorCodes.getErrorMessage(JSON.parse(e).status),
    //       'error'
    //     );
    //   },
    // });

    // this.dashboardService.getDynamicData('supplier', 'company_name').subscribe({
    //   next: (data: any) => {
    //     this.supplierData = data;
    //     this.filterSupplierData = this.supplierData.slice();

    //   },
    //   error: (e: any) => {
    //     this.showSwalMassage(
    //       this.errorCodes.getErrorMessage(JSON.parse(e).status),
    //       'error'
    //     );
    //   },
    // });

    this.dashboardService
      .getDynamicfieldData('get_dynamic_data', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.itemData = data;
          // this.showLoader = false;
        },
      });
  }

  setPopupPosition(event: MouseEvent) {
    this.popupPosition.x = event.clientX;
    this.popupPosition.y = event.clientY;
  }
  setPopupPosition1(event: MouseEvent) {
    this.popupPosition1.x = event.clientX;
    this.popupPosition1.y = event.clientY;
  }

  ApplyFilter() {
    const val = this.form.get('s_item_ref_id')?.value;
    if (val) {
      this.dashboardfunc(
        val,
        this.form.get('p_item_ref_id')?.value,
        this.form.get('customer_ref_id')?.value,
        ''
      );
    } else {
      this.dashboardTableSalesData = [];
    }
  }

  ApplyFilter1() {
    const val = this.form.get('p_item_ref_id')?.value;
    if (val) {
      this.dashboardfunc('', val, '', this.form.get('supplier_ref_id')?.value);
    } else {
      this.dashboardTablePurchaseData = [];
    }
  }

  ClearFilter() {
    this.form.get('customer_ref_id')?.setValue('');
    this.form.get('s_item_ref_id')?.setValue('all');
    this.dashboardfunc(
      'all',
      'all',
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );
  }

  ClearFilter1() {
    this.form.get('supplier_ref_id')?.setValue('');
    this.form.get('p_item_ref_id')?.setValue('all');
    this.dashboardfunc(
      'all',
      'all',
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );
  }

  showSwalMassage(message: any, icon: any): void {
    Swal.fire({
      title: message,
      icon: icon,
      timer: 2000,
      showConfirmButton: true,
    });
  }

  onChange(val: any) {
    console.log('------------vaaalllllllllll', val);

    this.dashboardService.getCustomerItemData(val).subscribe({
      next: (data: any) => {
        console.log('data', data);
        console.log('data', data.length);

        if (data.length > 0) {
          this.itemData = data[0];
          console.log('this.itemData', this.itemData);

          // this.showLoader = false;
        } else {
          this.showSwalMassage(
            'No Item available for this Customer,Please Select another!',
            'error'
          );
        }
      },
    });
  }
  onChange1(val: any) {
    console.log('------------vaaalllllllllll', val, this.ispopupOpened3);

    this.dashboardService.getSupplierItemData(val).subscribe({
      next: (data: any) => {
        console.log('data', data);
        console.log('data', data.length);

        if (data.length > 0) {
          this.itemData = data[0];
          console.log('this.itemData', this.itemData);

          // this.showLoader = false;
        } else {
          this.showSwalMassage(
            'No Item available for this Customer,Please Select another!',
            'error'
          );
        }
      },
    });
  }

  chartFunc(pielabelData: any, piechartData: any) {
    this.pieChartsOption = {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'top',
        },
      },
    };
    this.pieChartData = {
      labels: pielabelData,
      datasets: [
        {
          data: piechartData,
          // backgroundColor: ['#60A3F6', '#7C59E7', '#DD6811', '#5BCFA5'],
        },
      ],
    };
  }

  openDialogFilter(): void {
    const dialogRef: MatDialogRef<DialogComponent> = this.dialog.open(
      DialogComponent,
      {
        data: this.form.value,
      }
    );

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        // this.form.get('segment').setValue(result)
        // this.axis_chart_data()
        // this.processdatachange(this.form.get('process_type').value)
      }
    });
  }

  exportExcel(type: any) {
    console.log(type);

    if (type == 'Sales') {
      console.log('in here sales');

      type = '';
      const data = this.dashboardTableSalesData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'sales-report.xlsx');
    } else if (type == 'Purchase') {
      console.log('in here purchase');

      type = '';
      const data = this.dashboardTablePurchaseData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'purchase-report.xlsx');
    } else if (type == 'GMA') {
      type = '';
      const data = this.grossMarginTableData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'gross-margin-analysis-report.xlsx');
    }
  }
}

const ELEMENT_DATA: any[] = [];
