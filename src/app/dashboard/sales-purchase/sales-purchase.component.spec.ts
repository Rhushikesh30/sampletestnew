import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPurchaseComponent } from './sales-purchase.component';

describe('SalesPurchaseComponent', () => {
  let component: SalesPurchaseComponent;
  let fixture: ComponentFixture<SalesPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesPurchaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SalesPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
