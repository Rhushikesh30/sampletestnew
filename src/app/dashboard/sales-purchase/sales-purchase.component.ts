import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-sales-purchase',
  templateUrl: './sales-purchase.component.html',
  styleUrls: ['./sales-purchase.component.scss'],
})
export class SalesPurchaseComponent {
  @Input() title: any;
  @Input() legendData: any[] | undefined; // Replace with your actual legend data
  @Input() chartOptions: any; // Replace with your actual chart data
  @Output() filterClick: EventEmitter<any> = new EventEmitter();
  @Output() zoomClick: EventEmitter<any> = new EventEmitter();
}
