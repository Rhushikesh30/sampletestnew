import { Component, OnInit ,Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

export interface DialogData {
  data: any;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
  itemData:any = [];
  customerData:any = [];
  constructor(
    private dialogRef: MatDialogRef<DialogComponent>,
    private dialog:MatDialog,

    @Inject(MAT_DIALOG_DATA) public data: DialogData,

  ) {  }

  ngOnInit() {
    
  }

  onChange(val:any){

  }



}
