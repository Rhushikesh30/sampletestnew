import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { CalendarSetupService } from '../shared/services/calendar-setup.service';

import Swal from 'sweetalert2';
import { EncrDecrService } from '../core/service/encr-decr.service';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [ErrorCodes],
})
export class CalendarComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  resultData!: [];
  displayedColumns = ['view', 'edit', 'calendar_type_id_id', 'fiscal_year'];
  renderedData: any = [];
  screenName = 'Calendar Setup';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: CalendarSetupService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private roleSecurityService: RoleSecurityService,
    private calendarService: CalendarSetupService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Calendar Settings')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    console.log(row);

    this.calendarService.getCalenderById(row.fiscal_year).subscribe({
      next: (data: any) => {
        this.rowData = data;
        this.showList = false;
        this.submitBtn = flag;
        //  this.listDiv = true;
        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  showFormList(item: boolean) {
    if (item === false) {
      //  this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    // if (this.dataSource) {
    //   for (let res in this.dataSource.filteredData) {
    //     if (
    //       formValue.id &&
    //       this.dataSource.filteredData[res].id != formValue.id &&
    //       this.dataSource.filteredData[res].phone_no == formValue.phone_no
    //     ) {
    //       this.showSwalmessage(
    //         'Record Already Exist!',
    //         'Phone Number should be unique',
    //         'warning',
    //         false
    //       );
    //       return;
    //     }
    //     if (
    //       !formValue.id &&
    //       this.dataSource.filteredData[res].phone_no == formValue.phone_no
    //     ) {
    //       this.showSwalmessage(
    //         'Record Already Exist!',
    //         'Phone Number should be unique',
    //         'warning',
    //         false
    //       );
    //       return;
    //     }
    //   }
    // }
    this.showLoader = true;

    let json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.calendarService
      .createCalendar(json_data, formValue.id, formValue.fiscal_year)
      .subscribe({
        next: (data: any) => {
          if (data['status'] == 1) {
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          } else if (data['status'] == 2) {
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
          this.refresh();
          this.showList = true;
          this.listDiv = false;
          this.showLoader = false;
        },
        error: (e) => {
          this.showLoader = false;
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new CalendarSetupService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Calendar type': x.master_key,
        'Fiscal Year': x.fiscal_year,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: CalendarSetupService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        console.log(this.exampleDatabase);

        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.master_key + advanceTable.fiscal_year
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'calendar_type_id_id':
          [propertyA, propertyB] = [
            a.calendar_type_id_id,
            b.calendar_type_id_id,
          ];
          break;
        case 'fiscal_year':
          [propertyA, propertyB] = [a.fiscal_year, b.fiscal_year];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
