import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-calendar-details',
  templateUrl: './calendar-details.component.html',
  styleUrls: ['./calendar-details.component.scss'],
  providers: [ErrorCodes],
})
export class CalendarDetailsComponent implements OnInit {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() screenName!: string;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  showReset = true;

  calendarForm!: FormGroup;
  listDiv: boolean = false;
  showList: boolean = true;
  hideGrid: boolean = false;
  BTN_VAL = 'Submit';

  CalenderTypeData: any = [];
  statusData = [];

  constructor(
    private errorCodes: ErrorCodes,
    private formBuilder: FormBuilder,
    private fpcSetupservice: FpcSetupService
  ) {}

  ngOnInit(): void {
    this.calendarForm = this.formBuilder.group({
      id: [''],
      calendar_type_id: ['', [Validators.required]],
      is_deleted: false,
      fiscal_year: [''],
      initialItemRow: this.formBuilder.array([this.initialitemRow()]),
    });

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    }

    this.fpcSetupservice.getAllMasterData('Calendar Type').subscribe({
      next: (data: any) => {
        this.CalenderTypeData = data.filter(
          (x: any) => x['master_key'] == 'Monthly'
        );
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.statusData.push('Open' as never, 'Close' as never, 'Pending' as never);
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      fiscal_year: [''],
      period: [''],
      start_date: [''],
      end_date: [''],
      month: [''],
      status: [''],
    });
  }

  get formArray() {
    return this.calendarForm.get('initialItemRow') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);

    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          fiscal_year: element.fiscal_year,
          period: element.period,
          start_date: element.start_date,
          end_date: element.end_date,
          month: element.month,
          status: element.status,
          created_by: element.created_by,
          updated_by: element.updated_by,
          created_date_time: element.created_date_time,
          updated_date_time: element.updated_date_time,
        })
      );
    });
    return formArray;
  }

  viewRecord(row: any) {
    this.hideGrid = true;
    this.calendarForm.patchValue({
      id: row.initialItemRow[0]['mst_ref_id_id'],
      calendar_type_id: row.calender_type[0]['calendar_type_id_id'],
      fiscal_year: row.calender_type[0]['fiscal_year'],
    });

    this.calendarForm.setControl(
      'initialItemRow',
      this.setExistingArray(row.initialItemRow)
    );
    this.calendarForm.get('calendar_type_id')?.disable();

    if (this.submitBtn === true) {
      this.BTN_VAL = 'Update';
    }
  }

  onCancelForm() {
    this.onResetForm();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.calendarForm.reset();
  }

  onSubmit() {
    this.calendarForm.get('calendar_type_id')?.enable();
    this.showLoader = true;
    if (this.calendarForm.invalid) {
      this.showLoader = false;
      this.showSwalmessage(
        'Please Fill All Required Fields',
        '',
        'error',
        false
      );
    } else {
      this.handleSave.emit(this.calendarForm.value);
    }
  }
}
