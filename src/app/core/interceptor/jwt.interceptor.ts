import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    const cookie:any = localStorage.getItem('session_key');
    if (token) {
      const cloned = req.clone({
            headers: req.headers.set('Authorization', 'Bearer '.concat(token)),
            setHeaders: {'Session-Key': cookie}
          });
          return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
  
}
