import { CommonMethod } from "src/app/shared/common-methods/common-method";

export class Fpc {
    id: number;
    tenant_name: string;
    district_name: string;
    taluka_name: string;
    status: string;
    phone_no: number;


    constructor(fpc: Fpc, commonMethod: CommonMethod) {
        {
            this.id = fpc.id || commonMethod.getRandomID();
            this.tenant_name = fpc.tenant_name || '';
            this.district_name = fpc.district_name || '';
            this.taluka_name = fpc.taluka_name || '';
            this.status = fpc.status || '';
            this.phone_no = fpc.phone_no;

        }
    }
}
