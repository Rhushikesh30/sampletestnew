import { CommonMethod } from "src/app/shared/common-methods/common-method";

export class grn {
    id: number;
    tenant_name: string;
    district_name: string;
    taluka_name: string;
    status: string;
    phone_no: number;


    constructor(purchase: grn, commonMethod: CommonMethod) {
        {
            this.id = purchase.id || commonMethod.getRandomID();
            this.tenant_name = purchase.tenant_name || '';
            this.district_name = purchase.district_name || '';
            this.taluka_name = purchase.taluka_name || '';
            this.status = purchase.status || '';
            this.phone_no = purchase.phone_no;

        }
    }
}
