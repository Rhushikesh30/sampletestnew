import { CommonMethod } from 'src/app/shared/common-methods/common-method';

export class Employee {
    id: number;
    unique_id: number;
    emp_name: string;
    designation_name: string;
    department_name: string;
    role_name: string;
    phone: number;
    is_active: any;
    email: string;
    active: any;

    constructor(employee: Employee, commonMethod: CommonMethod) {
        {
            this.id = employee.id || commonMethod.getRandomID();
            this.unique_id = employee.unique_id || 0;
            this.emp_name = employee.emp_name || '';
            this.designation_name = employee.designation_name || '';
            this.department_name = employee.department_name || '';
            this.role_name = employee.role_name || '';
            this.phone = employee.phone;
            this.is_active = employee.is_active;
            this.email = employee.email || '';
            this.active = employee.active;
        }
    }
}
