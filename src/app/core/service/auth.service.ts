import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';

import { User } from '../models/user';
import { environment } from 'src/environments/environment.development';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public timout: ReturnType<typeof setTimeout> = setTimeout(() => { }, 100000);
  public ALLOWEDIDLETIME: number = 300 * 60 * 1000 // for this much time session can be idle
  code_token = null;
  username: string | null = '';
  password: string | null = '';

  constructor(private http: HttpClient, private encrDecrService: EncrDecrService) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser') || '{}')
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration: any = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }

  private UpdateSession(response: any) {
    const expiresAt = moment().add(Number(this.encrDecrService.decryptedData(response.expires_in)), 's');
    localStorage.setItem('token', this.encrDecrService.decryptedData(response.access_token).toString());
    localStorage.setItem('refresh_token', this.encrDecrService.decryptedData(response.refresh_token).toString());
    localStorage.setItem('expires_at', expiresAt.valueOf().toString());
    localStorage.setItem('session_key', this.encrDecrService.decryptedData(response.session_key.toString()));
    return
  }

  get refeshToken(): string | null {
    return localStorage.getItem('refresh_token');
  }

  get token(): string | null {
    return localStorage.getItem('token');
  }

  refreshToken() {
    let refreshResult
    if (moment().isBetween(this.getExpiration().subtract(1, 'h'), this.getExpiration())) {
      refreshResult = this.http.post(
        `${environment.apiUrl}/auth/refresh-token/`,
        { token: this.token, refresh_token: this.refeshToken }
      ).pipe(
        tap((response:any) => this.UpdateSession(response)),
        shareReplay(),
      ).subscribe();
    }

    return refreshResult

  }

  getLicenseKey() {
    return this.http.get(`${environment.apiUrl}/licensekey/`);

  }

  saveLicenseKey(form: any) {
    return this.http.post(`${environment.apiUrl}/licensewrite_key/`, form);

  }

  code(username: string, password: string) {
    this.username = username;
    this.password = password;
    return this.http.post(
      `${environment.apiUrl}/auth/code/`,
      { username, password }
    )
  }

  resendCode() {
    return this.http.post(
      `${environment.apiUrl}/auth/code/`,
      { username: this.username, password: this.password }
    )
  }

  getCodeToken(): string | null {
    return this.code_token;
  }

  setCodeToken(token: any) {
    this.code_token = token;
  }

  private setSession(authResult: any) {
    const expiresAt = moment().add(Number(this.encrDecrService.decryptedData(authResult.expires_in)), 's');
    localStorage.setItem('token', this.encrDecrService.decryptedData(authResult.access_token).toString());
    localStorage.setItem('refresh_token', this.encrDecrService.decryptedData(authResult.refresh_token).toString());
    localStorage.setItem('expires_at', expiresAt.valueOf().toString());
    localStorage.setItem('user_id', this.encrDecrService.decryptedData(authResult.ID).toString());
    localStorage.setItem('USER_NAME', this.encrDecrService.decryptedData(authResult.username).toString());
    // localStorage.setItem('EMAIL_ID', this.encrDecrService.decryptedData(authResult.email).toString());
    // localStorage.setItem('COMPANY_ID', this.encrDecrService.decryptedData(authResult.tenant_id.toString()));
    // localStorage.setItem('COMPANY_NAME', this.encrDecrService.decryptedData(authResult.tenant_name.toString()));
    localStorage.setItem('currentUser', this.encrDecrService.decryptedData(authResult.ID).toString());
    localStorage.setItem('roles', JSON.stringify(authResult.roles));
    localStorage.setItem('is_password_changed', this.encrDecrService.decryptedData(authResult.is_password_changed).toString());
    localStorage.setItem('employee_authority', this.encrDecrService.decryptedData(authResult.employee_authority).toString());
    localStorage.setItem('session_key', this.encrDecrService.decryptedData(authResult.session_key.toString()));
  }

  login(payload: any) {
    return this.http.post(
      `${environment.apiUrl}/auth/login/`, payload).pipe(tap((response: any) => {
        if (response['status'] === 'Success') {
          this.setSession(response);
          this.username = null;
          this.password = null;
        }
      }),
        shareReplay(),
      );
  }

  logout() {
    let payload = { 'token': localStorage.getItem('token') }
    this.http.post(`${environment.apiUrl}/logoutme/`, payload).pipe(tap((response: any) => {
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(this.currentUserValue);
    }))
    return of({ success: false });
  }

  UpdatePassword(user: any) {
    return this.http.post(`${environment.apiUrl}/change-password/`, user);
  }

  ResetPassword(username: string) {
    this.username = username;
    return this.http.post(`${environment.apiUrl}/get-otp/`,{ username });
  }
}

@Injectable({
  providedIn: 'root'
})

export class DataService {
  private dataSource: BehaviorSubject<string> = new BehaviorSubject<string>('Initial Value');
  data: Observable<string> = this.dataSource.asObservable();
  sendData(data: any) {
    this.dataSource.next(data);
  }
}