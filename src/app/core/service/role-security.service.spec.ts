import { TestBed } from '@angular/core/testing';

import { RoleSecurityService } from './role-security.service';

describe('RoleSecurityService', () => {
  let service: RoleSecurityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoleSecurityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
