import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/environments/environment.development';
@Injectable({
  providedIn: 'root'
})
export class EncrDecrService {
  
    decryptedData(Base64CBC:any){
      let iv = CryptoJS.enc.Utf8.parse(`${environment.EncryptionIV}`);
      let key = CryptoJS.enc.Utf8.parse(`${environment.EncryptionKey}`);
      let decrypted :any=  CryptoJS.AES.decrypt(Base64CBC, key, { iv: iv, mode: CryptoJS.mode.CFB});
      decrypted = decrypted.toString(CryptoJS.enc.Utf8);
      return decrypted;
    }
  
    encryptedData(Base64CBC:any) {
      let iv = CryptoJS.enc.Utf8.parse(`${environment.EncryptionIV}`);
      let key = CryptoJS.enc.Utf8.parse(`${environment.EncryptionKey}`);
      let encrypted =  CryptoJS.AES.encrypt(JSON.stringify(Base64CBC), key, { iv: iv, mode: CryptoJS.mode.CFB}).toString();
      return encrypted;
    }

}
