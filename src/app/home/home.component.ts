// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-home',
//   templateUrl: './home.component.html',
//   styleUrls: ['./home.component.scss'],
// })
// export class HomeComponent {}
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
// import { MatDialog,MatDialogRef} from "@angular/material/dialog";
import { EChartsOption } from 'echarts';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { DialogComponent } from '../dashboard/dialog/dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ErrorCodes],
})
export class HomeComponent implements OnInit {
  form!: FormGroup;
  public pieChartsOption: ChartConfiguration['options'];
  public pieChartData!: ChartData<'pie', number[], string | string[]>;
  public pieChartType: ChartType = 'pie';

  dashboardAllData: any = [];
  dashboardTableSalesData: any = [];

  dashboardData: any = [
    {
      order_table_data: [
        { type: 'Order', count: 2, amount: 10360.0 },
        { type: 'Sales', count: 2, amount: 10360.0 },
        { type: 'Collection', count: 2, amount: 10360.0 },
      ],
    },
  ];
  total_orders: any = [];
  total_stock: any = [];
  total_sales: any = [];
  total_purchase: any = [];
  farmer_count: any = [];
  itemData: any = [];
  customerData: any = [];
  filterCustomerData: any = [];
  supplierData: any = [];
  filterSupplierData: any = [];
  cardLabel = '';
  today_total_delivery_rejection_per = '90';
  grossMarginTableData: any;
  dashboardTablePurchaseData: any;
  constructor(
    private formbuilder: FormBuilder,
    private dashboardService: DashboardService,
    private errorCodes: ErrorCodes,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.form = this.formbuilder.group({
      id: [''],
      customer_ref_id: [''],
      s_item_ref_id: ['all'],
      p_item_ref_id: ['all'],
      supplier_ref_id: [''],
    });
    this.dashboardfunc(
      this.form.get('s_item_ref_id')?.value,
      this.form.get('p_item_ref_id')?.value,
      this.form.get('customer_ref_id')?.value,
      this.form.get('supplier_ref_id')?.value
    );

    this.form.get('s_item_ref_id')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.dashboardfunc(
            val,
            this.form.get('p_item_ref_id')?.value,
            this.form.get('customer_ref_id')?.value,
            ''
          );
        } else {
          this.dashboardTableSalesData = [];
        }
      },
      error: (e) => {
        this.dashboardTableSalesData = [];
      },
    });
    this.form.get('p_item_ref_id')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.dashboardfunc(
            '',
            val,
            '',
            this.form.get('supplier_ref_id')?.value
          );
        } else {
          this.dashboardTablePurchaseData = [];
        }
      },
      error: (e) => {
        this.dashboardTablePurchaseData = [];
      },
    });
  }

  getConvertedTo2Decimal(value: any) {
    return Number(value).toFixed(2);
  }

  dashboardfunc(s_item: any, p_item: any, customer: any, supplier: any) {
    this.dashboardService.getTilesDetails().subscribe({
      next: (data: any) => {
        this.dashboardAllData = data;
        this.total_orders = this.dashboardAllData.order_data.total_amount_lakhs;
        this.total_stock = this.dashboardAllData.stock_data.total_amount_lakhs;
        this.total_sales = this.dashboardAllData.sales_data.total_amount_lakhs;
        this.total_purchase =
          this.dashboardAllData.purchase_data.total_amount_lakhs;
        this.farmer_count = this.dashboardAllData.farmer_count.farmer_count;
      },
      error: (e: any) => {
        console.log('eeeeeee', e);

        // this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
      },
    });

    this.dashboardService
      .getSalesTableDetails(s_item, customer, supplier)
      .subscribe({
        next: (data: any) => {
          this.dashboardTableSalesData = data.all_data_sales;
        },
      });

    this.dashboardService
      .getPurchaseTableDetails(p_item, customer, supplier)
      .subscribe({
        next: (data: any) => {
          this.dashboardTablePurchaseData = data.all_data_purchase;
        },
      });
    this.dashboardService.getGrossMarginData().subscribe({
      next: (data: any) => {
        this.grossMarginTableData = data.filter((item: any) => {
          if (item.purchase_amount !== 0) {
            return item;
          }
        });
      },
    });

    this.dashboardService.getDynamicData('customer', 'company_name').subscribe({
      next: (data: any) => {
        this.customerData = data;
        this.filterCustomerData = this.customerData.slice();
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dashboardService.getDynamicData('supplier', 'company_name').subscribe({
      next: (data: any) => {
        this.supplierData = data;
        this.filterSupplierData = this.supplierData.slice();
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dashboardService
      .getDynamicfieldData('get_dynamic_data', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.itemData = data;
          // this.showLoader = false;
        },
      });
  }

  showSwalMassage(message: any, icon: any): void {
    Swal.fire({
      title: message,
      icon: icon,
      timer: 2000,
      showConfirmButton: true,
    });
  }

  onChange(val: any) {
    console.log('------------vaaalllllllllll', val);

    this.dashboardService.getCustomerItemData(val).subscribe({
      next: (data: any) => {
        console.log('data', data);
        console.log('data', data.length);

        if (data.length > 0) {
          this.itemData = data[0];
          console.log('this.itemData', this.itemData);

          // this.showLoader = false;
        } else {
          this.showSwalMassage(
            'No Item available for this Customer,Please Select another!',
            'error'
          );
        }
      },
    });
  }
  onChange1(val: any) {
    console.log('------------vaaalllllllllll', val);

    this.dashboardService.getSupplierItemData(val).subscribe({
      next: (data: any) => {
        console.log('data', data);
        console.log('data', data.length);

        if (data.length > 0) {
          this.itemData = data[0];
          console.log('this.itemData', this.itemData);

          // this.showLoader = false;
        } else {
          this.showSwalMassage(
            'No Item available for this Customer,Please Select another!',
            'error'
          );
        }
      },
    });
  }

  chartFunc(pielabelData: any, piechartData: any) {
    this.pieChartsOption = {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: true,
          position: 'top',
        },
      },
    };
    this.pieChartData = {
      labels: pielabelData,
      datasets: [
        {
          data: piechartData,
          // backgroundColor: ['#60A3F6', '#7C59E7', '#DD6811', '#5BCFA5'],
        },
      ],
    };
  }

  openDialogFilter(): void {
    const dialogRef: MatDialogRef<DialogComponent> = this.dialog.open(
      DialogComponent,
      {
        data: this.form.value,
      }
    );

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        // this.form.get('segment').setValue(result)
        // this.axis_chart_data()
        // this.processdatachange(this.form.get('process_type').value)
      }
    });
  }

  exportExcel(type: any) {
    console.log(type);

    if (type == 'Sales') {
      console.log('in here sales');

      type = '';
      const data = this.dashboardTableSalesData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'sales-report.xlsx');
    } else if (type == 'Purchase') {
      console.log('in here purchase');

      type = '';
      const data = this.dashboardTablePurchaseData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'purchase-report.xlsx');
    } else if (type == 'GMA') {
      type = '';
      const data = this.grossMarginTableData;
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, 'gross-margin-analysis-report.xlsx');
    }
  }
}
