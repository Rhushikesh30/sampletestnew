import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LederMasterComponent } from './leder-master.component';

describe('LederMasterComponent', () => {
  let component: LederMasterComponent;
  let fixture: ComponentFixture<LederMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LederMasterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LederMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
