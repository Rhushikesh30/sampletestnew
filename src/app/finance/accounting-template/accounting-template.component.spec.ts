import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingTemplateComponent } from './accounting-template.component';

describe('AccountingTemplateComponent', () => {
  let component: AccountingTemplateComponent;
  let fixture: ComponentFixture<AccountingTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountingTemplateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountingTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
