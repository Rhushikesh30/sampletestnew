import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingParameterComponent } from './accounting-parameter.component';

describe('AccountingParameterComponent', () => {
  let component: AccountingParameterComponent;
  let fixture: ComponentFixture<AccountingParameterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountingParameterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountingParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
