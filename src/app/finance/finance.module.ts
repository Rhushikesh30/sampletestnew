import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ComponentsModule } from '../shared/components/components.module';
import { DynamicFormModule } from '../shared/dynamic-form/dynamic-form.module';

import { FinanceRoutingModule } from './finance-routing.module';
import { LinkCompanyComponent } from './link-company/link-company.component';
import { LederMasterComponent } from './leder-master/leder-master.component';
import { LedgerGroupComponent } from './ledger-group/ledger-group.component';
import { TaxAuthorityComponent } from './tax-authority/tax-authority.component';
import { TaxRateComponent } from './tax-rate/tax-rate.component';
import { ProfitCenterComponent } from './profit-center/profit-center.component';
import { CostCenterComponent } from './cost-center/cost-center.component';

import { SharedModule } from '../shared/shared.module';
import { FinanceComponent } from './finance/finance.component';
import { AccountingTemplateComponent } from '../finance/accounting-template/accounting-template.component';
import { AccountingParameterComponent } from './accounting-parameter/accounting-parameter.component';

@NgModule({
  declarations: [
    LinkCompanyComponent,
    LederMasterComponent,
    LedgerGroupComponent,
    TaxAuthorityComponent,
    TaxRateComponent,
    ProfitCenterComponent,
    CostCenterComponent,
    FinanceComponent,
    AccountingTemplateComponent,
    AccountingParameterComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FinanceRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule,
  ],
})
export class FinanceModule {}
