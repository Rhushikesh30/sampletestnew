import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CostCenterComponent } from './cost-center/./cost-center.component';
import { LederMasterComponent } from './leder-master/./leder-master.component';
import { LedgerGroupComponent } from './ledger-group/./ledger-group.component';
import { LinkCompanyComponent } from './link-company/./link-company.component';
import { ProfitCenterComponent } from './profit-center/./profit-center.component';
import { TaxAuthorityComponent } from './tax-authority/./tax-authority.component';
import { TaxRateComponent } from './tax-rate/./tax-rate.component';
import { FinanceComponent } from './finance/finance.component';
import { AccountingTemplateComponent } from '../finance/accounting-template/accounting-template.component';



const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FinanceComponent
  },
  {
    path: 'costcenter',
    component: CostCenterComponent
  }, {
    path: 'ledermaster',
    component: LederMasterComponent
  }, {
    path: 'ledgergroup',
    component: LedgerGroupComponent
  },
  {
    path: 'linkcompany',
    component: LinkCompanyComponent
  },
  {
    path: 'profitcenter',
    component: ProfitCenterComponent
  },
  {
    path: 'taxauthority',
    component: TaxAuthorityComponent
  },
  {
    path: 'taxrate',
    component: TaxRateComponent
  },
  {
    path: 'accounttemplate',
    component: AccountingTemplateComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceRoutingModule { }
