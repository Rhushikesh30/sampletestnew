import {
  Component,
  ChangeDetectionStrategy,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';

import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';

import Swal from 'sweetalert2';
import { FinanceModule } from '../finance.module';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { MatMenuTrigger } from '@angular/material/menu';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { FormControl, FormGroup } from '@angular/forms';

import { FinanceService } from 'src/app/shared/services/finance.service';
import { CommonSetupService } from 'src/app/shared/services/common-setup.service';

@Component({
  selector: 'app-tax-rate',
  templateUrl: './tax-rate.component.html',
  styleUrls: ['./tax-rate.component.scss'],
  providers: [ErrorCodes],
})
export class TaxRateComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  renderedData: any = [];
  screenName = 'Tax Rate';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: DynamicFormService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  tableCols: any = [];
  tableData: any = [];
  ScreenJsonData: any = [];
  displayedColumns = [];
  BTN_VAL = 'Submit';
  showLoaderSubmit = false;
  form!: FormGroup;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private dynamicService: DynamicFormService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private financeservice: FinanceService,
    private commonsetupservice: CommonSetupService
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Finance Settings')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
          console.log(this.sidebarData);
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.financeservice.getDynamicScreenFormData(this.screenName).subscribe({
      next: (data: any) => {
        this.ScreenJsonData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.ScreenJsonData)),
    });

    this.refresh();
    this.getAllData();
  }
  getAllData() {
    this.financeservice
      .getDynamicScreenData(this.screenName)
      .subscribe((data: any) => {
        this.tableCols[0] = {
          mat_cell_name: 'Actions',
          mat_header_cell: 'Actions',
        };
        for (let i = 0; i < data['screenmatlistingcolumms_set'].length; i++) {
          this.tableCols[i + 1] = data['screenmatlistingcolumms_set'][i];
        }
        this.displayedColumns = this.tableCols.map((c: any) => c.mat_cell_name);
        this.tableData = data.screenmatlistingdata_set;

        console.log(this.tableCols);
      });
  }

  createVersion(row: any, flag: boolean) {
    this.BTN_VAL = 'Create Version';
    this.rowData = row;
    this.showList = false;
    this.listDiv = true;
    this.submitBtn = true;
  }

  editViewRecord(row: any, flag: boolean) {
    if (flag == false) {
      this.rowData = row;
      this.showList = false;
      this.submitBtn = false;
      this.listDiv = true;
    } else {
      this.rowData = row;
      this.showList = false;
      this.listDiv = true;
      this.submitBtn = true;
      this.BTN_VAL = 'Update';
    }
  }

  showFormList(item: boolean) {
    this.rowData = [];
    this.BTN_VAL = 'Submit';
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
      this.submitBtn = true;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    if (this.BTN_VAL == 'Create Version') {
      formValue['create_version'] = true;
    }

    this.showLoaderSubmit = true;
    this.commonsetupservice.createScreenData(formValue).subscribe(
      (data: any) => {
        if (data['status'] == 1) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 2) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 3) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your Version Created  successfully!',
              '',
              'success',
              false
            );
          }
        }
      },
      (error) => {
        this.showLoaderSubmit = false;
      }
    );
  }
  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DynamicFormService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    console.log(this.dataSource);
    console.log(typeof this.dataSource);
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    console.log(this.dataSource.filteredData);
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Tax Rate Name': x.tax_rate_name,
        'Tax Type': x.tax_authority_ref_id,
        'Tax Authority': x.tax_type_ref_id,
      }));
    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DynamicFormService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      //this._sort.sortChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables('Tax Rate');
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.tax_rate_name +
              advanceTable.tax_authority_ref_id +
              advanceTable.tax_type_ref_id
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });

        // Sort filtered data
        const sortedData = this.filteredData.slice();
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  /** Returns a sorted copy of the database data. */
  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'district_name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'taluka_name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
