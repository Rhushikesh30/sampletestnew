import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UntypedFormArray } from '@angular/forms';

import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { FederationService } from 'src/app/shared/services/federation.service';

import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [ErrorCodes],
})
export class ProfileComponent implements OnInit {
  federationForm!: FormGroup;
  btnVal = 'Update';
  fileKYC!: FormData;
  fileNameKYC = '';
  uploadStatus = '';
  previewImage: any;
  previewImageType = false;
  showFileLoader = false;
  showFileLoader1 = false;

  ownershipStatusData: { id: number; master_key: string }[] = [];
  transactionData: { id: number; master_key: string }[] = [];
  documentData: { id: number; master_key: string }[] = [];
  countrydata: { id: number; country_name: string }[] = [];
  stateData: { id: number; state_name: string }[] = [];
  districtData: { id: number; district_name: string }[] = [];
  talukaData: { id: number; sub_district_name: string }[] = [];
  empuserData: { id: number; name: string }[] = [];
  departmentdata: { id: number; name: string }[] = [];
  subdepartmentdata: { id: number; name: string }[] = [];
  designationdata: { id: number; name: string }[] = [];
  screenName: string = 'Federation';
  isDisable: boolean = false;

  uploadStatus1 = '';
  showReset = true;
  cancelFlag = true;
  previewImage1: any;
  previewImage2: any;
  previewImageType1: any;
  previewImageType2: any;
  fileName = '';
  fileName1 = '';
  fileKYC1!: FormData;
  submitBtn = true;
  showLoader = false;

  constructor(
    private formBuilder: FormBuilder,
    private federationService: FederationService,
    private errorCodes: ErrorCodes,
    private dynamicFormService: DynamicFormService,
    private encDecService: EncrDecrService,
    private router: Router,
    private encDecryService: EncrDecrService
  ) {}

  ngOnInit(): void {
    this.federationForm = this.formBuilder.group({
      id: [''],
      tenant_name: [''],
      tenant_short_name: [''],
      ownership_status: [''],
      email_id: [''],
      contact_person_name: [''],
      phone_no: [''],
      country: [''],
      state: [''],
      district: [''],
      taluka: [''],
      address: [''],
      address1: [''],
      pin_code: [''],
      pan_no: [''],
      tan_no: [''],
      gst_no: [''],
      cin_no: [''],
      tenant_logo_path: [''],
      header_section: [''],
      footer_section: [''],
      import_code: [''],
      export_code: [''],
      header_image_details: [''],
      header_image_type: [''], //ch
      header_text_details: [''],
      header_image_position: [''],
      header_text_position: [''],
      footer_image_details: [''],
      footer_image_type: [''], //ch
      footer_image_position: [''],
      footer_text_position: [''],
      footer_text_details: [''],
      contract_details: this.formBuilder.array([]),
      contact_details: this.formBuilder.array([this.contact_details()]),
      kyc_details: this.formBuilder.array([]),
    });

    this.federationService.getProfileData('profile').subscribe({
      next: (data: any) => {
        this.viewRecord(data[0]);
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.federationService.getAllMasterData('Ownership Type').subscribe({
      next: (data: any) => {
        this.ownershipStatusData = data;
      },
    });

    this.federationService.getAllMasterData('Transaction Type').subscribe({
      next: (data: any) => {
        this.transactionData = data;
      },
    });

    this.federationService.getAllMasterData('Document Type').subscribe({
      next: (data: any) => {
        this.documentData = data;
      },
    });

    this.dynamicFormService.getDynamicScreenData('Department').subscribe({
      next: (data: any) => {
        this.departmentdata = data.screenmatlistingdata_set;
      },
    });

    this.dynamicFormService.getDynamicScreenData('Sub Department').subscribe({
      next: (data: any) => {
        this.subdepartmentdata = data.screenmatlistingdata_set;
      },
    });

    this.dynamicFormService.getDynamicScreenData('Designation').subscribe({
      next: (data: any) => {
        this.designationdata = data.screenmatlistingdata_set;
      },
    });

    this.federationService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.countrydata = data;
      },
    });

    this.federationForm.get('country')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.countrySelected(val);
        } else {
          this.stateData = [];
        }
      },
      error: (e) => {
        this.stateData = [];
      },
    });

    this.federationForm.get('state')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.stateSelected(val);
        } else {
          this.districtData = [];
        }
      },
      error: (e) => {
        this.districtData = [];
      },
    });

    this.federationForm.get('district')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.districtSelected(val);
        } else {
          this.talukaData = [];
        }
      },
      error: (e) => {
        this.talukaData = [];
      },
    });
  }

  countrySelected(val: any) {
    if (val) {
      this.federationService.getStatesData(val).subscribe({
        next: (data: any) => {
          this.stateData = data;
        },
      });
    }
  }

  stateSelected(val: any) {
    if (val) {
      this.federationService.getDistrictData(val).subscribe({
        next: (data: any) => {
          this.districtData = data;
        },
      });
    }
  }

  districtSelected(val: any) {
    if (val) {
      this.federationService.getTalukaData(val).subscribe({
        next: (data: any) => {
          this.talukaData = data;
        },
      });
    }
  }

  getemployeeuser(val: any, i: any) {
    var gridRow = (<FormArray>this.federationForm.get('contact_details')).at(i);

    let employee_user = gridRow.get('emp_user')?.value;

    this.federationService.getAllempusrData(employee_user).subscribe({
      next: (data: any) => {
        this.empuserData = data;
      },
    });
  }

  showSwalmessage(message: any, icon: any) {
    Swal.fire({
      title: message,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  viewRecord(edata: any) {
    this.isDisable = true;
    if (edata.href_header_attachment_path != '') {
      this.previewImage1 = this.encDecService
        .decryptedData(edata.href_header_attachment_path)
        .toString();
    }
    if (edata.href_footer_attachment_path != '') {
      this.previewImage2 = this.encDecService
        .decryptedData(edata.href_footer_attachment_path)
        .toString();
    }
    this.federationForm.patchValue({
      id: edata.id,
      tenant_name: edata.tenant_name,
      tenant_short_name: edata.tenant_short_name,
      ownership_status: edata.ownership_status,
      email_id: edata.email_id,
      contact_person_name: edata.contact_person_name,
      phone_no: edata.phone_no,
      pan_no: edata.pan_no,
      tan_no: edata.tan_no,
      cin_no: edata.cin_no,
      gst_no: edata.gst_no,
      address: edata.address,
      address1: edata.address1,
      pin_code: edata.pin_code,
      country: edata.country,
      state: edata.state,
      district: edata.district,
      taluka: edata.taluka,
      tenant_logo_path: edata.tenant_logo_path,
      import_code: edata.import_code,
      export_code: edata.export_code,
      header_image_details: edata.header_image_details,
      header_text_details: edata.header_text_details,
      header_image_type: edata.header_image_type, //ch
      header_image_position: edata.header_image_position,
      header_text_position: edata.header_text_position,
      footer_image_details: edata.footer_image_details,
      footer_image_type: edata.footer_image_type, //ch
      footer_image_position: edata.footer_image_position,
      footer_text_position: edata.footer_text_position,
      footer_text_details: edata.footer_text_details,
    });

    let contactItemRow = edata.contact_details.filter(
      (data: any) => data.is_deleted == false && data.is_active == true
    );
    if (contactItemRow.length > 0) {
      this.federationForm.setControl(
        'contact_details',
        this.setExistingArray2(contactItemRow)
      );
    }
    this.federationForm.disable();
    this.federationForm.controls['header_image_details'].enable();
    this.federationForm.controls['footer_text_details'].enable();
    this.federationForm.controls['header_text_details'].enable();
    this.federationForm.controls['header_image_position'].enable();
    this.federationForm.controls['header_text_position'].enable();
    this.federationForm.controls['footer_image_details'].enable();
    this.federationForm.controls['footer_image_position'].enable();
    this.federationForm.controls['footer_text_position'].enable();
    this.federationForm.controls['header_image_type'].enable();
    this.federationForm.controls['footer_image_type'].enable();
    this.federationForm.controls['contact_details'].enable();
  }

  setAttachment(event: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';

    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage1 = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType1 = true;
      }
      this.fileName = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        let formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileKYC1 = formData1;
        this.showFileLoader = true;
        this.uploadStatus = '';
        this.federationService.saveFile(this.fileKYC1).subscribe({
          next: (data: any) => {
            this.showFileLoader = false;
            let files3path = data['s3_file_path'];

            this.federationForm
              .get('header_image_details')
              ?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e) => {
            this.uploadStatus = '';
            this.fileName = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileName = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.showSwalmessage1('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  setAttachment1(event: any) {
    this.previewImageType2 = false;
    this.previewImage2 = '';

    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage2 = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType2 = true;
      }
      this.fileName1 = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        let formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileKYC1 = formData1;
        this.showFileLoader1 = true;
        this.uploadStatus1 = '';
        this.federationService.saveFile(this.fileKYC1).subscribe({
          next: (data: any) => {
            this.showFileLoader1 = false;
            let files3path = data['s3_file_path'];

            this.federationForm
              .get('footer_image_details')
              ?.setValue(files3path);
            this.uploadStatus1 = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e) => {
            this.uploadStatus1 = '';
            this.fileName1 = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus1 = '';
        this.fileName1 = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.showSwalmessage1('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  removeImage() {
    this.previewImage1 = '';
  }
  removeImage1() {
    this.previewImage2 = '';
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  contract_details() {
    return this.formBuilder.group({
      id: [''],
      from_date: [''],
      to_date: [''],
      revision_status: [''],
      max_no_of_users: [''],
      concurent_user: [''],
      api_limit: [''],
      api_calls_blocked_for_in_minutes: [''],
      application_blocking_in_minutes: [''],
    });
  }

  setExistingArray1(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          from_date: element.from_date,
          to_date: element.to_date,
          revision_status: element.revision_status,
          max_no_of_users: element.max_no_of_users,
          concurent_user: element.concurent_user,
          api_limit: element.api_limit,
          api_calls_blocked_for_in_minutes:
            element.api_calls_blocked_for_in_minutes,
          application_blocking_in_minutes:
            element.application_blocking_in_minutes,
        })
      );
    });
    return formArray;
  }

  get formArrcontract() {
    return this.federationForm.get('contract_details') as UntypedFormArray;
  }

  addNewRow1() {
    this.formArrcontract.push(this.contract_details());
  }

  deleteRow1(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontract.removeAt(index);
      return true;
    }
  }

  contact_details() {
    return this.formBuilder.group({
      id: [''],
      full_name: [''],
      phone_no: [
        '',
        [Validators.maxLength(10), Validators.pattern('^[0-9_]*$')],
      ],
      email: ['', [Validators.email]],
      department: [0],
      sub_department: [0],
      designation: [0],
    });
  }

  setExistingArray2(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          full_name: element.full_name,
          phone_no: element.phone_no,
          email: element.email,
          department: element.department,
          sub_department: element.sub_department,
          designation: element.designation,
        })
      );
    });
    return formArray;
  }

  get formArrcontact() {
    return this.federationForm.get('contact_details') as UntypedFormArray;
  }

  addNewRow2() {
    this.formArrcontact.push(this.contact_details());
  }

  deleteRow2(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontact.removeAt(index);
      return true;
    }
  }

  kyc_details() {
    return this.formBuilder.group({
      id: [''],
      document_name: [''],
      document_seq_number: [''],
      document_path: [''],
      href_attachment_path: [''],
    });
  }

  setExistingArray3(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          document_name: element.document_name,
          document_seq_number: element.document_seq_number,
          document_path: element.document_path,
          href_attachment_path: atob(element.href_attachment_path),
        })
      );
    });
    return formArray;
  }

  get formArrkyc() {
    return this.federationForm.get('kyc_details') as UntypedFormArray;
  }

  addNewRow3() {
    this.formArrkyc.push(this.kyc_details());
  }

  deleteRow3(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrkyc.removeAt(index);
      return true;
    }
  }

  onSubmit() {
    this.showLoader = true;
    this.federationForm.enable();
    if (this.federationForm.invalid) {
      this.showLoader = false;
      return;
    } else {
      this.federationForm.value.contact_details.forEach((ele: any) => {
        if (ele.full_name == '') {
          this.federationForm.value.contact_details = [];
        }
      });
      let json_data = {
        data: this.encDecryService.encryptedData(this.federationForm.value),
      };
      this.federationService
        .createFederation(json_data, this.federationForm.value.id)
        .subscribe({
          next: (data: any) => {
            if (data['status'] == 1) {
              this.showSwalMassage(
                'Your record has been updated successfully!',
                'success'
              );
              this.router.navigate(['/dashboard/main']).then(() => {
                window.location.reload();
              }); //ch
            } else if (data['status'] == 2) {
              this.showSwalMassage(
                'Your record has been added successfully!',
                'success'
              );
              this.router.navigate(['/dashboard/main']); //ch
            }

            this.showLoader = false;
          },
          error: (e) => {
            this.showLoader = false;
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    }
  }

  header_image_type(val: any) {
    if (val == 'logo') {
      this.federationForm.controls['header_text_details'].enable();
      this.federationForm.controls['header_image_position'].enable();
      this.federationForm.controls['header_text_position'].enable();
    } else if (val == 'full_image') {
      this.federationForm.get('header_image_position')?.setValue('');
      this.federationForm.get('header_text_position')?.setValue('');
      this.federationForm.get('header_text_details')?.setValue('');
      this.federationForm.controls['header_text_details'].disable();
      this.federationForm.controls['header_image_position'].disable();
      this.federationForm.controls['header_text_position'].disable();
    }
  }
  footer_image_type(val: any) {
    if (val == 'logo') {
      this.federationForm.controls['footer_image_position'].enable();
      this.federationForm.controls['footer_text_position'].enable();
      this.federationForm.controls['footer_text_details'].enable();
    } else if (val == 'full_image') {
      this.federationForm.get('footer_image_position')?.setValue('');
      this.federationForm.get('footer_text_position')?.setValue('');
      this.federationForm.get('footer_text_details')?.setValue('');
      this.federationForm.controls['footer_image_position'].disable();
      this.federationForm.controls['footer_text_position'].disable();
      this.federationForm.controls['footer_text_details'].disable();
    }
  }

  showSwalmessage1(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
