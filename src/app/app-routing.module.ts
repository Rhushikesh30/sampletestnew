import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from './authentication/page404/page404.component';
import { AuthGuard } from './core/guard/auth.guard';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { GatePassPrintComponent } from './gate-pass-print-module/gate-pass-print/gate-pass-print.component';
import { PrintfarmerbillComponent } from './printfamer/printfarmerbill/printfarmerbill.component';
import { PrintInvoiceComponent } from './invoice-print/print-invoice/print-invoice.component';
import { SalesOrderPrintComponent } from './sales-order-print/sales-order-print/sales-order-print.component';
import { PurchaseOrderPrintComponent } from './purchase-order-print/purchase-order-print/purchase-order-print.component';
import { PrintPurchasebookingComponent } from './print-purchasebooking/print-purchasebooking/print-purchasebooking.component';
import { GrnPrintComponent } from './grn-print/grn-print/grn-print.component';
function generateComingSoonRoute(path: string): any {
  return {
    path,
    loadChildren: () =>
      import('../app/coming-soon/coming-soon.module').then(
        (m) => m.ComingSoonModule
      ),
  };
}

const paths = [
  'fer',
  'pecd',
  'seed',
  'bio-che',
  'feed',
  'advc',
  'processing',
  'contract-farming',
  'custom-hiring',
  'facility-center',
  'services-farmer',
  'analytics',
  'hr-manage',
  'finace-services',
  'bulk-upload/null',
  'procurement/null',
  'Reports/null',
];

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/authentication/signin', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'events',
        loadChildren: () =>
          import('./events/events.module').then((m) => m.EventsModule),
      },

      {
        path: 'subscription',
        loadChildren: () =>
          import('./subsciption/subsciption.module').then(
            (m) => m.SubsciptionModule
          ),
      },
      {
        path: 'advance-table',
        loadChildren: () =>
          import('./advance-table/advance-table.module').then(
            (m) => m.AdvanceTableModule
          ),
      },
      {
        path: 'federation-setup',
        loadChildren: () =>
          import('./federation-setup/federation-setup.module').then(
            (m) => m.FederationSetupModule
          ),
      },
      {
        path: 'fpc-setup',
        loadChildren: () =>
          import('../app/fpc-setup/fpc-setup.module').then(
            (m) => m.FpcSetupModule
          ),
      },
      {
        path: 'user',
        loadChildren: () =>
          import('../app/employee/employee.module').then(
            (m) => m.EmployeeModule
          ),
      },
      {
        path: 'common-setup',
        loadChildren: () =>
          import('./common-setup/common-setup.module').then(
            (m) => m.CommonSetupModule
          ),
      },
      {
        path: 'item-setup',
        loadChildren: () =>
          import('../app/item-master/item-master.module').then(
            (m) => m.ItemMasterModule
          ),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('../app/profile/profile.module').then((m) => m.ProfileModule),
      },
      {
        path: 'agent',
        loadChildren: () =>
          import('../app/registraion/registraion.module').then(
            (m) => m.RegistraionModule
          ),
      },
      {
        path: 'customer',
        loadChildren: () =>
          import('../app/registraion/registraion.module').then(
            (m) => m.RegistraionModule
          ),
      },
      {
        path: 'supplier',
        loadChildren: () =>
          import('../app/registraion/registraion.module').then(
            (m) => m.RegistraionModule
          ),
      },
      {
        path: 'calendar-setup',
        loadChildren: () =>
          import('../app/calendar/calendar.module').then(
            (m) => m.CalendarModule
          ),
      },
      {
        path: 'farmer',
        loadChildren: () =>
          import('../app/farmer/farmer.module').then((m) => m.FarmerModule),
      },
      {
        path: 'finance/finance',
        loadChildren: () =>
          import('../app/finance/finance.module').then((m) => m.FinanceModule),
      },
      {
        path: 'finance/chart-of-acc',
        loadChildren: () =>
          import('../app/chart-of-account/chart-of-account.module').then(
            (m) => m.ChartOfAccountModule
          ),
      },
      {
        path: 'accounting',
        loadChildren: () =>
          import('../app/accounting/accounting.module').then(
            (m) => m.AccountingModule
          ),
      },
      {
        path: 'manage-security',
        loadChildren: () =>
          import('../app/manage-security/manage-security.module').then(
            (m) => m.ManageSecurityModule
          ),
      },
      {
        path: 'sales',
        loadChildren: () =>
          import('../app/sales/sales.module').then((m) => m.SalesModule),
      },
      {
        path: 'purchase',
        loadChildren: () =>
          import('../app/purchase/purchase.module').then(
            (m) => m.PurchaseModule
          ),
      },

      {
        path: 'report',
        loadChildren: () =>
          import('../app/purchase/report/report.module').then(
            (m) => m.ReportModule
          ),
      },
      {
        path: 'report/finance',
        loadChildren: () =>
          import('../app/reports/reports.module').then((m) => m.ReportsModule),
      },
      {
        path: 'report/inventory',
        loadChildren: () =>
          import('../app/reports/reports.module').then((m) => m.ReportsModule),
      },
      {
        path: 'journal-voucher/purchase',
        loadChildren: () =>
          import('../app/journal-voucher/journal-voucher.module').then(
            (m) => m.JournalVoucherModule
          ),
      },
      {
        path: 'finance/accounting-parameter',
        loadChildren: () =>
          import(
            '../app/accounting-parameter/accounting-parameter.module'
          ).then((m) => m.AccountingParameterModule),
      },
      {
        path: 'purchase/stock-adjestment',
        loadChildren: () =>
          import('../app/stock-adjestment/stock-adjestment.module').then(
            (m) => m.StockAdjestmentModule
          ),
      },
      {
        path: 'printfamer',
        loadChildren: () =>
          import('../app/printfamer/printfamer.module').then(
            (m) => m.PrintfamerModule
          ),
      },

      ...paths.map((path) => generateComingSoonRoute(path)),
    ],
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
  },
  {
    path: 'gatepassprint',
    component: GatePassPrintComponent,
    loadChildren: () =>
      import(
        '../app/gate-pass-print-module/gate-pass-print-module.module'
      ).then((m) => m.GatePassPrintModuleModule),
  },

  {
    path: 'printfarmer',
    component: PrintfarmerbillComponent,
    loadChildren: () =>
      import('../app/printfamer/printfamer.module').then(
        (m) => m.PrintfamerModule
      ),
  },
  {
    path: 'invoiceprint',
    component: PrintInvoiceComponent,
    loadChildren: () =>
      import('../app/invoice-print/invoice-print.module').then(
        (m) => m.InvoicePrintModule
      ),
  },
  {
    path: 'sales-order-print',
    component: SalesOrderPrintComponent,
    loadChildren: () =>
      import('../app/sales-order-print/sales-order-print.module').then(
        (m) => m.SalesOrderPrintModule
      ),
  },
  {
    path: 'purchase-order-print',
    component: PurchaseOrderPrintComponent,
    loadChildren: () =>
      import('../app/purchase-order-print/purchase-order-print.module').then(
        (m) => m.PurchaseOrderPrintModule
      ),
  },
  {
    path: 'purchasebooking-print',
    component: PrintPurchasebookingComponent,
    loadChildren: () =>
      import('../app/print-purchasebooking/print-purchasebooking.module').then(
        (m) => m.PrintPurchasebookingModule
      ),
  },
  {
    path: 'grn-print',
    component: GrnPrintComponent,
    loadChildren: () =>
      import('../app/grn-print/grn-print.module').then((m) => m.GrnPrintModule),
  },

  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
