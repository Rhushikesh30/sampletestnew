import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalVoucherDetailsComponent } from './journal-voucher-details.component';

describe('JournalVoucherDetailsComponent', () => {
  let component: JournalVoucherDetailsComponent;
  let fixture: ComponentFixture<JournalVoucherDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JournalVoucherDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JournalVoucherDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
