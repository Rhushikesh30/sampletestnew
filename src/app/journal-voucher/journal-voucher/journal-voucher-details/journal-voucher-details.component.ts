import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MAT_DATE_FORMATS } from '@angular/material/core';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { JournalVoucherBookingService } from 'src/app/shared/services/journal-voucher-booking.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { ExpenseBookingService } from 'src/app/shared/services/expense-booking.service';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-journal-voucher-details',
  templateUrl: './journal-voucher-details.component.html',
  styleUrls: ['./journal-voucher-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class JournalVoucherDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() screenName!: string;

  cancelFlag = true;
  viewBtn = true;
  myTemplate = '';
  JVForm!: FormGroup;
  btnVal = 'Temporary Save';
  CurrencyData: any = [];
  txn_currency: any;
  AccountData: any;
  DebitCreditTypeData: any;
  filterAccountData: any;

  columnsData: any[] = [];
  columnsValues: any;

  todayDate = new Date().toJSON().split('T')[0];
  expenseDetailsParametersColumnsValues: any;
  constructor(
    private formBuilder: FormBuilder,
    private fpcService: FpcSetupService,
    private errorCodes: ErrorCodes,
    public datepipe: DatePipe,
    private JournalVoucherBookingService: JournalVoucherBookingService,
    private expenseBookingService: ExpenseBookingService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.viewEditRecord(this.rowData);
    }
    this.expenseBookingService.getAccountingParameters().subscribe({
      next: (data: any) => {
        console.log('accounting_parameter: ', data);
        this.columnsData = data.columns;
        this.columnsValues = data.data;
      },
    });
    this.JournalVoucherBookingService.getDynamicData(
      'chart_of_account',
      'name'
    ).subscribe({
      next: (data: any) => {
        this.AccountData = data;
        console.log(data);

        this.filterAccountData = this.AccountData.slice();
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.JournalVoucherBookingService.getSessionData().subscribe({
      next: (data: any) => {
        console.log(data);

        this.JVForm.get('fiscal_year')?.setValue(data['fiscal_year']);
        this.JVForm.get('period')?.setValue(data['period']);
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.JVForm.get('txn_currency')?.setValue(this.txn_currency[0]['id']);
      },
    });

    this.fpcService.getAllMasterData('Journal Voucher Type').subscribe({
      next: (data: any) => {
        this.DebitCreditTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
  getColumValues(paramter: any, id: any) {
    const arrayOfElement = this.columnsValues[paramter];
    console.log('arrayOfElement: ', arrayOfElement);
    const x = arrayOfElement.find((element: any) => element.id == id);
    console.log('x: ', x);
    return x?.['name'] || 'NA';
  }
  getParameter(i: any) {
    const paramter = 'parameter_' + i;
    return this.columnsValues[paramter];
  }
  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.JVForm = this.formBuilder.group({
      id: [''],
      transaction_date: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      txn_currency: [''],
      tenant_id: [''],
      ref_transaction_id: [''],
      ref_transaction_type_id: [''],
      base_currency: [''],
      conversion_rate: [1],
      txn_currency_debit_amount: [''],
      txn_currency_credit_amount: [''],
      base_currency_debit_amount: [''],
      base_currency_credit_amount: [''],
      posting_status: [''],
      posting_ref_no: [''],
      is_temporary_flag: [true],
      journal_voucher_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      txn_currency: [''],
      tenant_id: [''],
      ref_transaction_id: [''],
      ref_transaction_type_id: [''],
      account_ref_id: ['', Validators.required],
      conversion_rate: [1],
      txn_currency_amount: ['', Validators.required],
      txn_currency_debit_amount: [''],
      txn_currency_credit_amount: [''],
      base_currency_amount: [''],
      base_currency_debit_amount: [''],
      base_currency_credit_amount: [''],
      posting_status: [''],
      posting_ref_no: [''],
      debit_credit_type_id: ['', Validators.required],
      parameter_1: [0],
      parameter_2: [0],
      parameter_3: [0],
      parameter_4: [0],
      parameter_5: [0],
      parameter_6: [0],
      parameter_7: [0],
      parameter_8: [0],
      parameter_9: [0],
    });
  }

  get formArray() {
    return this.JVForm.get('journal_voucher_details') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      return true;
    }
  }

  viewEditRecord(row1: any) {
    this.JournalVoucherBookingService.getJVDataById(row1.id).subscribe({
      next: (row: any) => {
        console.log(row);
        row = row[0];
        this.JVForm.patchValue({
          id: row.id,
          transaction_id: row.transaction_id,
          transaction_ref_no: row.transaction_ref_no,
          transaction_type_id: row.transaction_type_id,
          period: row.period,
          ledger_group: row.ledger_group,
          fiscal_year: row.fiscal_year,
          created_by: row.created_by,
          base_currency: row.base_currency,
          transaction_date: row.transaction_date,
          txn_currency: row.txn_currency,
          tenant_id: row.tenant_id,
          is_active: row.is_active,
          is_deleted: row.is_deleted,
          ref_transaction_id: row.ref_transaction_id,
          ref_transaction_type_id: row.ref_transaction_type_id,
          conversion_rate: row.conversion_rate,
          txn_currency_debit_amount: row.txn_currency_debit_amount,
          txn_currency_credit_amount: row.txn_currency_credit_amount,
          base_currency_debit_amount: row.base_currency_debit_amount,
          base_currency_credit_amount: row.base_currency_credit_amount,
          posting_status: row.posting_status,
          posting_ref_no: row.posting_ref_no,
          is_temporary_flag: row.is_temporary_flag,
        });

        console.log(this.txn_currency);

        const ItemRow = row.journal_voucher_details.filter(function (
          data: any
        ) {
          return data.is_deleted == false && data.is_active == true;
        });

        this.JVForm.setControl(
          'journal_voucher_details',
          this.setExistingArray(ItemRow)
        );
      },
    });

    if (this.submitBtn) {
      //   this.btnVal = 'Update';
      this.viewBtn = false;
      if (this.JVForm.get('is_temporary_flag')?.value == true) {
        this.btnVal = 'Temporary Save';
      } else {
        this.btnVal = 'Permanent Save';
      }
    } else {
      this.JVForm.disable();
    }
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);

    initialArray.forEach((element: any) => {
      this.fpcService.getAllMasterData('Journal Voucher Type').subscribe({
        next: (data: any) => {
          this.DebitCreditTypeData = data;
          const del_type = this.DebitCreditTypeData.filter(
            (d: any) => d.master_key == 'Debit'
          );
          const del_type1 = this.DebitCreditTypeData.filter(
            (d: any) => d.master_key == 'Credit'
          );

          let debit_credit_type_id;

          if (
            element.txn_currency_debit_amount != 0.0 ||
            element.txn_currency_debit_amount != 0.0
          ) {
            debit_credit_type_id = del_type[0]['id'];
          }
          if (
            element.txn_currency_credit_amount != 0.0 ||
            element.txn_currency_credit_amount != 0.0
          ) {
            debit_credit_type_id = del_type1[0]['id'];
          }
          console.log(debit_credit_type_id);

          formArray.push(
            this.formBuilder.group({
              id: element.id,
              account_ref_id: element.account_ref_id,
              txn_currency: element.txn_currency,
              txn_currency_debit_amount: element.txn_currency_debit_amount,
              txn_currency_credit_amount: element.txn_currency_credit_amount,
              base_currency_debit_amount: element.base_currency_debit_amount,
              base_currency_credit_amount: element.base_currency_credit_amount,
              txn_currency_amount: Math.abs(element.txn_currency_amount),
              ref_id_5: element.ref_id_5,
              ref_id_6: element.ref_id_6,
              ref_id_7: element.ref_id_7,
              ref_id_8: element.ref_id_8,
              ref_id_9: element.ref_id_9,
              ref_id_10: element.ref_id_10,
              parameter_1:
                element.parameter_1 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_1),
              parameter_2:
                element.parameter_2 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_2),
              parameter_3:
                element.parameter_3 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_3),
              parameter_4:
                element.parameter_4 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_4),
              parameter_5:
                element.parameter_5 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_5),
              parameter_6:
                element.parameter_6 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_6),
              parameter_7:
                element.parameter_7 === 'ALL'
                  ? 'ALL'
                  : Number(element.parameter_7),

              transaction_id: element.transaction_id,
              transaction_ref_no: element.transaction_ref_no,
              transaction_type_id: element.transaction_type_id,
              period: element.period,
              ledger_group: element.ledger_group,
              fiscal_year: element.fiscal_year,
              created_by: element.created_by,
              base_currency: element.base_currency,
              posting_status: element.posting_status,
              posting_ref_no: element.posting_ref_no,
              ref_transaction_id: element.ref_transaction_id,
              ref_transaction_type_id: element.ref_transaction_type_id,
              conversion_rate: element.conversion_rate,
              debit_credit_type_id: debit_credit_type_id,
            })
          );
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    });
    return formArray;
  }

  onSubmit() {
    console.log(this.JVForm.value);

    if (this.JVForm.invalid) {
      const invalid = [];
      const controls = this.JVForm.controls;
      const fc: any = this.JVForm.controls;

      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }

      for (let i = 0; i < fc.journal_voucher_details.controls.length; i++) {
        const md = fc.journal_voucher_details.controls[i].controls;
        if (md.invalid) {
          md.markAsTouched();
        }
      }

      return;
    } else {
      const result = this.checkVoucherDetails();
      if (result == true) {
        console.log(
          this.JVForm.get('txn_currency_credit_amount')?.value,
          this.JVForm.get('txn_currency_debit_amount')?.value
        );
        console.log(
          parseFloat(this.JVForm.get('txn_currency_debit_amount')?.value) +
            parseFloat(this.JVForm.get('txn_currency_credit_amount')?.value)
        );

        if (
          parseFloat(this.JVForm.get('txn_currency_debit_amount')?.value) +
            parseFloat(this.JVForm.get('txn_currency_credit_amount')?.value) ==
          0
        ) {
          this.handleSave.emit(this.JVForm.getRawValue());
        } else {
          Swal.fire({
            title: 'Not allowed',
            text: 'Please Check Transaction!',
            icon: 'warning',
          });
        }
      }
    }
  }

  checkVoucherDetails() {
    const gridRow2 = <FormArray>this.JVForm.get('journal_voucher_details');

    let isBillingAddressSelected = false;
    let isshippingAddressSelected = false;
    let status = true;
    for (let j = 0; j < gridRow2.length; j++) {
      const del_type = this.DebitCreditTypeData.filter(
        (d: any) => d.id == gridRow2.at(j).get('debit_credit_type_id')?.value
      );
      for (let l = 0; l < gridRow2.length; l++) {
        if (del_type[0]['master_key'] == 'Debit') {
          isBillingAddressSelected = true;
          break;
        }
      }
      for (let l = 0; l < gridRow2.length; l++) {
        if (del_type[0]['master_key'] == 'Credit') {
          isshippingAddressSelected = true;
          break;
        }
      }
    }

    if (!isBillingAddressSelected) {
      status = false;
      Swal.fire({
        title: 'Not allowed',
        text: 'Please Select at least One Debit Account!',
        icon: 'warning',
      });
    }
    if (!isshippingAddressSelected) {
      status = false;
      Swal.fire({
        title: 'Not allowed',
        text: 'Please Select at least One Credit Account!',
        icon: 'warning',
      });
    }
    return status;
  }

  onResetForm() {
    this.initializeForm();
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.JVForm.reset();
    this.handleCancel.emit(false);
  }

  is_temporary_flag_change(val: any) {
    if (val.checked == true) {
      this.btnVal = 'Temporary Save';
      //  this.invoiceForm.get('status').setValue('Temporary Save')
      this.JVForm.get('is_temporary_flag')?.setValue(true);
    } else {
      this.btnVal = 'Permanent Save';
      this.JVForm.get('is_temporary_flag')?.setValue(false);
    }
  }

  TxnAmtChange(event: any, index: any) {
    const rec = event.target.value;
    if (rec == '' || rec == undefined || rec == 'None') {
      console.log('====================================');
      console.log('rec', rec);
      console.log('====================================');
    } else {
      const gridRow1 = (<FormArray>(
        this.JVForm.get('journal_voucher_details')
      )).at(index);
      const val = gridRow1.get('debit_credit_type_id')?.value;

      const del_type = this.DebitCreditTypeData.filter((d: any) => d.id == val);
      console.log(rec);

      if (del_type[0]['master_key'] == 'Debit') {
        // gridRow1.get('txn_currency_amount')?.setValue(rec)
        gridRow1.get('txn_currency_debit_amount')?.setValue(rec);
        gridRow1.get('txn_currency_credit_amount')?.setValue(0);
      } else if (del_type[0]['master_key'] == 'Credit') {
        //  gridRow1.get('txn_currency_amount')?.setValue(-rec)
        gridRow1.get('txn_currency_credit_amount')?.setValue(-rec);
        gridRow1.get('txn_currency_debit_amount')?.setValue(0);
      }
      this.CalculateTotalTxnAmount();
    }
  }

  DebitCreditChange(event: any, index: any) {
    const rec = event.value;
    if (rec == '' || rec == undefined || rec == 'None') {
      console.log('====================================');
      console.log('rec', rec);
      console.log('====================================');
    } else {
      const gridRow1 = (<FormArray>(
        this.JVForm.get('journal_voucher_details')
      )).at(index);

      const del_type = this.DebitCreditTypeData.filter((d: any) => d.id == rec);

      const txm_amt = gridRow1.get('txn_currency_amount')?.value;
      if (txm_amt == '' || txm_amt == undefined || txm_amt == 'None') {
        console.log('====================================');
        console.log('txm_amt', txm_amt);
        console.log('====================================');
      } else {
        if (del_type[0]['master_key'] == 'Debit') {
          //   gridRow1.get('txn_currency_amount')?.setValue(txm_amt)
          gridRow1.get('txn_currency_debit_amount')?.setValue(txm_amt);
          gridRow1.get('txn_currency_credit_amount')?.setValue(0);
        } else if (del_type[0]['master_key'] == 'Credit') {
          //    gridRow1.get('txn_currency_amount')?.setValue(-txm_amt)
          gridRow1.get('txn_currency_credit_amount')?.setValue(-txm_amt);
          gridRow1.get('txn_currency_debit_amount')?.setValue(0);
        }
        this.CalculateTotalTxnAmount();
      }
    }
  }

  CalculateTotalTxnAmount() {
    const gridRow2 = <FormArray>this.JVForm.get('journal_voucher_details');

    let total_debit_amt: any = 0;
    let total_credit_amt: any = 0;

    for (let i = 0; i < gridRow2.length; i++) {
      total_credit_amt =
        parseFloat(total_credit_amt) +
        parseFloat(gridRow2.at(i).get('txn_currency_credit_amount')?.value);
      total_debit_amt =
        parseFloat(total_debit_amt) +
        parseFloat(gridRow2.at(i).get('txn_currency_debit_amount')?.value);
    }

    this.JVForm.get('txn_currency_debit_amount')?.setValue(total_debit_amt);
    this.JVForm.get('txn_currency_credit_amount')?.setValue(total_credit_amt);
  }

  AccountNameChange(e: any, i: any) {
    const gridRow = (<FormArray>this.JVForm.get('journal_voucher_details')).at(
      i
    );
    console.log('gridrow', gridRow);

    const fieldLength = gridRow.get('account_ref_id')?.value;
    console.log('fieldLength', fieldLength);

    const getValue = (<FormArray>this.JVForm.get('journal_voucher_details'))
      .value;
    console.log('getValue', getValue);

    if (
      getValue.filter((data: any) => data.account_ref_id == fieldLength)
        .length > 1
    ) {
      const questions = this.AccountData.filter(
        (data: any) => data.id == fieldLength
      );
      Swal.fire({
        title: 'Not allowed',
        text: 'Please select another account name!',
        icon: 'warning',
      });
      gridRow.get('account_ref_id')?.setValue('');
      return;
    }
  }
}
