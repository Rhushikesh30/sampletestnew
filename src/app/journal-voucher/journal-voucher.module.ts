import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JournalVoucherRoutingModule } from './journal-voucher-routing.module';
import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';
import { JournalVoucherDetailsComponent } from './journal-voucher/journal-voucher-details/journal-voucher-details.component';


import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ComponentsModule } from '../shared/components/components.module';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTimepickerModule } from 'mat-timepicker';
import { MomentDateModule } from '@angular/material-moment-adapter';


@NgModule({
  declarations: [
    JournalVoucherComponent,
    JournalVoucherDetailsComponent
  ],
  imports: [
    CommonModule,
    JournalVoucherRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatCheckboxModule,
    ComponentsModule,
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,
    MatDialogModule,
    MatTimepickerModule,
    MomentDateModule
  ]
})
export class JournalVoucherModule { }
