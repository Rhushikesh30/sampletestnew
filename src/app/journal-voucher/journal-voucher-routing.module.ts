import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JournalVoucherComponent } from './journal-voucher/journal-voucher.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: JournalVoucherComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JournalVoucherRoutingModule { }
