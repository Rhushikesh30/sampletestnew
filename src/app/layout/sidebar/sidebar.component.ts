/* eslint-disable @typescript-eslint/no-unused-vars */
import { DOCUMENT } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  Renderer2,
  HostListener,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';
import { AuthService } from 'src/app/core/service/auth.service';
import { RouteInfo } from './sidebar.metadata';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';

export let ROUTES: RouteInfo[] = [];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarComponent implements OnInit, OnDestroy {
  public sidebarItems!: RouteInfo[];
  public innerHeight?: number;
  public bodyTag!: HTMLElement;
  listMaxHeight?: string;
  listMaxWidth?: string;
  headerHeight = 60;
  routerObj;
  screenToRole: any;
  leftpanelparentdata: any;
  leftpanelchilddata: any;
  leftpanelsubchilddata: any;
  role_ref_id: any;
  roles: any = localStorage.getItem('roles');

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private authService: AuthService,
    private router: Router,
    private roleSecurityService: RoleSecurityService
  ) {
    this.routerObj = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // close sidebar on mobile screen after menu select
        this.renderer.removeClass(this.document.body, 'overlay-open');
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  windowResizecall() {
    this.setMenuHeight();
    this.checkStatuForResize(false);
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event: Event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.renderer.removeClass(this.document.body, 'overlay-open');
    }
  }
  callToggleMenu(event: Event, length: number) {
    if (length > 0) {
      const parentElement = (event.target as HTMLInputElement).closest('li');
      const activeClass = parentElement?.classList.contains('active');
      if (activeClass) {
        this.renderer.removeClass(parentElement, 'active');
      } else {
        this.renderer.addClass(parentElement, 'active');
      }
    }
  }
  ngOnInit() {
    if (this.authService.currentUserValue) {
      this.roles = JSON.parse(this.roles);
      const role_id = [];
      for (let i = 0; i < this.roles.length; i++) {
        role_id.push(this.roles[i].role_id);
      }
      ROUTES = [];

      this.roleSecurityService
        .getLeftPanelData(localStorage.getItem('user_id'), role_id.toString())
        .subscribe((lpdata: []) => {
          this.leftpanelparentdata = lpdata;
          for (let i = 0; i < this.leftpanelparentdata.length; i++) {
            let groupTitle = false;
            let path = '';
            if (this.leftpanelparentdata[i].submenu_level1.length == 0) {
              groupTitle = true;
              path = this.leftpanelparentdata[i].module_path;
            }
            ROUTES.push({
              path: path,
              title: this.leftpanelparentdata[i].form_name,
              moduleName: this.leftpanelparentdata[i].module_path,
              parent_code: this.leftpanelparentdata[i].parent_code,
              child_code: this.leftpanelparentdata[i].child_code,
              icon: this.leftpanelparentdata[i].icon_class,
              class: 'menu-toggle',
              groupTitle: groupTitle,
              sequenceId: this.leftpanelparentdata[i].sequence_id,
              submenu: [],
              groupTitleName: this.leftpanelparentdata[i].group_title,
            });
            this.leftpanelchilddata =
              this.leftpanelparentdata[i].submenu_level1;
            for (let j = 0; j < this.leftpanelchilddata.length; j++) {
              ROUTES.forEach((x) => {
                if (
                  x.parent_code == this.leftpanelchilddata[j].parent_code &&
                  this.leftpanelchilddata[j].form_link != ''
                ) {
                  let cclass = '';
                  const exis = this.leftpanelchilddata[j].submenu_level2;
                  if (exis.length > 0) {
                    cclass = 'ml-sub-menu';
                  } else {
                    cclass = 'ml-menu2';
                  }
                  x.submenu.push({
                    path:
                      this.leftpanelchilddata[j].module_path +
                      '/' +
                      this.leftpanelchilddata[j].form_link,
                    title: this.leftpanelchilddata[j].form_name,
                    moduleName: this.leftpanelchilddata[j].form_link,
                    parent_code: this.leftpanelchilddata[j].parent_code,
                    child_code: this.leftpanelchilddata[j].child_code,
                    icon: this.leftpanelchilddata[j].icon_class,
                    class: cclass,
                    groupTitle: false,
                    sequenceId: this.leftpanelchilddata[j].sequence_id,
                    submenu: [],
                    groupTitleName: this.leftpanelchilddata[j].group_title,
                  });
                } else if (
                  x.parent_code == this.leftpanelchilddata[j].parent_code
                ) {
                  x.submenu.push({
                    path: this.leftpanelchilddata[j].module_path,
                    title: this.leftpanelchilddata[j].form_name,
                    moduleName: this.leftpanelchilddata[j].form_link,
                    parent_code: this.leftpanelchilddata[j].parent_code,
                    child_code: this.leftpanelchilddata[j].child_code,
                    icon: this.leftpanelchilddata[j].icon_class,
                    class: 'ml-sub-menu',
                    groupTitle: false,
                    sequenceId: this.leftpanelchilddata[j].sequence_id,
                    submenu: [],
                    groupTitleName: this.leftpanelchilddata[j].group_title,
                  });
                }
              });
              this.leftpanelsubchilddata =
                this.leftpanelchilddata[j].submenu_level2;
              for (let k = 0; k < this.leftpanelsubchilddata.length; k++) {
                ROUTES.forEach((x, ind) => {
                  x.submenu.forEach((y) => {
                    if (
                      y.child_code == this.leftpanelsubchilddata[k].child_code
                    ) {
                      x.class = 'menu-toggle';
                      y.submenu.push({
                        path:
                          this.leftpanelsubchilddata[k].module_path +
                          '/' +
                          this.leftpanelsubchilddata[k].form_link,
                        title: this.leftpanelsubchilddata[k].form_name,
                        moduleName: this.leftpanelsubchilddata[k].form_link,
                        parent_code: this.leftpanelsubchilddata[k].parent_code,
                        child_code: this.leftpanelsubchilddata[k].child_code,
                        icon: this.leftpanelsubchilddata[k].icon_class,
                        class: 'ml-menu2',
                        groupTitle: false,
                        sequenceId: this.leftpanelsubchilddata[k].sequence_id,
                        submenu: [],
                        groupTitleName: this.leftpanelsubchilddata[k],
                      });
                    }
                  });
                });
              }
            }
          }
          for (let i = 0; i < ROUTES.length; i++) {
            ROUTES[i].submenu.sort((a: any, b: any) =>
              a['sequenceId'] > b['sequenceId']
                ? 1
                : a['sequenceId'] === b['sequenceId']
                ? 0
                : -1
            );
          }
          ROUTES.sort((a: any, b: any) =>
            a['sequenceId'] > b['sequenceId']
              ? 1
              : a['sequenceId'] === b['sequenceId']
              ? 0
              : -1
          );
          this.sidebarItems = ROUTES.filter((sidebarItem) => sidebarItem);
        });
    }
    this.initLeftSidebar();
    this.bodyTag = this.document.body;
  }
  ngOnDestroy() {
    this.routerObj.unsubscribe();
  }
  initLeftSidebar() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this;
    // Set menu height
    _this.setMenuHeight();
    _this.checkStatuForResize(true);
  }
  setMenuHeight() {
    this.innerHeight = window.innerHeight;
    const height = this.innerHeight - this.headerHeight;
    this.listMaxHeight = height + '';
    this.listMaxWidth = '500px';
  }
  isOpen() {
    return this.bodyTag.classList.contains('overlay-open');
  }
  checkStatuForResize(firstTime: boolean) {
    if (window.innerWidth < 1170) {
      this.renderer.addClass(this.document.body, 'ls-closed');
    } else {
      this.renderer.removeClass(this.document.body, 'ls-closed');
    }
  }
  mouseHover() {
    const body = this.elementRef.nativeElement.closest('body');

    if (body.classList.contains('submenu-closed')) {
      this.renderer.addClass(this.document.body, 'side-closed-hover');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    }
  }
  mouseOut() {
    const body = this.elementRef.nativeElement.closest('body');

    if (body.classList.contains('side-closed-hover')) {
      this.renderer.removeClass(this.document.body, 'side-closed-hover');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }
}
