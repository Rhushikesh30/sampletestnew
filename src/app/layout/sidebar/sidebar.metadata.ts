// Sidebar route metadata
export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  groupTitle: boolean;
  moduleName: string;
  sequenceId: any;
  parent_code: string;
  child_code: string;
  submenu: RouteInfo[];
  groupTitleName: string;
}
