import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FpcSetupRoutingModule } from './fpc-setup-routing.module';
import { FpcOnboardingDetailsComponent } from './fpc-onboarding/fpc-onboarding-details/fpc-onboarding-details.component';
import { FpcOnboardingComponent } from './fpc-onboarding/fpc-onboarding.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ComponentsModule } from '../shared/components/components.module';
import { MatSelectModule } from '@angular/material/select';
import {CdkAccordionModule} from '@angular/cdk/accordion';


@NgModule({
  declarations: [
    FpcOnboardingComponent,
    FpcOnboardingDetailsComponent
  ],
  imports: [
    CommonModule,
    FpcSetupRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatCheckboxModule,
    ComponentsModule,
    MatSelectModule,
    CdkAccordionModule
  ]
})
export class FpcSetupModule { }
