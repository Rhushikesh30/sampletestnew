import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FpcOnboardingComponent } from './fpc-onboarding/fpc-onboarding.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'fpc',
    pathMatch: 'full'
  },
  {
    path: 'fpc',
    component: FpcOnboardingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FpcSetupRoutingModule { }
