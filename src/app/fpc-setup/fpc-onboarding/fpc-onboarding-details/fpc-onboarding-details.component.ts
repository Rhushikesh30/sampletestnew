import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
  FormGroup,
} from '@angular/forms';

import { DatePipe } from '@angular/common';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-fpc-onboarding-details',
  templateUrl: './fpc-onboarding-details.component.html',
  styleUrls: ['./fpc-onboarding-details.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class FpcOnboardingDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;

  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  tenantForm!: UntypedFormGroup;

  tenantsrtucture: any = [];
  tenantHierarchyData: any[] = [];
  ownershipStatusData: any[] = [];
  KYCTypeData: any[] = [];
  countrydata: any[] = [];
  talukaData: any[] = [];
  stateData: any[] = [];
  districtData: any[] = [];
  fileKYC!: FormData;
  fileNameKYC = '';
  uploadStatus = '';
  previewImage: any;
  previewImageType = false;
  showFileLoader = false;
  showFileLoader1 = false;
  showReset = true;
  cancelFlag = true;
  btnVal = 'Submit';

  uploadStatus1 = '';

  previewImage1: any;
  previewImage2: any;
  previewImageType1: any;
  previewImageType2: any;
  fileName = '';
  fileName1 = '';
  fileKYC1!: FormData;

  transactionData: { id: number; master_key: string }[] = [];
  documentData: { id: number; master_key: string }[] = [];

  empuserData: { id: number; name: string }[] = [];
  departmentdata: { id: number; name: string }[] = [];
  subdepartmentdata: { id: number; name: string }[] = [];
  designationdata: { id: number; name: string }[] = [];
  tenantData: { id: number; name: string }[] = [];
  result: any = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private fpcSetupService: FpcSetupService,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private dynamicFormService: DynamicFormService,
    private encDecService: EncrDecrService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    this.getAllData();

    this.fpcSetupService.getAllMasterData('Transaction Type').subscribe({
      next: (data: any) => {
        this.transactionData = data;
      },
    });

    this.tenantForm.get('country')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.countrySelected(val);
        } else {
          this.stateData = [];
        }
      },
      error: (e) => {
        this.stateData = [];
      },
    });

    this.tenantForm.get('state')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.stateSelected(val);
        } else {
          this.districtData = [];
        }
      },
      error: (e) => {
        this.districtData = [];
      },
    });

    this.tenantForm.get('district')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.districtSelected(val);
        } else {
          this.talukaData = [];
        }
      },
      error: (e) => {
        this.talukaData = [];
      },
    });

    this.dynamicFormService.getDynamicScreenData('Department').subscribe({
      next: (data: any) => {
        this.departmentdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dynamicFormService.getDynamicScreenData('Sub Department').subscribe({
      next: (data: any) => {
        this.subdepartmentdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dynamicFormService.getDynamicScreenData('Designation').subscribe({
      next: (data: any) => {
        this.designationdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    }
  }

  initializeForm() {
    this.fpcSetupService.getAllMasterData('Tenant Type').subscribe({
      next: (data: any) => {
        this.tenantData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.tenantForm = this.formBuilder.group({
      id: [''],
      tenant_type_id: [3],
      tenant_parent_id: localStorage.getItem('COMPANY_ID'),
      tenant_role: ['FPC Admin'],
      tenant_name: [
        '',
        [
          Validators.required,
          Validators.maxLength(255),
          Validators.pattern('^[a-zA-Z0-9_ -]*$'),
        ],
      ],
      tenant_short_name: [
        '',
        [Validators.maxLength(30), Validators.pattern('^[a-zA-Z0-9_ ]*$')],
      ],
      phone_no: [
        '',
        [Validators.maxLength(10), Validators.pattern('^[0-9_]*$')],
      ],
      email_id: ['', [Validators.required, Validators.email]],
      cin_no: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '[L,U]{1}[0-9]{5}[A-Z]{2}[1-9]{1}[0-9]{3}[A-Z]{3}[0-9]{6}'
          ),
        ],
      ],
      pan_no: [
        '',
        [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')],
      ],
      tan_no: ['', [Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      gst_no: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}'
          ),
        ],
      ],
      address: ['', [Validators.required]],
      address1: [''],
      pin_code: [
        '',
        [
          Validators.required,
          Validators.maxLength(6),
          Validators.pattern('^[0-9_]*$'),
        ],
      ],
      contact_person_name: ['', [Validators.required]],
      ownership_status: [''],
      status: [''],
      country: ['', [Validators.required]],
      state: ['', [Validators.required]],
      district: ['', [Validators.required]],
      taluka: [''],
      is_active: [true],
      is_deleted: [false],
      contract_details: this.formBuilder.array([this.contract_details()]),
      contact_details: this.formBuilder.array([this.contact_details()]),
      kyc_details: this.formBuilder.array([this.kyc_details()]),
      header_image_details: [''],
      header_image_type: [''],
      header_text_details: [''],
      header_image_position: [''],
      header_text_position: [''],
      footer_image_details: [''],
      footer_image_type: [''],
      footer_text_details: [''],
      footer_image_position: [''],
      footer_text_position: [''],
    });
  }

  setAttachment(event: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';

    let fileToUpload = event.target.files[0];

    var reader = new FileReader();
    reader.readAsDataURL(fileToUpload);
    reader.onload = (_event) => {
      this.previewImage1 = reader.result;
    };
    let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
    if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
      this.previewImageType1 = true;
    }
    this.fileName = fileToUpload['name'];
    let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

    if (compare_file_type.includes(fileFormat)) {
      let formData1 = new FormData();
      formData1.append('uploadedFile', fileToUpload);
      formData1.append('folder_name', 'fpcuploads');
      this.fileKYC1 = formData1;
      this.showFileLoader = true;
      this.uploadStatus = '';
      this.fpcSetupService.saveFile(this.fileKYC1).subscribe({
        next: (data: any) => {
          if (data['message'] == 'File Size Not More Than 5 MB') {
            this.showSwalmessage(
              'File Size Not More Than 5 MB',
              '',
              'error',
              false
            );
          } else {
            this.showFileLoader = false;
            let files3path = data['s3_file_path'];

            this.tenantForm.get('header_image_details')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          }
        },
        error: (e) => {
          this.uploadStatus = '';
          this.fileName = '';
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
    }
  }

  setAttachment1(event: any) {
    this.previewImageType2 = false;
    this.previewImage2 = '';

    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage2 = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType2 = true;
      }
      this.fileName1 = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        let formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileKYC1 = formData1;
        this.showFileLoader1 = true;
        this.uploadStatus1 = '';
        this.fpcSetupService.saveFile(this.fileKYC1).subscribe({
          next: (data: any) => {
            this.showFileLoader1 = false;
            let files3path = data['s3_file_path'];

            this.tenantForm.get('footer_image_details')?.setValue(files3path);
            this.uploadStatus1 = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e) => {
            this.uploadStatus1 = '';
            this.fileName1 = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus1 = '';
        this.fileName1 = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus1 = '';
      this.fileName1 = '';
      this.showSwalmessage('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  removeImage() {
    this.previewImage1 = '';
  }
  removeImage1() {
    this.previewImage2 = '';
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.tenantForm.reset();
      this.handleCancel.emit(false);
    }
  }

  getAllData() {
    this.fpcSetupService.getAllMasterData('Ownership Type').subscribe({
      next: (data: any) => {
        this.ownershipStatusData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcSetupService.getAllMasterData('Document Type').subscribe({
      next: (data: any) => {
        this.KYCTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcSetupService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.countrydata = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  viewRecord(edata: any) {
    if (edata.header_href_attachment_path != '') {
      this.previewImage1 = this.encDecService.decryptedData(
        edata.header_href_attachment_path
      );
    }
    if (edata.footer_href_attachment_path != '') {
      this.previewImage2 = this.encDecService.decryptedData(
        edata.footer_href_attachment_path
      );
    }

    this.tenantForm.patchValue({
      id: edata.id,
      tenant_name: edata.tenant_name,
      tenant_short_name: edata.tenant_short_name,
      ownership_status: edata.ownership_status,
      email_id: edata.email_id,
      contact_person_name: edata.contact_person_name,
      phone_no: edata.phone_no,
      pan_no: edata.pan_no,
      tan_no: edata.tan_no,
      cin_no: edata.cin_no,
      gst_no: edata.gst_no,
      address: edata.address,
      address1: edata.address1,
      pin_code: edata.pin_code,
      country: edata.country,
      state: edata.state,
      district: edata.district,
      taluka: edata.taluka,
      is_active: edata.is_active,
      is_deleted: edata.is_deleted,
      header_image_details: edata.header_image_details,
      header_text_details: edata.header_text_details,
      header_image_type: edata.header_image_type,
      header_image_position: edata.header_image_position,
      header_text_position: edata.header_text_position,
      footerr_image_details: edata.footer_image_details,
      footer_text_details: edata.footer_text_details,
      footer_image_type: edata.footer_image_type,
      footer_image_position: edata.footer_image_position,
      footer_text_position: edata.footer_text_position, //ch
    });
    let kycItemRow = edata.kyc_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (kycItemRow.length >= 1) {
      this.tenantForm.setControl(
        'kyc_details',
        this.setExistingArray3(kycItemRow)
      );
    }

    let contractItemRow = edata.contract_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (contractItemRow.length >= 1) {
      this.tenantForm.setControl(
        'contract_details',
        this.setExistingArray1(contractItemRow)
      );
    }

    let contactItemRow = edata.contact_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (contactItemRow.length >= 1) {
      this.tenantForm.setControl(
        'contact_details',
        this.setExistingArray2(contactItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.tenantForm.disable();
    }
  }

  countrySelected(val: any) {
    if (val) {
      this.fpcSetupService.getStatesData(val).subscribe({
        next: (data: any) => {
          this.stateData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }

  stateSelected(val: any) {
    if (val) {
      this.fpcSetupService.getDistrictData(val).subscribe({
        next: (data: any) => {
          this.districtData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }

  districtSelected(val: any) {
    if (val) {
      this.fpcSetupService.getTalukaData(val).subscribe({
        next: (data: any) => {
          this.talukaData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }

  setFile(event: any, i: any) {
    this.previewImageType = false;
    this.previewImage = '';
    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType = true;
      }
      this.fileNameKYC = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];
      if (compare_file_type.includes(fileFormat)) {
        let formData = new FormData();
        formData.append('uploadedFile', fileToUpload);
        formData.append('folder_name', 'kycuploads');
        this.fileKYC = formData;
        this.showFileLoader = true;
        this.uploadStatus = '';
        this.fpcSetupService.saveFile(this.fileKYC).subscribe({
          next: (data: any) => {
            let files3path = data['s3_file_path'];

            this.formArrkyc.at(i).get('document_path')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
            this.showFileLoader = false;
          },
          error: (e) => {
            this.uploadStatus = '';
            this.fileNameKYC = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileNameKYC = '';
        this.fileInputVar.nativeElement.value = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      this.showSwalmessage('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  onSubmit() {
    if (this.tenantForm.invalid) {
      this.showSwalMassage('Please Fill All Required Fields', 'error');
    } else {
      for (
        var i = 0;
        i < (<FormArray>this.tenantForm.get('contract_details')).length;
        i++
      ) {
        let gridrow1 = (<FormArray>this.tenantForm.get('contract_details')).at(
          i
        );
        let fromdate = gridrow1.get('from_date')?.value;
        let todate = gridrow1.get('to_date')?.value;
        let fromdate1 = this.datepipe.transform(fromdate, 'yyyy-MM-dd');
        let todate1 = this.datepipe.transform(todate, 'yyyy-MM-dd');
        gridrow1.get('from_date')?.setValue(fromdate1);
        gridrow1.get('to_date')?.setValue(todate1);
      }

      this.handleSave.emit(this.tenantForm.value);
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.tenantForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  contract_details() {
    return this.formBuilder.group({
      id: [''],
      from_date: [''],
      to_date: [''],
      revision_status: [''],
      max_no_of_users: [100],
      concurent_user: [10],
      api_limit: [10],
      api_calls_blocked_for_in_minutes: [3600],
      application_blocking_in_minutes: [3600],
    });
  }

  setExistingArray1(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          from_date: element.from_date,
          to_date: element.to_date,
          revision_status: element.revision_status,
          max_no_of_users: element.max_no_of_users,
          concurent_user: element.concurent_user,
          api_limit: element.api_limit,
          api_calls_blocked_for_in_minutes:
            element.api_calls_blocked_for_in_minutes,
          application_blocking_in_minutes:
            element.application_blocking_in_minutes,
        })
      );
    });
    return formArray;
  }

  get formArrcontract() {
    return this.tenantForm.get('contract_details') as UntypedFormArray;
  }

  addNewRow1() {
    this.formArrcontract.push(this.contract_details());
  }

  deleteRow1(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontract.removeAt(index);
      return true;
    }
  }
  contact_details() {
    return this.formBuilder.group({
      id: [''],
      full_name: [''],
      phone_no: [
        '',
        [Validators.maxLength(10), Validators.pattern('^[0-9_]*$')],
      ],
      email: ['', [Validators.email]],
      department: [''],
      sub_department: [''],
      designation: [''],
    });
  }
  setExistingArray2(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          full_name: element.full_name,
          phone_no: element.phone_no,
          email: element.email,
          department: element.department,
          sub_department: element.sub_department,
          designation: element.designation,
        })
      );
    });
    return formArray;
  }

  get formArrcontact() {
    return this.tenantForm.get('contact_details') as UntypedFormArray;
  }

  addNewRow2() {
    this.formArrcontact.push(this.contact_details());
  }

  deleteRow2(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontact.removeAt(index);
      return true;
    }
  }

  kyc_details() {
    return this.formBuilder.group({
      id: [''],
      document_name: ['', [Validators.required]],
      document_seq_number: [
        '',
        [Validators.required, Validators.pattern('^[a-zA-Z0-9_ -]*$')],
      ],
      document_path: ['', [Validators.required]],
      href_attachment_path: [''],
    });
  }

  setExistingArray3(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          document_name: element.document_name,
          document_seq_number: element.document_seq_number,
          document_path: element.document_path,
          href_attachment_path: this.encDecService.decryptedData(
            element.href_attachment_path
          ),
        })
      );
    });
    return formArray;
  }

  get formArrkyc() {
    return this.tenantForm.get('kyc_details') as UntypedFormArray;
  }

  addNewRow3(i: any) {
    this.formArrkyc.push(this.kyc_details());
  }

  deleteRow3(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrkyc.removeAt(index);
      return true;
    }
  }

  changeStatus(event: any) {
    if (event.checked) {
      this.tenantForm.get('is_deleted')?.setValue(false);
    } else {
      this.tenantForm.get('is_deleted')?.setValue(true);
    }
  }

  header_image_type(val: any) {
    if (val == 'logo') {
      this.tenantForm.controls['header_text_details'].enable();
      this.tenantForm.controls['header_image_position'].enable();
      this.tenantForm.controls['header_text_position'].enable();
    } else if (val == 'full_image') {
      this.tenantForm.get('header_image_position')?.setValue('');
      this.tenantForm.get('header_text_position')?.setValue('');
      this.tenantForm.get('header_text_details')?.setValue('');
      this.tenantForm.controls['header_text_details'].disable();
      this.tenantForm.controls['header_image_position'].disable();
      this.tenantForm.controls['header_text_position'].disable();
    }
  }
  footer_image_type(val: any) {
    if (val == 'logo') {
      this.tenantForm.controls['footer_image_position'].enable();
      this.tenantForm.controls['footer_text_position'].enable();
      this.tenantForm.controls['footer_text_details'].enable();
    } else if (val == 'full_image') {
      this.tenantForm.get('footer_image_position')?.setValue('');
      this.tenantForm.get('footer_text_position')?.setValue('');
      this.tenantForm.get('footer_text_details')?.setValue('');
      this.tenantForm.controls['footer_image_position'].disable();
      this.tenantForm.controls['footer_text_position'].disable();
      this.tenantForm.controls['footer_text_details'].disable();
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
