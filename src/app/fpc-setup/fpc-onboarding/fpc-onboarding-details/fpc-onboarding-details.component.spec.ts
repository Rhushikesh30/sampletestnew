import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FpcOnboardingDetailsComponent } from './fpc-onboarding-details.component';

describe('FpcOnboardingDetailsComponent', () => {
  let component: FpcOnboardingDetailsComponent;
  let fixture: ComponentFixture<FpcOnboardingDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FpcOnboardingDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FpcOnboardingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
