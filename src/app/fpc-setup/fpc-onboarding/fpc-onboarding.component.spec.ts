import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FpcOnboardingComponent } from './fpc-onboarding.component';

describe('FpcOnboardingComponent', () => {
  let component: FpcOnboardingComponent;
  let fixture: ComponentFixture<FpcOnboardingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FpcOnboardingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FpcOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
