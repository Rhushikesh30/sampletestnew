import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';

import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
@Component({
  selector: 'app-fpc-onboarding',
  templateUrl: './fpc-onboarding.component.html',
  styleUrls: ['./fpc-onboarding.component.scss'],
  providers: [ErrorCodes],
})
export class FpcOnboardingComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  resultData!: [];
  displayedColumns = [
    'view',
    'edit',
    'tenant_name',
    'district_name',
    'taluka_name',
    'phone_no',
    'active',
  ];
  renderedData: any = [];
  screenName = 'FPC';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;
  dataSource!: ExampleDataSource;

  exampleDatabase?: FpcSetupService;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private roleSecurityService: RoleSecurityService,
    private fpcSetupService: FpcSetupService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'FPC Company')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.fpcSetupService.getTenantById(row.id).subscribe({
      next: (data: any) => {
        this.rowData = data[0];
        this.showList = false;
        this.submitBtn = flag;
        this.listDiv = true;
        this.showLoader = false;
      },
    });
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;

    for (var i = 0; i < formValue.kyc_details.length; i++) {
      if (formValue.kyc_details[i].id == '') {
        delete formValue.kyc_details[i].id;
      }
      if (
        formValue.kyc_details[i].attachment_path == '' &&
        formValue.kyc_details[i].kyc_doc_id == ''
      ) {
        delete formValue.kyc_details[i];
      }
    }
    for (var i = 0; i < formValue.contact_details.length; i++) {
      if (formValue.contact_details[i].id == '') {
        delete formValue.contact_details[i].id;
      }
      if (
        formValue.contact_details[i].full_name == '' ||
        formValue.contact_details[i].full_name == null
      ) {
        formValue.contact_details = [];
      }
    }
    for (var i = 0; i < formValue.contract_details.length; i++) {
      if (formValue.contract_details[i].id == '') {
        delete formValue.contract_details[i].id;
      }
      if (
        formValue.contract_details[i].from_date == '' ||
        formValue.contract_details[i].from_date == null
      ) {
        formValue.contract_details = [];
      }
    }
    let json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.fpcSetupService.createTenantfpc(json_data, formValue.id).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 2) {
          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e) => {
        this.showLoader = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }
  refresh() {
    this.exampleDatabase = new FpcSetupService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'FPC Name': x.tenant_name,
        'District Name': x.district_name,
        'Taluka Name': x.taluka_name,
        'Phone No': x.phone_no,
        Status: x.active,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: FpcSetupService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];

    this.exampleDatabase.getAllAdvanceTables();

    return merge(...displayDataChanges).pipe(
      map(() => {
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['active'] = 'Inactive';
          }
        }
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.tenant_name +
              advanceTable.gst_no +
              advanceTable.pan_no +
              advanceTable.transaction_type_name +
              advanceTable.country_name +
              advanceTable.state_name +
              advanceTable.active
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {}

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'District Name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'Taluka Name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'transaction_type_name':
          [propertyA, propertyB] = [
            a.transaction_type_name,
            b.transaction_type_name,
          ];
          break;
        case 'Phone No':
          [propertyA, propertyB] = [a.phone_no, b.phone_no];
          break;
        case 'Status':
          [propertyA, propertyB] = [a.active, b.active];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
