export const STATUS = {
  REJECTED: 'rejected',
  APPROVED: 'approved',
  PENDING: 'pending',
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  ACCEPTED: 'accepted',
  PENDING_FOR_APPROVAL: 'pending for approval',
  OPEN: 'open',
  CLOSED: 'closed',
};

export const getStatusColor = (status: string): string => {
  switch (status.toLowerCase()) {
    case STATUS.ACCEPTED:
      return '#5CBE63';
    case STATUS.ACTIVE:
      return '#5CBE63';
    case STATUS.INACTIVE:
      return '#E35140';
    case STATUS.REJECTED:
      return '#E35140';
    case STATUS.OPEN:
      return '#E35140';
    case STATUS.CLOSED:
      return '#5CBE63';
    case STATUS.PENDING_FOR_APPROVAL:
      return '#FFE900';
    case STATUS.PENDING:
      return '#FFE900';
    default:
      return 'gray';
  }
};
