import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FederationComponent } from './federation/federation.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'federation',
    pathMatch: 'full'
  },
  
  {
    path: 'federation',
    component: FederationComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FederationSetupRoutingModule { }
