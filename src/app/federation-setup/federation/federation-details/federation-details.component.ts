import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UntypedFormArray } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FederationService } from 'src/app/shared/services/federation.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-federation-details',
  templateUrl: './federation-details.component.html',
  styleUrls: ['./federation-details.component.scss'],
  providers: [ErrorCodes],
})
export class FederationDetailsComponent implements OnInit {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData = [];
  @Input() submitBtn!: boolean;
  @Input() screenName: string = 'Federation';
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  federationForm!: FormGroup;
  btnVal = 'Submit';
  fileKYC!: FormData;
  fileNameKYC = '';
  uploadStatus = '';
  previewImage: any;
  previewImageType = false;
  showFileLoader = false;

  ownershipStatusData: { id: number; master_key: string }[] = [];
  transactionData: { id: number; master_key: string }[] = [];
  documentData: { id: number; master_key: string }[] = [];
  countrydata: { id: number; country_name: string }[] = [];
  stateData: { id: number; state_name: string }[] = [];
  districtData: { id: number; district_name: string }[] = [];
  talukaData: { id: number; sub_district_name: string }[] = [];
  empuserData: { id: number; name: string }[] = [];
  departmentdata: { id: number; name: string }[] = [];
  subdepartmentdata: { id: number; name: string }[] = [];
  designationdata: { id: number; name: string }[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private federationService: FederationService,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit(): void {
    this.federationForm = this.formBuilder.group({
      tenant_name: [
        '',
        [
          Validators.required,
          Validators.maxLength(255),
          Validators.pattern('^(?!\\s)[a-zA-Z0-9_ -]*$'),
        ],
      ],
      tenant_short_name: [
        '',
        [
          Validators.maxLength(30),
          Validators.pattern('^(?!\\s)[a-zA-Z0-9_ ]*$'),
        ],
      ],
      ownership_status: [''],
      email_id: ['', [Validators.required, Validators.email]],
      contact_person_name: [
        '',
        [Validators.required, Validators.pattern('^(?!\\s)[a-zA-Z_ ]*$')],
      ],
      phone_no: [
        '',
        [Validators.maxLength(10), Validators.pattern('^[0-9_]*$')],
      ],
      transaction_type: [''],
      country: [''],
      state: [''],
      district: [''],
      taluka: [''],
      address: ['', [Validators.required]],
      address1: [''],
      pin_code: [
        '',
        [
          Validators.required,
          Validators.maxLength(6),
          Validators.pattern('^[0-9_]*$'),
        ],
      ],
      pan_no: ['', [Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')]],
      tan_no: ['', [Validators.pattern('[A-Z]{4}[0-9]{5}[A-Z]{1}')]],
      gst_no: [
        '',
        [
          Validators.pattern(
            '[0-3]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9]{1}Z[0-9A-Z]{1}'
          ),
        ],
      ],
      cin_no: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '[L,U]{1}[0-9]{5}[A-Z]{2}[1-9]{1}[0-9]{3}[A-Z]{3}[0-9]{6}'
          ),
        ],
      ],
      tenant_logo_path: [''],
      header_section: [''],
      footer_section: [''],
      import_code: [''],
      export_code: [''],
      contract_details: this.formBuilder.array([this.contract_details()]),
      contact_details: this.formBuilder.array([this.contact_details()]),
      kyc_details: this.formBuilder.array([this.kyc_details()]),
    });

    this.federationService.getAllMasterData('Ownership Type').subscribe({
      next: (data: any) => {
        this.ownershipStatusData = data;
      },
    });

    this.federationService.getAllMasterData('Transaction Type').subscribe({
      next: (data: any) => {
        this.transactionData = data;
      },
    });

    this.federationService.getAllMasterData('Document Type').subscribe({
      next: (data: any) => {
        this.documentData = data;
      },
    });

    this.federationService.getDynamicScreenData('Department').subscribe({
      next: (data: any) => {
        this.departmentdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.federationService.getDynamicScreenData('Sub Department').subscribe({
      next: (data: any) => {
        this.subdepartmentdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.federationService.getDynamicScreenData('Designation').subscribe({
      next: (data: any) => {
        this.designationdata = data.screenmatlistingdata_set;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.federationService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.countrydata = data;
      },
    });

    this.federationForm.get('country')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.countrySelected(val);
        } else {
          this.stateData = [];
        }
      },
      error: (e) => {
        this.stateData = [];
      },
    });

    this.federationForm.get('state')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.stateSelected(val);
        } else {
          this.districtData = [];
        }
      },
      error: (e) => {
        this.districtData = [];
      },
    });

    this.federationForm.get('district')?.valueChanges.subscribe({
      next: (val: any) => {
        if (val) {
          this.districtSelected(val);
        } else {
          this.talukaData = [];
        }
      },
      error: (e) => {
        this.talukaData = [];
      },
    });
  }

  countrySelected(val: any) {
    if (val) {
      this.federationService.getStatesData(val).subscribe({
        next: (data: any) => {
          this.stateData = data;
        },
      });
    }
  }

  stateSelected(val: any) {
    if (val) {
      this.federationService.getDistrictData(val).subscribe({
        next: (data: any) => {
          this.districtData = data;
        },
      });
    }
  }

  districtSelected(val: any) {
    if (val) {
      this.federationService.getTalukaData(val).subscribe({
        next: (data: any) => {
          this.talukaData = data;
        },
      });
    }
  }

  getemployeeuser(val: any, i: any) {
    var gridRow = (<FormArray>this.federationForm.get('contact_details')).at(i);

    let employee_user = gridRow.get('emp_user')?.value;

    this.federationService.getAllempusrData(employee_user).subscribe({
      next: (data: any) => {
        this.empuserData = data;
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  viewRecord(edata: any) {
    this.federationForm.patchValue({
      id: edata.id,
      tenant_name: edata.tenant_name,
      tenant_short_name: edata.tenant_short_name,
      ownership_status: edata.ownership_status,
      email_id: edata.email_id,
      contact_person_name: edata.contact_person_name,
      phone_no: edata.phone_no,
      transaction_type: edata.transaction_type,
      pan_no: edata.pan_no,
      tan_no: edata.tan_no,
      cin_no: edata.cin_no,
      gst_no: edata.gst_no,
      address: edata.address,
      address1: edata.address1,
      pin_code: edata.pin_code,
      country: edata.country,
      state: edata.state,
      district: edata.district,
      taluka: edata.taluka,
      tenant_logo_path: edata.tenant_logo_path,
      header_section: edata.header_section,
      footer_section: edata.footer_section,
      import_code: edata.import_code,
      export_code: edata.export_code,
    });
    let kycItemRow = edata.kyc_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (kycItemRow.length >= 1) {
      this.federationForm.setControl(
        'kyc_details',
        this.setExistingArray1(kycItemRow)
      );
    }

    let contractItemRow = edata.contract_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (contractItemRow.length >= 1) {
      this.federationForm.setControl(
        'contract_details',
        this.setExistingArray2(contractItemRow)
      );
    }

    let contactItemRow = edata.contact_details.filter(function (data: any) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (contactItemRow.length >= 1) {
      this.federationForm.setControl(
        'contact_details',
        this.setExistingArray3(contactItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.federationForm.disable();
    }
  }

  contract_details() {
    return this.formBuilder.group({
      id: [''],
      from_date: [''],
      to_date: [''],
      revision_status: [''],
      max_no_of_users: [''],
      concurent_user: [''],
      api_limit: [''],
      api_calls_blocked_for_in_minutes: [''],
      application_blocking_in_minutes: [''],
    });
  }

  setExistingArray1(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          from_date: element.from_date,
          to_date: element.to_date,
          revision_status: element.revision_status,
          max_no_of_users: element.max_no_of_users,
          concurent_user: element.concurent_user,
          api_limit: element.api_limit,
          api_calls_blocked_for_in_minutes:
            element.api_calls_blocked_for_in_minutes,
          application_blocking_in_minutes:
            element.application_blocking_in_minutes,
        })
      );
    });
    return formArray;
  }

  get formArrcontract() {
    return this.federationForm.get('contract_details') as UntypedFormArray;
  }

  addNewRow1() {
    this.formArrcontract.push(this.contract_details());
  }

  deleteRow1(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontract.removeAt(index);
      return true;
    }
  }

  contact_details() {
    return this.formBuilder.group({
      id: [''],
      emp_user: [''],
      employee_id: [''],
      full_name: [''],
      phone_no: [
        '',
        [Validators.maxLength(10), Validators.pattern('^[0-9_]*$')],
      ],
      email: ['', [Validators.required, Validators.email]],
      department: [''],
      sub_department: [''],
      designation: [''],
    });
  }

  setExistingArray2(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          emp_user: element.emp_user,
          employee_id: element.employee_id,
          full_name: element.full_name,
          phone_no: element.phone_no,
          email: element.email,
          department: element.department,
          sub_department: element.sub_department,
          designation: element.designation,
        })
      );
    });
    return formArray;
  }

  get formArrcontact() {
    return this.federationForm.get('contact_details') as UntypedFormArray;
  }

  addNewRow2() {
    this.formArrcontact.push(this.contact_details());
  }

  deleteRow2(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontact.removeAt(index);
      return true;
    }
  }

  kyc_details() {
    return this.formBuilder.group({
      id: [''],
      document_name: [''],
      document_seq_number: [''],
      document_path: [''],
      href_attachment_path: [''],
    });
  }

  setExistingArray3(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          document_name: element.document_name,
          document_seq_number: element.document_seq_number,
          document_path: element.document_path,
          href_attachment_path: atob(element.href_attachment_path),
        })
      );
    });
    return formArray;
  }

  get formArrkyc() {
    return this.federationForm.get('kyc_details') as UntypedFormArray;
  }

  addNewRow3() {
    this.formArrkyc.push(this.kyc_details());
  }

  deleteRow3(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrkyc.removeAt(index);
      return true;
    }
  }

  onSubmit() {
    if (this.federationForm.invalid) {
      return;
    } else {
      this.handleSave.emit(this.federationForm.value);
    }
  }

  onCancelForm() {
    this.federationForm.reset();
    this.handleCancel.emit(false);
    this.btnVal = 'Submit';
  }

  setFile(event: any, i: any) {
    this.previewImageType = false;
    this.previewImage = '';
    let fileToUpload = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(fileToUpload);
    reader.onload = (_event) => {
      this.previewImage = reader.result;
    };
    let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
    if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
      this.previewImageType = true;
    }
    this.fileNameKYC = fileToUpload['name'];
    let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];
    if (compare_file_type.includes(fileFormat)) {
      let formData = new FormData();
      formData.append('uploadedFile', fileToUpload);
      formData.append('folder_name', 'kycuploads');
      this.fileKYC = formData;
      this.showFileLoader = true;
      this.uploadStatus = '';
      this.federationService.saveFile(this.fileKYC).subscribe({
        next: (data: any) => {
          if (data['message'] == 'File Size Not More Than 5 MB') {
            this.showSwalmessage(
              'File Size Not More Than 5 MB',
              '',
              'error',
              false
            );
          } else {
            let files3path = data['s3_file_path'];
            this.formArrkyc.at(i).get('document_path')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
            this.showFileLoader = false;
          }
        },
        error: (e) => {
          this.uploadStatus = '';
          this.fileNameKYC = '';
        },
      });
    } else {
      this.uploadStatus = '';
      this.fileNameKYC = '';
      this.fileInputVar.nativeElement.value = '';
      Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
