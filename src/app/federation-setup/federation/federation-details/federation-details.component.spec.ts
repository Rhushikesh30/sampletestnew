import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FederationDetailsComponent } from './federation-details.component';

describe('FederationDetailsComponent', () => {
  let component: FederationDetailsComponent;
  let fixture: ComponentFixture<FederationDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FederationDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FederationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
