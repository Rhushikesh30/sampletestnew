import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ComponentsModule } from '../shared/components/components.module';

import { FederationSetupRoutingModule } from './federation-setup-routing.module';
import { FederationComponent } from './federation/federation.component';
import { FederationDetailsComponent } from './federation/federation-details/federation-details.component';

import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    FederationComponent,
    FederationDetailsComponent
  ],
  imports: [
    CommonModule,
    FederationSetupRoutingModule,   
    ComponentsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    SharedModule
  ]
})
export class FederationSetupModule { }
