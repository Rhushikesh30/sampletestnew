import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountingParameterComponent } from './accounting-parameter/accounting-parameter.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AccountingParameterComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingParameterRoutingModule { }
