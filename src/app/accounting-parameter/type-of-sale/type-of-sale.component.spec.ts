import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeOfSaleComponent } from './type-of-sale.component';

describe('TypeOfSaleComponent', () => {
  let component: TypeOfSaleComponent;
  let fixture: ComponentFixture<TypeOfSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeOfSaleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TypeOfSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
