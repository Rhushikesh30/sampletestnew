import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ComponentsModule } from '../shared/components/components.module';
import { DynamicFormModule } from "../shared/dynamic-form/dynamic-form.module";
import { SharedModule } from '../shared/shared.module';

import { AccountingParameterRoutingModule } from './accounting-parameter-routing.module';
import { DivisionComponent } from './division/division.component';
import { SubDepartmentComponent } from './sub-department/sub-department.component';
import { TypeOfSaleComponent } from './type-of-sale/type-of-sale.component';
import { AccountingParameterComponent } from './accounting-parameter/accounting-parameter.component';
import { DepartmentComponent } from './department/department.component';
import { LocationComponent } from './location/location.component';


@NgModule({
  declarations: [
    DivisionComponent,
    SubDepartmentComponent,
    TypeOfSaleComponent,
    AccountingParameterComponent,
    DepartmentComponent,
    LocationComponent
  ],
  imports: [
    CommonModule,
    AccountingParameterRoutingModule,
    ComponentsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule
  ]
})
export class AccountingParameterModule { }
