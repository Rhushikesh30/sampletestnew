import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import {CdkAccordionModule} from '@angular/cdk/accordion';

import { InvoicePrintRoutingModule } from './invoice-print-routing.module';
import { PrintInvoiceComponent } from './print-invoice/print-invoice.component';


@NgModule({
  declarations: [
    PrintInvoiceComponent
  ],
  imports: [
    CommonModule,
    InvoicePrintRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,
  ]
})
export class InvoicePrintModule { }
