// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-debit-note-details',
//   templateUrl: './debit-note-details.component.html',
//   styleUrls: ['./debit-note-details.component.scss']
// })
// export class DebitNoteDetailsComponent {

// }

import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { DatePipe, formatDate } from '@angular/common';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { GrnService } from 'src/app/shared/services/grn.service';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import Swal from 'sweetalert2';
import { PaymentsService } from 'src/app/shared/services/payments.service';
import { PurchaseBookingService } from 'src/app/shared/services/purchase-booking.service';
import { DebitNoteService } from 'src/app/shared/services/debit-note.service';
import { SalesService } from 'src/app/shared/services/sales.service';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-debit-note-details',
  templateUrl: './debit-note-details.component.html',
  styleUrls: ['./debit-note-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class DebitNoteDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<Event | boolean>();

  debitNoteForm!: UntypedFormGroup;
  isColumnHidden = false;
  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  FederationView = false;
  btnVal = 'Submit';

  grn_details_data: any[] = [];
  SupplierTypeData: any[] = [];
  SupplierData: any[] = [];
  supplierRefId: any[] = [];
  poRefId: any[] = [];
  uomData: any[] = [];
  txnCurrency: any[] = [];
  itemRefId: any[] = [];
  poData: any[] = [];
  purchaseBookingData: any[] = [];
  fileNameKYC = '';
  fileNameKYC1 = '';
  uploadStatus = '';
  uploadStatus1 = '';
  previewImage: any;
  previewImage1: any;
  previewImageType: any;
  previewImageType1: any;
  fileName = '';
  fileName1 = '';
  fileKYC!: FormData;
  fileKYC1!: FormData;
  fileAttchatment!: FormData;
  fileAttchatment1!: FormData;
  dynamic: any;
  todayDate = new Date().toJSON().split('T')[0];

  currentDate = new Date().toJSON().split('T')[0];
  supplierExist = false;
  grnData: { id: number; name: string }[] = [];
  result: any = [];
  filterPurchaseOrderData: any = [];
  viewEdit = false;
  ItemType: any;
  ItemRateData: any = [];
  SearchDataSupplier: any[] = [];
  SearchData: any[] = [];
  itemDataId: any[] = [];
  POItemTypeData: any[] = [];

  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  @Input() screenName!: string;
  errorCodeData: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private grnService: GrnService,
    private paymentService: PaymentsService,
    private purchaseBookingService: PurchaseBookingService,
    private debitNoteService: DebitNoteService,
    private salesService: SalesService,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private dynamicFormService: DynamicFormService,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.getAllData();
    this.initializeForm();
  }
  async changeStatus(event: any) {
    const purchase_booking_mst_ref_id = this.debitNoteForm.get(
      'purchase_booking_ref_id'
    )?.value;
    if (!purchase_booking_mst_ref_id) {
      this.showSwalMassage('Please Select Purchase Booking', 'error');
      return;
    }
    // this.viewRecord(this.rowData);
    this.isColumnHidden = !this.isColumnHidden;

    this.debitNoteForm.get('is_with_qty')?.setValue(this.isColumnHidden);

    await this.getPurchaseBookingDataById(purchase_booking_mst_ref_id);
    // execute a for loop over the debitNoteForm and change grnDetails -> amount to disabled
    console.log(
      'this.debitNoteForm.controls["debit_note_details"]: ',
      this.debitNoteForm.controls['debit_note_details']
    );

    for (
      let i = 0;
      i < this.debitNoteForm.controls['debit_note_details'].value.length;
      i++
    ) {
      // this.debitNoteForm.controls['debit_note_details'].controls[i].get('with_quantity')?.setValue(event.value);
      const gridRow = (<FormArray>(
        this.debitNoteForm.get('debit_note_details')
      )).at(i);
      if (this.isColumnHidden) {
        gridRow.get('amount')?.disable();
      } else {
        gridRow.get('amount')?.enable();
      }
    }

    // if (this.isColumnHidden) {
    //   for (let i = 0; i < this.debitNoteForm.controls["debit_note_details"]; i++) {

    // }
    // }
  }

  initializeForm() {
    this.debitNoteForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', [Validators.required]],
      purchase_booking_ref_id: ['', [Validators.required]],
      supplier_ref_id: ['', [Validators.required]],
      purchase_booking_txn_amount: [''],
      transaction_currency: [''],
      is_active: [true],
      supplier_bill_no: [''],
      is_with_qty: [false],
      remark: [''],
      debit_note_details: this.formBuilder.array([this.debit_note_details()]),
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.debitNoteForm.reset();
      this.handleCancel.emit(false);
    }
  }

  getAllData() {
    this.grnService
      .getDynamicDataCombine(
        'get_combine_columns',
        'supplier',
        'company_name',
        'ref_type'
      )
      .subscribe({
        next: (data: any) => {
          this.SearchDataSupplier = data;
          this.SupplierData = data;
          this.grnService.getCurrencyData().subscribe({
            next: (data: any) => {
              this.txnCurrency = data;
              if (!Array.isArray(this.rowData)) {
                this.showReset = false;
                this.viewRecord(this.rowData);
              }
            },
          });
        },
      });

    this.debitNoteService.getDynamicData('item_master', 'name').subscribe({
      next: (data: any) => {
        this.itemRefId = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  viewRecord(edata: any) {
    console.log('edata: ', edata);
    this.viewEdit = true;

    const transaction_currency = this.txnCurrency.filter(
      (x: any) => x.id == edata.txn_currency
    );
    console.log('this.txnCurrency: ', this.txnCurrency);

    console.log('transaction_currency: ', transaction_currency);
    this.isColumnHidden = edata.is_debit_note_with_qty_amt;
    this.debitNoteForm.patchValue({
      id: edata.id,
      transaction_date: edata.transaction_date,
      purchase_booking_ref_id: edata.purchase_booking_mst_ref_id,
      supplier_ref_id: edata.supplier_ref_id,
      purchase_booking_txn_amount: edata.purchase_booking_txn_amount,
      transaction_currency: transaction_currency[0]?.currency_symbol,
      is_active: [true],
      supplier_bill_no: edata.supplier_bill_no,
      is_with_qty: edata.is_debit_note_with_qty_amt,
      remark: edata.remark,
    });

    this.supplierDataById(edata.supplier_ref_id);

    const gateDetailItemRow = edata.debit_details.filter(function (data: any) {
      return data;
    });
    console.log('gateDetailItemRow: ', gateDetailItemRow);

    if (gateDetailItemRow.length >= 1) {
      this.debitNoteForm.setControl(
        'debit_note_details',
        this.setExistingArray(gateDetailItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
      this.viewBtn = false;
    } else {
      this.debitNoteForm.disable();
      this.debitNoteForm.get('debit_note_details')?.value.disabled;
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onSubmit(btn: any) {
    this.showLoader = true;
    if (this.debitNoteForm.invalid) {
      this.showLoader = false;
      this.showSwalmessage(
        'Please Fill All Required Fields',
        '',
        'error',
        false
      );
    } else {
      const payload = this.debitNoteForm.getRawValue();
      const val = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );
      payload.transaction_date = val;

      const payload_data = {
        id: '',
        transaction_date: payload.transaction_date,
        supplier_ref_id: payload.supplier_ref_id,
        agent_ref_id: null,
        purchase_booking_mst_ref_id: payload.purchase_booking_ref_id, //PurchaseBooking
        txn_currency_purchase_booking_amount:
          payload.purchase_booking_txn_amount,
        is_debit_note_with_qty_amt: payload.is_with_qty,
        remark: payload.remark,
      };

      const gridRow1 = payload.debit_note_details;
      const debit_note_details = [];
      let debit_amount_sum = 0;
      for (const element of gridRow1) {
        console.log(gridRow1);
        if (element['rejected_qty'] > element['quantity']) {
          this.showSwalMassage(
            'Rejected Quantity cannot be greater than Quantity',
            'error'
          );
          return;
        }
        const detail_data = {
          id: '',
          purchase_booking_detail_ref_id:
            element['purchase_booking_detail_ref_id'], //PurchaseBookingDetails
          item_ref_id: element['item_ref_id'],
          is_gst_set_off: true, //
          rejected_qty: Number(element['rejected_qty'] || 0), // rejected_qty > 0 if is_debit_note_with_qty_amt = True
          rate: element['rate'], // rate > 0 if is_debit_note_with_qty_amt = True
          txn_currency_amount: element['amount'], // txn_currency_amount > 0 is_debit_note_with_qty_amt = false
        };
        debit_amount_sum += Number(element['amount']);
        debit_note_details.push(detail_data);
      }
      if (debit_amount_sum > payload.purchase_booking_txn_amount) {
        this.showSwalMassage(
          'Debit Amount cannot be greater than Purchase Booking Amount',
          'error'
        );
        return;
      }
      const final_payload = {
        ...payload_data,
        debit_note_details: debit_note_details,
      };
      console.log('final_payload: ', final_payload);

      this.debitNoteService.postDebitNoteData(final_payload).subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.handleSave.emit(payload);
          this.showSwalMassage('Debit Note Created Successfully', 'success');
          this.onCancelForm();
        },
        error: (e: any) => {
          console.log('e: ', e);
          this.showLoader = false;

        },
      });
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.debitNoteForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  debit_note_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: [{ value: '', disabled: true }, Validators.required],

      quantity: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$'),
        ],
      ],
      rate: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.pattern('^(?!-)[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
        ],
      ],

      rejected_qty: [''],
      amount: [''],
      tax_rate: [{ value: '', disabled: true }],
      tax_amount: [{ value: '', disabled: true }],
      txn_currency: [{ value: '', disabled: true }],
      debit_amount: [{ value: '', disabled: true }],
      purchase_booking_detail_ref_id: [''],
    });
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, index: number) => {
      console.log('element: ', element);
      let amount = 0;
      if (this.submitBtn) {
        amount = element.rejected_qty * element.rate;
      } else {
        amount = element.txn_currency_amount;
      }
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,

          quantity: element.alternate_qty,
          rate: element.rate,
          purchase_booking_detail_ref_id: element.id,
          rejected_qty: element.alternate_rejected_qty || '',
          amount: amount || '',
          tax_rate: '',
          tax_amount: '',
          txn_currency: '',
          debit_amount: '',
        })
      );

      this.salesService.getTaxRate(element.item_ref_id).subscribe({
        next: (data: any) => {
          const gridRow = (<FormArray>(
            this.debitNoteForm.get('debit_note_details')
          )).at(index);

          gridRow.get('tax_rate')?.setValue(data);

          gridRow.get('tax_amount')?.setValue((+amount * +data) / 100);
          gridRow
            .get('debit_amount')
            ?.setValue(+amount + (+amount * +data) / 100);
          gridRow.get('tax_rate')?.disable();
          gridRow.get('rate')?.disable();

          gridRow.get('tax_amount')?.disable();
          gridRow.get('debit_amount')?.disable();
        },
      });
    });

    return formArray;
  }

  get formArrgate() {
    return this.debitNoteForm.get('debit_note_details') as UntypedFormArray;
  }

  checkValidation(val: any, index: number) {
    console.log('debitNoteForm', val.target.value);

    const gridRow1 = (<FormArray>(
      this.debitNoteForm?.get('debit_note_details')
    )).at(index);
    const b = gridRow1.get('quantity')?.value;
    console.log('b: ', b);

    if (Number(val.target.value) > Number(b)) {
      // gridRow1.get('quantity')?.setErrors({ greaterThanQuantity: true });
      this.showSwalMassage(
        'Rejected Quantity cannot be greater thanquantity',
        'error'
      );
      gridRow1.get('rejected_qty')?.setValue('');
      // alert('quantity cannot be greater than accepted quantity');
    }
  }
  calculateQuantity(debitNoteForm: FormGroup, index: number) {
    console.log('debitNoteForm: ', debitNoteForm);
    console.log('debitNoteForm: ', debitNoteForm.value);

    let detailQuantity: any = [];
    detailQuantity = (debitNoteForm.get('debit_note_details') as FormArray).at(
      index
    );
    if (!this.isColumnHidden) {
      const amount = detailQuantity.get('amount')?.value;
      console.log('amount: ', amount);
      const taxRateControl = detailQuantity.get('tax_rate');
      console.log('taxRateControl: ', taxRateControl);

      const tax_amount = Number(
        ((taxRateControl?.value * amount) / 100).toFixed(2)
      );
      console.log('tax_amount: ', tax_amount);

      const debit_amt = Number((amount + tax_amount).toFixed(2));
      console.log('debit_amt: ', debit_amt);
      detailQuantity.get('tax_amount')?.setValue(tax_amount);
      detailQuantity.get('debit_amount')?.setValue(debit_amt);
    } else {
      console.log('detailQuantity: ', detailQuantity);

      if (detailQuantity) {
        const rejectedQtyControl = detailQuantity.get('rejected_qty');
        const rateControl = detailQuantity.get('rate');
        const taxRateControl = detailQuantity.get('tax_rate');
        const amount = Number(
          (rejectedQtyControl?.value * rateControl?.value).toFixed(2)
        );
        const tax_amount = Number(
          ((taxRateControl?.value * amount) / 100).toFixed(2)
        );
        const debit_amt = Number((amount + tax_amount).toFixed(2));
        detailQuantity.get('amount')?.setValue(amount);
        detailQuantity.get('tax_amount')?.setValue(tax_amount);
        detailQuantity.get('debit_amount')?.setValue(debit_amt);
      }
    }
  }

  supplierNameChange(event: any) {
    this.supplierDataById(event.value);
  }

  supplierDataById(id: any) {
    const supplier = this.SupplierData.filter((x: any) => x.id == id)[0];
    console.log('supplier: ', supplier);

    const supplier_name = supplier['key'].split('-')[1];
    const sn = supplier_name.split('/')[1];
    this.debitNoteForm.get('supplier_ref_type')?.setValue(sn);
    this.poRefId = this.poRefId.filter((d: any) => d.supplier_ref_id == id);

    this.paymentService
      .GetPurchaseBokkingBySupplier('get_by_supplier', supplier.id)
      .subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.purchaseBookingData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  purchaseOrderChange(event: any) {
    const d = {
      po_ref_id: event.value,
    };
    this.grnService.getFpcPodetails(d).subscribe({
      next: (data: any) => {
        const grnDetailsArray = this.debitNoteForm.get(
          'debit_note_details'
        ) as FormArray;
        for (let ind = 0; ind < grnDetailsArray.length; ind++) {
          const item_rfid = grnDetailsArray.at(ind).get('item_ref_id')?.value;
          for (let item = 0; item < data.length; item++) {
            if (data[item]['item_ref_id'] == item_rfid) {
              const qty = grnDetailsArray.at(ind).get('quantity')?.value;
              if (data[item]['balance_qty'] < qty) {
                this.showSwalMassage(
                  'Update  Purchase Order PO Quntity Should be less than Gate Pass Quntity.',
                  'warning'
                );
                this.debitNoteForm.get('po_ref_id')?.setValue('');
                return;
              } else {
                grnDetailsArray
                  .at(ind)
                  .get('rate')
                  ?.setValue(data[item]['rate']);
                grnDetailsArray
                  .at(ind)
                  .get('quantity')
                  ?.setValue(data[item]['balance_qty']);
              }
            }
          }
        }
      },
    });
  }

  handlePurchaseBookingChange(event: any) {
    console.log('event: ', event);
    this.poData = this.poRefId;
    this.getPurchaseBookingDataById(event.value);
  }

  getPurchaseBookingDataById(id: any) {
    console.log('id: ', id);
    this.purchaseBookingService.getPurchaseBookDataById(id).subscribe({
      next: (data: any) => {
        console.log('data: ', data);
        this.debitNoteForm
          .get('purchase_booking_txn_amount')
          ?.setValue(data.txn_currency_total_amount);
        this.debitNoteForm
          .get('supplier_bill_no')
          ?.setValue(data.supplier_bill_no || 'NA');

        const transaction_currency = this.txnCurrency.filter(
          (x: any) => x.id == data.txn_currency
        );
        console.log('transaction_currency: ', transaction_currency);

        this.debitNoteForm
          .get('transaction_currency')
          ?.setValue(transaction_currency[0]?.currency_symbol || 'NA');

        const detailsTable = data.purchase_booking_details;
        const gateDetailItemRow = detailsTable.filter(function (data: any) {
          return data;
        });
        if (gateDetailItemRow.length >= 1) {
          this.debitNoteForm.setControl(
            'debit_note_details',
            this.setExistingArray(gateDetailItemRow)
          );
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
}
