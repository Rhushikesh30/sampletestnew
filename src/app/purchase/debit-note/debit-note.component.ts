import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { GrnService } from 'src/app/shared/services/grn.service';

import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { DatePipe } from '@angular/common';
import { DebitNoteService } from 'src/app/shared/services/debit-note.service';

@Component({
  selector: 'app-debit-note',
  templateUrl: './debit-note.component.html',
  styleUrls: ['./debit-note.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class DebitNoteComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  resultData!: [];
  displayedColumns = [
    'actions',
    'edit',
    'debit_note_number',
    'transaction_date',
    'supplier_name',
    'purchase_booking_no',
    'purchase_booking_txn_amount',
    'total_txn_debit_amount',
    'with_quantity',
  ];
  renderedData: any = [];
  screenName = 'Debit Note';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;
  dataSource!: ExampleDataSource;

  exampleDatabase?: DebitNoteService;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  employee_authority: any;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private grnService: GrnService,
    private debitNoteService: DebitNoteService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService,
    private datePipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');
    this.employee_authority = localStorage.getItem('employee_authority');

    this.roleSecurityService.getAccessLeftPanel(userId, 'Purchase').subscribe({
      next: (data: any) => {
        this.sidebarData = data[0];
      },
      error: (e: any) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: Event | boolean) {
    this.listDiv = Boolean(item);
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = false;

    const json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.refresh();
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DebitNoteService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        // 'transaction_date': x.transaction_date,
        transactionRef_no: x.transaction_ref_no,
        purchase_booking_no: x.purchase_booking_no,
        supplier_ref_id: x.supplier_name,
        purchase_booking_txn_amount: x.purchase_booking_txn_amount,
        txn_currency_amount: x.txn_currency_amount,
        is_debit_note_with_qty_amt: x.is_debit_note_with_qty_amt,
        // 'quantity': x.quantity,
        // 'no_of_bags': x.no_of_bags
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}
export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DebitNoteService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];

    this.exampleDatabase.getAllAdvanceTables();

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = // advanceTable.transaction_date +
              (
                advanceTable.transaction_ref_no +
                advanceTable.purchase_booking_no +
                advanceTable.supplier_name +
                advanceTable.purchase_booking_txn_amount +
                advanceTable.txn_currency_amount +
                advanceTable.is_debit_note_with_qty_amt
              )
                // advanceTable.quantity +
                // advanceTable.no_of_bags
                .toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;

        case 'debit_note_number':
          [propertyA, propertyB] = [a.debit_note_number, b.debit_note_number];
          break;
        case 'transaction_date':
          [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
          break;
        case 'supplier_name':
          [propertyA, propertyB] = [a.supplier_name, b.supplier_name];
          break;
        case 'purchase_booking_no':
          [propertyA, propertyB] = [a.supplier_name, b.supplier_name];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
