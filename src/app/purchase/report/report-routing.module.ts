import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupplierBillReportComponent } from './supplier-bill-report/supplier-bill-report.component';
import { PurchaseComponent } from '../purchase/purchase.component';

const routes: Routes = [
  {
    path: 'farmer-bill',
    component: SupplierBillReportComponent
  } ,


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
