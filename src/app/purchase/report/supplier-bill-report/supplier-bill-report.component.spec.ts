import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierBillReportComponent } from './supplier-bill-report.component';

describe('SupplierBillReportComponent', () => {
  let component: SupplierBillReportComponent;
  let fixture: ComponentFixture<SupplierBillReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplierBillReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SupplierBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
