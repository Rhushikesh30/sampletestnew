import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  OnInit,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  FormArray,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { SalesOrderService } from 'src/app/shared/services/sales-order.service';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { ThemePalette } from '@angular/material/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { TableElement } from 'src/app/shared/TableElement';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { DatePipe, formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { SupplierBillReportService } from 'src/app/shared/services/supplier-bill-report.service';
import { Router } from '@angular/router';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';

@Component({
  selector: 'app-supplier-bill-report',
  templateUrl: './supplier-bill-report.component.html',
  styleUrls: ['./supplier-bill-report.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class SupplierBillReportComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  displayedColumns: string[] = [
    'actions',
    'transaction_date',
    'bill_number',
    'supplier_name',
    'supplier_type',
    'quantity',
    'total_transaction_amount',
    'payment_status',
  ];

  showHeader = false;
  showFooter = false;

  screenName = 'Farmer Bill';
  renderedData: any = [];
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  inputId!: number;
  listDiv = false;
  showList = true;
  sidebarData: any;

  exampleDatabase?: SupplierBillReportService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  allBillData!: ExampleDataSource;

  supplierBillForm!: UntypedFormGroup;
  supplier_name: any;
  transaction_date: any;
  to_date: any;
  so_type_id: any;
  payment_status: any;
  paymentData: any[] = [];
  supplierData: any[] = [];
  date: any;
  bill_no: any;
  net_quantity: any;
  amount: any;
  jsonData: any = null;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private formBuilder: FormBuilder,
    private supplierbillreportService: SupplierBillReportService,
    private errorCodes: ErrorCodes,
    private purchaseorderService: PurchaseorderService,
    public httpClient: HttpClient,
    private datePipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.purchaseorderService
      .getDynamicData('supplier', 'company_name')
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              data[i]['key'] = data[i]['key'] + '/' + data[i]['ref_type'];
            }
          }
          this.supplierData = data;
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.paymentData.push('Paid', 'UnPaid');

    this.refresh();
    this.initializeForm();
  }

  initializeForm() {
    this.supplierBillForm = this.formBuilder.group({
      supplier_id: [''],
      from_date: [''],
      to_date: [''],
      payment_status: [''],
    });
  }

  seerachData() {
    this.jsonData = {
      supplier_id: 0,
      from_date: '',
      to_date: '',
      payment_status: '',
    };
    console.log('Search button clicked!', this.supplierBillForm);

    if (this.supplierBillForm.value.supplier_id != '') {
      this.jsonData['supplier_id'] = this.supplierBillForm.value.supplier_id;
    }

    if (this.supplierBillForm.value.from_date != '') {
      this.jsonData['from_date'] = this.getDate(
        this.supplierBillForm.value.from_date
      );
    }

    if (this.supplierBillForm.value.to_date != '') {
      this.jsonData['to_date'] = this.getDate(
        this.supplierBillForm.value.to_date
      );
    }

    if (this.supplierBillForm.value.payment_status != '') {
      this.jsonData['payment_status'] =
        this.supplierBillForm.value.payment_status;
    }

    this.exampleDatabase = new SupplierBillReportService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort,
      this.jsonData
    );
  }

  getDate(new_date: any) {
    const str = new_date;
    const date = new Date(str);
    const day = date.getDate(); //Date of the month: 2 in our example
    const month = date.getMonth() + 1; //Month of the Year: 0-based index, so 1 in our example
    const year = date.getFullYear(); //Year: 2013
    const new_to_date = year + '-' + month + '-' + day;
    return new_to_date;
  }

  onResetForm() {
    this.supplierBillForm.reset();
    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.supplierbillreportService.getFarmerBillDataById(row.id).subscribe({
      next: (data: any) => {
        this.inputId = row.id;
        this.rowData = data;
        this.showList = false;
        this.submitBtn = flag;
        this.listDiv = true;
        this.showLoader = false;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  printRecord(row: any) {
    console.log('Printing record:', row);
    this.supplierbillreportService.getFarmerBillData(row.id).subscribe({
      next: (data: any) => {
        this.rowData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  // printInvoice(row: any) {
  //   window.open('#/report/farmerprint/?id=' + row, '_blank');
  // }

  printInvoice(row: any) {
    window.open('#/printfarmer/farmer/?id=' + row, '_blank');
  }

  cancelPress(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new SupplierBillReportService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort,
      this.jsonData
    );

    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'Bill Number': x.transaction_ref_no,
        'Supplier Name': x.supplier_name,
        'Supplier Type': x.supplier_type,
        'Net Quantity': x.quantity,
        Amount: x.total_transaction_amount,
        'Payment Status': x.payment_status,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}
export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: SupplierBillReportService,
    public paginator: MatPaginator,
    public _sort: MatSort,
    public searchData: any
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    if (this.searchData == null) {
      this.exampleDatabase.getAllAdvanceTables();
    } else {
      this.exampleDatabase.getSearchData(
        'search',
        JSON.stringify(this.searchData)
      );
    }
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.supplier_name +
              advanceTable.supplier_type +
              advanceTable.quantity +
              advanceTable.total_transaction_amount +
              advanceTable.payment_status
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    console.log('disconnect');
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'transaction_date':
          [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
          break;
        case 'transaction_ref_no':
          [propertyA, propertyB] = [a.transaction_ref_no, b.transaction_ref_no];
          break;
        case 'supplier_name':
          [propertyA, propertyB] = [a.supplier_name, b.supplier_name];
          break;
        case 'supplier_type':
          [propertyA, propertyB] = [a.supplier_type, b.supplier_type];
          break;
        case 'quantity':
          [propertyA, propertyB] = [a.quantity, b.quantity];
          break;
        case 'total_transaction_amount':
          [propertyA, propertyB] = [
            a.total_transaction_amount,
            b.total_transaction_amount,
          ];
          break;
        case 'payment_status':
          [propertyA, propertyB] = [a.payment_status, b.payment_status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
