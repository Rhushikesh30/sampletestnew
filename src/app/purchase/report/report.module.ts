import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { SupplierBillReportComponent } from './supplier-bill-report/supplier-bill-report.component';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTimepickerModule } from 'mat-timepicker';
import {OwlDateTimeModule,OwlNativeDateTimeModule,} from '@danielmoncada/angular-datetime-picker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    SupplierBillReportComponent,
  ],
  imports: [
    CommonModule,
    ReportRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule, 
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,
    MatDialogModule,
    MatTimepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatDatepickerModule
  ]
})
export class ReportModule { }
