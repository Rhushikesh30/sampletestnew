import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { GoodsReceiptNoteComponent } from './goods-receipt-note/goods-receipt-note.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { PurchaseBookingComponent } from './purchase-booking/purchase-booking.component';

import { QcComponent } from './qc/qc.component';
import { GatePassComponent } from './gate-pass/gate-pass.component';
import { PaymentComponent } from './payment/payment.component';
import { SupplierBillReportComponent } from './report/supplier-bill-report/supplier-bill-report.component';
import { DebitNoteComponent } from './debit-note/debit-note.component';
import { CreditNoteComponent } from './credit-note/credit-note.component';
import { ExpenseBookingComponent } from './expense-booking/expense-booking.component';
import { EmployeeExpenseComponent } from './employee-expense/employee-expense.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PurchaseComponent,
  },

  {
    path: 'grn',
    component: GoodsReceiptNoteComponent,
  },
  {
    path: 'purchase-order',
    component: PurchaseOrderComponent,
  },
  {
    path: 'purchase-booking',
    component: PurchaseBookingComponent,
  },
  {
    path: 'qc',
    component: QcComponent,
  },
  {
    path: 'gate-pass',
    component: GatePassComponent,
  },
  {
    path: 'payments',
    component: PaymentComponent,
  },

  {
    path: 'report/farmer-bill',
    component: SupplierBillReportComponent,
  },
  {
    path: 'debit-note',
    component: DebitNoteComponent,
  },
  {
    path: 'credit-note',
    component: CreditNoteComponent,
  },
  {
    path: 'expense-booking',
    component: ExpenseBookingComponent,
  },
  {
    path: 'employee-expense',
    component: EmployeeExpenseComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseRoutingModule {}
