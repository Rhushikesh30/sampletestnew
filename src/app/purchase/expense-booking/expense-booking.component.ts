import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { ThemePalette } from '@angular/material/core';
import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { ExpenseBookingService } from 'src/app/shared/services/expense-booking.service';

import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-expense-booking',
  templateUrl: './expense-booking.component.html',
  styleUrls: ['./expense-booking.component.scss'],
  providers: [ErrorCodes],
})
export class ExpenseBookingComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  displayedColumns: any = [
    'view',
    'unpost',
    'transaction_date',
    'transaction_ref_no',
    'supplier_name',
    'total_txn_amount',
    'posting_status',
    //  'txn_currency',
    //  'txn_currency_debit_amount',
    //  'txn_currency_credit_amount',
    //  'base_currency_debit_amount',
    //  'base_currency_credit_amount',
    //  'posting_status'
  ];
  renderedData: any = [];
  screenName = 'Expense Booking';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;
  rolename: any;

  exampleDatabase?: ExpenseBookingService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';

  constructor(
    private roleSecurityService: RoleSecurityService,
    private ExpenseBookingService: ExpenseBookingService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService
  ) {
    super();
  }

  ngOnInit(): void {
    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.rolename = rolename[0];

    const userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Purchase Order')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          console.log();

          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.refresh();
    this.showList = true;
    this.listDiv = false;
    this.showLoader = false;
  }
  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new ExpenseBookingService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'JV Number': x.transaction_ref_no,
        'Currency Name': x.supplier_name,
        'Total Transaction Debit Amount': x.total_txn_amount,
        'Posting Status': x.posting_status,
      }));

    TableExportUtil.exportToExcel(exportData, 'Journal Voucher Details');
  }

  Unpost(formValue: any) {
    const json_data = { data: this.encDecryService.encryptedData(formValue) };
    this.ExpenseBookingService.UnpostexpenseBooking(
      json_data,
      formValue['id']
    ).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been Unpost successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        // this.showList = true;
        // this.listDiv = false;
        // this.showLoader = false;
      },
      error: (e) => {
        //   this.showLoader = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: ExpenseBookingService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables('');
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.supplier_name +
              advanceTable.total_txn_amount +
              advanceTable.posting_status
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'district_name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'taluka_name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
