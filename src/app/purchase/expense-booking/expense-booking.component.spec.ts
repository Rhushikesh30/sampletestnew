import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseBookingComponent } from './expense-booking.component';

describe('ExpenseBookingComponent', () => {
  let component: ExpenseBookingComponent;
  let fixture: ComponentFixture<ExpenseBookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpenseBookingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExpenseBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
