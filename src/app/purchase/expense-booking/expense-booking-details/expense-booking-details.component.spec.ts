import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseBookingDetailsComponent } from './expense-booking-details.component';

describe('ExpenseBookingDetailsComponent', () => {
  let component: ExpenseBookingDetailsComponent;
  let fixture: ComponentFixture<ExpenseBookingDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpenseBookingDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExpenseBookingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
