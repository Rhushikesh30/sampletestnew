import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  UntypedFormArray,
} from '@angular/forms';
import { MAT_DATE_FORMATS } from '@angular/material/core';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { ExpenseBookingService } from 'src/app/shared/services/expense-booking.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { PaymentsService } from 'src/app/shared/services/payments.service';
import { FederationService } from 'src/app/shared/services/federation.service';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-expense-booking-details',
  templateUrl: './expense-booking-details.component.html',
  styleUrls: ['./expense-booking-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class ExpenseBookingDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  cancelFlag = true;
  bookingModeData: any[] = [];
  sessionData: any[] = [];
  withHoldingData: any[] = [];

  withHoldingDataSearch: any[] = [];
  supplierData: any[] = [];
  isOwnerShipStatus = false;
  SearchDataSupplier: any[] = [];
  SearchDataAgent: any[] = [];
  txnCurrency: any[] = [];
  agentData: any[] = [];
  purchaseBookingData: any[] = [];
  hsnSacData: any[] = [];
  hsnSacDataSearch: any[] = [];
  accountNameData: any[] = [];
  accountNameDataSearch: any[] = [];
  expenseDetailsParametersColumns: any[] = [];
  isColumnHidden = false;
  supplierBillNo: any;
  SearchData: any[] = [];
  EXPBForm!: FormGroup;
  btnVal = 'Submit';
  todayDate = new Date().toJSON().split('T')[0];
  previewImage1: any = '';
  previewImageType1: any;
  uploadStatus = '';
  fileName: any;
  fileKYC1!: FormData;
  showFileLoader = false;
  expenseDetailsParametersColumnsValues: any;
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  addButtonDisable = false;

  constructor(
    private formBuilder: FormBuilder,
    private paymentService: PaymentsService,
    private errorCodes: ErrorCodes,
    private federationService: FederationService,
    public datepipe: DatePipe,
    private expenseBookingService: ExpenseBookingService
  ) {}

  ngOnInit(): void {
    this.initializeForm();

    this.getAllData().then(() => {
      if (!Array.isArray(this.rowData)) {
        this.viewEditRecord(this.rowData);
      }
    });
  }
  removeImage() {
    this.previewImage1 = '';
    this.EXPBForm.get('attachment')?.setValue('');
  }
  setAttachment(event: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';

    const fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      const reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage1 = reader.result;
      };
      const fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType1 = true;
      }
      this.fileName = fileToUpload['name'];
      const compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        const formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileKYC1 = formData1;
        this.showFileLoader = true;
        this.uploadStatus = '';
        this.federationService.saveFile(this.fileKYC1).subscribe({
          next: (data: any) => {
            this.showFileLoader = false;
            const files3path = data['s3_file_path'];

            this.EXPBForm.get('attachment')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e: any) => {
            this.uploadStatus = '';
            this.fileName = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileName = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      this.showSwalMassage('File Size Not More Than 5 MB', 'error');
    }
  }

  getAllData() {
    return new Promise((resolve, reject) => {
      this.expenseBookingService.getSessionData().subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.sessionData = data;
          this.EXPBForm.get('fiscal_year')?.setValue(data.fiscal_year);
          this.EXPBForm.get('period')?.setValue(data.period);
        },
      });

      this.expenseBookingService.getAllDynamicData('With Holding').subscribe({
        next: (data: any) => {
          console.log('withhodingdata: ', data);
          this.withHoldingData = data.screenmatlistingdata_set;
          this.withHoldingDataSearch = data.screenmatlistingdata_set;
        },
      });
      this.expenseBookingService.getAllMasterData('Booking Mode').subscribe({
        next: (data: any) => {
          this.bookingModeData = data;
        },
      });
      this.expenseBookingService
        .getDynamicDataCombine(
          'get_combine_columns',
          'supplier',
          'company_name',
          'ref_type'
        )
        .subscribe({
          next: (data: any) => {
            this.SearchDataSupplier = data;
            this.supplierData = data;
            this.expenseBookingService.getCurrencyData().subscribe({
              next: (data: any) => {
                const tc = data.filter((d: any) => d.currency_code == 'INR')[0];
                this.EXPBForm.get('transaction_currency')?.setValue(tc.id);
                this.txnCurrency = data;
                resolve(data);
              },
            });
          },
        });

      this.expenseBookingService
        .getDynamicData('agent', 'agent_name')
        .subscribe({
          next: (data: any) => {
            this.agentData = data;
            this.SearchDataAgent = data;
          },
        });
      this.expenseBookingService.getHsnSacCode('hsn_sac').subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.hsnSacData = data;
          this.hsnSacDataSearch = data;
        },
      });
      this.expenseBookingService
        .getDynamicData('chart_of_account', 'name')
        .subscribe({
          next: (data: any) => {
            this.accountNameData = data;
            this.accountNameDataSearch = data;
          },
        });

      this.expenseBookingService.getAccountingParameters().subscribe({
        next: (data: any) => {
          console.log('accounting_parameter: ', data);
          this.expenseDetailsParametersColumns = data.columns;
          this.expenseDetailsParametersColumnsValues = data.data;
        },
      });

      this.expenseBookingService.getDynamicData('division', 'name').subscribe({
        next: (data: any) => {
          this.DivisionData = data;
          if (data.length > 0) {
            this.EXPBForm.get('division_ref_id')?.setValue(data[0]['id']);
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

      this.expenseBookingService.getDynamicData('location', 'name').subscribe({
        next: (data: any) => {
          this.LocationData = data;
          if (data.length > 0) {
            this.EXPBForm.get('location_ref_id')?.setValue(data[0]['id']);
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

      this.expenseBookingService
        .getDynamicData('department', 'name')
        .subscribe({
          next: (data: any) => {
            this.DepartmentData = data;
            if (data.length > 0) {
              this.EXPBForm.get('department_ref_id')?.setValue(data[0]['id']);
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

      this.expenseBookingService
        .getDynamicData('type_of_sale', 'name')
        .subscribe({
          next: (data: any) => {
            this.SaleTypeData = data;
            if (data.length > 0) {
              this.EXPBForm.get('sale_type_ref_id')?.setValue(data[0]['id']);
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    });
  }

  getParameter(i: any) {
    const paramter = 'parameter_' + i;
    return this.expenseDetailsParametersColumnsValues[paramter];
  }
  initializeForm() {
    this.EXPBForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', [Validators.required]],
      booking_ref_id: ['', [Validators.required]],
      supplier_ref_id: ['', [Validators.required]],
      supplier_ref_type: [''],
      agent_ref_id: [''],
      purchase_booking_ref_id: [''],
      supplier_bill_no: [''],
      fiscal_year: [''],
      period: [''],
      transaction_currency: ['', [Validators.required]],
      total_transaction_amount: [''],
      transaction_amount: [''],
      is_tds_flag: [false],
      section: [''],
      account_name: { value: '', disabled: true },
      tds_percent: [''],
      tds_amount: [''],
      is_clearing_agent_flag: [false],
      is_provision_flag: [false],
      attachment: [''],
      remark: [''],
      expense_booking_details: this.formBuilder.array([this.initialitemRow()]),
      is_supplier_agent: false,
      division_ref_id: ['', [Validators.required]],
      department_ref_id: ['', [Validators.required]],
      sale_type_ref_id: ['', [Validators.required]],
      location_ref_id: ['', [Validators.required]],
      is_user_generated: [true],
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      is_gst_set_off: [false],
      is_rcm: [false],
      hsn_sac: ['', [Validators.required]],
      account_name: ['', [Validators.required]],
      parameter_1: [''],
      parameter_2: [''],
      parameter_3: [''],
      parameter_4: [''],
      parameter_5: [''],
      parameter_6: [''],
      parameter_7: [''],
      transaction_amount: ['', [Validators.required]],
      tax_rate: [{ value: '', disabled: true }],
      tax_amount: [{ value: '', disabled: true }],
    });
  }

  get formArray() {
    return this.EXPBForm.get('expense_booking_details') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      const payload = this.EXPBForm.getRawValue();
      const gridRow1 = payload.expense_booking_details;
      let form_transaction_amount_sum = 0;
      for (let j = 0; j < gridRow1.length; j++) {
        form_transaction_amount_sum += Number(gridRow1[j].transaction_amount);
      }
      this.EXPBForm.get('total_transaction_amount')?.setValue(
        Number(form_transaction_amount_sum)
      );
      return true;
    }
  }

  viewEditRecord(row1: any) {
    this.expenseBookingService.getExpenseById(row1.id).subscribe({
      next: (edata: any) => {
        console.log(edata.purchase_booking_mst_ref_id, 'PIYUSHAAAAAAAA');
        console.log(edata.purchase_booking_ref_id, 'PIYUSHAAAAAAAA 22222222');

        console.log('edata: ', edata.attachment);
        edata = edata[0];

        if (edata.attachment !== null && edata.attachment) {
          console.log('edata: ', edata.attachment);

          this.previewImage1 =
            'https://tradingdev.sgp1.cdn.digitaloceanspaces.com/' +
            edata.attachment;
        }
        const transaction_currency = this.txnCurrency.filter(
          (x: any) => x.id == edata.txn_currency
        );
        console.log('this.txnCurrency: ', this.txnCurrency);

        console.log('transaction_currency: ', transaction_currency);
        this.isColumnHidden = edata.is_tds_applicable;
        this.EXPBForm.patchValue({
          id: edata.id,
          transaction_date: edata.transaction_date,
          supplier_ref_id: edata.supplier_ref_id,
          purchase_booking_ref_id: edata.purchase_booking_mst_ref_id,
          booking_ref_id: edata.booking_mode_ref_id,
          agent_ref_id: edata.agent_ref_id,
          supplier_bill_no: edata.supplier_bill_no || 'NA',
          fiscal_year: edata.fiscal_year,
          period: edata.period,
          transaction_currency: transaction_currency[0].id,
          total_transaction_amount: edata.total_txn_amount,
          transaction_amount: edata.txn_currency_amount,
          is_tds_flag: edata.is_tds_applicable,
          section: edata.section_ref_id,
          account_name: '',
          tds_percent: edata.tds_percent_applicable,
          tds_amount: edata.tds_amount,
          is_clearing_agent_flag: edata.is_clearing_agent,
          is_provision_flag: edata.is_provision,
          attachment: edata.attachment,
          remark: edata.remark,
          division_ref_id: edata.division_ref_id,
          department_ref_id: edata.department_ref_id,
          sale_type_ref_id: edata.sale_type_ref_id,
          location_ref_id: edata.location_ref_id,
          // expense_booking_details: this.formBuilder.array([
          //   this.initialitemRow(),
          // ]),
        });

        this.supplierDataById(edata.supplier_ref_id);
        const gateDetailItemRow = edata.expense_booking_details.filter(
          function (data: any) {
            return data;
          }
        );
        console.log('gateDetailItemRow: ', gateDetailItemRow);

        if (gateDetailItemRow.length >= 1) {
          this.EXPBForm.setControl(
            'expense_booking_details',
            this.setExistingArray(gateDetailItemRow)
          );
        }
        this.sectionChange(edata.section_ref_id);

        if (!this.submitBtn) {
          this.addButtonDisable = true;
          this.EXPBForm.disable();
          this.EXPBForm.get('expense_booking_details')?.value.disabled;
        }
        // else {
        //   this.debitNoteForm.disable();
        //   this.debitNoteForm.get('debit_note_details')?.value.disabled;
        // }
      },
    });
  }
  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, index: number) => {
      console.log('element: ', element);

      formArray.push(
        this.formBuilder.group({
          id: element.id,
          is_gst_set_off: element.is_gst_set_off || false,
          is_rcm: element.is_rcm || false,
          hsn_sac: element.hsn_sac_no,
          account_name: element.account_ref_id,
          parameter_1:
            element.parameter_1 === 'ALL' ? 'ALL' : Number(element.parameter_1),
          parameter_2:
            element.parameter_2 === 'ALL' ? 'ALL' : Number(element.parameter_2),
          parameter_3:
            element.parameter_3 === 'ALL' ? 'ALL' : Number(element.parameter_3),
          parameter_4:
            element.parameter_4 === 'ALL' ? 'ALL' : Number(element.parameter_4),
          parameter_5:
            element.parameter_5 === 'ALL' ? 'ALL' : Number(element.parameter_5),
          parameter_6:
            element.parameter_6 === 'ALL' ? 'ALL' : Number(element.parameter_6),
          parameter_7:
            element.parameter_7 === 'ALL' ? 'ALL' : Number(element.parameter_7),
          transaction_amount: element.txn_currency_amount,
          tax_rate: (
            parseFloat(element.txn_currency_cgst_rate) +
            parseFloat(element.txn_currency_sgst_rate) +
            parseFloat(element.txn_currency_igst_rate)
          ).toFixed(2),
          tax_amount: element.txn_currency_tax_amount,
          // (Number(element.rate) * Number(element.txn_currency_amount)) / 100,
        })
      );
    });

    return formArray;
  }

  onResetForm() {
    this.initializeForm();
    this.EXPBForm.get('transaction_date')?.setValue(this.todayDate);
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.EXPBForm.reset();
    this.handleCancel.emit(false);
  }
  onSubmit() {
    if (this.EXPBForm.invalid) {
      const invalid = [];
      const controls = this.EXPBForm.controls;
      const fc: any = this.EXPBForm.controls;
      console.log('iff');
      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }
      console.log(fc.expense_booking_details);

      for (let index = 0; index < this.formArray.length; index++) {
        const newForm = this.formArray.controls[index] as FormArray;
        for (const item in newForm.controls) {
          if (newForm.controls[item].status == 'INVALID') {
            newForm.controls[item].markAsTouched();
          }
        }
      }
      return;
    } else {
      this.showLoader = true;

      console.log('vLAUE');
      const data = this.EXPBForm.getRawValue();
      console.log('data: ', data);

      const payload_mst = {
        id: '',
        transaction_date: data.transaction_date,
        // ledger_group: 1,
        fiscal_year: data.fiscal_year,
        period: data.period,
        supplier_ref_id: data.supplier_ref_id,
        supplier_ref_type: data.supplier_ref_type,
        purchase_booking_mst_ref_id: data.purchase_booking_ref_id,
        booking_mode_ref_id: data.booking_ref_id,
        agent_ref_id: data.agent_ref_id,
        supplier_bill_no: data.supplier_bill_no,
        is_tds_applicable: data.is_tds_flag,
        section_ref_id: data.section || 0,
        tds_percent_applicable: data.tds_percent || 0,
        tds_amount: data.tds_amount || 0,
        is_clearing_agent: data.is_clearing_agent_flag,
        is_provision: data.is_provision_flag,
        txn_currency_amount: data.transaction_amount,
        total_txn_amount: data.total_transaction_amount,
        txn_currency: data.transaction_currency,
        conversion_rate: 1.0,
        attachment: data.attachment,
        remark: data.remark,
        division_ref_id: data.division_ref_id,
        department_ref_id: data.department_ref_id,
        sale_type_ref_id: data.sale_type_ref_id,
        location_ref_id: data.location_ref_id,
        is_user_generated: data.is_user_generated,
        expense_booking_details: [],
        is_supplier_agent: data.is_supplier_agent,
      };

      console.log(payload_mst, 'Piyusha Payloaddddddddddddd');

      const expense_booking_details = data.expense_booking_details;
      console.log('expense_booking_details: ', expense_booking_details);
      const payload_details = [];

      for (let i = 0; i < expense_booking_details.length; i++) {
        console.log(expense_booking_details[i]);
        payload_details.push({
          id: '',
          ledger_group: '',
          fiscal_year: data.fiscal_year,
          period: data.period,
          account_ref_id: expense_booking_details[i].account_name,
          is_gst_set_off: expense_booking_details[i].is_gst_set_off,
          is_rcm: expense_booking_details[i].is_rcm,
          hsn_sac_no: expense_booking_details[i].hsn_sac,
          rate_percentage: false,
          rate: 0.0,
          quantity: 0,
          parameter_1: expense_booking_details[i].parameter_1,
          parameter_2: expense_booking_details[i].parameter_2,
          parameter_3: expense_booking_details[i].parameter_3,
          parameter_4: expense_booking_details[i].parameter_4,
          parameter_5: expense_booking_details[i].parameter_5,
          parameter_6: expense_booking_details[i].parameter_6,
          parameter_7: expense_booking_details[i].parameter_7,
          conversion_rate: 1.0,
          txn_currency_amount: Number(
            expense_booking_details[i].transaction_amount
          ),
          base_currency_amount: Number(
            expense_booking_details[i].transaction_amount
          ),
          txn_currency_tax_amount: expense_booking_details[i].tax_amount || 0,
          txn_currency: 1,
          // posting_status: null,
          // posting_ref_no: null,
        });
      }
      const payload = {
        ...payload_mst,
        expense_booking_details: payload_details,
      };
      console.log('payload: ', payload);
      this.expenseBookingService.postExpenseBookingData(payload).subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.showSwalMassage('Expense Booking Saved Successfully', 'success');
          this.handleSave.emit(payload);
          this.onCancelForm();
        },
        error: (error: any) => {
          console.log(error);

          this.showLoader = false;
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(error).status),

            'error'
          );
        },
      });
    }
  }
  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  supplierNameChange(event: any) {
    this.supplierDataById(event.value);
  }
  AgentNameChange(e: any) {
    if (e.value == '' || e.value == undefined) {
      this.EXPBForm.get('is_supplier_agent')?.setValue(false);
    } else {
      this.EXPBForm.get('is_supplier_agent')?.setValue(true);
    }
  }

  supplierDataById(id: any) {
    const supplier = this.supplierData.filter((x: any) => x.id == id)[0];
    console.log('supplier: ', supplier);
    if (supplier == '' || supplier == undefined) {
    } else {
      const supplier_name = supplier['key'].split('-')[1];
      const sn = supplier_name.split('/')[1];
      this.EXPBForm.get('supplier_ref_type')?.setValue(sn);

      this.expenseBookingService
        .getDynamicData('supplier_all_data', supplier.id)
        .subscribe({
          next: (data: any) => {
            console.log('data: ', data);
            if (data.ownership_status_ref_id === 0) {
              this.isOwnerShipStatus = false;
            } else {
              this.isOwnerShipStatus = true;
            }
            // this.purchaseBookingData = data;
            this.EXPBForm.get('supplier_ref_type')?.setValue(sn);
          },
        });
      this.paymentService
        .GetPurchaseBokkingBySupplier('get_by_supplier', supplier.id)
        .subscribe({
          next: (data: any) => {
            console.log('data: ', data);
            this.purchaseBookingData = data;
            this.EXPBForm.get('supplier_bill_no')?.setValue(
              data.supplier_bill_no
            );
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    }
  }

  sectionChange(event: any) {
    console.log('event: ', event);
    const section = this.withHoldingData.filter((x: any) => x.id == event);
    console.log('section: ', section);
    this.EXPBForm.get('account_name')?.setValue(section[0].account_id);
    if (this.isOwnerShipStatus) {
      this.EXPBForm.get('tds_percent')?.setValue(section[0].nonc_rate);
    } else {
      this.EXPBForm.get('tds_percent')?.setValue(section[0].corporate_rate);
    }

    const gridRow = <FormArray>this.EXPBForm.get('expense_booking_details');
    let transaction_amount = 0;
    for (let i = 0; i < gridRow.length; i++) {
      transaction_amount =
        Number(transaction_amount) +
        Number(gridRow.at(i).get('transaction_amount')?.value);
    }
    console.log(transaction_amount);

    // const transaction_amount_sum = Number(
    //   this.EXPBForm.get('total_transaction_amount')?.value
    // );
    if (Number(transaction_amount) >= 0) {
      const tds_percent = Number(this.EXPBForm.get('tds_percent')?.value);
      const tds_amount = Number((transaction_amount * tds_percent) / 100);
      console.log(tds_amount);

      this.EXPBForm.get('tds_amount')?.setValue(tds_amount);
    }
  }

  purchaseBookingChange(event: any) {
    console.log('event: ', event.value);
    const supplierBillNo = this.purchaseBookingData.filter(
      (x: any) => x.id == event.value
    );
    console.log('supplierBillNo: ', supplierBillNo);
    this.EXPBForm.get('supplier_bill_no')?.setValue(
      supplierBillNo[0].supplier_bill_no
    );
    const transaction_currency = this.txnCurrency.filter(
      (x: any) => x.id == supplierBillNo[0].txn_currency
    );
    console.log('transaction_currency: ', transaction_currency);

    this.EXPBForm.get('transaction_currency')?.setValue(
      transaction_currency[0]?.currency_symbol || 'NA'
    );
  }
  async changeStatus(event: any) {
    this.isColumnHidden = !this.isColumnHidden;
    console.log(this.isColumnHidden);

    this.EXPBForm.get('is_tds_flag')?.setValue(this.isColumnHidden);
    if (!this.isColumnHidden) {
      this.EXPBForm.get('section')?.clearValidators();
      this.EXPBForm.get('section')?.updateValueAndValidity();

      this.EXPBForm.get('tds_percent')?.setValue('');
      this.EXPBForm.get('tds_amount')?.setValue('');
      this.EXPBForm.get('section')?.setValue('');
      this.EXPBForm.get('account_name')?.setValue('');
    } else if (this.isColumnHidden) {
      this.EXPBForm.get('section')?.addValidators(Validators.required);
      this.EXPBForm.get('section')?.updateValueAndValidity();
    }
  }

  hsnSacChange(EXPBForm: FormGroup, index: number) {
    const hsn_sac_id = this.EXPBForm?.get('expense_booking_details')?.value[
      index
    ]['hsn_sac'];
    console.log('hsn_sac_id: ', hsn_sac_id);
    if (
      this.EXPBForm?.get('expense_booking_details')?.value[index][
        'is_gst_set_off'
      ] == true
    ) {
      this.getTaxRate(index);
    }
  }

  calculateTxnAmount(EXPBForm: FormGroup, index: number) {
    console.log('EXPBForm: ', EXPBForm);
    console.log('EXPBForm: ', EXPBForm.value);
    const x = this.EXPBForm?.get('expense_booking_details')?.value[index][
      'is_gst_set_off'
    ];
    const gridRow = (<FormArray>(
      this.EXPBForm.get('expense_booking_details')
    )).at(index);
    if (x === false) {
      gridRow.get('tax_rate')?.setValue(0);

      gridRow.get('tax_amount')?.setValue(0);
    } else {
      const transaction_amount = Number(
        gridRow.get('transaction_amount')?.value
      );

      const tax_amount = Number(
        (transaction_amount * gridRow.get('tax_rate')?.value) / 100
      );
      gridRow.get('tax_amount')?.setValue(tax_amount);
    }
    console.log('x: ', x);
    const payload = this.EXPBForm.getRawValue();
    const gridRow1 = payload.expense_booking_details;

    let form_transaction_amount_sum = 0;
    const form_transaction_amount_sum1 = 0;

    let form_total_transaction_amount_sum = parseFloat('0');
    for (let j = 0; j < gridRow1.length; j++) {
      form_transaction_amount_sum +=
        parseFloat(gridRow1[j].transaction_amount) +
        parseFloat(gridRow1[j].tax_amount);
      form_total_transaction_amount_sum += parseFloat(
        gridRow1[j].transaction_amount
      );
    }
    this.EXPBForm.get('transaction_amount')?.setValue(
      Number(form_total_transaction_amount_sum)
    );

    const tds_percent = Number(this.EXPBForm?.get('tds_percent')?.value);
    const tds_amount = Number(
      (form_total_transaction_amount_sum * tds_percent) / 100
    );

    this.EXPBForm.get('total_transaction_amount')?.setValue(
      Number(form_transaction_amount_sum)
    );

    this.EXPBForm.get('tds_amount')?.setValue(tds_amount);
  }
  isDataHidden(EXPBForm: FormGroup, index: number) {
    console.log('EXPBForm: ', EXPBForm);
    console.log('EXPBForm: ', EXPBForm.value);

    const payload = this.EXPBForm.getRawValue();
    const gridRow1 = payload.expense_booking_details;

    this.EXPBForm?.get('expense_booking_details')?.get('is_gst_set_off');
    for (let j = 0; j < gridRow1.length; j++) {
      if (j == index) {
        return gridRow1[j].is_gst_set_off;
      }
    }
  }
  clickEvent(e: any, index: any) {
    const gridRow = (<FormArray>(
      this.EXPBForm.get('expense_booking_details')
    )).at(index);
    if (e.target.checked == true) {
      if (
        gridRow.get('hsn_sac')?.value == '' ||
        gridRow.get('hsn_sac')?.value == undefined
      ) {
      } else {
        this.getTaxRate(index);
      }
    } else if (e.target.checked == false) {
      gridRow.get('tax_rate')?.setValue('');
      gridRow.get('tax_amount')?.setValue('');
      const transaction_amount: any = Number(
        gridRow.get('transaction_amount')?.value
      );
      if (transaction_amount != '' || transaction_amount != undefined) {
        const payload = this.EXPBForm.getRawValue();
        const gridRow1 = payload.expense_booking_details;
        let form_transaction_amount_sum = 0;
        let form_total_transaction_amount_sum = parseFloat('0');
        for (let j = 0; j < gridRow1.length; j++) {
          form_transaction_amount_sum +=
            parseFloat(gridRow1[j].transaction_amount) +
            parseFloat(gridRow1[j].tax_amount);
          form_total_transaction_amount_sum += parseFloat(
            gridRow1[j].transaction_amount
          );
        }
        this.EXPBForm.get('transaction_amount')?.setValue(
          Number(form_total_transaction_amount_sum)
        );
      }
    }
  }

  getTaxRate(index: any) {
    const gridRow = (<FormArray>(
      this.EXPBForm.get('expense_booking_details')
    )).at(index);
    this.expenseBookingService
      .getTaxRate(gridRow.get('hsn_sac')?.value)
      .subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          gridRow.get('tax_rate')?.setValue(data);

          const transaction_amount: any = Number(
            gridRow.get('transaction_amount')?.value
          );
          if (transaction_amount != '' || transaction_amount != undefined) {
            const tax_amount = Number(
              (transaction_amount * gridRow.get('tax_rate')?.value) / 100
            );
            gridRow.get('tax_amount')?.setValue(tax_amount);
            const payload = this.EXPBForm.getRawValue();
            const gridRow1 = payload.expense_booking_details;
            let form_transaction_amount_sum = 0;
            let form_total_transaction_amount_sum = parseFloat('0');
            for (let j = 0; j < gridRow1.length; j++) {
              form_transaction_amount_sum +=
                parseFloat(gridRow1[j].transaction_amount) +
                parseFloat(gridRow1[j].tax_amount);
              form_total_transaction_amount_sum += parseFloat(
                gridRow1[j].transaction_amount
              );
            }
            this.EXPBForm.get('transaction_amount')?.setValue(
              Number(form_total_transaction_amount_sum)
            );
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }
}
