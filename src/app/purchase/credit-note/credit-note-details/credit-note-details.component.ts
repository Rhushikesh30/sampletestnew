// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-debit-note-details',
//   templateUrl: './debit-note-details.component.html',
//   styleUrls: ['./debit-note-details.component.scss']
// })
// export class DebitNoteDetailsComponent {

// }

import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { DatePipe, formatDate } from '@angular/common';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { GrnService } from 'src/app/shared/services/grn.service';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import Swal from 'sweetalert2';
import { PaymentsService } from 'src/app/shared/services/payments.service';
import { PurchaseBookingService } from 'src/app/shared/services/purchase-booking.service';
import { CreditNoteService } from 'src/app/shared/services/credit-note.service';
import { SalesService } from 'src/app/shared/services/sales.service';
import { SalesOrderService } from 'src/app/shared/services/sales-order.service';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-credit-note-details',
  templateUrl: './credit-note-details.component.html',
  styleUrls: ['./credit-note-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class CreditNoteDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<Event | boolean>();

  creditNoteForm!: UntypedFormGroup;
  isColumnHidden = false;
  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  FederationView = false;
  btnVal = 'Submit';

  grn_details_data: any[] = [];
  SupplierTypeData: any[] = [];
  customerData: any[] = [];
  supplierRefId: any[] = [];
  poRefId: any[] = [];
  uomData: any[] = [];
  txnCurrency: any[] = [];
  itemRefId: any[] = [];
  poData: any[] = [];
  purchaseBookingData: any[] = [];

  invoiceNumberData: any[] = [];
  fileNameKYC = '';
  fileNameKYC1 = '';
  uploadStatus = '';
  uploadStatus1 = '';
  previewImage: any;
  previewImage1: any;
  previewImageType: any;
  previewImageType1: any;
  fileName = '';
  fileName1 = '';
  fileKYC!: FormData;
  fileKYC1!: FormData;
  fileAttchatment!: FormData;
  fileAttchatment1!: FormData;
  dynamic: any;
  todayDate = new Date().toJSON().split('T')[0];

  currentDate = new Date().toJSON().split('T')[0];
  supplierExist = false;
  grnData: { id: number; name: string }[] = [];
  result: any = [];
  filterPurchaseOrderData: any = [];
  viewEdit = false;
  ItemType: any;
  ItemRateData: any = [];
  searchCustomerData: any[] = [];
  SearchData: any[] = [];
  itemDataId: any[] = [];
  POItemTypeData: any[] = [];

  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  @Input() screenName!: string;
  errorCodeData: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private grnService: GrnService,
    private paymentService: PaymentsService,
    private purchaseBookingService: PurchaseBookingService,
    private creditNoteService: CreditNoteService,
    private salesService: SalesService,
    private salesOrderService: SalesOrderService,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.getAllData();
    this.initializeForm();
  }
  async changeStatus() {
    const invoice_number_ref_id = this.creditNoteForm.get(
      'invoice_number_ref_id'
    )?.value;
    if (!invoice_number_ref_id) {
      this.showSwalMassage('Please Select Sales Order', 'error');
      return;
    }
    this.isColumnHidden = !this.isColumnHidden;

    this.creditNoteForm.get('is_with_qty')?.setValue(this.isColumnHidden);

    await this.getInvoiceById(invoice_number_ref_id);
    // execute a for loop over the creditNoteForm and change grnDetails -> amount to disabled
    console.log(
      'this.creditNoteForm.controls["credit_note_details"]: ',
      this.creditNoteForm.controls['credit_note_details']
    );

    for (
      let i = 0;
      i < this.creditNoteForm.controls['credit_note_details'].value.length;
      i++
    ) {
      const gridRow = (<FormArray>(
        this.creditNoteForm.get('credit_note_details')
      )).at(i);
      if (this.isColumnHidden) {
        gridRow.get('amount')?.disable();
      } else {
        gridRow.get('amount')?.enable();
      }
    }
  }

  initializeForm() {
    this.creditNoteForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', [Validators.required]],
      po_transaction_date: [''],
      sales_order_ref_id: ['', [Validators.required]],
      invoice_number_ref_id: ['', [Validators.required]],
      invoice_amount: ['', [Validators.required]],
      supplier_ref_id: ['', [Validators.required]],

      transaction_currency: [''],
      is_active: [true],
      purchase_order_no: [''],
      is_with_qty: [false],
      remark: [''],
      credit_note_details: this.formBuilder.array([this.credit_note_details()]),
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.creditNoteForm.reset();
      this.handleCancel.emit(false);
    }
  }

  getAllData() {
    this.grnService
      .getDynamicDataCombine(
        'get_combine_columns',
        'customer',
        'company_name',
        'ref_type'
      )
      .subscribe({
        next: (data: any) => {
          this.searchCustomerData = data;
          this.customerData = data;
          this.grnService.getCurrencyData().subscribe({
            next: (data: any) => {
              this.txnCurrency = data;
              if (!Array.isArray(this.rowData)) {
                this.showReset = false;
                this.viewRecord(this.rowData);
              }
            },
          });
        },
      });

    this.creditNoteService.getDynamicData('item_master', 'name').subscribe({
      next: (data: any) => {
        this.itemRefId = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
  

  viewRecord(edata: any) {
    console.log('edata: ', edata);
    this.viewEdit = true;

    const transaction_currency = this.txnCurrency.filter(
      (x: any) => x.id == edata.txn_currency
    );
    console.log('this.txnCurrency: ', this.txnCurrency);

    console.log('transaction_currency: ', transaction_currency);
    this.isColumnHidden = edata.is_credit_note_with_qty_amt;
    this.creditNoteForm.patchValue({
      id: edata.id,
      transaction_date: edata.transaction_date,

      po_transaction_date: edata.customer_po_date,
      sales_order_ref_id: edata.so_ref_id,
      invoice_number_ref_id: edata.invoice_mst_ref_id,
      supplier_ref_id: edata.customer_ref_id,
      transaction_currency: transaction_currency[0]?.id,
      invoice_amount: edata.txn_currency_invoice_amount,
      is_active: [true],
      purchase_order_no: edata.purchase_order_no,
      is_with_qty: edata.is_credit_note_with_qty_amt,
      remark: edata.remark,
    });

    this.supplierDataById(edata.customer_ref_id);

    const gateDetailItemRow = edata.credit_details.filter(function (data: any) {
      return data;
    });
    console.log('gateDetailItemRow: ', gateDetailItemRow);

    if (gateDetailItemRow.length >= 1) {
      this.creditNoteForm.setControl(
        'credit_note_details',
        this.setExistingArray(gateDetailItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
      this.viewBtn = false;
    } else {
      this.creditNoteForm.disable();
      this.creditNoteForm.get('credit_note_details')?.value.disabled;
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onSubmit(btn: any) {
    this.showLoader = true;
    if (this.creditNoteForm.invalid) {
      this.showLoader = false;
      this.showSwalmessage(
        'Please Fill All Required Fields',
        '',
        'error',
        false
      );
    } else {
      const payload = this.creditNoteForm.getRawValue();
      const val = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );
      const po_transaction_date = this.datepipe.transform(
        payload.po_transaction_date,
        'yyyy-MM-dd'
      );
      payload.transaction_date = val;
      payload.po_transaction_date = po_transaction_date;

      const payload_data = {
        id: '',
        transaction_date: payload.transaction_date,
        customer_ref_id: payload.supplier_ref_id,
        so_ref_id: payload.sales_order_ref_id,
        agent_ref_id: null,
        invoice_mst_ref_id: payload.invoice_number_ref_id, //PurchaseBooking
        txn_currency_invoice_amount: payload.invoice_amount,
        is_credit_note_with_qty_amt: payload.is_with_qty,
        remark: payload.remark,
        customer_po_date: payload.po_transaction_date,
      };

      const gridRow1 = payload.credit_note_details;
      const credit_note_details = [];
      let debit_amount_sum = 0;
      for (const element of gridRow1) {
        console.log(gridRow1);
        if (element['amount'] < 0) {
          this.showSwalMassage(
            'Debit Amount cannot be empty or negativw',
            'error'
          );
          return;
        }
        if (element['rejected_qty'] > element['quantity']) {
          this.showSwalMassage(
            'Rejected Quantity cannot be greater than Quantity',
            'error'
          );
          return;
        }
        const detail_data = {
          id: '',
          invoice_detail_ref_id: element['invoice_detail_ref_id'], //PurchaseBookingDetails
          item_ref_id: element['item_ref_id'],
          is_gst_set_off: true, //
          rejected_qty: Number(element['rejected_qty'] || 0), // rejected_qty > 0 if is_debit_note_with_qty_amt = True
          rate: element['rate'], // rate > 0 if is_debit_note_with_qty_amt = True
          txn_currency_amount: element['amount'], // txn_currency_amount > 0 is_debit_note_with_qty_amt = false
        };
        debit_amount_sum += Number(element['amount']);
        credit_note_details.push(detail_data);
      }
      console.log('debit_amount_sum: ', debit_amount_sum);
      console.log(
        'payload.txn_currency_invoice_amount: ',
        payload.txn_currency_invoice_amount
      );

      if (debit_amount_sum > payload_data.txn_currency_invoice_amount) {
        this.showSwalMassage(
          'Credit Amount cannot be greater than Invoice Amount',
          'error'
        );
        return;
      }
      const final_payload = {
        ...payload_data,
        credit_note_details: credit_note_details,
      };
      console.log('credit final_payload: ', final_payload);

      this.creditNoteService.postCreditNoteData(final_payload).subscribe({
        next: (data: any) => {
          console.log('data: ', data);
          this.handleSave.emit(payload);
          this.showSwalMassage('Credit Note Created Successfully', 'success');
          this.onCancelForm();
        },
        error: (e: any) => {
          console.log('e: ', e);
          this.showLoader = false;

        },
      });
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.creditNoteForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  credit_note_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: [{ value: '', disabled: true }, Validators.required],

      quantity: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$'),
        ],
      ],
      rate: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.pattern('^(?!-)[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
        ],
      ],

      rejected_qty: [''],
      amount: [''],
      tax_rate: [{ value: '', disabled: true }],
      tax_amount: [{ value: '', disabled: true }],
      txn_currency: [{ value: '', disabled: true }],
      debit_amount: [{ value: '', disabled: true }],
      invoice_detail_ref_id: [''],
    });
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, index: number) => {
      console.log('element: ', element);
      let amount = 0;
      let quantity = 0;
      if (this.submitBtn) {
        amount = element.rejected_qty * element.rate;
        quantity = element.order_qty;
      } else {
        amount = element.txn_currency_amount;
        quantity = element.quantity;
      }
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,

          quantity: quantity,
          rate: element.rate,
          invoice_detail_ref_id: element.id,
          rejected_qty: element.rejected_qty || '',
          amount: amount || '',
          tax_rate: '',
          tax_amount: '',
          txn_currency: '',
          debit_amount: '',
        })
      );

      this.salesService.getTaxRate(element.item_ref_id).subscribe({
        next: (data: any) => {
          const gridRow = (<FormArray>(
            this.creditNoteForm.get('credit_note_details')
          )).at(index);

          gridRow.get('tax_rate')?.setValue(data);

          gridRow.get('tax_amount')?.setValue((+amount * +data) / 100);
          gridRow
            .get('debit_amount')
            ?.setValue(+amount + (+amount * +data) / 100);
          gridRow.get('tax_rate')?.disable();
          gridRow.get('rate')?.disable();

          gridRow.get('tax_amount')?.disable();
          gridRow.get('debit_amount')?.disable();
        },
      });
    });

    return formArray;
  }

  get formArrgate() {
    return this.creditNoteForm.get('credit_note_details') as UntypedFormArray;
  }

  checkValidation(val: any, index: number) {
    const gridRow1 = (<FormArray>(
      this.creditNoteForm?.get('credit_note_details')
    )).at(index);
    const b = gridRow1.get('quantity')?.value;

    if (Number(val.target.value) > Number(b)) {
      this.showSwalMassage(
        'Rejected Quantity cannot be greater than quantity',
        'error'
      );
      gridRow1.get('rejected_qty')?.setValue('');
    }
  }
  calculateQuantity(creditNoteForm: FormGroup, index: number) {
    console.log('creditNoteForm: ', creditNoteForm);
    console.log('creditNoteForm: ', creditNoteForm.value);

    let detailQuantity: any = [];
    detailQuantity = (
      creditNoteForm.get('credit_note_details') as FormArray
    ).at(index);
    if (!this.isColumnHidden) {
      const amount = Number(detailQuantity.get('amount')?.value);
      console.log('amount: ', amount);
      const taxRateControl = detailQuantity.get('tax_rate');
      console.log('taxRateControl: ', taxRateControl);

      const tax_amount = Number(
        ((taxRateControl?.value * amount) / 100).toFixed(2)
      );
      console.log('tax_amount: ', tax_amount);

      const debit_amt = Number((amount + tax_amount).toFixed(2));
      console.log('debit_amt: ', debit_amt);
      detailQuantity.get('tax_amount')?.setValue(tax_amount);
      detailQuantity.get('debit_amount')?.setValue(debit_amt);
    } else {
      console.log('detailQuantity: ', detailQuantity);

      if (detailQuantity) {
        const rejectedQtyControl = detailQuantity.get('rejected_qty');
        const rateControl = detailQuantity.get('rate');
        const taxRateControl = detailQuantity.get('tax_rate');
        const amount = Number(
          (rejectedQtyControl?.value * rateControl?.value).toFixed(2)
        );
        const tax_amount = Number(
          ((taxRateControl?.value * amount) / 100).toFixed(2)
        );
        const debit_amt = Number((amount + tax_amount).toFixed(2));
        detailQuantity.get('amount')?.setValue(amount);
        detailQuantity.get('tax_amount')?.setValue(tax_amount);
        detailQuantity.get('debit_amount')?.setValue(debit_amt);
      }
    }
  }

  supplierNameChange(event: any) {
    this.supplierDataById(event.value);
  }

  supplierDataById(id: any) {
    const supplier = this.customerData.filter((x: any) => x.id == id)[0];
    console.log('supplier: ', supplier);

    const supplier_name = supplier['key'].split('-')[1];
    const sn = supplier_name.split('/')[1];
    this.creditNoteForm.get('supplier_ref_type')?.setValue(sn);
    this.poRefId = this.poRefId.filter((d: any) => d.supplier_ref_id == id);

    this.salesService.getSalesOrder(supplier.id, 'Invoice').subscribe({
      next: (data: any) => {
        console.log('data: ', data);
        this.purchaseBookingData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.creditNoteService.getAllCustomerInvoice(supplier.id).subscribe({
      next: (data: any) => {
        console.log('data: ', data);
        this.invoiceNumberData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  purchaseOrderChange(event: any) {
    const d = {
      po_ref_id: event.value,
    };
    this.grnService.getFpcPodetails(d).subscribe({
      next: (data: any) => {
        const grnDetailsArray = this.creditNoteForm.get(
          'credit_note_details'
        ) as FormArray;
        for (let ind = 0; ind < grnDetailsArray.length; ind++) {
          const item_rfid = grnDetailsArray.at(ind).get('item_ref_id')?.value;
          for (let item = 0; item < data.length; item++) {
            if (data[item]['item_ref_id'] == item_rfid) {
              const qty = grnDetailsArray.at(ind).get('quantity')?.value;
              if (data[item]['balance_qty'] < qty) {
                this.showSwalMassage(
                  'Update  Purchase Order PO Quntity Should be less than Gate Pass Quntity.',
                  'warning'
                );
                this.creditNoteForm.get('po_ref_id')?.setValue('');
                return;
              } else {
                grnDetailsArray
                  .at(ind)
                  .get('rate')
                  ?.setValue(data[item]['rate']);
                grnDetailsArray
                  .at(ind)
                  .get('quantity')
                  ?.setValue(data[item]['balance_qty']);
              }
            }
          }
        }
      },
    });
  }

  handleInvoiceNumberChange(event: any) {
    console.log('event: ', event);
    this.poData = this.poRefId;
    this.getInvoiceById(event.value);
  }

  getInvoiceById(id: any) {
    console.log('id: ', id);
    this.creditNoteService.getInvoiceById(id).subscribe({
      next: (data: any) => {
        console.log('invoice data: ', data);
        this.creditNoteForm
          .get('purchase_order_no')
          ?.setValue(data.purchase_order_no || 'NA');

        const transaction_currency = this.txnCurrency.filter(
          (x: any) => x.id == data.txn_currency
        );
        console.log('transaction_currency: ', transaction_currency);

        this.creditNoteForm
          .get('transaction_currency')
          ?.setValue(data.txn_currency);

        this.creditNoteForm
          .get('invoice_amount')
          ?.setValue(data.txn_currency_total_transaction_amount);

        const detailsTable = data.invoice_details;
        const gateDetailItemRow = detailsTable.filter(function (data: any) {
          return data;
        });
        if (gateDetailItemRow.length >= 1) {
          this.creditNoteForm.setControl(
            'credit_note_details',
            this.setExistingArray(gateDetailItemRow)
          );
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
}
