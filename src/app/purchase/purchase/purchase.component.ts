import { Component } from '@angular/core';
import { Event, Router, NavigationStart, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent {
  rolename: any='';

  ngOnInit(): void {
    let role: any = localStorage.getItem('roles');
    let roles = JSON.parse(role);
    let rolename = roles.map((a: any) => a.role_name)
    for(let rn in rolename){
      if(rolename[rn] == 'FPC Admin'){
        this.rolename = rolename[rn];
      }
    }
  }
}
