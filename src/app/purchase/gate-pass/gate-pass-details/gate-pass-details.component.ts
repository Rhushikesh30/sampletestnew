import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
  FormGroup,
} from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { GrnService } from 'src/app/shared/services/grn.service';

import { DatePipe, formatDate } from '@angular/common';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import Swal from 'sweetalert2';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-gate-pass-details',
  templateUrl: './gate-pass-details.component.html',
  styleUrls: ['./gate-pass-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class GatePassDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  gatepassForm!: UntypedFormGroup;
  FormGroup!: FormGroup;

  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  btnVal = 'Submit';
  selectedTime: any;
  gate_pass_details_data: any[] = [];
  SupplierTypeData: any[] = [];
  SupplierData: any[] = [];
  AgentData: any[] = [];
  DeliveryType: any[] = [];
  supplierRefId: any[] = [];
  uomData: any[] = [];
  itemRefId: any[] = [];
  ItemType: any[] = [];
  ItemSearchData: any[] = [];

  dynamic: any;
  todayDate = new Date().toJSON().split('T')[0];
  minValue: any;
  defaultValue: any;

  editable = true;

  grnData: { id: number; name: string }[] = [];
  result: any = [];
  delivery_req: any = [];
  currentDate: string;

  filterPurchaseOrderData: any = [];
  filterFPCData: any[] = [];
  SearchData: any[] = [];
  AgentSearchData: any[] = [];
  ItemData: any = [];
  addButtonDisable = false;
  empty_value = '';
  HsnData: any[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private grnService: GrnService,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private dynamicFormService: DynamicFormService
  ) {
    this.currentDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  }

  ngOnInit(): void {
    const t = new Date();
    this.minValue = t.toLocaleTimeString();
    this.defaultValue = this.minValue;

    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);

    this.getAllData();
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    } else {
      this.gatepassForm.get('transaction_date')?.setValue(this.currentDate);
    }
  }

  initializeForm() {
    this.gatepassForm = this.formBuilder.group({
      id: [''],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      transaction_date: ['', [Validators.required]],
      supplier_ref_id: [''],
      item_type_ref_id: ['', Validators.required],
      agent_ref_id: [''],
      vehicle_no: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z]{2}\\d{2}[A-Za-z]{1,2}\\d{4}$'),
        ],
      ],
      driver_name: [
        '',
        [
          Validators.maxLength(100),
          Validators.pattern('^[a-zA-Z]+(?: [a-zA-Z]+)*$'),
        ],
      ],
      driver_contact_no: [
        '',
        [Validators.required, Validators.pattern(/^\d{10}$/)],
      ],
      in_time: [''],
      out_time: [''],
      delivery_type: ['', Validators.required],
      distance: [''],
      remark: [''],
      transaction_ref_no: [],
      created_by: [localStorage.getItem('user_id')],
      gate_pass_details: this.formBuilder.array([this.gate_pass_details()]),
      workflow_status: '',
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.gatepassForm.reset();
      this.handleCancel.emit(false);
    }
  }
  convertToLocalTime(utcTime: string): string {
    const localTime = new Date(utcTime);
    console.log('locallll', localTime);

    return localTime.toLocaleString(); // You can customize the format as needed
  }
  getAllData() {
    this.grnService.getAllMasterData('Delivery Terms').subscribe({
      next: (data: any) => {
        this.DeliveryType = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.grnService.getAllMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.ItemType = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService.getItemTypeAndItem('GetItemMaster').subscribe({
      next: (data: any) => {
        this.itemRefId = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.uomData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService
      .getSupplierFarmerList(
        'get_combine_columns_supplier_farmer',
        'supplier',
        'company_name',
        'ref_type'
      )
      .subscribe({
        next: (data: any) => {
          this.SearchData = data;
          this.SupplierData = data;
        },
      });

    this.grnService.getDynamicData('agent', 'agent_name').subscribe({
      next: (data: any) => {
        this.AgentSearchData = data;
        this.AgentData = data;
      },
    });

    this.dynamicFormService.getDynamicData('hsn_sac', 'hsn_sac_no').subscribe({
      next: (data: any) => {
        this.HsnData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  viewRecord(edata: any) {
    // const formattedInTime = this.datepipe.transform(edata.in_time, 'hh:mm a');
    console.log('Before formatting:', edata.in_time);

    this.getItemTypeData(edata.item_type_ref_id);
    this.gatepassForm.patchValue({
      id: edata.id,
      tenant_id: edata.tenant_id,
      transaction_date: edata.transaction_date,
      supplier_ref_id: edata.supplier_ref_id,
      agent_ref_id: edata.agent_ref_id,
      item_type_ref_id: edata.item_type_ref_id,
      is_active: edata.is_active,
      vehicle_no: edata.vehicle_no,
      driver_name: edata.driver_name,
      driver_contact_no: edata.driver_contact_no,
      in_time: edata.in_time,
      out_time: edata.out_time,
      delivery_type: edata.delivery_type,
      distance: edata.distance,
      remark: edata.remark,
      transaction_ref_no: edata.transaction_ref_no,
      created_by: edata.created_by,
    });
    const gateDetailItemRow = edata.gate_pass_details.filter(function (
      data: any
    ) {
      return data;
    });
    if (gateDetailItemRow.length >= 1) {
      this.gatepassForm.setControl(
        'gate_pass_details',
        this.setExistingArray(gateDetailItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
      if (edata.qc_check) {
        this.addButtonDisable = true;
        this.gatepassForm.get('gate_pass_details')?.value.disabled;
        this.gatepassForm.disable();
        this.gatepassForm.get('out_time')?.enable();
        this.gatepassForm.get('remark')?.enable();
      }
    } else {
      this.addButtonDisable = true;
      this.gatepassForm.disable();
      this.gatepassForm.get('gate_pass_details')?.value.disabled;
    }
  }

  gate_pass_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      no_of_bags: [
        '',
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$'),
        ],
      ],
      quantity: [
        '',
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$'),
        ],
      ],
      uom: [{ value: '', disabled: true }],
      hsn_sac_no: [{ value: '', disabled: true }],
    });
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,
          uom: { value: element.alternate_uom, disabled: true },
          no_of_bags: element.no_of_bags,
          hsn_sac_no: [{ value: element.hsn_sac_no, disabled: true }],
          alternate_uom: element.base_uom,
          quantity: element.alternate_quantity,
        })
      );
    });

    return formArray;
  }

  get formArrgate() {
    return this.gatepassForm.get('gate_pass_details') as UntypedFormArray;
  }

  onSubmit(btn: any) {
    this.showLoader = true;

    const payload = this.gatepassForm.getRawValue();

    const intime = this.convertToLocalTime(
      this.gatepassForm.get('in_time')?.value
    );

    console.log('innnnnnnnnnnnnnnnnnnnnnn', intime);

    console.log(payload, 'PIYUSHAAAAAAAAAAA');
    payload.in_time = intime;

    console.log(payload, 'Afterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr');

    // this.gatepassForm.get('in_time')?.value;
    for (let index = 0; index < this.formArrgate.length; index++) {
      console.log(
        'this.formArrgate.controls[index].status: ',
        this.formArrgate.controls[index].status
      );
      const newForm = this.formArrgate.controls[index] as FormArray;
      console.log('newForm: ', newForm);
      for (const item in newForm.controls) {
        if (newForm.controls[item].status == 'INVALID') {
          newForm.controls[item].markAsTouched();
        }
      }

      // if (newForm['controls' == 'INVALID') {
      //   this.formArrgate.controls[index].markAsTouched();
      // }
    }

    // if (this.gate_pass_details.invalid) {
    //   this.gate_pass_details.markAsTouched();

    //   this.showLoader = false;
    //   return;
    // }

    if (this.gatepassForm.invalid) {
      const invalid = [];
      const controls = this.gatepassForm.controls;
      const fc: any = this.gatepassForm.controls;

      for (const name in controls) {
        if (controls[name].status === 'INVALID') {
          controls[name].markAsTouched();
          // console.log('controls[name]: ', controls[name]);
        }
      }
      this.showLoader = false;
      return;
    } else {
      const dt = this.DeliveryType.filter(
        (d: any) => d.id == this.gatepassForm.get('delivery_type')?.value
      );
      const distance_val = this.gatepassForm.get('distance')?.value;
      if (
        dt[0]['master_key'] == 'Spot' &&
        (distance_val == '' || distance_val == undefined)
      ) {
        this.showSwalMassage('Please Enter Distance', 'warning');
        this.showLoader = false;
        return;
      }
      if (this.gatepassForm.value.agent_ref_id == '') {
        console.log(
          'this.gatepassForm.value.agent_ref_id: ',
          this.gatepassForm.value.agent_ref_id
        );
        this.gatepassForm.value.agent_ref_id = NaN;
      }
      if (this.gatepassForm.value.out_time == '') {
        this.gatepassForm.value.out_time = NaN;
      }
      console.log(
        'this.gatepassForm.value.agent_ref_id: ',
        this.gatepassForm.value.agent_ref_id
      );

      const payload = this.gatepassForm.getRawValue();
      payload.transaction_date = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );

      console.log(payload);
      if (payload.agent_ref_id == '') {
        console.log('payload.agent_ref_id: ', payload.agent_ref_id);
        console.log(
          'this.gatepassForm.value.agent_ref_id: ',
          this.gatepassForm.value.agent_ref_id
        );
        payload.agent_ref_id = NaN;
      }
      if (payload.out_time == '') {
        payload.out_time = NaN;
      }
      this.handleSave.emit(payload);
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.gatepassForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
    this.gatepassForm.get('transaction_date')?.setValue(this.currentDate);
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  addNewRow() {
    this.formArrgate.push(this.gate_pass_details());
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrgate.removeAt(index);
      return true;
    }
  }

  ItemNameChange(event: any, index: any) {
    const item_cnt = this.gatepassForm.value.gate_pass_details.filter(
      (x: any) => x.item_ref_id == event.value
    );
    if (item_cnt.length > 1) {
      this.showSwalMassage('Item Already Selected', 'warning');
      (<FormArray>this.gatepassForm.get('gate_pass_details'))
        .at(index)
        .get('item_ref_id')
        ?.setValue('');
    }
    const gridRow = (<FormArray>this.gatepassForm.get('gate_pass_details')).at(
      index
    );

    this.grnService.getDynamicData('item_master', 'hsn_sac_code').subscribe({
      next: (data: any) => {
        for (let j = 0; j < data.length; j++) {
          if (event.value == data[j]['id']) {
            gridRow.get('hsn_sac_no')?.setValue(Number(data[j]['key']));
          }
        }
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService.getDynamicData('item_master', 'uom').subscribe({
      next: (data: any) => {
        const uomdata = data.filter((d: any) => d.id == event.value);
        const uom_id = this.uomData.filter(
          (d: any) => d.id == uomdata[0]['key']
        );

        gridRow.get('uom')?.setValue(Number(uom_id[0]['id']));
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  ItemTypeChange(item_id: any) {
    this.gatepassForm.get('gate_pass_details')?.reset();
    const gridRow = <FormArray>this.gatepassForm.get('gate_pass_details');
    if (gridRow.length > 1) {
      for (let index = gridRow.length - 1; index > 0; index--) {
        this.deleteRow(index);
      }
    }

    this.ItemData = this.itemRefId.filter((d: any) => d.item_type == item_id);
    this.ItemSearchData = this.itemRefId.filter(
      (d: any) => d.item_type == item_id
    );
  }

  getItemTypeData(item_id: any) {
    this.grnService.getItemTypeAndItem('GetItemMaster').subscribe({
      next: (data: any) => {
        this.itemRefId = data;
        this.ItemData = data.filter((d: any) => d.item_type == item_id);
        this.ItemSearchData = data.filter((d: any) => d.item_type == item_id);
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  DeliveryTypeChange(val: any) {
    const dt = this.DeliveryType.filter((d: any) => d.id == val);
    if (dt[0]['master_key'] == 'Spot') {
      this.gatepassForm.get('distance')?.addValidators(Validators.required);
    } else {
      this.gatepassForm.get('distance')?.clearValidators();
    }
    this.gatepassForm.controls['distance'].updateValueAndValidity();
  }
}
