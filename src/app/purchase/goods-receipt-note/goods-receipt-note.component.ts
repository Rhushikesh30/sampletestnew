import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  Renderer2,
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { GrnService } from 'src/app/shared/services/grn.service';

import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { DatePipe } from '@angular/common';
import { STATUS, getStatusColor } from 'src/app/constants/constants';

@Component({
  selector: 'app-goods-receipt-note',
  templateUrl: './goods-receipt-note.component.html',
  styleUrls: ['./goods-receipt-note.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class GoodsReceiptNoteComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  resultData!: [];
  displayedColumns = [
    'view',
    'edit',
    'print',
    'transaction_date',
    'transactionRef_no',
    'po_ref_id',
    'gate_pass_no',
    'supplier_ref_id',
    // 'quantity',
    // 'no_of_bags',
    'workflow_status',
  ];
  renderedData: any = [];
  screenName = 'GRN';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;
  dataSource!: ExampleDataSource;

  exampleDatabase?: GrnService;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  employee_authority: any;
  ispopupOpened = false;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private grnService: GrnService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService,
    private datePipe: DatePipe,
    private renderer: Renderer2,
    private el: ElementRef
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');
    this.employee_authority = localStorage.getItem('employee_authority');

    this.roleSecurityService.getAccessLeftPanel(userId, 'Purchase').subscribe({
      next: (data: any) => {
        this.sidebarData = data[0];
      },
      error: (e: any) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }

  showFormList(item: boolean) {
    this.ispopupOpened = !this.ispopupOpened;
    window.scrollTo(0, 0);

    const section =
      this.el.nativeElement.ownerDocument.querySelector('.content');
    if (section) {
      if (this.ispopupOpened) {
        this.renderer.addClass(section, 'popup-section');
      } else {
        this.renderer.removeClass(section, 'popup-section');
      }
    }
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: Event | boolean) {
    this.listDiv = Boolean(item);
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;

    const json_data = { data: this.encDecryService.encryptedData(formValue) };
    // const json_data = { data: formValue };
    console.log(formValue, 'HELLOOOOOO');

    this.grnService.createGrn(json_data, formValue.id).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 2) {
          console.log(formValue, 'HELLOOOOOO');

          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e: any) => {
        this.showLoader = false;
        console.log(e);
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  getColorStatus(status: string): string {
    // console.log('11111', getStatusColor(status));

    return getStatusColor(status);
  }
  refresh() {
    this.exampleDatabase = new GrnService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'GRN Number': x.transaction_ref_no,
        'PO Number': x.po_ref_no,
        'Gate Pass Number': x.gate_pass_no,
        'Supplier Name': x.supplier_name,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }

  printInvoice(row: any) {
    window.open('#/grn-print/printgrn/?id=' + row, '_blank');
  }
}
export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: GrnService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];

    this.exampleDatabase.getAllAdvanceTables();

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.po_ref_no +
              advanceTable.supplier_name +
              advanceTable.gate_pass_no
            )

              // advanceTable.quantity +
              // advanceTable.no_of_bags
              .toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'transaction_date':
          [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
          break;
        case 'transactionRef_no':
          [propertyA, propertyB] = [a.transactionRef_no, b.transactionRef_no];
          break;
        case 'po_ref_id':
          [propertyA, propertyB] = [a.po_ref_id, b.po_ref_id];
          break;
        case 'supplier_ref_id':
          [propertyA, propertyB] = [a.supplier_ref_id, b.supplier_ref_id];
          break;
        case 'item_ref_id':
          [propertyA, propertyB] = [a.item_ref_id, b.item_ref_id];
          break;
        case 'quantity':
          [propertyA, propertyB] = [a.quantity, b.quantity];
          break;
        case 'no_of_bags':
          [propertyA, propertyB] = [a.no_of_bags, b.no_of_bags];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
