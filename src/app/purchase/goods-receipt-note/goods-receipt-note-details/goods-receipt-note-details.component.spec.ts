import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsReceiptNoteDetailsComponent } from './goods-receipt-note-details.component';

describe('GoodsReceiptNoteDetailsComponent', () => {
  let component: GoodsReceiptNoteDetailsComponent;
  let fixture: ComponentFixture<GoodsReceiptNoteDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoodsReceiptNoteDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoodsReceiptNoteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
