import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { DatePipe, formatDate } from '@angular/common';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { GrnService } from 'src/app/shared/services/grn.service';
import Swal from 'sweetalert2';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-goods-receipt-note-details',
  templateUrl: './goods-receipt-note-details.component.html',
  styleUrls: ['./goods-receipt-note-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class GoodsReceiptNoteDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData: any = [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<boolean>();

  grnForm!: UntypedFormGroup;

  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  FederationView = false;
  btnVal = 'Submit';

  grn_details_data: any[] = [];
  SupplierTypeData: any[] = [];
  SupplierData: any[] = [];
  supplierRefId: any[] = [];
  poRefId: any[] = [];
  uomData: any[] = [];
  txnCurrency: any[] = [];
  itemRefId: any[] = [];
  poData: any[] = [];
  gatepassData: any[] = [];
  fileNameKYC = '';
  fileNameKYC1 = '';
  uploadStatus = '';
  uploadStatus1 = '';
  previewImage: any;
  previewImage1: any;
  previewImageType: any;
  previewImageType1: any;
  fileName = '';
  fileName1 = '';
  fileKYC!: FormData;
  fileKYC1!: FormData;
  fileAttchatment!: FormData;
  fileAttchatment1!: FormData;
  dynamic: any;
  todayDate = new Date().toJSON().split('T')[0];

  currentDate = new Date().toJSON().split('T')[0];
  supplierExist = false;
  grnData: { id: number; name: string }[] = [];
  result: any = [];
  filterPurchaseOrderData: any = [];
  viewEdit = false;
  viewBatchNo = false;
  ItemType: any;
  ItemRateData: any = [];
  SearchDataSupplier: any[] = [];
  SearchData: any[] = [];
  POSearchData: any[] = [];
  ItemSearchData: any[] = [];
  itemDataId: any[] = [];
  POItemTypeData: any[] = [];

  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  @Input() screenName!: string;
  errorCodeData: any;

  ViewLoader = false;
  viewOnSendback = false;
  HsnData: any[] = [];
  isPOSelected = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private grnService: GrnService,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private dynamicFormService: DynamicFormService,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef
  ) {
    this.currentDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  }

  ngOnInit(): void {
    this.getAllData();
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    } else {
      this.grnForm.get('transaction_date')?.setValue(this.currentDate);
    }
  }

  initializeForm() {
    this.grnForm = this.formBuilder.group({
      id: [''],
      form_name: ['GRN'],
      transaction_date: ['', [Validators.required]],
      transaction_ref_no: [0],
      supplier_ref_id: ['', [Validators.required]],
      supplier_ref_type: ['', Validators.required],
      po_ref_id: [''],
      gate_pass_ref_id: ['', [Validators.required]],
      is_active: [true],
      vehicle_receipt_no: [
        '',
        [Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9]{1,50}$')],
      ],
      vehicle_no: [
        '',
        [Validators.pattern('^[A-Za-z]{2}\\d{2}[A-Za-z]{1,2}\\d{4}$')],
      ],
      transporter_name: [
        '',
        [
          Validators.maxLength(100),
          Validators.pattern('^[a-zA-Z]+(?: [a-zA-Z]+)*$'),
        ],
      ],
      supplier_bill_no: [
        '',
        [Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9]{1,50}$')],
      ],
      supplier_bill_date: [''],
      e_way_bill_no: [''],
      bill_of_entry_no: [''],
      remark: [''],
      fiscal_year: [2022],
      created_by: [localStorage.getItem('user_id')],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      is_qc_generated: [false],
      is_qc_required: [true],
      qc_status: ['Pending'],
      total_gross_weight: ['0'],
      total_tare_weight: ['0'],
      txn_currency: [''],
      gate_pass_no: [''],
      workflow_remark: [''],
      error_code_id: [''],
      grn_details: this.formBuilder.array([this.grn_details()]),
      conversion_rate: [
        1,
        [
          Validators.required,
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
        ],
      ],
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.grnForm.reset();
      this.handleCancel.emit(false);
    }
  }

  getAllData() {
    this.grnService.getAllMasterData('PO Supplier Type').subscribe({
      next: (data: any) => {
        this.SupplierTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService.getItemData().subscribe({
      next: (data: any) => {
        this.ItemSearchData = data;
        this.itemRefId = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.uomData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService
      .getSupplierData('get_Supplier', 'supplier', 'company_name', 'ref_type')
      .subscribe({
        next: (data: any) => {
          this.SearchDataSupplier = data;
          this.SupplierData = data;
        },
      });

    this.grnService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.txnCurrency = data;
        this.txnCurrency = data.filter((d: any) => d.country_name == 'India');

        this.grnForm.get('txn_currency')?.setValue(this.txnCurrency[0]['id']);
        this.grnForm.get('base_currency')?.setValue(this.txnCurrency[0]['id']);

        const gridRow = (<FormArray>this.grnForm.get('grn_details')).at(0);
        gridRow.get('txn_currency')?.setValue(this.txnCurrency[0]['id']);
        gridRow.get('base_currency')?.setValue(this.txnCurrency[0]['id']);
      },
    });

    this.grnService.getAllMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.POItemTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dynamicFormService.getDynamicData('error_code_mst', 'code').subscribe({
      next: (res: any) => {
        this.errorCodeData = res;
      },
    });

    this.dynamicFormService.getDynamicData('hsn_sac', 'hsn_sac_no').subscribe({
      next: (data: any) => {
        this.HsnData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  viewRecord(edata: any) {
    this.viewEdit = true;
    console.log(edata.po_ref_id);

    this.grnService.getGetPassNumber(edata.supplier_ref_id, '').subscribe({
      next: (data: any) => {
        const arr: any = [];

        for (let j = 0; j < data['Purchase_order'].length; j++) {
          arr.push(data['Purchase_order'][j]['id']);
        }
        if (edata.po_ref_id !== '' && edata.po_ref_id !== null) {
          arr.push(edata.po_ref_id);
        }

        this.grnService.getAllPurchaseData('purchase_order').subscribe({
          next: (data1: any) => {
            data1 = data1.filter((d: any) => arr.includes(d.id));

            this.POSearchData = data1;
            this.poData = data1;
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.grnService
      .getGetPassNumber(edata.supplier_ref_id, edata.gate_pass_ref_id)
      .subscribe({
        next: (data: any) => {
          this.SearchData = data['gate_pass'];
          this.gatepassData = data['gate_pass'];
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    console.log(edata);
    const employee_authority = localStorage.getItem('employee_authority');
    // this.roleSecurityService
    //   .getWorkflowBtn(employee_authority, this.screenName, edata.gate_pass_no)
    //   .subscribe((res: any) => {
    //     this.myTemplate = res;
    //   });

    if (edata.po_ref_id != null) {
      this.isPOSelected = true;
    }

    this.grnForm.patchValue({
      id: edata.id,
      form_name: 'GRN',
      transaction_date: edata.transaction_date,
      transaction_ref_no: edata.transaction_ref_no,
      supplier_ref_id: edata.supplier_ref_id,
      po_ref_id: edata.po_ref_id,
      gate_pass_ref_id: edata.gate_pass_ref_id,
      is_active: edata.is_active,
      vehicle_receipt_no: edata.vehicle_receipt_no,
      vehicle_no: edata.vehicle_no,
      transporter_name: edata.transporter_name,
      supplier_bill_no: edata.supplier_bill_no,
      supplier_bill_date: edata.supplier_bill_date,
      e_way_bill_no: edata.e_way_bill_no,
      bill_of_entry_no: edata.bill_of_entry_no,
      remark: edata.remark,
      transaction_id: edata.transaction_id,
      transaction_type_id: edata.transaction_type_id,
      fiscal_year: edata.fiscal_year,
      created_by: edata.created_by,
      tenant_id: edata.tenant_id,
      base_currency: edata.base_currency,
      is_qc_generated: edata.is_qc_generated,
      qc_status: edata.qc_status,
      total_gross_weight: edata.total_gross_weight,
      total_tare_weight: edata.total_tare_weight,
      supplier_ref_type: edata.supplier_ref_type,
      gate_pass_no: edata.gate_pass_no,
      workflow_status: edata.workflow_status,
      error_code_id: edata.error_code_id,
      workflow_remark: edata.workflow_remark,
    });

    this.supplierTypeChnage(edata.supplier_ref_type);

    const gateDetailItemRow = edata.grn_details.filter(function (data: any) {
      return data;
    });
    if (gateDetailItemRow.length >= 1) {
      gateDetailItemRow.forEach((ele: any) => {
        ele['quantity'] = ele.alternate_qty;
      });
      this.grnForm.setControl(
        'grn_details',
        this.setExistingArray(gateDetailItemRow)
      );
    }

    if (this.submitBtn) {
      this.ViewLoader = true;
      this.btnVal = 'Update';
      this.viewBtn = false;
      let employee_authority = localStorage.getItem('employee_authority');
      if (employee_authority == 'None' && edata.workflow_status == 'SendBack') {
        employee_authority = 'Maker';
      }
      if (
        (employee_authority == 'None' &&
          edata.workflow_status == 'Pending For Approval') ||
        edata.workflow_status == 'Initiated'
      ) {
        employee_authority = 'Checker';
      }
      this.roleSecurityService
        .getWorkflowBtn(employee_authority, this.screenName, edata.gate_pass_no)
        .subscribe((res: any) => {
          this.ViewLoader = false;
          this.viewOnSendback = true;
          this.myTemplate = res;
        });

      if (employee_authority == 'Checker') {
        this.grnForm.disable();
        this.grnForm.controls['workflow_remark'].enable();
        this.grnForm.controls['error_code_id'].enable();
      }
      this.grnForm.get('supplier_ref_id')?.disable();
      this.grnForm.get('gate_pass_ref_id')?.disable();
    } else {
      this.viewBtn = true;
      this.grnForm.disable();
      this.grnForm.get('grn_details')?.value.disabled;
    }
  }

  openAlert(event: any) {
    this.cnt = this.cnt + 1;
    if (this.cnt == 1) {
      if (event.target.id == 'Cancel') {
        this.onCancelForm();
        (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
        (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        (<HTMLInputElement>document.getElementById('SendBack')).disabled = true;
      } else if (event.target.id == 'SendBack') {
        console.log(this.grnForm);
        if (
          this.grnForm.controls['workflow_remark'].value == null ||
          this.grnForm.controls['workflow_remark'].value == ''
        ) {
          console.log('if');
          this.grnForm.get('error_code_id')?.addValidators(Validators.required);
          this.grnForm.get('error_code_id')?.updateValueAndValidity();

          this.grnForm
            .get('workflow_remark')
            ?.addValidators(Validators.required);
          this.grnForm.get('workflow_remark')?.updateValueAndValidity();
          this.showSwalMassage(
            'Please Fill Sendback Remark and Sendback Code',
            'error'
          );
          this.cnt = 0;
        } else {
          event.target.disabled = true;
          this.grnForm.get('workflow_remark')?.clearValidators();
          this.grnForm.get('error_code_id')?.clearValidators();
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          console.log('else');
          this.onSubmit(event.target.id);
        }
      } else {
        event.target.disabled = true;
        this.grnForm.get('workflow_remark')?.clearValidators();
        this.grnForm.get('error_code_id')?.clearValidators();
        if (event.target.id == 'Approve') {
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        }
        if (event.target.id == 'Reject') {
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
        }
        this.onSubmit(event.target.id);
      }
    }
  }

  ngAfterContentChecked() {
    if (this.elementRef.nativeElement.querySelector('#myTemplate')) {
      this.showLoader = true;
      this.elementRef.nativeElement
        .querySelector('#myTemplate')
        .addEventListener('click', this.openAlert.bind(this));
    }
  }

  sendback() {
    if (
      this.grnForm.value.workflow_remark != '' ||
      this.grnForm.value.workflow_remark != null ||
      this.grnForm.value.error_code_id != '' ||
      this.grnForm.value.error_code_id != null
    ) {
      (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
    }
  }

  setFile(event: any, i: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';
    const fileToUpload = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(fileToUpload);
    reader.onload = (_event) => {
      this.previewImage1 = reader.result;
    };
    const fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
    if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
      this.previewImageType1 = true;
    }
    this.fileNameKYC = fileToUpload['name'];
    const compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];
    if (compare_file_type.includes(fileFormat)) {
      const formData = new FormData();
      formData.append('uploadedFile', fileToUpload);
      formData.append('folder_name', 'kycuploads');
      this.fileKYC = formData;
      this.showFileLoader = true;
      this.uploadStatus = '';
      this.grnService.saveFile(this.fileKYC).subscribe({
        next: (data: any) => {
          const files3path = data['s3_file_path'];

          this.formArrgate
            .at(i)
            .get('weighing_slip_attachment')
            ?.setValue(files3path);
          this.uploadStatus = 'Uploaded';
          Swal.fire('File Uploaded Successfully!');
          this.showFileLoader = false;
        },
        error: (e) => {
          this.uploadStatus = '';
          this.fileNameKYC = '';
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    } else {
      this.uploadStatus = '';
      this.fileNameKYC = '';
      this.fileInputVar.nativeElement.value = '';
      Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onSubmit(btn: any) {
    console.log(this.grnForm);
    this.showLoader = true;
    if (this.grnForm.invalid) {
      console.log('invalid');

      this.showLoader = false;
      // this.showSwalmessage('Please Fill All Required Fields', '', 'error', false);
    } else {
      const payload = this.grnForm.getRawValue();

      // console.log(payload, 'PAYYYYYYYYYYYYY');
      // const poRefId = payload.po_ref_id;
      // console.log('poRefIdddddddddd: ', poRefId);

      // if (poRefId === undefined) {
      //   payload.po_ref_id = null;
      // }

      // console.log(payload, 'PAYYYYYYYYYYYYY');

      const val = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );
      payload.transaction_date = val;
      const val1 = this.datepipe.transform(
        payload.supplier_bill_date,
        'yyyy-MM-dd'
      );
      payload.supplier_bill_date = val1;

      payload.txn_currency = this.txnCurrency[0]['id'];
      payload.base_currency = this.txnCurrency[0]['id'];

      const gridRow1 = payload.grn_details;

      for (let j = 0; j < gridRow1.length; j++) {
        console.log(gridRow1);
        gridRow1[j]['conversion_rate'] = 1;
        gridRow1[j]['txn_currency'] = this.txnCurrency[0]['id'];
      }
      payload.workflow_status = btn;
      console.log('----', payload);
      this.handleSave.emit(payload);
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.grnForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
    this.grnForm.get('transaction_date')?.setValue(this.currentDate);
  }

  // dateNotInPastValidator(control: FormControl): { [key: string]: any } | null {
  //   const currentDate = new Date();
  //   const selectedDate = new Date(control.value);
  //   if (selectedDate < currentDate) {
  //     return { 'backdatedEntry': true };
  //   }

  //   return null;
  // }

  supplierTypeChnage(val: any) {
    this.grnService.getAllMasterData('PO Supplier Type').subscribe({
      next: (data: any) => {
        this.SupplierTypeData = data;
        for (let i = 0; i < this.SupplierTypeData.length; i++) {
          if (this.SupplierTypeData[i]['id'] == val) {
            if (this.SupplierTypeData[i]['master_key'] == 'Supplier') {
              this.grnService
                .getDynamicData('supplier', 'company_name')
                .subscribe({
                  next: (data: any) => {
                    this.SupplierData = data;
                  },
                  error: (e) => {
                    this.showSwalMassage(
                      this.errorCodes.getErrorMessage(JSON.parse(e).status),
                      'error'
                    );
                  },
                });
            } else if (this.SupplierTypeData[i]['master_key'] == 'Customer') {
              this.grnService
                .getDynamicData('customer', 'company_name')
                .subscribe({
                  next: (data: any) => {
                    this.SupplierData = data;
                  },
                  error: (e) => {
                    this.showSwalMassage(
                      this.errorCodes.getErrorMessage(JSON.parse(e).status),
                      'error'
                    );
                  },
                });
            } else if (this.SupplierTypeData[i]['master_key'] == 'Farmer') {
              this.grnService.getDynamicData('farmer', 'name').subscribe({
                next: (data: any) => {
                  this.SupplierData = data;
                },
                error: (e) => {
                  this.showSwalMassage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    'error'
                  );
                },
              });
            } else if (this.SupplierTypeData[i]['master_key'] == 'Agent') {
              this.grnService.getDynamicData('agent', 'agent_name').subscribe({
                next: (data: any) => {
                  this.SupplierData = data;
                },
                error: (e) => {
                  this.showSwalMassage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    'error'
                  );
                },
              });
            }
          }
        }
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  grn_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      batch_no: [
        '',
        [Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9]{1,50}$')],
      ],
      no_of_bags: [
        { value: '', disabled: true },
        [Validators.pattern(/^(?!-)\d+$/), Validators.pattern(/^[1-9]\d*$/)],
      ],
      quantity: { value: '', disabled: true }, //[Validators.required, Validators.pattern("^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$")]],
      uom: [{ value: '', disabled: true }, [Validators.required]],
      hsn_sac_no: [{ value: '', disabled: true }],
      rate: [
        '',
        [
          Validators.required,
          Validators.pattern('^(?!-)[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
        ],
      ],
      received_qty: [
        { value: '', disabled: true },
        [
          Validators.required,
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$'),
        ],
      ],
      accepted_qty: [
        '',
        [
          Validators.required,
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$'),
        ],
      ],
      // sample_qty: [0, [Validators.pattern("^[+]?([0-9]+(?:[.][0-9]*)?|\\.[0-9]+)$")]],
      rejected_qty: [
        0,
        { value: '', disabled: true },
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
        ],
      ],
      txn_currency: [''],
      conversion_rate: [1],
      txn_currency_amount: [0, [Validators.required]],
      base_currency_amount: [1],
      weighing_slip_attachment: [''],
      base_currency: [1],
      created_by: [localStorage.getItem('user_id')],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      href_attachment_path: [''],
      total_gross_weight: ['0'],
      total_tare_weight: ['0'],
    });
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, index: number) => {
      console.log('element: ', element);
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: { value: element.item_ref_id, disabled: true },
          batch_no: element.batch_no,
          no_of_bags: { value: element.no_of_bags, disabled: true },
          quantity: { value: element.quantity, disabled: true },
          uom: { value: element.alternate_uom, disabled: true },
          hsn_sac_no: { value: element.hsn_sac_no, disabled: true },
          rate: element.rate,
          received_qty: {
            value: element.alternate_received_qty,
            disabled: true,
          },
          accepted_qty: element.alternate_accepted_qty,
          rejected_qty: element.alternate_rejected_qty,
          txn_currency: element.txn_currency,
          conversion_rate: element.conversion_rate,
          txn_currency_amount: element.txn_currency_amount,
          base_currency_amount: element.txn_currency_amount,
          base_currency: element.base_currency,

          weighing_slip_attachment: element.weighing_slip_attachment,
          created_by: element.created_by,
          tenant_id: element.tenant_id,
          is_qc_generated: element.is_qc_generated,
          qc_status: element.qc_status,
          total_gross_weight: element.total_gross_weight,
          total_tare_weight: element.total_tare_weight,
        })
      );
      // this.purchaseOrderChange(element.alternate_balance_qty)

      this.grnService
        .getDynamicData('item_rate_commision_details', element.item_ref_id)
        .subscribe({
          next: (data: any) => {
            const gridRow = (<FormArray>this.grnForm.get('grn_details')).at(
              index
            );
            if (data.length > 0) {
              gridRow.get('rate')?.setValue(data[0]['item_rate']);
            } else if (data.length == 0) {
              gridRow.get('rate')?.setValue('');
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

      this.grnService.getCurrencyData().subscribe({
        next: (data: any) => {
          this.txnCurrency = data;
          this.txnCurrency = data.filter((d: any) => d.country_name == 'India');
          this.grnForm
            .get('base_currency')
            ?.setValue(this.txnCurrency[0]['id']);
          const gridRow = (<FormArray>this.grnForm.get('grn_details')).at(
            index
          );
          gridRow.get('base_currency')?.setValue(this.txnCurrency[0]['id']);
        },
      });
    });

    return formArray;
  }

  get formArrgate() {
    return this.grnForm.get('grn_details') as UntypedFormArray;
  }

  calculateAmount(grnForm: FormGroup, index: number) {
    const detailControl = (grnForm.get('grn_details') as FormArray).at(index);

    if (detailControl) {
      const acceptedQtyControl = detailControl.get('accepted_qty');
      const RateControl = detailControl.get('rate');
      const txnCurrencyAmountControl = detailControl.get('txn_currency_amount');

      if (acceptedQtyControl && RateControl && txnCurrencyAmountControl) {
        const acceptedQty = acceptedQtyControl.value;
        const Rate = RateControl.value;

        if (acceptedQty !== null && Rate !== null) {
          const amount = acceptedQty * Rate;
          txnCurrencyAmountControl.setValue(amount.toFixed(2));
        }
      }
    }
  }

  calculateQuantity(grnForm: FormGroup, index: number) {
    const detailQuantity = (grnForm.get('grn_details') as FormArray).at(index);

    if (detailQuantity) {
      const receivedQtyControl = detailQuantity.get('received_qty');
      const acceptedQtyControl = detailQuantity.get('accepted_qty');
      const rejectedQtyControl = detailQuantity.get('rejected_qty');
      const quantityControl = detailQuantity.get('quantity');
      const RateControl = detailQuantity.get('rate');

      if (
        receivedQtyControl &&
        acceptedQtyControl &&
        rejectedQtyControl &&
        quantityControl &&
        RateControl
      ) {
        const receivedQty = receivedQtyControl.value;
        const acceptedQty = acceptedQtyControl.value;
        const quantity = quantityControl.value;

        if (receivedQty !== null && acceptedQty !== null && quantity !== null) {
          if (acceptedQty <= 0) {
            acceptedQtyControl.setErrors({ greaterThanZero: true });
            rejectedQtyControl.setValue('');
          } else if (acceptedQty > quantity) {
            acceptedQtyControl.setErrors({ greaterThanPO: true });
            rejectedQtyControl.setValue('');
          } else if (acceptedQty > receivedQty) {
            acceptedQtyControl.setErrors({ greaterThanGp: true });
            rejectedQtyControl.setValue('');
          } else {
            acceptedQtyControl.setErrors(null);
            let amount = receivedQty - acceptedQty;
            if (amount < 0) {
              amount = 0;
            }
            rejectedQtyControl.setValue(amount.toFixed(4));
          }

          if (detailQuantity) {
            const acceptedQtyControl = detailQuantity.get('accepted_qty');
            const RateControl = detailQuantity.get('rate');
            const txnCurrencyAmountControl = detailQuantity.get(
              'txn_currency_amount'
            );
            const baseCurrencyAmountControl = detailQuantity.get(
              'base_currency_amount'
            );
            const baseCurrencyControl = detailQuantity.get('base_currency');

            if (
              acceptedQtyControl &&
              RateControl &&
              txnCurrencyAmountControl &&
              baseCurrencyAmountControl &&
              baseCurrencyControl
            ) {
              const acceptedQty = acceptedQtyControl.value;
              const Rate = RateControl.value;

              if (acceptedQty !== null && Rate !== null) {
                const amount = acceptedQty * Rate;
                txnCurrencyAmountControl.setValue(amount.toFixed(2));
                baseCurrencyAmountControl.setValue(amount.toFixed(2));
              }
            }
          }
        }
      }
    }
  }
  onAcceptedQtyChange(grnForm: FormGroup, index: number) {
    const detailQuantity = (grnForm.get('grn_details') as FormArray).at(index);

    if (detailQuantity) {
      const acceptedQtyControl = detailQuantity.get('accepted_qty');
      const rejectedQtyControl = detailQuantity.get('rejected_qty');

      if (acceptedQtyControl && rejectedQtyControl) {
        const acceptedQty = acceptedQtyControl.value;

        if (acceptedQty === null || acceptedQty === '') {
          rejectedQtyControl.setValue('');
        }
      }
    }
  }

  addNewRow() {
    this.formArrgate.push(this.grn_details());
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrgate.removeAt(index);
      return true;
    }
  }

  supplierNameChange(event: any) {
    const supplier = this.SupplierData.filter(
      (x: any) => x.id == event.value
    )[0];
    const supplier_name = supplier['key'].split('-')[1];
    const sn = supplier_name.split('/')[1];
    this.grnForm.get('supplier_ref_type')?.setValue(sn);
    // this.poRefId = this.poRefId.filter((d: any) => d.supplier_ref_id == event.value)

    this.grnService.getGetPassNumber(event.value, '').subscribe({
      next: (data: any) => {
        this.SearchData = data['gate_pass'];
        this.gatepassData = data['gate_pass'];
        this.poRefId = data['Purchase_order'];
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  purchaseOrderChange(event: any) {
    console.log(event.value);
    if (event.value == undefined) {
      if (this.rowData) {
        const grnDetailsArray = this.grnForm.get('grn_details') as FormArray;
        console.log(grnDetailsArray, 'HELLOWWWWWWWWWWWWWWWWWWWWW');

        grnDetailsArray.controls.forEach((control) => {
          control
            .get('quantity')
            ?.setValue(control.get('alternate_quantity')?.value);
        });
        console.log(this.rowData, 'ROWWW DATA');
        const rowDetailsArray = this.rowData.grn_details;
        console.log(rowDetailsArray, 'MMMMMMMMM HEREEEEEEEEEE');
        if (rowDetailsArray) {
          //this.isPOSelected = false;
          this.grnForm.setControl(
            'grn_details',
            this.setExistingArray(rowDetailsArray)
          );
        } else {
          console.log('yess');
          this.isPOSelected = false;
          this.gatepassChange(this.grnForm.get('gate_pass_ref_id')?.value);
        }
      } else {
        this.isPOSelected = false;
        this.gatepassChange(this.grnForm.get('gate_pass_ref_id')?.value);
        console.log(
          this.gatepassChange,
          'WHAT VALUES ARE HEREEEEEE!!!!!!!!!!!'
        );

        // const grnDetailsArray = this.grnForm.get('grn_details') as FormArray;
        // grnDetailsArray.controls.forEach((control) => {
        //     control.get('quantity')?.setValue(control.get('alternate_quantity')?.value);
        // });
      }
    } else {
      this.isPOSelected = true;
      const d = {
        po_ref_id: event.value,
      };
      this.grnService.getFpcPodetails(d).subscribe({
        next: (data: any) => {
          console.log(data, 'WHAT DATA IS HERE');

          const grnDetailsArray = this.grnForm.get('grn_details') as FormArray;
          console.log('grnDetailsArrayyyyyyyyyyyyyyyyyyyy: ', grnDetailsArray);
          for (let ind = 0; ind < grnDetailsArray.length; ind++) {
            grnDetailsArray.at(ind).get('item_ref_id')?.enable();
            const item_rfid = grnDetailsArray.at(ind).get('item_ref_id')?.value;

            let isMatchingItemFound = false;

            for (let item = 0; item < data.length; item++) {
              if (data[item]['item_ref_id'] == item_rfid) {
                const qty = grnDetailsArray.at(ind).get('received_qty')?.value;
                console.log('received_qty: ', qty);
                console.log(
                  'alternate_balance_qty: ',
                  data[item]['alternate_balance_qty']
                );
                if (data[item]['alternate_balance_qty'] < qty) {
                  this.showSwalMassage(
                    'Gate Pass quantity should be less than PO quantity. Update PO quantity ',
                    'warning'
                  );
                  this.grnForm.get('po_ref_id')?.setValue('');
                  return;
                } else {
                  grnDetailsArray.at(ind).get('rate')?.enable();
                  grnDetailsArray
                    .at(ind)
                    .get('rate')
                    ?.setValue(data[item]['rate']);
                  grnDetailsArray
                    .at(ind)
                    .get('quantity')
                    ?.setValue(data[item]['alternate_balance_qty']);
                  grnDetailsArray.at(ind).get('rate')?.disable();

                  isMatchingItemFound = true;
                  break;
                }
              }
            }

            if (!isMatchingItemFound) {
              this.showSwalMassage(
                'PO Item Does Not Match With Gatepass Item',
                'warning'
              );
              this.grnForm.get('po_ref_id')?.setValue('');
              return;
            }
            grnDetailsArray.at(ind).get('item_ref_id')?.disable();
          }
        },
      });
    }
  }

  gatepassChange(event: any) {
    this.poData = this.poRefId;
    this.grnService.getGatePassDetails(event).subscribe({
      next: (data: any) => {
        console.log(data, 'PPPPPPPPPPP');

        console.log(data);
        this.grnForm.get('gate_pass_no')?.setValue(data[0].gate_pass_no);
        this.grn_details_data = data.gate_pass_details;
        const gateDetailItemRow = data.filter(function (data: any) {
          return data;
        });

        if (gateDetailItemRow.length >= 1) {
          gateDetailItemRow.forEach((ele: any) => {
            ele['quantity'] = ele.alternate_quantity;
          });
          this.grnForm.setControl(
            'grn_details',
            this.setExistingArray(gateDetailItemRow)
          );
          const grnDetailsArray = this.grnForm.get('grn_details') as FormArray;
          for (let ind = 0; ind < grnDetailsArray.length; ind++) {
            grnDetailsArray.at(ind).get('item_ref_id')?.disable();
            grnDetailsArray.at(ind).get('hsn_sac_no')?.disable();
            grnDetailsArray.at(ind).get('rate')?.disable();
          }
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }
}
