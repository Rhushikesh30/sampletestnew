// import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
// import { UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators, FormArray, FormGroup, FormControl } from '@angular/forms';
// import { ErrorCodes } from 'src/app/shared/codes/error-codes';
// import { FarmerbillService } from 'src/app/shared/services/farmerbill.service';
// import Swal from 'sweetalert2';
// import { DatePipe, formatDate } from '@angular/common';
// import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
// import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
// import { el } from '@fullcalendar/core/internal-common';
// import { number } from 'echarts';
// import { elementAt } from 'rxjs';
// import { ActivatedRoute, Router } from '@angular/router';
// import { SupplierBillReportService } from 'src/app/shared/services/supplier-bill-report.service';

// @Component({
//   selector: 'app-farmer-bill-details',
//   templateUrl: './farmer-bill-details.component.html',
//   styleUrls: ['./farmer-bill-details.component.scss'],
//   providers: [ErrorCodes, DatePipe]
// })
// export class FarmerBillDetailsComponent {
//   @ViewChild('fileInput')
//   fileInputVar!: ElementRef;

//   @Input() rowData!: [];
//   @Input() submitBtn!: boolean;
//   @Input() showLoader!: boolean;
//   @Output() handleSave = new EventEmitter<any>();
//   @Output() handleCancel = new EventEmitter<any>();

//   farmerForm!: UntypedFormGroup;

//   showFileLoader = false;
//   showReset = true;
//   cancelFlag = true;
//   FederationView = false
//   btnVal = 'Submit';

//   farmerData: any[]=[];
//   qcData: any=[];
//   currentDate: string;
//   todayDate = new Date().toJSON().split("T")[0];
//   qcDataForSelectedFarmer: any;
//   grnData: any=[];
//   poData:any=[];
//   itemData:any=[];
//   uomData:any=[];
//   allqc_data:any=[];
//   qc_details_data: any;
//   roleName:any = ''
//   allgrn_data: any;
//   vehicalNo:any = ''
//   allBillData: any;


//         transaction_date:any;
//         purchase_booking_ref_id:any; 
//         farmer_bill_no:any; 
//         transaction_ref_no:any; 
//         gate_pass_ref_id:any;
//         grn_ref_id:any; 
//         vehicle_number:any; 
//         address:any; 
//         contact_number:any; 
//         labour_charges:any; 
//         total_transaction_amount:any; 
//         payment_status:any; 
//         item_ref_id:any; 
//         no_of_bags:any; 
//         quantity:any; 
//         uom:any; 
//         weight_of_empty_bags:any; 
//         net_quantity:any; 
//         rate:any; 
//         paid_amount:any; 
//         final_rate:any; 
//         weight_detail:any=[]; 
//         qc_parameter_value:any; 
//         qc_value:any; 
//         date:any; 
//         payment_no:any; 
//         amount:any; 
//         mode:any; 


//   constructor(private formBuilder: UntypedFormBuilder, private farmerbillService: FarmerbillService , private route: ActivatedRoute,
//     private errorCodes: ErrorCodes, private datepipe: DatePipe, private dynamicFormService: DynamicFormService,
//     private encDecService: EncrDecrService) { 
      
//       this.currentDate = formatDate(new Date(), 'yyyy-MM-dd', 'en'); 

      
//     }

//     ngOnInit(): void {

//       if (this.route.snapshot.queryParams['id']) {
//         console.log("HHHHHHHHHHHHHHHHHH");
//         this.getAllData(this.route.snapshot.queryParams['id']);
//       }

//       this.initializeForm()

//     if (!Array.isArray(this.rowData)) {
//       this.showReset = false;
//       this.viewRecord(this.rowData);
//     }
 
//     }

//     getAllData(id:any){

//       this.farmerbillService.getSupplierBillData().subscribe({
//         next: (data: any) => {
//           this.allBillData = data;

//           console.log(this.allBillData,"---------------alll dataaaaa");
          
//         this.transaction_date= data.transaction_date;
//         this.purchase_booking_ref_id= data.purchase_booking_ref_id;
//         this.farmer_bill_no =data.farmer_bill_no;
//         this.transaction_ref_no=data.transaction_ref_no;
//         this.grn_ref_id=data.grn_ref_id; 
//         this.gate_pass_ref_id=data.gate_pass_ref_id;
//         this.vehicle_number=data.vehicle_number;
//         this.address=data.address;
//         this.contact_number=data.contact_number;
//         this.labour_charges=data.labour_charges;
//         this.total_transaction_amount=data.total_transaction_amount;
//         this.payment_status=data.payment_status; 
//         this.item_ref_id=data.item_ref_id;
//         this.no_of_bags=data.no_of_bags;
//         this.quantity=data.quantity;
//         this.uom=data.uom; 
//         this.weight_of_empty_bags=data.weight_of_empty_bags; 
//         this.net_quantity=data.net_quantity;
//         this.rate=data.rate; 
//         this.paid_amount=data.paid_amount;
//         this.final_rate=data.final_rate; 
//         this.weight_detail=data.weight_detail;
//         this.qc_parameter_value=data.qc_parameter_value;
//         this.qc_value=data.qc_value; 
//         this.date=data.date; 
//         this.payment_no=data.payment_no;
//         this.amount=data.amount; 
//         this.mode=data.mode;
          

//         }
//       });
     

//     }


  
//     initializeForm(){
//       this.farmerForm = this.formBuilder.group({
//         id:[''],
        
//         transaction_date:[''],
//         purchase_booking_ref_id:[''],
//         farmer_bill_no:[''],
//         transaction_ref_no:[{ value: '', disabled: true }],
//         gate_pass_ref_id:[''],
//         grn_ref_id:[''],
//         vehicle_number:[{ value: '', disabled: true }],
//         address:[''],
//         contact_number:[''],
//         labour_charges:[''],
//         total_transaction_amount:[''],
//         payment_status:[''],

      
//         fpc_farmer_bill: this.formBuilder.array([this.fpc_farmer_bill()]),


//       });
//     }

//     viewRecord(edata: any) {

//       this.farmerForm.patchValue({
//         id: edata.id,
//         transaction_date: edata.transaction_date,
//         purchase_booking_ref_id:edata.purchase_booking_ref_id,
//         farmer_bill_no:edata.farmer_bill_no,
//         transaction_ref_no:edata.transaction_ref_no,
//         gate_pass_ref_id:edata.gate_pass_ref_id,
//         grn_ref_id:edata.grn_ref_id,
//         vehicle_number:edata.vehicle_number,
//         address:edata.address,
//         contact_number:edata.contact_number,
//         labour_charges:edata.labour_charges,
//         total_transaction_amount:edata.total_transaction_amount,
//         payment_status:edata.payment_status,

        
     

    
//       })
     
//       if (this.submitBtn) {
//         this.btnVal = 'Update';
//       }
//       else {
//         this.farmerForm.disable();
//       }
//       let gateDetailItemRow = edata.fpc_farmer_bill.filter(function (data: any) {
//         return data; 
//       });
//       if (gateDetailItemRow.length >= 1) {
//         this.farmerForm.setControl('fpc_farmer_bill', this.setExistingArray(gateDetailItemRow));
//       }
      
//     }

    
//     fpc_farmer_bill() {

//       return this.formBuilder.group({
//         id:[''],
//         item_ref_id:[{ value: '', disabled: true }],
//         no_of_bags:[],
//         quantity:[''],
//         uom:[],
//         weight_of_empty_bags:[''],
//         net_quantity:[''],
//         rate:[{ value: '', disabled: true }],
//         paid_amount:[''],
//         final_rate:[''],
//         weight_detail:[''],
//         qc_parameter_value:[''],
//         qc_value:[''],
//         date:[''],
//         payment_no:[''],
//         amount:[''],
//         mode:['']

        
//       });
//     }

//     setExistingArray(initialArray = []): UntypedFormArray {
//       const formArray: any = new UntypedFormArray([]);
//       initialArray.forEach((element: any) => {
//         formArray.push(this.formBuilder.group({

//           id:element.id,
//           farmer_bill_no:element.farmer_bill_no,
//           item_ref_id:element.item_ref_id,
//           no_of_bags: element.no_of_bags,
//           quantity:element.quantity,
//           uom:element.uom,
//           weight_of_empty_bags:element.weight_of_empty_bags,
//           rate:element.rate,
//           paid_amount:element.paid_amount,
//           final_rate:element.final_rate,
//           weight_detail:element.weight_detail,
//           qc_parameter_value:element.qc_parameter_value,
//           qc_value:element.qc_value,
//           date:element.date,
//           payment_no:element.payment_no,
//           amount:element.amount,
//           mode:element.mode        
          

        }));
      });
      
//       return formArray;
//     }

//     get formArrfarmer() {
//       return this.farmerForm.get('fpc_farmer_bill') as UntypedFormArray;
//     }


//     printComponent() {
//       window.print(); 
//     } 































//     ngOnDestroy(): void {
//       if (this.cancelFlag) {
//         this.farmerForm.reset();
//         this.handleCancel.emit(false);
//       }
//     }

    onSubmit() {
      if (this.farmerForm.invalid) {
        const invalid = [];
        const controls = this.farmerForm.controls;
          let fc: any = this.farmerForm.controls;
           let md = fc.controls;
    
//         for (const name in controls) {
//           if (controls[name].invalid) {
//             controls[name].markAsTouched();
        
//           }
//         }
//         return;
//       } 
//       else {
       
//         console.log(this.farmerForm.value);
        
//         this.farmerForm.value.transaction_date = this.datepipe.transform(this.farmerForm.value.transaction_date, 'yyyy-MM-dd')
    
//         var gridRow1 = (<FormArray>(this.farmerForm.get("fpc_farmer_bill")))   
//         for(let j=0;j<gridRow1.length;j++){
    
//           gridRow1.at(j).get('rate')?.enable();
//           gridRow1.at(j).get('uom')?.enable();
//           gridRow1.at(j).get('item_ref_id')?.enable();
//           gridRow1.at(j).get('no_of_bags')?.enable();
//           gridRow1.at(j).get('actual_qty')?.enable();
//         }
//         this.handleSave.emit(this.farmerForm.value);
//        }
//     }

//     onCancelForm() {
//       this.cancelFlag = false;
//       this.farmerForm.reset();
//       this.handleCancel.emit(false);
//     }
//     onResetForm() {
//       this.initializeForm();
//     }

//     showSwalMassage(massage: any, icon: any): void {
//       Swal.fire({
//         title: massage,
//         icon: icon,
//         timer: 2000,
//         showConfirmButton: false
//       });
//     }

//     addNewRow() {
//       this.formArrfarmer.push(this.fpc_farmer_bill());
//     }
    
//     deleteRow(index: number) {
//       if (index == 0) {
//         return false;
//       } else {
//         this.formArrfarmer.removeAt(index);
//         return true;
//       }
//     }

 
//     is_temporary_flag_change(val: any): void {
//       if (val.checked) {
//         this.btnVal = 'Save As Draft';
//         this.farmerForm.get('status')?.setValue('Temporary Save');
//         this.farmerForm.get('is_temporary_save')?.setValue(true);
//       } else {
//         this.btnVal = 'Permanent Save';
//         this.farmerForm.get('status')?.setValue('Permanent Save');
//         this.farmerForm.get('is_temporary_save')?.setValue(false);
//       }
//     }

//     calculateQuantity( index: number){
//       const detailQuantity = (this.farmerForm.get('fpc_farmer_bill') as FormArray).at(index);
    
//       if (detailQuantity) {
//         const itemQtyControl = detailQuantity.get('quantity');
//         const gunnyBagsQtyControl = detailQuantity.get('weight_of_empty_bags');
//         const actualQtyControl = detailQuantity.get('actual_qty');
    
//         if(itemQtyControl && gunnyBagsQtyControl && actualQtyControl){
//           const itemQty = itemQtyControl.value;
//           const gunnyQty = gunnyBagsQtyControl.value;
    
//           if(itemQty !== null && gunnyQty !== null){
//             const amount = itemQty - gunnyQty;
//             actualQtyControl.setValue(amount);
//           }
    
//         }
//       }
//     }

  
//     calculateAmount(farmerForm: FormGroup, index: number): number {
//       const detailControl = (farmerForm.get('fpc_farmer_bill') as FormArray).at(index);
    
//       if (detailControl) {
//         const actualQtyControl = detailControl.get('actual_qty');
//         const finalRateControl = detailControl.get('final_rate');
//         const totalAmountControl = detailControl.get('transaction_amount');
//         const laborControl = this.farmerForm.get('labour_charges');
//         const freightControl = this.farmerForm.get('freight');
    
//         if (actualQtyControl && finalRateControl && totalAmountControl &&laborControl && freightControl) {
//           const actualQty = parseFloat(actualQtyControl.value); 
//           const finalRate = parseFloat(finalRateControl.value); 
//           const labor = parseFloat(laborControl.value);
//           const freight = parseFloat(freightControl.value);
    
//           if (!isNaN(actualQty) && !isNaN(finalRate)) {
//             const amount = actualQty * finalRate;
//             totalAmountControl.setValue(amount.toFixed(2));
    
//             const rows = farmerForm.get('fpc_farmer_bill') as FormArray;
//             let totalAmount = 0;
//             let allSum;
    
//             for (const row of rows.controls) {
//               const transactionAmountControl = row.get('transaction_amount');
    
//               if (transactionAmountControl) {
//                 const transactionAmount = parseFloat(transactionAmountControl.value);
//                 if (!isNaN(transactionAmount)) {
//                   totalAmount += transactionAmount;
//                   allSum = (totalAmount - labor) + freight

//                   // farmerForm.get('total_transaction_amount')?.setValue(allSum);
//                   // farmerForm.get('total_paid_amount')?.setValue(allSum);

//                   if (!isNaN(allSum)) {
//                     farmerForm.get('total_transaction_amount')?.setValue(allSum);
//                     farmerForm.get('total_paid_amount')?.setValue(allSum);
//                   } else {
//                     farmerForm.get('total_transaction_amount')?.setValue('');
//                     farmerForm.get('total_paid_amount')?.setValue('');
//                   }

//                 }
//               }
//             }
    
//             return totalAmount;
//           }
//         }
//       }
    
//       return 0; 
//     }


//     onFarmerChange(val: any) { 
//       console.log(val);

//       this.farmerbillService.getAllQcData("quality-control").subscribe({
//         next: (data: any) => {
//           this.allqc_data = data;

//           if(data.length>0){
//             for(let i=0;i<data.length;i++){
//               data[i]['key']=data[i]['transaction_ref_no'] + '/' + data[i]['transaction_date']
//             }
//           }
//           this.qcData = data;
//           this.qcData = data.filter((qcitem: { supplier_ref_id: any; }) => qcitem.supplier_ref_id === val );


//         }, 
//         error: (e) => {
//           this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
//         }    
//       });

//     }

//     onQcChange(val : any){
//       console.log('oc valllllllllllll',val);
//       let qci:any = this.allqc_data.filter((d:any)=> d.id== val)
      

//       console.log(val);
      
//       this.farmerbillService.getQcDataById(val).subscribe({
//         next: (data: any) => { 
//           console.log(data);
          
//         this.qc_details_data = data.qc_details  
//         console.log("qcccccccc dataaaaaaaaaa",this.qc_details_data);
//         for(let ind=0 ; ind < this.qc_details_data.length;ind++){
//           if(ind >0){
//             this.addNewRow()
//           }
//           let gridRow = (<FormArray>(this.farmerForm.get("fpc_farmer_bill"))).at(ind)
//           gridRow.get('item_ref_id')?.setValue(this.qc_details_data[ind]['item_ref_id'])
//           gridRow.get('quantity')?.setValue(this.qc_details_data[ind]['accepted_qty'])
//           gridRow.get('no_of_bags')?.setValue(this.qc_details_data[ind]['no_of_bags'])
//           gridRow.get('uom')?.setValue(this.qc_details_data[ind]['uom'])
//           gridRow.get('rate')?.setValue(this.qc_details_data[ind]['rate'])
//           gridRow.get('hsn_sac_no')?.setValue(this.qc_details_data[ind]['hsn_sac_no'])

//           }
//         let grid = (<FormArray>(this.farmerForm.get("fpc_farmer_bill")))
//         if(grid.length > data.length){
//           for(let g = data.length ; g < grid.length ; g++){
//             this.deleteRow(g)
//           }
//         } 
//         },
//         error: (e) => {
//           this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
//         }
//       });
     
     

//       this.farmerbillService.getAllGrnData("grn").subscribe({
//         next: (data: any) => {
//           console.log("hhhhhhhhhhhh",data,qci[0]['grn_ref_id']);
          
//           this.grnData = data.filter((d:any)=> d.id == qci[0]['grn_ref_id'])  
//         }, 
//         error: (e) => {
//           this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
//         }    
//       });

//       this.farmerbillService.getAllPurchaseData("purchase_order").subscribe({
//         next: (data: any) => {
//           console.log("getAllPurchaseDatalklklk",data);
          
//           this.poData = data.filter((d:any)=> d.id == qci[0]['po_ref_id'])
//           console.log("chekc for poData",this.poData);  
//         }, 
//         error: (e) => {
//           this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
//         }    
//       });

//     }


//     onGrnChange(val: any) { 
//       this.farmerbillService.getGrnDataById(val).subscribe({
//         next: (data: any) => {
//           this.vehicalNo = data['vehicle_no'];
//           console.log("grnnnnnnnnnnn vehicalNo",this.vehicalNo);
//           this.farmerForm.get('vehicle_number')?.setValue(this.vehicalNo)
//         }, 
//         error: (e) => {
//           this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
//         }    
//       });

//     }






    
//   }

    
    
  

  





 