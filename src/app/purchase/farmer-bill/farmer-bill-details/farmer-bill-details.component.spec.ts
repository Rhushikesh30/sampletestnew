import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerBillDetailsComponent } from './farmer-bill-details.component';

describe('FarmerBillDetailsComponent', () => {
  let component: FarmerBillDetailsComponent;
  let fixture: ComponentFixture<FarmerBillDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmerBillDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FarmerBillDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
