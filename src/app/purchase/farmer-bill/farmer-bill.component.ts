// import { DataSource, SelectionModel } from '@angular/cdk/collections';
// import { formatDate } from '@angular/common';
// import { HttpClient } from '@angular/common/http';
// import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// import { MatMenuTrigger } from '@angular/material/menu';
// import { MatPaginator } from '@angular/material/paginator';
// import { MatSort } from '@angular/material/sort';
// import { MatTableDataSource } from '@angular/material/table';
// import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
// import { AdvanceTableService } from 'src/app/advance-table/advance-table.service';
// import { Fpc } from 'src/app/core/models/fpc.model';
// import { RoleSecurityService } from 'src/app/core/service/role-security.service';
// import { TableElement } from 'src/app/shared/TableElement';
// import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
// import { ErrorCodes } from 'src/app/shared/codes/error-codes';
// import { FarmerbillService } from 'src/app/shared/services/farmerbill.service';
// import { TableExportUtil } from 'src/app/shared/tableExportUtil';
// import Swal from 'sweetalert2';

// @Component({
//   selector: 'app-farmer-bill',
//   templateUrl: './farmer-bill.component.html',
//   styleUrls: ['./farmer-bill.component.scss'],
//   providers: [ErrorCodes]
// })
// export class FarmerBillComponent  extends UnsubscribeOnDestroyAdapter implements OnInit{

//     @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
//     @ViewChild(MatSort, { static: true }) sort!: MatSort;
//     @ViewChild('filter', { static: true }) filter!: ElementRef;
//     resultData!: [];
//     displayedColumns = [
//       'actions',
//       'transaction_date',
//       'transactionRef_no',
//       'po_ref_id',
//       // 'supplier_ref_id',
//       // // 'item_ref_id',
//       // 'quantity',
//       // 'no_of_bags'
//     ];
//     renderedData: any = [];
//     screenName = 'FarmerBill';
//     submitBtn = true;
//     showLoader = false;
//     rowData: any = [];
//     listDiv: boolean = false;
//     showList: boolean = true;
//     sidebarData: any;
//     dataSource!: ExampleDataSource;
//     roleName:any = ''

//     exampleDatabase?: FarmerbillService;
//     //  dataSource!: ExampleDataSource;
//     selection = new SelectionModel<any>(true, []);
//     id?: number;
//     advanceTable?: any;
//     contextMenu?: MatMenuTrigger;
//     contextMenuPosition = { x: '0px', y: '0px' };

//     constructor(private roleSecurityService: RoleSecurityService, private farmerbillService: FarmerbillService,
//       private errorCodes: ErrorCodes, public httpClient: HttpClient) { super() }

//       ngOnInit(): void {
//         let userId = localStorage.getItem('user_id');
//         let role: any = localStorage.getItem('roles');
//         let roles = JSON.parse(role);
//         let rolename = roles.map((a:any) => a.role_name)
//         this.roleName = rolename[0]
//         this.roleSecurityService.getAccessLeftPanel(userId,'Purchase').subscribe({
//           next: (data: any) => {
//             this.sidebarData = data[0];
//             console.log(this.sidebarData)
//           },
//           error: (e) => {
//             this.showSwalmessage(this.errorCodes.getErrorMessage(JSON.parse(e).status), '', 'error', false);
//           }
//         });

//         this.refresh()
//       }

//       editViewRecord(row: any, flag: boolean) {

//             console.log(row)
//             this.rowData = row;
//             this.showList = false;
//             this.submitBtn = flag;
//             this.listDiv = true;
//             this.showLoader = false;
//           }
//       showFormList(item: boolean) {
//         if (item === false) {
//           this.listDiv = true;
//           this.showList = false;
//         }
//         else {
//           this.listDiv = false;
//           this.showList = true;
//         }
//       }

//       handleCancel(item: boolean) {
//         this.listDiv = item;
//         this.showList = true;
//         this.rowData = [];
//         this.submitBtn = true;
//       }
//       showSwalmessage(message: any, text: any, icon: any, confirmButton: any): void {
//         if (confirmButton == false) {
//           Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: false });
//         } else {
//           Swal.fire({ title: message, text: text, icon: icon, timer: 2000, showConfirmButton: true, confirmButtonText: "Yes", cancelButtonText: "No" });
//         }
//       }

//       handleSave(formValue: any) {
//         console.log(formValue,"hhhhhhhhhhhhh");

//         this.showLoader = true

//         this.farmerbillService.createFarmer(formValue).subscribe({
//           next: (data: any) => {

//             if (data['status'] == 1) {
//               this.showSwalmessage('Your record has been updated successfully!', '', 'success', false)
//             }
//             else if (data['status'] == 2) {
//               this.showSwalmessage('Your record has been added successfully!', '', 'success', false)
//             }
//             this.refresh();
//             this.showList = true;
//             this.listDiv = false;
//             this.showLoader = false
//           },
//           error: (e) => {
//             this.showLoader = false;
//             // this.showSwalmessage(this.errorCodes.getErrorMessage(JSON.parse(e).status), '', 'error', false);
//           }
//         });
//       }
//       onContextMenu(event: MouseEvent, item: any) {
//         event.preventDefault();
//         this.contextMenuPosition.x = event.clientX + 'px';
//         this.contextMenuPosition.y = event.clientY + 'px';
//         if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
//           this.contextMenu.menuData = { item: item };
//           this.contextMenu.menu.focusFirstItem('mouse');
//           this.contextMenu.openMenu();
//         }
//       }
//       refresh() {
//         this.exampleDatabase = new FarmerbillService(this.httpClient);
//         console.log(this.exampleDatabase)
//         this.dataSource = new ExampleDataSource(
//           this.exampleDatabase,
//           this.paginator,
//           this.sort
//         );
//         this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
//           () => {
//             if (!this.dataSource) {
//               return;
//             }
//             this.dataSource.filter = this.filter.nativeElement.value;
//           }
//         );
//       }
//     exportExcel() {
//       // key name with space add in brackets
//       const exportData: Partial<TableElement>[] =
//         this.dataSource.filteredData.map((x) => ({
//           // this.dataSource.map((x) => ({
//           'transaction_date': x.transaction_date,
//           'transactionRef_no': x.transaction_ref_no,
//           'po_ref_id': x.po_number,
//           // 'supplier_ref_id': x.supplier_name,
//           // 'item_ref_id': x.items,
//           // 'quantity': x.quantity,
//           // 'no_of_bags': x.no_of_bags
//         }));

//       TableExportUtil.exportToExcel(exportData, this.screenName);
//     }

//   }
//   export class ExampleDataSource extends DataSource<any> {
//     filterChange = new BehaviorSubject('');
//     get filter(): string {
//       return this.filterChange.value;
//     }
//     set filter(filter: string) {
//       this.filterChange.next(filter);
//     }
//     filteredData: any[] = [];
//     renderedData: any[] = [];
//     constructor(
//       public exampleDatabase: FarmerbillService,
//       public paginator: MatPaginator,
//       public _sort: MatSort
//     ) {
//       super();
//       console.log("ExampleDataSource",this.filterChange)///
//       this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));

//     }
//     connect(): Observable<any[]> {
//       const displayDataChanges = [
//         this.exampleDatabase.dataChange,
//         this.filterChange,
//          this.paginator.page,

//       ];

//       this.exampleDatabase.getAllAdvanceTables();
//       console.log("this.exampleDatabase.data ",this.exampleDatabase );

//       return merge(...displayDataChanges).pipe(
//         map(() => {

//           this.filteredData = this.exampleDatabase.data
//           .slice()
//           .filter((advanceTable: any) => {
//             const searchStr = (
//               advanceTable.transaction_date +
//               advanceTable.transaction_ref_no +
//               advanceTable.po_number
//               // advanceTable.supplier_name +
//               // advanceTable.items+
//               // advanceTable.quantity +
//               // advanceTable.no_of_bags
//             ).toLowerCase();
//             return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
//           });
//           const sortedData = this.filteredData.slice();
//           const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
//           this.renderedData = sortedData.splice(
//             startIndex,
//             this.paginator.pageSize
//           );
//           console.log(this.renderedData)
//           return this.renderedData;
//         })
//       );
//     }

//     disconnect() {
//       //disconnect
//     }

//     /** Returns a sorted copy of the database data. */
//     sortData(data: any[]): any[] {
//       if (!this._sort.active || this._sort.direction === '') {
//         return data;
//       }
//       return data.sort((a, b) => {
//         let propertyA: number | string = '';
//         let propertyB: number | string = '';
//         switch (this._sort.active) {
//           case 'id':
//             [propertyA, propertyB] = [a.id, b.id];
//             break;
//           case 'transaction_date':
//             [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
//             break;
//           case 'transactionRef_no':
//             [propertyA, propertyB] = [a.transactionRef_no, b.transactionRef_no];
//             break;
//           case 'po_ref_id':
//             [propertyA, propertyB] = [a.po_ref_id, b.po_ref_id];
//             break;
//           case 'supplier_ref_id':
//             [propertyA, propertyB] = [a.supplier_ref_id, b.supplier_ref_id];
//             break;
//           case 'item_ref_id':
//             [propertyA, propertyB] = [a.item_ref_id, b.item_ref_id];
//             break;
//           case 'quantity':
//             [propertyA, propertyB] = [a.quantity, b.quantity];
//             break;
//           case 'no_of_bags':
//             [propertyA, propertyB] = [a.no_of_bags, b.no_of_bags];
//             break;
//         }
//         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
//         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
//         return (
//           (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
//         );
//       });
//     }
//   }
