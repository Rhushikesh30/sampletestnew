import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  FormArray,
} from '@angular/forms';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { QcParameterDialogComponent } from '../qc-parameter-dialog/qc-parameter-dialog.component';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { QcService } from 'src/app/shared/services/qc.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';

import Swal from 'sweetalert2';
import { DatePipe, formatDate } from '@angular/common';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { GatePassService } from 'src/app/shared/services/gate-pass.service';
import { ErrorReportService } from 'src/app/shared/services/error-report.service';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-qc-details',
  templateUrl: './qc-details.component.html',
  styleUrls: ['./qc-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class QcDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  qcForm!: UntypedFormGroup;

  showFileLoader = false;
  showReset = true;
  cancelFlag = true;
  GridView = false;
  ViewLoader = false;
  btnVal = 'Submit';
  todayDate = new Date().toJSON().split('T')[0];
  GatePassData: any[] = [];
  SalesOrder: any[] = [];
  FarmerData: any[] = [];
  PurchaseOrder: any[] = [];
  itemDetailData: any[] = [];
  itemCodeData: any[] = [];
  uomData: any[] = [];
  CurrencyData: any[] = [];
  qc_params: any = [];
  materialStatus: any = [];
  grnDetails: any = [];
  SearchData: any[] = [];
  GRNSearchData: any[] = [];
  SearchGatePassData: any[] = [];
  ItemType: any[] = [];
  GrnData: any[] = [];
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  fileNameKYC = '';
  fileNameKYC1 = '';
  uploadStatus = '';
  uploadStatus1 = '';
  previewImage: any;
  previewImage1: any;
  previewImageType: any;
  previewImageType1: any;
  fileName = '';
  fileName1 = '';
  fileKYC!: FormData;
  fileKYC1!: FormData;
  fileAttchatment!: FormData;
  fileAttchatment1!: FormData;
  rate_ded = 0;
  po_base_rate: any;
  total_transaction_amount = 0;
  Base_Rate = 'Base Rate';
  empty_value = '';
  itemcode = '';
  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  @Input() cnt: any;
  @Input() screenName!: string;
  isDisable = true;
  errorCodeData: any;
  gatePassReadOnly = false;
  currentDate: string;
  @Input() viewOnSendback!: boolean;
  @Input() error!: any;

  HsnData: any[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private qcService: QcService,
    private datepipe: DatePipe,
    private dialog: MatDialog,
    private errorCodes: ErrorCodes,
    private dynamicFormService: DynamicFormService,
    private fpcService: FpcSetupService,
    private errorReport: ErrorReportService,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef,
    private gatePassService: GatePassService
  ) {
    this.currentDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  }

  ngOnInit(): void {
    console.log('in in');
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
      this.getAllData('save');
      console.log(this.viewOnSendback, this.isDisable, this.showLoader);
    }
    // this.qcForm.get('transaction_date')?.setValue(new Date())
    else {
      this.getAllData('');
      this.qcForm.get('transaction_date')?.setValue(this.currentDate);
    }
  }

  initializeForm() {
    this.qcForm = this.formBuilder.group({
      id: [''],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      transaction_date: ['', Validators.required],
      item_type_ref_id: ['', Validators.required],
      gate_pass_ref_id: ['', Validators.required],
      grn_ref_id: [''],
      so_ref_id: [''],
      supplier_ref_id: ['', Validators.required],
      po_ref_id: [''],
      total_txn_currency_amount: [{ value: '', disabled: true }],
      txn_currency: [''],
      conversion_rate: [1],
      remark: [''],
      attachment: [''],
      gate_pass_no: [''],
      transaction_ref_no: [''],
      workflow_remark: [''],
      error_code_id: [''],
      qc_details: this.formBuilder.array([this.qc_details()]),
      grn_no: [''],
      division_ref_id: [{ value: 0, disabled: true }],
      sale_type_ref_id: [''],
      department_ref_id: [''],
      location_ref_id: [''],
    });
  }

  getAllData(val: any) {
    console.log('val: ', val);
    this.qcService.getFarmerName('Farmer_Name', val).subscribe({
      next: (data: any) => {
        console.log('data: ', data);
        this.SearchData = data;
        this.FarmerData = data;
      },
    });
    this.qcService.getDynamicData('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
        const division_id = data.filter((d: any) => d.key == 'Business');
        if (division_id) {
          this.qcForm.get('division_ref_id')?.setValue(division_id[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.qcService.getDynamicData('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
        const dept_id = data.filter((d: any) => d.key == 'Trading');
        if (dept_id && Array.isArray(this.rowData)) {
          this.qcForm.get('department_ref_id')?.setValue(dept_id[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.qcService.getDynamicData('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
        const dept_id = data.filter((d: any) => d.key == 'B2B');
        if (dept_id && Array.isArray(this.rowData)) {
          this.qcForm.get('sale_type_ref_id')?.setValue(dept_id[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.qcService.getDynamicData('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
        if (data && Array.isArray(this.rowData)) {
          this.qcForm.get('location_ref_id')?.setValue(data[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.qcService.getAllMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.ItemType = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.qcService.getAllMasterData('Material Status').subscribe({
      next: (data: any) => {
        this.materialStatus = data;
      },
    });

    this.qcService
      .getFilterIData('GatePassFilter', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.itemCodeData = data;
        },
      });
    this.qcService
      .getFilterIData('GatePassFilter', 'item_master', 'name')
      .subscribe({
        next: (data: any) => {
          this.itemDetailData = data;
        },
      });
    this.qcService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.uomData = data;
      },
    });

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        const txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.qcForm.get('txn_currency')?.setValue(txn_currency[0]['id']);
      },
    });

    this.dynamicFormService.getDynamicData('error_code_mst', 'code').subscribe({
      next: (res: any) => {
        this.errorCodeData = res;
      },
    });

    this.dynamicFormService.getDynamicData('hsn_sac', 'hsn_sac_no').subscribe({
      next: (data: any) => {
        this.HsnData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  getuom(uom: any) {
    this.qcService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        const uom = data.filter((d: any) => d.id == uom);
        this.uomData = data;
        return uom[0]['key'];
      },
    });
  }

  viewRecord(edata: any) {
    this.qcForm.get('supplier_ref_id')?.disable();
    this.qcForm.get('gate_pass_ref_id')?.disable();
    this.qcForm.get('item_type_ref_id')?.disable();
    this.qcForm.get('grn_ref_id')?.disable();

    // if (edata.grn_ref_id) {
    //   this.qcForm.get('grn_ref_id')?.disable();
    // }
    console.log(this.btnVal, edata);
    this.isDisable = false;
    this.ItemTypeChange(edata.item_type_ref_id);
    this.getSalesOrder();
    this.FarmerNameSelected(edata.supplier_ref_id, edata.item_type_ref_id);

    this.qcForm.patchValue({
      id: edata.id,
      tenant_id: edata.tenant_id,
      transaction_date: edata.transaction_date,
      item_type_ref_id: edata.item_type_ref_id,
      gate_pass_ref_id: edata.gate_pass_ref_id,
      supplier_ref_id: edata.supplier_ref_id,
      transaction_ref_no: edata.transaction_ref_no,
      so_ref_id: edata.so_ref_id,
      po_ref_id: edata.po_ref_id,
      grn_ref_id: edata.grn_ref_id,
      total_txn_currency_amount: edata.total_txn_currency_amount,
      txn_currency: edata.txn_currency,
      attachment: edata.attachment,
      conversion_rate: edata.conversion_rate,
      remark: edata.remark,
      gate_pass_no: edata.gate_pass_no,
      workflow_remark: edata.workflow_remark,
      error_code_id: edata.error_code_id,
      grn_no: edata.grn_no,
      division_ref_id: edata.division_ref_id,
      department_ref_id: edata.department_ref_id,
      sale_type_ref_id: edata.sale_type_ref_id,
      location_ref_id: edata.location_ref_id,
    });

    const qcDetailItemRow = edata['qc_details'].filter(function (data: any) {
      return data;
    });
    if (qcDetailItemRow.length >= 1) {
      qcDetailItemRow.forEach((ele: any) => {
        ele.alternate_quantity = ele.alternate_qty;
      });
      this.qcForm.setControl(
        'qc_details',
        this.setExistingArray(qcDetailItemRow)
      );
    }

    this.qcService
      .getGrnAndGatePass(
        'GrnAndGatePass',
        edata.supplier_ref_id,
        edata.item_type_ref_id
      )
      .subscribe({
        next: (data: any) => {
          this.SearchGatePassData = data['gate_pass_data'];
          this.GatePassData = data['gate_pass_data'];
          console.log('GatePassData: ', this.GatePassData);
          this.GrnData = data['grn_data'];
          this.GRNSearchData = data['grn_data'];
        },
      });

    if (this.submitBtn) {
      this.ViewLoader = true;
      this.btnVal = 'Update';
      this.viewBtn = false;
      let employee_authority = localStorage.getItem('employee_authority');
      if (employee_authority == 'None' && edata.workflow_status == 'SendBack') {
        employee_authority = 'Maker';
      }
      if (
        (employee_authority == 'None' &&
          edata.workflow_status == 'Pending For Approval') ||
        edata.workflow_status == 'Initiated'
      ) {
        employee_authority = 'Checker';
      }
      this.roleSecurityService
        .getWorkflowBtn(employee_authority, this.screenName, edata.gate_pass_no)
        .subscribe((res: any) => {
          this.myTemplate = res;
          this.ViewLoader = false;
        });

      if (employee_authority == 'Checker') {
        this.qcForm.disable();
        this.qcForm.controls['workflow_remark'].enable();
        this.qcForm.controls['error_code_id'].enable();
        this.isDisable = false;
        console.log('isDisable: ', this.isDisable);
        this.viewOnSendback = true;
      }
      if (employee_authority == 'Maker') {
        this.isDisable = true;
        console.log('isDisable: ', this.isDisable);

        this.viewOnSendback = true;
      }
    } else {
      this.qcForm.disable();
    }
  }

  openAlert(event: any) {
    console.log('======', event.target.id, this.cnt);
    console.log(event);
    this.cnt = this.cnt + 1;
    if (this.cnt == 1) {
      if (event.target.id == 'Cancel') {
        this.onCancelForm();
        (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
        (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        (<HTMLInputElement>document.getElementById('SendBack')).disabled = true;
      } else if (event.target.id == 'SendBack') {
        this.showLoader = true;
        console.log(this.qcForm);
        if (
          this.qcForm.controls['workflow_remark'].value == null ||
          this.qcForm.controls['workflow_remark'].value == ''
        ) {
          console.log('if');
          this.qcForm.get('error_code_id')?.addValidators(Validators.required);
          this.qcForm.get('error_code_id')?.updateValueAndValidity();

          this.qcForm
            .get('workflow_remark')
            ?.addValidators(Validators.required);
          this.qcForm.get('workflow_remark')?.updateValueAndValidity();
          this.showSwalMassage(
            'Please Fill Sendback Remark and Sendback Code',
            'error'
          );
          this.cnt = 0;
          this.showLoader = false;
        } else {
          event.target.disabled = true;
          this.qcForm.get('workflow_remark')?.clearValidators();
          this.qcForm.get('error_code_id')?.clearValidators();
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          console.log('else');
          this.onSubmit(event.target.id);
        }
      } else {
        this.showLoader = true;
        event.target.disabled = true;
        this.qcForm.get('workflow_remark')?.clearValidators();
        this.qcForm.get('error_code_id')?.clearValidators();
        if (event.target.id == 'Approve') {
          // const i_tag = document.createElement('i');
          // i_tag.className = 'fa fa-spinner fa-spin';
          // (<HTMLInputElement>document.getElementById('Approve')).appendChild(i_tag);
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        }
        if (event.target.id == 'Reject') {
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
        }
        this.onSubmit(event.target.id);
      }
    }
  }

  ngAfterContentChecked() {
    if (this.elementRef.nativeElement.querySelector('#myTemplate')) {
      this.elementRef.nativeElement
        .querySelector('#myTemplate')
        .addEventListener('click', this.openAlert.bind(this));
    }
  }

  sendback(event: any) {
    console.log(event);
    console.log(this.qcForm);
    if (event == '' || event == undefined) {
      this.cnt = 0;
      console.log('ifff');
      this.qcForm.get('workflow_remark')?.clearValidators();
      this.qcForm.get('error_code_id')?.clearValidators();
      this.qcForm.get('workflow_remark')?.updateValueAndValidity();
      this.qcForm.get('error_code_id')?.updateValueAndValidity();
      (<HTMLInputElement>document.getElementById('Approve')).disabled = false;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = false;
    } else {
      (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
    }
  }

  sendback1(event: any) {
    console.log(event.value);
    if (event.value == '' || event.value == undefined) {
      this.cnt = 0;
      this.qcForm.get('workflow_remark')?.clearValidators();
      this.qcForm.get('error_code_id')?.clearValidators();
      this.qcForm.get('workflow_remark')?.updateValueAndValidity();
      this.qcForm.get('error_code_id')?.updateValueAndValidity();
      (<HTMLInputElement>document.getElementById('Approve')).disabled = false;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = false;
    } else {
      (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
    }
  }

  qc_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: [{ value: '', disabled: true }],
      no_of_bags: [{ value: '', disabled: true }, Validators.required],
      accepted_qty: ['', Validators.required],
      sample_qty: ['', Validators.required],
      grn_qty: [{ value: '', disabled: true }],
      rejected_qty: ['', Validators.required],
      base_rate: [{ value: '', disabled: true }],
      qc_deduction_per: [{ value: '', disabled: true }],
      qc_deduction_rate: [{ value: '', disabled: true }],
      net_rate: [{ value: '', disabled: true }],
      rate: [''],
      txn_currency_amount: [{ value: '', disabled: true }],
      hsn_sac_no: [{ value: '', disabled: true }],
      uom: [{ value: '', disabled: true }],
      qc_parameter_details: [],
      uom_name: [''],
    });
  }
  setExistingArray(initialArray: any): UntypedFormArray {
    this.GridView = true;
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: { value: element.item_ref_id, disabled: true },
          no_of_bags: { value: element.no_of_bags, disabled: true },
          grn_qty: { value: element.alternate_quantity, disabled: true },
          accepted_qty: element.alternate_accepted_qty,
          sample_qty: element.sample_qty,
          rejected_qty: {
            value: element.alternate_rejected_qty,
            disabled: true,
          },
          rate: element.rate,
          net_rate: parseFloat(element.qc_deduction_per || 0).toFixed(2),
          hsn_sac_no: { value: element.hsn_sac_no, disabled: true },
          uom: { value: element.alternate_uom, disabled: true },
          txn_currency_amount: {
            value: element.txn_currency_amount,
            disabled: true,
          },
          qc_deduction_rate: {
            value: parseFloat(element.qc_deduction_per || 0).toFixed(2),
            disabled: true,
          },
          qc_deduction_per: parseFloat(element.qc_deduction_per || 0).toFixed(
            2
          ),
          base_rate: { value: element.base_rate, disabled: true },
          qc_parameter_details: [element.qc_parameter_details],
          uom_name: element.uom_name,
        })
      );
    });

    return formArray;
  }

  get formArrcontact() {
    return this.qcForm.get('qc_details') as UntypedFormArray;
  }

  resetgridWithoutGatePass() {
    this.qcForm.get('grn_ref_id')?.setValue('');

    if (this.GridView && this.qcForm.get('qc_details')?.value) {
      this.qcForm.get('qc_details')?.reset();
      this.GridView = false;
      this.rowData = [];
    }
  }

  resetgridOnChange() {
    this.qcForm.get('grn_ref_id')?.setValue('');
    this.qcForm.get('gate_pass_ref_id')?.setValue('');

    if (this.GridView && this.qcForm.get('qc_details')?.value) {
      this.qcForm.get('qc_details')?.reset();
      this.GridView = false;
      this.rowData = [];
    }
  }

  ItemTypeChange(item_id: any) {
    this.resetgridOnChange();
    // this.qcForm.get('gate_pass_ref_id')?.disable();
    const supplier_id = this.qcForm.get('supplier_ref_id')?.value;
    if (supplier_id) {
      this.FarmerNameSelected(supplier_id, item_id);
    }
  }

  FarmerNameSelected(value: any, item_id: any) {
    this.resetgridOnChange();
    if (!item_id) {
      item_id = this.qcForm.get('item_type_ref_id')?.value;
      if (!item_id) {
        return;
      }
    }
    // this.qcService.GetPassNumber(value, '', item_id, '').subscribe({
    //   next: (data: any) => {
    //     this.SearchGatePassData = data['gate_pass'];
    //     this.GatePassData = data['gate_pass'];
    //     this.GrnData = data['grn'];
    //     this.GRNSearchData = data['grn'];
    //   },
    // });

    console.log(this.btnVal, this.submitBtn);
    if (this.btnVal == 'Submit' && this.submitBtn) {
      this.qcService
        .getGRNAndGatePassBySupplier('GRNAndGatePassBySupplier', value, item_id)
        .subscribe({
          next: (data: any) => {
            this.SearchGatePassData = data['gate_pass_data'];
            this.GatePassData = data['gate_pass_data'];
            console.log('GatePassData: ', this.GatePassData);

            this.GrnData = data['grn_data'];
            this.GRNSearchData = data['grn_data'];
          },
        });
    }
  }

  selectedgrn(value: any) {
    this.qc_params = [];
    this.getSalesOrder();
    this.qcForm.get('so_ref_id')?.setValue('');
    this.Base_Rate = 'Base Rate';
    const data = this.GrnData.filter((x: any) => x.id == value)[0]
      .transaction_ref_no;
    this.qcForm.get('grn_no')?.setValue(data);

    this.qcService.getGRNById(value).subscribe({
      next: (data: any) => {
        console.log(data);
        console.log(data[0].grn_details);
        const gpData = this.GatePassData.filter(
          (x: any) => x.id == data[0].gate_pass_ref_id
        )[0].transaction_ref_no;
        this.qcForm.get('gate_pass_no')?.setValue(gpData);
        this.qcForm.get('gate_pass_ref_id')?.setValue(data[0].gate_pass_ref_id);
        data[0].grn_details.forEach((ele: any) => {
          ele.alternate_quantity = ele.alternate_accepted_qty;
        });
        if (data[0].grn_details.length >= 1) {
          this.qcForm.setControl(
            'qc_details',
            this.setExistingArray(data[0].grn_details)
          );
        }
      },
    });
  }

  getSalesOrder() {
    this.qcService.getGrnData('getGrnData').subscribe({
      next: (data: any) => {
        this.PurchaseOrder = data['Purchase_Order'];
        this.SalesOrder = data['so_details'];
      },
    });
  }
  GatePassSelected(value: any) {
    this.resetgridWithoutGatePass();
    this.qc_params = [];
    this.getSalesOrder();
    this.qcForm.get('so_ref_id')?.setValue('');
    this.Base_Rate = 'Base Rate';

    const gpData = this.GatePassData.filter((x: any) => x.id == value)[0]
      .transaction_ref_no;
    this.qcForm.get('gate_pass_no')?.setValue(gpData);

    this.gatePassService.getAllGatePassDataByid(value).subscribe({
      next: (data: any) => {
        console.log(data);
        console.log(data[0].gate_pass_details);
        if (this.GrnData.length > 0) {
          console.log('if');
          const data1 = this.GrnData.filter(
            (x: any) => x.gate_pass_ref_id_id == value
          );
          console.log(data1);
          if (data1.length > 0) {
            this.qcForm.get('grn_ref_id')?.setValue(data1[0].id);
            this.selectedgrn(data1[0].id);
          } else {
            this.qcForm.get('grn_ref_id')?.setValue('');
          }
        }

        if (data[0].gate_pass_details.length >= 1) {
          data[0].gate_pass_details.forEach((ele: any) => {
            ele.alternate_accepted_qty = ele.alternate_quantity;
            ele.alternate_rejected_qty = 0;
            // ele.alternate_quantity =
          });
          this.qcForm.setControl(
            'qc_details',
            this.setExistingArray(data[0].gate_pass_details)
          );
        }
      },
    });
  }

  addNewRow() {
    this.formArrcontact.push(this.qc_details());
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontact.removeAt(index);
      return true;
    }
  }

  onSubmit(btn: any) {
    console.log(this.qcForm);
    if (this.qcForm.invalid) {
      // this.showSwalmessage(
      //   'Please Fill All Required Fields',
      //   '',
      //   'error',
      //   false
      // );

      return;
    } else {
      this.qcForm.getRawValue();
      this.qcForm.value['actual_qty'] = 0;
      this.qcForm.value['material_status'] = 0;

      const payload = this.qcForm.getRawValue();

      // const qcDetailsArray = this.qcForm.get('qc_details') as FormArray;
      // console.log('qcDetailsArray: ', qcDetailsArray);

      // qcDetailsArray.controls.forEach((qcDetailsGroup) => {
      //   const accQtControl = qcDetailsGroup.get('accepted_qty');

      //   if (accQtControl) {
      //     accQtControl.setValidators([Validators.required]);
      //     accQtControl.updateValueAndValidity();
      //   }
      // });

      //Accepted validation//
      // if (qcDetailsArray) {
      //   const accQt = qcDetailsArray.controls.map(
      //     (control) => control.get('accepted_qty')?.value
      //   );
      //   console.log(accQt, '@@@@@@@@@@@@');

      //   if (accQt && accQt.length > 0) {
      //     for (const acceptedQty of accQt) {
      //       console.log('acceptedQty: ', acceptedQty);

      //       if (acceptedQty === '' || acceptedQty === null) {
      //         this.showSwalMassage('Please Enter Accepted Qty', 'warning');
      //       }
      //     }
      //   }
      // }

      //
      const formattedDeductionPerArray = payload.qc_details.map(
        (qcDetail: { qc_deduction_per: any }) =>
          parseFloat(qcDetail?.qc_deduction_per || 0).toFixed(2),
        (qcDetail1: { net_rate: any }) =>
          parseFloat(qcDetail1?.net_rate || 0).toFixed(2),
        (qcDetail2: { qc_deduction_rate: any }) =>
          parseFloat(qcDetail2?.qc_deduction_rate || 0).toFixed(2)
      );
      console.log(
        formattedDeductionPerArray,
        'Formatted qc_deduction_per array'
      );

      for (let index = 0; index < payload.qc_details.length; index++) {
        const qcpl = this.qc_params.length ? true : false;
        if (
          (btn == 'Resubmit' && qcpl) ||
          !payload.qc_details[index].qc_parameter_details
        ) {
          if (
            this.qc_params[index] == '' ||
            this.qc_params[index] == undefined
          ) {
            const fv = this.qcForm.getRawValue();
            this.showSwalMassage(
              'Enter Quality Parameter Value for ' +
                this.getItemName(fv.qc_details),
              'error'
            );
            return;
          }
          payload.qc_details[index].qc_parameter_details =
            this.qc_params[index];
          console.log(
            'qc parameter added:',
            payload.qc_details[index].qc_parameter_details
          );
        }
      }

      this.qcForm.value.workflow_status = btn;
      payload.transaction_date = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );
      payload.workflow_status = btn;

      console.log('payload: ', payload);
      this.handleSave.emit(payload);
      this.qcService.errorReport.subscribe((data) =>
        console.log('data====', data)
      );
      // const er = this.handleSave.emit(payload);
      // console.log("error====",this.error)
      // console.log("er====",er)
      // this.handleSave.error((err:any) => {
      //   console.error('whoops! there was an error.........', err);
      // const i_tag = document.createElement('i');
      //     (<HTMLInputElement>document.getElementById('Approve')).removeChild(i_tag);
      //   });
    }
  }

  getItemName(id: any) {
    const key = this.itemDetailData.filter(
      (i: any) => i.id == id[0]['item_ref_id']
    );
    return key[0]['key'];
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.qcForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    this.initializeForm();
    this.qcForm.get('transaction_date')?.setValue(this.currentDate);
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  addQcParameterDetails(index: any) {
    console.log('isDisable', this.isDisable);

    const gridRow = (<FormArray>this.qcForm.get('qc_details')).at(index);
    gridRow.value['qc_parameter_details'] = [];
    gridRow.get('item_ref_id')?.enable();
    gridRow.get('base_rate')?.enable();

    if (!gridRow.get('item_ref_id')?.value) {
      return;
    }
    if (!Array.isArray(this.rowData)) {
      gridRow.value['qc_parameter_details'] =
        this.rowData['qc_details'][index]['qc_parameter_details'];
    }
    if (this.qc_params[index]) {
      gridRow.value['qc_parameter_details'] = this.qc_params[index];
    }
    gridRow.value['submitBtn'] = this.isDisable;
    gridRow.value['transaction_date'] = this.datepipe.transform(
      this.qcForm.get('transaction_date')?.value,
      'yyyy-MM-dd'
    );
    const dialogRef: MatDialogRef<QcParameterDialogComponent> =
      this.dialog.open(QcParameterDialogComponent, {
        data: gridRow,
      });
    dialogRef.afterClosed().subscribe((result: any) => {
      gridRow.get('item_ref_id')?.disable();
      gridRow.get('base_rate')?.disable();
      if (result) {
        this.qc_params[index] = result['qc_parameter_details'];
        this.rate_ded = 0;
        const gridRow = (<FormArray>this.qcForm.get('qc_details')).at(index);
        gridRow.get('qc_rate')?.setValue(result['deduction_rate']);
        gridRow.get('qc_deduction_rate')?.setValue(result['qc_deduction_rate']);
        gridRow.get('qc_deduction_per')?.setValue(result['qc_deduction_per']);
        gridRow.get('net_rate')?.setValue(result['net_rate']);
        gridRow.get('rate')?.setValue(result['net_rate']);

        this.calculateTransactionAmount();
      }
    });
  }

  NetRateChange(event: any, index: any) {
    this.calculateTransactionAmount();
  }

  getItemCode(j: any) {
    return this.itemCodeData.find(
      (e) => e.id == this.formArrcontact.controls[j].get('item_code')?.value
    ).key;
  }

  Sample_qty_change(event: any, index: any) {
    const val = parseFloat(event.target.value);
    const gridRow = (<FormArray>this.qcForm.get('qc_details')).at(index);
    const net_qty = parseFloat(gridRow.get('grn_qty')?.value);
    const accepted_qty = parseFloat(gridRow.get('accepted_qty')?.value);
    let reject_rate = 0;
    if ((val < net_qty && val > 0) || net_qty == 0) {
      if (accepted_qty) {
        const total = val + accepted_qty;
        if (total <= net_qty) {
          reject_rate = net_qty - total;
        } else {
          gridRow.get('accepted_qty')?.setValue('');
          gridRow.get('sample_qty')?.setValue(0);
        }
      } else {
        reject_rate = net_qty - val;
        if (net_qty == 0) {
          reject_rate = 0;
        }
      }
      gridRow.get('rejected_qty')?.setValue(reject_rate.toFixed(4));
    } else {
      gridRow.get('sample_qty')?.setValue(0);
    }
  }

  accepted_qty_change(event: any, index: any) {
    const val = parseFloat(event.target.value);
    const gridRow = (<FormArray>this.qcForm.get('qc_details')).at(index);
    const net_qty = parseFloat(gridRow.get('grn_qty')?.value);
    if (net_qty === 0) {
      gridRow.get('accepted_qty')?.setErrors(null);
    } else if (val > net_qty) {
      gridRow
        .get('accepted_qty')
        ?.setErrors({ acceptedQtyGreaterThanGrnQty: true });
    } else {
      gridRow.get('accepted_qty')?.setErrors(null);
    }
    this.calculateTransactionAmount();
    let rejected_qty = 0;
    if ((val >= 0 && val <= net_qty) || net_qty == 0) {
      // if (sample_qty) {
      // let smsm = sample_qty + val
      // if (smsm <= net_qty) {
      // rejected_qty = net_qty - smsm
      // }
      // else {
      // gridRow.get('accepted_qty')?.setValue('')
      // gridRow.get('sample_qty')?.setValue(0)
      // }
      // }
      // else {
      if (net_qty != 0 && !isNaN(Number(net_qty))) {
        rejected_qty = net_qty - event.target.value;
      }
      gridRow.get('rejected_qty')?.setValue(rejected_qty.toFixed(4));

      // }
    } else {
      // gridRow.get('accepted_qty')?.setValue(0)
      rejected_qty = net_qty - event.target.value;
      gridRow.get('rejected_qty')?.setValue(rejected_qty.toFixed(4));

      this.calculateTransactionAmount();
    }
  }

  calculateTransactionAmount() {
    const gridRow = <FormArray>this.qcForm.get('qc_details');
    this.total_transaction_amount = 0;
    for (let j = 0; j < gridRow.length; j++) {
      const act_qty = parseFloat(gridRow.at(j).get('accepted_qty')?.value);
      const rate = parseFloat(gridRow.at(j).get('rate')?.value);
      const transaction_amount = (rate * act_qty).toFixed(2);

      if (
        transaction_amount &&
        transaction_amount != '' &&
        !isNaN(Number(transaction_amount))
      ) {
        gridRow.at(j).get('txn_currency_amount')?.setValue(transaction_amount);
        this.total_transaction_amount =
          parseFloat(this.total_transaction_amount.toString()) +
          parseFloat(transaction_amount);
      } else {
        gridRow.at(j).get('txn_currency_amount')?.setValue(0);
      }
    }
    // this.total_transaction_amount = this.total_transaction_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    this.qcForm
      .get('total_txn_currency_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
  }

  setAttachment(event: any) {
    this.previewImageType = false;
    this.previewImage = '';

    const fileToUpload = event.target.files[0];

    if (fileToUpload.size <= 5 * 1024 * 1024) {
      const reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage = reader.result;
      };
      const fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType = true;
      }
      this.fileName = fileToUpload['name'];
      const compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        const formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileAttchatment = formData1;
        this.showFileLoader = true;
        this.uploadStatus = '';
        this.qcService.saveFile(this.fileAttchatment).subscribe({
          next: (data: any) => {
            this.showFileLoader = false;
            const files3path = data['s3_file_path'];

            this.qcForm.get('attachment')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e) => {
            this.uploadStatus = '';
            this.fileName = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileName = '';
        this.fileInputVar.nativeElement.value = '';

        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      this.showSwalmessage('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
  removeImage() {
    this.previewImage = '';
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.qcForm.reset();
      this.handleCancel.emit(false);
    }
  }
}
