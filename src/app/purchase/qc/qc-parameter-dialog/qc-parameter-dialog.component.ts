import { Component, OnInit ,Inject} from '@angular/core';
import { FormBuilder , FormArray} from '@angular/forms';
import { MatDialog ,MatDialogRef,MAT_DIALOG_DATA} from "@angular/material/dialog";

import { QcService } from 'src/app/shared/services/qc.service';
import { UntypedFormArray,  UntypedFormGroup,Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { number } from 'echarts';
export interface DialogData {
  data: any;
}
@Component({
  selector: 'app-qc-parameter-dialog',
  templateUrl: './qc-parameter-dialog.component.html',
  styleUrls: ['./qc-parameter-dialog.component.scss']
})
export class QcParameterDialogComponent implements OnInit{
  constructor(
    private qcService: QcService,
    private dialogRef: MatDialogRef<QcParameterDialogComponent>,
    private dialog:MatDialog,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  qcParameterdetails!:UntypedFormGroup
  qualityData:any=[]
  range = false
  submitBtn= this.data.value['submitBtn']
  showLoader = false
  total_weight = 0 
  ngOnInit(): void {
  this.intializeForm()

  if((this.data.value['qc_parameter_details'])){
    this.getqctype(this.data.value['item_ref_id'])
    this.viewRecord(this.data.value)

    this.total_weight  = 0
    for (let index = 0; index < this.data.value['qc_parameter_details'].length; index++) {
       let  v =  this.data.value['qc_parameter_details'][index]['actual_value']
       if(v){
        this.total_weight = (parseFloat(this.total_weight.toString()) + parseFloat(v))
       }
    }

      this.qcParameterdetails.get('total_weight_sample')?.setValue(this.total_weight.toFixed(4))

  }

  else{
  this.qcService.getQualityParameterDetailForQC(this.data.value['item_ref_id'], 'Purchase', this.data.value['transaction_date']).subscribe({
    next: (data: any) => {
      this.qualityData = data
      if(data[0]['range']){
        this.range = true
      }
      else{
        this.range = false
      }
      if (this.qualityData.length >= 1) {
        this.qcParameterdetails.setControl('qc_parameter_details', this.setExistingArray(this.qualityData));
      }
    },
  });

}
 }

 intializeForm(){
  this.qcParameterdetails = this.fb.group({
    id:[''],
    total_weight_sample:[''],
    qc_parameter_details: this.fb.array([this.qc_parameter_details()]),

  })
 }

 showSwalMassage(massage: any, icon: any): void {
  Swal.fire({
    title: massage,
    icon: icon,
    timer: 2000,
    showConfirmButton: false
  });
}

 qc_parameter_details() {
  return this.fb.group({
          id:[''],
          item_quality_parameter_ref_id:[{ value: '', disabled: true }],
          actual_value:['', [Validators.pattern("^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$"),Validators.pattern("^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$")]],
          deduction_per:[''],
          range:[{ value: '', disabled: true }]

  });
}

get formArrQcp() {
  return this.qcParameterdetails.get('qc_parameter_details') as UntypedFormArray;

}
addNewRow() {
  this.formArrQcp.push(this.qc_parameter_details());
}

setExistingArray(initialArray: any): UntypedFormArray {
  const formArray: any = new UntypedFormArray([]);
  initialArray.forEach((element: any) => {


    formArray.push(this.fb.group({
      id: element.id,
      item_quality_parameter_ref_id: {value:element.item_quality_parameter_ref_id, disabled: true },
      actual_value: element.actual_value,
      deduction_per: element.deduction_per, 
      range:{value:element.range, disabled: true }      
    }));
  });
  return formArray;

}

viewRecord(edata: any) {
  let qcDetailItemRow = edata['qc_parameter_details'].filter(function (data: any) {
    return data; 
  });
  if (qcDetailItemRow.length >= 1) {
    this.qcParameterdetails.setControl('qc_parameter_details', this.setExistingArray(qcDetailItemRow));
  }

  if(this.submitBtn == false){
    this.qcParameterdetails.disable()
  }
}
  
deleteRow(index: number) {
  if (index == 0) {
    return false;
  } else {
    this.formArrQcp.removeAt(index);
    return true;
  }
}

onCancelForm(){
  this.dialogRef.close()
}

onSubmit(){
  if(this.qcParameterdetails.invalid){
    return
  }
  else{
    this.showLoader = true
  // let gridRow1 = (<FormArray>(this.qcParameterdetails.get("qc_parameter_details")))
  // for(let j=0;j<gridRow1.length;j++){
  //   gridRow1.at(j).get('item_quality_parameter_ref_id')?.enable();
  // }
  // this.qcParameterdetails.value['item_ref_id'] =  this.data.value['item_ref_id']
  // this.qcParameterdetails.value['base_rate'] =  this.data.value['base_rate']

  const qc_parameter_details = this.qcParameterdetails.get("qc_parameter_details")?.getRawValue()
  const payload = {
    'item_ref_id': this.data.value['item_ref_id'],
    'base_rate' : this.data.value['base_rate'],
    'qc_parameter_details': qc_parameter_details

  }

  // console.log('qc_parameter_details: ', qc_parameter_details);
  // console.log('this.qcParameterdetails?.value: ', this.qcParameterdetails?.value);
  
  this.qcService.CalculateDeductionRate(payload).subscribe({
    next: (data: any) => {
    this.showLoader = false
    this.dialogRef.close(data);

    },
  });
  }
}

qualityParamsChange(event:any,index:any){
  let val = event.target.value
  let gridRow = (<FormArray>(this.qcParameterdetails.get('qc_parameter_details'))).at(index)
  let parameter = gridRow.get('item_quality_parameter_ref_id')?.value
  if(parameter== '' || parameter == undefined){
    this.showSwalMassage('Please Select Quality Parameter','warning')
    gridRow.get('actual_value')?.setValue('')
    return
  }
  let payload = {
    'item_ref_id':this.data.value['item_ref_id'],
    'item_quality_parameter_ref_id':parameter,
    'actual_value':val,
    'base_rate': 50

  }

  this.qcService.CalculateDeductionRate(payload).subscribe({
    next: (data: any) => {
      gridRow.get('rate_deduction')?.setValue(data['rate_deduction'])
      gridRow.get('deduction_per')?.setValue(data['deduction_per'])
    },
  });

}

getqctype(v:any){
  this.qcService.getQualityParameterDetailForQC(this.data.value['item_ref_id'], 'Purchase', this.data.value['transaction_date']).subscribe({
    next: (data: any) => {
      this.qualityData = data
      if(data[0]['range']){
        this.range = true
      }
    let gridRow = (<FormArray>(this.qcParameterdetails.get("qc_parameter_details")))
      for (let index = 0; index < gridRow.length; index++) {
        gridRow.at(index).get('range')?.setValue(data[index]['range'])
        gridRow.at(index).get('item_quality_parameter_ref_id')?.setValue(data[index]['item_quality_parameter_ref_id'])
        
      }
 
    },
  });

}

actual_value_change(){
  
  this.total_weight  = 0
  let gridRow = (<FormArray>(this.qcParameterdetails.get("qc_parameter_details")))
  for (let index = 0; index < gridRow.length; index++) {
    let  v =  gridRow.at(index).get('actual_value')?.value
    if(v){
     this.total_weight = (parseFloat(this.total_weight.toString()) + parseFloat(v))
    }
  }
  this.qcParameterdetails.get('total_weight_sample')?.setValue(this.total_weight.toFixed(4))


}

}
