import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, from, fromEvent, map, merge } from 'rxjs';
import { ThemePalette } from '@angular/material/core';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { TableElement } from 'src/app/shared/TableElement';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import Swal from 'sweetalert2';
import { QcService } from 'src/app/shared/services/qc.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { DatePipe } from '@angular/common';
import { getStatusColor } from 'src/app/constants/constants';
import { ErrorReportService } from 'src/app/shared/services/error-report.service';

@Component({
  selector: 'app-qc',
  templateUrl: './qc.component.html',
  styleUrls: ['./qc.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class QcComponent extends UnsubscribeOnDestroyAdapter implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  resultData!: [];
  displayedColumns = [
    'view',
    'edit',
    'transaction_date',
    'qc_number',
    'gate_pass_no',
    'farmer_name',
    'total_txn_currency_amount',
    'workflow_status',
  ];

  renderedData: any = [];
  screenName = 'QC';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;

  exampleDatabase?: QcService;
  dataSource!: ExampleDataSource;
  // dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';
  employee_authority: any;
  cnt: any = 0;
  viewOnSendback:any=false;
  error: any = '';

  constructor(
    private roleSecurityService: RoleSecurityService,
    private qcService: QcService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService,
    private errorReport: ErrorReportService
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');
    this.employee_authority = localStorage.getItem('employee_authority');

    this.roleSecurityService.getAccessLeftPanel(userId, 'QC').subscribe({
      next: (data: any) => {
        this.sidebarData = data[0];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.refresh();
  }
  getColorStatus(status: string): string {
    return getStatusColor(status);
  }

  editViewRecord(row: any, flag: boolean) {
    this.qcService.getAllQcDataByid(row.id).subscribe({
      next: (data: any) => {
        this.rowData = data[0];
        this.showList = false;
        this.submitBtn = flag;
        this.listDiv = true;
        this.showLoader = false;
      },
    });
  }

  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: Event | boolean) {
    this.listDiv = Boolean(item);
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }
  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;
    const json_data = { data: this.encDecryService.encryptedData(formValue) };

    this.qcService.createQC(json_data, formValue.id).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          this.showSwalmessage(
            'Your record has been updated successfully!',
            '',
            'success',
            false
          );
        } else if (data['status'] == 2) {
          this.showSwalmessage(
            'Your record has been added successfully!',
            '',
            'success',
            false
          );
        }
        this.refresh();
        this.showList = true;
        this.listDiv = false;
        this.showLoader = false;
      },
      error: (e) => {
        this.error = this.errorCodes.getErrorMessage(JSON.parse(e).status);
        this.cnt = 0;
        this.viewOnSendback=false;
        this.showLoader = false;
        this.showSwalmessage(
          this.error,
          '',
          'error',
          false
        );
        this.qcService.errorHandler(e);
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new QcService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'QC Number': x.transaction_ref_no,
        'Gate Pass Number': x.gate_pass_ref_id__transaction_ref_no,
        'Supplier Name': x.supplier_ref_id,
        'Total Transaction Amount': x.total_txn_currency_amount,
        'WorkFlow Status': x.workflow_status,
      }));

    TableExportUtil.exportToExcel(exportData, 'QC Details');
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: QcService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_ref_no +
              advanceTable.workflow_status +
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.gate_pass_ref_id__transaction_ref_no +
              advanceTable.supplier_ref_id +
              advanceTable.total_txn_currency_amount
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {
    //disconnect
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'transaction_ref_no':
          [propertyA, propertyB] = [a.transaction_ref_no, b.transaction_ref_no];
          break;
        case 'supplier_ref_id':
          [propertyA, propertyB] = [a.supplier_ref_id, b.supplier_ref_id];
          break;
        case 'transaction_date':
          [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
          break;
        case 'gate_pass_ref_id__transaction_ref_no':
          [propertyA, propertyB] = [
            a.gate_pass_ref_id__transaction_ref_no,
            b.gate_pass_ref_id__transaction_ref_no,
          ];
          break;
        case 'workflow_status':
          [propertyA, propertyB] = [a.workflow_status, b.workflow_status];
          break;
        case 'total_txn_currency_amount':
          [propertyA, propertyB] = [
            a.total_txn_currency_amount,
            b.total_txn_currency_amount,
          ];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
