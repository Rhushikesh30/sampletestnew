import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseBookingDetailsComponent } from './purchase-booking-details.component';

describe('PurchaseBookingDetailsComponent', () => {
  let component: PurchaseBookingDetailsComponent;
  let fixture: ComponentFixture<PurchaseBookingDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseBookingDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PurchaseBookingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
