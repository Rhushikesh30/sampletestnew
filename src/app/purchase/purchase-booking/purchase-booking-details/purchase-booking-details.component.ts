import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { PurchaseBookingService } from 'src/app/shared/services/purchase-booking.service';
import { PurchaseBookingDialogComponent } from '../purchase-booking-dialog/purchase-booking-dialog.component';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { index } from 'd3';
export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-purchase-booking-details',
  templateUrl: './purchase-booking-details.component.html',
  styleUrls: ['./purchase-booking-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class PurchaseBookingDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  cancelFlag = true;
  purchaseBookingForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  SupplierData: any = [];
  txn_currency: any;
  filterSupplierData: any = [];
  CurrencyData: any = [];
  filterSupplierBillData: any = [];
  SupplierBillData: any = [];
  SupplierBillNo: any = [];
  filterSupplierBillNo: any = [];
  GRNData: any = [];
  filterGRNData: any = [];
  ItemDataData: any = [];
  PaymentTermsTypeData: any = [];
  SupplierQC: any = [];
  filterSupplierQC: any = [];
  UOMData: any = [];
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  message: any;
  message1: any = 'Please Select Supplier Name First.';
  freight_charges: any = 0;
  insurance_charges: any = 0;
  p_f_charges: any = 0;
  total_other_charges: any = 0;
  total_transaction_amt: any = 0;
  filterPOData: any;
  POData: any;
  POTypeData: any;
  discount_amt: any = 0;
  GridView = false;
  SalesOrderData: any;
  rolename: any;
  filterAgentData: any;
  AgentData: any;
  purchase_sub_details: any = [];
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];
  DisabledAdd = true;
  total_qty: any;
  total_item_amt: any = 0;
  transporation_amt = 0;
  buttonName = 'Add';
  isGstSetOff = false;
  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  total_transaction_amount: any = 0;
  @Input() screenName!: string;

  HsnData: any[] = [];
  isDisable = false;

  constructor(
    private formBuilder: FormBuilder,
    private fpcService: FpcSetupService,
    private errorCodes: ErrorCodes,
    private dialog: MatDialog,
    public datepipe: DatePipe,
    private purchaseorderService: PurchaseorderService,
    private purchaseBookingService: PurchaseBookingService,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewEditRecord(this.rowData);
    }

    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.rolename = rolename[0];

    this.purchaseBookingService
      .getQCSupplierData('getQCSupplierData')
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              data[i]['key'] =
                data[i]['company_name'] +
                '-' +
                data[i]['mobile_no'] +
                '/' +
                data[i]['ref_type'];
            }
          }
          this.SupplierData = data;
          this.filterSupplierData = this.SupplierData.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.purchaseBookingForm
          .get('txn_currency')
          ?.setValue(this.txn_currency[0]['id']);
        const gridRow = (<FormArray>(
          this.purchaseBookingForm.get('purchase_booking_details')
        )).at(0);
        gridRow.get('txn_currency')?.setValue(this.txn_currency[0]['id']);
      },
    });

    this.purchaseorderService.getDynamicData('item_master', 'code').subscribe({
      next: (data: any) => {
        this.ItemDataData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.purchaseorderService.getDynamicData('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
        // let division_id = data.filter((d:any)=>d.key=='Business')
        // if(division_id){
        //   this.purchaseBookingForm.get('division_ref_id')?.setValue(division_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.purchaseorderService.getDynamicData('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
        // let dept_id = data.filter((d:any)=>d.key=='Trading')
        // if(dept_id){
        //   this.purchaseBookingForm.get('department_ref_id')?.setValue(dept_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.purchaseorderService.getDynamicData('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
        // let dept_id = data.filter((d:any)=>d.key=='B2B')
        // if(dept_id){
        //   this.purchaseBookingForm.get('sale_type_ref_id')?.setValue(dept_id[0]['id'])
        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.purchaseorderService.getDynamicData('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
        // if(data){
        //   this.purchaseBookingForm.get('location_ref_id')?.setValue(data[0]['id'])

        // }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.purchaseorderService.getDynamicData('agent', 'agent_name').subscribe({
      next: (data: any) => {
        this.AgentData = data;
        this.filterAgentData = this.AgentData.slice();
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Payment Terms').subscribe({
      next: (data: any) => {
        this.PaymentTermsTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.purchaseorderService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.UOMData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.purchaseorderService
      .getDynamicData('hsn_sac', 'hsn_sac_no')
      .subscribe({
        next: (data: any) => {
          this.HsnData = data;
        },
        error: (e: any) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.purchaseBookingForm = this.formBuilder.group({
      id: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      txn_currency: [{ value: '', disabled: true }],
      conversion_rate: [1],
      created_by: [''],
      base_currency: [''],
      base_currency_total_amount: [''],
      transaction_date: [''],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      supplier_ref_id: ['', Validators.required],
      supplier_ref_type: [{ value: '', disabled: true }],
      grn_ref_id: [{ value: '', disabled: true }],
      supplier_payment_terms_ref_id: [
        { value: false, disabled: true },
        Validators.required,
      ],
      // po_ref_id:[{value:'',disabled:true}],
      po_ref_id: [''],
      qc_ref_id: ['', Validators.required],
      po_type_ref_id: [{ value: '', disabled: true }],
      txn_currency_discount_percent: [{ value: 0, disabled: true }],
      txn_currency_discount_amount: [{ value: 0, disabled: true }],
      txn_currency_insurance_txn_charges: [{ value: 0, disabled: true }],
      txn_currency_packing_forwarding_charges: [{ value: 0, disabled: true }],
      txn_currency_clearing_charges: [0],
      txn_currency_total_other_txn_charges: [{ value: 0, disabled: true }],
      other_charges_igst_amount: [{ value: 0, disabled: true }],
      other_charges_cgst_amount: [{ value: 0, disabled: true }],
      other_charges_sgst_amount: [{ value: 0, disabled: true }],
      total_other_charges_tax_amount: [{ value: 0, disabled: true }],
      txn_currency_total_amount: [0],
      txn_currency_balance_total_amount: [0],
      is_active: [true],
      is_deleted: [false],
      so_ref_id: [{ value: '', disabled: true }],
      so_type_ref_id: [''],
      base_currency_freight_txn_charges: [0],
      is_liability_book: [false],
      is_rate_percentage: [{ value: false, disabled: true }],
      agent_ref_id: [{ value: '', disabled: true }],
      agent_commision: [{ value: 0, disabled: true }],
      agent_commision_amt: [{ value: 0, disabled: true }],
      transportation_charges: [{ value: 0, disabled: true }],
      division_ref_id: [{ value: 0, disabled: true }],
      sale_type_ref_id: [{ value: 0, disabled: true }],
      department_ref_id: [{ value: 0, disabled: true }],
      location_ref_id: [{ value: 0, disabled: true }],
      gate_pass_no: [''],
      purchase_booking_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: [{ value: '', disabled: true }, Validators.required],
      item_type_id: [''],
      batch_no: [{ value: '', disabled: true }],
      alternate_qty: [{ value: '', disabled: true }, Validators.required],
      rate: ['', Validators.required],
      alternate_uom: [{ value: '', disabled: true }, Validators.required],
      hsn_sac_no: [{ value: '', disabled: true }],
      no_of_bags: [{ value: '', disabled: true }],
      old_no_of_bags: [{ value: '', disabled: true }],
      conversion_rate: [1],
      txn_currency: [''],
      txn_currency_amount: [{ value: '', disabled: true }],
      txn_currency_igst_rate: [''],
      txn_currency_igst_amount: [''],
      txn_currency_cgst_rate: [''],
      txn_currency_cgst_amount: [''],
      txn_currency_sgst_rate: [''],
      txn_currency_sgst_amount: [''],
      txn_currency_tax_amount: [{ value: 0, disabled: true }],
      txn_currency_total_txn_amount: [{ value: 0, disabled: true }],
      txn_currency_total_base_amount: [{ value: 0, disabled: true }],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      base_currency: [],
      base_currency_amount: [0],
      base_currency_discount_percent: [0],
      base_currency_igst_rate: [0],
      base_currency_igst_amount: [0],
      base_currency_cgst_rate: [0],
      base_currency_cgst_amount: [0],
      base_currency_sgst_rate: [0],
      base_currency_sgst_amount: [0],
      base_currency_tax_amount: [],
      empty_bag_weight: [{ value: 0, disabled: true }],
      labour_charges: [{ value: 0, disabled: true }],
      tax_rate: [{ value: 0, disabled: true }],
      is_gst_set_off: [{ value: false, disabled: true }],
      alternate_net_qty: [{ value: '', disabled: true }, Validators.required],
      purchase_booking_sub_details: [],
    });
  }

  get formArray() {
    return this.purchaseBookingForm.get(
      'purchase_booking_details'
    ) as FormArray;
  }

  setExistingArray1(initialArray = [], val: any, val1: any): FormArray {
    this.total_qty = 0;
    this.total_item_amt = 0;
    let disable: any;
    if (!this.submitBtn) {
      disable = true;
    } else {
      disable = false;
    }
    if (val1 == 'supplier_change') {
      this.GridView = false;
      this.DisabledAdd = false;
    } else {
      this.GridView = true;
    }

    const formArray: any = new FormArray([]);
    initialArray.forEach((element: any, index: any) => {
      console.log(element);

      let rate: any;
      let transaction_amount: any;
      let grn_qty: any;
      let grn_qty1: any;
      let id: any;
      let empty_bag_weight: any;
      let labour_charges = 0;
      let tax_rate = 0;
      let txn_currency_total_base_amount: any = 0;
      let txn_currency_total_txn_amount: any = 0;
      let tax_amt = 0;
      let gst_flag: any;

      if (val == 'false') {
        this.purchaseBookingForm.disable();
        rate = element.rate;
        transaction_amount = element.txn_currency_amount;
        grn_qty = element.alternate_qty;
        grn_qty1 = element.alternate_net_qty;
        id = element.id;
        empty_bag_weight = element.empty_bag_weight;
        labour_charges = element.labour_charges;
        tax_rate = element.tax_rate;
        txn_currency_total_base_amount = element.txn_currency_total_base_amount;
        txn_currency_total_txn_amount = element.txn_currency_total_txn_amount;
        tax_amt = element.txn_currency_tax_amount;
        gst_flag = element.is_gst_set_off;
      } else {
        rate = element.rate;
        transaction_amount = element.rate * element.alternate_accepted_qty;
        grn_qty = element.alternate_accepted_qty;
        grn_qty1 = element.alternate_accepted_qty;
        id = '';
        empty_bag_weight = 0;
        labour_charges = 0;
        txn_currency_total_base_amount = transaction_amount;
        txn_currency_total_txn_amount = transaction_amount;
        tax_amt = 0;
        gst_flag = false;
      }
      console.log(txn_currency_total_txn_amount);

      this.total_transaction_amt =
        parseFloat(this.total_transaction_amt) + parseFloat(transaction_amount);
      this.total_qty = parseFloat(this.total_qty) + parseFloat(grn_qty1);
      this.total_item_amt =
        parseFloat(this.total_item_amt) + parseFloat(transaction_amount);

      this.purchaseorderService
        .getDynamicData('tax_rate', element.item_ref_id)
        .subscribe({
          next: (data: any) => {
            if (data.length > 0) {
              tax_rate = Number(data[0]['tax_rate']);
            } else {
              tax_rate = 0;
            }

            formArray.push(
              this.formBuilder.group({
                id: id,
                item_ref_id: [{ value: element.item_ref_id, disabled: true }],
                rate: [
                  { value: parseFloat(rate).toFixed(2), disabled: disable },
                ],
                alternate_uom: [
                  { value: element.alternate_uom, disabled: true },
                ],
                hsn_sac_no: [{ value: element.hsn_sac_no, disabled: true }],
                alternate_qty: [
                  { value: parseFloat(grn_qty).toFixed(2), disabled: true },
                ],
                alternate_net_qty: [
                  { value: parseFloat(grn_qty1).toFixed(2), disabled: true },
                ],
                item_type_id: element.item_type_id,
                batch_no: [{ value: element.batch_no, disabled: true }],
                no_of_bags: [{ value: element.no_of_bags, disabled: true }],
                old_no_of_bags: [{ value: element.no_of_bags, disabled: true }],
                empty_bag_weight: [
                  { value: empty_bag_weight, disabled: disable },
                ],
                conversion_rate: element.conversion_rate,
                txn_currency: element.txn_currency,
                txn_currency_igst_rate: element.txn_currency_igst_rate,
                txn_currency_igst_amount: element.txn_currency_igst_amount,
                txn_currency_cgst_rate: element.txn_currency_cgst_rate,
                txn_currency_cgst_amount: element.txn_currency_cgst_amount,
                txn_currency_sgst_rate: element.txn_currency_sgst_rate,
                txn_currency_sgst_amount: element.txn_currency_sgst_amount,
                txn_currency_tax_amount: [{ value: tax_amt, disabled: true }],
                txn_currency_total_txn_amount: [
                  {
                    value: parseFloat(txn_currency_total_txn_amount).toFixed(2),
                    disabled: true,
                  },
                ],
                txn_currency_total_base_amount: [
                  {
                    value: parseFloat(txn_currency_total_base_amount).toFixed(
                      2
                    ),
                    disabled: true,
                  },
                ],
                txn_currency_amount: [
                  {
                    value: parseFloat(transaction_amount).toFixed(2),
                    disabled: true,
                  },
                ],
                txn_currency_discount_percent:
                  element.txn_currency_discount_percent,
                txn_currency_net_rate: element.txn_currency_net_rate,
                base_currency: element.base_currency,
                base_currency_amount: element.base_currency_amount,
                base_currency_custom_duty_charges:
                  element.base_currency_custom_duty_charges,
                base_currency_discount_percent:
                  element.base_currency_discount_percent,
                base_currency_net_rate: element.base_currency_net_rate,
                base_currency_tax_amount: element.base_currency_tax_amount,
                transaction_id: element.transaction_id,
                transaction_ref_no: element.transaction_ref_no,
                transaction_type_id: element.transaction_type_id,
                period: element.period,
                ledger_group: element.ledger_group,
                fiscal_year: element.fiscal_year,
                tenant_id: element.tenant_id,
                labour_charges: [{ value: labour_charges, disabled: disable }],
                tax_rate: tax_rate,
                is_gst_set_off: [
                  { value: element.is_gst_set_off, disabled: true },
                ],
                purchase_booking_sub_details: [
                  element.purchase_booking_sub_details,
                ],
              })
            );
            if (element.is_gst_set_off == true) {
              this.isGstSetOff = true;
              this.cal_tax_amt(element.is_gst_set_off, index);
            }
            if (element.is_gst_set_off == false) {
              this.isGstSetOff = false;
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    });
    if (val == 'true') {
      this.purchaseBookingForm
        .get('txn_currency_total_amount')
        ?.setValue(parseFloat(this.total_transaction_amt).toFixed(2));
    }
    return formArray;
  }

  supplierChnage(val: any) {
    this.purchaseBookingForm.setControl(
      'purchase_booking_details',
      this.setExistingArray1([], 'true', 'supplier_change')
    );
    this.formArray.push(this.initialitemRow());
    this.purchaseBookingForm.get('grn_ref_id')?.setValue('');
    this.purchaseBookingForm.get('qc_ref_id')?.setValue('');
    this.purchaseBookingForm.get('txn_currency_total_amount')?.setValue('');
    this.purchaseBookingForm.get('po_ref_id')?.setValue('');
    this.purchaseBookingForm.get('po_type_ref_id')?.setValue('');
    this.purchaseBookingForm.get('supplier_ref_type')?.setValue('');
    this.purchaseBookingForm.get('txn_currency_discount_percent')?.setValue('');
    this.purchaseBookingForm
      .get('txn_currency_insurance_txn_charges')
      ?.setValue('');
    this.purchaseBookingForm
      .get('txn_currency_packing_forwarding_charges')
      ?.setValue('');
    this.purchaseBookingForm.get('agent_commision')?.setValue('');
    this.purchaseBookingForm.get('agent_commision_amt')?.setValue('');
    this.purchaseBookingForm.get('transportation_charges')?.setValue('');
    this.purchaseBookingForm.get('agent_ref_id')?.setValue('');

    this.GRNData = [];
    this.filterGRNData = [];
    this.SupplierQC = [];
    this.filterSupplierQC = [];
    this.total_transaction_amt = 0;
    if (val == '' || val == 'None' || val == undefined) {
      this.DisabledAdd = true;

      this.message1 = 'Please Select Supplier Name First.';
      this.POData = [];
      this.filterPOData = [];
      this.purchaseBookingForm.get('txn_currency_discount_percent')?.disable();
      this.purchaseBookingForm
        .get('txn_currency_insurance_txn_charges')
        ?.disable();
      this.purchaseBookingForm
        .get('txn_currency_packing_forwarding_charges')
        ?.disable();
      this.purchaseBookingForm.get('agent_commision')?.disable();
      // this.purchaseBookingForm.get('agent_commision_amt')?.disable()
      this.purchaseBookingForm.get('transportation_charges')?.disable();
      this.purchaseBookingForm.get('agent_ref_id')?.disable();
      this.purchaseBookingForm.get('is_rate_percentage')?.disable();
      this.purchaseBookingForm.get('supplier_payment_terms_ref_id')?.disable();
    } else {
      this.DisabledAdd = true;

      this.message1 = '';
      this.purchaseorderService
        .getDynamicData('supplier', 'payment_terms_ref_id')
        .subscribe({
          next: (data: any) => {
            for (let i = 0; i < data.length; i++) {
              if (data[i]['id'] == val) {
                if (data[i]['key'] == 0) {
                  this.purchaseBookingForm
                    .get('supplier_payment_terms_ref_id')
                    ?.setValue('');
                } else {
                  this.purchaseBookingForm
                    .get('supplier_payment_terms_ref_id')
                    ?.setValue(data[i]['key']);
                }
              }
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      if (this.SupplierData.length > 0) {
        for (let i = 0; i < this.SupplierData.length; i++) {
          if (this.SupplierData[i]['id'] == val) {
            this.purchaseBookingForm
              .get('supplier_ref_type')
              ?.setValue(this.SupplierData[i]['ref_type']);
          }
        }
      }
      this.GetQcData(val);

      this.getSupplierPOData(val);
    }
  }
  getSupplierPOData(val: any) {
    this.purchaseBookingService
      .getSupplierPOData('Supplier_PO', val)
      .subscribe({
        next: (data: any) => {
          this.POData = data;
          this.filterPOData = this.POData.slice();
          // if(this.POData.length > 0){
          // this.purchaseBookingForm.get('po_ref_id')?.setValue( this.POData[0]['id'] )

          // }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }
  getAgentCommision(rate: any, commision: any, total_amt: any) {
    console.log('yess', rate);

    let commision_amt = 0;
    if (rate == false) {
      commision_amt = this.total_qty * parseFloat(commision);
    } else {
      commision_amt = (total_amt * commision) / 100;
    }
    console.log(commision_amt);

    this.purchaseBookingForm
      .get('agent_commision_amt')
      ?.setValue(commision_amt.toFixed(1));
  }

  agentCommisionChange(e: any) {
    if (this.purchaseBookingForm.get('is_rate_percentage')?.value == true) {
      if (e.target.value == '' || e.target.value == undefined) {
        console.log(e.target.value, 'Empty');
      } else {
        if (parseFloat(e.target.value) > 100) {
          Swal.fire({
            title: 'Not allowed',
            text: 'Percentage Should less than or equal 100 !',
            icon: 'warning',
          });
          this.purchaseBookingForm.get('agent_commision')?.setValue('');
        } else {
          this.getAgentCommision(
            this.purchaseBookingForm.get('is_rate_percentage')?.value,
            e.target.value,
            this.total_item_amt
          );
        }
      }
    } else {
      this.getAgentCommision(
        this.purchaseBookingForm.get('is_rate_percentage')?.value,
        e.target.value,
        this.total_item_amt
      );
    }
  }
  onCancelForm() {
    this.cancelFlag = false;
    this.purchaseBookingForm.reset();
    this.handleCancel.emit(false);
  }

  agentIsRatePercenatgeChange(e: any) {
    if (e.target.checked == true) {
      if (
        parseFloat(this.purchaseBookingForm.get('agent_commision')?.value) > 100
      ) {
        this.purchaseBookingForm.get('agent_commision')?.setValue('');
      }
    }

    this.getAgentCommision(
      e.target.checked,
      this.purchaseBookingForm.get('agent_commision')?.value,
      this.total_item_amt
    );
  }

  get_data_by_gate_pass(gate_pass_ref_id: any) {
    this.purchaseBookingService
      .getSupplierGatepassData('Supplier_GatePass', gate_pass_ref_id)
      .subscribe({
        next: (data1: any) => {
          this.purchaseBookingForm
            .get('agent_ref_id')
            ?.setValue(data1[0]['agent_ref_id']);

          this.fpcService.getAllMasterData('Delivery Terms').subscribe({
            next: (data4: any) => {
              const del_type = data4.filter((d: any) => d.master_key == 'Spot');

              if (del_type[0]['id'] == data1[0]['delivery_type_id']) {
                this.purchaseBookingForm
                  .get('transportation_charges')
                  ?.disable();
                this.fpcService
                  .getAllMasterData('Transportation Cost')
                  .subscribe({
                    next: (data4: any) => {
                      this.transporation_amt =
                        data1[0]['distance'] * data4[0]['master_value'];
                      this.purchaseBookingForm
                        .get('transportation_charges')
                        ?.setValue(this.transporation_amt.toFixed(2));
                    },
                    error: (e) => {
                      this.showSwalMassage(
                        this.errorCodes.getErrorMessage(JSON.parse(e).status),
                        'error'
                      );
                    },
                  });
              } else {
                this.purchaseBookingForm
                  .get('transportation_charges')
                  ?.setValue(0);
                this.purchaseBookingForm
                  .get('transportation_charges')
                  ?.disable();
              }
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });

          if (data1[0]['agent_ref_id'] != null) {
            this.purchaseorderService
              .getDynamicData('agent_all_data', data1[0]['agent_ref_id'])
              .subscribe({
                next: (data2: any) => {
                  this.purchaseBookingForm
                    .get('agent_commision')
                    ?.setValue(data2[0]['commission']);
                  this.purchaseBookingForm
                    .get('is_rate_percentage')
                    ?.setValue(data2[0]['is_rate_percentage']);
                  this.getAgentCommision(
                    data2[0]['is_rate_percentage'],
                    data2[0]['commission'],
                    this.total_item_amt
                  );
                },
                error: (e) => {
                  this.showSwalMassage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    'error'
                  );
                },
              });
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  GetgrnData(grn_ref_id: any) {
    // if(this.SupplierQC[i]['grn_ref_id']=='null' || this.SupplierQC[i]['grn_ref_id']==undefined ||  this.SupplierQC[i]['grn_ref_id']=='')
    // {}
    this.purchaseBookingService
      .getSupplierGRNData('Supplier_GRN', grn_ref_id)
      .subscribe({
        next: (data: any) => {
          this.message = '';
          this.GRNData = data;
          //   this.purchaseBookingForm.get('grn_ref_id')?.setValue( this.GRNData[0]['id'] )
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  QCChange(val: any, SupplierQCData: any) {
    this.purchaseBookingForm.setControl(
      'purchase_booking_details',
      this.setExistingArray1([], 'true', 'supplier_change')
    );
    this.formArray.push(this.initialitemRow());
    this.total_transaction_amt = 0;
    this.purchaseBookingForm.get('grn_ref_id')?.setValue('');
    this.purchaseBookingForm.get('txn_currency_total_amount')?.setValue(0);
    // this.purchaseBookingForm.get('po_ref_id')?.setValue('');
    this.purchaseBookingForm.get('po_type_ref_id')?.setValue('');
    this.purchaseBookingForm.get('so_ref_id')?.setValue('');
    this.purchaseBookingForm
      .get('txn_currency_insurance_txn_charges')
      ?.setValue(0);
    this.purchaseBookingForm
      .get('txn_currency_packing_forwarding_charges')
      ?.setValue(0);
    this.purchaseBookingForm
      .get('txn_currency_total_other_txn_charges')
      ?.setValue(0);
    this.purchaseBookingForm.get('txn_currency_discount_percent')?.setValue(0);
    this.purchaseBookingForm.get('txn_currency_discount_amount')?.setValue(0);
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );

    if (val == '' || val == 'None' || val == undefined) {
      this.DisabledAdd = true;

      this.POData = [];
      this.filterPOData = [];
      this.purchaseBookingForm.get('txn_currency_discount_percent')?.disable();
      this.purchaseBookingForm
        .get('txn_currency_insurance_txn_charges')
        ?.disable();
      this.purchaseBookingForm
        .get('txn_currency_packing_forwarding_charges')
        ?.disable();
      this.purchaseBookingForm.get('agent_commision')?.disable();
      // this.purchaseBookingForm.get('agent_commision_amt')?.disable()
      this.purchaseBookingForm.get('transportation_charges')?.disable();
      this.purchaseBookingForm.get('agent_ref_id')?.disable();
      this.purchaseBookingForm.get('is_rate_percentage')?.disable();
      this.purchaseBookingForm.get('supplier_payment_terms_ref_id')?.disable();
      for (let i = 0; i < gridRow1.length; i++) {
        gridRow1.at(i).get('empty_bag_weight')?.disable();
        gridRow1.at(i).get('labour_charges')?.disable();
        gridRow1.at(i).get('tax_rate')?.disable();
        gridRow1.at(i).get('is_gst_set_off')?.disable();
      }
    } else {
      this.DisabledAdd = false;

      if (!this.isDisable) {
        for (let i = 0; i < gridRow1.length; i++) {
          gridRow1.at(i).get('empty_bag_weight')?.enable();
          gridRow1.at(i).get('labour_charges')?.enable();
          gridRow1.at(i).get('tax_rate')?.enable();
          gridRow1.at(i).get('is_gst_set_off')?.enable();
        }
      }
      this.purchaseBookingForm.get('txn_currency_discount_percent')?.enable();
      this.purchaseBookingForm
        .get('txn_currency_insurance_txn_charges')
        ?.enable();
      this.purchaseBookingForm
        .get('txn_currency_packing_forwarding_charges')
        ?.enable();
      this.purchaseBookingForm.get('agent_commision')?.enable();
      // this.purchaseBookingForm.get('agent_commision_amt')?.enable()
      this.purchaseBookingForm.get('transportation_charges')?.enable();
      this.purchaseBookingForm.get('agent_ref_id')?.enable();
      this.purchaseBookingForm.get('is_rate_percentage')?.enable();
      this.purchaseBookingForm.get('supplier_payment_terms_ref_id')?.enable();
    }
    this.purchaseBookingForm.get('agent_commision')?.setValue(0);
    this.purchaseBookingForm.get('agent_commision_amt')?.setValue(0);
    this.purchaseBookingForm.get('transportation_charges')?.setValue(0);
    this.purchaseBookingForm.get('agent_ref_id')?.setValue('');
    // this.purchaseBookingForm.get('is_rate_percentage')?.setValue('');

    const qc = SupplierQCData.filter((x: any) => x.id == val);
    if (qc.length > 0) {
      this.purchaseBookingForm
        .get('division_ref_id')
        ?.setValue(qc[0]['division_ref_id']);
      this.purchaseBookingForm
        .get('department_ref_id')
        ?.setValue(qc[0]['department_ref_id']);
      this.purchaseBookingForm
        .get('sale_type_ref_id')
        ?.setValue(qc[0]['sale_type_ref_id']);
      this.purchaseBookingForm
        .get('location_ref_id')
        ?.setValue(qc[0]['location_ref_id']);

      this.purchaseBookingForm
        .get('gate_pass_no')
        ?.setValue(qc[0].gate_pass_no);
    }

    for (let i = 0; i < this.SupplierQC.length; i++) {
      if (this.SupplierQC[i]['id'] == val) {
        if (
          this.SupplierQC[i]['gate_pass_ref_id'] == 'null' ||
          this.SupplierQC[i]['gate_pass_ref_id'] == undefined ||
          this.SupplierQC[i]['gate_pass_ref_id'] == ''
        ) {
          console.log('====================================');
          console.log('No Gate Pass');
          console.log('====================================');
        } else {
          this.get_data_by_gate_pass(this.SupplierQC[i]['gate_pass_ref_id']);
        }

        if (
          this.SupplierQC[i]['grn_ref_id'] == 'null' ||
          this.SupplierQC[i]['grn_ref_id'] == undefined ||
          this.SupplierQC[i]['grn_ref_id'] == ''
        ) {
          console.log('====================================');
          console.log('No GRN');
          console.log('====================================');
        } else {
          this.purchaseBookingService
            .getSupplierGRNData(
              'Supplier_GRN',
              this.SupplierQC[i]['grn_ref_id']
            )
            .subscribe({
              next: (data: any) => {
                this.message = '';
                this.GRNData = data;
                this.purchaseBookingForm
                  .get('grn_ref_id')
                  ?.setValue(this.GRNData[0]['id']);

                if (
                  this.GRNData[0]['gate_pass_ref_id'] == 'null' ||
                  this.GRNData[0]['gate_pass_ref_id'] == undefined ||
                  this.GRNData[0]['gate_pass_ref_id'] == ''
                ) {
                  console.log('====================================');
                  console.log('No Gate Pass');
                  console.log('====================================');
                } else {
                  this.get_data_by_gate_pass(
                    this.GRNData[0]['gate_pass_ref_id']
                  );
                }
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
        }
        this.DisabledAdd = false;
        this.purchaseBookingForm.setControl(
          'purchase_booking_details',
          this.setExistingArray1(this.SupplierQC[i]['qc_details'], 'true', '')
        );

        if (
          this.SupplierQC[i]['so_ref_id'] == 'null' ||
          this.SupplierQC[i]['so_ref_id'] == undefined ||
          this.SupplierQC[i]['so_ref_id'] == ''
        ) {
          console.log('====================================');
          console.log('No SO Ref ID');
          console.log('====================================');
        } else {
          this.GetSOTypeData(this.SupplierQC[i]['so_ref_id']);
        }
      }
    }
  }

  GetSOTypeData(key: any) {
    this.purchaseBookingService
      .getSupplierSOData('Supplier_SO', key)
      .subscribe({
        next: (data: any) => {
          this.SalesOrderData = data;
          this.purchaseBookingForm
            .get('so_ref_id')
            ?.setValue(this.SalesOrderData[0]['id']);
          // this.purchaseBookingForm.get('so_type_ref_id')?.setValue( this.SalesOrderData[0]['customer_po_type_ref_id'] )
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }
  Freight_change(e: any, i: any) {
    let val = e.target.value;
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );

    if (val == '' || val == 'None' || val == undefined) {
      val = 0;
    }

    let amt: any = 0;
    this.total_item_amt = 0;
    let tax_amt: any = 0;
    const qun = parseFloat(gridRow1.at(i).get('alternate_net_qty')?.value);
    const rate = parseFloat(gridRow1.at(i).get('rate')?.value);

    if (val > qun * rate) {
      Swal.fire('Labour Charges Not More Than Transaction Amount');
      gridRow1.at(i).get('labour_charges')?.setValue('');
    } else {
      gridRow1
        .at(i)
        .get('txn_currency_amount')
        ?.setValue((qun * rate - parseFloat(val)).toFixed(2));
      const i_rate = parseFloat(
        gridRow1.at(i).get('txn_currency_igst_rate')?.value
      );
      const cr = parseFloat(
        gridRow1.at(i).get('txn_currency_cgst_rate')?.value
      );
      const sr = parseFloat(
        gridRow1.at(i).get('txn_currency_sgst_rate')?.value
      );

      if (i_rate && i_rate != 0) {
        this.calculate_tax_amount(i, 'IGST');
      }
      if (cr && cr != 0) {
        this.calculate_tax_amount(i, 'CGST');
      }
      if (sr && sr != 0) {
        this.calculate_tax_amount(i, 'SGST');
      }
      if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
        // tax_amt=(parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) * parseFloat(gridRow1.at(i).get('tax_rate')?.value) )/100
        // gridRow1.at(i).get('txn_currency_tax_amount')?.setValue(tax_amt.toFixed(2))
      } else {
        tax_amt = 0;
      }
      gridRow1
        .at(i)
        .get('txn_currency_total_txn_amount')
        ?.setValue(
          (qun * rate - parseFloat(val) + parseFloat(tax_amt)).toFixed(2)
        );

      for (let i = 0; i < gridRow1.length; i++) {
        if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
          tax_amt =
            (parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) *
              parseFloat(gridRow1.at(i).get('tax_rate')?.value)) /
            100;
        } else {
          tax_amt = 0;
        }
        amt =
          parseFloat(amt) +
          parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
          parseFloat(tax_amt);
        this.total_item_amt =
          parseFloat(this.total_item_amt) +
          parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
          parseFloat(tax_amt);
        this.purchaseBookingForm
          .get('txn_currency_total_amount')
          ?.setValue(amt.toFixed(2));
      }
      // this.purchaseBookingForm
      //   .get('txn_currency_total_amount')
      //   ?.setValue(amt.toFixed(2));
      this.getAgentCommision(
        this.purchaseBookingForm.get('is_rate_percentage')?.value,
        this.purchaseBookingForm.get('agent_commision')?.value,
        this.total_item_amt
      );
    }
    this.setMasterTotalAmount();
  }

  Insurance_change(e: any) {
    let val;
    this.insurance_charges = 0;
    if (
      e.target.value == '' ||
      e.target.value == 'None' ||
      e.target.value == undefined
    ) {
      val = 0;
      this.total_other_charges =
        this.insurance_charges + this.p_f_charges - this.discount_amt;
    } else {
      val = e.target.value;
      this.insurance_charges =
        parseFloat(this.insurance_charges) + parseFloat(val);
      this.total_other_charges =
        parseFloat(this.insurance_charges) +
        parseFloat(this.p_f_charges) -
        parseFloat(this.discount_amt);
    }

    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    let amt: any = 0;
    let tax_amt: any = 0;

    for (let i = 0; i < gridRow1.length; i++) {
      if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
        tax_amt =
          (parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) *
            parseFloat(gridRow1.at(i).get('tax_rate')?.value)) /
          100;
      } else {
        tax_amt = 0;
      }
      amt =
        parseFloat(amt) +
        parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
        parseFloat(tax_amt);
    }
    this.total_transaction_amt = amt;
    const amt1 = parseFloat(this.total_transaction_amt);
    this.purchaseBookingForm
      .get('txn_currency_total_other_txn_charges')
      ?.setValue(parseFloat(this.total_other_charges).toFixed(2));
    this.purchaseBookingForm
      .get('txn_currency_total_amount')
      ?.setValue(amt1.toFixed(2));
  }

  p_f_change(e: any) {
    let val;
    this.p_f_charges = 0;
    if (
      e.target.value == '' ||
      e.target.value == 'None' ||
      e.target.value == undefined
    ) {
      val = 0;
      this.total_other_charges =
        this.insurance_charges + this.p_f_charges - this.discount_amt;
    } else {
      val = e.target.value;
      this.p_f_charges = parseFloat(this.p_f_charges) + parseFloat(val);
      this.total_other_charges =
        parseFloat(this.insurance_charges) +
        parseFloat(this.p_f_charges) -
        parseFloat(this.discount_amt);
    }

    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    let amt: any = 0;
    let tax_amt: any = 0;

    for (let i = 0; i < gridRow1.length; i++) {
      if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
        tax_amt =
          (parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) *
            parseFloat(gridRow1.at(i).get('tax_rate')?.value)) /
          100;
      } else {
        tax_amt = 0;
      }
      amt =
        parseFloat(amt) +
        parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
        parseFloat(tax_amt);
    }
    this.total_transaction_amt = amt;
    const amt1 = parseFloat(this.total_transaction_amt);

    //amt= parseFloat(this.total_transaction_amt) + parseFloat(this.total_other_charges)

    this.purchaseBookingForm
      .get('txn_currency_total_other_txn_charges')
      ?.setValue(this.total_other_charges.toFixed(2));
    this.purchaseBookingForm
      .get('txn_currency_total_amount')
      ?.setValue(amt1.toFixed(2));
  }

  discount_change(e: any) {
    let val;
    this.discount_amt = 0;
    if (
      e.target.value == '' ||
      e.target.value == 'None' ||
      e.target.value == undefined
    ) {
      val = 0;
      this.total_other_charges =
        this.freight_charges +
        this.insurance_charges +
        this.p_f_charges -
        this.discount_amt;
      this.purchaseBookingForm
        .get('txn_currency_discount_amount')
        ?.setValue(this.discount_amt.toFixed(2));
    } else {
      val = e.target.value;
      const gridRow1 = <FormArray>(
        this.purchaseBookingForm.get('purchase_booking_details')
      );
      let amt: any = 0;

      for (let i = 0; i < gridRow1.length; i++) {
        amt =
          parseFloat(amt) +
          parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value);
      }
      const cal_dis_amt = (amt * parseFloat(val)) / 100;
      this.discount_amt = parseFloat(this.discount_amt) + cal_dis_amt;
      this.purchaseBookingForm
        .get('txn_currency_discount_amount')
        ?.setValue(this.discount_amt.toFixed(2));
      this.total_other_charges =
        parseFloat(this.freight_charges) +
        parseFloat(this.insurance_charges) +
        parseFloat(this.p_f_charges) -
        parseFloat(this.discount_amt);
    }
    const amt = parseFloat(this.total_transaction_amt);
    this.purchaseBookingForm
      .get('txn_currency_total_other_txn_charges')
      ?.setValue(this.total_other_charges.toFixed(2));
    this.purchaseBookingForm
      .get('txn_currency_total_amount')
      ?.setValue(amt.toFixed(2));
  }

  get_supplier_data() {
    this.purchaseorderService
      .getDynamicData('supplier', 'company_name')
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              data[i]['key'] =
                data[i]['key'] +
                '-' +
                data[i]['mobile_no'] +
                '/' +
                data[i]['ref_type'];
            }
          }
          this.SupplierData = data;
          this.filterSupplierData = this.SupplierData.slice();
        },
      });
  }

  viewEditRecord(row1: any) {
    this.DisabledAdd = false;
    this.isDisable = true;
    this.buttonName = 'View';
    this.message = '';
    this.message1 = '';

    this.purchaseBookingService.getSupplierAllQCData().subscribe({
      next: (data: any) => {
        if (data.length > 0) {
          this.SupplierQC = data;
          this.filterSupplierQC = this.SupplierQC.slice();
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.purchaseBookingService.getPurchaseBookDataById(row1.id).subscribe({
      next: (row: any) => {
        this.GetPOTypeData();
        //   this.GetQcData(row.supplier_ref_id)
        this.QCChange(row.qc_ref_id, this.SupplierQC);
        this.get_supplier_data();
        this.getSupplierPOData(row.supplier_ref_id);
        if (
          row1.so_ref_id == 'null' ||
          row1.so_ref_id == undefined ||
          row1.so_ref_id == ''
        ) {
          console.log('====================================');
          console.log('No SO Ref ID');
          console.log('====================================');
        } else {
          this.GetSOTypeData(row1.so_ref_id);
        }

        if (
          row1.grn_ref_id == 'null' ||
          row1.grn_ref_id == undefined ||
          row1.grn_ref_id == ''
        ) {
          console.log('====================================');
          console.log('No GRN Ref Id');
          console.log('====================================');
        } else {
          this.GetgrnData(row1.grn_ref_id);
        }

        //       this.viewBtn = false;
        // let employee_authority = localStorage.getItem('employee_authority');
        // this.roleSecurityService.getWorkflowBtn(employee_authority, this.screenName, row.gate_pass_no).subscribe((res: any) => {
        //   this.myTemplate = res;
        // })

        this.purchaseBookingService
          .getSupplierBillNoData('Supplier_Bill_No', row.supplier_ref_id)
          .subscribe({
            next: (data: any) => {
              this.SupplierBillNo = data;
              this.filterSupplierBillNo = this.SupplierBillNo.slice();

              this.insurance_charges = row.txn_currency_insurance_txn_charges;
              this.p_f_charges = row.txn_currency_packing_forwarding_charges;

              this.purchaseBookingForm.patchValue({
                id: row.id,
                transaction_id: row.transaction_id,
                transaction_ref_no: row.transaction_ref_no,
                transaction_type_id: row.transaction_type_id,
                period: row.period,
                ledger_group: row.ledger_group,
                fiscal_year: row.fiscal_year,
                created_by: row.created_by,
                base_currency: row.base_currency,
                transaction_date: row.transaction_date,
                base_currency_total_amount: row.base_currency_total_amount,
                tenant_id: row.tenant_id,
                supplier_ref_id: row.supplier_ref_id,
                supplier_ref_type: row.supplier_ref_type,
                txn_currency_discount_amount: row.txn_currency_discount_amount,
                supplier_bill_no: row.supplier_bill_no,
                supplier_bill_date: row.supplier_bill_date,
                grn_ref_id: row.grn_ref_id,
                txn_currency: row.txn_currency,
                txn_currency_discount_percent:
                  row.txn_currency_discount_percent,
                qc_ref_id: row.qc_ref_id,
                po_ref_id: row.po_ref_id,
                supplier_payment_terms_ref_id:
                  row.supplier_payment_terms_ref_id,
                po_type_ref_id: row.po_type_ref_id,
                txn_currency_insurance_txn_charges:
                  row.txn_currency_insurance_txn_charges,
                txn_currency_packing_forwarding_charges:
                  row.txn_currency_packing_forwarding_charges,
                guratxn_currency_clearing_chargesntee:
                  row.txn_currency_clearing_charges,
                conversion_rate: row.conversion_rate,
                txn_currency_total_other_txn_charges:
                  row.txn_currency_total_other_txn_charges,
                total_other_charges_tax_amount:
                  row.total_other_charges_tax_amount,
                txn_currency_total_amount: row.txn_currency_total_amount,
                txn_currency_balance_total_amount:
                  row.txn_currency_balance_total_amount,
                is_active: row.is_active,
                is_deleted: row.is_deleted,
                so_ref_id: row.so_ref_id,
                agent_ref_id: row.agent_ref_id,
                so_type_ref_id: row.so_type_ref_id,
                agent_commision: row.agent_commision,
                agent_commision_amt: row.agent_commision_amt,
                is_rate_percentage: row.is_rate_percentage,
                transportation_charges: parseFloat(
                  row.transportation_charges
                ).toFixed(2),
                division_ref_id: row.division_ref_id,
                department_ref_id: row.department_ref_id,
                sale_type_ref_id: row.sale_type_ref_id,
                location_ref_id: row.location_ref_id,
                gate_pass_no: row.gate_pass_no,
                workflow_status: row.workflow_status,
              });

              this.purchaseorderService
                .getDynamicData('agent_all_data', row.agent_ref_id)
                .subscribe({
                  next: (data2: any) => {
                    this.getAgentCommision(
                      row.is_rate_percentage,
                      row.agent_commision,
                      this.total_item_amt
                    );
                  },
                  error: (e) => {
                    this.showSwalMassage(
                      this.errorCodes.getErrorMessage(JSON.parse(e).status),
                      'error'
                    );
                  },
                });
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });

        const ItemRow = row.purchase_booking_details.filter(function (
          data: any
        ) {
          return data.is_deleted == false && data.is_active == true;
        });

        this.purchaseBookingForm.setControl(
          'purchase_booking_details',
          this.setExistingArray1(ItemRow, 'false', '')
        );
        this.purchaseBookingForm.disable();
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.purchaseBookingForm.disable();
    }
  }

  openAlert(event: any) {
    this.cnt = this.cnt + 1;
    if (this.cnt == 1) {
      if (event.target.id == 'SendBack') {
        this.purchaseBookingForm.controls['remark']?.setValidators([
          Validators.required,
        ]);
      }
      this.onSubmit(event.target.id);
    }
  }

  ngAfterContentChecked() {
    if (this.elementRef.nativeElement.querySelector('#myTemplate')) {
      this.showLoader = true;
      this.elementRef.nativeElement
        .querySelector('#myTemplate')
        .addEventListener('click', this.openAlert.bind(this));
    }
  }

  onResetForm() {
    this.initializeForm();
  }

  handleCancelForm() {
    this.cancelFlag = false;
    this.purchaseBookingForm.reset();
    this.handleCancel.emit(false);
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
  onSubmit(btn: any) {
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    console.log(gridRow1, 'PIYUSHAAAAAAAAAAAAAa');

    ////rate validation//
    for (let index = 0; index < gridRow1.length; index++) {
      const rate = gridRow1.at(index).get('rate')?.value;

      if (!rate) {
        this.showSwalMassage('Please Enter Rate', 'warning');
        return;
      }
    }
    //////GST Validation//////

    const purchaseBookingDetailsArray = this.purchaseBookingForm.get(
      'purchase_booking_details'
    ) as FormArray;

    console.log(purchaseBookingDetailsArray, '@@@@@@ PURCHASE ID CHECK');

    let isAnyRateEntered = false;
    const supplierRefType =
      this.purchaseBookingForm.get('supplier_ref_type')?.value;

    console.log(supplierRefType, 'SUPPLIER TYPE');

    if (supplierRefType === 'Supplier') {
      for (let i = 0; i < purchaseBookingDetailsArray.length; i++) {
        const purchaseBookingDetailsGroup = purchaseBookingDetailsArray.at(
          i
        ) as FormGroup;

        const igstRate = purchaseBookingDetailsGroup.get(
          'txn_currency_igst_rate'
        )?.value;
        const cgstRate = purchaseBookingDetailsGroup.get(
          'txn_currency_cgst_rate'
        )?.value;
        const sgstRate = purchaseBookingDetailsGroup.get(
          'txn_currency_sgst_rate'
        )?.value;

        if (igstRate || cgstRate || sgstRate) {
          isAnyRateEntered = true;
          break;
        }
      }

      if (!isAnyRateEntered) {
        this.showSwalMassage('Please Enter GST Rate', 'warning');
      }
    }

    /////////
    for (
      let index = 0;
      index < this.purchaseBookingForm.value.purchase_booking_details.length;
      index++
    ) {
      this.purchaseBookingForm.value.purchase_booking_details[
        index
      ].purchase_booking_sub_details = this.purchase_sub_details[index];
    }
    if (this.purchaseBookingForm.invalid) {
      const invalid = [];
      const controls = this.purchaseBookingForm.controls;
      const fc: any = this.purchaseBookingForm.controls;
      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }
      for (let i = 0; i < fc.purchase_booking_details.controls.length; i++) {
        const md = fc.purchase_booking_details.controls[i].controls;
        if (md.invalid) {
          md.markAsTouched();
        }
      }
      return;
    } else {
      const sub_details_exists =
        this.purchaseBookingForm.value.purchase_booking_details.filter(
          (x: any) => x.purchase_booking_sub_details == null
        );
      if (sub_details_exists.length > 0) {
        if (sub_details_exists[0].no_of_bags != '') {
          // this.showSwalmessage(
          //   'Please fill Quantity Details',
          //   '',
          //   'error',
          //   false
          // );
          this.showSwalMassage('Please fill Quantity Details', 'warning');
        } else {
          this.showSwalmessage(
            'Please fill all required Fields',
            '',
            'error',
            false
          );
        }
      } else {
        const val = this.datepipe.transform(
          this.purchaseBookingForm.get('transaction_date')?.value,
          'yyyy-MM-dd'
        );
        this.purchaseBookingForm.get('transaction_date')?.setValue(val);

        const val1 = this.datepipe.transform(
          this.purchaseBookingForm.get('supplier_bill_date')?.value,
          'yyyy-MM-dd'
        );
        this.purchaseBookingForm.get('supplier_bill_date')?.setValue(val1);

        const gridRow1 = <FormArray>(
          this.purchaseBookingForm.get('purchase_booking_details')
        );
        if (this.isGstSetOff) {
          for (let index = 0; index < gridRow1.length; index++) {
            const i_rate = parseFloat(
              gridRow1.at(index).get('txn_currency_igst_rate')?.value
            );
            const c_rate = parseFloat(
              gridRow1.at(index).get('txn_currency_cgst_rate')?.value
            );
            const s_rate = parseFloat(
              gridRow1.at(index).get('txn_currency_sgst_rate')?.value
            );

            if (
              (!i_rate && !c_rate && !s_rate) ||
              (i_rate == 0 && c_rate == 0 && s_rate == 0)
            ) {
              gridRow1
                .get('txn_currency_igst_rate')
                ?.addValidators(Validators.required);
              gridRow1
                .get('txn_currency_igst_rate')
                ?.setErrors({ required: true });
              return;
            }
          }
        }

        const payload = this.purchaseBookingForm.getRawValue();

        for (
          let index = 0;
          index <
          this.purchaseBookingForm.value.purchase_booking_details.length;
          index++
        ) {
          payload.purchase_booking_details[index].purchase_booking_sub_details =
            this.purchase_sub_details[index];
        }

        payload.workflow_status = btn;
        console.log(payload);

        this.handleSave.emit(payload);
      }
    }
  }

  GetPOTypeData() {
    this.fpcService.getAllMasterData('PO Type').subscribe({
      next: (data: any) => {
        this.POTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  GetQcData(val: any) {
    this.purchaseBookingService
      .getSupplierQCData('Supplier_QC', val)
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            this.SupplierQC = data;
            this.filterSupplierQC = this.SupplierQC.slice();
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  EmptyBagQtyChange(e: any, i: any) {
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );

    if (
      e.target.value == '' ||
      e.target.value == 'null' ||
      e.target.value == undefined
    ) {
      const qty = gridRow1.at(i).get('alternate_qty')?.value;
      gridRow1.at(i).get('alternate_net_qty')?.setValue(qty.toFixed(2));
      gridRow1
        .at(i)
        .get('txn_currency_amount')
        ?.setValue((qty * gridRow1.at(i).get('rate')?.value).toFixed(2));
      gridRow1
        .at(i)
        .get('txn_currency_total_txn_amount')
        ?.setValue((qty * gridRow1.at(i).get('rate')?.value).toFixed(2));
      this.setMasterTotalAmount();
    } else {
      let tax_amt: any = 0;
      if (
        parseFloat(e.target.value) >
        parseFloat(gridRow1.at(i).get('alternate_qty')?.value)
      ) {
        Swal.fire('Empty Bag Weight should  not exceed than Quantity');
        gridRow1.at(i).get('empty_bag_weight')?.setValue('');
      } else {
        const qty = gridRow1.at(i).get('alternate_qty')?.value - e.target.value;
        gridRow1.at(i).get('alternate_net_qty')?.setValue(qty.toFixed(2));
        gridRow1
          .at(i)
          .get('txn_currency_amount')
          ?.setValue(
            (qty * parseFloat(gridRow1.at(i).get('rate')?.value)).toFixed(2)
          );

        if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
          // tax_amt=(parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) * parseFloat(gridRow1.at(i).get('tax_rate')?.value) )/100
          // gridRow1.at(i).get('txn_currency_tax_amount')?.setValue(tax_amt.toFixed(2))
        } else {
          tax_amt = 0;
        }

        gridRow1
          .at(i)
          .get('txn_currency_total_txn_amount')
          ?.setValue(
            (
              qty * parseFloat(gridRow1.at(i).get('rate')?.value) +
              tax_amt
            ).toFixed(2)
          );
        const i_rate = parseFloat(
          gridRow1.at(i).get('txn_currency_igst_rate')?.value
        );
        const cr = parseFloat(
          gridRow1.at(i).get('txn_currency_cgst_rate')?.value
        );
        const sr = parseFloat(
          gridRow1.at(i).get('txn_currency_sgst_rate')?.value
        );
        if (i_rate && i_rate != 0) {
          this.calculate_tax_amount(i, 'IGST');
        }
        if (cr && cr != 0) {
          this.calculate_tax_amount(i, 'CGST');
        }
        if (sr && sr != 0) {
          this.calculate_tax_amount(i, 'SGST');
        }
        this.setMasterTotalAmount();
      }
      let amt: any = 0;
      this.total_item_amt = 0;
      this.total_qty = 0;
      let qty: any = 0;

      for (let i = 0; i < gridRow1.length; i++) {
        if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
          tax_amt =
            (parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) *
              parseFloat(gridRow1.at(i).get('tax_rate')?.value)) /
            100;
        } else {
          tax_amt = 0;
        }
        amt =
          parseFloat(amt) +
          parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
          tax_amt;
        this.total_item_amt =
          parseFloat(this.total_item_amt) +
          parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value);
        qty =
          parseFloat(qty) +
          parseFloat(gridRow1.at(i).get('alternate_net_qty')?.value);
        this.purchaseBookingForm
          .get('txn_currency_total_amount')
          ?.setValue(amt.toFixed(2));
      }

      this.total_qty = qty;
      this.getAgentCommision(
        this.purchaseBookingForm.get('is_rate_percentage')?.value,
        this.purchaseBookingForm.get('agent_commision')?.value,
        this.total_item_amt
      );
      // this.purchaseBookingForm
      //   .get('txn_currency_total_amount')
      //   ?.setValue(amt.toFixed(2));
    }
  }

  addQuantityDetails(index: any) {
    const gridRow = (<FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    )).at(index);
    gridRow.get('alternate_uom')?.enable();
    if (gridRow && gridRow.get('item_ref_id')?.value) {
      // Access gridRow.value['purchase_booking_sub_details'] and other properties safely
      gridRow.value['purchase_booking_sub_details'] = [];
    }

    // gridRow.value['purchase_booking_sub_details'] = []

    // if(!gridRow.get('item_ref_id')?.value){
    //     return
    // }
    if (!Array.isArray(this.rowData)) {
      this.DisabledAdd = false;
      gridRow.get('alternate_uom')?.enable();
      gridRow.value['purchase_booking_sub_details'] =
        this.rowData['purchase_booking_details'][index][
          'purchase_booking_sub_details'
        ];
    }
    if (this.purchase_sub_details[index]) {
      gridRow.value['purchase_booking_sub_details'] =
        this.purchase_sub_details[index];
    }
    gridRow.value['submitBtn'] = this.submitBtn;
    const dialogRef: MatDialogRef<PurchaseBookingDialogComponent> =
      this.dialog.open(PurchaseBookingDialogComponent, {
        data: gridRow,
      });
    dialogRef.afterClosed().subscribe((result: any) => {
      gridRow.get('alternate_uom')?.disable();

      if (result) {
        this.purchase_sub_details[index] = result;
        gridRow.value['purchase_booking_sub_details'] =
          result['purchase_booking_sub_details'];
        const gridRow1 = <FormArray>(
          this.purchaseBookingForm.get('purchase_booking_details')
        );
        let total_no_of_bags: any = 0;
        let total_quantity: any = 0;

        for (let j = 0; j < result.length; j++) {
          total_no_of_bags =
            parseFloat(total_no_of_bags) + parseFloat(result[j]['no_of_bags']);
          total_quantity =
            parseFloat(total_quantity) + parseFloat(result[j]['quantity']);
        }

        if (!Array.isArray(this.rowData)) {
          console.log('Empty Row');
        } else {
          if (
            total_no_of_bags >
            parseFloat(gridRow1.at(index).get('old_no_of_bags')?.value)
          ) {
            Swal.fire({
              title: 'Not allowed',
              text: 'Total No of Bags Should not be more than No of Bags!',
              icon: 'warning',
            });
          } else if (
            total_no_of_bags <
            parseFloat(gridRow1.at(index).get('old_no_of_bags')?.value)
          ) {
            const result = confirm(
              'NO of Bags does not matching with gate pass no of Bags.'
            );

            if (result == true) {
              this.setNewValue(
                gridRow1,
                index,
                total_no_of_bags,
                total_quantity
              );
            }
            // else {
            //   this.showSwalmessage('Please fill Quantity Details', '', 'error', false);
            // }
          } else if (
            total_no_of_bags ==
            parseFloat(gridRow1.at(index).get('old_no_of_bags')?.value)
          )
            this.setNewValue(gridRow1, index, total_no_of_bags, total_quantity);
        }
      }
    });
  }

  setNewValue(
    gridRow1: any,
    index: any,
    total_no_of_bags: any,
    total_quantity: any
  ) {
    gridRow1.at(index).get('no_of_bags')?.setValue(total_no_of_bags.toFixed(2));
    gridRow1
      .at(index)
      .get('alternate_qty')
      ?.setValue(total_quantity.toFixed(2));
    gridRow1
      .at(index)
      .get('alternate_net_qty')
      ?.setValue(total_quantity.toFixed(2));

    if (
      gridRow1.at(index).get('empty_bag_weight')?.value == '' ||
      gridRow1.at(index).get('empty_bag_weight')?.value == undefined
    ) {
      console.log('EMpty Bag Weight');
    } else {
      gridRow1
        .at(index)
        .get('alternate_net_qty')
        ?.setValue(
          (
            parseFloat(gridRow1.at(index).get('alternate_net_qty')?.value) -
            parseFloat(gridRow1.at(index).get('empty_bag_weight')?.value)
          ).toFixed(2)
        );
      gridRow1
        .at(index)
        .get('txn_currency_amount')
        ?.setValue(
          (
            parseFloat(gridRow1.at(index).get('rate')?.value) *
            parseFloat(gridRow1.at(index).get('alternate_net_qty')?.value)
          ).toFixed(2)
        );
    }
    if (
      gridRow1.at(index).get('labour_charges')?.value == '' ||
      gridRow1.at(index).get('labour_charges')?.value == undefined
    ) {
      console.log('====================================');
      console.log('EMpty Labour Charges');
      console.log('====================================');
    } else {
      //gridRow1.at(index).get('net_quantity')?.setValue((parseFloat(gridRow1.at(index).get('net_quantity')?.value) -  parseFloat(gridRow1.at(index).get('empty_bag_weight')?.value)).toFixed(2))
      gridRow1
        .at(index)
        .get('txn_currency_amount')
        ?.setValue(
          (
            parseFloat(gridRow1.at(index).get('rate')?.value) *
              parseFloat(gridRow1.at(index).get('alternate_net_qty')?.value) -
            parseFloat(gridRow1.at(index).get('labour_charges')?.value)
          ).toFixed(2)
        );
    }
    if (
      (gridRow1.at(index).get('empty_bag_weight')?.value == '' ||
        gridRow1.at(index).get('empty_bag_weight')?.value == undefined) &&
      (gridRow1.at(index).get('labour_charges')?.value == '' ||
        gridRow1.at(index).get('labour_charges')?.value == undefined)
    ) {
      gridRow1
        .at(index)
        .get('txn_currency_amount')
        ?.setValue(
          (
            parseFloat(gridRow1.at(index).get('rate')?.value) *
            parseFloat(gridRow1.at(index).get('alternate_net_qty')?.value)
          ).toFixed(2)
        );
    }
    console.log(
      gridRow1.at(index).get('is_gst_set_off')?.value,
      gridRow1.at(index).get('txn_currency_amount')?.value
    );

    if (gridRow1.at(index).get('is_gst_set_off')?.value == true) {
      const tax_amount: any = this.calculate_total_tax_amount();
      gridRow1
        .at(index)
        .get('txn_currency_total_txn_amount')
        ?.setValue(
          (
            parseFloat(tax_amount) +
            parseFloat(gridRow1.at(index).get('txn_currency_amount')?.value)
          ).toFixed(2)
        );

      // (transaction_amount ) * parseFloat(gridRow1.at(i).get('tax_rate')?.value))/100
    } else if (gridRow1.at(index).get('is_gst_set_off')?.value == false) {
      gridRow1
        .at(index)
        .get('txn_currency_total_txn_amount')
        ?.setValue(gridRow1.at(index).get('txn_currency_amount')?.value);
    }

    let transaction_amount: any;
    this.total_transaction_amt = 0;
    let total_amt: any = 0;
    let tax_amount: any = 0;

    for (let i = 0; i < gridRow1.length; i++) {
      const rate = gridRow1.at(i).get('rate')?.value;
      if (
        gridRow1.at(index).get('labour_charges')?.value == '' ||
        gridRow1.at(index).get('labour_charges')?.value == undefined
      ) {
        transaction_amount =
          parseFloat(rate) *
          parseFloat(gridRow1.at(i).get('alternate_net_qty')?.value);
      } else {
        //gridRow1.at(index).get('net_quantity')?.setValue((parseFloat(gridRow1.at(index).get('net_quantity')?.value) -  parseFloat(gridRow1.at(index).get('empty_bag_weight')?.value)).toFixed(2))
        transaction_amount =
          parseFloat(rate) *
            parseFloat(gridRow1.at(i).get('alternate_net_qty')?.value) -
          parseFloat(gridRow1.at(index).get('labour_charges')?.value);
      }

      if (gridRow1.at(index).get('is_gst_set_off')?.value == true) {
        tax_amount = this.calculate_total_tax_amount();
        // (transaction_amount ) * parseFloat(gridRow1.at(i).get('tax_rate')?.value))/100
      }
      gridRow1
        .at(i)
        .get('txn_currency_tax_amount')
        ?.setValue(tax_amount.toFixed(2));

      total_amt =
        parseFloat(transaction_amount) +
        parseFloat(gridRow1.at(i).get('txn_currency_tax_amount')?.value);

      gridRow1
        .at(i)
        .get('txn_currency_total_base_amount')
        ?.setValue(total_amt.toFixed(2));

      this.total_transaction_amt =
        parseFloat(this.total_transaction_amt) +
        parseFloat(transaction_amount) +
        parseFloat(gridRow1.at(i).get('txn_currency_tax_amount')?.value);
    }
    this.purchaseBookingForm
      .get('txn_currency_total_amount')
      ?.setValue(this.total_transaction_amt.toFixed(2));
    this.total_item_amt = 0;
    for (let j = 0; j < gridRow1.length; j++) {
      this.total_item_amt =
        parseFloat(this.total_item_amt) +
        parseFloat(gridRow1.at(j).get('txn_currency_amount')?.value);
    }
    this.getAgentCommision(
      this.purchaseBookingForm.get('is_rate_percentage')?.value,
      this.purchaseBookingForm.get('agent_commision')?.value,
      this.total_item_amt
    );
  }
  clickEvent(e: any, index: any) {
    this.cal_tax_amt(e.checked, index);
  }

  cal_tax_amt(type: any, index: any) {
    console.log(type);

    const gridRow = (<FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    )).at(index);
    const transaction_amount = parseFloat(
      gridRow.get('txn_currency_amount')?.value
    );

    if (type == true) {
      const tax_amount = this.calculate_total_tax_amount();
      // ((transaction_amount ) * parseFloat(gridRow.get('tax_rate')?.value))/100
      gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));
      console.log(transaction_amount, tax_amount);
      const total_amt = transaction_amount + tax_amount;
      console.log(total_amt);
      gridRow
        .get('txn_currency_total_txn_amount')
        ?.setValue(total_amt.toFixed(2));
    } else if (type == false) {
      const tax_amount = 0;
      gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));
      console.log(transaction_amount, tax_amount);

      const total_amt = transaction_amount + tax_amount;
      console.log(total_amt);

      gridRow
        .get('txn_currency_total_txn_amount')
        ?.setValue(total_amt.toFixed(2));
    }
    this.total_transaction_amount = 0;
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );

    for (let j = 0; j < gridRow1.length; j++) {
      const amt = parseFloat(
        gridRow1.at(j).get('txn_currency_total_txn_amount')?.value
      );
      this.total_transaction_amount =
        parseFloat(this.total_transaction_amount.toString()) +
        amt +
        parseFloat(this.total_other_charges);
      this.purchaseBookingForm
        .get('txn_currency_total_amount')
        ?.setValue(this.total_transaction_amount.toFixed(2));
    }

    // this.purchaseBookingForm
    //   .get('txn_currency_total_amount')
    //   ?.setValue(this.total_transaction_amount.toFixed(2));
    this.purchaseBookingForm
      .get('base_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
  }

  calculate_tax_amount(index: any, gst_type: any) {
    console.log('gst_type: ', gst_type);

    if (this.isGstSetOff) {
      const gridRow = (<FormArray>(
        this.purchaseBookingForm.get('purchase_booking_details')
      )).at(index);
      const transaction_amount = parseFloat(
        gridRow.get('txn_currency_amount')?.value
      );
      if (gst_type == 'IGST') {
        let i_rate = gridRow.get('txn_currency_igst_rate')?.value;
        if (!i_rate) {
          i_rate = 0;
        }
        const tax_amount = (transaction_amount * parseFloat(i_rate)) / 100;
        gridRow
          .get('txn_currency_igst_amount')
          ?.setValue(tax_amount.toFixed(2));
        gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));
        const total_amt = transaction_amount + tax_amount;
        gridRow
          .get('txn_currency_total_txn_amount')
          ?.setValue(total_amt.toFixed(2));

        gridRow.get('txn_currency_cgst_amount')?.setValue(0);
        gridRow.get('txn_currency_sgst_amount')?.setValue(0);
        gridRow.get('txn_currency_cgst_rate')?.setValue(0);
        this.setMasterTotalAmount();
        gridRow.get('txn_currency_sgst_rate')?.setValue(0);
        gridRow.get('txn_currency_cgst_rate')?.setValidators(null);
        gridRow.get('txn_currency_sgst_rate')?.setValidators(null);
      }
      if (gst_type == 'CGST') {
        let cr = gridRow.get('txn_currency_cgst_rate')?.value;
        if (!cr) {
          cr = 0;
        }
        const tax_amount = (transaction_amount * parseFloat(cr)) / 100;
        gridRow
          .get('txn_currency_cgst_amount')
          ?.setValue(tax_amount.toFixed(2));
        gridRow
          .get('txn_currency_sgst_amount')
          ?.setValue(tax_amount.toFixed(2));
        const total_tax = tax_amount + tax_amount;
        gridRow.get('txn_currency_tax_amount')?.setValue(total_tax.toFixed(2));

        const total_amt = transaction_amount + total_tax;
        gridRow
          .get('txn_currency_total_txn_amount')
          ?.setValue(total_amt.toFixed(2));

        gridRow.get('txn_currency_sgst_rate')?.setValue(cr);
        gridRow.get('txn_currency_igst_amount')?.setValue(0);
        gridRow.get('txn_currency_igst_rate')?.setValue(0);
        const sgst_val = gridRow.get('txn_currency_sgst_rate')?.value;
        this.setMasterTotalAmount();
        if (!sgst_val) {
          gridRow
            .get('txn_currency_sgst_rate')
            ?.addValidators(Validators.required);
          gridRow.get('txn_currency_sgst_rate')?.setErrors({ required: true });
          gridRow.get('txn_currency_igst_rate')?.setValidators(null);
        }
      }
      if (gst_type == 'SGST') {
        let sr = gridRow.get('txn_currency_sgst_rate')?.value;
        if (!sr) {
          sr = 0;
        }
        const tax_amount = (transaction_amount * parseFloat(sr)) / 100;
        gridRow
          .get('txn_currency_sgst_amount')
          ?.setValue(tax_amount.toFixed(2));
        gridRow
          .get('txn_currency_cgst_amount')
          ?.setValue(tax_amount.toFixed(2));
        const total_tax = tax_amount + tax_amount;
        gridRow.get('txn_currency_tax_amount')?.setValue(total_tax.toFixed(2));
        const total_amt = transaction_amount + total_tax;
        gridRow
          .get('txn_currency_total_txn_amount')
          ?.setValue(total_amt.toFixed(2));
        gridRow.get('txn_currency_igst_amount')?.setValue(0);
        this.setMasterTotalAmount();
        gridRow.get('txn_currency_cgst_rate')?.setValue(sr);
        gridRow.get('txn_currency_igst_rate')?.setValue(0);
        const cgst_val = gridRow.get('txn_currency_cgst_rate')?.value;
        if (!cgst_val) {
          gridRow
            .get('txn_currency_cgst_rate')
            ?.addValidators(Validators.required);
          gridRow.get('txn_currency_cgst_rate')?.setErrors({ required: true });
          gridRow.get('txn_currency_igst_rate')?.setValidators(null);
        }
      }
    }
  }

  rate_change(event: any, index: any) {
    const gridRow = (<FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    )).at(index);
    const qty = parseFloat(gridRow.get('alternate_net_qty')?.value);
    gridRow
      .get('txn_currency_amount')
      ?.setValue((qty * parseFloat(gridRow.get('rate')?.value)).toFixed(2));
    gridRow
      .get('txn_currency_total_txn_amount')
      ?.setValue((qty * parseFloat(gridRow.get('rate')?.value)).toFixed(2));

    const i_rate = gridRow.get('txn_currency_igst_rate')?.value;
    const cr = gridRow.get('txn_currency_cgst_rate')?.value;
    const sr = gridRow.get('txn_currency_sgst_rate')?.value;
    if (i_rate && i_rate != 0) {
      this.calculate_tax_amount(index, 'IGST');
    }
    if (cr && cr != 0) {
      this.calculate_tax_amount(index, 'CGST');
    }
    if (sr && sr != 0) {
      this.calculate_tax_amount(index, 'SGST');
    }

    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    let amt: any = 0;
    // this.total_item_amt = 0;
    // this.total_qty = 0;
    let quantity: any = 0;
    let tax_amt: any = 0;
    for (let i = 0; i < gridRow1.length; i++) {
      if (gridRow1.at(i).get('is_gst_set_off')?.value == true) {
        tax_amt =
          (parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) *
            parseFloat(gridRow1.at(i).get('tax_rate')?.value)) /
          100;
      } else {
        tax_amt = 0;
      }
      amt =
        parseFloat(amt) +
        parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value) +
        tax_amt;
      this.total_item_amt =
        parseFloat(this.total_item_amt) +
        parseFloat(gridRow1.at(i).get('txn_currency_amount')?.value);
      quantity =
        parseFloat(quantity) +
        parseFloat(gridRow1.at(i).get('alternate_net_qty')?.value);
      console.log(
        this.purchaseBookingForm.get('txn_currency_total_amount')?.value
      );
      console.log(amt);
      this.purchaseBookingForm.get('txn_currency_total_amount')?.setValue(amt);
    }
    this.setMasterTotalAmount();
  }

  calculate_total_tax_amount() {
    let txn_total_tax_amount = 0;
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    for (let index = 0; index < gridRow1.length; index++) {
      txn_total_tax_amount = 0;
      const i_rate = parseFloat(
        gridRow1.at(index).get('txn_currency_igst_amount')?.value
      );
      const c_rate = parseFloat(
        gridRow1.at(index).get('txn_currency_cgst_amount')?.value
      );
      const s_rate = parseFloat(
        gridRow1.at(index).get('txn_currency_sgst_amount')?.value
      );

      if (i_rate) {
        txn_total_tax_amount = txn_total_tax_amount + i_rate;
      }
      if (c_rate) {
        txn_total_tax_amount = txn_total_tax_amount + c_rate;
      }
      if (s_rate) {
        txn_total_tax_amount = txn_total_tax_amount + s_rate;
      }
    }

    this.setMasterTotalAmount();

    return txn_total_tax_amount;
  }
  setMasterTotalAmount() {
    let tt = 0;
    const gridRow1 = <FormArray>(
      this.purchaseBookingForm.get('purchase_booking_details')
    );
    for (let index = 0; index < gridRow1.length; index++) {
      const total = parseFloat(
        gridRow1.at(index).get('txn_currency_total_txn_amount')?.value
      );
      tt = total + tt;
    }
    console.log('tt: ', tt);
    this.purchaseBookingForm
      .get('txn_currency_total_amount')
      ?.setValue(tt.toFixed(2));
  }
}
