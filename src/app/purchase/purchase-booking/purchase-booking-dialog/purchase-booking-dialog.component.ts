import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { UntypedFormArray, UntypedFormGroup } from '@angular/forms';

import { PurchaseBookingService } from 'src/app/shared/services/purchase-booking.service';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
export interface DialogData {
  data: any;
}
@Component({
  selector: 'app-purchase-booking-dialog',
  templateUrl: './purchase-booking-dialog.component.html',
  styleUrls: ['./purchase-booking-dialog.component.scss'],
})
export class PurchaseBookingDialogComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<PurchaseBookingDialogComponent>,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private purchaseorderService: PurchaseorderService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  qcParameterdetails!: UntypedFormGroup;
  qualityData: any = [];
  materialStatus: any = [];
  parameter_details: any = [];
  uomName: any;
  DisabledAdd = false;
  disableSubmit = true;
  submitBtn = this.data.value['submitBtn'];

  ngOnInit(): void {
    this.intializeForm();
    console.log(this.data.value);

    this.purchaseorderService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i]['id'] == this.data.value['alternate_uom']) {
            this.uomName = data[i]['key'];
            console.log(this.uomName);
          }
        }
      },
    });

    if (this.data.value['purchase_booking_sub_details']) {
      this.viewRecord(this.data.value);
    }
  }

  intializeForm() {
    this.qcParameterdetails = this.fb.group({
      id: [''],
      purchase_booking_sub_details: this.fb.array([
        this.purchase_booking_sub_details(),
      ]),
    });
  }

  purchase_booking_sub_details() {
    return this.fb.group({
      id: [''],
      no_of_bags: ['', Validators.required],
      quantity: ['', Validators.required],
    });
  }

  get formArrQcp() {
    return this.qcParameterdetails.get(
      'purchase_booking_sub_details'
    ) as UntypedFormArray;
  }
  addNewRow() {
    this.formArrQcp.push(this.purchase_booking_sub_details());
  }

  setExistingArray(initialArray: any): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    this.disableSubmit = false;

    initialArray.forEach((element: any) => {
      formArray.push(
        this.fb.group({
          id: element.id,
          no_of_bags: element.no_of_bags,
          quantity: element.quantity,
        })
      );
    });
    return formArray;
  }

  viewRecord(edata: any) {
    let qcDetailItemRow = edata['purchase_booking_sub_details'].filter(
      function (data: any) {
        return data;
      }
    );

    if (qcDetailItemRow.length >= 1) {
      this.qcParameterdetails.setControl(
        'purchase_booking_sub_details',
        this.setExistingArray(qcDetailItemRow)
      );
    }

    if (this.submitBtn == false) {
      this.qcParameterdetails.disable();
      this.DisabledAdd = true;
    }
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrQcp.removeAt(index);
      return true;
    }
  }

  onCancelForm() {
    this.dialogRef.close();
  }

  onSubmit() {
    console.log(this.qcParameterdetails.invalid);
    console.log(this.qcParameterdetails);

    if (this.qcParameterdetails.invalid) {
      return;
    } else {
      this.dialogRef.close(
        this.qcParameterdetails
          .get('purchase_booking_sub_details')
          ?.getRawValue()
      );
    }
  }
}
