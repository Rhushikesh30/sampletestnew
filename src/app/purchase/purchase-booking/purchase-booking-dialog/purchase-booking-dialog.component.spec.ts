import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseBookingDialogComponent } from './purchase-booking-dialog.component';

describe('PurchaseBookingDialogComponent', () => {
  let component: PurchaseBookingDialogComponent;
  let fixture: ComponentFixture<PurchaseBookingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseBookingDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PurchaseBookingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
