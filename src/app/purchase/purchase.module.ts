import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ComponentsModule } from '../shared/components/components.module';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import { CdkAccordionModule } from '@angular/cdk/accordion';

import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { PurchaseOrderDetailsComponent } from './purchase-order/purchase-order-details/purchase-order-details.component';
import { PurchaseRoutingModule } from './purchase-routing.module';

import { GoodsReceiptNoteComponent } from './goods-receipt-note/goods-receipt-note.component';
import { GoodsReceiptNoteDetailsComponent } from './goods-receipt-note/goods-receipt-note-details/goods-receipt-note-details.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { QcComponent } from './qc/qc.component';
import { QcDetailsComponent } from './qc/qc-details/qc-details.component';
import { PurchaseBookingComponent } from './purchase-booking/purchase-booking.component';
import { PurchaseBookingDetailsComponent } from './purchase-booking/purchase-booking-details/purchase-booking-details.component';
import { GatePassComponent } from './gate-pass/gate-pass.component';
import { GatePassDetailsComponent } from './gate-pass/gate-pass-details/gate-pass-details.component';
import { QcParameterDialogComponent } from './qc/qc-parameter-dialog/qc-parameter-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTimepickerModule } from 'mat-timepicker';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
} from '@danielmoncada/angular-datetime-picker';
import { PaymentComponent } from './payment/payment.component';
import { PaymentDetailsComponent } from './payment/payment-details/payment-details.component';
import { PurchaseBookingDialogComponent } from './purchase-booking/purchase-booking-dialog/purchase-booking-dialog.component';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { DebitNoteComponent } from './debit-note/debit-note.component';
import { DebitNoteDetailsComponent } from './debit-note/debit-note-details/debit-note-details.component';
import { CreditNoteComponent } from './credit-note/credit-note.component';
import { CreditNoteDetailsComponent } from './credit-note/credit-note-details/credit-note-details.component';
import { ExpenseBookingComponent } from './expense-booking/expense-booking.component';
import { ExpenseBookingDetailsComponent } from './expense-booking/expense-booking-details/expense-booking-details.component';
import { EmployeeExpenseComponent } from './employee-expense/employee-expense.component';
import { EmployeeExpenseDetailsComponent } from './employee-expense/employee-expense-details/employee-expense-details.component';
import { NgxMaterialToolsModule } from 'ngx-material-tools';

@NgModule({
  declarations: [
    PurchaseOrderComponent,
    PurchaseOrderDetailsComponent,
    GoodsReceiptNoteComponent,
    GoodsReceiptNoteDetailsComponent,
    PurchaseComponent,
    QcComponent,
    QcDetailsComponent,
    GatePassComponent,
    GatePassDetailsComponent,
    QcParameterDialogComponent,
    PurchaseBookingComponent,
    PurchaseBookingDetailsComponent,
    PaymentComponent,
    PaymentDetailsComponent,
    PurchaseBookingDialogComponent,
    DebitNoteComponent,
    DebitNoteDetailsComponent,
    CreditNoteComponent,
    CreditNoteDetailsComponent,
    ExpenseBookingComponent,
    ExpenseBookingDetailsComponent,
    EmployeeExpenseComponent,
    EmployeeExpenseDetailsComponent,
  ],
  imports: [
    CommonModule,
    PurchaseRoutingModule,
    NgxMaterialToolsModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    MatCheckboxModule,
    ComponentsModule,
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,
    MatDialogModule,
    MatTimepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MomentDateModule,
  ],
})
export class PurchaseModule {}
