import { GrnService } from 'src/app/shared/services/grn.service';
import { SweetAlertService } from 'src/app/shared/services/sweet-alert.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  UntypedFormArray,
} from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { text, index } from 'd3';
export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-purchase-order-details',
  templateUrl: './purchase-order-details.component.html',
  styleUrls: ['./purchase-order-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class PurchaseOrderDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  cancelFlag = true;
  purchaseForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  PurchaseTypeData: any = [];
  FPCData: any = [];
  SalesOrderData: any = [];
  POTypeData: any = [];
  filterFPCData: any = [];
  SupplierTypeData: any = [];
  SupplierTypeBasedData: any = [];
  PaymentTermsTypeData: any = [];
  ModeofDeliveryTypeData: any = [];
  PackageForwardingData: any = [];
  ItemDataData: any = [];
  UOMData: any;
  CurrencyData: any = [];
  SupplierData: any = [];
  ItemSearchData: any[] = [];

  txn_currency: any;
  total_transaction_amount: any = 0;
  filterSalesOrderData: any = [];
  filterSupplierData: any = [];
  supplierExist = false;
  isToggleDisabled = true;
  rolename: any;
  supplierbillfromData: any = [];
  supplierShipfromData: any = [];
  HsnData: any[] = [];

  POItemTypeData: any;
  todayDate = new Date().toJSON().split('T')[0];
  todayDate1 = new Date().toJSON().split('T')[0];

  constructor(
    private formBuilder: FormBuilder,
    private fpcService: FpcSetupService,
    private errorCodes: ErrorCodes,
    public datepipe: DatePipe,
    private sweetAlertService: SweetAlertService,
    private grnService: GrnService,
    private purchaseorderService: PurchaseorderService,
    private dynamic: DynamicFormService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewEditRecord(this.rowData);
    }

    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.rolename = rolename[0];
    if (this.rolename == 'FPC Admin') {
      this.supplierExist = true;
    }

    this.purchaseorderService
      .getDynamicData('supplier', 'company_name')
      .subscribe({
        next: (data: any) => {
          this.supplierExist = true;
          if (data.length > 0) {
            console.log(data);

            for (let i = 0; i < data.length; i++) {
              data[i]['key'] =
                data[i]['key'] +
                '-' +
                data[i]['mobile_no'] +
                '/' +
                data[i]['ref_type'];
            }
          }
          this.SupplierData = data;
          console.log('SupplierData----------------------', this.SupplierData);

          this.filterSupplierData = this.SupplierData.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.fpcService.getAllMasterData('PO Type').subscribe({
      next: (data: any) => {
        for (let i = 0; i < data.length; i++) {
          if (data[i]['master_key'] == 'Domestic') {
            this.purchaseForm.get('po_type')?.setValue(data[i]['id']);
          }
        }
        this.POTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Item Type').subscribe({
      next: (data: any) => {
        this.POItemTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Purchase Order Type').subscribe({
      next: (data: any) => {
        this.PurchaseTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('PO Supplier Type').subscribe({
      next: (data: any) => {
        this.SupplierTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Payment Terms').subscribe({
      next: (data: any) => {
        this.PaymentTermsTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getAllMasterData('Delivery Type').subscribe({
      next: (data: any) => {
        this.ModeofDeliveryTypeData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
    this.purchaseorderService
      .getDynamicData('hsn_sac', 'hsn_sac_no')
      .subscribe({
        next: (data: any) => {
          this.HsnData = data;
        },
        error: (e: any) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.fpcService.getAllMasterData('Package Forwarding').subscribe({
      next: (data: any) => {
        this.PackageForwardingData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.purchaseorderService.getDynamicData('uom', 'uom_code').subscribe({
      next: (data: any) => {
        this.UOMData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        if (this.btnVal === 'Submit' && this.submitBtn) {
          this.txn_currency = data.filter(
            (d: any) => d.country_name == 'India'
          );
          this.purchaseForm
            .get('txn_currency')
            ?.setValue(this.txn_currency[0]['id']);
        }
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.purchaseForm = this.formBuilder.group({
      id: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      created_by: [''],
      base_currency: [''],
      base_currency_total_amount: [''],
      transaction_date: [''],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      purchase_type: [''],
      so_ref_id: [''],
      po_type: ['', Validators.required],
      txn_currency: [''],
      txn_currency_discount_percent: [null],
      txn_currency_interest_percent: [null],
      txn_currency_discount_amount: [null],
      txn_currency_interest_amount: [null],
      txn_currency_total_amount: [{ value: '', disabled: true }],
      gurantee: [''],
      conversion_rate: [1],
      warranty: [''],
      remark: [''],
      supplier_ref_id: ['', Validators.required],
      supplier_payment_terms: ['', Validators.required],
      supplier_delivery_terms: ['', Validators.required],
      packing_forwarding_ref_id: ['', Validators.required],
      supplier_bill_from: ['', Validators.required],
      supplier_ship_from: ['', Validators.required],
      txn_currency_commision_amt: [''],
      base_currency_commision_amt: [''],
      is_active: [true],
      is_deleted: [false],
      po_item_type: ['', Validators.required],
      transportation_charges: [{ value: '', disabled: true }],
      purchase_order_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }
  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.purchaseForm.reset();
      this.handleCancel.emit(false);
    }
  }

  initialitemRow() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      quantity: [''],
      rate: ['', Validators.required],
      uom: [{ value: '', disabled: true }],
      hsn_sac_no: [{ value: '', disabled: true }],
      conversion_rate: [1],
      txn_currency: [''],
      is_gst_set_off: [false],
      txn_currency_igst_rate: [''],
      txn_currency_igst_amount: [''],
      txn_currency_cgst_rate: [''],
      txn_currency_cgst_amount: [''],
      txn_currency_sgst_rate: [''],
      txn_currency_sgst_amount: [''],
      txn_currency_tax_amount: [{ value: '', disabled: true }],
      txn_currency_amount: [{ value: '', disabled: true }],
      txn_currency_custom_duty_charges: [0],
      txn_currency_discount_percent: [0],
      txn_currency_net_rate: [0],
      lead_time: [''],
      expected_delivery_date: [],
      base_currency: [],
      base_currency_amount: [''],
      base_currency_custom_duty_charges: [0],
      base_currency_discount_percent: [0],
      base_currency_net_rate: [0],
      base_currency_tax_amount: [],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      tax_rate: [{ value: '', disabled: true }],
      is_rate_percentage: [{ value: false, disabled: true }],
      commision: [{ value: 0, disabled: true }],
      txn_currency_commision_amt: [{ value: '', disabled: true }],
      base_currency_commision_amt: [''],
      total_amount: [{ value: '', disabled: true }],
      base_total_amount: [''],
      alternate_uom: [''],
      alternate_quantity: ['', Validators.required],
      uom_name: [],
    });
  }

  get formArray() {
    return this.purchaseForm.get('purchase_order_details') as FormArray;
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow());
    const gridRow = <FormArray>this.purchaseForm.get('purchase_order_details');

    for (let i = 0; i < gridRow.length; i++) {
      gridRow
        .at(i)
        .get('txn_currency')
        ?.setValue(this.purchaseForm.get('txn_currency')?.value);
    }
  }

  deleteRow(index: number) {
    if (this.formArray.length == 1) {
      return false;
    } else {
      this.formArray.removeAt(index);
      const gridRow1 = <FormArray>(
        this.purchaseForm.get('purchase_order_details')
      );
      this.total_transaction_amount = 0;
      for (let j = 0; j < gridRow1.length; j++) {
        const amt = gridRow1.at(j).get('txn_currency_amount')?.value;
        this.total_transaction_amount =
          parseFloat(this.total_transaction_amount.toString()) +
          parseFloat(amt);
      }
      this.purchaseForm
        .get('txn_currency_total_amount')
        ?.setValue(this.total_transaction_amount);
      this.purchaseForm
        .get('base_currency_total_amount')
        ?.setValue(this.total_transaction_amount);

      return true;
    }
  }

  viewEditRecord(row1: any) {
    this.purchaseorderService.getPODataById(row1.id).subscribe({
      next: (row: any) => {
        console.log(row);
        console.log(row.is_linked_to_grn, 'link with grn ');

        if (
          row.so_ref_id == '' ||
          row.so_ref_id == undefined ||
          row.so_ref_id == 'None'
        ) {
          this.supplierExist = true;
        } else {
          if (this.rolename == 'Federation Admin') {
            this.purchaseorderService
              .getTenatNameData('tenant_name_get')
              .subscribe({
                next: (data: any) => {
                  this.supplierExist = false;
                  this.FPCData = data;
                  this.filterFPCData = this.FPCData.slice();
                },
                error: (e) => {
                  this.showSwalMassage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    'error'
                  );
                },
              });
          }
        }
        console.log(row.txn_currency_total_amount);

        this.purchaseForm.patchValue({
          id: row.id,
          transaction_id: row.transaction_id,
          transaction_ref_no: row.transaction_ref_no,
          transaction_type_id: row.transaction_type_id,
          period: row.period,
          ledger_group: row.ledger_group,
          fiscal_year: row.fiscal_year,
          created_by: row.created_by,
          base_currency: row.base_currency,
          transaction_date: row.transaction_date,
          base_currency_total_amount: row.base_currency_total_amount,
          tenant_id: row.tenant_id,
          transportation_charges: row.transportation_charges,
          so_ref_id: row.so_ref_id,
          po_type: row.po_type,
          txn_currency: row.txn_currency,
          txn_currency_discount_percent: row.txn_currency_discount_percent,
          txn_currency_interest_percent: row.txn_currency_interest_percent,
          txn_currency_discount_amount: row.txn_currency_discount_amount,
          txn_currency_interest_amount: row.txn_currency_interest_amount,
          txn_currency_total_amount: parseFloat(
            row.txn_currency_total_amount
          ).toFixed(2),
          gurantee: row.gurantee,
          conversion_rate: row.conversion_rate,
          warranty: row.warranty,
          remark: row.remark,
          supplier_ref_id: row.supplier_ref_id,
          supplier_payment_terms: row.supplier_payment_terms,
          supplier_delivery_terms: row.supplier_delivery_terms,
          packing_forwarding_ref_id: row.packing_forwarding_ref_id,
          supplier_bill_from: row.supplier_bill_from,
          supplier_ship_from: row.supplier_ship_from,
          txn_currency_commision_amt: row.txn_currency_commision_amt,
          base_currency_commision_amt: row.base_currency_commision_amt,
          is_active: row.is_active,
          is_deleted: row.is_deleted,
          po_item_type: row.po_item_type,
        });

        this.fpcService.getAllMasterData('Delivery Type').subscribe({
          next: (data: any) => {
            const del_type = data.filter((d: any) => d.master_key == 'Spot');
            if (del_type[0]['id'] == row.supplier_delivery_terms) {
              this.purchaseForm.get('transportation_charges')?.enable();
            } else {
              this.purchaseForm.get('transportation_charges')?.disable();
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

        this.ItemTypeChnage(row.po_item_type, 'InView');
        const ItemRow = row.purchase_order_details.filter(function (data: any) {
          return data.is_deleted == false && data.is_active == true;
        });
        console.log(ItemRow, 'itemrow');

        console.log(row.supplier_bill_from);

        this.getAddress(row.supplier_ref_id, 'inview');

        this.fpcService.getAllMasterData('PO Supplier Type').subscribe({
          next: (data: any) => {
            this.SupplierTypeData = data;
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

        this.purchaseForm.setControl(
          'purchase_order_details',
          this.setExistingArray(ItemRow)
        );
        console.log();

        if (this.submitBtn) {
          if (row.po_status != 'Open') {
            this.purchaseForm.disable();
          }

          this.btnVal = 'Update';
          if (row.is_linked_to_grn == true) {
            this.purchaseForm.disable();
            this.purchaseForm.get('alternate_quantity')?.enable();
          } else {
            this.purchaseForm.enable();
          }
        }
      },
    });
    // this.purchaseForm.get('purchase_order_details')?.value.disabled;
  }

  setExistingArray(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);
    initialArray.forEach((element: any) => {
      console.log(element);

      formArray.push(
        this.formBuilder.group({
          id: element.id,
          // item_ref_id: [{ value: element.item_ref_id, disabled: true }],
          item_ref_id: element.item_ref_id,
          quantity: element.quantity,
          rate: [parseFloat(element.rate).toFixed(2), [Validators.required]],
          alternate_uom: element.alternate_uom,
          uom_name: element.uom_name,
          hsn_sac_no: [{ value: element.hsn_sac_no, disabled: true }],
          conversion_rate: element.conversion_rate,
          txn_currency: element.txn_currency,
          is_gst_set_off: [{ value: element.is_gst_set_off, disabled: true }],
          txn_currency_igst_rate: element.txn_currency_igst_rate,
          txn_currency_igst_amount: element.txn_currency_igst_amount,
          txn_currency_cgst_rate: element.txn_currency_cgst_rate,
          txn_currency_cgst_amount: element.txn_currency_cgst_amount,
          txn_currency_sgst_rate: element.txn_currency_sgst_rate,
          txn_currency_sgst_amount: element.txn_currency_sgst_amount,
          txn_currency_tax_amount: element.txn_currency_tax_amount,
          txn_currency_amount: element.txn_currency_amount,
          txn_currency_custom_duty_charges:
            element.txn_currency_custom_duty_charges,
          txn_currency_discount_percent: element.txn_currency_discount_percent,
          txn_currency_net_rate: element.txn_currency_net_rate,
          lead_time: element.lead_time,
          expected_delivery_date: element.expected_delivery_date,
          base_currency: element.base_currency,
          base_currency_amount: element.base_currency_amount,
          base_currency_custom_duty_charges:
            element.base_currency_custom_duty_charges,
          base_currency_discount_percent:
            element.base_currency_discount_percent,
          base_currency_net_rate: element.base_currency_net_rate,
          base_currency_tax_amount: element.base_currency_tax_amount,
          transaction_id: element.transaction_id,
          transaction_ref_no: element.transaction_ref_no,
          transaction_type_id: element.transaction_type_id,
          period: element.period,
          ledger_group: element.ledger_group,
          fiscal_year: element.fiscal_year,
          tenant_id: element.tenant_id,
          tax_rate: element.tax_rate,
          is_rate_percentage: element.is_rate_percentage,
          commision: element.commision,
          txn_currency_commision_amt: element.txn_currency_commision_amt,
          base_currency_commision_amt: element.base_currency_commision_amt,
          total_amount: element.total_amount,
          base_total_amount: element.base_total_amount,
          created_by: element.created_by,
          updated_by: element.updated_by,
          created_date_time: element.created_date_time,
          updated_date_time: element.updated_date_time,
          uom: element.uom,
          alternate_quantity: [
            parseFloat(element.alternate_quantity).toFixed(2),
            [Validators.required],
          ],
          // alternate_quantity: [  rate: [parseFloat(element.rate).toFixed(2), [Validators.required]],

          //   { value: element.alternate_quantity, enabled: true },
          // ],

          //  [{value:element.alternate_quantity,[ Validators.required}]
        })
      );
    });
    return formArray;
  }

  setExistingArray1(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);
    let tax_rate = 0;
    initialArray.forEach((element: any) => {
      this.purchaseorderService
        .getDynamicData('tax_rate', element.item_ref_id)
        .subscribe({
          next: (data: any) => {
            if (data.length > 0) {
              tax_rate = Number(data[0]['tax_rate']);
            }

            this.purchaseorderService
              .getDynamicData(
                'item_rate_commision_details',
                element.item_ref_id
              )
              .subscribe({
                next: (data: any) => {
                  formArray.push(
                    this.formBuilder.group({
                      id: [],
                      item_ref_id: element.item_ref_id,
                      rate: element.rate,
                      alternate_uom: [
                        { value: element.alternate_uom, disabled: true },
                      ],
                      hsn_sac_no: [
                        { value: element.hsn_sac_no, disabled: true },
                      ],
                      conversion_rate: [1],
                      alternate_quantity: ['', [Validators.required]],
                      txn_currency: [''],
                      is_gst_set_off: [false],
                      txn_currency_igst_rate: [''],
                      txn_currency_igst_amount: [''],
                      txn_currency_cgst_rate: [''],
                      txn_currency_cgst_amount: [''],
                      txn_currency_sgst_rate: [''],
                      txn_currency_sgst_amount: [''],
                      txn_currency_tax_amount: [{ value: '', disabled: true }],
                      txn_currency_amount: [{ value: '', disabled: true }],
                      txn_currency_custom_duty_charges: [0],
                      txn_currency_discount_percent: [0],
                      txn_currency_net_rate: [0],
                      lead_time: [''],
                      expected_delivery_date: [''],
                      base_currency: [''],
                      base_currency_amount: [''],
                      base_currency_custom_duty_charges: [0],
                      base_currency_discount_percent: [0],
                      base_currency_net_rate: [0],
                      base_currency_tax_amount: [''],
                      transaction_id: [''],
                      transaction_ref_no: [''],
                      transaction_type_id: [''],
                      period: [''],
                      ledger_group: [''],
                      fiscal_year: [''],
                      tenant_id: [''],
                      tax_rate: [{ value: tax_rate, disabled: true }],
                      is_rate_percentage: [{ value: false, disabled: true }],
                      commision: [{ value: 0, disabled: true }],
                      txn_currency_commision_amt: [
                        { value: '', disabled: true },
                      ],
                      base_currency_commision_amt: [''],
                      total_amount: [{ value: '', disabled: true }],
                      base_total_amount: [''],
                      uom_name: [],
                    })
                  );
                },
                error: (e) => {
                  this.showSwalMassage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    'error'
                  );
                },
              });
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    });
    return formArray;
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.purchaseForm.reset();
    this.handleCancel.emit(false);
  }

  onSubmit() {
    console.log(this.purchaseForm);
    console.log(this.purchaseForm.value);
    if (this.purchaseForm.invalid) {
      const invalid = [];
      const controls = this.purchaseForm.controls;
      const fc: any = this.purchaseForm.controls;
      console.log('iff');
      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }

      for (let i = 0; i < fc.purchase_order_details.controls.length; i++) {
        const md = fc.purchase_order_details.controls[i].controls;
        if (md.invalid) {
          md.markAsTouched();
        }
      }

      for (let index = 0; index < this.formArray.length; index++) {
        const newForm = this.formArray.controls[index] as FormArray;
        for (const item in newForm.controls) {
          if (newForm.controls[item].status == 'INVALID') {
            newForm.controls[item].markAsTouched();
          }
        }
      }

      return;
    } else {
      this.showLoader = true;
      console.log('else');
      console.log(this.submitBtn);
      //  this.purchaseForm.get('txn_currency_total_amount')?.enable();

      const val = this.datepipe.transform(
        this.purchaseForm.get('transaction_date')?.value,
        'yyyy-MM-dd'
      );
      this.purchaseForm.get('transaction_date')?.setValue(val);
      const gridRow1 = <FormArray>(
        this.purchaseForm.get('purchase_order_details')
      );

      //   gridRow1.at(j).get('tax_rate')?.enable();
      if (this.submitBtn) {
        if (this.btnVal == 'Update') {
          this.purchaseForm.enable();
        }
      }
      for (let j = 0; j < gridRow1.length; j++) {
        // gridRow1.at(j).get('tax_rate')?.enable();
        // gridRow1.at(j).get('uom')?.enable();
        // gridRow1.at(j).get('hsn_sac_no')?.enable();
        // gridRow1.at(j).get('is_rate_percentage')?.enable();
        // gridRow1.at(j).get('commision')?.enable();
        // gridRow1.at(j).get('txn_currency_commision_amt')?.enable();
        // gridRow1.at(j).get('total_amount')?.enable();
        // gridRow1.at(j).get('txn_currency_tax_amount')?.enable();
        // gridRow1.at(j).get('txn_currency_amount')?.enable();

        gridRow1.at(j).value.expected_delivery_date = this.datepipe.transform(
          gridRow1.at(j).value.expected_delivery_date,
          'yyyy-MM-dd'
        );
      }
      this.handleSave.emit(this.purchaseForm.getRawValue());
    }
  }
  formatDate() {
    const dateString = '2023-09-28T18:30:00.000Z';
    const dateObject = new Date(dateString);

    const day = String(dateObject.getDate()).padStart(2, '0');
    const month = String(dateObject.getMonth() + 1).padStart(2, '0'); // Note: Months are 0-based, so we add 1.
    const year = dateObject.getFullYear();

    const formattedDate = `${day}-${month}-${year}`;
    console.log(formattedDate);

    return formattedDate;
  }
  onResetForm() {
    this.initializeForm();
  }

  changeStatus(event: any) {
    if (event.checked) {
      this.purchaseForm.get('is_deleted')?.setValue(false);
    } else {
      this.purchaseForm.get('is_deleted')?.setValue(true);
    }
  }

  supplierTypeChnage(event: any) {
    for (let i = 0; i < this.SupplierTypeData.length; i++) {
      if (this.SupplierTypeData[i]['id'] == event) {
        if (this.SupplierTypeData[i]['master_key'] == 'Supplier') {
          this.purchaseorderService
            .getDynamicData('supplier', 'company_name')
            .subscribe({
              next: (data: any) => {
                this.SupplierData = data;
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
        } else if (this.SupplierTypeData[i]['master_key'] == 'Customer') {
          this.purchaseorderService
            .getDynamicData('customer', 'company_name')
            .subscribe({
              next: (data: any) => {
                this.SupplierData = data;
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
        } else if (this.SupplierTypeData[i]['master_key'] == 'Farmer') {
          this.purchaseorderService.getDynamicData('farmer', 'name').subscribe({
            next: (data: any) => {
              this.SupplierData = data;
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
        } else if (this.SupplierTypeData[i]['master_key'] == 'Agent') {
          this.purchaseorderService
            .getDynamicData('agent', 'agent_name')
            .subscribe({
              next: (data: any) => {
                this.SupplierData = data;
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
        }
      }
    }
  }

  getAddress(e: any, type: any) {
    // let masterkey;
    let parent_id = '';
    const masterkey = 'supplier_details';
    parent_id = e;

    this.purchaseorderService
      .GetBillingShippingAdrress(masterkey, e, parent_id)
      .subscribe({
        next: (data: any) => {
          this.supplierShipfromData = [];
          this.supplierbillfromData = [];
          for (let i = 0; i < data.length; i++) {
            if (data[i][0]['Billing'] != undefined) {
              console.log('yess');

              const dict = {
                Billing: data[i][0]['Billing'],
                id: data[i][1]['id'],
              };
              this.supplierbillfromData.push(dict);
              if (type != 'inview') {
                this.purchaseForm
                  .get('supplier_bill_from')
                  ?.setValue(this.supplierbillfromData[0]['id']);
              }
            }
            if (data[i][0]['Shipping'] != undefined) {
              const dict = {
                Shipping: data[i][0]['Shipping'],
                id: data[i][1]['id'],
              };
              this.supplierShipfromData.push(dict);
              if (type != 'inview') {
                this.purchaseForm
                  .get('supplier_ship_from')
                  ?.setValue(this.supplierShipfromData[0]['id']);
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  supplierChnage(event: any) {
    this.getAddress(event, '');
    this.purchaseForm.get('supplier_bill_from')?.setValue('');
    this.purchaseForm.get('supplier_ship_from')?.setValue('');

    this.purchaseForm.setControl(
      'purchase_order_details',
      this.setExistingArray1([])
    );
    this.formArray.push(this.initialitemRow());
    this.purchaseForm.get('supplier_payment_terms')?.setValue('');
    this.purchaseForm.get('supplier_delivery_terms')?.setValue('');

    let parent_id = '';
    const masterkey = 'supplier_details';
    parent_id = event;

    this.purchaseorderService
      .getDynamicData('supplier', 'payment_terms_ref_id')
      .subscribe({
        next: (data: any) => {
          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == 0) {
                this.purchaseForm.get('supplier_payment_terms')?.setValue('');
              } else {
                this.purchaseForm
                  .get('supplier_payment_terms')
                  ?.setValue(data[i]['key']);
                this.purchaseForm
                  .get('txn_currency')
                  ?.setValue(data[i]['default_currency_ref_id']);
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    this.purchaseorderService
      .getDynamicData('supplier', 'delivery_terms_ref_id')
      .subscribe({
        next: (data: any) => {
          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == 0) {
                this.purchaseForm.get('supplier_delivery_terms')?.setValue('');
              } else {
                this.purchaseForm
                  .get('supplier_delivery_terms')
                  ?.setValue(data[i]['key']);
                const del_type = this.ModeofDeliveryTypeData.filter(
                  (d: any) => d.master_key == 'Spot'
                );
                if (del_type[0]['id'] == data[i]['key']) {
                  this.purchaseForm.get('transportation_charges')?.enable();
                }
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  DeliveryTermsChnage(e: any) {
    this.purchaseForm.get('transportation_charges')?.setValue('');
    const del_type = this.ModeofDeliveryTypeData.filter(
      (d: any) => d.master_key == 'Spot'
    );
    if (del_type[0]['id'] == e) {
      this.purchaseForm.get('transportation_charges')?.enable();
    } else {
      this.purchaseForm.get('transportation_charges')?.disable();
    }
  }

  FPCChnage(event: any) {
    let parent_id = '';
    const masterkey = 'tenant_details';
    parent_id = event;

    this.purchaseorderService
      .GetBillingShippingAdrress(masterkey, event, parent_id)
      .subscribe({
        next: (data: any) => {
          this.purchaseForm
            .get('supplier_bill_from')
            ?.setValue(data['Billing']);
          this.purchaseForm
            .get('supplier_ship_from')
            ?.setValue(data['Shipping']);
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  ItemNameChange(event: any, index: any) {
    console.log(event, 'item of po');
    const item_cnt = this.purchaseForm.value.purchase_order_details.filter(
      (x: any) => x.item_ref_id == event.value
    );
    if (item_cnt.length > 1) {
      this.showSwalMassage('Item Already Selected', 'warning');
      (<FormArray>this.purchaseForm.get('purchase_order_details'))
        .at(index)
        .get('item_ref_id')
        ?.setValue('');
    }

    const gridRow = (<FormArray>(
      this.purchaseForm.get('purchase_order_details')
    )).at(index);
    gridRow.get('alternate_quantity')?.setValue('');
    gridRow.get('txn_currency_commision_amt')?.setValue('');
    gridRow.get('txn_currency_amount')?.setValue('');
    gridRow.get('total_amount')?.setValue('');

    this.purchaseForm.get('txn_currency_total_amount')?.setValue('');
    this.purchaseorderService
      .getDynamicData('item_rate_commision_details', event.value)
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            gridRow.get('rate')?.setValue(data[0]['item_rate']);
            if (data[0]['is_commision_in_amount_percentage'] == null) {
              gridRow.get('is_rate_percentage')?.setValue(false);
            } else {
              gridRow
                .get('is_rate_percentage')
                ?.setValue(data[0]['is_commision_in_amount_percentage']);
            }
            if (data[0]['commision'] == null) {
              gridRow.get('commision')?.setValue(0);
            } else {
              gridRow.get('commision')?.setValue(data[0]['commision']);
            }
          } else if (data.length == 0) {
            gridRow.get('rate')?.setValue('');
            gridRow.get('is_rate_percentage')?.setValue(false);
            gridRow.get('commision')?.setValue(0);
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.purchaseorderService
      .getDynamicData('item_master', 'hsn_sac_code')
      .subscribe({
        next: (data: any) => {
          for (let j = 0; j < data.length; j++) {
            if (event.value == data[j]['id']) {
              const gridRow = (<FormArray>(
                this.purchaseForm.get('purchase_order_details')
              )).at(index);
              gridRow.get('hsn_sac_no')?.setValue(Number(data[j]['key']));
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.purchaseorderService.getDynamicData('item_master', 'uom').subscribe({
      next: (data: any) => {
        for (let j = 0; j < data.length; j++) {
          if (event.value == data[j]['id']) {
            const gridRow = (<FormArray>(
              this.purchaseForm.get('purchase_order_details')
            )).at(index);
            gridRow.get('alternate_uom')?.setValue(Number(data[j]['key']));
            for (let i = 0; i < this.UOMData.length; i++) {
              if (this.UOMData[i]['id'] == data[j]['key']) {
                gridRow.get('uom_name')?.setValue(this.UOMData[i]['key']);
              }
            }
          }
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    // this.purchaseorderService.getDynamicData("item_master",'base_uom').subscribe({
    //   next: (data: any) => {
    //       for(let j=0;j<data.length;j++){
    //         if(event.value==data[j]['id']){
    //           var gridRow = (<FormArray>(this.purchaseForm.get("purchase_order_details"))).at(index)
    //           gridRow.get('alternate_uom')?.setValue(Number(data[j]['key']))

    //         }
    //       }
    //   },
    //   error: (e) => {
    //     this.showSwalMassage(this.errorCodes.getErrorMessage(JSON.parse(e).status), 'error');
    //   }
    // });

    this.purchaseorderService
      .getDynamicData('item_master', 'base_uom')
      .subscribe({
        next: (data: any) => {
          console.log();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.purchaseorderService
      .getDynamicData('tax_rate', event.value)
      .subscribe({
        next: (data: any) => {
          console.log(data);
          const gridRow = (<FormArray>(
            this.purchaseForm.get('purchase_order_details')
          )).at(index);

          if (data.length > 0) {
            gridRow.get('tax_rate')?.setValue(Number(data[0]['tax_rate']));
          } else {
            gridRow.get('tax_rate')?.setValue(0);
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  get_quantity_fu(event: any, index: any, gridRow: any) {
    const quantity = parseFloat(event.target.value);
    const rate = parseFloat(gridRow.get('rate')?.value);

    const transaction_amount: any = rate * quantity;

    gridRow.get('txn_currency_amount')?.setValue(transaction_amount.toFixed(2));
    this.total_transaction_amount = 0;
    const gridRow1 = <FormArray>this.purchaseForm.get('purchase_order_details');

    for (let j = 0; j < gridRow1.length; j++) {
      const amt: any = parseFloat(
        gridRow1.at(j).get('txn_currency_amount')?.value
      );
      this.total_transaction_amount =
        parseFloat(this.total_transaction_amount.toString()) + parseFloat(amt);
    }
    this.purchaseForm
      .get('txn_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    this.purchaseForm
      .get('base_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    let commision_amount = 0;
    if (gridRow.get('is_rate_percentage')?.value == 'true') {
      commision_amount = quantity * gridRow.get('commision')?.value;
    } else if (gridRow.get('is_rate_percentage')?.value == 'false') {
      commision_amount =
        (parseFloat(transaction_amount) *
          parseFloat(gridRow.get('commision')?.value)) /
        100;
    }
    gridRow
      .get('txn_currency_commision_amt')
      ?.setValue(commision_amount.toFixed(2));

    const total_amt = transaction_amount + commision_amount;
    gridRow.get('total_amount')?.setValue(total_amt.toFixed(2));
  }

  QuantityChange(event: any, index: any) {
    const gridRow = (<FormArray>(
      this.purchaseForm.get('purchase_order_details')
    )).at(index);
    this.get_quantity_fu(event, index, gridRow);
    this.cal_tax_amt(gridRow.get('is_gst_set_off')?.value, index);
  }

  get_rate_fu(event: any, index: any, gridRow: any) {
    const rate = parseFloat(event.target.value);
    const quantity = parseFloat(gridRow.get('alternate_quantity')?.value);

    const transaction_amount: any = rate * quantity;

    gridRow.get('txn_currency_amount')?.setValue(transaction_amount.toFixed(2));
    this.total_transaction_amount = 0;
    var gridRow1 = <FormArray>this.purchaseForm.get('purchase_order_details');

    for (let j = 0; j < gridRow1.length; j++) {
      const amt = gridRow1.at(j).get('total_amount')?.value;
      this.total_transaction_amount =
        parseFloat(this.total_transaction_amount.toString()) + parseFloat(amt);
    }
    this.purchaseForm
      .get('txn_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    this.purchaseForm
      .get('base_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    let commision_amount = 0;
    if (gridRow.get('is_rate_percentage')?.value == 'true') {
      commision_amount = quantity * gridRow.get('commision')?.value;
    } else if (gridRow.get('is_rate_percentage')?.value == 'false') {
      commision_amount =
        (parseFloat(transaction_amount) *
          parseFloat(gridRow.get('commision')?.value)) /
        100;
    }
    let tax_amount = 0;
    if (gridRow.get('is_gst_set_off')?.value == 'true') {
      tax_amount =
        (parseFloat(transaction_amount) *
          parseFloat(gridRow.get('txn_currency_tax_amount')?.value)) /
        100;
    } else if (gridRow.get('is_gst_set_off')?.value == 'false') {
      tax_amount = 0;
    }
    gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));

    gridRow
      .get('txn_currency_commision_amt')
      ?.setValue(commision_amount.toFixed(2));

    const total_amt = transaction_amount + tax_amount;
    gridRow.get('total_amount')?.setValue(total_amt.toFixed(2));

    this.total_transaction_amount = 0;
    var gridRow1 = <FormArray>this.purchaseForm.get('purchase_order_details');

    for (let j = 0; j < gridRow1.length; j++) {
      const amt = parseFloat(gridRow1.at(j).get('total_amount')?.value);
      this.total_transaction_amount =
        parseFloat(this.total_transaction_amount.toString()) + amt;
    }
    this.purchaseForm
      .get('txn_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    this.purchaseForm
      .get('base_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
  }

  RateChange(event: any, index: any) {
    const gridRow = (<FormArray>(
      this.purchaseForm.get('purchase_order_details')
    )).at(index);
    this.get_rate_fu(event, index, gridRow);
    this.cal_tax_amt(gridRow.get('is_gst_set_off')?.value, index);
  }

  PurchaseTypeChange(val: any) {
    if (this.purchaseForm.get('supplier_ref_id')?.value == '') {
      Swal.fire('Select FPC Name First');
      this.purchaseForm.get('purchase_type_id')?.setValue('');
    }
  }

  SalesOrderChange(event: any) {
    this.purchaseForm.get('txn_currency_total_amount')?.setValue('');
    if (
      event.value == '' ||
      event.value == undefined ||
      event.value == 'None'
    ) {
      this.supplierExist = true;
      this.purchaseForm.get('supplier_ref_id')?.setValue('');
      this.purchaseForm.get('supplier_bill_from')?.setValue('');
      this.purchaseForm.get('supplier_ship_from')?.setValue('');

      this.purchaseForm.setControl(
        'purchase_order_details',
        this.setExistingArray1([])
      );
      this.formArray.push(this.initialitemRow());
      this.purchaseForm.get('supplier_payment_terms')?.setValue('');
      this.purchaseForm.get('supplier_delivery_terms')?.setValue('');
    } else {
      if (this.rolename == 'FPC Admin') {
        this.supplierExist = true;
      } else {
        this.supplierExist = false;
      }

      this.purchaseorderService.getTenatNameData('tenant_name_get').subscribe({
        next: (data: any) => {
          this.FPCData = data;
          this.filterFPCData = this.FPCData.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

      for (let i = 0; i < this.SalesOrderData.length; i++) {
        if (this.SalesOrderData[i]['id'] == event.value) {
          this.fpcService.getAllMasterData('Purchase Order Type').subscribe({
            next: (data: any) => {
              for (let i = 0; i < data.length; i++) {
                if (data[i]['master_key'] == 'Commision') {
                  if (
                    data[i]['id'] ==
                    this.SalesOrderData[i]['customer_po_type_ref_id']
                  ) {
                    console.log();
                  }
                }
              }
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
          this.FPCChnage(this.SalesOrderData[i]['tenant_id']);
          this.purchaseForm.setControl(
            'purchase_order_details',
            this.setExistingArray1(this.SalesOrderData[i]['order_details'])
          );
          this.purchaseForm
            .get('supplier_payment_terms')
            ?.setValue(this.SalesOrderData[i]['payment_terms_ref_id']);
          this.purchaseForm
            .get('supplier_delivery_terms')
            ?.setValue(this.SalesOrderData[i]['delivery_terms_ref_id']);
        }
      }
    }
  }

  cal_tax_amt(type: any, index: any) {
    const gridRow = (<FormArray>(
      this.purchaseForm.get('purchase_order_details')
    )).at(index);
    const transaction_amount: any = parseFloat(
      gridRow.get('txn_currency_amount')?.value
    );

    if (type == true) {
      const tax_amount =
        (parseFloat(transaction_amount) *
          parseFloat(gridRow.get('tax_rate')?.value)) /
        100;
      gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));

      const total_amt = transaction_amount + tax_amount;
      gridRow.get('total_amount')?.setValue(total_amt.toFixed(2));
    } else if (type == false) {
      const tax_amount = 0;
      gridRow.get('txn_currency_tax_amount')?.setValue(tax_amount.toFixed(2));

      const total_amt = transaction_amount + tax_amount;
      gridRow.get('total_amount')?.setValue(total_amt.toFixed(2));
    }
    this.total_transaction_amount = 0;
    const gridRow1 = <FormArray>this.purchaseForm.get('purchase_order_details');

    for (let j = 0; j < gridRow1.length; j++) {
      const amt = parseFloat(gridRow1.at(j).get('total_amount')?.value);
      this.total_transaction_amount =
        parseFloat(this.total_transaction_amount.toString()) + amt;
    }
    this.purchaseForm
      .get('txn_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
    this.purchaseForm
      .get('base_currency_total_amount')
      ?.setValue(this.total_transaction_amount.toFixed(2));
  }
  clickEvent(e: any, index: any) {
    this.cal_tax_amt(e.target.checked, index);
  }

  ItemTypeChnage(e: any, type: any) {
    if (type != 'InView') {
      this.purchaseForm.get('txn_currency_total_amount')?.setValue('');
    }
    this.purchaseForm.setControl(
      'purchase_order_details',
      this.setExistingArray1([])
    );
    this.formArray.push(this.initialitemRow());
    this.purchaseorderService
      .getDynamicData('item_master_alldata', e)
      .subscribe({
        next: (data: any) => {
          this.ItemSearchData = data;
          this.ItemDataData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }
}
