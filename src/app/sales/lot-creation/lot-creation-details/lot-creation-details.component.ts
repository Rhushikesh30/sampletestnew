import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormArray,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { LotCreationService } from 'src/app/shared/services/lot-creation.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-lot-creation-details',
  templateUrl: './lot-creation-details.component.html',
  styleUrls: ['./lot-creation-details.component.scss'],
  providers: [ErrorCodes],
})
export class LotCreationDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;

  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  lotForm!: UntypedFormGroup;
  btnVal = 'Submit';
  cancelFlag = true;
  saleddata: any;
  DisabledCheckbox = true;
  checkAllProcess = false;
  ShowGrid = false;
  farmerdata: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private lotService: LotCreationService,
    private errorCodes: ErrorCodes
  ) {}

  ngOnInit() {
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.viewRecord(this.rowData);
    }

    this.lotService.getSalesOrderData().subscribe({
      next: (data: any) => {
        this.saleddata = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.lotForm = this.formBuilder.group({
      id: [''],
      so_ref_id: ['', Validators.required],
      quantity: { value: '', disabled: true },
      lot_details: this.formBuilder.array([this.lot_details()]),
    });
  }

  lot_details() {
    return this.formBuilder.group({
      id: [''],
      is_check: [false],
      isChecked: true,
      farmer_ref_id: ['', Validators.required],
      purchase_qty: ['', Validators.required],
      allocation_qty: ['', Validators.required],
    });
  }

  get formArray() {
    return this.lotForm.get('lot_details') as FormArray;
  }

  viewRecord(row: any) {
    this.DisabledCheckbox = false;
    this.ShowGrid = true;
  }

  onResetForm() {
    this.initializeForm();
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.lotForm.reset();
    this.handleCancel.emit(false);
  }

  checkAll() {
    this.checkAllProcess = !this.checkAllProcess;
    this.formArray.controls.map((e) => {
      e.get('isChecked')?.setValue(this.checkAllProcess);
      e.get('is_check')?.setValue(this.checkAllProcess);
    });
  }

  toggle(val: any, index: any) {
    const gridRow = (<FormArray>this.lotForm.get('lot_details')).at(index);
    gridRow.get('is_check')?.setValue(val.checked);
  }

  SalesOrderChnage(e: any) {
    if (e == '' || e == undefined) {
      this.farmerdata = [];
    } else {
      this.lotService.getFarmerBySO(e).subscribe({
        next: (data: any) => {
          this.farmerdata = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }
}
