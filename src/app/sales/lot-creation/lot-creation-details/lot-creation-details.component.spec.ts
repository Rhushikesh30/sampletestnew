import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LotCreationDetailsComponent } from './lot-creation-details.component';

describe('LotCreationDetailsComponent', () => {
  let component: LotCreationDetailsComponent;
  let fixture: ComponentFixture<LotCreationDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LotCreationDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LotCreationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
