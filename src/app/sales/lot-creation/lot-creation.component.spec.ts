import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LotCreationComponent } from './lot-creation.component';

describe('LotCreationComponent', () => {
  let component: LotCreationComponent;
  let fixture: ComponentFixture<LotCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LotCreationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LotCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
