import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { ReceiptService } from 'src/app/shared/services/receipt.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { MAT_DATE_FORMATS } from '@angular/material/core';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-receipt-details',
  templateUrl: './receipt-details.component.html',
  styleUrls: ['./receipt-details.component.scss'],
  providers: [
    DatePipe,
    ErrorCodes,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ],
})
export class ReceiptDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @Input() screenName!: string;

  cancelFlag = true;
  paymentsForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  SupplierData: any = [];
  txn_currency: any;
  filterSupplierData: any = [];
  CurrencyData: any = [];
  PaymentMethodData: any = [];
  BankData: any = [];
  filterBankData: any = [];
  filterPOData: any = [];
  POData: any = [];
  BookingTypeData: any = [];
  DisabledCheckbox = true;
  PaymentType: any;
  ShowAdvanceGrid: any = false;
  ShowPaymentGrid: any = false;
  total_advance_net_amt: any = 0;

  viewBtn = true;
  myTemplate = '';
  //*
  todayDate = new Date().toJSON().split('T')[0];
  txn_currency_grand_total = 0;
  checkAllProcess = false;

  constructor(
    private formBuilder: FormBuilder,
    private fpcService: FpcSetupService,
    private errorCodes: ErrorCodes,
    public datepipe: DatePipe,
    private purchaseorderService: PurchaseorderService,
    private ReceiptService: ReceiptService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewEditRecord(this.rowData);
    }
    this.formArray.removeAt(-1);

    this.purchaseorderService
      .getDynamicData('bank', 'account_number')
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              data[i]['key'] =
                data[i]['bank_name'] + ' - ' + data[i]['account_number'];
            }
          }
          this.BankData = data;
          this.filterBankData = this.SupplierData.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.paymentsForm
          .get('txn_currency')
          ?.setValue(this.txn_currency[0]['id']);
      },
    });

    this.fpcService.getAllMasterData('Payment Method').subscribe({
      next: (data: any) => {
        this.PaymentMethodData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.fpcService
      .getAllMasterData('Purchase Expense Booking Ref Type')
      .subscribe({
        next: (data: any) => {
          this.BookingTypeData = data;
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    this.fpcService.getAllMasterData('Payment Type').subscribe({
      next: (data: any) => {
        this.PaymentType = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.paymentsForm = this.formBuilder.group({
      id: [''],
      transaction_date: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      txn_currency: [],
      base_currency: [''],
      conversion_rate: [1],
      payment_method_ref_id: [''],
      tenant_account_ref_id: [''],
      customer_ref_id: ['', Validators.required],
      customer_ref_type: [''],
      is_tds_applicable: [false],
      tds_percentage: [0],
      tds_account_ref_id: [0],
      txn_currency_bank_charges: [0],
      txn_currency_net_amount: [''],
      base_currency_bank_charges: [0],
      base_currency_net_amount: [''],
      txn_currency_gross_amount: ['', Validators.required],
      tds_amount: [0],
      base_currency_gross_amount: [''],
      tenant_bank_name: [''],
      tenant_ifsc_code: [''],
      tenant_account_no: [''],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      supplier_bank_name: [{ value: '', disabled: true }],
      supplier_ifsc_code: [{ value: '', disabled: true }],
      supplier_account_no: [{ value: '', disabled: true }],
      is_active: [true],
      is_deleted: [false],
      payment_type_ref_id: ['', Validators.required],
      advance_details: this.formBuilder.array([this.initialitemRow1()]),
      receipt_details: this.formBuilder.array([this.initialitemRow()]),
    });
  }

  initialitemRow() {
    return this.formBuilder.group({
      is_check: [false],
      id: [''],
      isChecked: true,
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      conversion_rate: [1],
      txn_currency: [{ value: '', disabled: true }],
      base_currency: [{ value: '', disabled: true }],
      invoice_ref_id: [''],
      invoice_type: [''],
      invoice_date: [this.todayDate],
      txn_currency_amount: [{ value: '', disabled: true }],
      txn_currency_advance_amount_applied: [{ value: '', disabled: true }],
      base_currency_amount: [{ value: '', disabled: true }],
      base_currency_advance_amount_applied: [{ value: '', disabled: true }],
      invoice_ref_name: [''],
      invoice_amount: ['', Validators.required],
    });
  }

  get formArray1() {
    return this.paymentsForm.get('advance_details') as FormArray;
  }

  get formArray() {
    return this.paymentsForm.get('receipt_details') as FormArray;
  }

  initialitemRow1() {
    return this.formBuilder.group({
      id: [''],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      conversion_rate: [1],
      txn_currency: [''],
      base_currency: [],
      invoice_ref_id: [''],
      invoice_date: [this.todayDate],
      txn_currency_amount: [{ value: '', disabled: true }],
      txn_currency_advance_amount_applied: [{ value: '', disabled: true }],
      base_currency_amount: [''],
      base_currency_advance_amount_applied: [0],
      invoice_amount: [0],
      invoice_type: [''],
      txn_currency_net_amount: [''],
    });
  }

  addNewRow() {
    this.formArray.push(this.initialitemRow1());
  }

  deleteRow(index: number) {
    if (this.formArray1.length == 1) {
      return false;
    } else {
      this.formArray1.removeAt(index);
      return true;
    }
  }

  toggle(val: any, index: any) {
    const gridRow = (<FormArray>this.paymentsForm.get('receipt_details')).at(
      index
    );
    gridRow.get('is_check')?.setValue(val.checked);

    let txn_currency_gross_amount = 0;
    if (
      (<FormArray>this.paymentsForm.get('receipt_details')).value.filter(
        (x: any) => x.is_check == true
      ).length > 0
    ) {
      (<FormArray>this.paymentsForm.get('receipt_details')).value.forEach(
        (ele: any) => {
          if (ele.is_check) {
            console.log(txn_currency_gross_amount, ele.txn_currency_amount);
            txn_currency_gross_amount =
              parseFloat(txn_currency_gross_amount.toString()) +
              parseFloat(ele.txn_currency_amount);
            this.paymentsForm
              .get('txn_currency_gross_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
            this.paymentsForm
              .get('txn_currency_net_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
          }
        }
      );
    } else {
      this.paymentsForm
        .get('txn_currency_gross_amount')
        ?.setValue(txn_currency_gross_amount.toFixed(2));
      this.paymentsForm
        .get('txn_currency_net_amount')
        ?.setValue(txn_currency_gross_amount.toFixed(2));
    }
  }

  viewEditRecord(row1: any) {
    this.ReceiptService.getReceiptDataById(row1.id).subscribe({
      next: (row: any) => {
        row = row[0];
        this.paymentsForm.patchValue({
          id: row.id,
          transaction_id: row.transaction_id,
          transaction_ref_no: row.transaction_ref_no,
          transaction_type_id: row.transaction_type_id,
          period: row.period,
          ledger_group: row.ledger_group,
          fiscal_year: row.fiscal_year,
          created_by: row.created_by,
          base_currency: row.base_currency,
          transaction_date: row.transaction_date,
          payment_method_ref_id: row.payment_method_ref_id,
          tenant_id: row.tenant_id,
          txn_currency: row.txn_currency,
          tenant_account_ref_id: row.tenant_account_ref_id,
          customer_ref_type: row.customer_ref_type,
          txn_currency_bank_charges: parseFloat(
            row.txn_currency_bank_charges
          ).toFixed(2),
          txn_currency_net_amount: parseFloat(
            row.txn_currency_net_amount
          ).toFixed(2),
          txn_currency_gross_amount: parseFloat(
            row.txn_currency_gross_amount
          ).toFixed(2),
          base_currency_gross_amount: row.base_currency_gross_amount,
          customer_ref_id: row.customer_ref_id,
          is_active: row.is_active,
          is_deleted: row.is_deleted,
          payment_type_ref_id: row.payment_type_ref_id,
        });
        this.purchaseorderService
          .getCustomerByType(
            'customer',
            'company_name',
            row.payment_type_ref_id
          )
          .subscribe({
            next: (data: any) => {
              this.SupplierData = data;
              this.filterSupplierData = this.SupplierData.slice();
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
        this.DisabledCheckbox = false;
        this.fpcService.getAllMasterData('Payment Type').subscribe({
          next: (data: any) => {
            this.PaymentType = data;
            const type = this.PaymentType.filter(
              (d: any) => d.master_key == 'Advance'
            );
            if (row.payment_type_ref_id == type[0]['id']) {
              this.ShowAdvanceGrid = true;
              this.ShowPaymentGrid = false;
              this.paymentsForm.get('txn_currency_gross_amount')?.disable();
            } else {
              this.ShowAdvanceGrid = false;
              this.ShowPaymentGrid = true;
              this.paymentsForm.get('txn_currency_gross_amount')?.enable();
            }
            this.paymentsForm.setControl(
              'receipt_details',
              this.setExistingArray(row.receipt_details)
            );

            this.ReceiptService.GetSOBySupplier(
              'so_by_supplier_id',
              row.customer_ref_id
            ).subscribe({
              next: (data) => {
                this.POData = data;
                this.paymentsForm.setControl(
                  'advance_details',
                  this.setExistingArray1(row.advance_details)
                );
              },
              error: (e) => {
                this.showSwalMassage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  'error'
                );
              },
            });
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

        this.get_supplier_bank_details(row.customer_ref_id);

        if (this.submitBtn) {
          this.btnVal = 'Update';
          this.viewBtn = false;
        }
      },
    });
    this.paymentsForm.disable();
    // this.paymentsForm.get('txn_currency_amount')?.disable();
  }

  setExistingArray(initialArray = []): FormArray {
    // this.paymentsForm.get('txn_currency_amount')?.disable();
    const formArray: any = new FormArray([]);
    const txn_currency_gross_amount = 0;
    initialArray.forEach((element: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          conversion_rate: element.conversion_rate,
          txn_currency: element.txn_currency,
          invoice_ref_id: element.invoice_ref_id,
          invoice_date: element.invoice_date,
          txn_currency_amount: [
            {
              value: parseFloat(element.txn_currency_amount).toFixed(2),
              disabled: true,
            },
          ],
          // txn_currency_amount: parseFloat(element.txn_currency_amount).toFixed(
          //   2
          // ),
          // txn_currency_advance_amount_applied: parseFloat(
          //   element.txn_currency_advance_amount_applied
          // ).toFixed(2),
          txn_currency_advance_amount_applied: [
            {
              value: parseFloat(
                element.txn_currency_advance_amount_applied
              ).toFixed(2),
              disabled: true,
            },
          ],
          base_currency_amount: element.base_currency_amount,
          base_currency_advance_amount_applied:
            element.base_currency_advance_amount_applied,
          base_currency: element.base_currency,
          invoice_amount: parseFloat(element.invoice_amount).toFixed(2),
          supplier_bill_no: element.supplier_bill_no,
          transaction_id: element.transaction_id,
          transaction_ref_no: element.transaction_ref_no,
          transaction_type_id: element.transaction_type_id,
          period: element.period,
          ledger_group: element.ledger_group,
          fiscal_year: element.fiscal_year,
          tenant_id: element.tenant_id,
          invoice_ref_name: element.invoice_ref_name,
          created_by: element.created_by,
          updated_by: element.updated_by,
          created_date_time: element.created_date_time,
          updated_date_time: element.updated_date_time,
        })
      );
      // txn_currency_gross_amount = parseFloat(txn_currency_gross_amount.toString()) + parseFloat(element.txn_currency_amount);
      // this.paymentsForm.get('txn_currency_gross_amount')?.setValue(txn_currency_gross_amount.toFixed(2));
      // this.paymentsForm.get('txn_currency_net_amount')?.setValue(txn_currency_gross_amount.toFixed(2));
    });

    return formArray;
  }

  setExistingArray1(initialArray = []): FormArray {
    console.log(this.setExistingArray1, 'Array Data....');
    // this.paymentsForm.get('txn_currency_amount')?.disable();
    const formArray1: any = new FormArray([]);
    console.log(this.POData);

    initialArray.forEach((element: any) => {
      console.log(element.invoice_ref_id);

      formArray1.push(
        this.formBuilder.group({
          id: element.id,
          conversion_rate: element.conversion_rate,
          txn_currency: element.txn_currency,
          invoice_ref_id: Number(element.invoice_ref_id),
          invoice_date: element.invoice_date,
          txn_currency_amount: element.txn_currency_amount,
          txn_currency_advance_amount_applied:
            element.txn_currency_advance_amount_applied,
          base_currency_amount: element.base_currency_amount,
          base_currency_advance_amount_applied:
            element.base_currency_advance_amount_applied,
          base_currency: element.base_currency,
          invoice_amount: element.invoice_amount,
          supplier_bill_no: element.supplier_bill_no,
          transaction_id: element.transaction_id,
          transaction_ref_no: element.transaction_ref_no,
          transaction_type_id: element.transaction_type_id,
          period: element.period,
          ledger_group: element.ledger_group,
          fiscal_year: element.fiscal_year,
          tenant_id: element.tenant_id,
          invoice_ref_name: element.invoice_ref_name,
          created_by: element.created_by,
          updated_by: element.updated_by,
          created_date_time: element.created_date_time,
          updated_date_time: element.updated_date_time,
        })
      );
    });

    return formArray1;
  }

  onResetForm() {
    this.initializeForm();
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.paymentsForm.reset();
    this.handleCancel.emit(false);
  }

  onSubmit() {
    if (this.paymentsForm.invalid) {
      const invalid = [];
      const controls = this.paymentsForm.controls;
      const fc: any = this.paymentsForm.controls;
      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }
      for (let i = 0; i < fc.receipt_details.controls.length; i++) {
        const md = fc.receipt_details.controls[i].controls;
        if (md.invalid) {
          md.markAsTouched();
        }
      }
      return;
    } else {
      this.paymentsForm.get('txn_currency_net_amount')?.enable();

      const val = this.datepipe.transform(
        this.paymentsForm.get('transaction_date')?.value,
        'yyyy-MM-dd'
      );
      this.paymentsForm.get('transaction_date')?.setValue(val);
      const type = this.PaymentType.filter(
        (d: any) => d.master_key == 'Advance'
      );

      if (
        this.paymentsForm.get('payment_type_ref_id')?.value == type[0]['id']
      ) {
        this.handleSave.emit(this.paymentsForm.getRawValue());
      } else {
        const gridRow1 = <FormArray>this.paymentsForm.get('receipt_details');

        for (let j = 0; j < gridRow1.length; j++) {
          const val1 = this.datepipe.transform(
            gridRow1.at(j).get('invoice_date')?.value,
            'yyyy-MM-dd'
          );
          gridRow1.at(j).get('invoice_date')?.setValue(val1);

          //gridRow1.at(j).get('txn_currency_amount')?.enable();
          gridRow1.at(j).get('txn_currency_advance_amount_applied')?.enable();
        }
        const gridRow = <FormArray>this.paymentsForm.get('receipt_details');
        const formvalue = gridRow.value.filter((d: any) => d.is_check == true);
        let total_amt: any = 0;

        if (formvalue.length <= 0) {
          Swal.fire('Select At least One Receipt Details');
        } else {
          for (let i = 0; i < formvalue.length; i++) {
            total_amt =
              parseFloat(total_amt) +
              parseFloat(formvalue[i]['txn_currency_amount']);
          }
          if (
            total_amt !=
            this.paymentsForm.get('txn_currency_gross_amount')?.value
          ) {
            //Swal.fire("Total Paid Amount Same As Net Amount")
            Swal.fire(
              'Please ensure that the total amount receivable matches Gross Transaction Amount.'
            );
          } else {
            const d = { ...this.paymentsForm.getRawValue() };
            d.receipt_details = formvalue;
            this.handleSave.emit(d);
          }
        }
      }
    }
  }

  get_supplier_bank_details(val: any) {
    this.purchaseorderService
      .getDynamicData('customer_all_data', val)
      .subscribe({
        next: (data: any) => {
          this.paymentsForm
            .get('supplier_bank_name')
            ?.setValue(data[0]['bank_name']);
          this.paymentsForm
            .get('supplier_account_no')
            ?.setValue(data[0]['account_number']);
          this.paymentsForm
            .get('supplier_ifsc_code')
            ?.setValue(data[0]['ifsc_code']);
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }
  supplierChnage(val: any) {
    this.paymentsForm.get('supplier_bank_name')?.setValue('');
    this.paymentsForm.get('supplier_account_no')?.setValue('');
    this.paymentsForm.get('supplier_ifsc_code')?.setValue('');
    this.paymentsForm.get('customer_ref_type')?.setValue('');

    this.paymentsForm.get('txn_currency_net_amount')?.setValue('');
    this.paymentsForm.get('txn_currency_gross_amount')?.setValue('');

    if (val == '' || val == 'None' || val == undefined) {
      console.log();
    } else {
      const type = this.PaymentType.filter(
        (d: any) => d.master_key == 'Advance'
      );

      // if(this.paymentsForm.get('customer_ref_id')?.value=='' || this.paymentsForm.get('customer_ref_id')?.value==undefined){
      //   Swal.fire("Select Customer Name first")
      //   this.paymentsForm.get('payment_type_ref_id')?.setValue('')
      // }
      // else{
      if (
        this.paymentsForm.get('payment_type_ref_id')?.value == type[0]['id']
      ) {
        this.getalltabedata(val, 'Advance');

        this.ShowAdvanceGrid = true;
        this.ShowPaymentGrid = false;
      } else {
        this.getalltabedata(val, 'Regular');

        this.ShowAdvanceGrid = false;
        this.ShowPaymentGrid = true;
      }

      this.purchaseorderService
        .getDynamicData('customer', 'ref_type')
        .subscribe({
          next: (data: any) => {
            const data1 = data.filter(
              (name: { id: string }) => name.id === val
            );

            this.paymentsForm
              .get('customer_ref_type')
              ?.setValue(data1[0]['key']);
          },
        });

      this.get_supplier_bank_details(val);
    }

    // this.getalltabedata(val)
  }

  addDynamicNewRow2_process(data: any) {
    this.formArray.push(this.addInitialitemRow_process(data));
  }

  addInitialitemRow_process(data: any) {
    let advance_amt: any = 0;
    const final_amt = 0;
    let show: any = false;

    if (
      data['advance_amount'] == 0 ||
      data['advance_amount'] == 0.0 ||
      data['advance_amount'] == 0.0
    ) {
      show = true;
      advance_amt = data['advance_amount'];
    } else {
      show = false;
      if (
        parseFloat(data['txn_currency_balance_grand_total_amount']) <
        parseFloat(data['advance_amount'])
      ) {
        advance_amt = parseFloat(
          data['txn_currency_balance_grand_total_amount']
        );
      } else {
        advance_amt = data['advance_amount'];
      }
    }
    // if(parseFloat(data['advance_amount']) > parseFloat(data['txn_currency_balance_grand_total_amount'])){
    //   advance_amt=parseFloat(data['txn_currency_balance_grand_total_amount'])
    //   final_amt= advance_amt - parseFloat(data['txn_currency_balance_grand_total_amount'])
    // }
    // else{
    //   advance_amt=parseFloat(data['advance_amount'])
    //   final_amt= parseFloat(data['txn_currency_balance_grand_total_amount']) -advance_amt
    // }

    this.txn_currency_grand_total =
      parseFloat(this.txn_currency_grand_total.toString()) +
      (parseFloat(data['txn_currency_balance_grand_total_amount']) -
        parseFloat(advance_amt));
    this.paymentsForm
      .get('txn_currency_gross_amount')
      ?.setValue(this.txn_currency_grand_total.toFixed(2));
    this.paymentsForm
      .get('txn_currency_net_amount')
      ?.setValue(this.txn_currency_grand_total.toFixed(2));

    return this.formBuilder.group({
      invoice_ref_id: [data['id']],
      is_check: [true],
      isChecked: true,
      invoice_ref_name: [{ value: data['transaction_ref_no'], disabled: true }],
      transaction_id: [''],
      transaction_ref_no: [''],
      transaction_type_id: [''],
      period: [''],
      ledger_group: [''],
      fiscal_year: [''],
      tenant_id: [''],
      conversion_rate: [1],
      txn_currency: [''],
      base_currency: [],
      invoice_date: [this.todayDate],
      txn_currency_amount: [
        (
          parseFloat(data['txn_currency_balance_grand_total_amount']) -
          parseFloat(advance_amt)
        ).toFixed(2),
      ],
      txn_currency_advance_amount_applied: {
        value: advance_amt,
        disabled: show,
      },
      old_advance_amount_applied: advance_amt,
      base_currency_amount: [''],
      base_currency_advance_amount_applied: [''],

      invoice_amount: [
        parseFloat(data['txn_currency_balance_grand_total_amount']).toFixed(2),
      ],
    });
  }

  getalltabedata(e: any, type: any) {
    while (this.formArray.length) {
      this.formArray.removeAt(-1);
    }

    if (e == 'None' || e == '' || e == undefined) {
      console.log();
    } else {
      if (type == 'Advance') {
        this.ReceiptService.GetSOBySupplier('so_by_supplier_id', e).subscribe({
          next: (data) => {
            this.POData = data;
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.ReceiptService.GetInvoiceBySupplier(
          'get_invoice_by_customer',
          e
        ).subscribe({
          next: (data) => {
            for (const myData in data) {
              console.log(data[myData]);
              this.checkAllProcess = true;
              this.addDynamicNewRow2_process(data[myData]);
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      }
    }
  }

  // amt_transaction_change(e:any,i:any){
  //   if(e.target.value =='' || e.target.value =='null' || e.target.value ==undefined )
  //   {

  //   }
  //   else{
  //     var gridRow = (<FormArray>(this.paymentsForm.get("receipt_details"))).at(i)
  //     if(e.target.value > gridRow.get('invoice_amount')?.value ){
  //       Swal.fire("Amount Receivable Cannot Be Greater than Invoice Amount")
  //      gridRow.get('txn_currency_amount')?.setValue('')
  //     }
  //   }

  // }

  netAmountChange(e: any) {
    if (
      e.target.value == '' ||
      e.target.value == 'null' ||
      e.target.value == undefined
    ) {
      console.log();
    } else {
      this.paymentsForm
        .get('txn_currency_net_amount')
        ?.setValue(e.target.value);
    }
  }

  BankChargesAmountChange(e: any) {
    if (
      e.target.value == '' ||
      e.target.value == 'null' ||
      e.target.value == undefined
    ) {
      this.paymentsForm
        .get('txn_currency_net_amount')
        ?.setValue(this.paymentsForm.get('txn_currency_gross_amount')?.value);
    } else {
      const amt =
        parseFloat(this.paymentsForm.get('txn_currency_gross_amount')?.value) -
        parseFloat(e.target.value);
      this.paymentsForm.get('txn_currency_net_amount')?.setValue(amt);
    }
  }

  advance_amt_change(e: any, i: any) {
    const gridRow = <FormArray>this.paymentsForm.get('advance_details');
    //
    console.log(e.target.value);

    this.total_advance_net_amt = 0;
    if (
      e.target.value == '' ||
      e.target.value == 'null' ||
      e.target.value == undefined
    ) {
      console.log();
    } else {
      console.log(gridRow.get('txn_currency_amount')?.value);

      for (let i = 0; i < gridRow.length; i++) {
        this.total_advance_net_amt =
          parseFloat(this.total_advance_net_amt) +
          parseFloat(gridRow.at(i).get('txn_currency_amount')?.value);
        gridRow
          .at(i)
          .get('txn_currency_net_amount')
          ?.setValue(gridRow.get('txn_currency_amount')?.value);
      }
    }
    this.paymentsForm
      .get('txn_currency_net_amount')
      ?.setValue(this.total_advance_net_amt);
    this.paymentsForm
      .get('txn_currency_gross_amount')
      ?.setValue(this.total_advance_net_amt);
  }

  PaymentTypeChnage(val: any) {
    const type = this.PaymentType.filter((d: any) => d.id == val);
    const type1 = this.PaymentType.filter(
      (d: any) => d.master_key == 'Advance'
    );

    if (type1[0]['id'] == val) {
      if (
        this.paymentsForm.get('customer_ref_id')?.value == '' ||
        this.paymentsForm.get('customer_ref_id')?.value == undefined
      ) {
        this.ShowAdvanceGrid = false;
        this.ShowPaymentGrid = false;
      } else {
        this.ShowAdvanceGrid = true;
        this.ShowPaymentGrid = false;
      }
    } else {
      if (
        this.paymentsForm.get('customer_ref_id')?.value == '' ||
        this.paymentsForm.get('customer_ref_id')?.value == undefined
      ) {
      } else {
        this.ShowAdvanceGrid = false;
        this.ShowPaymentGrid = true;
      }
    }
    this.purchaseorderService
      .getCustomerByType('customer', 'company_name', type[0].master_value)
      .subscribe({
        next: (data: any) => {
          this.SupplierData = data;
          this.filterSupplierData = this.SupplierData.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  advance_amt_apply_change(e: any, i: any) {
    const getValue = <FormArray>this.paymentsForm.get('receipt_details');
    if (
      parseFloat(e.target.value) >
      parseFloat(getValue.at(i).get('old_advance_amount_applied')?.value)
    ) {
      Swal.fire(
        'Advance Amount Not More than ' +
          getValue.at(i).get('old_advance_amount_applied')?.value
      );
      getValue.at(i).get('txn_currency_advance_amount_applied')?.setValue('');
    } else {
      const get_new_amt =
        parseFloat(getValue.at(i).get('invoice_amount')?.value) -
        parseFloat(e.target.value);
      getValue
        .at(i)
        .get('txn_currency_amount')
        ?.setValue(get_new_amt.toFixed(2));
    }
  }

  amt_transaction_change(e: any, i: any) {
    console.log(e, i);
    const getValue = <FormArray>this.paymentsForm.get('receipt_details');

    if (
      getValue.at(i).get('txn_currency_advance_amount_applied')?.value == 0 ||
      getValue.at(i).get('txn_currency_advance_amount_applied')?.value == '' ||
      getValue.at(i).get('txn_currency_advance_amount_applied')?.value ==
        undefined
    ) {
      if (
        parseFloat(e.target.value) >
        parseFloat(getValue.at(i).get('invoice_amount')?.value)
      ) {
        Swal.fire('Amount receivable cannot be greater than Invoice Amount');
        getValue.at(i).get('txn_currency_amount')?.setValue('');
      }

      let txn_currency_gross_amount = 0;
      (<FormArray>this.paymentsForm.get('receipt_details')).value.forEach(
        (ele: any) => {
          if (ele.is_check) {
            console.log(txn_currency_gross_amount, ele.txn_currency_amount);
            txn_currency_gross_amount =
              parseFloat(txn_currency_gross_amount.toString()) +
              parseFloat(ele.txn_currency_amount);
            this.paymentsForm
              .get('txn_currency_gross_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
            this.paymentsForm
              .get('txn_currency_net_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
          }
        }
      );
    } else {
      let amt: any = 0;
      if (
        parseFloat(
          getValue.at(i).get('txn_currency_advance_amount_applied')?.value
        ) > parseFloat(getValue.at(i).get('invoice_amount')?.value)
      ) {
        amt =
          parseFloat(
            getValue.at(i).get('txn_currency_advance_amount_applied')?.value
          ) - parseFloat(getValue.at(i).get('invoice_amount')?.value);
      } else if (
        parseFloat(
          getValue.at(i).get('txn_currency_advance_amount_applied')?.value
        ) < parseFloat(getValue.at(i).get('invoice_amount')?.value)
      ) {
        amt =
          parseFloat(getValue.at(i).get('invoice_amount')?.value) -
          parseFloat(
            getValue.at(i).get('txn_currency_advance_amount_applied')?.value
          );
      }
      console.log(amt);

      let txn_currency_gross_amount = 0;
      (<FormArray>this.paymentsForm.get('receipt_details')).value.forEach(
        (ele: any) => {
          if (ele.is_check) {
            console.log(txn_currency_gross_amount, ele.txn_currency_amount);
            txn_currency_gross_amount =
              parseFloat(txn_currency_gross_amount.toString()) +
              parseFloat(ele.txn_currency_amount);
            this.paymentsForm
              .get('txn_currency_gross_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
            this.paymentsForm
              .get('txn_currency_net_amount')
              ?.setValue(txn_currency_gross_amount.toFixed(2));
          }
        }
      );

      if (parseFloat(e.target.value) > amt) {
        Swal.fire(
          '(Advance + Amount Receivable) cannot be greater than Invoice Amount'
        );
        getValue
          .at(i)
          .get('txn_currency_amount')
          ?.setValue(
            parseFloat(getValue.at(i).get('invoice_amount')?.value) -
              parseFloat(
                getValue.at(i).get('txn_currency_advance_amount_applied')?.value
              )
          );
      } else {
        console.log();
      }
    }
  }

  checkAll() {
    this.checkAllProcess = !this.checkAllProcess;
    this.formArray.controls.map((e) => {
      e.get('isChecked')?.setValue(this.checkAllProcess);
      e.get('is_check')?.setValue(this.checkAllProcess);
    });
    console.log(this.checkAllProcess);

    if (this.checkAllProcess == false) {
      this.paymentsForm.get('txn_currency_gross_amount')?.setValue('');
      this.paymentsForm.get('txn_currency_net_amount')?.setValue('');
      // this.showLoader1=true;
    } else {
      let txn_currency_gross_amount = 0;
      (<FormArray>this.paymentsForm.get('receipt_details')).value.forEach(
        (ele: any) => {
          console.log(txn_currency_gross_amount, ele.txn_currency_amount);
          txn_currency_gross_amount =
            parseFloat(txn_currency_gross_amount.toString()) +
            parseFloat(ele.txn_currency_amount);
          this.paymentsForm
            .get('txn_currency_gross_amount')
            ?.setValue(txn_currency_gross_amount.toFixed(2));
          this.paymentsForm
            .get('txn_currency_net_amount')
            ?.setValue(txn_currency_gross_amount.toFixed(2));
        }
      );
    }
  }
}
