import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesOrderComponent } from './sales-order/sales-order.component';
import { SalesComponent } from './sales/sales.component';
import { DispatchNoteComponent } from './dispatch-note/dispatch-note.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { LotCreationComponent } from './lot-creation/lot-creation.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SalesComponent,
  },
  {
    path: 'sales-order',
    component: SalesOrderComponent,
  },
  {
    path: 'dispatch-note',
    component: DispatchNoteComponent,
  },
  {
    path: 'receipt',
    component: ReceiptComponent,
  },
  {
    path: 'invoice',
    component: InvoiceComponent,
  },
  {
    path: 'lot-creation',
    component: LotCreationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalesRoutingModule {}
