import { DatePipe } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormGroup,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FarmerbillService } from 'src/app/shared/services/farmerbill.service';
import { SalesService } from 'src/app/shared/services/sales.service';
import Swal from 'sweetalert2';
import { QcParameterDialogComponent } from '../qc-parameter-dialog/qc-parameter-dialog.component';
import { GrnService } from 'src/app/shared/services/grn.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { environment } from 'src/environments/environment.development';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-dispatch-note-details',
  templateUrl: './dispatch-note-details.component.html',
  styleUrls: ['./dispatch-note-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class DispatchNoteDetailsComponent {
  fileInputVar!: ElementRef;

  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  dispatchNoteForm!: UntypedFormGroup;
  todayDate = new Date().toJSON().split('T')[0];
  btnVal = 'Submit';
  showReset = true;
  viewEdit = false;
  previewImage: any;
  showFileLoader = false;
  fileName: any;
  soData: any = [];
  customerData: any = [];
  billAddress: any = [];
  shipAddress: any = [];
  itemData: any = [];
  uomData: any = [];
  soTypeData: any = [];
  dispatchView!: boolean;
  qc_params: any = [];
  sub_total_amount: any = 0;
  tax_amount: any = 0;
  grand_total_amount: any = 0;
  tax_rate: any;
  txnCurrency: any;
  filterCustomerData: any = [];
  filterSoData: any = [];
  soAmount: any = 0;
  viewBill = false;
  isQCRequired = false;
  btnDisable = false;
  filteredData: any[] = [];
  parameterData: any = [];
  qualityParameter: any = [];
  quantity: any = [];
  qc_rate: any = [];
  //an
  isViewingImage = false;
  amount: any = [];
  agreed_rate: any = [];
  agreed_amount: any = [];
  avgActualValue: any = [];
  isTransportationView = false;
  farmerAllocated: any = [];
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  base_rate: any = [];

  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  @Input() screenName!: string;
  isDisable = false;
  errorCodeData: any;
  viewOnSendback = false;
  lrAttachmentUrl: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private salesService: SalesService,
    private errorCodes: ErrorCodes,
    private encDecService: EncrDecrService,
    private datepipe: DatePipe,
    private farmerBillService: FarmerbillService,
    private dialog: MatDialog,
    private grnService: GrnService,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    } else {
      this.getAllData('save');
    }
  }

  getAllData(val: any) {
    this.salesService
      .getCustomerFarmer(
        'get_combine_columns_customer_farmer',
        this.screenName,
        'customer',
        'company_name',
        'ref_type',
        val
      )
      .subscribe({
        next: (data: any) => {
          if (data.length == 0) {
            Swal.fire('No Data Available');
          } else {
            this.customerData = data;
            this.filterCustomerData = this.customerData.slice();
          }
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.salesService.getDynamicData('UOM').subscribe({
      next: (data: any) => {
        this.uomData = data['screenmatlistingdata_set'];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicData('Item Master').subscribe({
      next: (data: any) => {
        this.itemData = data['screenmatlistingdata_set'];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.grnService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.txnCurrency = data.filter((d: any) => d.country_name == 'India');
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.dynamicFormService.getDynamicData('error_code_mst', 'code').subscribe({
      next: (res: any) => {
        this.errorCodeData = res;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  initializeForm() {
    this.dispatchNoteForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', Validators.required],
      transaction_ref_no: [''],
      tenant_id: [''],
      customer_ref_id: ['', Validators.required],
      customer_ref_type: ['', Validators.required],
      so_ref_id: ['', Validators.required],
      so_type_id: [''],
      customer_bill_to: ['', Validators.required],
      customer_ship_to: [''],
      lr_no: [''],
      lr_attachment: [''],
      e_way_bill_no: [''],
      txn_currency: [''],
      base_currency: [''],
      conversion_rate: [1],
      txn_currency_transportation_charges: [0, [Validators.min(0)]],
      txn_currency_insurance_charges: [0, [Validators.min(0)]],
      txn_currency_packaging_charges: [0, [Validators.min(0)]],
      txn_currency_sub_total_amount: [''],
      txn_currency_tax_rate: [0, Validators.required],
      txn_currency_tax_amount: [0, Validators.required],
      txn_currency_grand_total_amount: [0, Validators.required],
      base_currency_transportation_charges: [''],
      base_currency_insurance_charges: [0],
      base_currency_packaging_charges: [0],
      base_currency_sub_total_amount: [0],
      base_currency_tax_rate: [0, Validators.required],
      base_currency_tax_amount: [0, Validators.required],
      base_currency_grand_total_amount: [0, Validators.required],
      is_active: true,
      is_deleted: false,
      is_allocation_complete: false,
      dispatch_note_details: this.formBuilder.array([
        this.dispatch_note_details(),
      ]),
      farmer_bill: this.formBuilder.array([this.farmer_bill()]),
      sales_order_no: '',
      workflow_remark: [''],
      error_code_id: [''],
      workflow_status: [''],
      status: [''],
      division_ref_id: [''],
      department_ref_id: [''],
      sale_type_ref_id: [''],
      location_ref_id: [''],
    });
  }

  dispatch_note_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      hsn_sac_no: ['', Validators.required],
      uom: ['', Validators.required],
      order_qty: ['', Validators.required],
      available_stock_qty: ['', Validators.required],
      dispatch_qty: ['', [Validators.required, Validators.min(0)]],
      dispatch_no_of_bags: ['', [Validators.required, Validators.min(0)]],
      rate: ['', Validators.required],
      txn_currency: [''],
      base_currency: [''],
      conversion_rate: [1],
      txn_currency_amount: ['', Validators.required],
      tax_rate: [0, Validators.required],
      txn_currency_igst_rate: [0],
      txn_currency_igst_amount: [0],
      txn_currency_cgst_rate: [0],
      txn_currency_cgst_amount: [0],
      txn_currency_sgst_rate: [0],
      txn_currency_sgst_amount: [0],
      txn_currency_tax_amount: [0],
      txn_currency_total_amount: [0],
      base_currency_amount: [0],
      base_currency_igst_rate: [0],
      base_currency_igst_amount: [0],
      base_currency_cgst_rate: [0],
      base_currency_cgst_amount: [0],
      base_currency_sgst_rate: [0],
      base_currency_sgst_amount: [0],
      base_currency_tax_amount: [0],
      base_currency_total_amount: [0],
      balance_quantity: [0],
      is_active: true,
      is_deleted: false,
      qc_parameter_details: [],
      detail_ref_id: '',
    });
  }

  farmer_bill_details() {
    return this.formBuilder.group({
      id: [''],
      bill_date: [''],
      farmer_bill_no: [''],
      item_ref_id: [''],
      uom: [''],
      no_of_bags: [''],
      quantity: [''],
      qc_rate: [''],
      amount: [''],
      agreed_rate: [''],
      agreed_amount: [''],
      farmer_name: [''],
      item_name: [''],
      uom_name: [''],
      actual_value: [''],
      base_rate: [''],
    });
  }

  farmer_bill() {
    return this.formBuilder.array([this.farmer_bill_details()]);
  }

  customerChange(event: any) {
    console.log(event);
    this.dispatchNoteForm.get('txn_currency_grand_total_amount')?.setValue('');
    this.dispatchNoteForm.get('dispatch_note_details')?.reset();
    this.salesService.getSalesOrder(event, 'Dispatch Note').subscribe({
      next: (data: any) => {
        this.soData = data;
        this.filterSoData = this.soData.slice();
        this.soTypeData = [];
        this.billAddress = [];
        this.shipAddress = [];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  soChange(event: any) {
    this.farmerBillService.getAllMasterData('SO Type').subscribe({
      next: (data: any) => {
        this.soTypeData = data;
        console.log(event);
        const ele = this.soData.filter((x: any) => x.id == event)[0];
        console.log(ele);
        console.log(this.soTypeData);
        this.dispatchNoteForm
          .get('division_ref_id')
          ?.setValue(ele.division_ref_id);
        this.dispatchNoteForm
          .get('department_ref_id')
          ?.setValue(ele.department_ref_id);
        this.dispatchNoteForm
          .get('sale_type_ref_id')
          ?.setValue(ele.sale_type_ref_id);
        this.dispatchNoteForm
          .get('location_ref_id')
          ?.setValue(ele.location_ref_id);
        this.dispatchNoteForm.get('division_ref_id')?.disable();

        const soType = this.soTypeData.filter(
          (x: any) => x.id == ele.so_type_ref_id
        )[0];
        console.log(soType);
        if (soType.master_key == 'Commision') {
          const items: any[] = [];
          ele.order_details.filter((r: any) => {
            if (r.qc_parameter_details.length > 0) {
              this.isQCRequired = true;
            }
            items.push(r.item_ref_id);
          });
          console.log('=====', items);
          this.salesService
            .getFarmerBillCount('farmerBillExists', items.toString())
            .subscribe({
              next: (res: any) => {
                if (res == 0) {
                  this.showSwalmessage(
                    'Purchase Booking Is Not Generated',
                    '',
                    'error',
                    false
                  );
                  return;
                } else {
                  this.getDetailsData(ele, event);
                }
              },
              error: (e) => {
                this.showSwalmessage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  '',
                  'error',
                  false
                );
              },
            });
        } else {
          this.getDetailsData(ele, event);
        }
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  getDetailsData(ele: any, event: any) {
    this.formArr.clear();
    const masterkey = 'customer_details';
    const parent_id = ele.customer_ref_id;
    this.salesService
      .getBillingShippingAdrress(masterkey, parent_id)
      .subscribe({
        next: (data: any) => {
          this.billAddress = data['Billing'];
          this.shipAddress = data['Shipping'];
          this.dispatchNoteForm
            .get('customer_bill_to')
            ?.setValue(ele.customer_bill_from);
          this.dispatchNoteForm
            .get('customer_ship_to')
            ?.setValue(ele.customer_ship_to);
          this.dispatchNoteForm.get('customer_bill_to')?.disable();
          this.dispatchNoteForm.get('customer_ship_to')?.disable();
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    this.dispatchNoteForm
      .get('customer_ref_type')
      ?.patchValue(ele.customer_ref_type);
    this.dispatchNoteForm.get('so_type_id')?.patchValue(ele.so_type_ref_id);
    this.dispatchNoteForm
      .get('sales_order_no')
      ?.patchValue(ele.transaction_ref_no);
    this.dispatchNoteForm.get('so_type_id')?.disable();
    this.dispatchNoteForm
      .get('txn_currency_transportation_charges')
      ?.setValue(ele.transportation_charges);

    if (ele.so_type == 'Spot') {
      ele.order_details.forEach((ele: any) => {
        this.soAmount =
          parseFloat(this.soAmount.toString()) +
          parseFloat(ele.txn_currency_amount);
      });
      this.isTransportationView = true;
    }

    console.log(ele.order_details.length, this.formArr.value.length);

    ele.order_details.forEach((elem: any, ind: number) => {
      if (ele.order_details.length > this.formArr.value.length) {
        this.formArr.push(this.dispatch_note_details());
        // } else if (
        //   ele.order_details.length <= this.formArr.value.length &&
        //   this.submitBtn
        // ) {
        //   this.formArr.clear();
        //   this.formArr.push(this.dispatch_note_details());
      }

      const gridRow = (<FormArray>(
        this.dispatchNoteForm.get('dispatch_note_details')
      )).at(ind);
      console.log(this.formArr);
      gridRow.get('item_ref_id')?.setValue(elem.item_ref_id);
      gridRow.get('uom')?.setValue(elem.uom);
      gridRow.get('hsn_sac_no')?.setValue(elem.hsn_sac_no);
      gridRow.get('order_qty')?.setValue(elem.balance_quantity);
      gridRow.get('rate')?.setValue(elem.rate);
      gridRow.get('detail_ref_id')?.setValue(elem.id);

      gridRow.get('item_ref_id')?.disable();
      gridRow.get('uom')?.disable();

      this.salesService
        .getAvailableQty(elem.item_ref_id, ele.so_type_ref_name, ele.id)
        .subscribe({
          next: (res: any) => {
            if (res == 0) {
              const item_name = this.itemData.filter(
                (x: any) => x.id == elem.item_ref_id
              );
              this.showSwalmessage(
                'Stock Is Not Available For ' + item_name[0]['name'] + ' Item',
                '',
                'error',
                false
              );
              this.dispatchNoteForm
                .get('txn_currency_grand_total_amount')
                ?.setValue('');
              return;
            } else {
              gridRow
                .get('available_stock_qty')
                ?.setValue(parseFloat(res).toFixed(4));
              gridRow.get('available_stock_qty')?.disable();
              if (parseFloat(res) >= parseFloat(elem.balance_quantity)) {
                gridRow
                  .get('dispatch_qty')
                  ?.setValue(parseFloat(elem.balance_quantity).toFixed(4));
                gridRow
                  .get('balance_quantity')
                  ?.setValue(parseFloat(elem.balance_quantity).toFixed(4));
                const amt =
                  parseFloat(elem.balance_quantity) * parseFloat(elem.rate);
                gridRow
                  .get('txn_currency_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                gridRow
                  .get('base_currency_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                const tax_amt = parseFloat(
                  gridRow.get('txn_currency_tax_amount')?.value
                );
                gridRow
                  .get('txn_currency_total_amount')
                  ?.setValue(amt + tax_amt.toFixed(2));
                gridRow
                  .get('base_currency_total_amount')
                  ?.setValue(amt + tax_amt.toFixed(2));
              } else if (parseFloat(res) < parseFloat(elem.balance_quantity)) {
                gridRow
                  .get('dispatch_qty')
                  ?.setValue(parseFloat(res).toFixed(4));
                const amt = parseFloat(res) * parseFloat(elem.rate);
                gridRow
                  .get('txn_currency_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                gridRow
                  .get('base_currency_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                const tax_amt = parseFloat(
                  gridRow.get('txn_currency_tax_amount')?.value
                );
                gridRow
                  .get('balance_quantity')
                  ?.setValue(parseFloat(elem.balance_quantity).toFixed(4));
                gridRow
                  .get('txn_currency_total_amount')
                  ?.setValue(amt + tax_amt.toFixed(2));
                gridRow
                  .get('base_currency_total_amount')
                  ?.setValue(amt + tax_amt.toFixed(2));
              }

              this.salesService.getTaxRate(elem.hsn_sac_no).subscribe({
                next: (res1: any) => {
                  console.log(res1);
                  gridRow.get('tax_rate')?.setValue(res1);
                  const amt = gridRow.get('txn_currency_amount')?.value;
                  const tax_amt = (parseFloat(res1) * parseFloat(amt)) / 100;
                  console.log(tax_amt);
                  gridRow
                    .get('txn_currency_tax_amount')
                    ?.setValue(tax_amt.toFixed(2));
                  gridRow
                    .get('base_currency_tax_amount')
                    ?.setValue(tax_amt.toFixed(2));
                  gridRow
                    .get('txn_currency_total_amount')
                    ?.setValue((parseFloat(amt) + tax_amt).toFixed(2));
                  gridRow
                    .get('base_currency_total_amount')
                    ?.setValue(parseFloat(amt) + tax_amt);

                  let base_grand_total = 0;
                  let txn_grand_total = 0;
                  let total_amt = 0;
                  let grand_amount = 0;
                  this.dispatchNoteForm.value.dispatch_note_details.forEach(
                    (ele1: any) => {
                      console.log(ele1);
                      total_amt =
                        parseFloat(total_amt.toString()) +
                        parseFloat(ele1.txn_currency_amount);
                      grand_amount =
                        parseFloat(grand_amount.toString()) +
                        parseFloat(ele1.txn_currency_total_amount);
                      console.log(total_amt, this.soAmount);
                    }
                  );

                  let charges = 0;
                  if (this.soAmount != 0) {
                    const proportion =
                      parseFloat(total_amt.toString()) /
                      parseFloat(this.soAmount);
                    console.log(proportion);
                    const so = this.soData.filter((x: any) => x.id == event)[0];
                    console.log(so);
                    charges = so.transportation_charges * proportion;
                  }

                  console.log(charges);
                  this.dispatchNoteForm
                    .get('txn_currency_transportation_charges')
                    ?.setValue(charges.toFixed(2));
                  this.dispatchNoteForm
                    .get('base_currency_transportation_charges')
                    ?.setValue(charges.toFixed(2));

                  const pack_charge = this.dispatchNoteForm.get(
                    'txn_currency_packaging_charges'
                  )?.value;
                  const insu_charge = this.dispatchNoteForm.get(
                    'txn_currency_insurance_charges'
                  )?.value;
                  const trans_charge = this.dispatchNoteForm.get(
                    'txn_currency_transportation_charges'
                  )?.value;

                  this.sub_total_amount =
                    parseFloat(pack_charge) +
                    parseFloat(insu_charge) +
                    parseFloat(trans_charge);
                  this.sub_total_amount = parseFloat(
                    this.sub_total_amount
                  ).toFixed(2);
                  this.dispatchNoteForm
                    .get('txn_currency_sub_total_amount')
                    ?.setValue(this.sub_total_amount);
                  this.dispatchNoteForm
                    .get('base_currency_sub_total_amount')
                    ?.setValue(this.sub_total_amount);

                  const tax_amount = this.dispatchNoteForm.get(
                    'txn_currency_tax_amount'
                  )?.value;

                  base_grand_total =
                    parseFloat(grand_amount.toString()) +
                    parseFloat(this.sub_total_amount) +
                    parseFloat(tax_amount);
                  txn_grand_total =
                    parseFloat(grand_amount.toString()) +
                    parseFloat(this.sub_total_amount) +
                    parseFloat(tax_amount);

                  this.dispatchNoteForm
                    .get('base_currency_grand_total_amount')
                    ?.setValue(
                      parseFloat(base_grand_total.toString()).toFixed(2)
                    );
                  this.dispatchNoteForm
                    .get('txn_currency_grand_total_amount')
                    ?.setValue(
                      parseFloat(txn_grand_total.toString()).toFixed(2)
                    );
                  console.log(txn_grand_total, 50000, txn_grand_total > 50000);
                  if (txn_grand_total > 50000) {
                    console.log('if');
                    this.dispatchNoteForm
                      .get('e_way_bill_no')
                      ?.addValidators(Validators.required);
                  } else {
                    console.log('else');
                    this.dispatchNoteForm
                      .get('e_way_bill_no')
                      ?.clearValidators();
                  }
                  this.dispatchNoteForm
                    .get('e_way_bill_no')
                    ?.updateValueAndValidity();
                  this.charges();
                },
                error: (e) => {
                  this.showSwalmessage(
                    this.errorCodes.getErrorMessage(JSON.parse(e).status),
                    '',
                    'error',
                    false
                  );
                },
              });
            }
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
    });
  }

  chargesChange() {
    this.charges();
  }

  chargesChange1() {
    this.charges();
  }

  chargesChange2() {
    this.charges();
  }

  charges() {
    const cnt = this.dispatchNoteForm.value.dispatch_note_details.length;
    if (cnt > 1) {
      this.tax_rate = 18;
      this.dispatchNoteForm
        .get('txn_currency_tax_rate')
        ?.setValue(this.tax_rate);
      this.dispatchNoteForm
        .get('base_currency_tax_rate')
        ?.setValue(parseFloat(this.tax_rate).toFixed(2));
    } else {
      this.tax_rate =
        this.dispatchNoteForm.value.dispatch_note_details[0].tax_rate;
      this.dispatchNoteForm
        .get('txn_currency_tax_rate')
        ?.setValue(this.tax_rate);
      this.dispatchNoteForm
        .get('base_currency_tax_rate')
        ?.setValue(parseFloat(this.tax_rate).toFixed(2));
    }

    const pack_charge = this.dispatchNoteForm.get(
      'txn_currency_packaging_charges'
    )?.value;
    const insu_charge = this.dispatchNoteForm.get(
      'txn_currency_insurance_charges'
    )?.value;
    const trans_charge = this.dispatchNoteForm.get(
      'txn_currency_transportation_charges'
    )?.value;
    this.dispatchNoteForm
      .get('base_currency_packaging_charges')
      ?.setValue(pack_charge.toFixed(2));
    this.dispatchNoteForm
      .get('base_currency_insurance_charges')
      ?.setValue(insu_charge.toFixed(2));
    this.dispatchNoteForm
      .get('base_currency_transportation_charges')
      ?.setValue(trans_charge);

    this.sub_total_amount =
      parseFloat(pack_charge) +
      parseFloat(insu_charge) +
      parseFloat(trans_charge);
    console.log(this.sub_total_amount);
    this.sub_total_amount = this.sub_total_amount.toFixed(2);
    console.log(this.sub_total_amount);
    this.tax_amount = (this.tax_rate * this.sub_total_amount) / 100;
    this.tax_amount = parseFloat(this.tax_amount).toFixed(4);
    this.grand_total_amount = this.tax_amount + this.sub_total_amount;
    this.grand_total_amount = parseFloat(this.grand_total_amount).toFixed(2);

    this.dispatchNoteForm
      .get('base_currency_sub_total_amount')
      ?.setValue(parseFloat(this.sub_total_amount).toFixed(2));
    this.dispatchNoteForm
      .get('txn_currency_sub_total_amount')
      ?.setValue(this.sub_total_amount);
    this.dispatchNoteForm
      .get('base_currency_tax_amount')
      ?.setValue(parseFloat(this.tax_amount).toFixed(2));
    this.dispatchNoteForm
      .get('txn_currency_tax_amount')
      ?.setValue(parseFloat(this.tax_amount).toFixed(2));

    let base_grand_total = 0;
    let txn_grand_total = 0;
    let total_amt = 0;
    this.dispatchNoteForm.value.dispatch_note_details.forEach((ele: any) => {
      console.log(ele);
      total_amt =
        parseFloat(total_amt.toString()) +
        parseFloat(ele.txn_currency_total_amount);
    });

    base_grand_total =
      parseFloat(total_amt.toString()) +
      parseFloat(this.sub_total_amount) +
      parseFloat(this.tax_amount);
    txn_grand_total =
      parseFloat(total_amt.toString()) +
      parseFloat(this.sub_total_amount) +
      parseFloat(this.tax_amount);

    this.dispatchNoteForm
      .get('base_currency_grand_total_amount')
      ?.setValue(parseFloat(base_grand_total.toString()).toFixed(2));
    this.dispatchNoteForm
      .get('txn_currency_grand_total_amount')
      ?.setValue(parseFloat(txn_grand_total.toString()).toFixed(2));
    console.log(txn_grand_total, 50000, txn_grand_total > 50000);
    if (txn_grand_total > 50000) {
      console.log('if');
      this.dispatchNoteForm
        .get('e_way_bill_no')
        ?.addValidators(Validators.required);
    } else {
      console.log('else');
      this.dispatchNoteForm.get('e_way_bill_no')?.clearValidators();
    }
    this.dispatchNoteForm.get('e_way_bill_no')?.updateValueAndValidity();
  }

  // dispatchQtyChange(event: any, j: number) {
  //   const gridRow = (<FormArray>(
  //     this.dispatchNoteForm.get('dispatch_note_details')
  //   )).at(j);
  //   console.log(
  //     gridRow.get('order_qty')?.value,
  //     gridRow.get('available_stock_qty')?.value,
  //     event.target.value
  //   );
  //   console.log(
  //     (<FormArray>this.dispatchNoteForm.get('dispatch_note_details')).controls[
  //       j
  //     ].get('dispatch_qty')
  //   );
  //   const available_qty = gridRow.get('available_stock_qty')?.value;
  //   const order_qty = gridRow.get('order_qty')?.value;
  //   if (
  //     parseFloat(available_qty) >= parseFloat(order_qty) &&
  //     parseFloat(event.target.value) > parseFloat(order_qty)
  //   ) {
  //     this.showSwalmessage(
  //       'Dispatch Quantity Should Not Exceed than Order Quantity',
  //       '',
  //       'error',
  //       false
  //     );
  //     gridRow
  //       .get('dispatch_qty')
  //       ?.setErrors(Validators.maxLength(parseFloat(available_qty)));
  //   } else if (
  //     parseFloat(available_qty) < parseFloat(order_qty) &&
  //     parseFloat(event.target.value) > parseFloat(available_qty)
  //   ) {
  //     this.showSwalmessage(
  //       'Dispatch Quantity Should Not Exceed than Avaiable Stock Quantity',
  //       '',
  //       'error',
  //       false
  //     );
  //     gridRow
  //       .get('dispatch_qty')
  //       ?.setErrors(Validators.maxLength(parseFloat(order_qty)));
  //   }
  //   const amt =
  //     parseFloat(event.target.value) * parseFloat(gridRow.get('rate')?.value);
  //   gridRow
  //     .get('txn_currency_amount')
  //     ?.setValue(parseFloat(amt.toString()).toFixed(2));
  //   gridRow
  //     .get('base_currency_amount')
  //     ?.setValue(parseFloat(amt.toString()).toFixed(2));

  //   const amt1 = gridRow.get('txn_currency_amount')?.value;
  //   const rate = parseFloat(gridRow.get('rate')?.value);
  //   const tax_amt1 = (amt * parseFloat(gridRow.get('tax_rate')?.value)) / 100;
  //   console.log(tax_amt1);
  //   gridRow.get('txn_currency_tax_amount')?.setValue(tax_amt1.toFixed(2));
  //   gridRow.get('base_currency_tax_amount')?.setValue(tax_amt1.toFixed(2));
  //   gridRow
  //     .get('txn_currency_total_amount')
  //     ?.setValue((amt + tax_amt1).toFixed(2));
  //   gridRow
  //     .get('base_currency_total_amount')
  //     ?.setValue((amt + tax_amt1).toFixed(2));

  //   let base_grand_total = 0;
  //   let txn_grand_total = 0;
  //   let total_amt = 0;
  //   let grand_amount = 0;
  //   this.dispatchNoteForm.value.dispatch_note_details.forEach((ele: any) => {
  //     console.log(ele);
  //     total_amt =
  //       parseFloat(total_amt.toString()) + parseFloat(ele.txn_currency_amount);
  //     grand_amount =
  //       parseFloat(total_amt.toString()) +
  //       parseFloat(ele.txn_currency_total_amount);
  //     console.log(total_amt, this.soAmount, grand_amount);
  //   });

  //   if (this.soAmount != 0) {
  //     const proportion =
  //       parseFloat(total_amt.toString()) / parseFloat(this.soAmount);
  //     console.log(proportion);
  //     const so = this.soData.filter(
  //       (x: any) => x.id == this.dispatchNoteForm.get('so_ref_id')?.value
  //     )[0];
  //     const charges = so.transportation_charges * proportion;
  //     console.log(charges);
  //     this.dispatchNoteForm
  //       .get('txn_currency_transportation_charges')
  //       ?.setValue(charges.toFixed(2));
  //     this.dispatchNoteForm
  //       .get('base_currency_transportation_charges')
  //       ?.setValue(charges.toFixed(2));
  //   }

  //   const pack_charge = this.dispatchNoteForm.get(
  //     'txn_currency_packaging_charges'
  //   )?.value;
  //   const insu_charge = this.dispatchNoteForm.get(
  //     'txn_currency_insurance_charges'
  //   )?.value;
  //   const trans_charge = this.dispatchNoteForm.get(
  //     'txn_currency_transportation_charges'
  //   )?.value;

  //   this.sub_total_amount =
  //     parseFloat(pack_charge) +
  //     parseFloat(insu_charge) +
  //     parseFloat(trans_charge);
  //   this.sub_total_amount = parseFloat(this.sub_total_amount).toFixed(2);
  //   this.dispatchNoteForm
  //     .get('txn_currency_sub_total_amount')
  //     ?.setValue(this.sub_total_amount);
  //   this.dispatchNoteForm
  //     .get('base_currency_sub_total_amount')
  //     ?.setValue(this.sub_total_amount);

  //   base_grand_total =
  //     parseFloat(grand_amount.toString()) +
  //     parseFloat(this.sub_total_amount) +
  //     parseFloat(tax_amt1.toString());
  //   txn_grand_total =
  //     parseFloat(grand_amount.toString()) +
  //     parseFloat(this.sub_total_amount) +
  //     parseFloat(tax_amt1.toString());

  //   this.dispatchNoteForm
  //     .get('base_currency_grand_total_amount')
  //     ?.setValue(parseFloat(base_grand_total.toString()).toFixed(2));
  //   this.dispatchNoteForm
  //     .get('txn_currency_grand_total_amount')
  //     ?.setValue(parseFloat(txn_grand_total.toString()).toFixed(2));
  //   console.log(txn_grand_total, 50000, txn_grand_total > 50000);
  //   if (txn_grand_total > 50000) {
  //     console.log('if');
  //     this.dispatchNoteForm
  //       .get('e_way_bill_no')
  //       ?.addValidators(Validators.required);
  //   } else {
  //     console.log('else');
  //     this.dispatchNoteForm.get('e_way_bill_no')?.clearValidators();
  //   }
  //   this.dispatchNoteForm.get('e_way_bill_no')?.updateValueAndValidity();
  //   this.charges();
  // }

  dispatchQtyChange(event: any, j: number) {
    const gridRow = (<FormArray>(
      this.dispatchNoteForm.get('dispatch_note_details')
    )).at(j);
    console.log(
      gridRow.get('order_qty')?.value,
      gridRow.get('available_stock_qty')?.value,
      event.target.value
    );
    console.log(
      (<FormArray>this.dispatchNoteForm.get('dispatch_note_details')).controls[
        j
      ].get('dispatch_qty')
    );
    const available_qty = gridRow.get('available_stock_qty')?.value;
    const order_qty = gridRow.get('order_qty')?.value;
    if (
      parseFloat(available_qty) >= parseFloat(order_qty) &&
      parseFloat(event.target.value) > parseFloat(order_qty)
    ) {
      this.showSwalmessage(
        'Dispatch Quantity Should Not Exceed than Order Quantity',
        '',
        'error',
        false
      );
      gridRow
        .get('dispatch_qty')
        ?.setErrors(Validators.maxLength(parseFloat(available_qty)));
    } else if (
      parseFloat(available_qty) < parseFloat(order_qty) &&
      parseFloat(event.target.value) > parseFloat(available_qty)
    ) {
      this.showSwalmessage(
        'Dispatch Quantity Should Not Exceed than Avaiable Stock Quantity',
        '',
        'error',
        false
      );
      gridRow
        .get('dispatch_qty')
        ?.setErrors(Validators.maxLength(parseFloat(order_qty)));
    }
    const amt =
      parseFloat(event.target.value) * parseFloat(gridRow.get('rate')?.value);
    gridRow
      .get('txn_currency_amount')
      ?.setValue(parseFloat(amt.toString()).toFixed(2));
    gridRow
      .get('base_currency_amount')
      ?.setValue(parseFloat(amt.toString()).toFixed(2));

    const amt1 = gridRow.get('txn_currency_amount')?.value;
    const rate = parseFloat(gridRow.get('rate')?.value);
    const tax_amt1 = (amt * parseFloat(gridRow.get('tax_rate')?.value)) / 100;
    console.log(tax_amt1);
    gridRow.get('txn_currency_tax_amount')?.setValue(tax_amt1.toFixed(2));
    gridRow.get('base_currency_tax_amount')?.setValue(tax_amt1.toFixed(2));
    gridRow
      .get('txn_currency_total_amount')
      ?.setValue((amt + tax_amt1).toFixed(2));
    gridRow
      .get('base_currency_total_amount')
      ?.setValue((amt + tax_amt1).toFixed(2));

    let base_grand_total = 0;
    let txn_grand_total = 0;
    let total_amt = 0;
    let grand_amount = 0;
    this.dispatchNoteForm.value.dispatch_note_details.forEach((ele: any) => {
      console.log(ele);
      total_amt =
        parseFloat(total_amt.toString()) + parseFloat(ele.txn_currency_amount);
      grand_amount =
        parseFloat(total_amt.toString()) +
        parseFloat(ele.txn_currency_total_amount);
      console.log(total_amt, this.soAmount);
    });

    if (this.soAmount != 0) {
      const proportion =
        parseFloat(total_amt.toString()) / parseFloat(this.soAmount);
      console.log(proportion);
      const so = this.soData.filter(
        (x: any) => x.id == this.dispatchNoteForm.get('so_ref_id')?.value
      )[0];
      const charges = so.transportation_charges * proportion;
      console.log(charges);
      this.dispatchNoteForm
        .get('txn_currency_transportation_charges')
        ?.setValue(charges.toFixed(2));
      this.dispatchNoteForm
        .get('base_currency_transportation_charges')
        ?.setValue(charges.toFixed(2));
    }

    const pack_charge = this.dispatchNoteForm.get(
      'txn_currency_packaging_charges'
    )?.value;
    const insu_charge = this.dispatchNoteForm.get(
      'txn_currency_insurance_charges'
    )?.value;
    const trans_charge = this.dispatchNoteForm.get(
      'txn_currency_transportation_charges'
    )?.value;

    this.sub_total_amount =
      parseFloat(pack_charge) +
      parseFloat(insu_charge) +
      parseFloat(trans_charge);
    this.sub_total_amount = parseFloat(this.sub_total_amount).toFixed(2);
    this.dispatchNoteForm
      .get('txn_currency_sub_total_amount')
      ?.setValue(this.sub_total_amount);
    this.dispatchNoteForm
      .get('base_currency_sub_total_amount')
      ?.setValue(this.sub_total_amount);

    base_grand_total =
      parseFloat(grand_amount.toString()) +
      parseFloat(this.sub_total_amount) +
      parseFloat(tax_amt1.toString());
    txn_grand_total =
      parseFloat(grand_amount.toString()) +
      parseFloat(this.sub_total_amount) +
      parseFloat(tax_amt1.toString());

    this.dispatchNoteForm
      .get('base_currency_grand_total_amount')
      ?.setValue(parseFloat(base_grand_total.toString()).toFixed(2));
    this.dispatchNoteForm
      .get('txn_currency_grand_total_amount')
      ?.setValue(parseFloat(txn_grand_total.toString()).toFixed(2));
    console.log(txn_grand_total, 50000, txn_grand_total > 50000);
    if (txn_grand_total > 50000) {
      console.log('if');
      this.dispatchNoteForm
        .get('e_way_bill_no')
        ?.addValidators(Validators.required);
      this.dispatchNoteForm.get('e_way_bill_no')?.setErrors({ required: true });
    } else {
      console.log('else');
      this.dispatchNoteForm.get('e_way_bill_no')?.clearValidators();
    }
    this.dispatchNoteForm.get('e_way_bill_no')?.updateValueAndValidity();
    this.charges();
  }

  addQcParameterDetails(index: any) {
    console.log(index);
    console.log(this.dispatchNoteForm);
    const gridRow = (<FormArray>(
      this.dispatchNoteForm.get('dispatch_note_details')
    )).at(index);
    gridRow.value['qc_parameter_details'] = [''];
    console.log(gridRow, 'hiiii');

    gridRow.value['qc_parameter_details'] = [];
    gridRow.get('item_ref_id')?.enable();
    console.log(this.rowData);

    if (!gridRow.get('item_ref_id')?.value) {
      this.showSwalmessage('Please Select Item First', '', 'error', false);
      return;
    }
    if (!Array.isArray(this.rowData)) {
      gridRow.value['qc_parameter_details'] =
        this.rowData['dispatch_note_details'][index]['qc_parameter_details'];
    }
    if (this.qc_params[index]) {
      gridRow.value['qc_parameter_details'] = this.qc_params[index];
    }
    gridRow.value['submitBtn'] = this.submitBtn;
    console.log('Grid ROW....', gridRow);

    gridRow.value['so_ref_id'] = this.dispatchNoteForm.getRawValue().so_ref_id;

    console.log(
      'this.dispatchNoteForm.value.so_ref_id',
      this.dispatchNoteForm.value
    );
    console.log(gridRow, 'GRID VALUEEEEEEEEEEEEEEEEEE');

    this.salesService
      .getQualityParameterInSO(
        gridRow.get('item_ref_id')?.value,
        this.dispatchNoteForm.getRawValue().so_ref_id
      )
      .subscribe({
        next: (res1: any) => {
          if (res1.length > 0) {
            const dialogRef: MatDialogRef<QcParameterDialogComponent> =
              this.dialog.open(QcParameterDialogComponent, {
                data: gridRow,
              });
            dialogRef.afterClosed().subscribe((result: any) => {
              if (result) {
                this.isQCRequired = true;
                result['detail_ref_id'] = gridRow.get('detail_ref_id')?.value;
                console.log(result);
                //changes Ankita
                // result['qc_pameter_details'].reset();
                if (this.submitBtn) {
                  this.salesService.getStatus(result).subscribe({
                    next: (res: any) => {
                      console.log(res);
                      this.dispatchNoteForm
                        .get('workflow_status')
                        ?.setValue(res);
                      if (res == 'Accepted') {
                        this.dispatchNoteForm.get('status')?.setValue('Open');
                      }
                      if (res == 'Rejected') {
                        this.dispatchNoteForm.get('status')?.setValue('Cancel');
                      }
                      this.qc_params[index] = result['qc_parameter_details'];
                      gridRow.value['qc_parameter_details'] =
                        result['qc_parameter_details'];
                    },
                    error: (e) => {
                      this.showFileLoader = false;
                      this.showSwalmessage(
                        this.errorCodes.getErrorMessage(JSON.parse(e).status),
                        '',
                        'error',
                        false
                      );
                    },
                  });
                } else {
                  this.qc_params[index] = result['qc_parameter_details'];
                  gridRow.value['qc_parameter_details'] =
                    result['qc_parameter_details'];
                }
              }
            });
          } else {
            this.isQCRequired = false;
            this.showSwalmessage(
              'Quality Parameters Are Not Defined In Sales Order',
              '',
              'error',
              false
            );
            this.dispatchNoteForm.get('workflow_status')?.setValue('Accepted');
            this.dispatchNoteForm.get('status')?.setValue('Open');
            return;
          }
        },
        error: (e) => {
          this.showFileLoader = false;
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
  }

  //changes Ankita

  onResetForm() {
    this.initializeForm();
    console.log('qrpppp', this.qc_params.length);
    for (let i = 0; i < this.qc_params.length; i++) {
      for (let j = 0; j < this.qc_params[i].length; j++) {
        this.qc_params[i][j]['actual_value'] = '';
        console.log('this.qc_params[i]: ', this.qc_params[i]);
      }
      // this.qc_params[i][0]['actual_value'] = '';
    }

    // this.qc_params('');
  }

  deleteSelectedFile() {
    this.fileName = '';
    const fileInput = this.fileInput.nativeElement;
    if (fileInput) {
      fileInput.value = '';
    }
  }

  openImage(path: any) {
    console.log('path: ', path);
    window.open(path, '_blank');
  }

  downloadImage(path: any) {
    const link = document.createElement('a');
    link.href = path;
    link.target = '_blank';
    link.download = 'downloaded_image'; // You can set the downloaded file name here
    link.click();
  }
  setAttachment(event: any) {
    this.previewImage = '';
    const fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      const reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage = reader.result;
      };
      this.fileName = fileToUpload['name'];
      const fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      const compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        this.showFileLoader = true;
        const formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');

        this.salesService.saveFile(formData1).subscribe({
          next: (data: any) => {
            this.showFileLoader = false;
            const files3path = data['s3_file_path'];
            console.log('files3path: ', files3path);
            this.lrAttachmentUrl =
              environment.endpoint_url_cdn + '/' + data['s3_file_path'];
            this.dispatchNoteForm.get('lr_attachment')?.setValue(files3path);
            this.showSwalmessage(
              'File Uploaded Successfully!',
              '',
              'success',
              false
            );
          },
          error: (e) => {
            this.showFileLoader = false;
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
      } else {
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.showSwalmessage('File Size Not More Than 5 MB', '', 'error', false);
    }
  }

  removeImage() {
    this.previewImage = '';
  }

  viewRecord(data: any) {
    console.log(data);
    console.log(data.so_ref_id, 'SOOOO REFFFF IDDD');

    this.viewEdit = true;
    // this.btnDisable = true;
    this.getAllData('view');

    if (data.href_attchment_path != '') {
      this.previewImage = this.encDecService.decryptedData(
        data.href_attchment_path
      );
    }
    this.dispatchNoteForm.patchValue({
      id: data.id,
      transaction_date: data.transaction_date,
      transaction_ref_no: data.transaction_ref_no,
      tenant_id: data.tenant_id,
      customer_ref_id: data.customer_ref_id,
      customer_ref_type: data.customer_ref_type,
      so_ref_id: data.so_ref_id,
      // so_type_id: data.so_type_id,
      customer_bill_to: data.customer_bill_to,
      customer_ship_to: data.customer_ship_to,
      lr_no: data.lr_no,
      lr_attachment: data.lr_attachment,
      e_way_bill_no: data.e_way_bill_no,
      txn_currency: data.txn_currency,
      base_currency: data.base_currency,
      conversion_rate: data.conversion_rate,
      txn_currency_transportation_charges: parseFloat(
        data.txn_currency_transportation_charges
      ).toFixed(2),
      txn_currency_insurance_charges: parseFloat(
        data.txn_currency_insurance_charges
      ).toFixed(2),
      txn_currency_packaging_charges: parseFloat(
        data.txn_currency_packaging_charges
      ).toFixed(2),
      txn_currency_sub_total_amount: parseFloat(
        data.txn_currency_sub_total_amount
      ).toFixed(2),
      txn_currency_tax_rate: '',
      txn_currency_tax_amount: parseFloat(data.txn_currency_tax_amount).toFixed(
        2
      ),
      txn_currency_grand_total_amount: parseFloat(
        data.txn_currency_grand_total_amount
      ).toFixed(2),
      base_currency_transportation_charges: parseFloat(
        data.base_currency_transportation_charges
      ).toFixed(2),
      base_currency_insurance_charges: parseFloat(
        data.base_currency_insurance_charges
      ).toFixed(2),
      base_currency_packaging_charges: parseFloat(
        data.base_currency_packaging_charges
      ).toFixed(2),
      base_currency_sub_total_amount: parseFloat(
        data.base_currency_sub_total_amount
      ).toFixed(2),
      base_currency_tax_rate: '',
      base_currency_tax_amount: parseFloat(
        data.base_currency_tax_amount
      ).toFixed(2),
      base_currency_grand_total_amount: parseFloat(
        data.base_currency_grand_total_amount
      ).toFixed(2),
      is_active: data.is_active,
      is_deleted: data.is_deleted,
      sales_order_no: data.sales_order_no,
      workflow_remark: data.workflow_remark,
      error_code_id: data.error_code_id,
      division_ref_id: data.division_ref_id,
      department_ref_id: data.department_ref_id,
      sale_type_ref_id: data.sale_type_ref_id,
      location_ref_id: data.location_ref_id,
    });

    this.salesService.getAllocationData(data.id).subscribe({
      next: (res: any) => {
        console.log(res);
        if (res.length > 0) {
          this.viewBill = true;
          // this.dispatchNoteForm.setControl('farmer_bill', this.setExistingArray1(res));
          res.forEach((element: any, ind: number) => {
            this.dispatchNoteForm.setControl(
              'farmer_bill',
              this.setExistingArray1(element, ind)
            );
          });
        }
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getSalesOrderById(data.so_ref_id).subscribe({
      next: (res: any) => {
        console.log('SO DATAAAAAAAAAA', res);

        this.soData.push(res);
        console.log(this.soData);
        this.filterSoData = this.soData.slice();

        this.farmerBillService.getAllMasterData('SO Type').subscribe({
          next: (data: any) => {
            this.soTypeData = data;
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });

        const ele = this.soData.filter((x: any) => (x.id = data.so_ref_id))[0];
        console.log(ele, 'PIYUSHAAAAAAAAAAAAAA');

        this.dispatchNoteForm.get('so_type_id')?.patchValue(ele.so_type_ref_id);
        if (ele.so_type == 'Spot') {
          this.isTransportationView = true;
        }
        const masterkey = 'customer_details';
        const parent_id = ele.customer_ref_id;
        this.salesService
          .getBillingShippingAdrress(masterkey, parent_id)
          .subscribe({
            next: (data: any) => {
              this.billAddress = data['Billing'];
              this.shipAddress = data['Shipping'];
              this.dispatchNoteForm
                .get('customer_bill_to')
                ?.setValue(ele.customer_bill_from);
              this.dispatchNoteForm
                .get('customer_ship_to')
                ?.setValue(ele.customer_ship_to);
              this.dispatchNoteForm.get('customer_bill_to')?.disable();
              this.dispatchNoteForm.get('customer_ship_to')?.disable();
            },
            error: (e) => {
              this.showSwalmessage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                '',
                'error',
                false
              );
            },
          });

        ele.order_details.forEach((ele1: any, ind: number) => {
          this.salesService
            .getAvailableQty(ele1.item_ref_id, ele.so_type_ref_name, ele.id)
            .subscribe({
              next: (res: any) => {
                const gridRow = (<FormArray>(
                  this.dispatchNoteForm.get('dispatch_note_details')
                )).at(ind);
                gridRow
                  .get('available_stock_qty')
                  ?.setValue(parseFloat(res).toFixed(4));

                this.salesService.getTaxRate(ele1.hsn_sac_no).subscribe({
                  next: (res1: any) => {
                    console.log(res1);
                    gridRow.get('tax_rate')?.setValue(res1);
                    const cnt = data.dispatch_note_details.length;
                    console.log('cnt:---------- ', cnt);
                    if (cnt > 1) {
                      this.tax_rate = 18;
                      this.dispatchNoteForm
                        .get('txn_currency_tax_rate')
                        ?.setValue(this.tax_rate);
                      this.dispatchNoteForm
                        .get('base_currency_tax_rate')
                        ?.setValue(parseFloat(this.tax_rate).toFixed(2));
                    } else {
                      this.tax_rate = res1;
                      this.dispatchNoteForm
                        .get('txn_currency_tax_rate')
                        ?.setValue(this.tax_rate);
                      this.dispatchNoteForm
                        .get('base_currency_tax_rate')
                        ?.setValue(parseFloat(this.tax_rate).toFixed(2));
                    }
                    this.dispatchNoteForm.disable();
                  },
                  error: (e) => {
                    this.showSwalmessage(
                      this.errorCodes.getErrorMessage(JSON.parse(e).status),
                      '',
                      'error',
                      false
                    );
                  },
                });
              },
              error: (e) => {
                this.showSwalmessage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  '',
                  'error',
                  false
                );
              },
            });
        });
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.dispatchNoteForm.setControl(
      'dispatch_note_details',
      this.setExistingArray(data.dispatch_note_details)
    );

    if (this.submitBtn) {
      this.btnVal = 'Update';
      this.viewBtn = false;
    } else {
      this.dispatchNoteForm.disable();
    }
  }

  openAlert(event: any) {
    this.cnt = this.cnt + 1;
    if (this.cnt == 1) {
      if (event.target.id == 'SendBack') {
        this.dispatchNoteForm.controls['remark']?.addValidators([
          Validators.required,
        ]);
        this.dispatchNoteForm.controls['remark'].updateValueAndValidity();
      }
      this.onSubmit(event.target.id);
    }
  }

  ngAfterContentChecked() {
    if (this.elementRef.nativeElement.querySelector('#myTemplate')) {
      this.showLoader = true;
      this.elementRef.nativeElement
        .querySelector('#myTemplate')
        .addEventListener('click', this.openAlert.bind(this));
    }
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, ind: number) => {
      console.log(element);
      console.log(element.dispatch_no_of_bags);
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,
          hsn_sac_no: element.hsn_sac_no,
          uom: element.uom,
          order_qty: element.order_qty,
          available_stock_qty: element.available_stock_qty,
          dispatch_qty: element.dispatch_qty,
          dispatch_no_of_bags: Number(element.dispatch_no_of_bags),
          rate: parseFloat(element.rate).toFixed(2),
          txn_currency: element.txn_currency,
          base_currency: element.base_currency,
          conversion_rate: element.conversion_rate,
          txn_currency_amount: parseFloat(element.txn_currency_amount).toFixed(
            2
          ),
          tax_rate: parseFloat(element.tax_rate).toFixed(2),
          txn_currency_igst_rate: element.txn_currency_igst_rate,
          txn_currency_igst_amount: element.txn_currency_igst_amount,
          txn_currency_cgst_rate: element.txn_currency_cgst_rate,
          txn_currency_cgst_amount: element.txn_currency_cgst_amount,
          txn_currency_sgst_rate: element.txn_currency_sgst_rate,
          txn_currency_sgst_amount: element.txn_currency_sgst_amount,
          txn_currency_tax_amount: parseFloat(
            element.txn_currency_tax_amount
          ).toFixed(2),
          txn_currency_total_amount: parseFloat(
            element.txn_currency_total_amount
          ).toFixed(2),
          base_currency_amount: parseFloat(
            element.base_currency_amount
          ).toFixed(2),
          base_currency_igst_rate: element.base_currency_igst_rate,
          base_currency_igst_amount: element.base_currency_igst_amount,
          base_currency_cgst_rate: element.base_currency_cgst_rate,
          base_currency_cgst_amount: element.base_currency_cgst_amount,
          base_currency_sgst_rate: element.base_currency_sgst_rate,
          base_currency_sgst_amount: element.base_currency_sgst_amount,
          base_currency_tax_amount: parseFloat(
            element.base_currency_tax_amount
          ).toFixed(2),
          base_currency_total_amount: parseFloat(
            element.base_currency_total_amount
          ).toFixed(2),
          is_active: element.is_active,
          is_deleted: element.is_deleted,
        })
      );
    });
    console.log(formArray);

    return formArray;
  }

  setExistingArray1(initialArray = [], index: number): UntypedFormArray {
    // const formArray: any = [];
    const formArr: any = new UntypedFormArray([]);
    let agreed_amount = 0;
    let agreed_rate = 0;
    let amount = 0;
    let qc_rate = 0;
    let quantity = 0;
    let base_rate = 0;
    const arr: any[] = [];
    const arr1: any[] = [];

    initialArray.forEach((res: any, ind: number) => {
      console.log(res);
      // const formArr: any = new UntypedFormArray([]);
      // element.forEach((res: any) => {
      const actual_value: any[] = [];
      const json_obj = JSON.parse(res.jsonb_build_object);
      json_obj.actual_value.forEach((x: any) => {
        actual_value.push(parseFloat(x).toFixed(2));
      });

      formArr.push(
        this.formBuilder.group({
          bill_date: res.transaction_date,
          farmer_bill_no: res.transaction_ref_no,
          item_ref_id: res.item_ref_id,
          uom: res.uom,
          no_of_bags: res.no_of_bags,
          quantity: res.net_quantity.toFixed(4),
          qc_rate: res.rate.toFixed(2),
          amount: (res.net_quantity * res.rate).toFixed(2),
          agreed_rate: res.net_rate.toFixed(2),
          agreed_amount: (res.net_quantity * res.net_rate).toFixed(2),
          item_name: res.item_name,
          uom_name: res.uom_name,
          actual_value: [actual_value],
          farmer_name: res.farmer_name,
          base_rate: res.base_rate.toFixed(2),
        })
      );
      console.log(res.avg_actual_value);
      json_obj.parameter_name.forEach((ele: any, i: number) => {
        if (!arr.includes(ele)) {
          arr.push(ele);
          const val = (res.avg_actual_value[i] / initialArray.length).toFixed(
            2
          );
          arr1.push(val);
        }
      });

      console.log(res.avg_actual_value);
      quantity = quantity + parseFloat(res.net_quantity);
      base_rate = base_rate + res.base_rate;
      qc_rate = qc_rate + res.rate;
      amount = amount + res.net_quantity * res.rate;
      agreed_rate = agreed_rate + res.net_rate;
      agreed_amount = agreed_amount + res.net_quantity * res.net_rate;
      // })
      // formArray.push(formArr);
    });
    console.log(formArr);
    this.parameterData[index] = arr;
    this.avgActualValue[index] = arr1;
    this.quantity[index] = quantity.toFixed(4);
    this.base_rate[index] = (base_rate / initialArray.length).toFixed(2);
    this.qc_rate[index] = (qc_rate / initialArray.length).toFixed(2);
    this.amount[index] = amount.toFixed(2);
    this.agreed_rate[index] = (agreed_rate / initialArray.length).toFixed(2);
    this.agreed_amount[index] = agreed_amount.toFixed(2);

    this.filteredData[index] = formArr.value;
    console.log(this.filteredData);
    this.farmerAllocated[index] = formArr;

    return formArr;
  }

  get formArr() {
    return this.dispatchNoteForm.get(
      'dispatch_note_details'
    ) as UntypedFormArray;
  }

  get formArray() {
    return this.dispatchNoteForm.get('farmer_bill') as UntypedFormArray;
  }

  addNewRow() {
    this.formArr.push(this.dispatch_note_details());
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArr.removeAt(index);
      return true;
    }
  }

  onCancelForm() {
    this.dispatchNoteForm.reset();
    this.handleCancel.emit(false);
  }

  // onResetForm() {
  //   // this.dispatchNoteForm.reset();

  //   this.initializeForm();
  // }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onSubmit(btn: any) {
    this.showLoader = true;
    console.log(this.dispatchNoteForm);
    console.log('aaaaaaaaaaaaa', this.formArr.controls[0]);

    const qc_param_details_exists =
      this.dispatchNoteForm.value.dispatch_note_details.filter(
        (x: any) => x.qc_parameter_details == null
      );
    console.log(this.isQCRequired, qc_param_details_exists);

    if (qc_param_details_exists.length > 0 && this.isQCRequired) {
      if (qc_param_details_exists[0].item_ref_id != '') {
        this.showSwalmessage(
          'Please fill Quality Parameter for ' +
            qc_param_details_exists[0].item_ref_id,
          '',
          'error',
          false
        );
        this.showLoader = false;
      }
    } else {
      if (this.dispatchNoteForm.invalid) {
        this.showLoader = false;

        if (!this.isQCRequired) {
          const available_stock_qty =
            this.dispatchNoteForm.value.dispatch_note_details.filter(
              (x: any) => x.available_stock_qty == ''
            );
          if (available_stock_qty.length > 0) {
            this.showSwalmessage('Stock Is Not Available', '', 'error', false);
          } else {
            this.showSwalmessage(
              'Please fill all required Fields',
              '',
              'error',
              false
            );
          }
        }

        for (let index = 0; index < this.formArr.controls.length; index++) {
          const newForm = this.formArr.controls[index] as FormArray;
          for (const item in newForm.controls) {
            if (newForm.controls[item].status == 'INVALID') {
              newForm.controls[item].markAsTouched();
            }
          }
        }

        const dispatch = this.dispatchNoteForm.controls;
        for (const name in dispatch) {
          if (dispatch[name].invalid) {
            dispatch[name].markAsTouched();
            console.log('-----[name]: ', dispatch[name]);
          }
        }
      } else {
        const payload = this.dispatchNoteForm.getRawValue();
        const val = this.datepipe.transform(
          payload.transaction_date,
          'yyyy-MM-dd'
        );
        payload.transaction_date = val;
        payload.txn_currency = this.txnCurrency[0]['id'];
        payload.base_currency = this.txnCurrency[0]['id'];

        const gridRow = payload.dispatch_note_details;
        for (let j = 0; j < gridRow.length; j++) {
          gridRow[j].txn_currency = this.txnCurrency[0]['id'];
          gridRow[j].base_currency = this.txnCurrency[0]['id'];
          if (this.qc_params.length > 0) {
            gridRow[j].qc_parameter_details = this.qc_params[j];
          } else {
            payload.workflow_status = 'Accepted';
            payload.status = 'Open';
          }
        }

        delete payload.farmer_bill;
        console.log(payload);

        this.handleSave.emit(payload);
      }
    }
  }

  exportExcel(ind: any) {
    // key name with space add in brackets

    // this.filteredData[val].forEach((element: any, ind: number) => {
    console.log(this.filteredData);
    const exportData: Partial<TableElement>[] = this.filteredData[ind].map(
      (x: any) => ({
        'Bill Date': x.bill_date,
        'Bill Number': x.farmer_bill_no,
        'Farmer Name': x.farmer_name,
        'Item Name': x.item_name,
        'UOM Name': x.uom_name,
        'Number Of Bags': x.no_of_bags,
        Quantity: x.quantity,
        'Base Rate': x.base_rate,
        'QC Rate': x.qc_rate,
        Amount: x.amount,
        'Final Rate': x.agreed_rate,
        'Final Amount': x.agreed_amount,
      })
    );
    exportData.push({
      'Bill Date': '',
      'Bill Number': '',
      'Farmer Name': '',
      'Item Name': '',
      'UOM Name': '',
      'Number Of Bags': '',
      Quantity: this.quantity[ind],
      'Base Rate': this.base_rate[ind],
      'QC Rate': this.qc_rate[ind],
      Amount: this.amount[ind],
      'Final Rate': this.agreed_rate[ind],
      'Final Amount': this.agreed_amount[ind],
    });
    console.log(exportData);
    console.log(this.filteredData[ind]);
    exportData.forEach((ele: any, index: number) => {
      if (index < this.filteredData[ind].length) {
        console.log(index, this.filteredData[ind][index]);
        console.log(this.parameterData);
        this.parameterData[ind].forEach((elem: any, j: number) => {
          ele[elem] = this.filteredData[ind][index].actual_value[j];
        });
      } else {
        console.log(this.avgActualValue);
        this.parameterData[ind].forEach((elem: any, i: number) => {
          this.avgActualValue[ind].forEach((element: any, j: number) => {
            console.log(element, elem);
            if (i == j) {
              ele[elem] = element;
            }
          });
        });
      }
    });
    console.log(exportData);
    TableExportUtil.exportToExcel(
      exportData,
      this.filteredData[ind][0].item_name
    );
    // });

    // this.parameterData.forEach((ele: any, ind: number) => {
    //   exportData.forEach((element: any, index: number) => {
    //     element[ele] = this.filteredData[index].actual_value[ind];
    //   })
    // })
  }
}
