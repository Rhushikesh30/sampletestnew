import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchNoteDetailsComponent } from './dispatch-note-details.component';

describe('DispatchNoteDetailsComponent', () => {
  let component: DispatchNoteDetailsComponent;
  let fixture: ComponentFixture<DispatchNoteDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispatchNoteDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DispatchNoteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
