import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QcParameterDialogComponent } from './qc-parameter-dialog.component';

describe('QcParameterDialogComponent', () => {
  let component: QcParameterDialogComponent;
  let fixture: ComponentFixture<QcParameterDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QcParameterDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QcParameterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
