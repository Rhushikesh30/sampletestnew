import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormArray } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { UntypedFormArray, UntypedFormGroup, Validators } from '@angular/forms';

import { QcService } from 'src/app/shared/services/qc.service';

import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { SalesService } from 'src/app/shared/services/sales.service';
export interface DialogData {
  data: any;
}
@Component({
  selector: 'app-qc-parameter-dialog',
  templateUrl: './qc-parameter-dialog.component.html',
  styleUrls: ['./qc-parameter-dialog.component.scss'],
  providers: [ErrorCodes],
})
export class QcParameterDialogComponent implements OnInit {
  constructor(
    private salesService: SalesService,
    private dialogRef: MatDialogRef<QcParameterDialogComponent>,
    private dialog: MatDialog,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private errorCodes: ErrorCodes
  ) {}

  qcParameterdetails!: UntypedFormGroup;
  qualityData: any = [];
  submitBtn = this.data.value['submitBtn'];
  showLoader = false;

  ngOnInit(): void {
    this.intializeForm();

    if (this.data.value['qc_parameter_details']) {
      this.getqctype(this.data.value['item_ref_id']);
      this.viewRecord(this.data.value);
    } else {
      this.salesService
        .getQualityParameterInSO(
          this.data.value['item_ref_id'],
          this.data.value['so_ref_id']
        )
        .subscribe({
          next: (data: any) => {
            this.qualityData = data;
            if (this.qualityData.length >= 1) {
              this.qcParameterdetails.setControl(
                'qc_parameter_details',
                this.setExistingArray(this.qualityData)
              );
            }
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
    }
  }

  intializeForm() {
    this.qcParameterdetails = this.fb.group({
      id: [''],
      qc_parameter_details: this.fb.array([this.qc_parameter_details()]),
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  qc_parameter_details() {
    return this.fb.group({
      id: [''],
      quality_parameter_ref_id: [{ value: '', disabled: true }],
      actual_value: [
        '',
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$'),
        ],
      ],
    });
  }

  get formArrQcp() {
    return this.qcParameterdetails.get(
      'qc_parameter_details'
    ) as UntypedFormArray;
  }

  addNewRow() {
    this.formArrQcp.push(this.qc_parameter_details());
  }

  setExistingArray(initialArray: any): UntypedFormArray {
    console.log(this.qualityData);
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      console.log(element);
      formArray.push(
        this.fb.group({
          id: element.id,
          quality_parameter_ref_id: {
            value: element.item_quality_parameter_ref_id,
            disabled: true,
          },
          actual_value: element.actual_value,
        })
      );
    });
    return formArray;
  }

  viewRecord(edata: any) {
    const qcDetailItemRow = edata['qc_parameter_details'].filter(function (
      data: any
    ) {
      return data;
    });
    if (qcDetailItemRow.length >= 1) {
      qcDetailItemRow.forEach((ele: any, ind: any) => {
        qcDetailItemRow[ind].item_quality_parameter_ref_id =
          ele.quality_parameter_ref_id;
      });
      this.qcParameterdetails.setControl(
        'qc_parameter_details',
        this.setExistingArray(qcDetailItemRow)
      );
    }
    if (this.submitBtn == false) {
      this.qcParameterdetails.disable();
    }
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrQcp.removeAt(index);
      return true;
    }
  }

  onCancelForm() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.showLoader = true;
    if (this.qcParameterdetails.invalid) {
      this.showLoader = false;
      return;
    } else {
      const qc_parameter_details = this.qcParameterdetails
        .get('qc_parameter_details')
        ?.getRawValue();
      const payload = {
        item_ref_id: this.data.value['item_ref_id'],
        qc_parameter_details: qc_parameter_details,
      };
      console.log('payload: ', payload);
      this.dialogRef.close(payload);
    }
  }

  qualityParamsChange(event: any, index: any) {
    const val = event.target.value;
    const gridRow = (<FormArray>(
      this.qcParameterdetails.get('qc_parameter_details')
    )).at(index);
    const parameter = gridRow.get('quality_parameter_ref_id')?.value;
    if (parameter == '' || parameter == undefined) {
      this.showSwalMassage('Please Select Quality Parameter', 'warning');
      gridRow.get('actual_value')?.setValue('');
      return;
    }
  }

  getqctype(v: any) {
    this.salesService
      .getQualityParameterInSO(
        this.data.value['item_ref_id'],
        this.data.value['so_ref_id']
      )
      .subscribe({
        next: (data: any) => {
          this.qualityData = data;
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
