import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormArray,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import { FarmerbillService } from 'src/app/shared/services/farmerbill.service';
import { GrnService } from 'src/app/shared/services/grn.service';
import { SalesService } from 'src/app/shared/services/sales.service';
import Swal from 'sweetalert2';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class InvoiceDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;

  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  invoiceForm!: UntypedFormGroup;
  todayDate = new Date().toJSON().split('T')[0];
  btnVal = 'Submit';
  showReset = true;
  viewEdit = false;
  showFileLoader = false;
  txnCurrency: any;
  uomData: any;
  itemData: any;
  tax_rate: any;
  soTypeData: any;
  soData: any = [];
  customerData: any = [];
  CurrencyData: any = [];
  billAddress: any = [];
  shipAddress: any = [];
  filterCustomerData: any = [];
  filterSoData: any = [];
  isTransportationView = false;
  dispatchNoteData: any = [];
  filterDispatchNoteData: any = [];
  agentData: any = [];
  filterAgentData: any = [];
  inv_qty = 0;
  is_commission = false;
  empty_value = '';
  is_regular_invoice = true;
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  is_agent = false;
  comAmount: any = 0;
  txn_currency: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private salesService: SalesService,
    private errorCodes: ErrorCodes,
    private grnService: GrnService,
    private datepipe: DatePipe,
    private farmerBillService: FarmerbillService,
    private dynamicService: DynamicFormService
  ) {}

  ngOnInit() {
    this.initializeForm();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    } else {
      this.defaultData('save');
    }
  }

  defaultData(val: any) {
    this.salesService
      .getCustomerFarmer(
        'get_combine_columns_customer_farmer',
        'Invoice',
        'customer',
        'company_name',
        'ref_type',
        val
      )
      .subscribe({
        next: (data: any) => {
          if (data.length == 0) {
            Swal.fire('No Data Available');
          } else {
            this.customerData = data;
            this.filterCustomerData = this.customerData.slice();
          }
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.grnService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.txnCurrency = data.filter((d: any) => d.country_name == 'India');
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
    this.grnService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.invoiceForm
          .get('txn_currency')
          ?.setValue(this.txn_currency[0]['id']);
        this.invoiceForm
          .get('base_currency')
          ?.setValue(this.txn_currency[0]['id']);
        const gridRow = (<FormArray>this.invoiceForm.get('invoice_details')).at(
          0
        );
        gridRow.get('txn_currency')?.setValue(this.txn_currency[0]['id']);
        gridRow.get('base_currency')?.setValue(this.txn_currency[0]['id']);
      },
    });

    this.salesService.getDynamicData('UOM').subscribe({
      next: (data: any) => {
        this.uomData = data['screenmatlistingdata_set'];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicData('Item Master').subscribe({
      next: (data: any) => {
        this.itemData = data['screenmatlistingdata_set'];
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  initializeForm() {
    this.invoiceForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', Validators.required],
      transaction_ref_no: [''],
      tenant_id: localStorage.getItem('COMPANY_ID'),
      customer_ref_id: ['', Validators.required],
      customer_ref_type: ['', Validators.required],
      so_ref_id: ['', Validators.required],
      dispatch_note_ref_id: ['', Validators.required],
      agent_ref_id: [''],
      so_type_ref_id: ['', Validators.required],
      customer_bill_to: ['', Validators.required],
      customer_ship_to: ['', Validators.required],
      e_way_bill_no: [''],
      txn_currency: [''],
      base_currency: [''],
      conversion_rate: [1],
      txn_currency_packaging_charges: [0],
      txn_currency_freight_charges: [0],
      txn_currency_insurance_charges: [0],
      txn_currency_sub_total_amount: [0],
      tax_rate: ['', Validators.required],
      txn_currency_tax_amount: [0, Validators.required],
      txn_currency_grand_total_amount: [0, Validators.required],
      txn_currency_total_transaction_amount: [0, Validators.required],
      base_currency_packaging_charges: [0],
      base_currency_freight_charges: [0],
      base_currency_insurance_charges: [0],
      base_currency_sub_total_amount: [0],
      base_currency_tax_amount: [0, Validators.required],
      base_currency_grand_total_amount: [0, Validators.required],
      base_currency_total_transaction_amount: [0, Validators.required],
      agent_rate_percentage: [false],
      agent_commission: [0],
      agent_commission_amount: [0],
      txn_currency_balance_grand_total_amount: [0],
      is_receipt_generated: [false],
      is_active: true,
      is_deleted: false,
      created_by: localStorage.getItem('user_id'),
      invoice_details: this.formBuilder.array([this.invoice_details()]),
      is_regular_invoice: true,
      division_ref_id: '',
      department_ref_id: '',
      sale_type_ref_id: '',
      location_ref_id: '',
      fpc_commision_total_amt: 0,
      fpc_commision_igst_rate: 0,
      fpc_commision_igst_amount: 0,
      fpc_commision_cgst_rate: 0,
      fpc_commision_cgst_amount: 0,
      fpc_commision_sgst_rate: 0,
      fpc_commision_sgst_amount: 0,
      fpc_commision_tax_amount: 0,
      fpc_commision_tax_rate: 0,
      transaction_currency: [''],
    });
  }

  invoice_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      hsn_sac_no: ['', Validators.required],
      uom: ['', Validators.required],
      order_qty: ['', Validators.required],
      dispatch_qty: ['', Validators.required],
      dispatch_no_of_bags: ['', Validators.required],
      rate: ['', Validators.required],
      txn_currency: [''],
      base_currency: [''],
      conversion_rate: [1],
      txn_currency_amount: ['', Validators.required],
      tax_rate: ['', Validators.required],
      txn_currency_igst_rate: [0],
      txn_currency_igst_amount: [0],
      txn_currency_cgst_rate: [0],
      txn_currency_cgst_amount: [0],
      txn_currency_sgst_rate: [0],
      txn_currency_sgst_amount: [0],
      txn_currency_tax_amount: [0],
      txn_currency_transaction_amount: [0],
      base_currency_amount: [0],
      base_currency_igst_rate: [0],
      base_currency_igst_amount: [0],
      base_currency_cgst_rate: [0],
      base_currency_cgst_amount: [0],
      base_currency_sgst_rate: [0],
      base_currency_sgst_amount: [0],
      base_currency_tax_amount: [0],
      base_currency_transaction_amount: [0],
      is_active: true,
      is_deleted: false,
      tenant_id: localStorage.getItem('COMPANY_ID'),
      created_by: localStorage.getItem('user_id'),
      is_fpc_commission_rate_percentage: false,
      fpc_commision: '',
      fpc_commision_amt: 0,
      invoice_qty: ['', Validators.required],
    });
  }

  customerChange(event: any) {
    this.salesService.getSalesOrder(event, 'Invoice').subscribe({
      next: (data: any) => {
        this.soData = data;
        this.filterSoData = this.soData.slice();
        const masterkey = 'customer_details';
        const parent_id = event;
        this.salesService
          .getBillingShippingAdrress(masterkey, parent_id)
          .subscribe({
            next: (data: any) => {
              this.billAddress = data['Billing'];
              this.shipAddress = data['Shipping'];
            },
            error: (e) => {
              this.showSwalmessage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                '',
                'error',
                false
              );
            },
          });
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  soChange(event: any) {
    const delivery_terms_ref_id = this.soData.filter((x: any) => x.id == event);

    console.log(delivery_terms_ref_id);

    this.salesService.getDisaptchFromSO(event).subscribe({
      next: (res: any) => {
        this.dispatchNoteData = res;
        this.filterDispatchNoteData = this.dispatchNoteData.slice();
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('agent', 'agent_name').subscribe({
      next: (data: any) => {
        this.agentData = data;
        this.filterAgentData = this.agentData.slice();
      },
    });

    this.soData.forEach((ele: any) => {
      if (ele.id == event) {
        this.farmerBillService.getAllMasterData('Delivery Terms').subscribe({
          next: (edata: any) => {
            const delivery_terms_ref_id = edata.filter(
              (x: any) => x.id == ele.delivery_terms_ref_id
            );
            if (delivery_terms_ref_id[0].master_value == 'Spot') {
              this.isTransportationView = true;
            }
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });

        this.farmerBillService.getAllMasterData('SO Type').subscribe({
          next: (data: any) => {
            this.soTypeData = data;

            const so_type = this.soTypeData.filter(
              (x: any) => x.id == ele.so_type_ref_id
            );
            if (
              so_type[0].master_value == 'Commision' &&
              this.invoiceForm.get('is_regular_invoice')?.value
            ) {
              this.is_commission = true;
            }

            this.invoiceForm.get('agent_ref_id')?.patchValue(ele.agent_ref_id);
            if (ele.agent_ref_id != 0) {
              this.is_agent = true;
              console.log(ele);
              this.invoiceForm
                .get('agent_rate_percentage')
                ?.setValue(ele.agent_rate_percentage);
              this.invoiceForm
                .get('agent_commission')
                ?.setValue(parseFloat(ele.agent_commission).toFixed(2));
              this.invoiceForm
                .get('agent_commission_amount')
                ?.setValue(parseFloat(ele.agent_commission_amount).toFixed(2));
            }
            this.invoiceForm
              .get('so_type_ref_id')
              ?.patchValue(ele.so_type_ref_id);
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
      }
    });
  }

  dispatchNoteChange(event: any) {
    const ele = this.dispatchNoteData.filter((x: any) => x.id == event)[0];
    const order_details = this.soData.filter(
      (x: any) => x.id == ele.so_ref_id
    )[0].order_details;

    this.invoiceForm
      .get('txn_currency_grand_total_amount')
      ?.patchValue(ele.txn_currency_grand_total_amount);
    this.invoiceForm
      .get('txn_currency_packaging_charges')
      ?.patchValue(ele.txn_currency_packaging_charges);
    this.invoiceForm
      .get('txn_currency_insurance_charges')
      ?.patchValue(ele.txn_currency_insurance_charges);
    this.invoiceForm
      .get('txn_currency_freight_charges')
      ?.patchValue(ele.txn_currency_transportation_charges);
    this.invoiceForm
      .get('txn_currency_sub_total_amount')
      ?.patchValue(ele.txn_currency_sub_total_amount);
    this.invoiceForm
      .get('tax_rate')
      ?.patchValue(parseFloat(ele.txn_currency_tax_rate).toFixed(2));
    this.invoiceForm
      .get('txn_currency_tax_amount')
      ?.patchValue(ele.txn_currency_tax_amount);
    this.invoiceForm.get('e_way_bill_no')?.patchValue(ele.e_way_bill_no);
    this.invoiceForm.get('customer_bill_to')?.patchValue(ele.customer_bill_to);
    this.invoiceForm.get('customer_ship_to')?.patchValue(ele.customer_ship_to);

    this.invoiceForm
      .get('base_currency_grand_total_amount')
      ?.patchValue(ele.base_currency_grand_total_amount);
    this.invoiceForm
      .get('base_currency_packaging_charges')
      ?.patchValue(ele.base_currency_packaging_charges);
    this.invoiceForm
      .get('base_currency_insurance_charges')
      ?.patchValue(ele.base_currency_insurance_charges);
    this.invoiceForm
      .get('base_currency_freight_charges')
      ?.patchValue(
        parseFloat(ele.txn_currency_transportation_charges).toFixed(2)
      );
    this.invoiceForm
      .get('base_currency_sub_total_amount')
      ?.patchValue(ele.base_currency_sub_total_amount);
    this.invoiceForm
      .get('base_currency_tax_rate')
      ?.patchValue(ele.base_currency_tax_rate);
    this.invoiceForm
      .get('base_currency_tax_amount')
      ?.patchValue(ele.base_currency_tax_amount);

    this.invoiceForm
      .get('customer_ref_type')
      ?.patchValue(ele.customer_ref_type);
    this.invoiceForm.get('division_ref_id')?.patchValue(ele.division_ref_id);
    this.invoiceForm
      .get('department_ref_id')
      ?.patchValue(ele.department_ref_id);
    this.invoiceForm.get('sale_type_ref_id')?.patchValue(ele.sale_type_ref_id);
    this.invoiceForm.get('location_ref_id')?.patchValue(ele.location_ref_id);

    this.invoiceForm.setControl(
      'invoice_details',
      this.setArray(ele.dispatch_note_details)
    );

    this.agentCommission();

    let qty = 0;
    order_details.sort((a: any, b: any) =>
      a['id'] > b['id'] ? 1 : a['id'] === b['id'] ? 0 : -1
    );
    ele.dispatch_note_details.forEach((ele: any, ind: any) => {
      const tax_rate =
        parseFloat(ele.txn_currency_igst_rate) +
        parseFloat(ele.txn_currency_cgst_rate) +
        parseFloat(ele.txn_currency_sgst_rate);
      const transaction_amount =
        parseFloat(ele.txn_currency_amount) +
        parseFloat(((ele.txn_currency_amount * tax_rate) / 100).toString());
      qty = qty + ele.dispatch_qty;
      this.calculateAmoounts(
        ind,
        this.invoiceForm.get('invoice_details'),
        order_details,
        ele,
        transaction_amount,
        qty
      );
    });
  }

  agentChange(event: any) {
    const agent_name = this.agentData.filter((x: any) => x.id == event)[0][
      'key'
    ];
    this.dynamicService.getDynamicScreenData('Agent').subscribe({
      next: (res: any) => {
        res['screenmatlistingdata_set'].forEach((data: any) => {
          if (data.agent_name == agent_name) {
            this.invoiceForm
              .get('agent_rate_percentage')
              ?.setValue(data.is_rate_percentage);
            this.invoiceForm.get('agent_commission')?.setValue(data.commission);

            this.agentCal();
          }
        });
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  agentCal() {
    const amt = this.invoiceForm.get(
      'txn_currency_total_transaction_amount'
    )?.value;
    const com = this.invoiceForm.get('agent_commission')?.value;
    const per = this.invoiceForm.get('agent_rate_percentage')?.value;

    if (per) {
      const total_amt = (amt * com) / 100;
      this.invoiceForm.get('agent_commission_amount')?.setValue(total_amt);
    } else {
      const total_amt = this.inv_qty * com;
      this.invoiceForm.get('agent_commission_amount')?.setValue(total_amt);
    }

    this.invoiceForm.controls['so_ref_id'].enable();
    this.invoiceForm.controls['customer_ref_id'].enable();
    this.invoiceForm.controls['dispatch_note_ref_id'].enable();
    this.invoiceForm.controls['transaction_date'].enable();
    this.invoiceForm.get('txn_currency_packaging_charges')?.enable();
    this.invoiceForm.get('txn_currency_insurance_charges')?.enable();
    this.invoiceForm.get('txn_currency_freight_charges')?.enable();
    this.invoiceForm.get('txn_currency_sub_total_amount')?.enable();
  }

  amountCal(i: number) {
    const gridRow = (<FormArray>this.invoiceForm.get('invoice_details')).at(i);
    const tax_amt = gridRow.get('txn_currency_tax_amount')?.value;
    const fpc_com = gridRow.get('fpc_commision_amt')?.value;
    const txn_amt = gridRow.get('txn_currency_amount')?.value;

    const tax_amt1 = gridRow.get('base_currency_tax_amount')?.value;
    const txn_amt1 = gridRow.get('base_currency_amount')?.value;

    const txn_total_amt =
      parseFloat(tax_amt) + parseFloat(fpc_com) + parseFloat(txn_amt);
    const txn_total_amt1 =
      parseFloat(tax_amt1) + parseFloat(fpc_com) + parseFloat(txn_amt1);

    if (this.is_commission) {
      console.log(fpc_com);
      gridRow
        .get('txn_currency_transaction_amount')
        ?.setValue(parseFloat(fpc_com.toString()).toFixed(2));
      gridRow
        .get('base_currency_transaction_amount')
        ?.setValue(parseFloat(fpc_com.toString()).toFixed(2));
    } else {
      gridRow
        .get('txn_currency_transaction_amount')
        ?.setValue(parseFloat(txn_total_amt.toString()).toFixed(2));
      gridRow
        .get('base_currency_transaction_amount')
        ?.setValue(parseFloat(txn_total_amt1.toString()).toFixed(2));
    }
  }

  viewRecord(data: any) {
    this.viewEdit = true;
    this.is_regular_invoice = data.is_regular_invoice;
    this.defaultData('view');
    this.customerChange(data.customer_ref_id);

    this.salesService.getAllDispatchData().subscribe({
      next: (res: any) => {
        this.dispatchNoteData = res;
        this.filterDispatchNoteData = this.dispatchNoteData.slice();
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    this.salesService.getDynamicDataById('agent', 'agent_name').subscribe({
      next: (data: any) => {
        this.agentData = data;
        this.filterAgentData = this.agentData.slice();
      },
    });

    this.tax_rate =
      parseFloat(data.txn_currency_igst_rate) +
      parseFloat(data.txn_currency_cgst_rate) +
      parseFloat(data.txn_currency_sgst_rate);
    this.invoiceForm.patchValue({
      id: data.id,
      transaction_date: data.transaction_date,
      transaction_ref_no: data.transaction_ref_no,
      tenant_id: data.tenant_id,
      customer_ref_id: data.customer_ref_id,
      customer_ref_type: data.customer_ref_type,
      so_ref_id: data.so_ref_id,
      dispatch_note_ref_id: data.dispatch_note_ref_id,
      so_type_ref_id: data.so_type_ref_id,
      customer_bill_to: data.customer_bill_to,
      customer_ship_to: data.customer_ship_to,
      e_way_bill_no: data.e_way_bill_no,
      transaction_currency: data.transaction_currency,
      txn_currency: data.txn_currency,
      base_currency: data.base_currency,
      conversion_rate: data.conversion_rate,
      agent_ref_id: data.agent_ref_id,
      txn_currency_packaging_charges: parseFloat(
        data.txn_currency_packaging_charges
      ).toFixed(2),
      txn_currency_freight_charges: parseFloat(
        data.txn_currency_freight_charges
      ).toFixed(2),
      txn_currency_insurance_charges: parseFloat(
        data.txn_currency_insurance_charges
      ).toFixed(2),
      txn_currency_sub_total_amount: parseFloat(
        data.txn_currency_sub_total_amount
      ).toFixed(2),
      tax_rate: this.tax_rate.toFixed(2),
      txn_currency_tax_amount: parseFloat(data.txn_currency_tax_amount).toFixed(
        2
      ),
      txn_currency_grand_total_amount: parseFloat(
        data.txn_currency_grand_total_amount
      ).toFixed(2),
      txn_currency_total_transaction_amount: parseFloat(
        data.txn_currency_total_transaction_amount
      ).toFixed(2),
      base_currency_packaging_charges: data.base_currency_packaging_charges,
      base_currency_freight_charges: data.base_currency_freight_charges,
      base_currency_insurance_charges: data.base_currency_insurance_charges,
      base_currency_sub_total_amount: data.base_currency_sub_total_amount,
      base_currency_tax_rate: data.base_currency_tax_rate,
      base_currency_tax_amount: data.base_currency_tax_amount,
      base_currency_grand_total_amount: data.base_currency_grand_total_amount,
      base_currency_total_transaction_amount:
        data.base_currency_total_transaction_amount,
      agent_rate_percentage: data.agent_rate_percentage,
      agent_commission: parseFloat(data.agent_commission).toFixed(2),
      agent_commission_amount: parseFloat(data.agent_commission_amount).toFixed(
        2
      ),
      txn_currency_balance_grand_total_amount:
        data.txn_currency_balance_grand_total_amount,
      is_receipt_generated: data.is_receipt_generated,
      is_active: data.is_active,
      is_deleted: data.is_deleted,
      is_regular_invoice: data.is_regular_invoice,
      location_ref_id: data.location_ref_id,
      division_ref_id: data.division_ref_id,
      department_ref_id: data.department_ref_id,
      sale_type_ref_id: data.sale_type_ref_id,
      fpc_commision_igst_rate: data.fpc_commision_igst_rate,
      fpc_commision_igst_amount: data.fpc_commision_igst_amount,
      fpc_commision_cgst_rate: data.fpc_commision_cgst_rate,
      fpc_commision_cgst_amount: data.fpc_commision_cgst_amount,
      fpc_commision_sgst_rate: data.fpc_commision_sgst_rate,
      fpc_commision_sgst_amount: data.fpc_commision_sgst_amount,
      fpc_commision_tax_amount: (
        parseFloat(data.fpc_commision_igst_amount) +
        parseFloat(data.fpc_commision_cgst_amount) +
        parseFloat(data.fpc_commision_sgst_amount)
      ).toFixed(2),
      fpc_commision_tax_rate:
        parseFloat(data.fpc_commision_igst_rate) +
        parseFloat(data.fpc_commision_cgst_rate) +
        parseFloat(data.fpc_commision_sgst_rate),
      fpc_commision_total_amt: parseFloat(data.fpc_commision_total_amt).toFixed(
        2
      ),
    });

    if (data.agent_ref_id != 0) {
      this.is_agent = true;
    }

    this.farmerBillService.getAllMasterData('SO Type').subscribe({
      next: (edata: any) => {
        this.soTypeData = edata;

        this.farmerBillService.getAllMasterData('Delivery Terms').subscribe({
          next: (rdata: any) => {
            const soData = this.soData.filter(
              (x: any) => x.id == data.so_ref_id
            );
            console.log(this.soData);
            console.log(soData);
            const delivery_terms_ref_id = rdata.filter(
              (x: any) => x.id == soData[0].delivery_terms_ref_id
            );
            if (delivery_terms_ref_id[0].master_value == 'Spot') {
              this.isTransportationView = true;
            }
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });

        const so_type = this.soTypeData.filter(
          (x: any) => x.id == data.so_type_ref_id
        );
        if (so_type[0].master_value == 'Commision' && data.is_regular_invoice) {
          this.is_commission = true;
        }
      },
      error: (e) => {
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });

    // this.dispatchNoteChange(data.dispatch_note_ref_id);

    this.invoiceForm.setControl(
      'invoice_details',
      this.setExistingArray(data.invoice_details)
    );

    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.invoiceForm.disable();
    }
  }

  setArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    let qty = 0;
    initialArray.forEach((element: any) => {
      console.log(element)
      this.inv_qty = this.inv_qty + element.dispatch_qty;
      const tax_rate =
        parseFloat(element.txn_currency_igst_rate) +
        parseFloat(element.txn_currency_cgst_rate) +
        parseFloat(element.txn_currency_sgst_rate);
      // this.salesService.getTaxRate(element.item_ref_id).subscribe({
      //   next: (res: any) => {
      const transaction_amount =
        parseFloat(element.txn_currency_amount) +
        parseFloat(((element.txn_currency_amount * tax_rate) / 100).toString());
      qty = qty + element.dispatch_qty;
      formArray.push(
        this.formBuilder.group({
          item_ref_id: element.item_ref_id,
          hsn_sac_no: element.hsn_sac_no,
          uom: element.uom,
          order_qty: element.order_qty,
          dispatch_qty: element.dispatch_qty,
          invoice_qty: element.dispatch_qty,
          dispatch_no_of_bags: Number(element.dispatch_no_of_bags),
          rate: parseFloat(element.rate).toFixed(2),
          txn_currency: element.txn_currency,
          base_currency: element.base_currency,
          conversion_rate: element.conversion_rate,
          is_fpc_commission_rate_percentage: false,
          fpc_commision: '',
          fpc_commision_amt: 0,
          txn_currency_amount: parseFloat(element.txn_currency_amount).toFixed(
            2
          ),
          txn_currency_igst_rate: parseFloat(
            element.txn_currency_igst_rate
          ).toFixed(2),
          txn_currency_igst_amount: parseFloat(
            element.txn_currency_igst_amount
          ).toFixed(2),
          txn_currency_cgst_rate: parseFloat(
            element.txn_currency_cgst_rate
          ).toFixed(2),
          txn_currency_cgst_amount: parseFloat(
            element.txn_currency_cgst_amount
          ).toFixed(2),
          txn_currency_sgst_rate: parseFloat(
            element.txn_currency_sgst_rate
          ).toFixed(2),
          txn_currency_sgst_amount: parseFloat(
            element.txn_currency_sgst_amount
          ).toFixed(2),
          txn_currency_transaction_amount: transaction_amount.toFixed(2),
          tax_rate: tax_rate,
          txn_currency_tax_amount: parseFloat(
            ((tax_rate * element.dispatch_qty * element.rate) / 100).toString()
          ).toFixed(2),
          base_currency_amount: element.base_currency_amount,
          base_currency_igst_rate: element.base_currency_igst_rate,
          base_currency_igst_amount: element.base_currency_igst_amount,
          base_currency_cgst_rate: element.base_currency_cgst_rate,
          base_currency_cgst_amount: element.base_currency_cgst_amount,
          base_currency_sgst_rate: element.base_currency_sgst_rate,
          base_currency_sgst_amount: element.base_currency_sgst_amount,
          base_currency_tax_amount: parseFloat(
            ((tax_rate * element.dispatch_qty * element.rate) / 100).toString()
          ).toFixed(2),
          base_currency_transaction_amount: (
            parseFloat(element.base_currency_amount) +
            parseFloat(
              ((element.base_currency_amount * tax_rate) / 100).toString()
            )
          ).toFixed(2),
        })
      );
    });

    const agent = this.invoiceForm.get('agent_ref_id')?.value;
    this.invoiceForm.disable();

    if (agent != 0) {
      this.invoiceForm.get('agent_rate_percentage')?.enable();
      this.invoiceForm.get('agent_commission')?.enable();
      this.invoiceForm.get('agent_commission_amount')?.enable();
    }

    return formArray;
  }

  calculateAmoounts(
    ind: any,
    formArray: any,
    order_details: any,
    element: any,
    transaction_amount: any,
    qty: any
  ) {
    this.amountCal(ind);
    console.log(formArray.getRawValue());
    console.log(order_details);
    let trn_amount = 0;
    const gridRow = (<FormArray>this.invoiceForm.get('invoice_details')).at(
      ind
    );
    gridRow.get('item_ref_id')?.disable();
    gridRow.get('uom')?.disable();

    if (this.is_commission) {
      let total_com_amt = parseFloat('0');
      // formArray.getRawValue().forEach((ele: any, i: number) => {
      order_details.forEach((e: any) => {
        if (e.item_ref_id == element.item_ref_id) {
          // this.getFpcCommission(ind, e, element);
          formArray
            .at(ind)
            .get('is_fpc_commission_rate_percentage')
            .setValue(e.is_fpc_commission_rate_percentage);
          formArray.at(ind).get('fpc_commision').setValue(e.fpc_commision);

          if (e.is_fpc_commission_rate_percentage) {
            const amount =
              (parseFloat(element.txn_currency_amount) * e.fpc_commision) / 100;
            formArray
              .at(ind)
              .get('fpc_commision_amt')
              .setValue(amount.toFixed(2));
            formArray
              .at(ind)
              .get('txn_currency_transaction_amount')
              .setValue(amount.toFixed(2));
            transaction_amount = amount;
          } else {
            const amount = parseFloat(element.dispatch_qty) * e.fpc_commision;
            formArray
              .at(ind)
              .get('fpc_commision_amt')
              .setValue(amount.toFixed(2));
            formArray
              .at(ind)
              .get('txn_currency_transaction_amount')
              .setValue(amount.toFixed(2));
            transaction_amount = amount;
          }

          const com_amt = this.invoiceForm.get(
            'fpc_commision_total_amt'
          )?.value;
          total_com_amt =
            total_com_amt + parseFloat(com_amt) + transaction_amount;
          this.invoiceForm
            .get('fpc_commision_total_amt')
            ?.setValue(total_com_amt.toFixed(2));
          this.salesService
            .getFPCTaxRate(
              this.invoiceForm.get('customer_ref_id')?.value,
              total_com_amt,
              this.invoiceForm.getRawValue().customer_bill_to,
              this.invoiceForm.getRawValue().customer_ship_to
            )
            .subscribe({
              next: (res: any) => {
                this.invoiceForm
                  .get('fpc_commision_igst_rate')
                  ?.setValue(res.igst_rate);
                this.invoiceForm
                  .get('fpc_commision_igst_amount')
                  ?.setValue(res.igst_amount);
                this.invoiceForm
                  .get('fpc_commision_cgst_rate')
                  ?.setValue(res.cgst_rate);
                this.invoiceForm
                  .get('fpc_commision_cgst_amount')
                  ?.setValue(res.cgst_amount);
                this.invoiceForm
                  .get('fpc_commision_sgst_rate')
                  ?.setValue(res.sgst_rate);
                this.invoiceForm
                  .get('fpc_commision_sgst_amount')
                  ?.setValue(res.sgst_amount);
                this.invoiceForm
                  .get('fpc_commision_tax_rate')
                  ?.setValue(res.igst_rate + res.cgst_rate + res.sgst_rate);
                this.invoiceForm
                  .get('fpc_commision_tax_amount')
                  ?.setValue(
                    res.igst_amount + res.cgst_amount + res.sgst_amount
                  );
                const final_amt =
                  total_com_amt +
                  (res.igst_amount + res.cgst_amount + res.sgst_amount);
                this.invoiceForm
                  .get('fpc_commision_total_amt')
                  ?.setValue(final_amt.toFixed(2));

                let amt = 0;
                if (
                  this.invoiceForm.get('txn_currency_total_transaction_amount')
                    ?.value != ''
                ) {
                  amt = this.invoiceForm.get(
                    'txn_currency_total_transaction_amount'
                  )?.value;
                }
                console.log('amt', amt);
                console.log('transaction_amount', transaction_amount);

                amt = parseFloat(amt.toString()) + transaction_amount;
                this.invoiceForm
                  .get('txn_currency_total_transaction_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                this.invoiceForm
                  .get('txn_currency_grand_total_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                this.invoiceForm
                  .get('base_currency_grand_total_amount')
                  ?.setValue(parseFloat(amt.toString()).toFixed(2));
                console.log(
                  this.invoiceForm.get('agent_rate_percentage')?.value
                );
                trn_amount += parseFloat(
                  gridRow.get('txn_currency_amount')?.value
                );
                if (this.invoiceForm.get('agent_rate_percentage')?.value) {
                  this.invoiceForm
                    .get('agent_commission_amount')
                    ?.setValue(
                      (
                        (trn_amount *
                          this.invoiceForm.get('agent_commission')?.value) /
                        100
                      ).toFixed(2)
                    );
                } else {
                  this.invoiceForm
                    .get('agent_commission_amount')
                    ?.setValue(
                      (
                        (qty / 0.001) *
                        this.invoiceForm.get('agent_commission')?.value
                      ).toFixed(2)
                    );
                }

                const amt1 =
                  parseFloat(
                    gridRow.get('base_currency_transaction_amount')?.value
                  ) +
                  parseFloat(
                    this.invoiceForm.get(
                      'base_currency_total_transaction_amount'
                    )?.value
                  );
                this.invoiceForm
                  .get('base_currency_total_transaction_amount')
                  ?.setValue(parseFloat(amt1.toString()).toFixed(2));
                // this.agentCal();
              },
              error: (e) => {
                this.showSwalmessage(
                  this.errorCodes.getErrorMessage(JSON.parse(e).status),
                  '',
                  'error',
                  false
                );
              },
            });
        }
      });
      // });
    }
    if (!this.is_commission) {
      let amt = 0;
      if (
        this.invoiceForm.get('txn_currency_total_transaction_amount')?.value !=
        ''
      ) {
        amt = this.invoiceForm.get(
          'txn_currency_total_transaction_amount'
        )?.value;
      }
      const sub_amount = this.invoiceForm.get(
        'txn_currency_sub_total_amount'
      )?.value;
      console.log('amt', amt);
      console.log('transaction_amount', transaction_amount);

      amt = parseFloat(amt.toString()) + transaction_amount;
      this.invoiceForm
        .get('txn_currency_total_transaction_amount')
        ?.setValue(parseFloat(amt.toString()).toFixed(2));
      const grand_amount =
        amt +
        parseFloat(sub_amount) +
        parseFloat(this.invoiceForm.get('txn_currency_tax_amount')?.value);
      const base_grand_amount =
        parseFloat(
          this.invoiceForm.get('base_currency_total_transaction_amount')?.value
        ) +
        parseFloat(
          this.invoiceForm.get('base_currency_sub_total_amount')?.value
        ) +
        parseFloat(this.invoiceForm.get('base_currency_tax_amount')?.value);
      console.log('grand_amount', grand_amount);
      console.log('base_grand_amount', base_grand_amount);
      this.invoiceForm
        .get('txn_currency_grand_total_amount')
        ?.setValue(grand_amount.toFixed(2));
      this.invoiceForm
        .get('base_currency_grand_total_amount')
        ?.setValue(base_grand_amount.toFixed(2));
    }
  }

  isAgentCommissionRate(event: any) {
    console.log(event);
    if (event) {
      const commission = this.invoiceForm.get('agent_commission')?.value;
      if (this.invoiceForm.get('agent_rate_percentage')?.value) {
        if (commission != 0 && parseFloat(commission) > 100) {
          Swal.fire({
            title: 'Not allowed',
            text: 'Percentage Should less than or equal 100 !',
            icon: 'warning',
          });
          this.invoiceForm.get('agent_commission')?.setValue('');
        } else {
          this.agentCommission();
        }
      } else {
        this.agentCommission();
      }
    } else {
      this.agentCommission();
    }
  }

  agentCommission() {
    console.log(this.invoiceForm.getRawValue());

    const payload = this.invoiceForm.getRawValue();
    let qty: any = 0;
    let amount: any = 0;
    payload.invoice_details.forEach((ele: any) => {
      console.log(ele);

      qty = parseFloat(qty) + parseFloat(ele.dispatch_qty);
      amount = parseFloat(amount) + parseFloat(ele.txn_currency_amount);
    });
    const commission: any = parseFloat(payload.agent_commission);
    console.log(commission, qty);

    console.log(this.invoiceForm.get('agent_rate_percentage')?.value);
    if (this.invoiceForm.get('agent_rate_percentage')?.value) {
      if (parseFloat(commission) > 100) {
        Swal.fire({
          title: 'Not allowed',
          text: 'Percentage Should less than or equal 100 !',
          icon: 'warning',
        });
        this.invoiceForm.get('agent_commission')?.setValue('');
      } else {
        this.invoiceForm
          .get('agent_commission_amount')
          ?.setValue(((amount * commission) / 100).toFixed(2));
      }
    } else {
      this.invoiceForm
        .get('agent_commission_amount')
        ?.setValue(((qty / 0.001) * commission).toFixed(2));
    }
  }

  setExistingArray(initialArray = []): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, ind: number) => {
      const tax_rate =
        parseFloat(element.txn_currency_igst_rate) +
        parseFloat(element.txn_currency_cgst_rate) +
        parseFloat(element.txn_currency_sgst_rate);
      // this.salesService.getTaxRate(element.item_ref_id).subscribe({
      //   next: (res: any) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,
          hsn_sac_no: element.hsn_sac_no,
          uom: element.uom,
          order_qty: element.order_qty,
          dispatch_qty: element.dispatch_qty,
          dispatch_no_of_bags: Number(element.dispatch_no_of_bags),
          rate: parseFloat(element.rate).toFixed(2),
          txn_currency: element.txn_currency,
          base_currency: element.base_currency,
          conversion_rate: element.conversion_rate,
          txn_currency_amount: parseFloat(element.txn_currency_amount).toFixed(
            2
          ),
          is_fpc_commission_rate_percentage:
            element.is_fpc_commission_rate_percentage,
          fpc_commision: element.fpc_commision,
          fpc_commision_amt: parseFloat(element.fpc_commision_amt).toFixed(2),
          fpc_commision_igst_rate: element.fpc_commision_igst_rate,
          fpc_commision_igst_amount: element.fpc_commision_igst_amount,
          fpc_commision_cgst_rate: element.fpc_commision_cgst_rate,
          fpc_commision_cgst_amount: element.fpc_commision_cgst_amount,
          fpc_commision_sgst_rate: element.fpc_commision_sgst_rate,
          fpc_commision_sgst_amount: element.fpc_commision_sgst_amount,
          fpc_commision_tax_amount: parseFloat(
            element.fpc_commision_tax_amount
          ).toFixed(2),
          fpc_commision_tax_rate:
            parseFloat(element.fpc_commision_igst_rate) +
            parseFloat(element.fpc_commision_cgst_rate) +
            parseFloat(element.fpc_commision_sgst_rate),
          tax_rate: tax_rate,
          txn_currency_igst_rate: element.txn_currency_igst_rate,
          txn_currency_igst_amount: element.txn_currency_igst_amount,
          txn_currency_cgst_rate: element.txn_currency_cgst_rate,
          txn_currency_cgst_amount: element.txn_currency_cgst_amount,
          txn_currency_sgst_rate: element.txn_currency_sgst_rate,
          txn_currency_sgst_amount: element.txn_currency_sgst_amount,
          txn_currency_tax_amount: parseFloat(
            element.txn_currency_tax_amount
          ).toFixed(2),
          txn_currency_transaction_amount: parseFloat(
            element.txn_currency_transaction_amount
          ).toFixed(2),
          base_currency_amount: element.base_currency_amount,
          base_currency_igst_rate: element.base_currency_igst_rate,
          base_currency_igst_amount: element.base_currency_igst_amount,
          base_currency_cgst_rate: element.base_currency_cgst_rate,
          base_currency_cgst_amount: element.base_currency_cgst_amount,
          base_currency_sgst_rate: element.base_currency_sgst_rate,
          base_currency_sgst_amount: element.base_currency_sgst_amount,
          base_currency_tax_amount: element.base_currency_tax_amount,
          base_currency_transaction_amount:
            element.base_currency_transaction_amount,
          is_active: element.is_active,
          is_deleted: element.is_deleted,
          invoice_qty: element.invoice_qty,
        })
      );

      const gridRow = (<FormArray>this.invoiceForm.get('invoice_details')).at(
        ind
      );
      // gridRow.disable();

      //   },
      //   error: (e) => {
      //     this.showSwalmessage(this.errorCodes.getErrorMessage(JSON.parse(e).status), '', 'error', false);
      //   }
      // })
    });

    return formArray;
  }

  getFpcCommission(ind: any, data: any, element: any) {
    const gridRow = (<FormArray>this.invoiceForm.get('invoice_details')).at(
      ind
    );
    gridRow
      .get('is_fpc_commission_rate_percentage')
      ?.setValue(data['is_rate_percentage']);
    gridRow.get('fpc_commission')?.setValue(data['commision']);
    if (data['is_rate_percentage']) {
      const amt =
        (parseFloat(element.txn_currency_amount) *
          parseFloat(data['commision'])) /
        100;
      gridRow.get('fpc_commision_amt')?.setValue(amt.toFixed(2));
      const tax_rate = gridRow.get('tax_rate')?.value;
      gridRow
        .get('txn_currency_tax_amount')
        ?.setValue(((amt * tax_rate) / 100).toFixed(2));
      const transaction_amt =
        amt + parseFloat(gridRow.get('txn_currency_tax_amount')?.value);
      gridRow.get('txn_currency_transaction_amount')?.setValue(amt.toFixed(2));
    } else {
      const amt =
        parseFloat(element.dispatch_qty) * parseFloat(data['commision']);
      gridRow.get('fpc_commision_amt')?.setValue(amt.toFixed(2));
      const tax_rate = gridRow.get('tax_rate')?.value;
      gridRow
        .get('txn_currency_tax_amount')
        ?.setValue(((amt * tax_rate) / 100).toFixed(2));
      const transaction_amt =
        amt + parseFloat(gridRow.get('txn_currency_tax_amount')?.value);
      gridRow.get('txn_currency_transaction_amount')?.setValue(amt.toFixed(2));
    }
  }

  get formArr() {
    return this.invoiceForm.get('invoice_details') as UntypedFormArray;
  }

  onCancelForm() {
    this.invoiceForm.reset();
    this.handleCancel.emit(false);
  }

  onResetForm() {
    const originalTransactionDate =
      this.invoiceForm.get('transaction_date')?.value;
    this.invoiceForm.reset();

    this.invoiceForm.get('transaction_date')?.setValue(originalTransactionDate);
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  onSubmit() {
    this.showLoader = true;
    console.log(this.invoiceForm);
    if (this.invoiceForm.invalid) {
      this.showLoader = false;
      // this.showSwalmessage('Please fill all required Fields', '', 'error', false);
    } else {
      const payload = this.invoiceForm.getRawValue();
      const val = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );

      payload.transaction_date = val;
      // this.invoiceForm.controls['so_ref_id'].enable();
      // this.invoiceForm.controls['customer_ref_id'].enable();

      payload.txn_currency = this.txnCurrency[0]['id'];
      payload.base_currency = this.txnCurrency[0]['id'];

      for (let j = 0; j < payload.invoice_details.length; j++) {
        payload.invoice_details[j].txn_currency = this.txnCurrency[0]['id'];
        payload.invoice_details[j].base_currency = this.txnCurrency[0]['id'];
      }
      console.log(payload);

      this.handleSave.emit(payload);
    }
  }

  chargesChange() {
    const sub_total =
      parseFloat(
        this.invoiceForm.get('txn_currency_packaging_charges')?.value
      ) +
      parseFloat(
        this.invoiceForm.get('txn_currency_insurance_charges')?.value
      ) +
      parseFloat(this.invoiceForm.get('txn_currency_freight_charges')?.value);
    this.invoiceForm
      .get('txn_currency_sub_total_amount')
      ?.setValue(sub_total.toFixed(2));
    const tax_amount =
      (sub_total * parseFloat(this.invoiceForm.get('tax_rate')?.value)) / 100;
    this.invoiceForm
      .get('txn_currency_tax_amount')
      ?.setValue(tax_amount.toFixed(2));

    const gridRow = <FormArray>this.invoiceForm.get('invoice_details');
    let grand_amount = sub_total + tax_amount;
    gridRow.value.forEach((element: any) => {
      grand_amount =
        grand_amount + parseFloat(element.txn_currency_transaction_amount);
    });
    this.invoiceForm
      .get('txn_currency_grand_total_amount')
      ?.setValue(grand_amount.toFixed(2));
  }
}
