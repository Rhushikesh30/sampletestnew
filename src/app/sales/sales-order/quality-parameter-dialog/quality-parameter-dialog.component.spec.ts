import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityParameterDialogComponent } from './quality-parameter-dialog.component';

describe('QualityParameterDialogComponent', () => {
  let component: QualityParameterDialogComponent;
  let fixture: ComponentFixture<QualityParameterDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualityParameterDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualityParameterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
