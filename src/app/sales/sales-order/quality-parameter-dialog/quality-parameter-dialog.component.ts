import { Component, Inject } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  UntypedFormArray,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { QcService } from 'src/app/shared/services/qc.service';
import { SalesOrderService } from 'src/app/shared/services/sales-order.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quality-parameter-dialog',
  templateUrl: './quality-parameter-dialog.component.html',
  styleUrls: ['./quality-parameter-dialog.component.scss'],
  providers: [ErrorCodes],
})
export class QualityParameterDialogComponent {
  constructor(
    private qcService: QcService,
    private dialogRef: MatDialogRef<QualityParameterDialogComponent>,
    private dialog: MatDialog,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private errorCodes: ErrorCodes,
    private salesOrderService: SalesOrderService
  ) {}

  qualityParameterdetails!: UntypedFormGroup;
  qualityData: any = [];

  submitBtn = this.data.value['submitBtn'];
  showLoader = false;
  qualityParameter: any = [];

  ngOnInit(): void {
    console.log(this.data);
    this.intializeForm();

    if (this.data.value['qc_parameter_details']) {
      this.getqctype();
      this.viewRecord(this.data.value);
    } else {
      this.qcService
        .getQualityParameterDetail(this.data.value['item_ref_id'], 'Sales')
        .subscribe({
          next: (data: any) => {
            this.qualityData = data;
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });

      this.salesOrderService
        .getItemQualityParameter(
          this.data.value['item_ref_id'],
          'Sales',
          this.data.value['transaction_date']
        )
        .subscribe({
          next: (res: any) => {
            console.log(res);
            this.qualityParameter = res;
            this.qualityParameter.forEach((ele: any) => {
              ele.item_quality_parameter_ref_id = ele.quality_type;
              ele.min_value = ele.min_quality_value;
              ele.max_value = ele.max_quality_value;
            });
            this.qualityParameterdetails.setControl(
              'qc_parameter_details',
              this.setExistingArray(this.qualityParameter)
            );
          },
          error: (e) => {
            this.showSwalmessage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              '',
              'error',
              false
            );
          },
        });
    }
  }

  intializeForm() {
    this.qualityParameterdetails = this.fb.group({
      id: [''],
      qc_parameter_details: this.fb.array([this.qc_parameter_details()]),
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  qc_parameter_details() {
    return this.fb.group({
      id: [''],
      item_quality_parameter_ref_id: [''],
      min_value: [
        '',
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$'),
        ],
      ],
      max_value: [
        '',
        [
          Validators.pattern('^[+]?([0-9]+(?:[.][0-9]*)?|.[0-9]+)$'),
          Validators.pattern('^(?!-)[+]?(\\d+(\\.\\d*)?|\\.\\d+)$'),
        ],
      ],
    });
  }

  get formArrQcp() {
    return this.qualityParameterdetails.get(
      'qc_parameter_details'
    ) as UntypedFormArray;
  }
  addNewRow() {
    this.formArrQcp.push(this.qc_parameter_details());
  }

  setExistingArray(initialArray: any): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any) => {
      console.log(element);
      formArray.push(
        this.fb.group({
          id: element.id,
          item_quality_parameter_ref_id: {
            value: element.item_quality_parameter_ref_id,
            disabled: true,
          },
          min_value: element.min_value,
          max_value: element.max_value,
        })
      );
    });
    return formArray;
  }

  viewRecord(edata: any) {
    console.log(edata);
    const qcDetailItemRow = edata['qc_parameter_details'].filter(function (
      data: any
    ) {
      return data;
    });
    if (qcDetailItemRow.length >= 1) {
      this.qualityParameterdetails.setControl(
        'qc_parameter_details',
        this.setExistingArray(qcDetailItemRow)
      );
    }

    if (this.submitBtn == false) {
      this.qualityParameterdetails.disable();
    }
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrQcp.removeAt(index);
      return true;
    }
  }

  onCancelForm() {
    this.dialogRef.close();
  }

  onSubmit() {
    console.log(this.qualityParameterdetails);
    if (this.qualityParameterdetails.invalid) {
      return;
    } else {
      this.showLoader = true;

      const qc_parameter_details = this.qualityParameterdetails
        .get('qc_parameter_details')
        ?.getRawValue();
      const payload = {
        item_ref_id: this.data.value['item_ref_id'],
        qc_parameter_details: qc_parameter_details,
      };
      console.log('payload: ', payload);
      this.dialogRef.close(payload);
    }
  }

  qualityParamsChange(event: any, index: any) {
    const val = event.target.value;
    const gridRow = (<FormArray>(
      this.qualityParameterdetails.get('qc_parameter_details')
    )).at(index);
    const parameter = gridRow.get('item_quality_parameter_ref_id')?.value;
    if (parameter == '' || parameter == undefined) {
      this.showSwalMassage('Please Select Quality Parameter', 'warning');
      gridRow.get('min_value')?.setValue('');
      gridRow.get('max_value')?.setValue('');
      return;
    }
  }

  getqctype() {
    this.qcService
      .getQualityParameterDetail(this.data.value['item_ref_id'], 'Sales')
      .subscribe({
        next: (data: any) => {
          this.qualityData = data;
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.salesOrderService
      .getItemQualityParameter(
        this.data.value['item_ref_id'],
        'Sales',
        this.data.value['transaction_date']
      )
      .subscribe({
        next: (res: any) => {
          console.log(res);
          this.qualityParameter = res;
          this.qualityParameter.forEach((ele: any) => {
            ele.item_quality_parameter_ref_id = ele.quality_type;
            ele.min_value = ele.min_quality_value;
            ele.max_value = ele.max_quality_value;
          });
          this.qualityParameterdetails.setControl(
            'qc_parameter_details',
            this.setExistingArray(this.qualityParameter)
          );
          console.log(this.data.value);
          console.log(this.qualityParameter)
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }
}
