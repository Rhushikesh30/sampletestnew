import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
  AbstractControl,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { SalesOrderService } from 'src/app/shared/services/sales-order.service';
import Swal from 'sweetalert2';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { QualityParameterDialogComponent } from '../quality-parameter-dialog/quality-parameter-dialog.component';
import { SalesService } from 'src/app/shared/services/sales.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-sales-order-details',
  templateUrl: './sales-order-details.component.html',
  styleUrls: ['./sales-order-details.component.scss'],
  providers: [
    ErrorCodes,
    DatePipe,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
  ],
})
export class SalesOrderDetailsComponent {
  @ViewChild('fileInput')
  fileInputVar!: ElementRef;
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  salesForm!: UntypedFormGroup;
  btnVal = 'Submit';
  FederationView = false;
  showReset = true;
  FPCView = false;
  fponum = false;
  CommisionView = false;
  todayDate = new Date().toJSON().split('T')[0];
  purchaseOrderData: any[] = [];
  transactionData: any[] = [];
  salesOrderData: any[] = [];
  orderDeliveryTypeData: any = [];
  customerTypeData: any = [];
  customerNameData: any = [];
  customerContactData: any = [];
  poTypeData: any = [];
  addressTypeData: any = [];
  countrydata: any = [];
  stateData: any = [];
  districtData: any = [];
  talukaData: any = [];
  leadTimeData: any = [];
  itemDetailData: any = [];
  designationdata: any = [];
  uomData: any = [];
  paymentTermData: any = [];
  deliveryTermData: any = [];
  modeOfDeliveryData: any = [];
  courierTermData: any = [];
  orderPriorityData: any = [];
  gstPortData: any = [];
  tenantData: any = [];
  countryNameData: any = [];
  CurrencyData: any = [];
  txn_currency: any;
  CustomerData: any = [];
  contactName: any = [];
  farmePaymentType: any = [];
  contactNameData: any = [];
  purchaseDetailData: any = [];
  total_transaction_amount = 0;
  total_commision_amt = 0;
  roleName: any;
  supplierPaymentData: any = [];
  billingData: any = [];
  shippingData: any = [];
  paymentTermDataID: any = [];
  deliveryTermDataID: any = [];
  agentData: any = [];
  modedeliveryTermDataID: any = [];
  DisabledSupplierPayment = false;
  showDropdown: any;
  customerName = true;
  filterAgentData: any;
  quality_params: any = [];
  empty_value = '';
  btnDisable = false;
  isTransportationView = false;
  is_agent_commission = false;
  DivisionData: any = [];
  DepartmentData: any = [];
  SaleTypeData: any = [];
  LocationData: any = [];
  ViewLoader = false;

  viewBtn = true;
  workflowBtns: any;
  myTemplate = '';
  cnt: any = 0;
  @Input() screenName!: string;
  isDisable = false;
  errorCodeData: any;
  viewOnSendback = false;

  HsnData: any[] = [];
  ItemSearchData: any;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private errorCodes: ErrorCodes,
    private datepipe: DatePipe,
    private salesorderService: SalesOrderService,
    private fpcService: FpcSetupService,
    private dialog: MatDialog,
    private roleSecurityService: RoleSecurityService,
    private elementRef: ElementRef,
    private dynamicFormService: DynamicFormService,
    private salesService: SalesService
  ) {}

  ngOnInit(): void {
    console.log(this.todayDate);
    this.initializeForm();
    const role: any = localStorage.getItem('roles');
    const roles = JSON.parse(role);
    const rolename = roles.map((a: any) => a.role_name);
    this.roleName = rolename[0];

    if (this.roleName == 'FPC Admin') {
      this.FPCView = true;
      this.FederationView = true;
    }

    this.getAlldata();

    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.viewRecord(this.rowData);
    }
  }

  getAlldata() {
    this.salesService
      .getCustomerFarmer(
        'get_combine_columns_customer_farmer',
        this.screenName,
        'customer',
        'company_name',
        'ref_type',
        ''
      )
      .subscribe({
        next: (data: any) => {
          this.CustomerData = data;
          this.customerNameData = data;
        },
      });

    this.salesorderService.getAllMasterData('Transaction Type').subscribe({
      next: (data: any) => {
        this.transactionData = data;
      },
    });
    this.salesorderService.getAllMasterData('SO Type').subscribe({
      next: (data: any) => {
        this.salesOrderData = data;
      },
    });
    this.salesorderService.getAllMasterData('Order Delivery Type').subscribe({
      next: (data: any) => {
        this.orderDeliveryTypeData = data;
      },
    });
    this.salesorderService.getAllMasterData('Sales Customer Type').subscribe({
      next: (data: any) => {
        this.customerTypeData = data;
      },
    });
    this.salesorderService.getAllMasterData('Supplier Payment Type').subscribe({
      next: (data: any) => {
        this.supplierPaymentData = data;
      },
    });
    this.salesorderService.getAllMasterData('Purchase Order Type').subscribe({
      next: (data: any) => {
        this.poTypeData = data;
      },
    });

    this.salesorderService.getAllMasterData('Payment Terms').subscribe({
      next: (data: any) => {
        this.paymentTermData = data;
      },
    });
    this.salesorderService.getAllMasterData('Delivery Terms').subscribe({
      next: (data: any) => {
        this.deliveryTermData = data;
      },
    });
    this.salesorderService.getAllMasterData('Mode Of Delivery').subscribe({
      next: (data: any) => {
        this.modeOfDeliveryData = data;
      },
    });
    this.salesorderService.getAllMasterData('Courier Terms').subscribe({
      next: (data: any) => {
        this.courierTermData = data;
      },
    });
    this.salesorderService.getAllMasterData('Order Priority').subscribe({
      next: (data: any) => {
        this.orderPriorityData = data;
      },
    });

    this.salesorderService.getCountryData().subscribe({
      next: (data: any) => {
        this.countrydata = data;
      },
    });
    this.salesorderService
      .getDynamicData('get_dynamic_data', 'uom', 'uom_code')
      .subscribe({
        next: (data: any) => {
          this.uomData = data;
        },
      });

    this.salesorderService.getCountryData().subscribe({
      next: (data: any) => {
        this.countryNameData = data;
      },
    });
    this.salesorderService
      .getDynamicData('get_dynamic_data', 'item_master', 'code')
      .subscribe({
        next: (data: any) => {
          this.itemDetailData = data;
          this.ItemSearchData = data;
        },
      });
    this.salesorderService
      .getDynamicData('get_dynamic_data', 'gst_port', 'name')
      .subscribe({
        next: (data: any) => {
          this.gstPortData = data;
        },
      });
    this.salesorderService
      .getDynamicData('get_dynamic_data', 'customer', 'display_name_as')
      .subscribe({
        next: (data: any) => {
          this.customerContactData = data;
        },
      });
    this.dynamicFormService.getDynamicData('hsn_sac', 'hsn_sac_no').subscribe({
      next: (data: any) => {
        this.HsnData = data;
      },
      error: (e: any) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.dynamicFormService.getDynamicScreenData('Agent').subscribe({
      next: (data: any) => {
        this.agentData = data['screenmatlistingdata_set'];
        this.filterAgentData = this.agentData.slice();
        console.log(this.agentData);
      },
    });
    this.salesorderService.getTenatNameData('tenant_name_get').subscribe({
      next: (data: any) => {
        this.tenantData = data;
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.salesForm.get('transaction_date')?.setValue(new Date());

    this.fpcService.getCurrencyData().subscribe({
      next: (data: any) => {
        this.CurrencyData = data;
        this.txn_currency = data.filter((d: any) => d.country_name == 'India');
        this.salesForm
          .get('txn_currency')
          ?.setValue(this.txn_currency[0]['id']);
        this.salesForm
          .get('base_currency')
          ?.setValue(this.txn_currency[0]['id']);
        const gridRow = (<FormArray>this.salesForm.get('order_details')).at(0);
        gridRow.get('txn_currency')?.setValue(this.txn_currency[0]['id']);
        gridRow.get('base_currency')?.setValue(this.txn_currency[0]['id']);
      },
    });

    this.dynamicFormService.getDynamicData('error_code_mst', 'code').subscribe({
      next: (res: any) => {
        this.errorCodeData = res;
      },
    });

    this.salesService.getDynamicDataById('division', 'name').subscribe({
      next: (data: any) => {
        this.DivisionData = data;
        const division_id = data.filter((d: any) => d.key == 'Business');
        if (division_id) {
          this.salesForm.get('division_ref_id')?.setValue(division_id[0]['id']);
          this.salesForm.get('division_ref_id')?.disable();
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.salesService.getDynamicDataById('department', 'name').subscribe({
      next: (data: any) => {
        this.DepartmentData = data;
        const dept_id = data.filter((d: any) => d.key == 'Trading');
        if (dept_id && Array.isArray(this.rowData)) {
          this.salesForm.get('department_ref_id')?.setValue(dept_id[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.salesService.getDynamicDataById('type_of_sale', 'name').subscribe({
      next: (data: any) => {
        this.SaleTypeData = data;
        const dept_id = data.filter((d: any) => d.key == 'B2B');
        if (dept_id && Array.isArray(this.rowData)) {
          this.salesForm.get('sale_type_ref_id')?.setValue(dept_id[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.salesService.getDynamicDataById('location', 'name').subscribe({
      next: (data: any) => {
        this.LocationData = data;
        if (data && Array.isArray(this.rowData)) {
          this.salesForm.get('location_ref_id')?.setValue(data[0]['id']);
        }
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  initializeForm() {
    this.salesForm = this.formBuilder.group({
      id: [''],
      transaction_date: ['', [Validators.required]],
      transaction_ref_no: [''],
      so_type_ref_id: [''],
      customer_ref_id: ['', Validators.required],
      supplier_payment_type_ref_id: [''],
      customer_contact_person_name: ['', Validators.required],
      txn_currency: ['', Validators.required],
      txn_currency_amount: [''],
      customer_po_number: [
        '',
        [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')],
      ],
      customer_po_date: ['', Validators.required],
      customer_bill_from: ['', Validators.required],
      customer_ship_to: ['', Validators.required],
      payment_terms_ref_id: ['', Validators.required],
      delivery_terms_ref_id: ['', Validators.required],
      mode_of_delivery_ref_id: ['', Validators.required],
      transportation_charges: [''],
      agent_ref_id: [''],
      agent_rate_percentage: [false],
      agent_commission: [0],
      agent_commission_amount: [0],
      customer_ref_type: [''],
      created_by: [localStorage.getItem('user_id')],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      po_ref_id: [''],

      farmer_payment_type: [''],
      is_so_on_fedration: [''],
      conversion_rate: [1],
      courier_terms_ref_id: [''],
      customer_po_type_ref_id: [''],
      base_currency: [],
      workflow_remark: [''],
      error_code_id: [''],
      order_details: this.formBuilder.array([this.order_details()]),
      division_ref_id: '',
      department_ref_id: '',
      sale_type_ref_id: '',
      location_ref_id: '',
      fpc_commision_total_amt: '',
      fpc_commision_igst_rate: 0,
      fpc_commision_igst_amount: 0,
      fpc_commision_cgst_rate: 0,
      fpc_commision_cgst_amount: 0,
      fpc_commision_sgst_rate: 0,
      fpc_commision_sgst_amount: 0,
      fpc_commision_tax_amount: 0,
      fpc_commision_tax_rate: 0,
    });
  }
  get formArrcontact() {
    return this.salesForm.get('order_details') as UntypedFormArray;
  }

  check_delivery_type(val: any) {
    this.salesorderService.getAllMasterData('Delivery Terms').subscribe({
      next: (data: any) => {
        const delivery_type = data.filter((d: any) => d.master_key == 'Spot');
        if (delivery_type[0]['id'] == val) {
          this.isTransportationView = true;
          this.salesForm
            .get('transportation_charges')
            ?.addValidators(Validators.required);
        } else {
          this.isTransportationView = false;
          this.salesForm.get('transportation_charges')?.clearValidators();
        }
        this.salesForm.get('transportation_charges')?.updateValueAndValidity();
      },
    });
  }

  viewRecord(row: any) {
    this.FederationView = true;
    console.log(row);
    this.check_delivery_type(row.delivery_terms_ref_id);

    this.salesForm.patchValue({
      id: row.id,
      transaction_date: row.transaction_date,
      transaction_ref_no: row.transaction_ref_no,
      so_type_ref_id: row.so_type_ref_id,
      customer_ref_id: row.customer_ref_id,
      supplier_payment_type_ref_id: row.supplier_payment_type_ref_id,
      customer_contact_person_name: row.customer_contact_person_name,

      txn_currency: row.txn_currency,
      txn_currency_amount: row.txn_currency_amount,
      customer_po_number: row.customer_po_number,
      customer_po_date: row.customer_po_date,
      customer_bill_from: row.customer_bill_from,
      customer_ship_to: row.customer_ship_to,
      payment_terms_ref_id: row.payment_terms_ref_id,
      delivery_terms_ref_id: row.delivery_terms_ref_id,
      mode_of_delivery_ref_id: row.mode_of_delivery_ref_id,
      transportation_charges: row.transportation_charges,
      agent_ref_id: row.agent_ref_id,
      agent_rate_percentage: row.agent_rate_percentage,
      agent_commission: parseFloat(row.agent_commission).toFixed(2),
      agent_commission_amount: parseFloat(row.agent_commission_amount).toFixed(
        2
      ),

      po_ref_id: row.po_ref_id,
      created_by: row.created_by,
      tenant_id: row.tenant_id,
      base_currency: row.base_currency,

      conversion_rate: row.conversion_rate,
      is_so_on_fedration: row.is_so_on_fedration,
      farmer_payment_type: row.farmer_payment_type,
      customer_ref_type: row.customer_ref_type,
      courier_terms_ref_id: row.courier_terms_ref_id,
      customer_po_type_ref_id: row.customer_po_type_ref_id,
      workflow_remark: row.workflow_remark,
      error_code_id: row.error_code_id,
      division_ref_id: row.division_ref_id,
      department_ref_id: row.department_ref_id,
      sale_type_ref_id: row.sale_type_ref_id,
      location_ref_id: row.location_ref_id,
      fpc_commision_igst_rate: row.fpc_commision_igst_rate,
      fpc_commision_igst_amount: row.fpc_commision_igst_amount,
      fpc_commision_cgst_rate: row.fpc_commision_cgst_rate,
      fpc_commision_cgst_amount: row.fpc_commision_cgst_amount,
      fpc_commision_sgst_rate: row.fpc_commision_sgst_rate,
      fpc_commision_sgst_amount: row.fpc_commision_sgst_amount,
      fpc_commision_tax_amount: (
        parseFloat(row.fpc_commision_igst_amount) +
        parseFloat(row.fpc_commision_cgst_amount) +
        parseFloat(row.fpc_commision_sgst_amount)
      ).toFixed(2),
      fpc_commision_tax_rate: (
        parseFloat(row.fpc_commision_igst_rate) +
        parseFloat(row.fpc_commision_cgst_rate) +
        parseFloat(row.fpc_commision_sgst_rate)
      ).toFixed(2),
      fpc_commision_total_amt: parseFloat(row.fpc_commision_total_amt).toFixed(
        2
      ),
    });

    if (row.agent_ref_id != 0) {
      this.is_agent_commission = true;
    }

    this.getAddressForOnChange(row.customer_ref_id);
    this.getCustomerContact(row.customer_ref_id);

    this.salesorderService.getAllMasterData('SO Type').subscribe({
      next: (data: any) => {
        this.salesOrderData = data;
        const so_type = this.salesOrderData.filter(
          (d: any) => d.master_key == 'Contract'
        );

        if (so_type[0]['id'] != row.so_type_ref_id) {
          this.CommisionView = true;
        } else {
          this.CommisionView = false;
        }
      },
    });

    const contractItemRow = row.order_details.filter(function (data: any) {
      return data;
    });
    if (contractItemRow.length >= 1) {
      this.salesForm.setControl(
        'order_details',
        this.setExistingArray(contractItemRow)
      );
    }

    if (this.submitBtn) {
      this.btnVal = 'Update';
      this.viewBtn = false;
      this.ViewLoader = true;

      let employee_authority = localStorage.getItem('employee_authority');
      if (employee_authority == 'None' && row.workflow_status == 'SendBack') {
        employee_authority = 'Maker';
      }
      if (
        (employee_authority == 'None' &&
          row.workflow_status == 'Pending For Approval') ||
        row.workflow_status == 'Initiated'
      ) {
        employee_authority = 'Checker';
      }
      this.roleSecurityService
        .getWorkflowBtn(
          employee_authority,
          this.screenName,
          row.transaction_ref_no
        )
        .subscribe((res: any) => {
          this.myTemplate = res;
          this.ViewLoader = false;
          console.log(res);
        });

      if (employee_authority == 'Checker') {
        this.salesForm.disable();
        this.salesForm.controls['workflow_remark'].enable();
        this.salesForm.controls['error_code_id'].enable();
        console.log(this.salesForm);
        this.isDisable = true;
        this.viewOnSendback = true;
        this.btnDisable = true;
      }
      if (employee_authority == 'Maker') {
        this.isDisable = false;
        this.viewOnSendback = true;
        if (row.workflow_status == 'SendBack') {
          this.btnDisable = false;
        } else {
          this.btnDisable = true;
        }
      }
    } else {
      let employee_authority = localStorage.getItem('employee_authority');
      if (employee_authority == 'None' && row.workflow_status == 'SendBack') {
        employee_authority = 'Maker';
      }
      if (
        (employee_authority == 'None' &&
          row.workflow_status == 'Pending For Approval') ||
        row.workflow_status == 'Initiated'
      ) {
        employee_authority = 'Checker';
      }
      this.isDisable = true;
      console.log(employee_authority);

      if (employee_authority == 'Maker' || employee_authority == 'Checker') {
        this.viewOnSendback = true;
      }

      this.salesForm.disable();
      this.salesForm.get('order_details')?.value.disabled;
    }
  }

  openAlert(event: any) {
    this.cnt = this.cnt + 1;
    if (this.cnt == 1) {
      if (event.target.id == 'Cancel') {
        this.onCancelForm();
        (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
        (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        (<HTMLInputElement>document.getElementById('SendBack')).disabled = true;
      } else if (event.target.id == 'SendBack') {
        console.log(this.salesForm);
        if (
          this.salesForm.controls['workflow_remark'].value == null ||
          this.salesForm.controls['workflow_remark'].value == ''
        ) {
          console.log('if');
          this.salesForm
            .get('error_code_id')
            ?.addValidators(Validators.required);
          this.salesForm.get('error_code_id')?.updateValueAndValidity();

          this.salesForm
            .get('workflow_remark')
            ?.addValidators(Validators.required);
          this.salesForm.get('workflow_remark')?.updateValueAndValidity();
          this.showSwalMassage(
            'Please Fill Sendback Remark and Sendback Code',
            'error'
          );
          this.cnt = 0;
        } else {
          event.target.disabled = true;
          this.salesForm.get('workflow_remark')?.clearValidators();
          this.salesForm.get('error_code_id')?.clearValidators();
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          console.log('else');
          this.onSubmit(event.target.id);
        }
      } else {
        event.target.disabled = true;
        this.salesForm.get('workflow_remark')?.clearValidators();
        this.salesForm.get('error_code_id')?.clearValidators();
        if (event.target.id == 'Approve') {
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
        }
        if (event.target.id == 'Reject') {
          (<HTMLInputElement>document.getElementById('SendBack')).disabled =
            true;
          (<HTMLInputElement>document.getElementById('Approve')).disabled =
            true;
        }
        this.onSubmit(event.target.id);
      }
    }
  }

  ngAfterContentChecked() {
    if (this.elementRef.nativeElement.querySelector('#myTemplate')) {
      this.showLoader = true;
      this.elementRef.nativeElement
        .querySelector('#myTemplate')
        .addEventListener('click', this.openAlert.bind(this));
    }
  }

  sendback() {
    console.log(this.salesForm.value);
    if (
      this.salesForm.value.workflow_remark != '' ||
      this.salesForm.value.workflow_remark != null ||
      this.salesForm.value.error_code_id != '' ||
      this.salesForm.value.error_code_id != null
    ) {
      (<HTMLInputElement>document.getElementById('Approve')).disabled = true;
      (<HTMLInputElement>document.getElementById('Reject')).disabled = true;
    }
  }

  order_details() {
    return this.formBuilder.group({
      id: [''],
      item_ref_id: ['', Validators.required],
      rate: ['', Validators.required],
      hsn_sac_no: [{ value: '', disabled: true }],
      uom: [{ value: '', disabled: true }, Validators.required],
      quantity: ['', [Validators.required]],
      txn_currency_amount: ['', Validators.required],
      lead_time: [''],

      base_currency_amount: [],
      base_currency: [],
      created_by: [localStorage.getItem('user_id')],
      tenant_id: [localStorage.getItem('COMPANY_ID')],
      qc_parameter_details: [],
      expected_delivery_date: ['', Validators.required],
      is_fpc_commission_rate_percentage: false,
      fpc_commision: [''],
      fpc_commision_amt: 0,
    });
  }

  setExistingArray(initialArray: any): UntypedFormArray {
    const formArray: any = new UntypedFormArray([]);
    initialArray.forEach((element: any, ind: number) => {
      formArray.push(
        this.formBuilder.group({
          id: element.id,
          item_ref_id: element.item_ref_id,
          // item_ref_id: {value:element.item_ref_id, disabled: true },

          rate: parseFloat(element.rate).toFixed(2),
          hsn_sac_no: { value: element.hsn_sac_no, disabled: true },
          uom: { value: element.uom, disabled: true },
          quantity: element.quantity,
          txn_currency_amount: element.txn_currency_amount,
          lead_time: element.lead_time,

          created_by: element.created_by,
          tenant_id: element.tenant_id,
          base_currency_amount: element.base_currency_amount,
          base_currency: element.base_currency,
          qc_parameter_details: [element.qc_parameter_details],
          expected_delivery_date: element.expected_delivery_date,
          is_fpc_commission_rate_percentage:
            element.is_fpc_commission_rate_percentage,
          fpc_commision: element.fpc_commision,
          fpc_commision_amt: parseFloat(element.fpc_commision_amt).toFixed(2),
        })
      );

      this.quality_params[ind] = element.qc_parameter_details;
      this.CommisionView = true;
    });
    return formArray;
  }

  onCancelForm() {
    this.handleCancel.emit(false);
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  deleteRow(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontact.removeAt(index);
      const gridRow1 = <FormArray>this.salesForm.get('order_details');
      let commision = parseFloat('0');
      let transaction_amt = parseFloat('0');
      for (let j = 0; j < gridRow1.length; j++) {
        commision =
          commision +
          parseFloat(gridRow1.at(j).get('fpc_commision_amt')?.value);
        transaction_amt =
          transaction_amt +
          parseFloat(gridRow1.at(j).get('txn_currency_amount')?.value);
        this.salesForm
          .get('txn_currency_amount')
          ?.setValue(transaction_amt.toFixed(2));
        this.salesForm
          .get('fpc_commision_total_amt')
          ?.setValue(commision.toFixed(2));
      }
      return true;
    }
  }

  addNewRow() {
    this.formArrcontact.push(this.order_details());
    const Row = <FormArray>this.salesForm.get('order_details');
    const gridRow = (<FormArray>this.salesForm.get('order_details')).at(
      Row.length - 1
    );
    gridRow.get('txn_currency')?.setValue(this.txn_currency[0]['id']);
    gridRow.get('base_currency')?.setValue(this.txn_currency[0]['id']);
    gridRow.get('is_fpc_commission_rate_percentage')?.setValue(false);
    console.log(this.salesForm);
  }

  onResetForm() {
    this.initializeForm();
  }

  onSubmit(btn: any) {
    if (this.CommisionView) {
      const orderDetailsArray = this.salesForm.get(
        'order_details'
      ) as FormArray;

      for (let i = 0; i < orderDetailsArray.length; i++) {
        const fpcCommision = orderDetailsArray.at(i).get('fpc_commision');
        const fpcCommisionAmt = orderDetailsArray
          .at(i)
          .get('fpc_commision_amt');

        if (fpcCommision?.value === null || fpcCommision?.value === '') {
          fpcCommision?.setValidators(Validators.required);
          fpcCommision?.updateValueAndValidity();
        } else {
          fpcCommision?.removeValidators(Validators.required);
          fpcCommision?.updateValueAndValidity();
        }
      }
    }

    console.log(this.salesForm);
    if (this.salesForm.invalid) {
      const invalid = [];
      const controls = this.salesForm.controls;
      const fc: any = this.salesForm.controls;
      const md = fc.controls;

      for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].markAsTouched();
        }
      }
      console.log(this.formArrcontact);
      for (let index = 0; index < this.formArrcontact.length; index++) {
        const newForm = this.formArrcontact.controls[index] as FormArray;
        for (const item in newForm.controls) {
          if (newForm.controls[item].status == 'INVALID') {
            newForm.controls[item].markAsTouched();
            console.log('newForm.controls[item]: ', [item]);
          }
        }
      }

      return;
    } else {
      // this.salesForm.enable();
      this.showLoader = true;
      const payload = this.salesForm.getRawValue();

      payload.transaction_date = this.datepipe.transform(
        payload.transaction_date,
        'yyyy-MM-dd'
      );
      payload.customer_po_date = this.datepipe.transform(
        payload.customer_po_date,
        'yyyy-MM-dd'
      );
      payload.workflow_status = btn;

      const total_amt = payload.txn_currency_amount;
      const total_com_amt = payload.fpc_commision_amt;
      if (total_amt != '') {
        if (parseFloat(total_amt) == parseFloat('0')) {
          this.showSwalMassage(
            'Total Transaction Amount must be greater than zero',
            'error'
          );
          this.showLoader = false;
          return;
        }
      } else if (total_amt == '') {
        this.showSwalMassage('Please Enter Total Transaction Amount', 'error');
        this.showLoader = false;
        return;
      }

      if (this.CommisionView && total_com_amt != '') {
        if (parseFloat(total_com_amt) == parseFloat('0')) {
          this.showSwalMassage(
            'FPC Commission Amount must be greater than zero',
            'error'
          );
          this.showLoader = false;
          return;
        }
      } else if (this.CommisionView && total_com_amt == '') {
        this.showSwalMassage('Please Enter FPC Commission Amount', 'error');
        this.showLoader = false;
        return;
      }

      const grid = payload.order_details;
      for (let j = 0; j < grid.length; j++) {
        console.log(this.quality_params);
        if (this.quality_params.length > 0) {
          grid[j].qc_parameter_details = this.quality_params[j];
        } else {
          grid[j].qc_parameter_details = null;
        }

        grid[j].expected_delivery_date = this.datepipe.transform(
          grid[j].expected_delivery_date,
          'yyyy-MM-dd'
        );

        const qty = grid[j].quantity;
        const rate = grid[j].rate;
        const com = grid[j].commision;
        const amt = grid[j].txn_currency_amount;

        if (qty != '') {
          if (parseFloat(qty) == parseFloat('0')) {
            this.showSwalMassage('Quantity must be greater than zero', 'error');
            this.showLoader = false;
            return;
          }
        } else if (qty == '') {
          this.showSwalMassage('Please Enter Quantity', 'error');
          this.showLoader = false;
          return;
        }
        if (rate != '') {
          if (parseFloat(rate) == parseFloat('0')) {
            this.showSwalMassage('Rate must be greater than zero', 'error');
            this.showLoader = false;
            return;
          }
        } else if (rate == '') {
          this.showSwalMassage('Rate Enter Quantity', 'error');
          this.showLoader = false;
          return;
        }
        if (this.CommisionView && com != '') {
          if (parseFloat(com) == parseFloat('0')) {
            this.showSwalMassage(
              'Commission must be greater than zero',
              'error'
            );
            this.showLoader = false;
            return;
          }
          if (
            grid[j].is_fpc_commission_rate_percentage &&
            parseFloat(com) > parseFloat('100')
          ) {
            this.showSwalMassage('Commission must be less than 100', 'error');
            this.showLoader = false;
            return;
          }
        } else if (this.CommisionView && com == '') {
          this.showSwalMassage('Please Enter Commission', 'error');
          this.showLoader = false;
          return;
        }
        if (amt != '') {
          if (parseFloat(amt) == parseFloat('0')) {
            this.showSwalMassage(
              'Transaction Amount must be greater than zero',
              'error'
            );
            this.showLoader = false;
            return;
          }
        } else if (amt == '') {
          this.showSwalMassage('Please Enter Transaction Amount', 'error');
          this.showLoader = false;
          return;
        }
      }

      console.log(payload);
      this.handleSave.emit(payload);
    }
  }

  agentIsRatePercenatgeChange(i: number) {
    this.getcommision(
      (<FormArray>this.salesForm.get('order_details'))
        .at(i)
        .get('fpc_commision')?.value,
      i
    );
  }

  CommisionChange(event: any, i: number) {
    this.getcommision(event.target.value, i);
  }

  getcommision(e: any, i: number) {
    console.log(
      e,
      parseFloat(e),
      parseFloat('100'),
      parseFloat(e) > parseFloat('100')
    );
    const gridRow = (<FormArray>this.salesForm.get('order_details')).at(i);
    const is_rate_per = gridRow.get('is_fpc_commission_rate_percentage')?.value;
    if (is_rate_per && parseFloat(e) > parseFloat('100')) {
      this.showSwalMassage('Commission Should Be Less Than 100%', false);
      gridRow.get('fpc_commision')?.addValidators(Validators.max(100));
      gridRow.get('fpc_commision')?.updateValueAndValidity();
    }

    this.total_transaction_amount = 0;
    this.total_commision_amt = 0;
    let commision_amount = parseFloat('0');
    let total_com_amt = parseFloat('0');
    const quantity = gridRow.get('quantity')?.value;
    const amount = gridRow.get('txn_currency_amount')?.value;

    console.log(gridRow);
    if (quantity != '' && amount != '' && e != '' && this.CommisionView) {
      if (is_rate_per == false) {
        commision_amount = parseFloat(quantity) * parseFloat(e);
        gridRow.get('fpc_commision_amt')?.setValue(commision_amount.toFixed(2));
      } else if (is_rate_per == true) {
        commision_amount = (parseFloat(amount) * parseFloat(e)) / 100;
        gridRow.get('fpc_commision_amt')?.setValue(commision_amount.toFixed(2));
      }
    }

    const gridRow1 = <FormArray>this.salesForm.get('order_details');
    let transaction_amt = parseFloat('0');
    for (let j = 0; j < gridRow1.length; j++) {
      transaction_amt =
        transaction_amt +
        parseFloat(gridRow1.at(j).get('txn_currency_amount')?.value);
      this.salesForm
        .get('txn_currency_amount')
        ?.setValue(transaction_amt.toFixed(2));
      const com_amt = gridRow1.at(j).get('fpc_commision_amt')?.value;
      total_com_amt = total_com_amt + parseFloat(com_amt);
      this.salesForm
        .get('fpc_commision_total_amt')
        ?.setValue(total_com_amt.toFixed(2));
      if (this.CommisionView) {
        this.salesService
          .getFPCTaxRate(
            this.salesForm.get('customer_ref_id')?.value,
            total_com_amt,
            this.salesForm.getRawValue().customer_bill_from,
            this.salesForm.getRawValue().customer_ship_to
          )
          .subscribe({
            next: (data: any) => {
              this.salesForm
                .get('fpc_commision_igst_rate')
                ?.setValue(data.igst_rate);
              this.salesForm
                .get('fpc_commision_igst_amount')
                ?.setValue(data.igst_amount);
              this.salesForm
                .get('fpc_commision_cgst_rate')
                ?.setValue(data.cgst_rate);
              this.salesForm
                .get('fpc_commision_cgst_amount')
                ?.setValue(data.cgst_amount);
              this.salesForm
                .get('fpc_commision_sgst_rate')
                ?.setValue(data.sgst_rate);
              this.salesForm
                .get('fpc_commision_sgst_amount')
                ?.setValue(data.sgst_amount);
              this.salesForm
                .get('fpc_commision_tax_amount')
                ?.setValue(parseFloat(data.tax_amount).toFixed(2));
              this.salesForm
                .get('fpc_commision_tax_rate')
                ?.setValue(data.igst_rate + data.cgst_rate + data.sgst_rate);

              console.log(this.salesForm);
              this.salesForm
                .get('fpc_commision_total_amt')
                ?.setValue((total_com_amt + data.tax_amount).toFixed(2));
            },
            error: (e) => {
              this.showSwalMassage(
                this.errorCodes.getErrorMessage(JSON.parse(e).status),
                'error'
              );
            },
          });
      }
    }
  }

  QuantityChange(event: any, index: any) {
    const gridRow = (<FormArray>this.salesForm.get('order_details')).at(index);
    const item_value = gridRow.get('item_ref_id')?.value;

    if (this.purchaseDetailData.length > 0) {
      const det = this.purchaseDetailData.filter(
        (d: any) => d.item_ref_id == item_value
      );
      if (event.target.value > det[0]['quantity'] && this.fponum) {
        gridRow.get('quantity')?.setValue(det[0]['quantity']);
        this.showSwalMassage(
          'Sales Order Quantity Should less than Quantity of Purchase Order',
          'warning'
        );
        return;
      }
    }

    const quantity = event.target.value;
    const rate = gridRow.get('rate')?.value;
    const transaction_amount = rate * quantity;
    gridRow.get('txn_currency_amount')?.setValue(transaction_amount.toFixed(2));

    this.agentCommission();
    this.getcommision(gridRow.get('fpc_commision')?.value, index);
  }

  RateChange(event: any, index: any) {
    const gridRow = (<FormArray>this.salesForm.get('order_details')).at(index);

    const rate = event.target.value;
    const quantity = gridRow.get('quantity')?.value;
    const transaction_amount = rate * quantity;
    gridRow.get('txn_currency_amount')?.setValue(transaction_amount.toFixed(2));

    this.agentCommission();
    this.getcommision(gridRow.get('fpc_commision')?.value, index);
  }

  ItemNameChange(event: any, index: any) {
    const item_cnt = this.salesForm.value.order_details.filter(
      (x: any) => x.item_ref_id == event.value
    );
    if (item_cnt.length > 1) {
      this.showSwalMassage('Item Already Selected', 'warning');
      (<FormArray>this.salesForm.get('order_details'))
        .at(index)
        .get('item_ref_id')
        ?.setValue('');
    } else {
      this.salesorderService
        .getDynamicData(
          'get_dynamic_data',
          'item_rate_commision_details',
          event.value
        )
        .subscribe({
          next: (data: any) => {
            const gridRow = (<FormArray>this.salesForm.get('order_details')).at(
              index
            );

            if (data.length > 0) {
              if (data[0]['is_commision_in_amount_percentage'] == null) {
                gridRow
                  .get('is_fpc_commission_rate_percentage')
                  ?.setValue(false);
              } else {
                gridRow
                  .get('is_fpc_commission_rate_percentage')
                  ?.setValue(data[0]['is_commision_in_amount_percentage']);
              }
              if (this.CommisionView == true) {
                gridRow.get('commision')?.setValue(data[0]['commision']);
              } else {
                gridRow.get('commision')?.setValue(0);
              }
            } else if (data.length == 0) {
              // gridRow.get('rate')?.setValue('')
              gridRow.get('is_fpc_commission_rate_percentage')?.setValue(false);
              gridRow.get('commision')?.setValue(0);
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

      this.salesorderService
        .getDynamicData('get_dynamic_data', 'item_master', 'hsn_sac_code')
        .subscribe({
          next: (data: any) => {
            for (let j = 0; j < data.length; j++) {
              if (event.value == data[j]['id']) {
                const gridRow = (<FormArray>(
                  this.salesForm.get('order_details')
                )).at(index);
                gridRow.get('hsn_sac_no')?.setValue(Number(data[j]['key']));
              }
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });

      this.salesorderService
        .getDynamicData('get_dynamic_data', 'item_master', 'base_uom')
        .subscribe({
          next: (data: any) => {
            for (let j = 0; j < data.length; j++) {
              if (event.value == data[j]['id']) {
                const gridRow = (<FormArray>(
                  this.salesForm.get('order_details')
                )).at(index);
                gridRow.get('uom')?.setValue(Number(data[j]['key']));
              }
            }
          },
          error: (e) => {
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
    }
  }

  soTypeChange(value: any) {
    const so_type = this.salesOrderData.filter((d: any) => d.id == value);
    if (so_type[0]['master_value'] != 'Contract') {
      this.CommisionView = true;
      const gridRow = (<FormArray>this.salesForm.get('order_details')).at(0);
      gridRow.get('is_fpc_commission_rate_percentage')?.setValue(false);
      gridRow.get('commision')?.addValidators(Validators.required);
      gridRow.get('commision')?.updateValueAndValidity;
      gridRow.get('fpc_commision_amt')?.addValidators(Validators.required);
      gridRow.get('fpc_commision_amt')?.updateValueAndValidity();
    } else {
      this.CommisionView = false;
      const gridRow = (<FormArray>this.salesForm.get('order_details')).at(0);
      gridRow.get('commision')?.clearValidators();
      gridRow.get('fpc_commision_amt')?.clearValidators();
      gridRow.get('commision')?.setValue('');
      gridRow.get('fpc_commision_amt')?.setValue(0.0);
      this.salesForm.get('fpc_commision_total_amt')?.setValue(0);
      this.salesForm.get('fpc_commision_igst_rate')?.setValue(0);
      this.salesForm.get('fpc_commision_igst_amount')?.setValue(0);
      this.salesForm.get('fpc_commision_cgst_rate')?.setValue(0);
      this.salesForm.get('fpc_commision_cgst_amount')?.setValue(0);
      this.salesForm.get('fpc_commision_sgst_rate')?.setValue(0);
      this.salesForm.get('fpc_commision_sgst_amount')?.setValue(0);
      this.salesForm.get('fpc_commision_tax_rate')?.setValue(0);
      this.salesForm.get('fpc_commision_tax_amount')?.setValue(0);
    }
  }

  deliveryTypeChange(value: any) {
    const delivery_type = this.deliveryTermData.filter(
      (d: any) => d.id == value
    );
    if (delivery_type[0]['master_value'] == 'Spot') {
      this.salesForm.get('transportation_charges')?.setValue('');
      this.salesForm
        .get('transportation_charges')
        ?.addValidators(Validators.required);
      this.isTransportationView = true;
    } else {
      this.salesForm.get('transportation_charges')?.setValue(0);
      this.salesForm.get('transportation_charges')?.clearValidators();
      this.isTransportationView = false;
    }
    this.salesForm.get('transportation_charges')?.updateValueAndValidity();
  }

  transactionAmount(salesForm: FormGroup, index: number) {
    const detailQuantity = (salesForm.get('order_details') as FormArray).at(
      index
    );

    if (detailQuantity) {
      const quantityControl = detailQuantity.get('quantity');
      const rateControl = detailQuantity.get('rate');
      const txnControl = detailQuantity.get('txn_currency_amount');
      const commisionControl = detailQuantity.get('commision');

      if (quantityControl && rateControl && txnControl && commisionControl) {
        const quantityQty = quantityControl.value;
        const rateQty = rateControl.value;

        if (quantityQty !== null && rateQty !== null) {
          const amount = quantityQty * rateQty;

          txnControl.setValue(amount);

          const rows = salesForm.get('order_details') as FormArray;
          let totalAmount = 0;

          let allSum;

          for (const row of rows.controls) {
            const transactionAmountControl = row.get('txn_currency_amount');

            if (transactionAmountControl) {
              const transactionAmount = parseFloat(
                transactionAmountControl.value
              );
              if (!isNaN(transactionAmount)) {
                totalAmount += transactionAmount;
                allSum = totalAmount;

                if (!isNaN(allSum)) {
                  salesForm.get('txn_currency_amount')?.setValue(allSum);
                } else {
                  salesForm.get('txn_currency_amount')?.setValue('');
                }
              }
            }
          }
          return totalAmount;
        }
      }
    }
    return 0;
  }

  getAddressForOnChange(e: any) {
    let parent_id = '';
    const masterkey = 'customer_details';
    parent_id = e;

    this.salesService
      .getBillingShippingAdrress(masterkey, parent_id)
      .subscribe({
        next: (data: any) => {
          this.billingData = data['Billing'];
          this.shippingData = data['Shipping'];
          console.log(data);
          if (this.btnVal == 'Submit' && this.submitBtn == true) {
            console.log(
              'billl frommm',
              this.salesForm.value.customer_bill_from
            );

            this.salesForm
              .get('customer_bill_from')
              ?.setValue(this.billingData[0]['id']);
            this.salesForm
              .get('customer_ship_to')
              ?.setValue(this.shippingData[0]['id']);
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  getCustomerContact(event: any) {
    this.salesorderService
      .getDynamicData('get_dynamic_data', 'customer', 'display_name_as')
      .subscribe({
        next: (data: any) => {
          this.customerContactData = data;
          this.customerContactData = data.filter(
            (name: { id: string }) => name.id === event
          );

          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == '' || data[i]['key'] == undefined) {
                this.salesForm
                  .get('customer_contact_person_name')
                  ?.setValue('');
              } else {
                this.salesForm
                  .get('customer_contact_person_name')
                  ?.setValue(data[i]['key']);
              }
            }
          }
        },
      });
  }

  onCustomerName(event: any) {
    const ref_type = this.customerNameData
      .filter((x: any) => x.id == event)[0]
      ['key'].split('-')[1]
      .split('/')[1];
    this.salesForm.get('customer_ref_type')?.setValue(ref_type);

    this.getAddressForOnChange(event);
    let parent_id = '';
    const masterkey = 'customer_details';
    parent_id = event;

    this.getCustomerContact(event);

    this.salesorderService.getDynamicDataa('customer', 'ref_type').subscribe({
      next: (data: any) => {
        const data1 = data.filter((name: { id: string }) => name.id === event);
        if (data1[0]['key'] == 'Federation') {
          this.DisabledSupplierPayment = true;
        } else {
          this.DisabledSupplierPayment = false;
        }
      },
    });

    this.salesorderService
      .getDynamicDataa('customer', 'payment_terms_ref_id')
      .subscribe({
        next: (data: any) => {
          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == 0) {
                this.salesForm.get('payment_terms_ref_id')?.setValue('');
              } else {
                this.salesForm
                  .get('payment_terms_ref_id')
                  ?.setValue(data[i]['key']);
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.salesorderService
      .getDynamicDataa('customer', 'delivery_terms_ref_id')
      .subscribe({
        next: (data: any) => {
          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == 0) {
                this.salesForm.get('delivery_terms_ref_id')?.setValue('');
              } else {
                this.salesForm
                  .get('delivery_terms_ref_id')
                  ?.setValue(data[i]['key']);
                const del_type = this.deliveryTermData.filter(
                  (d: any) => d.master_key == 'Spot'
                );
                if (del_type[0]['id'] == data[i]['key']) {
                  this.salesForm.get('transportation_charges')?.setValue('');
                  this.salesForm
                    .get('transportation_charges')
                    ?.addValidators(Validators.required);
                  this.isTransportationView = true;
                } else {
                  this.salesForm.get('transportation_charges')?.setValue(0);
                  this.salesForm
                    .get('transportation_charges')
                    ?.updateValueAndValidity();
                  this.isTransportationView = false;
                }
                this.salesForm
                  .get('transportation_charges')
                  ?.updateValueAndValidity();
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.salesorderService
      .getDynamicDataa('customer', 'mode_of_delivery_ref_id')
      .subscribe({
        next: (data: any) => {
          for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == event) {
              if (data[i]['key'] == 0) {
                this.salesForm.get('mode_of_delivery_ref_id')?.setValue('');
              } else {
                this.salesForm
                  .get('mode_of_delivery_ref_id')
                  ?.setValue(data[i]['key']);
              }
            }
          }
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
  }

  addQcParameterDetails(index: any) {
    const gridRow = (<FormArray>this.salesForm.get('order_details')).at(index);
    gridRow.value['qc_parameter_details'] = [];
    gridRow.get('item_ref_id')?.enable();
    console.log(this.quality_params);
    console.log(this.rowData);
    if (!gridRow.get('item_ref_id')?.value) {
      this.showSwalMassage('Please Select Item First', 'error');
      return;
    }
    if (!Array.isArray(this.rowData)) {
      console.log('row');
      gridRow.value['qc_parameter_details'] =
        this.rowData['order_details'][index]['qc_parameter_details'];
    }
    if (this.quality_params[index]) {
      console.log('quality_params');
      gridRow.value['qc_parameter_details'] = this.quality_params[index];
    }
    console.log(gridRow);
    gridRow.value['submitBtn'] = this.submitBtn;
    gridRow.value['transaction_date'] = this.datepipe.transform(
      this.salesForm.getRawValue().transaction_date,
      'yyyy-MM-dd'
    );
    const dialogRef: MatDialogRef<QualityParameterDialogComponent> =
      this.dialog.open(QualityParameterDialogComponent, {
        data: gridRow,
      });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(result);
      if (result) {
        this.quality_params[index] = result['qc_parameter_details'];
      }
    });
  }

  agentChange(event: any) {
    console.log(event);
    if (event == 0) {
      this.is_agent_commission = false;
      this.salesForm.get('agent_rate_percentage')?.setValue(false);
      this.salesForm.get('agent_commission')?.setValue(0);
      this.salesForm.get('agent_commission_amount')?.setValue(0);
      this.salesForm.get('agent_commission')?.clearValidators();
      this.salesForm.get('agent_commission_amount')?.clearValidators();
    } else {
      this.is_agent_commission = true;
      const agent = this.agentData.filter((x: any) => x.id == event)[0];
      this.salesForm
        .get('agent_rate_percentage')
        ?.setValue(agent.is_rate_percentage);
      this.salesForm.get('agent_commission')?.setValue(agent.commission);
      this.salesForm
        .get('agent_commission')
        ?.addValidators(Validators.required);
      this.salesForm
        .get('agent_commission_amount')
        ?.addValidators(Validators.required);
    }
    this.salesForm.get('agent_commission')?.updateValueAndValidity();
    this.salesForm.get('agent_commission_amount')?.updateValueAndValidity();
  }

  isAgentCommissionRate(event: any) {
    console.log(event.target.value);
    if (event) {
      const commission = this.salesForm.get('agent_commission')?.value;
      if (this.salesForm.get('agent_rate_percentage')?.value) {
        if (commission != 0 && parseFloat(commission) > 100) {
          Swal.fire({
            title: 'Not allowed',
            text: 'Percentage Should less than or equal 100 !',
            icon: 'warning',
          });
          this.salesForm.get('agent_commission')?.setValue('');
        } else {
          this.agentCommission();
        }
      } else {
        this.agentCommission();
      }
    } else {
      this.agentCommission();
    }
  }

  agentCommission() {
    const gridRow = <FormArray>this.salesForm.get('order_details');
    let qty = 0;
    let amount = 0;
    console.log(gridRow.value);
    gridRow.value.forEach((ele: any) => {
      qty = qty + ele.quantity;
      amount = amount + parseFloat(ele.txn_currency_amount);
    });
    console.log('amount', amount);
    const commission = this.salesForm.get('agent_commission')?.value;

    if (this.salesForm.get('agent_rate_percentage')?.value) {
      if (parseFloat(commission) > 100) {
        Swal.fire({
          title: 'Not allowed',
          text: 'Percentage Should less than or equal 100 !',
          icon: 'warning',
        });
        this.salesForm.get('agent_commission')?.setValue('');
      } else {
        this.salesForm
          .get('agent_commission_amount')
          ?.setValue(((amount * commission) / 100).toFixed(2));
      }
    } else {
      this.salesForm
        .get('agent_commission_amount')
        ?.setValue(((qty / 0.001) * commission).toFixed(2));
    }
  }
}
