import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { DataSource, SelectionModel } from '@angular/cdk/collections';

import { TableElement } from 'src/app/shared/TableElement';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';

import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { SalesOrderService } from 'src/app/shared/services/sales-order.service';
import { DatePipe } from '@angular/common';

import Swal from 'sweetalert2';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { STATUS, getStatusColor } from 'src/app/constants/constants';
@Component({
  selector: 'app-sales-order',
  templateUrl: './sales-order.component.html',
  styleUrls: ['./sales-order.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class SalesOrderComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  displayedColumns = [
    'actions',
    'edit',
    'print',
    'transaction_date',
    'order_number',
    'custumer_name',
    'amount',
    'status',
    'workflow_status',
  ];
  // quantity

  screenName = 'Sales Order';
  renderedData: any = [];
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv = false;
  showList = true;
  sidebarData: any;

  exampleDatabase?: SalesOrderService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  color: ThemePalette = 'primary';
  employee_authority: any;

  constructor(
    private roleSecurityService: RoleSecurityService,
    private salesorderService: SalesOrderService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private encDecryService: EncrDecrService,
    private datePipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    const userId = localStorage.getItem('user_id');
    this.employee_authority = localStorage.getItem('employee_authority');
    this.roleSecurityService
      .getAccessLeftPanel(userId, this.screenName)
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.refresh();
  }

  editViewRecord(row: any, flag: boolean) {
    this.rowData = row;
    this.showList = false;
    this.submitBtn = flag;
    this.listDiv = true;
    this.showLoader = false;
  }
  getColorStatus(status: string): string {
    return getStatusColor(status);
  }
  showFormList(item: boolean) {
    if (item === false) {
      this.listDiv = true;
      this.showList = false;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoader = true;

    const json_data = { data: this.encDecryService.encryptedData(formValue) };

    this.salesorderService
      .createSalesOrderRecord(json_data, formValue.id)
      .subscribe({
        next: (data: any) => {
          if (data['status'] == 1) {
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          } else if (data['status'] == 2) {
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
          this.refresh();
          this.showList = true;
          this.listDiv = false;
          this.showLoader = false;
          this.rowData = [];
        },
        error: (e) => {
          this.showLoader = false;
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
  }
  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new SalesOrderService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        'Transaction Date': x.transaction_date,
        'SO Number': x.transaction_ref_no,
        'Customer Name': x.customer_name,
        'Total SO Amount': x.amount,
        Status: x.status,
        'Workflow Status': x.workflow_status,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }

  printInvoice(row: any) {
    window.open('#/sales-order-print/sales-order/?id=' + row, '_blank');
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: SalesOrderService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.transaction_date +
              advanceTable.transaction_ref_no +
              advanceTable.customer_name +
              advanceTable.amount +
              advanceTable.status +
              advanceTable.workflow_status
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    console.log();
  }

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'transaction_date':
          [propertyA, propertyB] = [a.transaction_date, b.transaction_date];
          break;
        case 'transaction_ref_no':
          [propertyA, propertyB] = [a.transaction_ref_no, b.transaction_ref_no];
          break;
        case 'customer_name':
          [propertyA, propertyB] = [a.customer_name, b.customer_name];
          break;
        case 'amount':
          [propertyA, propertyB] = [a.amount, b.amount];
          break;
        case 'status':
          [propertyA, propertyB] = [a.status, b.status];
          break;
        case 'workflow_status':
          [propertyA, propertyB] = [a.workflow_status, b.workflow_status];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
