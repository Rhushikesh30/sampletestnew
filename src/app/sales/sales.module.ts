import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesRoutingModule } from './sales-routing.module';
import { SalesOrderComponent } from './sales-order/sales-order.component';
import { SalesOrderDetailsComponent } from './sales-order/sales-order-details/sales-order-details.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { ComponentsModule } from '../shared/components/components.module';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { MatSelectFilterModule } from 'mat-select-filter';
import { DispatchNoteComponent } from './dispatch-note/dispatch-note.component';
import { DispatchNoteDetailsComponent } from './dispatch-note/dispatch-note-details/dispatch-note-details.component';
import { SalesComponent } from './sales/sales.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ReceiptComponent } from './receipt/receipt.component';
import { ReceiptDetailsComponent } from './receipt/receipt-details/receipt-details.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { QcParameterDialogComponent } from './dispatch-note/qc-parameter-dialog/qc-parameter-dialog.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoiceDetailsComponent } from './invoice/invoice-details/invoice-details.component';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { QualityParameterDialogComponent } from './sales-order/quality-parameter-dialog/quality-parameter-dialog.component';
import { NgxMaterialToolsModule } from 'ngx-material-tools';
import { LotCreationComponent } from './lot-creation/lot-creation.component';
import { LotCreationDetailsComponent } from './lot-creation/lot-creation-details/lot-creation-details.component';

@NgModule({
  declarations: [
    SalesOrderComponent,
    SalesOrderDetailsComponent,
    DispatchNoteComponent,
    DispatchNoteDetailsComponent,
    QcParameterDialogComponent,
    SalesComponent,
    ReceiptComponent,
    ReceiptDetailsComponent,
    InvoiceComponent,
    InvoiceDetailsComponent,
    QualityParameterDialogComponent,
    LotCreationComponent,
    LotCreationDetailsComponent,
  ],
  imports: [
    CommonModule,
    NgxMaterialToolsModule,
    SalesRoutingModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    ComponentsModule,
    SharedModule,
    MatSelectModule,
    MatInputModule,
    CdkAccordionModule,
    MatSelectFilterModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatDialogModule,
    MomentDateModule,
  ],
})
export class SalesModule {}
