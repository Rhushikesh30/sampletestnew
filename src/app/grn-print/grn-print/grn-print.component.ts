import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { GrnService } from 'src/app/shared/services/grn.service';

import { ToWords } from 'to-words';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-grn-print',
  templateUrl: './grn-print.component.html',
  styleUrls: ['./grn-print.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class GrnPrintComponent {
  // getTotalAmountSum(arg0: any): number {
  //   let sum = 0;
  //   arg0.map((item: any) => {
  //     sum = sum + item.txn_currency_amount;
  //   });
  //   console.log('total_sum: ', sum);

  //   return sum;
  // }
  showHeader = false;
  showFooter = false;
  tenant_name = localStorage.getItem('COMPANY_NAME');
  grand_total: any;

  constructor(
    private grnService: GrnService,
    private route: ActivatedRoute,
    private encDecService: EncrDecrService,
    private errorCodes: ErrorCodes,
    private router: Router,
    private datePipe: DatePipe,
    private PurchaseorderService: PurchaseorderService
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.queryParams['id']) {
      this.getAllData(this.route.snapshot.queryParams['id']);
    }
  }

  screenName = 'GRN';

  agent_name: any;
  customer_address_bill: any;
  customer_address_ship: any;
  customer_ref_id: any;
  customer_ref_type: any;
  gst_no: any;
  dispatch_no_of_bags: any;
  hsn_sac_no: any;
  invoice_date: any;
  mode_of_delivary_terms: any;
  mode_of_delivary: any;
  state_name: any;
  po_no: any;
  item_name: any;
  rate: any;
  quantity: any;
  payment_terms: any;
  cgst_rate: any;
  cgst_amount: any;
  sgst_rate: any;
  sgst_amount: any;
  total_tax_amount: any;
  taxabale_value: any;
  txn_currency_amount: any;
  sales_order_no: any;
  sales_order_date: any;
  transaction_date: any;
  transaction_ref_no: any;
  cancelFlag = true;
  customer_name: any;
  contact_person_name: any;
  so_ref_type: any;
  supplier_name: any;
  po_date: any;
  po_ref_type: any;
  po_item_type: any;
  remark: any;
  amount: any;
  byer_adderess: any;
  byer_gst_no: any;
  po_type: any;
  contact_person: any;
  grn_no: any;
  grn_date: any;
  supplier_adress: any;
  vehicle_no: any;
  e_way_bill_no: any;
  received_qty: any;
  po_qty: any;
  supplier_gst_no: any;
  gst: any;

  totalAmount = 0;
  amount_in_word = '';

  itemData: any;
  allData: any[] = [];
  invoice_detials: any[] = [];
  itemDataList: any[] = [];

  header_image_type: any;
  header_image_logo: any;
  header_full_image = false;

  footer_image_type: any;
  footer_full_image = false;
  footer_image_logo = false;

  previewImage1: any;
  previewImage2 = '';

  NotFullImage = false;
  NotFullImageFooter = false;
  headerapplicable = false;
  footerapplicable = false;
  header_image_applicable = false;

  header_text_top_left = false;
  header_text_top_right = false;
  header_text_top_center = false;
  header_text_center_left = false;
  header_text_center_right = false;
  header_text_center_center = false;
  header_text_bottom_left = false;
  header_text_bottom_right = false;
  header_text_bottom_center = false;

  footer_image_top_left = false;
  footer_image_top_right = false;
  footer_image_top_center = false;
  footer_image_center_left = false;
  footer_image_center_right = false;
  footer_image_center_center = false;
  footer_image_bottom_left = false;
  footer_image_bottom_right = false;
  footer_image_bottom_center = false;

  footer_image_details: any;
  footer_image_position: any;
  footer_text_details: any;
  footer_text_position: any;
  header_image_details: any;
  header_image_position: any;
  header_text_details: any;
  header_text_position: any;

  headerText = '';
  headeratextpplicable = false;
  top_left = false;
  top_right = false;
  top_center = false;
  center_left = false;
  center_right = false;
  center_center = false;
  bottom_left = false;
  bottom_right = false;
  bottom_center = false;

  footertextpplicable = false;
  footerText = '';
  footer_text_top_left = false;
  footer_text_top_right = false;
  footer_text_top_center = false;
  footer_text_center_left = false;
  footer_text_center_right = false;
  footer_text_center_center = false;
  footer_text_bottom_left = false;
  footer_text_bottom_right = false;
  footer_text_bottom_center = false;

  getAllCompanyData(company_id: any) {
    this.grnService.getTenantById(company_id).subscribe({
      next: (edata: any) => {
        console.log(edata[0]['header_image_type']);

        this.header_image_type = edata[0]['header_image_type'];
        this.footer_image_type = edata[0]['footer_image_type'];

        if (edata.length > 0) {
          if (edata.header_href_attachment_path != '') {
            this.headerapplicable = true;

            if (edata[0]['header_href_attachment_path'] != '') {
              this.previewImage1 = this.encDecService.decryptedData(
                edata[0]['header_href_attachment_path']
              );
            }
            this.NotFullImage = false;
          }

          if (this.header_image_type == 'full_image') {
            this.headerapplicable = true;
            this.header_full_image = true;
            this.header_image_logo = false;
            this.NotFullImage = false;
          } else if (this.header_image_type == 'logo') {
            this.NotFullImage = true;
            this.headerapplicable = true;
            this.header_full_image = false;
            this.header_image_logo = true;
            if (edata[0]['header_image_position'] != '') {
              this.header_image_applicable = true;

              if (edata[0]['header_image_position'] == 'top-right') {
                this.header_text_top_right = true;
              }
              if (edata[0]['header_image_position'] == 'top-center') {
                this.header_text_top_center = true;
              }
              if (edata[0]['header_image_position'] == 'center-left') {
                this.header_text_center_left = true;
              }
              if (edata[0]['header_image_position'] == 'center-right') {
                this.header_text_center_right = true;
              }
              if (edata[0]['header_image_position'] == 'center-center') {
                this.header_text_center_center = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-left') {
                this.header_text_bottom_left = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-right') {
                this.header_text_bottom_right = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-center') {
                this.header_text_bottom_center = true;
              }
              if (edata[0]['header_image_position'] == 'top-left') {
                this.header_text_top_left = true;
              }
            }
          }

          if (edata[0]['header_text_details'] != '') {
            this.headerapplicable = true;
            this.NotFullImage = true;
            this.headerText = edata[0]['header_text_details'];
            this.headeratextpplicable = true;
            console.log(edata[0]['header_text_position'], 'Dataaaaaaaaaaaaa');

            if (edata[0]['header_text_position'] == 'top-left') {
              this.top_left = true;
            }
            if (edata[0]['header_text_position'] == 'top-right') {
              this.top_right = true;
            }
            if (edata[0]['header_text_position'] == 'top-center') {
              this.top_center = true;
            }
            if (edata[0]['header_text_position'] == 'center-left') {
              this.center_left = true;
            }
            if (edata[0]['header_text_position'] == 'center-right') {
              this.center_right = true;
            }
            if (edata[0]['header_text_position'] == 'center-center') {
              this.center_center = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-left') {
              this.bottom_left = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-right') {
              this.bottom_right = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-center') {
              this.bottom_center = true;
            }
          }

          if (edata[0]['footer_href_attachment_path'] != '') {
            if (edata[0]['footer_href_attachment_path'] != '') {
              this.previewImage2 = this.encDecService.decryptedData(
                edata[0]['footer_href_attachment_path']
              );
            }
            this.footerapplicable = true;
            this.NotFullImageFooter = false;
          }
          if (this.footer_image_type == 'full_image') {
            this.footerapplicable = true;
            this.footer_full_image = true;
            this.footer_image_logo = false;
            this.NotFullImageFooter = false;
          } else if (this.footer_image_type == 'logo') {
            this.footerapplicable = true;
            this.footer_full_image = false;
            this.footer_image_logo = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_image_position'] != '') {
              if (edata[0]['footer_image_position'] == 'top-left') {
                this.footer_image_top_left = true;
              }
              if (edata[0]['footer_image_position'] == 'top-right') {
                this.footer_image_top_right = true;
              }
              if (edata[0]['footer_image_position'] == 'top-center') {
                this.footer_image_top_center = true;
              }
              if (edata[0]['footer_image_position'] == 'center-left') {
                this.footer_image_center_left = true;
              }
              if (edata[0]['footer_image_position'] == 'center-right') {
                this.footer_image_center_right = true;
              }
              if (edata[0]['footer_image_position'] == 'center-center') {
                this.footer_image_center_center = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-left') {
                this.footer_image_bottom_left = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-right') {
                this.footer_image_bottom_right = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-center') {
                this.footer_image_bottom_center = true;
              }
            }
          }

          if (edata[0]['footer_text_details'] != '') {
            this.footerText = edata[0]['footer_text_details'];
            this.footertextpplicable = true;
            this.footerapplicable = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_text_position'] == 'top-left') {
              this.footer_text_top_left = true;
            }
            if (edata[0]['footer_text_position'] == 'top-right') {
              this.footer_text_top_right = true;
            }
            if (edata[0]['footer_text_position'] == 'top-center') {
              this.footer_text_top_center = true;
            }
            if (edata[0]['footer_text_position'] == 'center-left') {
              this.footer_text_center_left = true;
            }
            if (edata[0]['footer_text_position'] == 'center-right') {
              this.footer_text_center_right = true;
            }
            if (edata[0]['footer_text_position'] == 'center-center') {
              this.footer_text_center_center = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-left') {
              this.footer_text_bottom_left = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-right') {
              this.footer_text_bottom_right = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-center') {
              this.footer_text_bottom_center = true;
            }
          }
        }
      },
    });
  }
  getAllData(id: any) {
    this.grnService.getGRNPrintDataById(id).subscribe({
      next: (data: any) => {
        console.log('API DATAAAAAAAAAAAAAAAAAAAAAAAAA: ', data);
        this.allData = data;
        this.customer_address_bill = data.supplier_address_bill;
        this.customer_address_ship = data.supplier_address_ship;
        this.gst_no = data.gst_no;
        this.gst = data.gst;
        this.customer_name = data.customer_name;
        this.transaction_date = data.transaction_date;
        this.agent_name = data.agent_name;
        this.state_name = data.state_name;
        this.po_no = data.customer_po_number;
        this.invoice_date = data.customer_po_date;
        this.sales_order_no = data.sales_no;
        this.sales_order_date = data.so_date;
        this.mode_of_delivary_terms = data.mode_of_delivary_terms;
        this.mode_of_delivary = data.mode_of_delivary;
        this.payment_terms = data.payment_terms;
        this.contact_person_name = data.customer_contact_person_name;
        this.so_ref_type = data.so_ref_type;
        this.supplier_name = data.supplier_name;
        this.po_date = data.po_date;
        this.po_ref_type = data.po_ref_type;
        this.po_item_type = data.po_item_type;
        this.po_no = data.po_no;
        this.remark = data.remark;
        this.amount = data.amount;
        this.byer_adderess = data.byer_adderess;
        this.byer_gst_no = data.byer_gst_no;
        this.po_type = data.po_type;
        this.contact_person = data.contact_person;
        this.grn_no = data.grn_no;
        this.grn_date = data.grn_date;
        this.supplier_adress = data.supplier_adress;
        this.vehicle_no = data.vehicle_no;
        this.e_way_bill_no = data.e_way_bill_no;
        this.received_qty = data.received_qty;
        this.po_qty = data.po_qty;
        this.tenant_name = data.tenant_name;

        this.itemDataList = data.item_data;
        for (let i = 0; i < this.itemDataList.length; i++) {
          this.itemDataList[i]['rate'] = parseFloat(
            this.itemDataList[i]['rate']
          ).toFixed(2);
        }

        const totalAmount = this.itemDataList.reduce(
          (total, itemData) => total + parseFloat(itemData.txn_currency_amount),
          0
        );
        this.totalAmount = totalAmount;
        this.getAllCompanyData(data['tenant_id']);

        this.grand_total = 0;
        for (let ind = 0; ind < this.itemDataList.length; ind++) {
          this.grand_total +=
            this.itemDataList[ind].net_quantity * this.itemDataList[ind].rate -
            this.itemDataList[ind].labour_charges +
            this.itemDataList[ind].txn_currency_tax_amount;
        }
      },
    });
  }

  getTotalAmountSum(itemDataList: any[]): number {
    return itemDataList.reduce(
      (sum, item) => sum + parseFloat(item.txn_currency_amount),
      0
    );
  }
  convertToWords(amount: number) {
    const words = new ToWords()?.convert(amount, { currency: true });
    return `Rupees ${words.replace('Rupees', '')}`;
  }

  printComponent() {
    window.print();
  }

  Back() {
    window.close();
  }
}
