import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrnPrintComponent } from './grn-print/grn-print.component';

const routes: Routes = [
  {
    path: 'printgrn',
    component: GrnPrintComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GrnPrintRoutingModule {}
