import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GrnPrintRoutingModule } from './grn-print-routing.module';
import { GrnPrintComponent } from './grn-print/grn-print.component';


@NgModule({
  declarations: [
    GrnPrintComponent
  ],
  imports: [
    CommonModule,
    GrnPrintRoutingModule
  ]
})
export class GrnPrintModule { }
