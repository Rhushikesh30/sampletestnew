import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrintPurchasebookingComponent } from './print-purchasebooking/print-purchasebooking.component';
const routes: Routes = [

  {
    path: 'print',
    component: PrintPurchasebookingComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintPurchasebookingRoutingModule { }
