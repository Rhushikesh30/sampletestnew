import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintPurchasebookingRoutingModule } from './print-purchasebooking-routing.module';
import { PrintPurchasebookingComponent } from './print-purchasebooking/print-purchasebooking.component';


@NgModule({
  declarations: [
    PrintPurchasebookingComponent
  ],
  imports: [
    CommonModule,
    PrintPurchasebookingRoutingModule
  ]
})
export class PrintPurchasebookingModule { }
