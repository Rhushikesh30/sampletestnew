import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPurchasebookingComponent } from './print-purchasebooking.component';

describe('PrintPurchasebookingComponent', () => {
  let component: PrintPurchasebookingComponent;
  let fixture: ComponentFixture<PrintPurchasebookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintPurchasebookingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrintPurchasebookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
