import { Component , OnInit} from '@angular/core';
import { SupplierBillReportService } from 'src/app/shared/services/supplier-bill-report.service';

import { ActivatedRoute, Router } from '@angular/router';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';
import { ToWords } from 'to-words';

@Component({
  selector: 'app-print-purchasebooking',
  templateUrl: './print-purchasebooking.component.html',
  styleUrls: ['./print-purchasebooking.component.scss'],
  providers: [ErrorCodes, DatePipe],

})
export class PrintPurchasebookingComponent {

  showHeader = false;
  showFooter = false;
  tenant_name = localStorage.getItem('COMPANY_NAME');
  grandTotalInWords: any;

  constructor(
    private supplierbillreportService: SupplierBillReportService,
    private route: ActivatedRoute,
    private encDecService: EncrDecrService,
    private errorCodes: ErrorCodes,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.queryParams['id']) {
      this.getAllData(this.route.snapshot.queryParams['id']);
    }
  }

  screenName = 'Purchase Bill';

  cancelFlag = true;


  itemData: any;
  date: any;
  transaction_ref_no: any;
  farmer_ref_id: any;
  aadhar:any;
  farmer_name: any;
  vehicle_no: any;
  gate_pass_no: any;
  vehicle_token_no: any;
  farmer_address: any;
  payment_details: any;
  item_name: any;
  qc_ref_id: any;
  total_transaction_amount: any;
  farmer_unique_id: any;
  address: any;
  total_payable_amount: any;
  freight: any;
  labour_charges: any;
  payment_status: any;
  purchase_booking_ref_id: any;
  so_ref_id: any;
  so_type_ref_id: any;
  no_of_bags: any;
  gross_quantity: any;
  net_quantity: any;
  uom: any;
  hsn_sac_no: any;
  weight_of_empty_bags: any;
  rate: any;
  transaction_amount: any;
  dispatch_detail_ref_id: any;
  pb_quantity: any;
  phone_number: any;
  taluka_name: any;
  village_name:any;
  district_name:any;
  supplier_name:any;
  supplier_bill_no:any;
  supplier_id:any;
  grand_total:any;


  allData: any[] = [];
  itemDataList: any[] = [];
  paymentItemList: any[] = [];
  qualityDetailsList: any[] = [];

  header_image_type: any;
  header_image_logo: any;
  header_full_image = false;

  footer_image_type: any;
  footer_full_image = false;
  footer_image_logo = false;

  previewImage1: any;
  previewImage2 = '';

  NotFullImage = false;
  NotFullImageFooter = false;
  headerapplicable = false;
  footerapplicable = false;
  header_image_applicable = false;

  header_text_top_left = false;
  header_text_top_right = false;
  header_text_top_center = false;
  header_text_center_left = false;
  header_text_center_right = false;
  header_text_center_center = false;
  header_text_bottom_left = false;
  header_text_bottom_right = false;
  header_text_bottom_center = false;

  footer_image_top_left = false;
  footer_image_top_right = false;
  footer_image_top_center = false;
  footer_image_center_left = false;
  footer_image_center_right = false;
  footer_image_center_center = false;
  footer_image_bottom_left = false;
  footer_image_bottom_right = false;
  footer_image_bottom_center = false;

  footer_image_details: any;
  footer_image_position: any;
  footer_text_details: any;
  footer_text_position: any;
  header_image_details: any;
  header_image_position: any;
  header_text_details: any;
  header_text_position: any;

  headerText = '';
  headeratextpplicable = false;
  top_left = false;
  top_right = false;
  top_center = false;
  center_left = false;
  center_right = false;
  center_center = false;
  bottom_left = false;
  bottom_right = false;
  bottom_center = false;

  footertextpplicable = false;
  footerText = '';
  footer_text_top_left = false;
  footer_text_top_right = false;
  footer_text_top_center = false;
  footer_text_center_left = false;
  footer_text_center_right = false;
  footer_text_center_center = false;
  footer_text_bottom_left = false;
  footer_text_bottom_right = false;
  footer_text_bottom_center = false;

  getCompanyData(company_id:any){
    this.supplierbillreportService.getTenantById(company_id).subscribe({
      next: (edata: any) => {
        console.log(edata[0]['header_image_type']);

        this.header_image_type = edata[0]['header_image_type'];
        this.footer_image_type = edata[0]['footer_image_type'];

        if (edata.length > 0) {
          if (edata.header_href_attachment_path != '') {
            this.headerapplicable = true;

            if (edata[0]['header_href_attachment_path'] != '') {
              this.previewImage1 = this.encDecService.decryptedData(
                edata[0]['header_href_attachment_path']
              );
            }
            this.NotFullImage = false;
          }

          if (this.header_image_type == 'full_image') {
            this.headerapplicable = true;
            this.header_full_image = true;
            this.header_image_logo = false;
            this.NotFullImage = false;
          } else if (this.header_image_type == 'logo') {
            this.NotFullImage = true;
            this.headerapplicable = true;
            this.header_full_image = false;
            this.header_image_logo = true;
            if (edata[0]['header_image_position'] != '') {
              this.header_image_applicable = true;

              if (edata[0]['header_image_position'] == 'top-right') {
                this.header_text_top_right = true;
              }
              if (edata[0]['header_image_position'] == 'top-center') {
                this.header_text_top_center = true;
              }
              if (edata[0]['header_image_position'] == 'center-left') {
                this.header_text_center_left = true;
              }
              if (edata[0]['header_image_position'] == 'center-right') {
                this.header_text_center_right = true;
              }
              if (edata[0]['header_image_position'] == 'center-center') {
                this.header_text_center_center = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-left') {
                this.header_text_bottom_left = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-right') {
                this.header_text_bottom_right = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-center') {
                this.header_text_bottom_center = true;
              }
              if (edata[0]['header_image_position'] == 'top-left') {
                this.header_text_top_left = true;
              }
            }
          }

          if (edata[0]['header_text_details'] != '') {
            this.headerapplicable = true;
            this.NotFullImage = true;
            this.headerText = edata[0]['header_text_details'];
            this.headeratextpplicable = true;
            console.log(edata[0]['header_text_position'], 'Dataaaaaaaaaaaaa');

            if (edata[0]['header_text_position'] == 'top-left') {
              this.top_left = true;
            }
            if (edata[0]['header_text_position'] == 'top-right') {
              this.top_right = true;
            }
            if (edata[0]['header_text_position'] == 'top-center') {
              this.top_center = true;
            }
            if (edata[0]['header_text_position'] == 'center-left') {
              this.center_left = true;
            }
            if (edata[0]['header_text_position'] == 'center-right') {
              this.center_right = true;
            }
            if (edata[0]['header_text_position'] == 'center-center') {
              this.center_center = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-left') {
              this.bottom_left = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-right') {
              this.bottom_right = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-center') {
              this.bottom_center = true;
            }
          }

          if (edata[0]['footer_href_attachment_path'] != '') {
            if (edata[0]['footer_href_attachment_path'] != '') {
              this.previewImage2 = this.encDecService.decryptedData(
                edata[0]['footer_href_attachment_path']
              );
            }
            this.footerapplicable = true;
            this.NotFullImageFooter = false;
          }
          if (this.footer_image_type == 'full_image') {
            this.footerapplicable = true;
            this.footer_full_image = true;
            this.footer_image_logo = false;
            this.NotFullImageFooter = false;
          } else if (this.footer_image_type == 'logo') {
            this.footerapplicable = true;
            this.footer_full_image = false;
            this.footer_image_logo = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_image_position'] != '') {
              if (edata[0]['footer_image_position'] == 'top-left') {
                this.footer_image_top_left = true;
              }
              if (edata[0]['footer_image_position'] == 'top-right') {
                this.footer_image_top_right = true;
              }
              if (edata[0]['footer_image_position'] == 'top-center') {
                this.footer_image_top_center = true;
              }
              if (edata[0]['footer_image_position'] == 'center-left') {
                this.footer_image_center_left = true;
              }
              if (edata[0]['footer_image_position'] == 'center-right') {
                this.footer_image_center_right = true;
              }
              if (edata[0]['footer_image_position'] == 'center-center') {
                this.footer_image_center_center = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-left') {
                this.footer_image_bottom_left = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-right') {
                this.footer_image_bottom_right = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-center') {
                this.footer_image_bottom_center = true;
              }
            }
          }

          if (edata[0]['footer_text_details'] != '') {
            this.footerText = edata[0]['footer_text_details'];
            this.footertextpplicable = true;
            this.footerapplicable = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_text_position'] == 'top-left') {
              this.footer_text_top_left = true;
            }
            if (edata[0]['footer_text_position'] == 'top-right') {
              this.footer_text_top_right = true;
            }
            if (edata[0]['footer_text_position'] == 'top-center') {
              this.footer_text_top_center = true;
            }
            if (edata[0]['footer_text_position'] == 'center-left') {
              this.footer_text_center_left = true;
            }
            if (edata[0]['footer_text_position'] == 'center-right') {
              this.footer_text_center_right = true;
            }
            if (edata[0]['footer_text_position'] == 'center-center') {
              this.footer_text_center_center = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-left') {
              this.footer_text_bottom_left = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-right') {
              this.footer_text_bottom_right = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-center') {
              this.footer_text_bottom_center = true;
            }
          }
        }
      },
    });
  }
  getAllData(id: any) {
   

    this.supplierbillreportService.getPurchaseBookingDataById(id).subscribe({
      next: (data: any) => {
        console.log('data: ', data);
        data = data[0];
        this.allData = data;
        this.date = data.date;
        this.farmer_name = data.farmer_name;
        this.farmer_address = data.farmer_address;
        this.vehicle_no = data.vehicle_no;
        this.vehicle_token_no = data.vehicle_token_no;
        this.gate_pass_no = data.gate_pass_no;
        this.no_of_bags = data.no_of_bags;
        this.payment_status = data.payment_status;
        this.rate = data.rate;
        this.phone_number = data.phone_number;
        this.aadhar= data.aadhar;
        this.taluka_name = data.taluka_name;
        this.district_name=data.district_name;
        this.village_name=data.village_name;
        this.supplier_name=data.supplier_name;
        this.supplier_bill_no=data.supplier_bill_no;
        this.supplier_id=data.supplier_id;
        this.transaction_ref_no = data.transaction_ref_no;
        this.itemDataList = data.quality_details_agg_data;
        
        // this.grand_total=
        // ((this.itemDataList[0].net_quantity * this.itemDataList[0].rate) - this.itemDataList[0].labour_charges
        // ) + this.itemDataList[0].txn_currency_tax_amount;

        this.grand_total = 0;
        for (let ind = 0; ind < this.itemDataList.length; ind++) {
          this.grand_total += (
            (this.itemDataList[ind].net_quantity * this.itemDataList[ind].rate) - this.itemDataList[ind].labour_charges +
            this.itemDataList[ind].txn_currency_tax_amount
          );
        }
        

        

        this.total_transaction_amount = Number(
          data.total_transaction_amount
        ).toFixed(2);
        this.farmer_unique_id = data.unique_id;
        this.address = data.address;
        this.paymentItemList = data.payment_details;
        
    this.getCompanyData(data.tenant_id)

      },
    });
  }

  //   convertToWords(amount: number) {
  //   const words = new ToWords().convert(amount, { currency: true });
  //   return words;
  // }
  convertToWords(amount: number) {
    const words = new ToWords().convert(amount, { currency: true });
    return `Rupees ${words.replace('Rupees', '')}`;
  }

  printComponent() {
    window.print();
  }

  Back() {
    // this.router.navigate(['/purchase/report/farmer-bill']); 
    window.close()

  }

}



