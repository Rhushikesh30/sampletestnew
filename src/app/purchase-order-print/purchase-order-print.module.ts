import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseOrderPrintRoutingModule } from './purchase-order-print-routing.module';
import { PurchaseOrderPrintComponent } from './purchase-order-print/purchase-order-print.component';


@NgModule({
  declarations: [
    PurchaseOrderPrintComponent
  ],
  imports: [
    CommonModule,
    PurchaseOrderPrintRoutingModule
  ]
})
export class PurchaseOrderPrintModule { }
