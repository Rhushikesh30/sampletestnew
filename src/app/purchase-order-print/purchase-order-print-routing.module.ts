import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderPrintComponent } from './purchase-order-print/purchase-order-print.component';

const routes: Routes = [
  {
  path: 'purchase-order',
  component: PurchaseOrderPrintComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderPrintRoutingModule { }
