import {
  Component,
  ViewEncapsulation,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Renderer2,
} from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EventManagementService } from 'src/app/shared/services/event-management.service';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { FpcSetupService } from 'src/app/shared/services/fpc-setup.service';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

import { environment } from 'src/environments/environment.development';
import { DatePipe } from '@angular/common';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ErrorCodes, DatePipe],
})
export class EventsComponent implements OnInit {
  eventForm: any;
  responsiveOptions: any;
  previewData: any;
  isPreviewOpen = false;
  selectedFile!: File;
  selectedFileName = '';
  FPCData: any;
  filterFPCData: any;

  previewImage1: any;
  previewImageType1: any;
  fileName = '';
  showFileLoader = false;
  fileKYC1!: FormData;
  uploadStatus = '';
  lrAttachmentUrl!: string;
  districtData: any;
  talukadata: any;
  location: any;
  filterlocation: any;
  submitBtn = true;
  btnVal = 'Submit';

  items = [
    {
      stock: 'AAPL',
      img: 'https://picsum.photos/200/300',
    },
    {
      stock: 'F',
      img: 'https://picsum.photos/200/300',
    },
    {
      stock: 'G',
      img: 'https://picsum.photos/200/300',
    },
    {
      stock: 'G',
      img: 'https://picsum.photos/200/300',
    },
    // Add more items here...
  ];

  @ViewChild('fileInput', { static: false })
  fileInput!: ElementRef;
  isDescriptionExpanded = false;

  constructor(
    private fb: FormBuilder,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private EventManagementService: EventManagementService,
    private errorCodes: ErrorCodes,
    private purchaseorderService: PurchaseorderService,
    private fpcSetupService: FpcSetupService,
    public datepipe: DatePipe,
    private encDecService: EncrDecrService
  ) {}

  ngOnInit() {
    this.initializeCarousel();
    this.eventForm = this.fb.group({
      id: [],
      event_name: ['', Validators.required],
      location_ref_id: ['', Validators.required],
      event_date: ['', Validators.required],
      event_start_time: ['', Validators.required],
      event_end_time: ['', Validators.required],
      event_description: [''],
      event_poster_image_path: ['', Validators.required],
      fpc_ref_id: [''],
      district_ref_id: [''],
      taluka_ref_id: [''],
      pin_code: [''],
      //  organiser: ['', ],
      organiser_details: ['', Validators.required],
      location_name: [''],
    });

    this.purchaseorderService.getTenatNameData('tenant_name_get').subscribe({
      next: (data: any) => {
        this.FPCData = data;
        this.filterFPCData = this.FPCData.slice();
      },
    });

    this.purchaseorderService.getDynamicData('location', 'name').subscribe({
      next: (data: any) => {
        this.location = data;
        this.filterlocation = this.location.slice();
      },
    });

    this.EventManagementService.getAllEventData().subscribe({
      next: (data: any) => {
        this.eventsData = data;
        for (let i = 0; i < this.eventsData.length; i++) {
          if (this.eventsData[i]['href_attachment_path'] != '') {
            this.eventsData[i]['img'] = this.encDecService.decryptedData(
              this.eventsData[i]['href_attachment_path']
            );
          }
        }
        this.mediator = [
          this.eventsData[0],
          this.eventsData[1],
          this.eventsData[2],
        ];
      },
    });
  }

  slides = [
    {
      image: 'https://via.placeholder.com/700x500.png/CB997E/333333?text=1',
      caption: 'Slide 1',
    },
    {
      image: 'https://via.placeholder.com/700x500.png/DDBEA9/333333?text=2',
      caption: 'Slide 2',
    },
    {
      image: 'https://via.placeholder.com/700x500.png/FFE8D6/333333?text=3',
      caption: 'Slide 3',
    },
    {
      image: 'https://via.placeholder.com/700x500.png/B7B7A4/333333?text=4',
      caption: 'Slide 4',
    },
    {
      image: 'https://via.placeholder.com/700x500.png/A5A58D/333333?text=5',
      caption: 'Slide 5',
    },
    {
      image: 'https://via.placeholder.com/700x500.png/6B705C/eeeeee?text=6',
      caption: 'Slide 6',
    },
  ];

  initializeCarousel(): void {
    const items = document.querySelectorAll('.carousel .carousel-item');
    const minPerSlide = 3;

    items.forEach((el) => {
      let next = el.nextElementSibling;
      for (let i = 1; i < minPerSlide; i++) {
        if (!next) {
          // Wrap the carousel by using the first child
          next = items[0];
        }
        const cloneChild = next.cloneNode(true);
        if (el instanceof HTMLElement) {
          el.appendChild(cloneChild.childNodes[0]);
        }
        next = next.nextElementSibling;
      }
    });
  }

  // onFileSelected(event: any) {
  //   const inputElement = event.target as HTMLInputElement;
  //   if (inputElement.files && inputElement.files.length > 0) {
  //     this.selectedFile = inputElement.files[0];
  //     this.selectedFileName = this.selectedFile.name;
  //   }
  // }

  removeImage() {
    this.previewImage1 = '';
    this.previewImageType1 = false;
  }
  openImage(path: any) {
    console.log('path: ', path);
    console.log(this.previewImage1);

    window.open(this.previewImage1, '_blank');
  }

  onFileSelected(event: any) {
    this.previewImageType1 = false;
    this.previewImage1 = '';

    let fileToUpload = event.target.files[0];
    if (fileToUpload.size <= 5 * 1024 * 1024) {
      var reader = new FileReader();
      reader.readAsDataURL(fileToUpload);
      reader.onload = (_event) => {
        this.previewImage1 = reader.result;
      };
      let fileFormat = fileToUpload['name'].split('.')[1].toLowerCase();
      if (fileFormat == 'png' || fileFormat == 'jpg' || fileFormat == 'jpeg') {
        this.previewImageType1 = true;
      }
      this.fileName = fileToUpload['name'];
      let compare_file_type = ['png', 'jpg', 'jpeg', 'pdf'];

      if (compare_file_type.includes(fileFormat)) {
        let formData1 = new FormData();
        formData1.append('uploadedFile', fileToUpload);
        formData1.append('folder_name', 'fpcuploads');
        this.fileKYC1 = formData1;

        this.showFileLoader = true;
        this.uploadStatus = '';
        this.fpcSetupService.saveFile(this.fileKYC1).subscribe({
          next: (data: any) => {
            this.showFileLoader = false;
            let files3path = data['s3_file_path'];
            this.previewImage1 =
              environment.endpoint_url_cdn + '/' + data['s3_file_path'];
            this.lrAttachmentUrl =
              environment.endpoint_url_cdn + '/' + data['s3_file_path'];

            this.eventForm.get('event_poster_image_path')?.setValue(files3path);
            this.uploadStatus = 'Uploaded';
            Swal.fire('File Uploaded Successfully!');
          },
          error: (e) => {
            this.uploadStatus = '';
            this.fileName = '';
            this.showSwalMassage(
              this.errorCodes.getErrorMessage(JSON.parse(e).status),
              'error'
            );
          },
        });
      } else {
        this.uploadStatus = '';
        this.fileName = '';
        Swal.fire('Only .png .jpg .jpeg .pdf file formats allowed !');
      }
    } else {
      this.uploadStatus = '';
      this.fileName = '';
      this.showSwalMassage('File Size Not More Than 5 MB', 'error');
    }
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  FPCNameChange(e: any) {
    const supplier = this.FPCData.filter((x: any) => x.id == e.value);
    console.log(supplier);

    this.fpcSetupService.getDistrictData(supplier[0]['state_id']).subscribe({
      next: (data: any) => {
        this.districtData = data.filter(
          (x: any) => x.id == supplier[0]['district_id']
        );
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  DistrictNameChange(e: any) {
    const supplier = this.FPCData.filter(
      (x: any) => x.id == this.eventForm.get('fpc_ref_id')?.value
    );
    console.log(supplier);

    this.fpcSetupService.getTalukaData(e.value).subscribe({
      next: (data: any) => {
        console.log(data);

        this.talukadata = data.filter(
          (x: any) => x.id == supplier[0]['taluka_id']
        );
        console.log(this.talukadata);
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });
  }

  getSelectedFileUrl(): string {
    if (this.selectedFile) {
      return URL.createObjectURL(this.selectedFile);
    } else {
      return 'assets/usedimg/alternateEvent.png';
    }
  }

  // eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
  openPreviewDialog() {
    this.selectedFile = this.previewImage1;
    this.previewData = this.eventForm.value;
    this.previewData.selectedFileName = this.selectedFileName;
    this.isPreviewOpen = true;
  }

  closePreviewDialog() {
    this.isPreviewOpen = false;
  }
  truncateDescription(description: string, wordCount: number): string {
    const words = description.split(' ');
    if (words.length > wordCount) {
      return words.slice(0, wordCount).join(' ') + '...'; // Add ellipsis for truncated text
    }
    return description;
  }
  toggleDescription() {
    this.isDescriptionExpanded = !this.isDescriptionExpanded;

    const descriptionElement =
      this.elementRef.nativeElement.querySelector('.description');

    if (descriptionElement) {
      if (this.isDescriptionExpanded) {
        this.renderer.removeStyle(descriptionElement, 'max-height');
        this.renderer.removeStyle(descriptionElement, 'overflow');
      } else {
        this.renderer.setStyle(descriptionElement, 'max-height', '50px'); // Set the initial height
        this.renderer.setStyle(descriptionElement, 'overflow', 'hidden');
      }
    }
  }
  allFieldsFilled(): boolean {
    const eventName = this.eventForm.get('event_name').value;
    const eventDescription = this.eventForm.get('event_description').value;
    const eventDate = this.eventForm.get('event_date').value;
    const startTime = this.eventForm.get('event_start_time').value;
    const endTime = this.eventForm.get('event_end_time').value;
    const location = this.eventForm.get('location_ref_id').value;

    return (
      eventName &&
      eventDescription &&
      eventDate &&
      startTime &&
      endTime &&
      location &&
      this.selectedFile
    );
  }
  // carousel
  eventsData: any = [];
  // {
  //   heading: 'Event 1',
  //   total_farmers: 573,
  //   farmers_accepted: 251,
  //   fpc: 17,
  //   districts: 6,
  //   img: 'assets/usedimg/events.png',
  //   subheading: 'Empowering Women Farmers',
  //   date: '08 Sep 2023',
  //   time: '01:00 PM - 02:30 PM',
  //   address: 'Saraswati Hr. Sec. School, Jhanda chok, Kannod',
  // },

  startIndex = 0;
  lastIndex = 2;
  mediator: any = [];

  leftClick() {
    if (this.startIndex === 0) {
      this.startIndex = this.eventsData.length - 1;
      this.lastIndex--;
      this.mediator.unshift(this.eventsData[this.eventsData.length - 1]);
      this.mediator.pop();
    } else if (this.lastIndex === 0) {
      this.lastIndex = this.eventsData.length - 1;
      this.startIndex--;
      this.mediator.unshift(this.eventsData[this.startIndex]);
      this.mediator.pop();
    } else {
      this.startIndex--;
      this.lastIndex--;
      this.mediator.unshift(this.eventsData[this.startIndex]);
      this.mediator.pop();
    }
    console.log('start ', this.startIndex, 'last ', this.lastIndex);
  }

  rightClick() {
    if (this.lastIndex === this.eventsData.length - 1) {
      this.lastIndex = 0;
      this.startIndex++;
      this.mediator.shift();
      this.mediator.push(this.eventsData[0]);
    } else if (this.startIndex === this.eventsData.length - 1) {
      this.startIndex = 0;
      this.lastIndex++;
      this.mediator.shift();
      this.mediator.push(this.eventsData[this.lastIndex]);
    } else {
      this.startIndex++;
      this.lastIndex++;
      this.mediator.shift();
      this.mediator.push(this.eventsData[this.lastIndex]);
    }
    console.log('start ', this.startIndex, 'last ', this.lastIndex);
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  Onsubmit() {
    console.log(this.eventForm.value);

    if (this.eventForm.invalid) {
    } else {
      let val = this.datepipe.transform(
        this.eventForm.get('event_date')?.value,
        'yyyy-MM-dd'
      );
      this.eventForm.get('event_date')?.setValue(val);

      this.EventManagementService.createEvent(this.eventForm.value).subscribe({
        next: (data: any) => {
          if (data['status'] == 1) {
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
            window.location.reload();
          } else if (data['status'] == 2) {
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
            window.location.reload();
          }
        },
        error: (e: any) => {
          console.log(e);
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    }
  }

  UpdateEvent(edata: any) {
    this.submitBtn = true;
    this.btnVal = 'Update';
    const dateObj = new Date(`1970-01-01T${edata.event_start_time}Z`);
    const formattedISOString = dateObj.toISOString().substr(0, 19) + 'Z';

    const dateObj1 = new Date(`1970-01-01T${edata.event_end_time}Z`);
    const formattedISOString1 = dateObj1.toISOString().substr(0, 19) + 'Z';

    if (edata.href_attachment_path != '') {
      this.previewImage1 = this.encDecService.decryptedData(
        edata.href_attachment_path
      );
      this.previewImageType1 = true;
    }
    this.eventForm.patchValue({
      id: edata.id,
      location_name: edata.location_name,
      event_name: edata.event_name,
      location_ref_id: edata.location_ref_id,
      event_date: edata.event_date,
      event_start_time: formattedISOString,
      event_end_time: formattedISOString1,
      event_description: edata.event_description,
      event_poster_image_path: edata.event_poster_image_path,
      fpc_ref_id: edata.fpc_ref_id,
      district_ref_id: edata.district_ref_id,
      taluka_ref_id: edata.taluka_ref_id,
      pin_code: edata.pin_code,
      organiser_details: edata.organiser_details,
      img: this.previewImage1,
    });
  }

  deleteEvent(ID: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.EventManagementService.deleteEvent(ID).subscribe(
          (data: any) => {
            this.showSwalMassage(
              'Your record has been deleted successfully!',
              'success'
            );
            window.location.reload();
          },
          (error) => {
            Swal.fire(error);
          }
        );
      }
    });
  }
}
