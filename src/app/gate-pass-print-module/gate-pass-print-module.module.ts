import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
//import { SharedModule } from '../shared/shared.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
//import { ComponentsModule } from '../shared/components/components.module';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import {CdkAccordionModule} from '@angular/cdk/accordion';

import { GatePassPrintModuleRoutingModule } from './gate-pass-print-module-routing.module';
import { GatePassPrintComponent } from './gate-pass-print/gate-pass-print.component';


@NgModule({
  declarations: [
    GatePassPrintComponent,
  ],
  imports: [
    CommonModule,
    GatePassPrintModuleRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
   // SharedModule,
    MatCheckboxModule,
  //  ComponentsModule,
    MatSelectModule,
    MatSelectFilterModule,
    CdkAccordionModule,

  ]
})
export class GatePassPrintModuleModule { }
