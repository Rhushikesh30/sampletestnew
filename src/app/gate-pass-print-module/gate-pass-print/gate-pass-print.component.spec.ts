import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GatePassPrintComponent } from './gate-pass-print.component';

describe('GatePassPrintComponent', () => {
  let component: GatePassPrintComponent;
  let fixture: ComponentFixture<GatePassPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GatePassPrintComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GatePassPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
