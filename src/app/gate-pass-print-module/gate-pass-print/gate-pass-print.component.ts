import { Component, OnInit } from '@angular/core';
import { GatePassService } from 'src/app/shared/services/gate-pass.service';

import { ActivatedRoute, Router } from '@angular/router';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { EncrDecrService } from 'src/app/core/service/encr-decr.service';

@Component({
  selector: 'app-gate-pass-print',
  templateUrl: './gate-pass-print.component.html',
  styleUrls: ['./gate-pass-print.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class GatePassPrintComponent {
  showHeader = false;
  showFooter = false;
  tenant_name = localStorage.getItem('COMPANY_NAME');
  itemDataList: any;

  constructor(
    private gatepassService: GatePassService,
    private route: ActivatedRoute,
    private encDecService: EncrDecrService,
    private errorCodes: ErrorCodes,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.queryParams['id']) {
      this.getAllData(this.route.snapshot.queryParams['id']);
    }
  }

  screenName = 'Gate Pass';

  transaction_ref_no: any;
  transaction_date: any;
  supplier_ref_id: any;
  supplier_ref_type: any;
  agent_ref_id: any;
  vehicle_no: any;
  vehicle_token_no: any;
  driver_name: any;
  driver_contact_no: any;
  in_time: any;
  out_time: any;
  delivery_type: any;
  distance: any;
  remark: any;
  supplier_name: any;
  item_ref_id: any;
  item_type_ref_id: any;
  no_of_bags: any;
  uom: any;
  base_uom: any;
  alternate_uom: any;
  hsn_sac_no: any;
  quantity: any;
  alternate_quantity: any;
  supplier_address: any;
  item_name = [];
  allData: any[] = [];

  header_image_type: any;
  header_image_logo: any;
  header_full_image = false;

  footer_image_type: any;
  footer_full_image = false;
  footer_image_logo = false;

  previewImage1: any;
  previewImage2 = '';

  NotFullImage = false;
  NotFullImageFooter = false;
  headerapplicable = false;
  footerapplicable = false;
  header_image_applicable = false;

  header_text_top_left = false;
  header_text_top_right = false;
  header_text_top_center = false;
  header_text_center_left = false;
  header_text_center_right = false;
  header_text_center_center = false;
  header_text_bottom_left = false;
  header_text_bottom_right = false;
  header_text_bottom_center = false;

  footer_image_top_left = false;
  footer_image_top_right = false;
  footer_image_top_center = false;
  footer_image_center_left = false;
  footer_image_center_right = false;
  footer_image_center_center = false;
  footer_image_bottom_left = false;
  footer_image_bottom_right = false;
  footer_image_bottom_center = false;

  footer_image_details: any;
  footer_image_position: any;
  footer_text_details: any;
  footer_text_position: any;
  header_image_details: any;
  header_image_position: any;
  header_text_details: any;
  header_text_position: any;

  headerText = '';
  headeratextpplicable = false;
  top_left = false;
  top_right = false;
  top_center = false;
  center_left = false;
  center_right = false;
  center_center = false;
  bottom_left = false;
  bottom_right = false;
  bottom_center = false;

  footertextpplicable = false;
  footerText = '';
  footer_text_top_left = false;
  footer_text_top_right = false;
  footer_text_top_center = false;
  footer_text_center_left = false;
  footer_text_center_right = false;
  footer_text_center_center = false;
  footer_text_bottom_left = false;
  footer_text_bottom_right = false;
  footer_text_bottom_center = false;

  get_company_data(company_id: any) {
    this.gatepassService.getTenantById(company_id).subscribe({
      next: (edata: any) => {
        console.log(edata[0]['header_image_type']);

        this.header_image_type = edata[0]['header_image_type'];
        this.footer_image_type = edata[0]['footer_image_type'];

        if (edata.length > 0) {
          if (edata.header_href_attachment_path != '') {
            this.headerapplicable = true;

            if (edata[0]['header_href_attachment_path'] != '') {
              this.previewImage1 = this.encDecService.decryptedData(
                edata[0]['header_href_attachment_path']
              );
            }
            //  this.previewImage1=atob(edata[0]['href_header_attachment_path'])
            this.NotFullImage = false;
          }

          if (this.header_image_type == 'full_image') {
            this.headerapplicable = true;
            this.header_full_image = true;
            this.header_image_logo = false;
            this.NotFullImage = false;
          } else if (this.header_image_type == 'logo') {
            this.NotFullImage = true;
            this.headerapplicable = true;
            this.header_full_image = false;
            this.header_image_logo = true;
            if (edata[0]['header_image_position'] != '') {
              this.header_image_applicable = true;

              if (edata[0]['header_image_position'] == 'top-right') {
                this.header_text_top_right = true;
              }
              if (edata[0]['header_image_position'] == 'top-center') {
                this.header_text_top_center = true;
              }
              if (edata[0]['header_image_position'] == 'center-left') {
                this.header_text_center_left = true;
              }
              if (edata[0]['header_image_position'] == 'center-right') {
                this.header_text_center_right = true;
              }
              if (edata[0]['header_image_position'] == 'center-center') {
                this.header_text_center_center = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-left') {
                this.header_text_bottom_left = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-right') {
                this.header_text_bottom_right = true;
              }
              if (edata[0]['header_image_position'] == 'bottom-center') {
                this.header_text_bottom_center = true;
              }
              if (edata[0]['header_image_position'] == 'top-left') {
                this.header_text_top_left = true;
              }
            }
          }

          if (edata[0]['header_text_details'] != '') {
            this.headerapplicable = true;
            this.NotFullImage = true;
            this.headerText = edata[0]['header_text_details'];
            this.headeratextpplicable = true;
            console.log(edata[0]['header_text_position'], 'Dataaaaaaaaaaaaa');

            if (edata[0]['header_text_position'] == 'top-left') {
              this.top_left = true;
            }
            if (edata[0]['header_text_position'] == 'top-right') {
              this.top_right = true;
            }
            if (edata[0]['header_text_position'] == 'top-center') {
              this.top_center = true;
            }
            if (edata[0]['header_text_position'] == 'center-left') {
              this.center_left = true;
            }
            if (edata[0]['header_text_position'] == 'center-right') {
              this.center_right = true;
            }
            if (edata[0]['header_text_position'] == 'center-center') {
              this.center_center = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-left') {
              this.bottom_left = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-right') {
              this.bottom_right = true;
            }
            if (edata[0]['header_text_position'] == 'bottom-center') {
              this.bottom_center = true;
            }
          }

          if (edata[0]['footer_href_attachment_path'] != '') {
            if (edata[0]['footer_href_attachment_path'] != '') {
              this.previewImage2 = this.encDecService.decryptedData(
                edata[0]['footer_href_attachment_path']
              );
            }
            this.footerapplicable = true;
            // this.previewImage2=atob(edata[0]['footer_href_attachment_path'])
            this.NotFullImageFooter = false;
          }
          if (this.footer_image_type == 'full_image') {
            this.footerapplicable = true;
            this.footer_full_image = true;
            this.footer_image_logo = false;
            this.NotFullImageFooter = false;
          } else if (this.footer_image_type == 'logo') {
            this.footerapplicable = true;
            this.footer_full_image = false;
            this.footer_image_logo = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_image_position'] != '') {
              if (edata[0]['footer_image_position'] == 'top-left') {
                this.footer_image_top_left = true;
              }
              if (edata[0]['footer_image_position'] == 'top-right') {
                this.footer_image_top_right = true;
              }
              if (edata[0]['footer_image_position'] == 'top-center') {
                this.footer_image_top_center = true;
              }
              if (edata[0]['footer_image_position'] == 'center-left') {
                this.footer_image_center_left = true;
              }
              if (edata[0]['footer_image_position'] == 'center-right') {
                this.footer_image_center_right = true;
              }
              if (edata[0]['footer_image_position'] == 'center-center') {
                this.footer_image_center_center = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-left') {
                this.footer_image_bottom_left = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-right') {
                this.footer_image_bottom_right = true;
              }
              if (edata[0]['footer_image_position'] == 'bottom-center') {
                this.footer_image_bottom_center = true;
              }
            }
          }

          if (edata[0]['footer_text_details'] != '') {
            this.footerText = edata[0]['footer_text_details'];
            this.footertextpplicable = true;
            this.footerapplicable = true;
            this.NotFullImageFooter = true;
            if (edata[0]['footer_text_position'] == 'top-left') {
              this.footer_text_top_left = true;
            }
            if (edata[0]['footer_text_position'] == 'top-right') {
              this.footer_text_top_right = true;
            }
            if (edata[0]['footer_text_position'] == 'top-center') {
              this.footer_text_top_center = true;
            }
            if (edata[0]['footer_text_position'] == 'center-left') {
              this.footer_text_center_left = true;
            }
            if (edata[0]['footer_text_position'] == 'center-right') {
              this.footer_text_center_right = true;
            }
            if (edata[0]['footer_text_position'] == 'center-center') {
              this.footer_text_center_center = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-left') {
              this.footer_text_bottom_left = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-right') {
              this.footer_text_bottom_right = true;
            }
            if (edata[0]['footer_text_position'] == 'bottom-center') {
              this.footer_text_bottom_center = true;
            }
          }
        }
      },
    });
  }

  getAllData(id: any) {
    this.gatepassService.getGatePassPrintDataById(id).subscribe({
      next: (data: any) => {
        console.log(data, 'All Dataaaaaaaaaaa');
        this.get_company_data(data.tenant_id);

        this.allData = data;

        this.transaction_ref_no = data.transaction_ref_no;
        this.vehicle_token_no = data.vehicle_token_no;
        this.transaction_date = data.transaction_date;
        this.vehicle_no = data.vehicle_no;
        this.driver_name = data.driver_name;
        this.driver_contact_no = data.driver_contact_no;
        this.supplier_name = data.supplier_name;
        this.supplier_address = data.supplier_address;
        // this.item_name = data.item_name;
        this.no_of_bags = data.no_of_bags;
        this.in_time = data.in_time;
        this.itemDataList = data.item_name;
        console.log(this.itemDataList, 'HHHHHHHHHHHHHHHHHHHHHHHH');

        this.out_time = data.out_time;
        this.alternate_quantity = data.alternate_quantity;
        this.uom = data.uom;
      },
    });
  }

  printComponent() {
    window.print();
  }
  handlebackButton() {
    window.close();
  }
}
