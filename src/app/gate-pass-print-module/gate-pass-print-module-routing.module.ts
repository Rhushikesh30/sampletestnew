import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GatePassPrintComponent } from './gate-pass-print/gate-pass-print.component';

const routes: Routes = [
 
  {
    path: 'print',
    component: GatePassPrintComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GatePassPrintModuleRoutingModule { 

}
