import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingRoutingModule } from './accounting-routing.module';
import { AccountingComponent } from './accounting/accounting.component';
import { AccountingDetailsComponent } from './accounting/accounting-details/accounting-details.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { ComponentsModule } from '../shared/components/components.module';
import { SharedModule } from '../shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatSelectFilterModule } from 'mat-select-filter';


@NgModule({
  declarations: [
    AccountingComponent,
    AccountingDetailsComponent
  ],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    ComponentsModule,
    SharedModule,
    MatSelectModule,
    MatInputModule,
    MatSelectFilterModule
  ]
})
export class AccountingModule { }
