import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { AccountingService } from 'src/app/shared/services/accounting.service';
import { PurchaseorderService } from 'src/app/shared/services/purchaseorder.service';
import { ExpenseBookingService } from 'src/app/shared/services/expense-booking.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-accounting-details',
  templateUrl: './accounting-details.component.html',
  styleUrls: ['./accounting-details.component.scss'],
  providers: [ErrorCodes],
})
export class AccountingDetailsComponent {
  @Input() rowData!: [];
  @Input() submitBtn!: boolean;
  @Input() showLoader!: boolean;
  @Input() screenName!: string;
  @Output() handleSave = new EventEmitter<any>();
  @Output() handleCancel = new EventEmitter<any>();

  cancelFlag = true;
  accountingForm!: FormGroup;
  btnVal = 'Submit';
  showReset = true;
  viewBtn = true;
  myTemplate = '';
  departmentdata: any = [];
  accountdata: any = [];
  profitcenterdata: any = [];
  costcenterdata: any = [];
  refiddata1: any = [];
  COMPANY_ID: any;
  TransactionTypedata: any;
  Companydata: any;
  filterCompanydata: any;
  filterTransactionTypedata: any;
  filteraccountdata: any = [];
  filterTransactionFieldName: any;
  TransactionFieldName: any;
  filterTransactionFieldName1: any;
  TransactionFieldName1: any;

  expenseDetailsParametersColumns: any[] = [];
  expenseDetailsParametersColumnsValues: any;
  accountingDefinitionColumns: any[] = [];
  accountingDefinitionColumnsValues: any;

  constructor(
    private formBuilder: FormBuilder,
    private errorCodes: ErrorCodes,
    private accountingService: AccountingService,
    private purchaseorderService: PurchaseorderService,
    private expenseBookingService: ExpenseBookingService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    if (!Array.isArray(this.rowData)) {
      this.showReset = false;
      this.editViewRecord(this.rowData);
    }

    this.purchaseorderService
      .getDynamicData('transaction_type', 'name')
      .subscribe({
        next: (data: any) => {
          console.log(data);
          this.TransactionTypedata = data;
          this.filterTransactionTypedata = this.TransactionTypedata.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });

    this.purchaseorderService.getTenatNameData('tenant_name_get').subscribe({
      next: (data: any) => {
        this.Companydata = data;
        this.filterCompanydata = this.Companydata.slice();
      },
      error: (e) => {
        this.showSwalMassage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          'error'
        );
      },
    });

    this.accountingService.getDynamicScreenData('Chart Of Account').subscribe({
      next: (data: any) => {
        this.accountdata = data.screenmatlistingdata_set;
        this.filteraccountdata[0] = this.accountdata.slice();
      },
    });

    this.expenseBookingService.getAccountingParameters().subscribe({
      next: (data: any) => {
        console.log('accounting_parameter: ', data);
        this.expenseDetailsParametersColumns = data.columns;
        this.expenseDetailsParametersColumnsValues = data.data;
      },
    });
  }

  showSwalMassage(massage: any, icon: any): void {
    Swal.fire({
      title: massage,
      icon: icon,
      timer: 2000,
      showConfirmButton: false,
    });
  }

  initializeForm() {
    this.accountingForm = this.formBuilder.group({
      id: [''],
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(255),
          Validators.pattern('^[a-zA-Z_ ]*$'),
        ],
      ],
      tenant_id: [this.COMPANY_ID],
      accounting_definition_detail: this.formBuilder.array([
        this.defination_details(),
      ]),
    });
  }

  ngOnDestroy(): void {
    if (this.cancelFlag) {
      this.accountingForm.reset();
      this.handleCancel.emit(false);
    }
  }

  editViewRecord(row: any) {
    this.accountingForm.patchValue({
      id: row.id,
      tenant_id: row.tenant_id,
      name: row.name,
      created_by: row.created_by,
    });

    if (this.submitBtn) {
      this.btnVal = 'Update';
    } else {
      this.accountingForm.disable();
    }

    const difinationItemRow = row.accounting_definition_detail.filter(function (
      data: any
    ) {
      return data.is_deleted == false && data.is_active == true;
    });
    if (difinationItemRow.length >= 1) {
      this.purchaseorderService.getTenatNameData('tenant_name_get').subscribe({
        next: (data: any) => {
          this.Companydata = data;
          this.filterCompanydata = this.Companydata.slice();
          this.accountingForm.setControl(
            'accounting_definition_detail',
            this.setExistingArray1(difinationItemRow)
          );
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    }
  }

  onCancelForm() {
    this.cancelFlag = false;
    this.accountingForm.reset();
    this.handleCancel.emit(false);
  }

  onSubmit() {
    console.log(this.accountingForm.value);
    if (this.accountingForm.invalid) {
      return;
    } else {
      this.handleSave.emit(this.accountingForm.value);
    }
  }

  onResetForm() {
    this.initializeForm();
  }

  defination_details() {
    this.COMPANY_ID = localStorage.getItem('COMPANY_ID');
    return this.formBuilder.group({
      id: [''],
      tenant_id: [],
      inter_tenant_id: [''],
      ref_id_1: ['', Validators.required],
      ref_id_2: [],
      ref_id_3: ['', Validators.required],
      ref_id_4: [[]],
      ref_id_5: [''],
      ref_id_6: [''],
      ref_id_7: [''],
      ref_id_8: [''],
      ref_id_9: [''],
      ref_id_10: [''],
      parameter_1: [''],
      parameter_2: [''],
      parameter_3: [''],
      parameter_4: [''],
      parameter_5: [''],
      parameter_6: [''],
      parameter_7: [''],
      account_ref_id: ['', Validators.required],
      dr_cr: [''],
    });
  }

  setExistingArray1(initialArray = []): FormArray {
    const formArray: any = new FormArray([]);
    initialArray.forEach((element: any, ind: any) => {
      if (ind >= 1) {
        this.accountingService
          .getDynamicScreenData('Chart Of Account')
          .subscribe({
            next: (data: any) => {
              this.accountdata = data.screenmatlistingdata_set;
              this.filteraccountdata[ind] = this.accountdata.slice();
            },
          });
      }
      this.TransactionTypeChnage(element.ref_id_1);
      if (element.ref_id_4 == '' || element.ref_id_4 == null) {
        console.log();
      } else {
        // eslint-disable-next-line no-var
        var volume_type_ref = element.ref_id_4
          .split(',')
          .map(function (item: unknown) {
            return Number(item);
          });
      }
      if (element.ref_id_5 == '' || element.ref_id_5 == null) {
        console.log();
      } else {
        // eslint-disable-next-line no-var
        var volume_type_ref1 = element.ref_id_5
          .split(',')
          .map(function (item: any) {
            return Number(item);
          });
      }
      if (element.ref_id_6 == '' || element.ref_id_6 == null) {
        console.log();
      } else {
        // eslint-disable-next-line no-var
        var volume_type_ref2 = element.ref_id_6
          .split(',')
          .map(function (item: any) {
            return Number(item);
          });
      }
      if (element.ref_id_7 == '' || element.ref_id_7 == null) {
        console.log();
      } else {
        // eslint-disable-next-line no-var
        var volume_type_ref3 = element.ref_id_7
          .split(',')
          .map(function (item: any) {
            return Number(item);
          });
      }

      formArray.push(
        this.formBuilder.group({
          id: element.id,
          tenant_id: element.tenant_id,
          created_by: element.created_by,
          ref_id_1: Number(element.ref_id_1),
          ref_id_2: Number(element.ref_id_2),
          ref_id_3: Number(element.ref_id_3),
          ref_id_4: [volume_type_ref],
          ref_id_5: [volume_type_ref1],
          ref_id_6: [volume_type_ref2],
          ref_id_7: [volume_type_ref3],
          ref_id_8: element.ref_id_8,
          ref_id_9: element.ref_id_9,
          ref_id_10: element.ref_id_10,
          account_ref_id: element.account_ref_id,
          inter_tenant_id: element.inter_tenant_id,
          parameter_1:
            element.parameter_1 === 'ALL' ? 'ALL' : Number(element.parameter_1),
          parameter_2:
            element.parameter_2 === 'ALL' ? 'ALL' : Number(element.parameter_2),
          parameter_3:
            element.parameter_3 === 'ALL' ? 'ALL' : Number(element.parameter_3),
          parameter_4:
            element.parameter_4 === 'ALL' ? 'ALL' : Number(element.parameter_4),
          parameter_5:
            element.parameter_5 === 'ALL' ? 'ALL' : Number(element.parameter_5),
          parameter_6:
            element.parameter_6 === 'ALL' ? 'ALL' : Number(element.parameter_6),
          parameter_7:
            element.parameter_7 === 'ALL' ? 'ALL' : Number(element.parameter_7),
          dr_cr: element.dr_cr,
        })
      );
    });
    return formArray;
  }

  get formArrcontract() {
    return this.accountingForm.get('accounting_definition_detail') as FormArray;
  }

  addNewRow1() {
    this.formArrcontract.push(this.defination_details());
    for (let i = 0; i < this.formArrcontract.length; i++) {
      this.filteraccountdata[i + 1] = this.accountdata.slice();
    }
  }

  deleteRow1(index: number) {
    if (index == 0) {
      return false;
    } else {
      this.formArrcontract.removeAt(index);
      return true;
    }
  }

  getParameter(i: any) {
    const paramter = 'parameter_' + i;
    return this.expenseDetailsParametersColumnsValues[paramter];
  }

  getParameter1(i: any) {
    const paramter = 'ref_id_' + i;
    return this.accountingDefinitionColumnsValues[paramter];
  }

  TransactionTypeChnage(e: any) {
    this.purchaseorderService
      .getDynamicData('transaction_type_details_ref_id_3', e)
      .subscribe({
        next: (data: any) => {
          this.TransactionFieldName = data;
          this.filterTransactionFieldName = this.TransactionFieldName.slice();
        },
        error: (e) => {
          this.showSwalMassage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            'error'
          );
        },
      });
    this.accountingService.getAccountingParameters(e).subscribe({
      next: (data: any) => {
        console.log(data);

        this.accountingDefinitionColumns = data.columns;
        this.accountingDefinitionColumnsValues = data.data;
      },
    });
  }
}
