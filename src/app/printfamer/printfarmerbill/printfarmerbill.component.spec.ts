import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintfarmerbillComponent } from './printfarmerbill.component';

describe('PrintfarmerbillComponent', () => {
  let component: PrintfarmerbillComponent;
  let fixture: ComponentFixture<PrintfarmerbillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintfarmerbillComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrintfarmerbillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
