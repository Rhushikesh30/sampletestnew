import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrintfarmerbillComponent } from './printfarmerbill/printfarmerbill.component';

const routes: Routes = [

  {
    path: 'farmer',
    component: PrintfarmerbillComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintfamerRoutingModule { }
