import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintfamerRoutingModule } from './printfamer-routing.module';
import { PrintfarmerbillComponent } from './printfarmerbill/printfarmerbill.component';


@NgModule({
  declarations: [
    PrintfarmerbillComponent
  ],
  imports: [
    CommonModule,
    PrintfamerRoutingModule
  ]
})
export class PrintfamerModule { }
