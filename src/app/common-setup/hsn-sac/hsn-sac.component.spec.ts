import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HsnSacComponent } from './hsn-sac.component';

describe('HsnSacComponent', () => {
  let component: HsnSacComponent;
  let fixture: ComponentFixture<HsnSacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HsnSacComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HsnSacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
