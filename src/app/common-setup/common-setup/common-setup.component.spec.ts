import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonSetupComponent } from './common-setup.component';

describe('CommonSetupComponent', () => {
  let component: CommonSetupComponent;
  let fixture: ComponentFixture<CommonSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonSetupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CommonSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
