import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesignationComponent } from './designation/designation.component';
import { CommonSetupComponent } from './common-setup/common-setup.component';
import { BankComponent } from './bank/bank.component';
import { SeasonComponent } from './season/season.component';
import { TaxAuthorityComponent } from './tax-authority/tax-authority.component';
import { TaxRateComponent } from './tax-rate/tax-rate.component';
import { UomConversionComponent } from './uom-conversion/uom-conversion.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'setup',
    pathMatch: 'full'
  },
  {
    path: 'setup',
    component: CommonSetupComponent
  },
  {
    path: 'designation',
    component: DesignationComponent,
  },
  {
    path: 'bank',
    component: BankComponent,
  },
  {
    path: 'season',
    component: SeasonComponent,
  },
  {
    path: 'tax-authority',
    component: TaxAuthorityComponent,
  },
  {
    path: 'tax-rate',
    component: TaxRateComponent,
  },
  {
    path: 'uom-conversion',
    component: UomConversionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonSetupRoutingModule { }






