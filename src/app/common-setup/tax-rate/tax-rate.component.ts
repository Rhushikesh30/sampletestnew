import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatMenuTrigger } from '@angular/material/menu';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';

import { TableElement } from 'src/app/shared/TableElement';
import { ErrorCodes } from 'src/app/shared/codes/error-codes';
import { TableExportUtil } from 'src/app/shared/tableExportUtil';

import { CommonSetupService } from 'src/app/shared/services/common-setup.service';
import { RoleSecurityService } from 'src/app/core/service/role-security.service';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { DynamicFormService } from 'src/app/shared/services/dynamic-form.service';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-tax-rate',
  templateUrl: './tax-rate.component.html',
  styleUrls: ['./tax-rate.component.scss'],
  providers: [ErrorCodes, DatePipe],
})
export class TaxRateComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;
  resultData!: [];

  renderedData: any = [];
  screenName = 'Tax Rate';
  submitBtn = true;
  showLoader = false;
  rowData: any = [];
  listDiv: boolean = false;
  showList: boolean = true;
  sidebarData: any;

  exampleDatabase?: DynamicFormService;
  dataSource!: ExampleDataSource;
  selection = new SelectionModel<any>(true, []);
  id?: number;
  advanceTable?: any;
  contextMenu?: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  tableCols: any = [];
  tableData: any = [];
  ScreenJsonData: any = [];
  displayedColumns = [];
  displayedColumnsheader = [];
  BTN_VAL = 'Submit';
  showLoaderSubmit = false;
  form!: FormGroup;

  todayDate = new Date().toJSON().split('T')[0];

  constructor(
    private roleSecurityService: RoleSecurityService,
    private dynamicService: DynamicFormService,
    private errorCodes: ErrorCodes,
    public httpClient: HttpClient,
    private commonsetupservice: CommonSetupService,
    public datepipe: DatePipe
  ) {
    super();
  }

  ngOnInit(): void {
    let userId = localStorage.getItem('user_id');

    this.roleSecurityService
      .getAccessLeftPanel(userId, 'Finance Settings')
      .subscribe({
        next: (data: any) => {
          this.sidebarData = data[0];
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.commonsetupservice
      .getDynamicScreenFormData(this.screenName)
      .subscribe({
        next: (data: any) => {
          this.ScreenJsonData = data;
        },
        error: (e) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });

    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.ScreenJsonData)),
    });

    this.refresh();
    this.getAllData();
  }
  getAllData() {
    this.commonsetupservice
      .getDynamicScreenData(this.screenName)
      .subscribe((data: any) => {
        this.tableCols[0] = { mat_cell_name: 'View', mat_header_cell: 'View' };
        this.tableCols[1] = { mat_cell_name: 'Edit', mat_header_cell: 'Edit' };
        this.tableCols[2] = {
          mat_cell_name: 'Version',
          mat_header_cell: 'Version',
        };
        for (let i = 0; i < data['screenmatlistingcolumms_set'].length; i++) {
          this.tableCols[i + 3] = data['screenmatlistingcolumms_set'][i];

          if (this.tableCols[i + 3]['mat_header_cell'] == 'Is Active') {
            this.tableCols[i + 3]['mat_header_cell'] = 'Status';
          }
        }

        this.displayedColumns = this.tableCols.map((c: any) => c.mat_cell_name);
        this.tableData = data.screenmatlistingdata_set;
        for (let i = 0; i < this.tableData.length; i++) {
          if (this.tableData[i]['is_active'] == true) {
            this.tableData[i]['is_active'] = 'Active';
          } else {
            this.tableData[i]['is_active'] = 'Inactive';
          }
        }
      });
  }

  createVersion(row: any, flag: boolean) {
    if (
      row.from_date == this.datepipe.transform(this.todayDate, 'yyyy-MM-dd')
    ) {
      Swal.fire({
        title: 'Not allowed',
        text: 'Cannot create version on same day!!',
        icon: 'warning',
      });
    } else {
      this.commonsetupservice.CheckVersionExist('Tax Rate', row.id).subscribe({
        next: (data: any) => {
          if (data['msg'] == 'Version already exists.') {
            Swal.fire({
              title: 'Not allowed',
              text: data['msg'] + '!!',
              icon: 'warning',
            });
          } else {
            this.BTN_VAL = 'Create Version';
            this.rowData = row;
            this.showList = false;
            this.listDiv = true;
            this.submitBtn = true;
          }
        },
        error: (e: any) => {
          this.showSwalmessage(
            this.errorCodes.getErrorMessage(JSON.parse(e).status),
            '',
            'error',
            false
          );
        },
      });
    }
  }

  editViewRecord(row: any, flag: boolean) {
    if (flag == false) {
      this.rowData = row;
      this.showList = false;
      this.submitBtn = false;
      //this.listDiv = true;
    } else {
      this.rowData = row;
      this.showList = false;
      // this.listDiv = true;
      this.submitBtn = true;
      this.BTN_VAL = 'Update';
    }
  }
  showFormList(item: boolean) {
    // window.scrollTo(0, 0);

    this.rowData = [];
    this.BTN_VAL = 'Submit';
    if (item === false) {
      //   this.listDiv = true;
      this.showList = false;
      this.submitBtn = true;
    } else {
      this.listDiv = false;
      this.showList = true;
    }
  }

  handleCancel(item: boolean) {
    this.listDiv = item;
    this.showList = true;
    this.rowData = [];
    this.submitBtn = true;
  }

  showSwalmessage(
    message: any,
    text: any,
    icon: any,
    confirmButton: any
  ): void {
    if (confirmButton == false) {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: false,
      });
    } else {
      Swal.fire({
        title: message,
        text: text,
        icon: icon,
        timer: 2000,
        showConfirmButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
      });
    }
  }

  handleSave(formValue: any) {
    this.showLoaderSubmit = true;
    if (this.BTN_VAL == 'Create Version') {
      formValue['create_version'] = true;
    }
    if (this.BTN_VAL == 'Update') {
      console.log(formValue);
      if (formValue.master_data[0].revision_status == 'Future') {
        formValue['update_version'] = true;
      }
    }

    this.commonsetupservice.createScreenData(formValue).subscribe({
      next: (data: any) => {
        if (data['status'] == 1) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been updated successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 2) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your record has been added successfully!',
              '',
              'success',
              false
            );
          }
        } else if (data['status'] == 3) {
          if (data['msg'] == 'Record already exists.') {
            this.showLoaderSubmit = false;
            this.showSwalmessage('Record already exists!', '', 'error', false);
          } else {
            this.refresh();
            this.showList = true;
            this.listDiv = false;
            this.showLoaderSubmit = false;
            this.showSwalmessage(
              'Your Version Created  successfully!',
              '',
              'success',
              false
            );
          }
        }
      },
      error: (e) => {
        this.showLoaderSubmit = false;
        this.showSwalmessage(
          this.errorCodes.getErrorMessage(JSON.parse(e).status),
          '',
          'error',
          false
        );
      },
    });
  }

  onContextMenu(event: MouseEvent, item: any) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    if (this.contextMenu !== undefined && this.contextMenu.menu !== null) {
      this.contextMenu.menuData = { item: item };
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }
  }

  refresh() {
    this.exampleDatabase = new DynamicFormService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort
    );
    this.subs.sink = fromEvent(this.filter.nativeElement, 'keyup').subscribe(
      () => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    );
  }

  exportExcel() {
    const exportData: Partial<TableElement>[] =
      this.dataSource.filteredData.map((x) => ({
        Name: x.tax_rate_name,
        'Tax Type': x.tax_type_ref_id,
        'Tax Authoriry': x.tax_authority_ref_id,
        'From Date': x.from_date,
        'To Date': x.to_date,
        'Revision Status': x.revision_status,
      }));

    TableExportUtil.exportToExcel(exportData, this.screenName);
  }
}

export class ExampleDataSource extends DataSource<any> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: any[] = [];
  renderedData: any[] = [];

  constructor(
    public exampleDatabase: DynamicFormService,
    public paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe(() => (this.paginator.pageIndex = 0));
  }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.exampleDatabase.dataChange,
      this.filterChange,
      this.paginator.page,
    ];
    this.exampleDatabase.getAllAdvanceTables('Tax Rate');
    return merge(...displayDataChanges).pipe(
      map(() => {
        for (let i = 0; i < this.exampleDatabase.data.length; i++) {
          if (this.exampleDatabase.data[i]['is_active'] == true) {
            this.exampleDatabase.data[i]['is_active'] = 'Active';
          } else if (this.exampleDatabase.data[i]['is_active'] == false) {
            this.exampleDatabase.data[i]['is_active'] = 'Inactive';
          }
        }
        this.filteredData = this.exampleDatabase.data
          .slice()
          .filter((advanceTable: any) => {
            const searchStr = (
              advanceTable.tax_rate_name +
              advanceTable.tax_type_ref_id +
              advanceTable.tax_authority_ref_id +
              advanceTable.from_date +
              advanceTable.to_date +
              advanceTable.revision_status
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.filteredData.slice();
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this.paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {}

  sortData(data: any[]): any[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'tenant_name':
          [propertyA, propertyB] = [a.tenant_name, b.tenant_name];
          break;
        case 'district_name':
          [propertyA, propertyB] = [a.district_name, b.district_name];
          break;
        case 'taluka_name':
          [propertyA, propertyB] = [a.taluka_name, b.taluka_name];
          break;
        case 'is_active':
          [propertyA, propertyB] = [a.is_active, b.is_active];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
