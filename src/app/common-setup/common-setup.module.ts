import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ComponentsModule } from '../shared/components/components.module';
import { DynamicFormModule } from "../shared/dynamic-form/dynamic-form.module";

import { CommonSetupRoutingModule } from './common-setup-routing.module';
import { DesignationComponent } from './designation/designation.component';
import { CommonSetupComponent } from './common-setup/common-setup.component';
import { SharedModule } from '../shared/shared.module';
import { UomComponent } from './uom/uom.component';
import { BankComponent } from './bank/bank.component';
import { SeasonComponent } from './season/season.component';
import { TaxAuthorityComponent } from './tax-authority/tax-authority.component';
import { TaxRateComponent } from './tax-rate/tax-rate.component';
import { UomConversionComponent } from './uom-conversion/uom-conversion.component';
import { WithHoldingComponent } from './with-holding/with-holding.component';
import { HsnSacComponent } from './hsn-sac/hsn-sac.component';
import { VehicleMasterComponent } from './vehicle-master/vehicle-master.component';


@NgModule({
  declarations: [
    DesignationComponent,
    CommonSetupComponent,
    UomComponent,
    BankComponent,
    SeasonComponent,
    TaxAuthorityComponent,
    TaxRateComponent,
    UomConversionComponent,
    WithHoldingComponent,
    HsnSacComponent,
    VehicleMasterComponent,
  ],
  imports: [
    CommonModule,
    CommonSetupRoutingModule,
    ComponentsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormModule,
    SharedModule
  ]
})
export class CommonSetupModule { }
