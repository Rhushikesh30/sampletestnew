import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithHoldingComponent } from './with-holding.component';

describe('WithHoldingComponent', () => {
  let component: WithHoldingComponent;
  let fixture: ComponentFixture<WithHoldingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WithHoldingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WithHoldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
